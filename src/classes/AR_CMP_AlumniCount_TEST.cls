@isTest
public class AR_CMP_AlumniCount_TEST 
{
    public static  testmethod void test1()
    {
         // set up some test data to work with
        company__c company = new Company__C(Name='Test098765', DUNS_Number__c='012345678');
        insert company;

        // start the test execution context
        Test.startTest();

        // set the test's page to your VF page (or pass in a PageReference)
        Test.setCurrentPage(Page.AR_CMP_AlumniCount_inline);

        // call the constructor
        AR_CMP_AlumniCount_CTRL controller = new AR_CMP_AlumniCount_CTRL(new ApexPages.StandardController(company));
		system.debug('Sample :: '+controller.allumniPresent);
        system.debug('Sample 2 :: '+controller.allumniCount);
       
        // stop the test
        Test.stopTest();
        
    }
	    
}