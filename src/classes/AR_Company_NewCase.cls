public with sharing class AR_Company_NewCase {
 public case ocase {get;set;}
 
 public List<Company__c> company{get; set;}
 public List<Contact> contacts{get; set;}
 public List<RecordType> recselection{get;set;}
 public integer  requestType{get;set;}
  public AR_Company_NewCase() {
    ocase = new  Case();
    recselection= new List<RecordType>();
    company = New List<Company__c>();
    contacts= new List<Contact>();
    requestType=validateRequest();
      if(requestType==1)
      {
        recselection =[Select id,Name from RecordType where Name=:ApexPages.currentPage().getParameters().get('rectype')];
        contacts=[select id,Name from contact where id=:ApexPages.currentPage().getParameters().get('contid')];	
     }
     else if(requestType==2)
      {
          recselection =[Select id,Name from RecordType where Name=:ApexPages.currentPage().getParameters().get('rectype')];
          company =[select id,Name from Company__c where id=:ApexPages.currentPage().getParameters().get('compid')];
      }
  
    
    Ocase.WCT_Category__c='Alumni Relations';
       if(requestType==1)
       {
            Ocase.WCT_SubCategory1__c='New Company Request';
            Ocase.Origin='Direct';   
           //Ocase.ownerId='00G40000001W7wz';
            if(contacts.size()>0)
            {
                /*Confirmmation of what should go in description.*/
                //Ocase.Description =contacts[0].id+Ocase.Description+' contact id ';
                Ocase.Subject='New Company ';
            }
       }
      else if(requestType==2)
       {
            Ocase.WCT_SubCategory1__c='Company Edit';
            Ocase.Origin='Direct';  
            //Ocase.ownerId='00G40000001W7wz';
            if(Company.size()>0)
            {
                /*Confirmmation of what should go in description.*/
                //Ocase.Description =Company[0].id+Ocase.Description+' company id ';
                Ocase.Subject='Edit Company '+Company[0].Name;
            }
        }
    }
    
    /*@desc This method validates the request input and return appropiate values :

		0- invalid Request 
		1- new Request
		2- edit Request
*/
    public integer validateRequest()
    {
        integer status = 0;
        if(ApexPages.currentPage().getParameters().get('actionType')!=null)
        {
            if(ApexPages.currentPage().getParameters().get('actionType')=='new')
            {
                if(ApexPages.currentPage().getParameters().get('contid')!=null )
                {
                    status = 1;
                }
            }
            else  if(ApexPages.currentPage().getParameters().get('actionType')=='edit')
            {
                if(ApexPages.currentPage().getParameters().get('compid')!=null )
                {
                    status = 2;
                    
                }
            }
            
        }
        
        return status;
    }
    
    public pageReference Save()
    {
        insert ocase;
        return new PageReference('/'+ocase.id);
    }

}