@isTest
public class AR_Company_NewCase_TEST {

    
     public static  testmethod void test1()
    {
         // set up some test data to work with
        company__c company = new Company__C(Name='Test098765', DUNS_Number__c='012345678');
        insert company;
       
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Contact; 
        Map<String,Schema.RecordTypeInfo> contactRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = contactRecordTypeInfo .get('Employee').getRecordTypeId();
        List<Contact> tempContact= WCT_UtilTestDataCreation.createContactAsCandidateLst(rtId);
        insert tempContact;

        // start the test execution context
        Test.startTest();

        // set the test's page to your VF page (or pass in a PageReference)
        Test.setCurrentPage(Page.AR_Company_NewCase);
        ApexPages.currentPage().getParameters().put('compid',company.id);
        ApexPages.currentPage().getParameters().put('actionType','edit');
        // call the constructor new ApexPages.StandardController(company)
        AR_Company_NewCase controller = new AR_Company_NewCase();
        
         // set the test's page to your VF page (or pass in a PageReference)
        Test.setCurrentPage(Page.AR_Company_NewCase);
        ApexPages.currentPage().getParameters().put('contid',tempContact[0].id);
        ApexPages.currentPage().getParameters().put('actionType','new');
        // call the constructor
         controller = new AR_Company_NewCase();
        
		// stop the test
        Test.stopTest();
        
    }
}