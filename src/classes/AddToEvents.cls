/**===============================================================================
 Class Name   : AddToEvents
 ===============================================================================
PURPOSE:       This class is invoked when user clicks on Add To Event
               button on the contact list view. This class adds the contacts
               as Event Registration to the selected Event.
                
CHANGE HISTORY
===============================================================================
DATE              NAME                         DESC

10/20/2013        Rajasekhara Reddy Yeturi     Created

===============================================================================*/

public class AddToEvents {

     //Variable Declaration
    ApexPages.StandardSetController setCon;
    public Event_Registration_Attendance__c eventRegObj {get;set;}
    public Integer selectedSize {get;set;}
    public String selectedMemberStatus{get;set;}
    public String selectedOption{get;set;}
    Set<Id> sObjectIds = new Set<Id>();        
    List<Event_Registration_Attendance__c> eventRegistrationsToCreate = new List<Event_Registration_Attendance__c>();
    Map<Id,Event_Registration_Attendance__c> eventRegistrationsExisting = new Map<Id,Event_Registration_Attendance__c>();
    public AddToEvents(ApexPages.StandardSetController controller) {
       setCon = controller;
       eventRegObj = new Event_Registration_Attendance__c();
       selectedSize = setCon.getSelected().size();
       selectedOption = '1';
       //If no contacts are selected, display an error message.
       if(selectedSize==0){  
        if(setCon.getRecord().getsObjectType() == Contact.sObjectType) 
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least one contact'));        
        else if(setCon.getRecord().getsObjectType() == Lead.sObjectType)
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least one Lead'));        
       } 
      for(sObject s:setCon.getSelected()) 
       sObjectIds.add(s.id);
    }
    
    //This method adds the selected contacts as Event Registrants.
    public pagereference AddeventRegistrations(){
     //Fetch existing Event Registrants with the same contact/lead and Recruiting Event Ids.
    if(setCon.getRecord().getsObjectType() == Contact.sObjectType) {
     // for(Event_Registration_Attendance__c c:[Select Contact__c,CRM_Status__c from Event_Registration_Attendance__c where Contact__c in:sObjectIds and Event__c =:eventRegObj.Id]) 
      for(Event_Registration_Attendance__c c:[Select Id,Contact__c from Event_Registration_Attendance__c where Contact__c in:sObjectIds and Event__c =: eventRegObj.Event__c])
             eventRegistrationsExisting.put(c.Contact__c,c);
      } 
      for(sObject con:setCon.getSelected()){
        Event_Registration_Attendance__c er = new Event_Registration_Attendance__c();
        if(eventRegistrationsExisting.containsKey(con.Id)){
          er = eventRegistrationsExisting.get(con.id);
          //Update campaign member status based on the selected values
          //er.CRM_Status__c = (selectedOption == '2' ? selectedMemberStatus : er.CRM_Status__C);         
        }else{
          er.Event__c = eventRegObj.Event__c;
          if(setCon.getRecord().getsObjectType() == Contact.sObjectType)
            er.Contact__c = con.Id; 
          //er.CRM_Status__c  = selectedMemberStatus;
              }         
         eventRegistrationsToCreate.add(er);
      }
      try{ 
        //Update or Insert campaign members
       upsert eventRegistrationsToCreate;       
      }catch(Exception e){
        ApexPages.addMessages(e);
        return null;
      } 
      
      //Redirect to the campaign detail page.
      Pagereference p= new Pagereference('/'+eventRegObj.Event__c);
      p.setRedirect(true);
      return p;
    }
}