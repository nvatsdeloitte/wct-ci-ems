public class Ar_listofattending {
public Event__c event{get;set;}
public datetime siteenddate {get;set;}
public datetime sitestartdate {get;set;}
public String eid = ApexPages.currentPage().getParameters().get('eid');
public ar_listofattending()
{
event=[select id,name,School__c,Venue_City_AR__c,AR_Start_Date_Time_Text__c,AR_End_Date_Time_Text__c,Venue_state_AR__c,AR_image_for_registration_page__c,End_Date_Time__c,Start_Date_time__c,NIR_Venue__c,recordtype.name from event__c where id=:eid];
List <String> stringParts = event.AR_Start_Date_Time_Text__c.split(' ');
            sitestartdate=date.parse(stringParts[0]);
            List <String> stringParts1 = event.AR_end_Date_Time_Text__c.split(' ');
            siteenddate=date.parse(stringParts1[0]);
}
public list<Event_Registration_Attendance__c> getlstatt()
{
list<Event_Registration_Attendance__c> finaler = new list<Event_Registration_Attendance__c>();
list<Event_Registration_Attendance__c> lstatt = [select contact__r.firstname,Contact_FSS__c, contact__r.AR_Current_Title__c,contact__r.lastname,curent_employer__c, city__c from Event_Registration_Attendance__c where Do_you_consent_to_having_your_name_and_e__c <> 'no'  and attending__c=true and event__c=:ApexPages.currentPage().getParameters().get('eid')];
for(Event_Registration_Attendance__c adder:lstatt)
{
if(adder.contact__r.AR_Current_Title__c=='Unknown'||adder.contact__r.AR_Current_Title__c==''||adder.contact__r.AR_Current_Title__c==null)
adder.contact__r.AR_Current_Title__c='';
finaler.add(adder);
}

return finaler;
}

public string getDocumentImageUrl()
    {
    
    
    Map<String,eEvent_AR_Images__c> allStates = eEvent_AR_Images__c.getAll();
    
    if(event.AR_image_for_registration_page__c<>null)
    {List<Document> lstDocument = [Select Id,Name from Document where id =: allStates.get(event.AR_image_for_registration_page__c).eEvent_AR_Images_ID__c limit 1];
    
    string strOrgId = UserInfo.getOrganizationId();
    string strDocUrl = '/servlet/servlet.ImageServer?oid=' + strOrgId + '&id=';
    return strDocUrl + lstDocument[0].Id;
    }
    
    else{
     string strOrgId = UserInfo.getOrganizationId();
    string strDocUrl = '/servlet/servlet.ImageServer?oid=' + strOrgId + '&id=';
    return strDocUrl + allStates.get('Tree branches growing.jpeg').eEvent_AR_Images_ID__c;}
    }
           
}