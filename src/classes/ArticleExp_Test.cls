/*****************************************************************************************
    Name    : ArticleExp_Test
    Desc    : Test class for ArticleExp class                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Rahul Agarwal                 09/10/2013         Created 
******************************************************************************************/
@isTest
public class ArticleExp_Test{
	/****************************************************************************************
    * @author      - Rahul Agarwal
    * @date        - 09/10/2013
    * @description - Test Method for ArticleExp's method kavNames
    *****************************************************************************************/
    public static testmethod void ArticleExp(){
    	/**********
    	// Calling Test Data Utility to create Knowledge Article Version
    	Test_Data_Utility.createKnowledgeArticleVersion();
    	list<KnowledgeArticleVersion> kavList = new list<KnowledgeArticleVersion>();
    	// Querying all the Knowledge Article Versions with PublishStatus = Draft and Language = en_US
    	kavList = [select KnowledgeArticleId from KnowledgeArticleVersion where PublishStatus = 'Draft' and Language = 'en_US'];
    	// Publishing all the queried articles
    	for(Integer i = 0; i < kavList.size(); i++){
    		KbManagement.PublishingService.publishArticle(kavList[i].KnowledgeArticleId, true);
    	}
    	test.startTest();
    	// Calling kavNames method of ArticleExp class
    	ArticleExp.kavNames();
    	test.stopTest();
    	************/
    }
}