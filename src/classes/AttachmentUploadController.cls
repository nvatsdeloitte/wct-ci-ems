public class AttachmentUploadController {
 
  public Attachment attachment {
  get {
      if (attachment == null)
        attachment = new Attachment();
      return attachment;
    }
  set;
  }
  public String custName {get;set;}
  public Id parentId{get;set;}
  public string isInterview{get;set;}  
  public String sflagvalue; 
  public string isdebriefchk{get;set;}  
  public AttachmentUploadController (){
  parentId   = Apexpages.currentpage().getparameters().get('id');
  sflagvalue = Apexpages.currentpage().getparameters().get('Flag');
  isInterview   = Apexpages.currentpage().getparameters().get('pg');   
  isdebriefchk  = Apexpages.currentpage().getparameters().get('dbchk');  
      system.debug('@@@@'+isdebriefchk);
  }
  
  public PageReference cancel(){
   if(sflagvalue=='yes')
    {
      return new PageReference('/apex/WCT_AttachmentRelatedList?id='+parentId);
    }
    else{
      return new PageReference('/'+parentId);
       }
  }
  
  //attach debrief note 
 public set<id> candidatelst()
 {
     set<id> candids=new set<id>();
        for(WCT_Interview_Junction__c IntJuncId :[select  id,WCT_Candidate_Tracker__c,WCT_Candidate_Tracker__r.WCT_Contact__c from WCT_Interview_Junction__c where WCT_Interview__c = :parentId and WCT_IEF_Submission_Status__c !='Canceled'])
        {   
            system.debug('@@@'+IntJuncId.WCT_Candidate_Tracker__r.WCT_Contact__c);
            candids.add(IntJuncId.WCT_Candidate_Tracker__r.WCT_Contact__c);
        }
     system.debug('$$$'+candids);
     return candids;
 }
 //update Interview record if debrief checkbox checked
 public void checkdebrief()
 {
     WCT_Interview__c intvw=new WCT_Interview__c();
     intvw.id=parentId;
     intvw.Debrief_Notes__c=true;
     update intvw;
 }
   public PageReference upload() { 
    list<attachment> attlst=new list<attachment>();
    if(isInterview=='debrief') 
    {
         
    set<id> candidates= candidatelst(); 
    system.debug('&&&'+candidates); 
    for(id cand:candidates)
    {
     attachment att=new attachment();
     //att=attachment;
     att.Body=attachment.Body;
     att.OwnerId = UserInfo.getUserId();
     att.ParentId = cand;
     if(custName != null && custName != ''){
     att.Name = custName+(attachment.Name).substring(((attachment.Name).lastIndexOf('.')),(attachment.Name).length());
     attlst.add(att);   
    }
        
    }
    }
    system.debug('####'+attlst);
    attachment.OwnerId = UserInfo.getUserId();
    attachment.ParentId = parentId;
    if(custName != null && custName != ''){
        attachment.Name = custName+(attachment.Name).substring(((attachment.Name).lastIndexOf('.')),(attachment.Name).length());
        
    }
     attlst.add(attachment); 
    try {
      insert attlst;
        if(isInterview=='debrief') {
            if(isdebriefchk=='0')
                checkdebrief();
        } 
    } catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
      return null;
    } finally {
      attachment = new Attachment(); 
    }
 
    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
    if(sflagvalue=='yes')
     {
       return new PageReference('/apex/WCT_AttachmentRelatedList?id='+parentId);
     }
     else{
        return new PageReference('/'+parentId);
       }
  }
 
}