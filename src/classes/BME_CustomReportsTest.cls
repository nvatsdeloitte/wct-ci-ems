/**
        Class Name: BME_CustomReportsTest
        Created By: Balu Devarapu(Deloitte India)
        Started: 21/07/2015

        Description: Test class for ProjectsByTimeLineController Class.              
    **/

@IsTest()
private class BME_CustomReportsTest{

    /**
        Test Method : testProjectsByTimeLine
        Description: Test Method for code coverage of ProjectsByTimeLineController.
    **/

  private static testmethod void testProjectsByTimeLine(){
      Test.startTest();
      Contact Objcon=new Contact(LastName='TestUSI(USI)');
      insert objCon;
      Contact Objcon2=new Contact(LastName='TestUS(US)');
      insert objCon2;
      Id USILead=objCon.Id;
      Id USLead=objCon2.Id;
      string LeadName=objCon2.LastName;
      Project__c objProj=new Project__c();
      objProj.Name='Test123';
      objProj.Talent_Channel__c='AOC';
      objProj.Project_Start_Date__c=system.today();
      objProj.Project_Planned_End_Date__c=system.today().addDays(2);
      objProj.Priority__c='Critical';
      objProj.Project_Lead_US__c=USLead;
      objProj.Project_Lead_USI__c=USILead;
      insert objProj;
      

      
      Project__c objProj1=new Project__c();
      objProj1.Name='Test1231';
      objProj1.Talent_Channel__c='CEO';
      objProj1.Project_Start_Date__c=system.today();
      objProj1.Project_Planned_End_Date__c=system.today().addMonths(6);
      objProj1.Priority__c='High';
      objProj1.Project_Lead_USI__c=USILead;
      objProj1.Project_Lead_US__c=USLead;
      insert objProj1;
      
      Project__c objProj2=new Project__c();
      objProj2.Name='Test1232';
      objProj2.Talent_Channel__c='BAs';
      objProj2.Project_Start_Date__c=system.today();
      objProj2.Project_Planned_End_Date__c=system.today().addMonths(8);
      objProj2.Priority__c='Medium';
      objProj2.Project_Lead_US__c=USLead;
      objProj2.Project_Lead_USI__c=USILead;
      insert objProj2;
      
      Project__c objProj3=new Project__c();
      objProj3.Name='Test12321';
      objProj3.Talent_Channel__c='BAs';
      objProj3.Project_Start_Date__c=system.today();
      objProj3.Project_Planned_End_Date__c=system.today().addMonths(4);
      objProj3.Priority__c='Low';
      objProj3.Project_Lead_US__c=USLead;
      objProj3.Project_Lead_USI__c=USILead;
      insert objProj3;
      
      ProjectsByTimeLineController objPBTC=new ProjectsByTimeLineController();
      for(integer i=0;i<=12;i++){
      ProjectsByTimeLineController.ConvertToMonth(i);
      }
      objPBTC.FilterResult();
      string TestStr='test';
      PageReference pRef = Page.ProjectsByTimeLine;
      pRef.getParameters().put('SearchLead', UserInfo.getLastName());
      pRef.getParameters().put('Priority', 'High');
      Test.setCurrentPage(pRef);
      ProjectsByTimeLineController objPBTC2=new ProjectsByTimeLineController();
      objPBTC2.FilterResult();
      
      PageReference pRef1 = Page.ProjectsByTimeLine;
      pRef1.getParameters().put('SearchLead', LeadName);
      pRef1.getParameters().put('Priority', '');
      Test.setCurrentPage(pRef1);
      ProjectsByTimeLineController objPBTC3=new ProjectsByTimeLineController();
      objPBTC3.FilterResult();
      
      PageReference pRef2 = Page.ProjectsByTimeLine;
      pRef2.getParameters().put('SearchLead', '');
      pRef2.getParameters().put('Priority', 'High');
      Test.setCurrentPage(pRef2);
      ProjectsByTimeLineController objPBTC4=new ProjectsByTimeLineController();
      objPBTC4.FilterResult();
      
      PageReference pRef3 = Page.ProjectsByTimeLine;
      pRef3.getParameters().put('SearchLead', '');
      pRef3.getParameters().put('Priority', '');
      Test.setCurrentPage(pRef3);
      ProjectsByTimeLineController objPBTC5=new ProjectsByTimeLineController();
      objPBTC5.FilterResult();
      
      System.assert(TestStr!=null);
      Test.stopTest();
  }

  /**
  Test Method : testProjectsByPerson
  Description: Test Method for code coverage of ProjectsByPersonController.
  **/

  private static testmethod void testProjectsByPerson(){
      Test.startTest();
      Contact Objcon=new Contact(LastName='TestUSI(USI)');
      insert objCon;
      Contact Objcon2=new Contact(LastName='TestUS(US)');
      insert objCon2;
      Id USILead=objCon.Id;
      Id USLead=objCon2.Id;
      string LeadName=objCon2.LastName;
      Project__c objProj=new Project__c();
      objProj.Name='Test123';
      objProj.Talent_Channel__c='AOC';
      objProj.Project_Start_Date__c=system.today();
      objProj.Project_Planned_End_Date__c=system.today().addDays(2);
      objProj.Priority__c='Critical';
      objProj.Project_Lead_US__c=USLead;
      objProj.Project_Lead_USI__c=USILead;
      objProj.Status__c='In Progress';
      insert objProj;
      

      
      Project__c objProj1=new Project__c();
      objProj1.Name='Test1231';
      objProj1.Talent_Channel__c='CEO';
      objProj1.Project_Start_Date__c=system.today();
      objProj1.Project_Planned_End_Date__c=system.today().addMonths(6);
      objProj1.Priority__c='High';
      objProj1.Project_Lead_USI__c=USILead;
      objProj1.Project_Lead_US__c=USLead;
      objProj1.Status__c='In Progress';
      insert objProj1;
      
      Project__c objProj2=new Project__c();
      objProj2.Name='Test1232';
      objProj2.Talent_Channel__c='BAs';
      objProj2.Project_Start_Date__c=system.today();
      objProj2.Project_Planned_End_Date__c=system.today().addMonths(8);
      objProj2.Priority__c='Medium';
      objProj2.Project_Lead_US__c=USLead;
      objProj2.Project_Lead_USI__c=USILead;
      objProj2.Status__c='In Progress';
      insert objProj2;
      
      Project__c objProj3=new Project__c();
      objProj3.Name='Test12321';
      objProj3.Talent_Channel__c='BAs';
      objProj3.Project_Start_Date__c=system.today();
      objProj3.Project_Planned_End_Date__c=system.today().addMonths(4);
      objProj3.Priority__c='Low';
      objProj3.Project_Lead_US__c=USLead;
      objProj3.Project_Lead_USI__c=USILead;
      objProj3.Status__c='In Progress';
      insert objProj3;
      
      ProjectsByPersonController objPBTC=new ProjectsByPersonController();

      objPBTC.FilterResult();
      string TestStr='test';
      PageReference pRef = Page.ProjectsByPerson;
      pRef.getParameters().put('SearchLead', '');
      Test.setCurrentPage(pRef);
      ProjectsByPersonController objPBTC2=new ProjectsByPersonController();
      objPBTC2.FilterResult();
      
      PageReference pRef1 = Page.ProjectsByPerson;
      pRef1.getParameters().put('SearchLead', LeadName);
      Test.setCurrentPage(pRef1);
      ProjectsByPersonController objPBTC3=new ProjectsByPersonController();
      objPBTC3.FilterResult();
      

      
      System.assert(TestStr!=null);
      Test.stopTest();
  }

    /**
        Test Method : testProjectsByTalentChannel
        Description: Test Method for code coverage of ProjectsByTalentChannelController.
    **/

  private static testmethod void testProjectsByTalentChannel(){
      
      Test.startTest();
      
      Project__c objProj=new Project__c();
      objProj.Name='Test123';
      objProj.Talent_Channel__c='AOC';
      insert objProj;
      
      Project__c objProj1=new Project__c();
      objProj1.Name='Test1231';
      objProj1.Talent_Channel__c='CEO';
      insert objProj1;
      
      Project__c objProj2=new Project__c();
      objProj2.Name='Test1232';
      objProj2.Talent_Channel__c='BAs';
      insert objProj2;
      
      ProjectsByTalentChannelController objPBTC=new ProjectsByTalentChannelController();
      objPBTC.FilterResult();
      string TestStr='test';
      PageReference pRef = Page.ProjectsByTalentChannel;
      pRef.getParameters().put('SelectedTC', 'BAs');
      Test.setCurrentPage(pRef);
      ProjectsByTalentChannelController objPBTC2=new ProjectsByTalentChannelController();
      objPBTC2.FilterResult();
      PageReference pRef2 = Page.ProjectsByTalentChannel;
      pRef2.getParameters().put('SelectedTC', '-None-');
      Test.setCurrentPage(pRef2);
      ProjectsByTalentChannelController objPBTC3=new ProjectsByTalentChannelController();
      objPBTC3.FilterResult();
      System.assert(TestStr!=null);
      Test.stopTest();
      }
  
  
}