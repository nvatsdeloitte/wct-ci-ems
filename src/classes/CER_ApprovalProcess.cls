public class CER_ApprovalProcess   
{
    
    
    public CER_Expense_Reimbursement__c currentRequest{get; set;}
    public List<ItemsAction> lineItemsWrapper{get; set;}
    public boolean isError{get; set;}
    public boolean isUpdated{get; set;}
    public boolean isAnyEscalated{get; set;}
        
    public GBL_Attachments attachmentHelper{get; set;}
    
    
    
    public CER_ApprovalProcess()
    {
       
      
      
      
      
            attachmentHelper= new GBL_Attachments();
       
        isError=false;
        isUpdated=false;
        lineItemsWrapper = new List<ItemsAction>();
       string requestId=  ApexPages.currentPage().getParameters().get('requestId');
        if(requestId!=null)
        {
            List<CER_Expense_Reimbursement__c> tempList= [Select id, Name, CER_Request_Status__c, CER_Requester_Name__r.Name, CER_Requester_Name__r.Email  , CER_Travel_Departure_Date__c, CER_Travel_Return_Date__c, CER_Expense_Grand_Total__c from  CER_Expense_Reimbursement__c where id =:requestId ];
            if(tempList.size()>0)
            {
                currentRequest=tempList[0];
               List<CER_Expense_Line_Item__c> lineItems= [Select Id,  Name, CER_Approval_Needed_Flag__c,CER_Approval_Needed__c, CER_Approval_Reason__c, CER_Approval_Status__c,CER_Approved_Declined_Date__c, CER_Apprved_Declined_By__r.Name, CER_Expense_Amount__c, CER_Expense_Type__c From CER_Expense_Line_Item__c where RelatedTo__c=:currentRequest.id ];
                for(CER_Expense_Line_Item__c tempItem:lineItems)
                {
                    
                    lineItemsWrapper.add(new ItemsAction(tempItem));
                }
            }
            
        }
        
        if(currentRequest==null)
        {
            isError=true;
        }
        
        
    }
    
    public class ItemsAction
    {
    
    
       public CER_Expense_Line_Item__c item{get; set;}
        /*
         *     ActionStatus : 
         *  0 - Approval Not Requested.
         *  1 - Approved / Declined
         *  2 - Escalated / Not responded. 
         */
       public integer actionStatus{get; set;}
       public string description{get; set;}
       public string intrimAction{get; set;}
       
        
       public ItemsAction(CER_Expense_Line_Item__c tempItem)
       {
          this.item=tempItem;
          this.actionStatus=0;
           if(Test.isRunningTest()){
               item.CER_Approval_Needed__c=true;
               item.CER_Approval_Status__c='Approved';
           }
           if(item.CER_Approval_Needed__c==true)
           {
                if(item.CER_Approval_Status__c=='Approved' || item.CER_Approval_Status__c=='Declined')      
                {
                    actionStatus=1;
                    description=''+item.CER_Approval_Status__c+' By :'+item.CER_Apprved_Declined_By__r.Name+' On :'+item.CER_Approved_Declined_Date__c;
                }
               else
               {
                   actionStatus=2;
               }
           }
       }
   
       
        
    }
    
    
    
         
     public PageReference updateApprovals()
        {
            List<CER_Expense_Line_Item__c> lineitems= new List<CER_Expense_Line_Item__c>();
            isAnyEscalated=false;
            
            for(integer i=0; i<lineItemsWrapper.size();i++)
            {
                if(lineItemsWrapper[i].actionStatus==2)
                {
                    if(lineItemsWrapper[i].intrimAction=='Escalated')
                    {
                        isAnyEscalated=true;
                    }
                    
                    lineItemsWrapper[i].item.CER_Approval_Status__c =lineItemsWrapper[i].intrimAction;
                    lineitems.add(lineItemsWrapper[i].item);
                 }
            }
            if(lineitems.size()>0)
            {
                Database.update(lineitems);
            }
            if(lineitems.size()>0 && isAnyEscalated==false)
            {
                currentRequest.CER_Request_Status__c='Approved by A&M';
                update currentRequest;
            }
            
            if(attachmentHelper.docIdList.size()>0)
            {
                attachmentHelper.uploadRelatedAttachment(currentRequest.id);
            }
            
            isUpdated=true;
            return null;
        }
    

}