public class CER_ExpenseLineItemHandler {

    
    public static void updateAcceptedByAndOn(List<CER_Expense_Line_Item__c> newExpenseLineItems, List<CER_Expense_Line_Item__c> oldExpenseLineItems)
    {
        for(integer i=0; i<newExpenseLineItems.size();i++)
        {
            if(newExpenseLineItems[i].CER_Approval_Status__c!=oldExpenseLineItems[i].CER_Approval_Status__c)
            {
                newExpenseLineItems[i].CER_Apprved_Declined_By__c=UserInfo.getUserId();
                newExpenseLineItems[i].CER_Approved_Declined_Date__c=Datetime.now();
            }
        }
    }
}