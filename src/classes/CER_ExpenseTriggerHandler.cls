/**************************************************************************************
Apex Class Name:  CER_ExpenseTriggerHandler
Version           : 1.0 
Created Date      : 07-28-2015.
Function          : Handler class for the Expense Request Trigger.
Function Template : public static void <functionName>(<Params>)
Modification Log  :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deepu Ginde             07-28-2015             Original Version
*************************************************************************************/



public class CER_ExpenseTriggerHandler {

   public CER_ExpenseTriggerHandler()
    {
    }
   
    /*
     * Function Name     : syncGroupWithRecordType
     * Functionality     : Sync the Group and Record Type, 
     * 					     If Group is changed by the user, then change the Records as well.
     * Params		     : New Trigger Values
     * Expected Instance : Before => Update & Insert.
     * Created Date      :  07-28-2015
	 */
    public static void syncGroupWithRecordType(List<CER_Expense_Reimbursement__c> newExpenses)
    {
        Map<String, String> GRP_RTID_MAP=new Map<String, String>();
        GRP_RTID_MAP.put('Federal', Label.CER_Candidate_Expense_FEDERAL_Record_Type_ID);
        GRP_RTID_MAP.put('PDAT', Label.CER_Candidate_Expense_PDAT_Record_Type_ID);
        GRP_RTID_MAP.put('Federal PDAT', Label.CER_Candidate_Expense_PDAT_FEDERAL_Record_Type_ID);
        GRP_RTID_MAP.put('Other', Label.CER_Candidate_Expense_OTHER_Record_Type_ID);
        for(CER_Expense_Reimbursement__c expense : newExpenses)
        {
            if(GRP_RTID_MAP.get(expense.CER_Expense_Group__c)!=null)
            {
                expense.recordTypeId=GRP_RTID_MAP.get(expense.CER_Expense_Group__c);
            }
        }
    }
    
    /*
     * Function Name     : checkForDupeExp
     * Functionality     : Identify the Expense Request need any Review if so, flag it for Review. The following are the cases for same:
     * 						1. Same Contact with Same Grand total Line  
     * Params		     : New Trigger Values
     * Expected Instance : Before => Update.
     * Created Date      :  07-28-2015
	 */
    public static void checkForDupeExp(List<CER_Expense_Reimbursement__c> newExpenses, List<CER_Expense_Reimbursement__c> oldExpenses)
    {
        List<ID> contactids= new List<Id>();
        Map<String, CER_Expense_Reimbursement__c> expenseCandMap=new Map<String, CER_Expense_Reimbursement__c>();
        Map<String, String> expenseDupes=new Map<String, String>();
        /*Known Issue in Bulk -> If Expenses with same contact. The Map will maintain only one expense request for one contact.*/
        for(CER_Expense_Reimbursement__c expense : newExpenses)
        {
			contactids.add(expense.CER_Requester_Name__c);   
            expenseCandMap.put(expense.CER_Requester_Name__c, expense);
        }
        /*Query the All existing Expense Reimbursement System*/
        if(contactids.size()>0)
        {
        	List<CER_Expense_Reimbursement__c> extExpenses=[Select id,Name,CER_Expense_Grand_Total__c,CER_Requester_Name__c FRom CER_Expense_Reimbursement__c where CER_Requester_Name__c in :contactids   ];
            for(CER_Expense_Reimbursement__c expense : extExpenses)
            {
                if(expense.CER_Expense_Grand_Total__c==expenseCandMap.get(expense.CER_Requester_Name__c).CER_Expense_Grand_Total__c)
                {
                    string tempDupeList=expenseDupes.get(expenseCandMap.get(expense.CER_Requester_Name__c).id)!=null?expenseDupes.get(expenseCandMap.get(expense.CER_Requester_Name__c).id)+','+expense.Name:expense.Name;
                    // Avoid the Self Record to be marked as dupe to itself. 
                    if(expense.id!=expenseCandMap.get(expense.CER_Requester_Name__c).id)
                    {
                        expenseDupes.put(expenseCandMap.get(expense.CER_Requester_Name__c).id,tempDupeList);
                    }
                }
            }
            
            for(CER_Expense_Reimbursement__c expense : newExpenses)
            {
                if(expenseDupes.get(expense.id)!=null)
                {
                    expense.CER_SF_Dupe_Expenses__c=expenseDupes.get(expenseCandMap.get(expense.CER_Requester_Name__c).id);
                }
            }

        }
        
                
    }
    
    /*
     * Function Name     : flagForReview
     * Functionality     : Identify the Expense Request need any Review if so, flag it for Review. The following are the cases for same:
     * 						1. Same contact with Same Travel Dates.
     * 						2. More than Five Exising Expense.
     * 						3. Check for a tracker with Interview in between the Travel Dates.
     * Params		     : New Trigger Values
     * Expected Instance : Before => Insert.
     * Created Date      :  07-28-2015
	 */
    public static void flagForReview(List<CER_Expense_Reimbursement__c> newExpenses)
    {
		List<ID> contactids= new List<Id>();
        Map<String, CER_Expense_Reimbursement__c> expenseCandMap=new Map<String, CER_Expense_Reimbursement__c>();
        Map<String, String> overlapExp=new Map<String, String>();
        Map<String, integer> noOfExp=new Map<String, integer>();
        Map<String, boolean> isTrackerPresent=new Map<String, boolean>();
        
        for(CER_Expense_Reimbursement__c expense : newExpenses)
        {
			contactids.add(expense.CER_Requester_Name__c);   
            expenseCandMap.put(expense.CER_Requester_Name__c, expense);
        }
       if(contactids.size()>0)
       {
           /*Query with Overlapping dates. */
           Date currentdate = date.Today();
           Date lastyeardate= date.newInstance(currentdate.year()-1, currentdate.month(),currentdate.day() );
           List<CER_Expense_Reimbursement__c> extExpenses=[Select id,Name,CER_Travel_Return_Date__c, CER_Travel_Departure_Date__c, CER_Requester_Name__c From CER_Expense_Reimbursement__c where CER_Requester_Name__c in :contactids and createdDate > :lastyeardate limit :Limits.getLimitQueryRows()];
           for(CER_Expense_Reimbursement__c expense : extExpenses)
            {
                date extExpDeptDate=expense.CER_Travel_Departure_Date__c;
                date extExpRetDate=expense.CER_Travel_Return_Date__c;
                date newExpDepDate=expenseCandMap.get(expense.CER_Requester_Name__c).CER_Travel_Departure_Date__c;
                date newExpRetDate=expenseCandMap.get(expense.CER_Requester_Name__c).CER_Travel_Return_Date__c;
                
                
                /*
                 * Overlapping Expenses : 
                 * IF Start date fall between the other expense Start and End Date 
                 * 		OR 
                 * 	  End Date falls between the Other Expense Start and End Date.
					*/
                if(extExpDeptDate!=null && extExpRetDate!=null &&  newExpDepDate!=null && newExpRetDate!=null)
                {
                    if((extExpDeptDate>=newExpDepDate && extExpDeptDate<newExpRetDate)||(extExpRetDate>=newExpDepDate &&  extExpRetDate<newExpRetDate))
                    {
                        string tempDupeList=overlapExp.get(expenseCandMap.get(expense.CER_Requester_Name__c).id)!=null?overlapExp.get(expenseCandMap.get(expense.CER_Requester_Name__c).id)+','+expense.Name:expense.Name;
                        overlapExp.put(expenseCandMap.get(expense.CER_Requester_Name__c).id,tempDupeList);
                    }
                }
                
                
                integer count=noOfExp.get(expenseCandMap.get(expense.CER_Requester_Name__c).id)!=null?noOfExp.get(expenseCandMap.get(expense.CER_Requester_Name__c).id)+1:1;
                noOfExp.put(expenseCandMap.get(expense.CER_Requester_Name__c).id,count);
            }
           
           List<WCT_Interview_Junction__c> intervTrackers = [Select  Id, Name, WCT_Start_Date_Time__c, WCT_End_Date_Time__c, WCT_Candidate_Tracker__r.WCT_Contact__c  From WCT_Interview_Junction__c where WCT_Candidate_Tracker__r.WCT_Contact__c in :contactids ];
           for(WCT_Interview_Junction__c intervTracker : intervTrackers)
           {
               CER_Expense_Reimbursement__c tempExpense= expenseCandMap.get(intervTracker.WCT_Candidate_Tracker__r.WCT_Contact__c);
               if(tempExpense!=null)
               {
                   /*Checking Start Date and End date is present . - Scenario such as Pending Interview might not have Start and End Date. */
                   if(intervTracker.WCT_Start_Date_Time__c!=null && intervTracker.WCT_End_Date_Time__c!=null)
                   {
                        Date StartDate= intervTracker.WCT_Start_Date_Time__c.date();
                        Date EndDate= intervTracker.WCT_End_Date_Time__c.date();
                        if((StartDate>=tempExpense.CER_Travel_Departure_Date__c && StartDate<=tempExpense.CER_Travel_Return_Date__c)||(EndDate>=tempExpense.CER_Travel_Departure_Date__c && EndDate<=tempExpense.CER_Travel_Return_Date__c))
                           {
                               isTrackerPresent.put(tempExpense.id,true);
                               
                           }
                   }
                   
               }
           }
           
            for(CER_Expense_Reimbursement__c expense : newExpenses)
            {
                boolean flagged=false;
                if(overlapExp.get(expense.id)!=null)
                {
                    flagged=true;
                    expense.CER_SF_Overlapping_Expenses__c=overlapExp.get(expense.id);
                }
                 if(noOfExp.get(expense.id)!=null)
                {
                    if(noOfExp.get(expense.id)>5)
                    {
                       flagged=true;
                    }
                    expense.CER_SF_Number_of_Ext_Expenses__c=noOfExp.get(expense.id);
                }
                
                if( (isTrackerPresent.get(expense.id)!=null) || (expense.RecordTypeID==Label.CER_Candidate_Expense_PDAT_Record_Type_ID) || (expense.RecordTypeID==Label.CER_Candidate_Expense_PDAT_FEDERAL_Record_Type_ID))
                {
                    expense.CER_Is_Cand_Tracker_Present__c=true;
                }
                else
                {
                    flagged=true;
                    expense.CER_Is_Cand_Tracker_Present__c=false;
                }
                expense.CER_SF_Requires_Lead_Review__c=flagged;
            }
       }
    }
    
    /*
     * Function Name     : updateTimeStamps
     * Functionality     : 
     * Params		     : New Trigger Values
     * Expected Instance : Before => Insert.
     * Created Date      :  07-28-2015
	 */
    public static void updateTimeStamps(List<CER_Expense_Reimbursement__c> newExpenses, List<CER_Expense_Reimbursement__c> oldExpenses)
    {
       List<Integer> indexToHandle=new List<Integer>();
        Map<String, List<CER_Expense_Reimbursement__History>> historyIdMap=new Map<String, List<CER_Expense_Reimbursement__History>>();
       List<CER_Expense_Reimbursement__c> listtoHandle= new List<CER_Expense_Reimbursement__c>();
       for(integer i=0; i<newExpenses.size();i++)
       {
           if(newExpenses[i].CER_Request_Status__c!=oldExpenses[i].CER_Request_Status__c)
           {
               listtoHandle.add(newExpenses[i]);
               indexToHandle.add(i);
           }
       }
       Set<Id> idstoHandle = (new Map<Id,SObject>(listtoHandle)).keySet();
       if(idstoHandle.size()>0)
        {
			List<CER_Expense_Reimbursement__History> histories= [Select ID, OldValue, NewValue, Field, CreatedDate, CreatedById ,parentId From CER_Expense_Reimbursement__History where Field= 'CER_Request_Status__c' and parentId in :idstoHandle order by parentId,CreatedDate ];
            for(CER_Expense_Reimbursement__History history:histories)
            {
                List<CER_Expense_Reimbursement__History> tempList=historyIdMap.get(history.parentId)==null?new List<CER_Expense_Reimbursement__History>(): historyIdMap.get(history.parentId);
                tempList.add(history);
                historyIdMap.put(history.parentId, tempList);
            }
        }
        
        /*Iterate thourgh All */
        for(integer index : indexToHandle)
        {
            CER_Expense_Reimbursement__c currentExpense= newExpenses[index];
            Map<String, integer> currentExpensemetrics = new Map<String, integer>();
            datetime prevStatusDate=null;
            string prevStatus=null;
            if(historyIdMap.get(currentExpense.id)!=null)
            {
                List<CER_Expense_Reimbursement__History> curExpHistories=historyIdMap.get(currentExpense.id);
                for(integer i=0; i<curExpHistories.size();i++)
                {
                    
                    if(i==0)
                    {
                       currentExpensemetrics=CER_ExpenseTriggerHandler.updateMaterics(String.valueOf(curExpHistories[i].OldValue), currentExpense.CreatedDate, curExpHistories[i].createdDate, currentExpensemetrics);
                    }
                    if(prevStatusDate!=null)
                    {
                        if(prevStatus==curExpHistories[i].OldValue)
                        {
                           currentExpensemetrics= CER_ExpenseTriggerHandler.updateMaterics(prevStatus, prevStatusDate,  curExpHistories[i].createdDate,currentExpensemetrics);
                        }
                    }
                    prevStatus=String.valueOf(curExpHistories[i].NewValue);
                    prevStatusDate= curExpHistories[i].CreatedDate;
                }
                if(prevStatusDate!=null)
                {
                    currentExpensemetrics= CER_ExpenseTriggerHandler.updateMaterics(prevStatus, prevStatusDate, datetime.now(),currentExpensemetrics);
                }
                
            }
            else
            {
                /*This is the code to handle the very first update on the Request Status,in which case no exising history records are avaiable. */
                currentExpensemetrics=CER_ExpenseTriggerHandler.updateMaterics(oldExpenses[index].CER_Request_Status__c, currentExpense.createdDate, datetime.now(),currentExpensemetrics);
            }
            
            
             /*Update the Timeout into the Record. both for very first time Request status update and for secound Request update.*/
                currentExpense.CER_BH_Time_in_Submitted__c=getTime('Submitted by Candidate', currentExpensemetrics)+getTime('Submitted by Candidate - Pending Original Receipt(s)', currentExpensemetrics);
                currentExpense.CER_BH_Time_in_Under_Review__c=getTime('Under Review', currentExpensemetrics);
                currentExpense.CER_BH_Time_in_Complete__c=getTime('Complete', currentExpensemetrics);
                currentExpense.CER_BH_Time_in_Sent_for_Payment__c=getTime('Sent for Payment', currentExpensemetrics);
                currentExpense.CER_BH_Time_in_Pending_Correction__c=getTime('PENDING CORR.', currentExpensemetrics);
            
        }
    }
    
    public static void updateTotalProcessingTime(List<CER_Expense_Reimbursement__c> newExpenses, List<CER_Expense_Reimbursement__c> oldExpenses)
    {
       List<Integer> indexToHandleTotalProTime=new List<Integer>();
       List<Integer> indexToHandleAPProTime=new List<Integer>();
       List<Integer> indexToHandleTotalCycleTime=new List<Integer>();
       
        
        
       for(integer i=0; i<newExpenses.size();i++)
       {
           if( (newExpenses[i].CER_Paid_Date__c!=oldExpenses[i].CER_Paid_Date__c ) && newExpenses[i].CER_Paid_Date__c!=null)
           {
                   /*TOTAL PROCESSING : Check that Ownwership taken date and padi date are updated and not equal to null.*/
                   if(newExpenses[i].CER_Ownership_taken_Date__c!=null)
                   {
                       indexToHandleTotalProTime.add(i);
                   }           
                   
                   /*AP PROCESSING  TIME */
                   if(newExpenses[i].CER_Sent_For_Payment_Date__c!=null)
                   {
                       indexToHandleAPProTime.add(i);
                   }  
         
                  /*Total Cycle  TIME */
                  indexToHandleTotalCycleTime.add(i);
		   }  
       }
        
        /*Iterate thourgh All indexToHandleAPProTime */
        for(integer index : indexToHandleAPProTime)
        {
            CER_Expense_Reimbursement__c currentExpense= newExpenses[index];
            Long diffMS= BusinessHours.diff(Label.VRM_Team_Business_hours_ID,currentExpense.CER_Sent_For_Payment_Date__c, currentExpense.CER_Paid_Date__c);
        	Integer minutes= (integer)diffMS/(60*1000);
            currentExpense.CER_BH_AP_Processing_Time__c=minutes;
        }
         
        /*Iterate thourgh All  indexToHandleTotalProTime */
        for(integer index : indexToHandleTotalProTime)
        {
            CER_Expense_Reimbursement__c currentExpense= newExpenses[index];
            Long diffMS= BusinessHours.diff(Label.VRM_Team_Business_hours_ID,currentExpense.CER_Ownership_taken_Date__c, currentExpense.CER_Paid_Date__c);
        	Integer minutes= (integer)diffMS/(60*1000);
            currentExpense.CER_BH_Total_Processing_Time__c=minutes;
        }
        /*Iterate thourgh All */
        for(integer index : indexToHandleTotalCycleTime)
        {
            CER_Expense_Reimbursement__c currentExpense= newExpenses[index];
            Long diffMS= BusinessHours.diff(Label.VRM_Team_Business_hours_ID,currentExpense.createdDate, currentExpense.CER_Paid_Date__c);
        	Integer minutes= (integer)diffMS/(60*1000);
            currentExpense.CER_BH_Total_Cycle_Time__c=minutes;
        }
    }
    
    public static integer getTime(String Status, Map<String, integer> metrics)
    {
        integer i=(metrics.get(Status)!=null)?metrics.get(Status):0;
        return i;
    }
    
     public static Map<String, Integer> updateMaterics(string status, datetime Startdate, datetime endDate, Map<String, Integer> metrics)
    {
                    /*
                    integer intDays =  Startdate.Date().daysBetween(endDate.Date());
                    datetime sameDayEndDate = Startdate.addDays(intDays);
                                        
                        decimal decMin = ((endDate.getTime())/1000/60) - ((sameDayEndDate.getTime())/1000/60);
                     */
                        /************************************************
                         * 01mc00000008PQB
                        intDays : this value show the number of day b/w the given datetime (2 days for the given dates)
                        decHours : this value shows the number of (12 hours for the given dates )
                         */
                    /*
                        decMin= (intDays*24*60)+decMin;
                        system.debug('decMin '+decMin);
                        system.debug('(int)decHours '+((integer)decMin));
                    */
        
        			/*
        			 * Difference Between start and end date with respect to Business Hours in minutes. 
        			 */
        			Long diffMS= BusinessHours.diff(Label.VRM_Team_Business_hours_ID,Startdate, endDate);
        		    Integer minutes= (integer)diffMS/(60*1000);
                    integer existingTime= metrics.get(status)!=null?metrics.get(status):0;
                    metrics.put(status, existingTime+((integer)minutes));
                    return metrics;
    }
}