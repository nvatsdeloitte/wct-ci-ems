public class CER_FlagForApproval {

    public List<CER_Expense_Line_Item__c> records{get; set;}
    public string returnURL{get; set;}
    public boolean isError{get; set;}   
    public List<Id> ids;
    public CER_FlagForApproval()
    {
    
    
        string selectedid= ApexPages.currentPage().getParameters().get('selectedId');
        returnURL=ApexPages.currentPage().getParameters().get('returnURL');
        if(returnURL==null)
        {
            returnURL='\\';
        }
         if(selectedid!='' && selectedid !=null )
         {
            ids=selectedid.split(',');
            records= [Select id, Name, CER_Approval_Needed__c,  CER_Approval_Status__c,CER_Approval_Reason__c,CER_Expense_Amount__c, CER_Expense_Type__c From CER_Expense_Line_Item__c where id in :ids ];
             for(CER_Expense_Line_Item__c record : records)
             {
               record.CER_Approval_Needed__c=true;  
             }
         }
         else
         {
             isError=true;
         }
        
        
    }
    
    public PageReference updateFlags()
    {
        Database.update(records);
        return new PageReference(returnURL);
    }
    
     public PageReference cancelAction()
    {
        return new PageReference(returnURL);
    }
    
}