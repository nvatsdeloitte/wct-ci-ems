@isTest
public class CER_Test1{
    
     public static testmethod void testDate()
     {
          User testUser= WCT_UtilTestDataCreation.createUser('testUser',Label.Integration_Profile_Id,'testUser@gmail.com.test.ignore','testUser@gmail.com');
          insert testUser;
         
         /**/
          Contact testcontact = WCT_UtilTestDataCreation.createAdhocContactsbyEmail('test@gmail.com');
          insert testcontact;
         
         /**/
         CER_Expense_Reimbursement__c request= CER_Test1.getExpense(testcontact, testUser.id);
         insert request;
         
         
         
         CER_Expense_Line_Item__c item1= CER_Test1.getExpenseLine('Mileage',request.id);
         CER_Expense_Line_Item__c item2= CER_Test1.getExpenseLine('Tips', request.id);
         List<CER_Expense_Line_Item__c> items = new  List<CER_Expense_Line_Item__c>();
         items.add(item1);
         items.add(item2);
         insert items;
     }
    
    
    public static CER_Expense_Reimbursement__c getExpense(contact testcontact,string reccoord)
    {
        
        CER_Expense_Reimbursement__c request= new CER_Expense_Reimbursement__c();
         request.CER_Requester_Name__c=testcontact.id;
         //request.CER_NON_US_Address__c=true;
         request.CER_Request_Status__c='Submitted by Candidate';
         request.CER_Recruiter_Coordinator__c=reccoord;
         return request;
    }
    
    public static CER_Expense_Line_Item__c getExpenseLine(string expenseType, string expenseId)
    {
        
        CER_Expense_Line_Item__c item= new CER_Expense_Line_Item__c();
        item.CER_Expense_Type__c=expenseType;
         //request.CER_NON_US_Address__c=true;
        item.CER_Expense_Amount__c=20;
        item.CER_Expense_Explanation__c='Test';
        item.RelatedTo__c=expenseId;
        return item;
    }
    
    
    private static testmethod void testCER_FlagForApproval(){
  
    CER_FlagForApproval objSU=new CER_FlagForApproval();
    //records= [Select id, Name, CER_Approval_Needed__c,  CER_Approval_Status__c,CER_Approval_Reason__c,CER_Expense_Amount__c, CER_Expense_Type__c From CER_Expense_Line_Item__c where id in :ids ];
      CER_Test1.testDate();
      Test.startTest();
      String Ids=[select Id from CER_Expense_Line_Item__c limit 1].Id;
      PageReference pRef = Page.CER_FlagForApproval;
      pRef.getParameters().put('selectedId', Ids);
      pRef.getParameters().put('returnURL', 'https://www.google.com');
      Test.setCurrentPage(pRef);
      CER_FlagForApproval objSU2=new CER_FlagForApproval();
      objSU2.updateFlags();
      objSU2.cancelAction();
      
      string TestStr='Test';
      System.assert(TestStr!=null);
      Test.stopTest();
    }
    
    private static testmethod void testCER_StatusUpload(){
  
      
      CER_Test1.testDate();
      CER_StatusUpload objSU=new CER_StatusUpload();
      Test.startTest();
      objSU.contentFile=Blob.valueOf('Test Data/n test /n testsetet/n are <br/> testet');
      objSU.isError=true;
      
      List<CER_Expense_Reimbursement__c> lstER=[select Name from CER_Expense_Reimbursement__c limit 5];
      List<string> lstNames=new List<string>();
      for(CER_Expense_Reimbursement__c objER:lstER){
      lstNames.add(objER.Name);
      }
      lstNames.add('PAID');
      lstNames.add('PENDING CORR.');
      objSU.IdentifierRequest(lstNames);
      
      List<List<string>> lstFileName=new List<List<string>>();
      lstFileName.add(lstNames);
      objSU.ProcessRequest(lstFileName);
      objSU.readFile();
       
      string TestStr='Test';
      System.assert(TestStr!=null);
      Test.stopTest();
    }
    
    private static testmethod void testCER_ApprovalProcess(){
  
      CER_ApprovalProcess objSU=new CER_ApprovalProcess();
      CER_Test1.testDate();
      Test.startTest();
      String Ids=[select Id from CER_Expense_Reimbursement__c limit 1].Id;
      PageReference pRef = Page.CER_ApprovalProcess;
      pRef.getParameters().put('requestId', Ids);
      
      Test.setCurrentPage(pRef);
      CER_ApprovalProcess objSU2=new CER_ApprovalProcess();
      objSU2.updateApprovals();
      
      
      string TestStr='Test';
      System.assert(TestStr!=null);
      Test.stopTest();
    }
    
    private static testmethod void testCER_SendForPayment(){
  
    CER_SendForPayment objSU=new CER_SendForPayment();
    //records= [Select id, Name, CER_Approval_Needed__c,  CER_Approval_Status__c,CER_Approval_Reason__c,CER_Expense_Amount__c, CER_Expense_Type__c From CER_Expense_Line_Item__c where id in :ids ];
      CER_Test1.testDate();
      Test.startTest();
      CER_Expense_Reimbursement__c objExL=[select Id,CER_Request_Status__c,CER_Sent_For_Payment_Date__c from CER_Expense_Reimbursement__c limit 1];
      PageReference pRef = Page.CER_SendForPayment;
      pRef.getParameters().put('id', objExL.Id);
      Test.setCurrentPage(pRef);
      CER_SendForPayment objSU2=new CER_SendForPayment();
      objSU2.currentRequest=objExL;
      objSU2.updateRequest();
     
      
      string TestStr='Test';
      System.assert(TestStr!=null);
      Test.stopTest();
    }
    
    
    
      
}