@isTest
public class CER_Test2 {
   public static CER_Expense_Reimbursement__c gblrequest= new CER_Expense_Reimbursement__c();
   public static testmethod void testDate()
     {
          User testUser= WCT_UtilTestDataCreation.createUser('testUser',Label.Integration_Profile_Id,'testUser@gmail.com.test.ignore','testUser@gmail.com');
          insert testUser;
         
         /**/
          Contact testcontact = WCT_UtilTestDataCreation.createAdhocContactsbyEmail('test@gmail.com');
          insert testcontact;
         
         /**/
          gblrequest= CER_Test2.getExpense(testcontact, testUser.id);
         insert gblrequest;
         
         
         
         CER_Expense_Line_Item__c item1= CER_Test2.getExpenseLine('Mileage',gblrequest.id);
         CER_Expense_Line_Item__c item2= CER_Test2.getExpenseLine('Tips', gblrequest.id);
         List<CER_Expense_Line_Item__c> items = new  List<CER_Expense_Line_Item__c>();
         items.add(item1);
         items.add(item2);
         insert items;
     }
    
    
    public static CER_Expense_Reimbursement__c getExpense(contact testcontact,string reccoord)
    {
        
        CER_Expense_Reimbursement__c request= new CER_Expense_Reimbursement__c();
         request.CER_Requester_Name__c=testcontact.id;
         //request.CER_NON_US_Address__c=true;
         request.CER_Request_Status__c='Submitted by Candidate';
         request.CER_Recruiter_Coordinator__c=reccoord;
         return request;
    }
    
    public static CER_Expense_Line_Item__c getExpenseLine(string expenseType, string expenseId)
    {
        
        CER_Expense_Line_Item__c item= new CER_Expense_Line_Item__c();
        item.CER_Expense_Type__c=expenseType;
         //request.CER_NON_US_Address__c=true;
        item.CER_Expense_Amount__c=20;
        item.CER_Expense_Explanation__c='Test';
        item.RelatedTo__c=expenseId;
        return item;
    }
    
    /*Test the update WBS Element. */
    public static testmethod void test1()
    {
        CER_Test2.testDate();
        Test.startTest();
          PageReference pRef = Page.CER_UpdateWBSElement;
          pRef.getParameters().put('selectedId', UserInfo.getLastName());
          Test.setCurrentPage(pRef);
          CER_UpdateWBSElement objPBTC2=new CER_UpdateWBSElement();
           objPBTC2.updateWBSElement();
           objPBTC2.cancelAction();
      Test.stopTest();

    }
    
    /*Test Intake form*/
    public static testmethod void testIntakeForm()
    {
        CER_Test2.testDate();
        Test.startTest();
            CER_ReimbursementRequestForm_CTRL tempCntrl = new CER_ReimbursementRequestForm_CTRL();
            tempCntrl.emailId='test@gmail.com';
            tempCntrl.emailValidate();
        /*Validate the Rec coordinator. */
            tempCntrl.expenseRequest.CER_Rec_Coordinator_Email__c='testUser@gmail.com';
            tempCntrl.validateRecCord();
        
        /* Update a Expense Item. */
            tempCntrl.tempExpenseLineItem.CER_Expense_Type__c='Mileage';
            tempCntrl.tempExpenseLineItem.CER_Expense_Explanation__c='sample';
            tempCntrl.tempExpenseLineItem.CER_Total_Miles_Travelled__c=20;
            tempCntrl.expenseType();
        /*Show Expense page from Trip details. */
            tempCntrl.expenseRequest.CER_Travel_Departure_Date__c=date.today();
            tempCntrl.showExpense();
        
        /*Remove a EXpense type */
        PageReference pRef = Page.CER_ReimbursementRequestForm;
          pRef.getParameters().put('index','1');
          Test.setCurrentPage(pRef);
        tempCntrl.removeExpenseType();
        
        /**/
		tempCntrl.next();        
        tempCntrl.prev();
        tempCntrl.getContactInfo();
        tempCntrl.expenseRequest.CER_Expense_Group__c=tempCntrl.REQUEST_GROUP_PDAT_FEDERAL;
        tempCntrl.getIsFED();
        tempCntrl.getIsPDAT(); 
        tempCntrl.getTotalExpense();
        tempCntrl.getMoreNumOfQuery();
        tempCntrl.submitForm();
        
        pRef = Page.CER_ReimbursementRequestForm;
          pRef.getParameters().put('requestId',tempCntrl.expenseRequest.id);
          Test.setCurrentPage(pRef);
        CER_ReimbursementRequestForm_CTRL temp2Cntrl = new CER_ReimbursementRequestForm_CTRL();
        temp2Cntrl.submitMissingExpense();

         
        Test.stopTest();
        
        
        
    }
    
    /*Test Security Flag*/
    public static testmethod void testSecurityFlag()
    {
        CER_Test2.testDate();
        Test.startTest();
        
        ApexPages.Standardcontroller sp = new ApexPages.Standardcontroller(gblrequest);
        ApexPages.currentPage().getparameters().put('id',gblrequest.id);
        CER_SecurityFlag flag= new CER_SecurityFlag(sp);
        
        
        Test.stopTest();
        
    }
    
    public static testmethod void TEST_CER_Candidate_Expense_Helper()
    {
        
        	CER_Test2.testDate();
        Test.startTest();
        
        List<CER_Expense_Reimbursement__c> requests = [Select Id from CER_Expense_Reimbursement__c where CER_Requester_Name__r.email='test@gmail.com'];
        List<Id> ids= new List<Id>();
        for(CER_Expense_Reimbursement__c request :requests )
        {
            ids.add(request.Id);
        }
        CER_Candidate_Expense_Helper.createEventToFollowRejection(ids);
        Test.stopTest();
        
    }
    
    
    
    
}