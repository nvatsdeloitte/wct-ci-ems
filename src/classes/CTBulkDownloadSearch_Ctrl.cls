public class CTBulkDownloadSearch_Ctrl{
    
    //Variable Declaration
    public Date startDate {get;set;}
    public Date EndDate {get;set;}
    
    public WCT_Candidate_Requisition__c tempCandTrackerFromDate {get;set;}
    /*Used to fetch the filter value of Document start date and end date. This is used to get Datepicker for same.*/
    public WCT_Candidate_Requisition__c tempCandTrakerForDocDates {get;set;}
    /*Used to fetch the start date of Interview. this is used to get Datepicker. And the CTS_recruiter & CTS_Recruitor_Coordinator doesnt have */
    public WCT_Candidate_Requisition__c tempCandTrakerForInterviewDate {get;set;}
    public contact tempcontactforUserLookup {get;set;}
    public contact tempcontactforRecruiterLookup {get;set;}
    /*Used for Tracker To date, Recruiter Co-ordinator*/
    public WCT_Candidate_Requisition__c tempCandTrackerToDate {get;set;}
    //public WCT_Interview__c interviewStartDate {get;set;}
   
    
    public string FSSValue {get;set;}
    public string interviewJobtype {get;set;}
    public string SchoolValue {get;set;}
    public string Recruiter {get;set;}
    public string RecCoordinator {get;set;}
    public string candidateEmailId {get;set;}
    public List<WCT_Candidate_Requisition__c> candTrackerList {get;set;}
    public boolean renderCTList {get;set;}
    public list<candTrackWrapper> candTrackWrapList {get;set;}
    public List<Id> contIds{get;set;}
    public List<Attachment> attList {get;set;}
         integer QUERY_LIMIT=2000;
    integer ATTACHMENT_QUERY_LIMIT=400;
    
    public string CandidateLastName {get;set;}

    public string documentName{get;set;}

    
    //attachment page
     public String zipFileName {get; set;}
    public String zipContent {get; set;}
    public String AttIdsArr {get; set;}  
    public Id docId {get;set;}
    public List<Attachment> attachments {get;set;}
    public List<AttWrapper> attachmentsWrapper  {get;set;}
    
    
    
    public static string formatDate(Date dt, string fmt) {
        return (dt==null) ? null : DateTime.newInstance(dt,Time.newInstance(0,0,0,0)).format(fmt);
    }
    
    public static string DatetoXml(date dt) {
        return (dt==null) ? null : formatDate(dt,'yyyy-MM-dd');  
    }

    public class candTrackWrapper{
        public boolean isSelected{get;set;}
        public WCT_Candidate_Requisition__c candTracker{get;set;}
        
        public candTrackWrapper(boolean selected,WCT_Candidate_Requisition__c candtrack){
            isSelected = selected;
            candTracker = candtrack;
        }
    
    }
    
    //Constructor
    public CTBulkDownloadSearch_Ctrl(){
        renderCTList = false;
        tempCandTrackerFromDate = new WCT_Candidate_Requisition__c();
        tempCandTrackerToDate = new WCT_Candidate_Requisition__c();
        tempCandTrakerForInterviewDate= new WCT_Candidate_Requisition__c() ;
        tempCandTrakerForDocDates= new WCT_Candidate_Requisition__c() ;
        /*Used for filter Requisition.*/
        tempCandTrackerToDate.WCT_Requisition__r= new WCT_Requisition__c();
       
        tempcontactforUserLookup= new contact();
        tempcontactforRecruiterLookup= new contact();
        attList = new List<Attachment>();     
        tempCandTrackerFromDate.WCT_Contact__r= new Contact();
        
    }
    
    public void searchCandidates(){
        candTrackWrapList = new list<candTrackWrapper>();
        candTrackerList = new List<WCT_Candidate_Requisition__c>();
        String query;
        boolean isValidSearch=true;
        query = 'SELECT id,name,WCT_Contact__c,WCT_Contact__r.name,WCT_FSS__c,WCT_Recruiter__c,Requisition_RC_R__c,WCT_CR_School__c FROM WCT_Candidate_Requisition__c WHERE Name !=null';
        /*Validation Rule goes here :*/
        if(tempCandTrackerFromDate.WCT_Time_till_start_of_first_Interview__c > tempCandTrackerToDate.WCT_Time_till_start_of_first_Interview__c){
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL,'\'From Start Date\' should be before \'To Start Date\''));
             isValidSearch=false;
        }
        
        /*If Document name is given the Document start date and End date must be given. and From Date should be smaller than the To date  */
        
        if(documentName!=null && documentName!='')
        {
        boolean temp=true;
            if(tempCandTrakerForDocDates.WCT_Time_to_Schedule_First_Interview__c==null )
            {
              ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL,'Document created from date is mandatory when document Name is used as filter'));
             isValidSearch=false;
             temp=false;
            }
            if(tempCandTrakerForDocDates.WCT_Time_till_start_of_first_Interview__c==null )
            {
              ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL,'Document created to date is mandatory when document Name is used as filter'));
             isValidSearch=false;
             temp=false;
            }
            if(temp)
            {
             if(tempCandTrakerForDocDates.WCT_Time_to_Schedule_First_Interview__c > tempCandTrakerForDocDates.WCT_Time_till_start_of_first_Interview__c){
                     ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL,'\'Document From Start Date\' should be before \' Document To Start Date\''));
                     isValidSearch=false;
                }
            
            }
        
        }
        
        /*Proceed only when valid search filters are provided*/
        
     if(isValidSearch)
     {
               
        if(FSSValue!= null && FSSValue!=''){
            query += ' AND WCT_FSS__c LIKE \'%'+FSSValue.trim()+'%\'' ;
        }
        if(SchoolValue!= null && SchoolValue!=''){
            query += ' AND WCT_CR_School__c LIKE \'%'+SchoolValue.trim()+'%\'' ;
        }
        if(tempcontactforRecruiterLookup.NDR_Owner__c!= null ){
            query += ' AND WCT_Requisition__r.WCT_Recruiter__c = \''+tempcontactforRecruiterLookup.NDR_Owner__c+'\'' ;
        }
        if(tempcontactforUserLookup.NDR_Owner__c!= null ){
            query += ' AND WCT_Requisition__r.WCT_Recruiting_Coordinator__c = \''+tempcontactforUserLookup.NDR_Owner__c+'\'' ;
        }
        if(tempCandTrackerFromDate.WCT_Time_till_start_of_first_Interview__c!=null){
            query+= ' AND CreatedDate__c >='+ DatetoXml(tempCandTrackerFromDate.WCT_Time_till_start_of_first_Interview__c.date());
        }
        if(tempCandTrackerToDate.WCT_Time_till_start_of_first_Interview__c!=null){
            query+= ' AND CreatedDate__c <='+ DatetoXml(tempCandTrackerToDate.WCT_Time_till_start_of_first_Interview__c.date());
        }
        if(tempCandTrakerForInterviewDate.WCT_Time_till_start_of_first_Interview__c!=null){
            query+= ' AND DAY_ONLY(WCT_Related_Interview_Tracker__r.WCT_Start_Date_Time__c) <='+ DatetoXml(tempCandTrakerForInterviewDate.WCT_Time_till_start_of_first_Interview__c.date());
        }
        if(tempCandTrackerFromDate.WCT_Requisition__c!=null){
            query+= ' AND WCT_Requisition__c =\''+ tempCandTrackerFromDate.WCT_Requisition__c+'\'';
        }
         system.debug('#### Dasd Debug tempCandTrackerFromDate.WCT_Contact__r.lastnam '+tempCandTrackerFromDate.WCT_Contact__r.lastname);
        if(tempCandTrackerFromDate.WCT_Contact__r.lastname!= null && tempCandTrackerFromDate.WCT_Contact__r.lastname!=''){
                
            
            query+= ' AND WCT_Contact__r.Name like \'%'+tempCandTrackerFromDate.WCT_Contact__r.lastname+'%\'';
            
   
        }
        if(candidateEmailId!=null && candidateEmailId!='')
        {
             
              query += ' AND WCT_Contact__r.Email LIKE \''+candidateEmailId.trim()+'\'' ;
        }
        if(interviewJobtype!=null && interviewJobtype!='')
        {
            query+= ' AND WCT_Related_Interview_Tracker__r.WCT_Interview__r.WCT_Job_Type__c =\''+ interviewJobtype+'\'';
        }
        
        /*Fileter Document Name */
         
        if(documentName!=null && documentName!='')
        {
            /*Document Name Search filter provided*/
              List<Id> candidateTrackerIds=new List<Id>();
              List<Id> interviewTrackerIds=new List<Id>();
              List<Id> candidateIds=new List<Id>();
              List<Id> candidateDocumentIds=new List<Id>();
            
            /*Query 1 : Query the Attachments to get all the Attachments with parent type "Interview"*/
            string query3='Select ParentId, Parent.Type From Attachment where parent.type in (\'WCT_Interview_Junction__c\',\'WCT_Candidate_Requisition__c\',\'WCT_Candidate_Documents__c\') and Name Like \'%'+documentName+'%\' and DAY_ONLY(createdDate)>= '+DatetoXml(tempCandTrakerForDocDates.WCT_Time_to_Schedule_First_Interview__c.date())+' and DAY_ONLY(createdDate)<= '+DatetoXml(tempCandTrakerForDocDates.WCT_Time_till_start_of_first_Interview__c.date());
            List<Attachment> filteredAttachment=Database.query(query3);
            
            /*Processing agreement result to Candidate Ids, Canidate Tracker Id  and  InterviewTracker Id */
            for(Attachment attachment :filteredAttachment)
            {
                if(attachment.Parent.Type=='WCT_Interview_Junction__c')
                {
                    interviewTrackerIds.add(attachment.ParentId);
                }
                else if(attachment.Parent.Type=='WCT_Candidate_Requisition__c')
                {
                    candidateTrackerIds.add(attachment.ParentId);
                }
                else  if(attachment.Parent.Type=='WCT_Candidate_Documents__c')
                {
                    candidateDocumentIds.add(attachment.ParentId);
                }
            }
            /*Query 2 
                Query the Interview Tracker to get the associated Canidate Tracker id. Already have the ids of the interview tracker with matching document name as attachment.
                  Add the resultant canidate Tracker ids to candidateTrackerIds list.   */
            if(interviewTrackerIds.size()>0)
            {
                List<WCT_Interview_Junction__c> interviewjunctions=[select WCT_Candidate_Tracker__c from WCT_Interview_Junction__c where id in :interviewTrackerIds];
                for(WCT_Interview_Junction__c interviewjunction : interviewjunctions)
                {
                    if(interviewjunction.WCT_Candidate_Tracker__c!=null)
                    candidateTrackerIds.add(interviewjunction.WCT_Candidate_Tracker__c);
                }
            }
            
            /*Query 3 
                Query the candidate documents to get the candidate id associeted with it. Already have the ids of the canidate documents with matching document name in the  
                Add the resultant canidate ids to candidateIds list.*/
            if(candidateDocumentIds.size()>0)
            {
                List<WCT_Candidate_Documents__c> candidateDocs=[select WCT_Candidate__c from WCT_Candidate_Documents__c where id in :candidateDocumentIds];
                for(WCT_Candidate_Documents__c candidateDoc : candidateDocs)
                {
                    if(candidateDoc.WCT_Candidate__c!=null)
                    candidateIds.add(candidateDoc.WCT_Candidate__c);
                }
            }
            
            
            
            /*The above quries leads to two list of Ids : CandidateTracker ids and Canidate ids. 
            Add the folowing conditions as "where clause" in  "Master Query on Candidate Tracker :
              1. Candidate tracker id should be one of the id in candidateTrackerIds
                                Or 
             2. Canidate id of the Candidate Tracker should be one of the  candidateIds
            */
                      
             query+= ' AND ( id in (';
                 for(Id id : candidateTrackerIds)
                 {
                     if(id!=null)
                  query+='\''+id+'\',';
                 }
            query+=' \'\' ) ';
            
            query+= ' OR WCT_Contact__c in (';
                 for(Id id : candidateIds)
                 {
                     if(id!=null)
                  	query+='\''+id+'\',';
                 }
            query+=' \'\' ) )';
                   
        }
        /*END : Filter Document Name*/
         system.debug('#### Dasd Debug documentName '+query);
        query += ' order by createdDate desc LIMIT '+QUERY_LIMIT;
        candTrackerList = DataBase.query(query);
        
        
        renderCTList = true;
        if(!candTrackerList.isEmpty()){
            renderCTList = true;
            
            for(WCT_Candidate_Requisition__c ct:candTrackerList){
                candTrackWrapList.add(new candTrackWrapper(false,ct));
            }
            
   

        }//else{renderCTList = false;}
        //system.debug('&&&'+candTrackerList[0].name);
     }
    }
    
    /*Describe the picklist value of the Job type . this is used in the Vf page to provide the Job Type picklist . Cant Use directly the Picklist for InputField as it is dependable picklist. */
   public List<SelectOption> getJobTypeValues()
    {
      List<SelectOption> options = new List<SelectOption>();
            
       Schema.DescribeFieldResult fieldResult =WCT_Interview__c.WCT_Job_Type__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
       for( Schema.PicklistEntry f : ple)
       {
          options.add(new SelectOption(f.getLabel(), f.getValue()));
       }       
       return options;
    }
    public pageReference getDocuments(){
        contIds = new List<Id>();
        if(!candTrackWrapList.isEmpty()){
        for(candTrackWrapper cw:candTrackWrapList){
            if(cw.isSelected){
                if(cw.candTracker.WCT_Contact__c!=null){
                contIds.add(cw.candTracker.WCT_Contact__c); 
                }           
            }
        } 
        }
        candTrackWrapList= new list<candTrackWrapper>();
        getAttachmentsfromIds(contIds);
         PageReference returnURL;
         returnURL =  Page.WCT_SObjectAttachmentZipPage;
         
        returnURL.getParameters().put('nooverride', '1');
        returnURL.setRedirect(false);  
        return returnURL;
    }
    
    /*
        Method Name  : uploadZip   
        Return Type  : Void
    */
    public void uploadZip() {
        if (String.isEmpty(zipFileName) ||
            String.isBlank(zipFileName)) {
            zipFileName = 'BulkZip_File.zip';//Default Name for zip file.
        }
        else {
            zipFileName.replace('.', '');// To prevent file extension
            zipFileName += '.zip';
        }
         
        Document doc = new Document();
        doc.Name = zipFileName;
        doc.ContentType = 'application/zip';
        doc.FolderId = Label.Attachment_Zip_Document_Folder_Id;
        doc.Body = EncodingUtil.base64Decode(zipContent);
        
        try{
            insert doc;  
        }Catch(DMLException ex)
        {
            WCT_ExceptionUtility.logException('WCT_AttachmentZipController-uploadZip', null, ex.getMessage() + ' ::: ' + ex.getStackTraceString());
        }        
        docId = doc.Id; 
        this.zipFileName = null;
        this.zipContent = null;
    }
    
    

    /*
        Method Name  : getAttachments   
        Return Type  : List of Attachments
    */
    public List<AttWrapper> getAttachmentsfromIds(List<Id> idList) {
        attachmentsWrapper = new List<AttWrapper>();
        list<String> attIds = new list<String>();
        if(AttIdsArr!=null)attIds = AttIdsArr.split(',');
        Map<Id,string> ContactAttParentIdMap = new Map<Id,string>();

        set<Id> attParentId = new set<Id>();
        if(!idList.isEmpty()){
            attParentId.addAll(idList);
            
            for(Contact c: [SELECT id,Name FROM Contact where Id IN :idList]){
                ContactAttParentIdMap.put(c.id,c.Name);
            }
            
            for(WCT_Candidate_Documents__c candidDocId : [SELECT Id,WCT_Candidate__r.name FROM WCT_Candidate_Documents__c where WCT_Candidate__c  IN :idList]){
                attParentId.add(candidDocId.Id);
                ContactAttParentIdMap.put(candidDocId.id,candidDocId.WCT_Candidate__r.Name);
            }
            for(WCT_Candidate_Requisition__c canTrackerId : [SELECT Id,WCT_Contact__r.name FROM WCT_Candidate_Requisition__c where WCT_Contact__c  IN :idList]){
                attParentId.add(canTrackerId.Id);
                ContactAttParentIdMap.put(canTrackerId.id,canTrackerId.WCT_Contact__r.Name);
            }
            for(WCT_Interview_Junction__c canReqId : [SELECT Id,WCT_Candidate_Tracker__r.WCT_Contact__r.name FROM WCT_Interview_Junction__c where WCT_Candidate_Tracker__r.WCT_Contact__c IN :idList]){
                attParentId.add(canReqId.Id);
                ContactAttParentIdMap.put(canReqId.id,canReqId.WCT_Candidate_Tracker__r.WCT_Contact__r.name);
            }
        }
        if(attIds.isEmpty())
            attachments = [select Id, ParentId, Parent.Name, Parent.Type, Name, ContentType, BodyLength, CreatedDate from Attachment where parentId in :attParentId Limit :ATTACHMENT_QUERY_LIMIT ];
        else
            attachments = [select Id, ParentId, Parent.Name, Parent.Type, Name, ContentType, BodyLength, CreatedDate from Attachment where Id in :attIds Limit :ATTACHMENT_QUERY_LIMIT ];
        //return attachments ;
        for(Attachment att:attachments){
        attachmentsWrapper.add(new AttWrapper(att,ContactAttParentIdMap.get(att.ParentId)));
        }
        return attachmentsWrapper;
    }



    //Attachment wrapper
    public class AttWrapper{
        public Attachment att{get;set;}
        public String candidateName {get;set;}
        
        public AttWrapper(Attachment retAtt,string cand){
            att = retAtt;
            candidateName = cand;
        }       
    }



    @RemoteAction
    public static AttachmentWrapper getAttachment(String attId) {
         
        Attachment att = [select Id, Name, ContentType, Body
                          from Attachment
                          where Id = :attId];
         
        AttachmentWrapper attWrapper = new AttachmentWrapper();
        attWrapper.attEncodedBody = EncodingUtil.base64Encode(att.body);
        attWrapper.attName = att.Name;
        return attWrapper;
    }
    /*
        Wrapper Class: AttachmentWrapper
        Description  : Wrapper class to wrap Attachment Name and Attachment body.
    */   
    
    public class AttachmentWrapper {
        public String attEncodedBody {get; set;}
        public String attName {get; set;}
    }
    
    
}