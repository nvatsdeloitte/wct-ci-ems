/*****************************************************************************************
    Name    : Case_Utility
    Desc    : Utility class for Case object. This class will hold all the logic for case triggers.
    Approach: 
                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Rahul Agarwal               30 Aug, 2013         Created 
Rahul Agarwal               02 Sep, 2013         Modified
Charles Mutsu               06 Sep, 2013         Modified
Vivek Sharma                03 Mar, 2014         Modified
Rakesh Ala                  07 Mar, 2014         Modified
******************************************************************************************/
public Class Case_Utility{
    public static boolean beforeTrigger = false;
     
  /****************************************************************************************
     * @author      - Rahul Agarwal
     * @date        - 2 Sep, 2013 
     * @description - This method queries all the recordtype of the sobject type passed, forms a map of record type developer name vs record type
                      and returns the map.
     * @param       - SObjectType being captured in string variable SobjectTypePassed
     *****************************************************************************************/
  public static map<String, RecordType> devNameRecType(String SobjectTypePassed){
    map<String, RecordType> devNameRecTypeMap = new map<String, RecordType>();
    list<RecordType> RecTypeList = new list<RecordType>();
    // querying all the case recordtypes
    RecTypeList = [Select Id, DeveloperName from RecordType where SobjectType =: SobjectTypePassed];
    // populating devNameRecTypeMap map to store RecordType developerName vs RecordType key value pair
    for(RecordType r : RecTypeList)
      devNameRecTypeMap.put(r.DeveloperName, r);
    return devNameRecTypeMap;
  }
  /****************************************************************************************
     * @author      - Rahul Agarwal
     * @date        - 30 Aug, 2013 
     * @description - This method is called from CaseTrigger in before insert operation. This method encrypts the Subject and Description on Case and 
               copies the originally entered Subject to WCT_TRSecureSubject__c field and Description to WCT_TRSecureDescription__c field when Case
               Record Type is Talent Relations Concern and Subject to WCT_ELESecureSubject__c field and WCT_ELESecureDescription__c field
               when Case Record Type is either Employee Lifecycle Events - Voluntary Separations or Employee Lifecycle Events - Involuntary Separations
     * @param       - trigger.new list is captured in TriggerNew list.
     * @author      - Charles Mutsu
     * @date        - 06 Sep, 2013
     * @description - Added Business Hours to calculate Due Date        
     *****************************************************************************************/
  public static void updateCaseSubjectAndDes(list<Case> TriggerNew){
    
    
    Map<String, RecordType> devNameRecTypeMap = new Map<String, RecordType>();
    devNameRecTypeMap = Case_Utility.devNameRecType('Case');
    // Capturing Recordtype developername into strings
    String Talent_Relations_EmployeeMatter = 'TalentRelations_Employee_Matter';
    String Employee_Lifecycle_Events_Voluntary_Separations = 'EmployeeLifecycleEvents_VoluntarySeparations';
    String Employee_Lifecycle_Events_Involuntary_Separations = 'EmployeeLifecycleEvents_InvoluntarySeparations';
   
    // Getting the encryptions for subject and description from custom labels
    //String encryptedSubject = Label.Encrypted_Subject;
    //String encryptedDescription = Label.Encrypted_Description;
    String seperatedCaseSubject = Label.Separation_Case_Subject;
    String seperatedCaseDescription = Label.Separation_Case_Description;
    String talentCaseSubject = Label.Talent_Relations_Case_Subject;
    String talentCaseDescription = Label.Talent_Relations_Case_Description;
    // looping over all the triggers being inserted
    for(Case c : TriggerNew){
      // If the Case Due Date field is blank at the time of creation populate Due Date with CreatedDate + two days
      //if(c.WCT_Due_Date__c == null && c.RecordTypeId == devNameRecTypeMap.get(Development_LearningDelivery).Id){
      //  c.WCT_Due_Date__c = BusinessHours.add(bh.id, startTime, 48 * 60 * 60 * 1000L);
      //}
      if(c.ContactId != null && c.WCT_ReportedBy__c == null){
        c.WCT_ReportedBy__c = c.ContactId;
      }
      // If the Case Record Type is Talent Relations - Concern
      if(c.RecordTypeId == devNameRecTypeMap.get(Talent_Relations_EmployeeMatter).Id){
        // Copying original Subject to another field and encrypting original Subject field
        if(c.Subject <> null){
          c.WCT_TRSecureSubject__c = c.Subject;
        }
        c.Subject = talentCaseSubject;
        // Copying original Description to another field and encrypting original Description field
        if(c.Description <> null){
          c.WCT_TRSecureDescription__c = c.Description;
        }  
        c.Description = talentCaseDescription;
      }
      // If the Case Record Type is Employee Lifecycle Events - Involuntary Separations
      else if((c.RecordTypeId == devNameRecTypeMap.get(Employee_Lifecycle_Events_Involuntary_Separations).Id)){
        // Copying original Subject to another field and encrypting original Subject field
        if(c.Subject <> null){
          c.WCT_ELESecureSubject__c = c.Subject;
        }
        c.Subject = seperatedCaseSubject;
        // Copying original Description to another field and encrypting original Description field
        if(c.Description <> null){
          c.WCT_ELESecureDescription__c = c.Description;
        }
        c.Description = seperatedCaseDescription;
      }
    }
  }
  /****************************************************************************************
     * @author      - Rahul Agarwal
     * @date        - 2 Sep, 2013 
     * @description - 
     * @param       - trigger.newMap map is captured in TriggerNewMap map.
     * @param       - trigger.oldMap map is captured in TriggerOldMap map.
     *****************************************************************************************/
  public static void caseRecordTypeChanged(map<Id, Case> TriggerNewMap, map<Id, Case> TriggerOldMap){
    map<String, RecordType> devNameRecTypeMap = new map<String, RecordType>();
    devNameRecTypeMap = Case_Utility.devNameRecType('Case');
    //capturing RecordType developer names into strings
    String Case_RecordType = 'Case';
    String Talent_Relations_EmployeeMatter = 'TalentRelations_Employee_Matter';
    String Employee_Lifecycle_Events_Involuntary_Separations = 'EmployeeLifecycleEvents_InvoluntarySeparations';
    // Getting the encryptions for subject and description from custom labels
    //String encryptedSubject = Label.Encrypted_Subject;
    //String encryptedDescription = Label.Encrypted_Description;
    String seperatedCaseSubject = Label.Separation_Case_Subject;
    String seperatedCaseDescription = Label.Separation_Case_Description;
    String talentCaseSubject = Label.Talent_Relations_Case_Subject;
    String talentCaseDescription = Label.Talent_Relations_Case_Description;
    set<Id> reportedByIds = new set<Id>();
    for(Case c : TriggerNewMap.values()){
        if(c.WCT_ReportedBy__c <> null)
            reportedByIds.add(c.WCT_ReportedBy__c);
    }
    map<Id, Contact> reportedByMap = new map<Id, Contact>([select Id, Name from Contact where Id IN : reportedByIds]);
    for(Case c : TriggerNewMap.values()){
      // checking if recordtype got channged from 'Case' to 'TalentRelations_Concern' 
      if(TriggerOldMap.get(c.Id).RecordTypeId == devNameRecTypeMap.get(Case_RecordType).Id && 
          c.RecordTypeId == devNameRecTypeMap.get(Talent_Relations_EmployeeMatter).Id){
        if(c.WCT_TRSecureDescription__c <> null && !c.WCT_TRSecureDescription__c.contains('Original Subject')){
            c.WCT_TRSecureDescription__c = c.WCT_TRSecureDescription__c + '\r\n' + '++++++++++++++++++++++++++++++++++++++++++'+ '\r\n' + ' Original Subject : ' + c.Subject + '\r\n' + ' Original Description : ' + c.Description;
        }
        else if(c.WCT_TRSecureDescription__c == null){
            c.WCT_TRSecureDescription__c = ' Original Subject : ' + c.Subject + '\r\n' + ' Original Description : ' + c.Description;
        }
        if(c.WCT_ReportedBy__c <> null){
            c.WCT_TRSecureDescription__c = c.WCT_TRSecureDescription__c + '\r\n' + 'Original Reported By :' +  reportedByMap.get(c.WCT_ReportedBy__c).Name;
        }
        c.Subject = talentCaseSubject;
        c.Description = talentCaseDescription;
        if(c.WCT_ReportedBy__c <> null)
          c.WCT_TRReportedBy__c = c.WCT_ReportedBy__c;
      }
      
      // if recordtype got changed from 'Case' to 'EmployeeLifecycleEvents_InvoluntarySeparations'
      else if(TriggerOldMap.get(c.Id).RecordTypeId == devNameRecTypeMap.get(Case_RecordType).Id && 
          c.RecordTypeId == devNameRecTypeMap.get(Employee_Lifecycle_Events_Involuntary_Separations).Id){
          // Copying original Subject to another field and encrypting original Subject field
        if(c.WCT_ELESecureDescription__c <> null && !c.WCT_ELESecureDescription__c.contains('Original Subject')){
            c.WCT_ELESecureDescription__c = c.WCT_ELESecureDescription__c + '\r\n' + '++++++++++++++++++++++++++++++++++++++++++'+ '\r\n' + ' Original Subject : ' + c.Subject + '\r\n' + ' Original Description : ' + c.Description;
        }
        else if(c.WCT_ELESecureDescription__c == null){
            c.WCT_ELESecureDescription__c = ' Original Subject : ' + c.Subject + '\r\n' + ' Original Description : ' + c.Description;
        }
        if(c.WCT_ReportedBy__c <> null){
            c.WCT_ELESecureDescription__c = c.WCT_ELESecureDescription__c + '\r\n' + 'Original Reported By :' +  reportedByMap.get(c.WCT_ReportedBy__c).Name;
        }
        c.Subject = seperatedCaseSubject;
        c.Description = seperatedCaseDescription;
        if(c.WCT_ReportedBy__c <> null)
          c.WCT_TRReportedBy__c = c.WCT_ReportedBy__c;
      }
    }
    beforeTrigger  = true;
  }
  
  /****************************************************************************************
     * @author      - Charles Mutsu
     * @date        - 11 Sep, 2013 
     * @description - Send Email to users with 2_CIC_Manager profile using the SMS Email address using the "High Priority Case (Text)" template
     * @param       - trigger.new list is captured in TriggerNew list
     *****************************************************************************************/
  public static void sendSMSEmailToCIC(list<Case> TriggerNew){
    
    Set<Id> CICUserId = new Set<Id>();
    List<String> SMSEmailIds = new List<String>();
    Set<String> ProfileNames = new Set<String>{'2_CIC_Basic', '2_CIC_Manager'};
    Set<Id> CICProfileIds = new Set<Id>();
    
    List<Profile> CICProfiles = [Select Id, Name from Profile where Name IN: ProfileNames];
    Id CICManagerProfileId = [Select Id, Name from Profile where Name = '2_CIC_Manager'].Id;
    for(Profile p:CICProfiles){
        CICProfileIds.add(p.Id);
    }
    
    for(User u: [Select Id, ProfileId, WCT_SMSEmail__c from User where ProfileId IN:CICProfileIds]){
        CICUserId.add(u.Id);
        if(u.ProfileId == CICManagerProfileId && u.WCT_SMSEmail__c != null){
            SMSEmailIds.add(u.WCT_SMSEmail__c); 
        }
    }
    for(Case c:TriggerNew){
        if(c.Priority == '1 - Critical' && c.Status == 'New' && c.TimeCheck__c){
            
        //} && CICUserId.contains(c.OwnerId)){
            c.TimeCheck__c = False;
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
            // Strings to hold the email addresses to which you are sending the email.
            String[] toAddresses = SMSEmailIds;
                        
            // Assign the addresses for the To and CC lists to the mail object.
            mail.setToAddresses(toAddresses);
            
            // Specify the address used when the recipients reply to the email. 
            //mail.setReplyTo('support@acme.com');
            
            // Specify the name used as the display name.
            mail.setSenderDisplayName('CIC Support');
            
            // Specify the subject line for your email address.
            mail.setSubject('Customer Interaction Center Follow Up - Case:' + c.CaseNumber);
            
            // Set to True if you want to BCC yourself on the email.
            mail.setBccSender(false);
            
            // Optionally append the salesforce.com email signature to the email.
            // The email address of the user executing the Apex Code will be used.
            mail.setUseSignature(false);
            
            // Specify the text content of the email.
            mail.setPlainTextBody('Urgent: Immediate Action Required. A high priority case has been created for you in Salesforce. Case Number:' + c.CaseNumber + 'Please respond immediately and no later than 1 hour after receiving this message.');
            
            mail.setHtmlBody('Urgent: Immediate Action Required.<p>'+
                              'A high priority case has been created for you in Salesforce.'+
                              'Case Number:' + c.CaseNumber + 
                              '. Please respond immediately and no later than 1 hour after receiving this message.' );
            
            // Send the email you have created.
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }       
                
  }
    public static void updateCaseStatus_InProgress(set<Id> CaseIds){
        String processRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Process Issue').getRecordTypeId();
        String changeRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change Control Request').getRecordTypeId();
        String preBiRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pre-BI').getRecordTypeId();
        
        List<String> recordTypeIds = new List<String>();
        recordTypeIds.add(processRecTypeId);
        recordTypeIds.add(changeRecTypeId);
        recordTypeIds.add(preBiRecTypeId);
        
        List<Case> updateCaseList = new list<Case>();
        List<Case> caseRecList = [SELECT Id, Status FROM Case WHERE Id IN :CaseIds AND Isclosed = false AND recordtypeId NOT IN :recordTypeIds AND (NOT Status LIKE '%Pending%') AND (Status <> 'In Progress')];
        
        for(Case c : caseRecList){
            c.Status = 'In Progress';                
            updateCaseList.add(c);
        }
        
        if(!updateCaseList.isEmpty()) {
            UPDATE updateCaseList;
        }
    }
      /****************************************************************************************
     * @author      - Sreenivasa Munnangi
     * @date        - 15th Oct 2013
     * @description - Set Due Date to 48 hrs for Learning Delivery Cases when Case Owner
                       Changes to 'Development: Learning Delivery' Q or Case Record Type is Delivery'Development: Learning - Delivery'
     * @param       - trigger.new list is captured in TriggerNew list
     *****************************************************************************************/
    public static void setDueDateForDeliveryCases(List<Case> TriggerNew){
        Datetime startTime = System.now();     
        Id bhId = (Id) System.Label.WCT_Default_Business_Hours_ID;
        Id learningDeliveryQId = (Id) System.Label.WCT_learningDeliveryQueueId;
        Id caseRecordTypeId = (Id) System.Label.WCT_learningDeliveryRecordTypeId;
        
        for(Case c : TriggerNew){
          // If the Case Due Date field is blank at the time of creation populate Due Date with CreatedDate + two days
          if(c.WCT_Due_Date__c == null && (c.OwnerId == learningDeliveryQId || c.RecordTypeId == caseRecordTypeId)){
            c.WCT_Due_Date__c = BusinessHours.add(bhId, startTime, 48 * 60 * 60 * 1000L);
          }
        }   
        
    }
    
    /****************************************************************************************
     * @author      - Rakesh Ala
     * @date        - 7th March 2014
     * @description - Used in Acknowledgement mails functionality
     *****************************************************************************************/
    public static void populateLONOnCase(List<Case> caseList){
        
        List<WCT_List_Of_Names__c> caseLONList = new List<WCT_List_Of_Names__c>();
        Map<string,WCT_List_Of_Names__c> caseIdentifierLONMap = new Map<string,WCT_List_Of_Names__c>();
        List<Case> GetcasesToAck = caseList;
                
        //try{
            caseLONList = [SELECT id,WCT_Case_AcknowledgeMent_Identifiers__c,WCT_Time_to_Acknowledge__c from WCT_List_Of_Names__c where recordTypeId = :WCT_Util.getRecordTypeIdByLabel('WCT_List_Of_Names__c','Non-IEF') AND WCT_Type__c = 'Case Identifier'];
        system.debug('>>>>>>>>>>>>>>>>>caseLONList  >>>>>>>>>'+caseLONList);
       // }catch(exception ex){}
        if(!caseLONList.isEmpty()){
            for(WCT_List_Of_Names__c identifier : caseLONList){
                caseIdentifierLONMap.put(identifier.WCT_Case_AcknowledgeMent_Identifiers__c,identifier);
            }
        }
        
        for(Case c : GetcasesToAck){
            Case tmpCase = new case();
            tmpCase = c;
            if(caseIdentifierLONMap.containsKey(c.RecordTypeid+'_'+c.WCT_Category__c+'_'+c.Priority)){
                tmpCase.WCT_AcknowledgeMent_Hours_for_Case__c = caseIdentifierLONMap.get(c.RecordTypeid+'_'+c.WCT_Category__c+'_'+c.Priority).WCT_Time_to_Acknowledge__c;
                tmpCase.WCT_IsAcknowledgeMent_sent__c = true;
            }
        }
    }
    
        /****************************************************************************************
     * @author      - Vivek Sharma
     * @date        - 28th Feb 2014
     * @description - Populate Reported by field on Case by searching the email obtained from Clicktools form for Process Issue record type
     * @param       - trigger.new list is captured in TriggerNew list
     *****************************************************************************************/
    
    public static void populateContactOnCase(List<Case> TriggerNew){
        String recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Process Issue').getRecordTypeId();
        
        List<Case> reportedCase = new List<Case>();
        List<Case> relCase = new List<Case>();
        List<Case> caseCon = new List<Case>();
        
        Set<String> setReportedCaseid = new set<String>();
        Set<String> setRelatedCaseId = new set<String>();
        Set<String> setRelatedContactId = new set<String>();
        
        Map<id,string> caseToEmailMap = new Map<id,string>();
        Map<id,string> caseToCaseMap = new Map<id,string>();
        Map<id,string> caseToContactMap = new Map<id,string>();
        
        for(Case c: TriggerNew){
            //get case with Reported By(IEF) not null
            if(c.recordTypeId == recTypeId && c.Contact == Null && c.WCT_Reported_By_Email_IEF__c != Null ){
                reportedCase.add(c);
                setReportedCaseid.add( c.WCT_Reported_By_Email_IEF__c);
                caseToEmailMap.put(c.id,c.WCT_Reported_By_Email_IEF__c);
            }
            //get Related Case
            if(c.recordTypeId == recTypeId && c.WCT_Related_Case_IEF__c != Null){
                relCase.add(c);
                setRelatedCaseId.add(c.WCT_Related_Case_IEF__c);
                caseToCaseMap.put(c.Id,c.WCT_Related_Case_IEF__c);
            }
            //get Contact
            if(c.recordTypeId == recTypeId && c.Contact == Null && c.WCT_Contact_Email_IEF__c != Null){
                caseCon.add(c);
                setRelatedContactId.add(c.WCT_Contact_Email_IEF__c);
                caseToContactMap.put(c.Id,c.WCT_Contact_Email_IEF__c);
            }
        }
        //Assumption: contact has unique email id
        List<contact> list_con=[select Id, Name, Email from Contact where Email IN :setReportedCaseid];                
        for (Case c : reportedCase){
            for(contact con: list_con){
                if(caseToEmailMap.get(c.id).equals(String.valueOf(con.Email)))
                    //put Case in Reported By and its email in Reported By Email
                    c.WCT_ReportedBy__c = con.Id;
                    c.WCT_Reported_By_Email_Process_Issue__c = con.Email;
            }                
        } 
        
        List<Case> list_case = [select Id, WCT_RelatedCase__c, CaseNumber from Case where CaseNumber IN : setRelatedCaseId];
        for(Case c: relCase){
            for(case cse: list_case){
                if(caseToCaseMap.get(c.id).equals(String.valueOf(cse.CaseNumber)))
                    c.WCT_RelatedCase__c = cse.Id;
            }
        }
        List<Contact> conList = [select Id, Name, Email from Contact where Email IN : setRelatedContactId];
        for(Case c: caseCon){
            for(contact con: conList){
                if(caseToContactMap.get(c.id).equals(String.valueOf(con.Email)))
                    c.ContactId = con.Id;
            }
        }   
    }
    
    /****************************************************************************************
     * @author      - Vivek Sharma
     * @date        - 5th Mar 2014
     * @description - Populate Case Owner by searching in List Of Names
     * @param       - trigger.new list is captured in TriggerNew list
     *****************************************************************************************/
     
     public static void populateCaseOwner(List<Case> TriggerNew){
         
         String LONRecTypeId = Schema.getGlobalDescribe().get('WCT_List_Of_Names__c').getDescribe().getRecordTypeInfosByName().get('Case Routing').getRecordTypeId();
         String processRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Process Issue').getRecordTypeId();
         String changeRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change Control Request').getRecordTypeId();
         
         List<WCT_List_Of_Names__c> listLON = new List<WCT_List_Of_Names__c> ([select id, name, WCT_Case_Owner__c, WCT_Case_Owner_is_Queue__c, WCT_Case_Record_Type__c, WCT_Category__c, WCT_Country__c from WCT_List_Of_Names__c where recordTypeId = :LONRecTypeId]);
         List<Case> lstCase = new List<Case>();
         List<Case> lstQueueOwnedCase = new List<Case>();
         
         Set<String> caseOwnerSet = new Set<String>();
         Set<String> caseQueueOwnedSet = new Set<String>();
         
         Map<Id,String> caseOwnerMap = new Map<Id,String>();
         
         for(Case c: TriggerNew){
             if((c.recordTypeId == processRecTypeId || c.recordTypeId == changeRecTypeId) && c.WCT_Category__c != Null && c.WCT_Country_IEF__c !=Null){
                 
                 for(WCT_List_Of_Names__c LON:listLON){
                     //check if case owner is not queue in list of names                     
                     //try{
                     if(!LON.WCT_Case_Owner_is_Queue__c){
                         if(LON.WCT_Case_Owner__c != Null && LON.WCT_Category__c != Null && LON.WCT_Country__c != Null && LON.WCT_Category__c == c.WCT_Category__c && LON.WCT_Country__c == c.WCT_Country_IEF__c){
                             system.debug('In first If loop');
                             If((c.recordTypeId == processRecTypeId && LON.WCT_Case_Record_Type__c == 'Process Issue') || (c.recordTypeId == changeRecTypeId && LON.WCT_Case_Record_Type__c == 'Change Control')){                                 
                                 caseOwnerMap.put(c.Id,LON.WCT_Case_Owner__c);                                 
                                 caseOwnerSet.add(LON.WCT_Case_Owner__c);
                                 lstCase.add(c);
                             }    
                         }
                     }
                     else{
                         if(LON.WCT_Case_Owner__c != Null && LON.WCT_Category__c != Null && LON.WCT_Country__c != Null && LON.WCT_Category__c == c.WCT_Category__c && LON.WCT_Country__c == c.WCT_Country_IEF__c){
                             If((c.recordTypeId == processRecTypeId && LON.WCT_Case_Record_Type__c == 'Process Issue') || (c.recordTypeId == changeRecTypeId && LON.WCT_Case_Record_Type__c == 'Change Control')){
                                 caseOwnerMap.put(c.Id,LON.WCT_Case_Owner__c);                                 
                                 caseQueueOwnedSet.add(LON.WCT_Case_Owner__c);
                                 lstQueueOwnedCase.add(c);
                             }
                         }
                     }
                     //}catch(exception e){}
                 }                
             }
         }
         
         List<User> caseOwners = new List<User>([select id, name from User where name =: caseOwnerSet]);
         List<Group> caseQueues = new List<Group>([select Id, Name from Group where type = 'Queue' and Name =: caseQueueOwnedSet]);
         system.debug('>>> caseOwners >>'+caseOwners);
         system.debug('>>> caseQueues >>'+caseQueues);
         for(Case c: lstCase){
             for(User u: caseOwners){
                 if(caseOwnerMap.get(c.Id).equals(u.Name)){
                     c.OwnerId = u.Id;
                 }
             }
         }
         for(Case c: lstQueueOwnedCase){
             for(Group g: caseQueues){
                 if(caseOwnerMap.get(c.Id).equals(g.Name)){
                     c.OwnerId = g.Id;
                 }
             }
         }
     }
    /****************************************************************************************
     * @author      - Jeetesh Bahuguna
     * @date        - 18 ,Mar, 2014 
     * @description - This method is called from CaseTrigger in after insert operation to attach an article with case.. 
     * @param       - trigger.new list is captured in TriggerNew list.
     * @author      - Charles Mutsu
     * @date        - 06 Sep, 2013
     * @description - Added Business Hours to calculate Due Date        
     *****************************************************************************************/
    public static void AtatchArticleOnCase(list<Case> TriggerNew)
    {
        if(Label.SOP_article_id!='null'){
         String processRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Process Issue').getRecordTypeId();
         String changeRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change Control Request').getRecordTypeId();
         List<CaseArticle> list_caseArticle= new list<CaseArticle>();
         
          for(Case c: TriggerNew)
          {
            if(c.recordTypeId == processRecTypeId || c.recordTypeId == changeRecTypeId) 
                {
                     CaseArticle cA= new CaseArticle();
                     cA.KnowledgeArticleId=Label.SOP_article_id;
                     cA.CaseId= c.id;
                     list_caseArticle.add(cA);
                }
           }
        insert list_caseArticle;
        }
    }
     /*
     */
     public void CaseOwnerChanged(List<case> caseLst, Map<Id , case> caseOldMap){ 
        for(case caseRec : caseLst){
            if(caseRec.OwnerId != caseOldMap.get(caseRec.id).OwnerId){
                caseRec.OwnerChangeWrg__c = true;
                caseRec.Trigger_Time_01__c = system.now()-0.04145;
            }
            else{
                caseRec.OwnerChangeWrg__c = false;
            }
        }
     } 
    
    public void ELEprograms(List<case> lstnewcases){
    
    list<case> cas = new list<case>();
    for(case c : lstnewcases){
        if(c.ContactID <> null){
         cas.add(c);
        }
    }
    
    //string sample;
    //list<>    
        for(case casrec : cas){
        
            if(casrec.Origin == 'US ELE Programs' && casrec.SuppliedEmail <> null && casrec.SuppliedEmail <> '' && casrec.Status == 'New'){
               // sample = right(case.SuppliedEmail,13);
             //  sample = case.SuppliedEmail;
               list<string> s = casrec.SuppliedEmail.split('\\@');
               system.debug(s[1]);
               if(s[1] == 'seyfarth.com'){
                   casrec.ContactId = s[1];
             }
              else if (s[1] == 'Frogomen') {
                  casrec.ContactId = s[1];
             }     
          }
       }
    
    }
     public static void runAssignmentRule(Map<Id, Case> oldcaseMap, List<Case> newCaseList) {
         /* system.debug('oldcaseMap'+oldcaseMap);
          system.debug('newCaseList'+newCaseList);
          AssignmentRule rule = [SELECT id, Name FROM AssignmentRule WHERE SobjectType = 'Case' AND Active = true AND Name='Rules Engine' limit 1];
          system.debug('rule'+rule);
          List<Id> caseIds = new List<Id>();
          List<Case> updateCasesList = new List<Case>();
          system.debug('System.label.TR_Queue_ID'+System.label.TR_Queue_ID);
          system.debug('(Id) System.label.TR_Queue_ID'+(Id) System.label.TR_Queue_ID);
          Id trQueueId = (Id) System.label.TR_Queue_ID;
          system.debug('trQueueId'+trQueueId);
          for(Case c:newCaseList) {
              caseIds.add(c.id);
          }
          system.debug('caseIds'+caseIds);          
          for (Case c:[SELECT id, ownerid FROM Case WHERE id IN: caseIds]) {
              system.debug('c.ownerId'+c.ownerId);
              system.debug('oldcaseMap.get(c.id).ownerid'+oldcaseMap.get(c.id).ownerid);
              system.debug('oldcaseMap.get(c.id)'+oldcaseMap.get(c.id));
            if((c.ownerId != oldcaseMap.get(c.id).ownerid) && c.ownerId == trQueueId) {
                Database.DMLOptions dmo = new Database.DMLOptions();
                 system.debug('dmo'+dmo);
                 system.debug('rule.id'+rule.id);
                dmo.assignmentRuleHeader.assignmentRuleId= rule.id;
                dmo.emailHeader.triggerUserEmail = true;
                system.debug('dmo1'+dmo);
                c.setOptions(dmo);
                system.debug('c'+c);
                updateCasesList.add(c);
            }      
          }
        system.debug('updateCasesList'+updateCasesList);
        if(!updateCasesList.isEmpty()) {
            update updateCasesList;
        } */
     }       
}