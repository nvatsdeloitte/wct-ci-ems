/*****************************************************************************************
    Name    : Case_Utility_Test
    Desc    : Test class for Case_Utility class                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Rahul Agarwal                 09/10/2013         Created 
Rahul Agarwal                 09/11/2013         Modified 
******************************************************************************************/
@isTest
public class Case_Utility_Test{
  /****************************************************************************************
    * @author      - Rahul Agarwal
    * @date        - 09/10/2013
    * @description - Test Method for both the methods of Case Utility Class
    *****************************************************************************************/
    public static testmethod void CaseUtility(){
      list<Case> caseList = new list<Case>();
      map<String, Schema.SObjectField> fldObjMap = new map<String, Schema.SObjectField>();
      fldObjmap = schema.SObjectType.Case.fields.getmap();
      list<Schema.SObjectField> fldObjmapValues = fldObjmap.values();
        map<String, RecordType> devNameRecTypeMap = new map<String, RecordType>();
        // Querying all the Case Record Types
        devNameRecTypeMap = Case_Utility.devNameRecType('Case');
      Test_Data_Utility.createCase();
      // Creating dynamic query for case
        String theQuery = 'SELECT ';
        for(Schema.SObjectField s : fldObjmapValues){
           String theName = s.getDescribe().getName();
           theQuery += theName + ',';
        }
        theQuery = theQuery.subString(0, theQuery.length() - 1);
        theQuery += ' FROM Case';
        caseList = Database.query(theQuery);
      set<Id> CaseIds = new set<Id>();
      for(Case c : caseList)
        CaseIds.add(c.Id);
      test.startTest();
      Case_Utility.updateCaseStatus_InProgress(CaseIds);
      Case_Utility.beforeTrigger = false;
      // passing the created case to updateCaseSubjectAndDes method of case utility class
      Case_Utility.updateCaseSubjectAndDes(caseList);   
      Case_Utility.beforeTrigger = false;
      // Changing the record type of a case record from 'Case' to 'TalentRelations_Employee_Matter'
      for(Case c : caseList){
        if(c.RecordTypeId == devNameRecTypeMap.get('Case').Id)
          c.RecordTypeId = devNameRecTypeMap.get('TalentRelations_Employee_Matter').Id;
      }
      update caseList;
      Case_Utility.beforeTrigger = false;
      // Changing the record type of a case record from 'TalentRelations_Employee_Matter' to 'Case'
      for(Case c : caseList){
        if(c.RecordTypeId == devNameRecTypeMap.get('TalentRelations_Employee_Matter').Id)
          c.RecordTypeId = devNameRecTypeMap.get('Case').Id;
      }
      update caseList;
      Case_Utility.beforeTrigger = false;
      // Changing the record type of a case record from 'Case' to 'EmployeeLifecycleEvents_InvoluntarySeparations'
      for(Case c : caseList){
        if(c.RecordTypeId == devNameRecTypeMap.get('Case').Id)
          c.RecordTypeId = devNameRecTypeMap.get('EmployeeLifecycleEvents_InvoluntarySeparations').Id;
      }
      update caseList;
      test.stopTest();   
    }
    public static testmethod void addingEvent(){
      list<Case> caseList = new list<Case>();
      Test_Data_Utility.createCase();
      caseList = [select Id from Case limit 1];
      Event e = new Event(WhatId = caseList[0].Id, DurationInMinutes = 10, ActivityDateTime = system.today());
      insert e;
      
    }
    public static testmethod void addingTask(){
      list<Case> caseList = new list<Case>();
      Test_Data_Utility.createCase();
      caseList = [select Id from Case limit 1];
      Task t = new Task(WhatId = caseList[0].Id, Subject = 'Test');
      insert t;
    }
    public static testmethod void addingNote(){
      list<Case> caseList = new list<Case>();
      Test_Data_Utility.createCase();
      caseList = [select Id from Case limit 1];
      WCT_Notes__c n = new WCT_Notes__c(Case__c = caseList[0].Id);
      insert n;
    }
    public static testmethod void addingInterestedParties(){
      list<Case> caseList = new list<Case>();
      Test_Data_Utility.createCase();
      caseList = [select Id from Case limit 1];
      Interested_Party__c i = new Interested_Party__c(WCT_Case__c = caseList[0].Id);
      insert i;
    }
    public static testmethod void addingAckHours(){
      string caseRecTypeId= Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case').getRecordTypeId();
      string LONRtypeId =  WCT_Util.getRecordTypeIdByLabel('WCT_List_Of_Names__c','Non-IEF');
      WCT_List_Of_Names__c LON = WCT_UtilTestDataCreation.createListOfNames('Test',LONRtypeId,'Case Identifier');
      LON.WCT_Time_to_Acknowledge__c = '8 hours';
      LON.WCT_Case_AcknowledgeMent_Identifiers__c = caseRecTypeId+'_ELE: Transfers_1 - Critical';
      Insert LON;
      list<Case> caseList = new list<Case>();
      list<Case> UpdatecaseList = new list<Case>();
      Test_Data_Utility.createCase();
      caseList = [select Id,WCT_Category__c,Priority,WCT_IsAcknowledgeMent_sent__c from Case];
      for(case c : caseList){
        Case ca = new case();
        ca = c;
        ca.WCT_Category__c = 'ELE: Transfers';
        UpdatecaseList.add(ca);
      }
      update UpdatecaseList;      
      
    }
}