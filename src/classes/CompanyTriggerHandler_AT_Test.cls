@isTest
public class CompanyTriggerHandler_AT_Test
{
public static testmethod void CompanyTriggerHandler_AT ()
{
 Contact c = new Contact();
 c.firstname='jkxcver434';
 c.lastname='kljklsdrererrer';
 c.Email='abc@gmnailxdferer.com';
 insert c;
 
 Company__c comp = new Company__c();
 comp.Name='abc';
 comp.DUNS_Number__c='120459709';
 comp.MDR_Parent_Company__c='xyz';
 insert comp;
 
 List<Company__c> company = new List<Company__c>();
 company.add(comp);
 
 CompanyTriggerHandler_AT compTrigger = new CompanyTriggerHandler_AT();
 // CompanyTriggerHandler_AT.CompanyNameList();
 compTrigger.assignCompanyName(company);

}

}