/*****************************************************************************************
    Name    : ContactOtherEditController_Test
    Desc    : Test class for ContactOtherEditController class                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Rahul Agarwal                 09/11/2013         Created
******************************************************************************************/
@isTest
public class ContactOtherEditController_Test{
    /****************************************************************************************
    * @author      - Rahul Agarwal
    * @date        - 09/11/2013
    * @description - 
    *****************************************************************************************/
    public static testmethod void ContactOtherEditController(){
        // creating contacts
        Test_Data_Utility.createContact();
        //querying one contact
        Contact c = [Select Id from Contact limit 1];
        //setting id and phone parameters
        System.currentPageReference().getParameters().put('id',c.Id);
        System.currentPageReference().getParameters().put('phone','1111111111');
        test.startTest();
        // instantiating the controller class
        ContactOtherEditController con = new ContactOtherEditController();
        con.updateContact();
        test.stopTest();
    }
}