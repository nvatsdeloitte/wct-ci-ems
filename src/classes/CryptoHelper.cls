public class CryptoHelper {

    /*
        base64Encode --> Blob to String
        base64Decode --> String to Blob
    */

    public static String encrypt(String tobeEncryptedString){
        Blob secretKey = EncodingUtil.base64Decode(System.Label.WCT_TOD_Email_Encryption_Key);
        Blob iv = EncodingUtil.base64Decode(System.Label.WCT_TOD_Email_Encryption_IV);
        Blob stringToEncrypt = Blob.valueOf(toBeEncryptedString);
        Blob encryption = Crypto.encrypt('AES256', secretkey, iv, stringToEncrypt);
        return(EncodingUtil.urlEncode(EncodingUtil.base64Encode(encryption), 'UTF-8'));
    }

    public static String decrypt(String encryptedString){
        Blob secretKey = EncodingUtil.base64Decode(System.Label.WCT_TOD_Email_Encryption_Key);
        Blob iv = EncodingUtil.base64Decode(System.Label.WCT_TOD_Email_Encryption_IV);
        Blob stringToBlob = EncodingUtil.base64Decode(encryptedString);
        Blob decryption = Crypto.decrypt('AES256', secretkey, iv, stringToBlob);

        return decryption.toString();
    }
    
    /** 
     * This method is used only for the EncryptionHelperController class. This method first urlDecodes the
     * String and then decrypts it.
     */
    public static String decryptURLEncoded(String encryptedString){
        return decrypt(EncodingUtil.urlDecode(encryptedString, 'UTF-8'));
    }
}