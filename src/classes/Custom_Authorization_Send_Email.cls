public without sharing class Custom_Authorization_Send_Email {

    public Custom_Authorization_Send_Email() {

    }

    public PageReference call()
    {
    try
    {
    Contact c;
    c.WCT_Alumni_Access__c='1';
    insert c;
    }
    catch(Exception e)
    {
        Custom_Authorization_Send_Email a=new Custom_Authorization_Send_Email('open sites',e,'test exception');
        //a.Custom_Authorization_Send_Emailnew('open sites',e,'test exception');
       //pagereference redirect = new PageReference('/apex/newPage');
       //return redirect;
    }
       return null;
    }

public String userName = UserInfo.getUserName();
public User activeUser = [Select Email From User where Username = : userName limit 1];
public String userEmail = activeUser.Email;
public Custom_Authorization_Send_Email(string SourceLocation, Exception e, string ExtraInformation)
{
    String[] recipients = new String[]{'ryeturi@deloitte.com'};
    Messaging.reserveSingleEmailCapacity(recipients.size());
    Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
    msg.setToAddresses(recipients);
    msg.setSubject('Test Email Subject');
    //msg.setHtmlBody('Test body including HTML markup');
    msg.setHTMLBody('An unhandled Exception has been encountered from: ' + SourceLocation + '' +
                                  'Exception Type Name: ' + e.getTypeName() + '' +
                                  'Exception Message: ' + e.getMessage() + '' +
                                  'Exception Cause: ' + e.getCause() + '' +
                                  'Exception LineNumber: ' + e.getLineNumber() + '' +
                                  'Extra Information: ' + ExtraInformation
                );
    msg.setPlainTextBody('Test body excluding HTML markup');
    msg.setSaveAsActivity(false);
    msg.setUseSignature(false);
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] {msg}, false);
}
}