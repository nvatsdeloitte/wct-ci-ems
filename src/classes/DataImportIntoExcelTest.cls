/**
Class Name: DataImportIntoExcelTest
Created By: Balu Devarapu(Deloitte India)
Started: 04/07/2015

Description: Test class for DataImportIntoExcel and DataWizardBatch Classes.              
**/

@IsTest
private class DataImportIntoExcelTest{
/**
Test Method : testMethod1
**/

  private static testmethod void testMethod1(){
  
  Test.startTest();
    List<Account> lstAcc=new List<Account>();
  Account objAccount=new Account(Name='TestAccount');
  insert objAccount;
  for(integer i=0; i<100; i++){
   Account objAc=new Account(Name='Test'+string.ValueOf(i),ParentId=objAccount.Id);
   lstAcc.add(objAc);
  }
  insert lstAcc;
  
  DataImportIntoExcel objDI=new DataImportIntoExcel();
  objDI.preinit();
  objDI.bindSobjects();
  objDI.getSObjects();
  objDI.ObjectRequestType();
  objDI.Showfields();
  objDI.RefFields();
  objDI.finalString();
  objDI.ParentRecords();
  objDI.Check();
  objDI.Export();
  objDI.AddRemoveQueryFields();
  objDI.MoveUp();
  objDI.MoveDown();
  objDI.Order();
  objDI.Filters();
  objDI.getOperatorOptions();
  string TestStr='test';
  System.assert(TestStr!=null);
  Test.stopTest();
  }

/**
Test Method : testMethod2
            
**/
  private static testmethod void testMethod2(){
  
  Test.startTest();
    List<Account> lstAcc=new List<Account>();
  Account objAccount=new Account(Name='TestAccount');
  insert objAccount;
  for(integer i=0; i<100; i++){
   Account objAc=new Account(Name='Test'+string.ValueOf(i),ParentId=objAccount.Id);
   lstAcc.add(objAc);
  }
  insert lstAcc;
  
  Account objAcc=new Account(Name='test');
  insert objAcc;
  PageReference pRef = Page.WCT_DataImportIntoExcel;
  pRef.getParameters().put('sObjectAPI', 'Account');
  pRef.getParameters().put('ReqStatus', 'true');
  pRef.getParameters().put('selectedObject', 'Account');
  pRef.getParameters().put('selectedRefField', 'Name');
  pRef.getParameters().put('ParentRefRecordsDownload', 'true');
  pRef.getParameters().put('QueryField', 'Name');
  pRef.getParameters().put('FieldReq', 'true');
  pRef.getParameters().put('SelectedOrderItem', 'Name');
  Test.setCurrentPage(pRef); 
  DataImportIntoExcel objAS2=new DataImportIntoExcel();
  objAS2.QueryText='select Id,Name from Account where name!=null';
  objAS2.selectedObject='Account';
  objAS2.MapOrder.put('Name','Name');
  objAS2.ObjectRequestType();
  objAS2.Showfields();
  objAS2.RefFields();
  objAS2.ParentRecords();
  objAS2.AddRemoveQueryFields();
  objAS2.Export();
 
  pRef.getParameters().put('selectedRefField', 'Account');
  objAS2.RefFields();
  
  pRef.getParameters().put('selectedRefField', 'OwnerId');
  objAS2.RefFields();
  
  objAS2.bindSobjects();
  objAS2.getSObjects();
  objAS2.ObjectRequestType();
  objAS2.selectedSubRefField='Email';
  objAS2.finalString();
  objAS2.ParentRecords();
  
  objAS2.Check();
  objAS2.Export();

  objAS2.MoveUp();
  objAS2.MoveDown();
  objAS2.Order();
  objAS2.Filters();
  objAS2.getOperatorOptions();
  objAS2.go();
  pRef.getParameters().put('FieldReq', 'false');
  Test.setCurrentPage(pRef); 
  objAS2=new DataImportIntoExcel();
  objAS2.AddRemoveQueryFields();
  
  objAS2.colName1='Name';
  objAS2.iVal1='del';
  objAS2.operator1='contains';
  objAS2.convertData('Name', 'del');
  objAS2.Check();
  objAS2.Export();
  objAS2.CallBatchApex();
  string TestStr='test';
  System.assert(TestStr!=null);
  Test.stopTest();
  
  }
  
  /**
Test Method : testMethod3
            
**/
  private static testmethod void testMethod3(){
  
  Test.startTest();
  List<Account> lstAcc=new List<Account>();
  Account objAccount=new Account(Name='TestAccount');
  insert objAccount;
  for(integer i=0; i<100; i++){
   Account objAc=new Account(Name='Test'+string.ValueOf(i),ParentId=objAccount.Id);
   lstAcc.add(objAc);
  }
  insert lstAcc;
  
  
  
  
  BatchDataImport objBDI=new BatchDataImport();
    objBDI.QueryText='Select Id,Name,Type,Parent.Id from Account';
    objBDI.Conditions=' where Name!=null ';
    objBDI.selectedObject='Account';
    objBDI.IsContainParentRecords=false;

    
    
    set<string> lstQueryFieldsTemp=new set<String>();
    lstQueryFieldsTemp.add('Id');
    lstQueryFieldsTemp.add('Name');
    lstQueryFieldsTemp.add('Type');
    lstQueryFieldsTemp.add('Parent.Id');
    objBDI.lstQueryFields=lstQueryFieldsTemp;
    objBDI.ParentRefAPIName='Parent.Id';  
    objBDI.ParentQuery='Select Id,Name,Type,Parent.Id from Account'; 
    ID batchprocessid = Database.executeBatch(objBDI,2000); // execute batch 

    objBDI.IsContainParentRecords=true; 
    ID batchprocessid2 = Database.executeBatch(objBDI,2000); // execute batch   
    string TestStr='test';
    System.assert(TestStr!=null);
    Test.stopTest();
  }
}