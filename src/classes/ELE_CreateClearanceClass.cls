/**************************************************************************************
Apex class       :  ELE_CreateClearanceClass
Version          : 1.0 
Created Date     : 25 April 2015
Function         : To create primary stakeholders and secondary stakeholder clearances when the button is clicked in ele separation case
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                   25/04/2015            Original Version
*************************************************************************************/

global class ELE_CreateClearanceClass {
    
    
    webservice static void ELE_CreateClearance(String sELEid,String sCaseType, String sFSS, String sRegion, String sStakeholderType)
    {
      list<Clearance_Master_Separation__c>  lst_ClearanceMaster = [Select ELE_Stakeholder_Designation__c,ELE_Additional_Emails__c,ELE_Serial_Number__c, ELE_SLA_Criteria__c,
                                                                    ELE_Reminder3_Template_Id__c,  ELE_Reminder2_Template_ID__c,
                                                                    ELE_Reminder1_Template_ID__c, ELE_Region__c,  ELE_Notification_Template_Id__c,
                                                                   ELE_Notification_Required__c, Name, ELE_Location__c,
                                                                    IsDeleted,Id,ELE_FSS__c, ELE_Dependent_Clearance_No__c,
                                                                   CreatedDate, CreatedById,ELE_Clearance_authority__c,ELE_Clearance_Type__c, ELE_Clearance_SLA__c,
                                                                   ELE_Clearance_Authority_Type__c,ELE_Team_mailbox__c,
                                                                   ELE_Reminder_1_SLA__c,ELE_Reminder_2_SLA__c,ELE_Reminder_3_SLA__c
                                                                   From Clearance_Master_Separation__c 
                                                                       where ELE_Clearance_Type__c =:sCaseType AND ELE_fss__c =: sFSS AND ELE_region__c =: sRegion 
                                                                       AND ELE_Clearance_Authority_Type__c= : sStakeholderType AND  ELE_Record_Status_Active__c = true];
      
      List<Clearance_separation__c> lstClearanceToCreate= new list<Clearance_separation__c>();
        for(Clearance_Master_Separation__c lp_MasterList: lst_ClearanceMaster )
        {
                 Clearance_separation__c cNewClearance = new Clearance_separation__c();
                cNewClearance.ELE_Clearance_authority__c = lp_MasterList.ELE_Clearance_authority__c;
                cNewClearance.ELE_Clearance_Authority_Type__c = lp_MasterList.ELE_Clearance_Authority_Type__c;
                cNewClearance.ELE_Clearance_SLA__c = lp_MasterList.ELE_Clearance_SLA__c;
                cNewClearance.ELE_Clearance_Type__c = lp_MasterList.ELE_Clearance_Type__c;
                cNewClearance.ELE_Dependent_Clearance_No__c = lp_MasterList.ELE_Dependent_Clearance_No__c;
                cNewClearance.ELE_FSS__c = lp_MasterList.ELE_FSS__c;
                //cNewClearance.ELE_Location_Separation__c = lp_MasterList.ELE_Location__c;
                cNewClearance.ELE_Notification_Required__c = lp_MasterList.ELE_Notification_Required__c;
                cNewClearance.ELE_Notification_Template_Id__c = lp_MasterList.ELE_Notification_Template_Id__c;
                cNewClearance.ELE_Region__c = lp_MasterList.ELE_Region__c;
                cNewClearance.ELE_Reminder_1_Criteria__c = lp_MasterList.ELE_Reminder1_Template_ID__c;
                cNewClearance.ELE_Reminder_1_SLA__c = lp_MasterList.ELE_Reminder_1_SLA__c;
                cNewClearance.ELE_Reminder_2_Criteria__c = lp_MasterList.ELE_Reminder2_Template_ID__c;
                cNewClearance.ELE_Reminder_2_SLA__c = lp_MasterList.ELE_Reminder_2_SLA__c;
                cNewClearance.ELE_Reminder_3_Criteria__c = lp_MasterList.ELE_Reminder3_Template_Id__c;
                cNewClearance.ELE_Reminder_3_SLA__c = lp_MasterList.ELE_Reminder_3_SLA__c;
                cNewClearance.ELE_Stakeholder_Designation__c = lp_MasterList.ELE_Stakeholder_Designation__c;
                cNewClearance.ELE_Serial_Number__c = lp_MasterList.ELE_Serial_Number__c;
                cNewClearance.ELE_SLA_Criteria__c = lp_MasterList.ELE_SLA_Criteria__c;
                cNewClearance.ELE_Separation__c = sELEid;
                cNewClearance.ELE_Additional_Fields__c = lp_MasterList.ELE_Additional_Emails__c;
                cNewClearance.ELE_Team_mailbox__c= lp_MasterList.ELE_Team_mailbox__c;
                cNewClearance.ELE_Master_clearance_ID__c=lp_MasterList.id;
                lstClearanceToCreate.add(cNewClearance);
        
        }  
        ELE_Separation__c eleobjToupdate= [select id,ELE_Clearance_Records_Created__c,ELE_Create_Secondary_Clearance_Records__c from  ELE_Separation__c where id=: sELEid limit 1];
        
        try{
            insert lstClearanceToCreate;
        }catch(Exception e)
        {
         
        }
        
        if(sStakeholderType=='Primary Stakeholder')
        {
             eleobjToupdate.ELE_Clearance_Records_Created__c=true;
           
        }else 
        {
          eleobjToupdate.ELE_Create_Secondary_Clearance_Records__c=true;
        }
        try
        {
           update eleobjToupdate; 
        }catch(Exception es)
        {
                 
        }
        
    }
    /** 
        Method Name  : createAsset
        Return Type  : void
        Description  : to create assets based on asset master records
       */
    global static void createAsset(list<Clearance_separation__c> newListToupdate)// String sTeamName, string sClearanceID
    {
        
        set<string> set_teamName= new set<String>();
        for(Clearance_separation__c lp_cls :newListToupdate)
        {
            set_teamName.add(lp_cls.ELE_Stakeholder_Designation__c);
        }
       List<Asset__c> lst_asset_toCreate =new list<Asset__c>();
        
        for(Asset_Master__c lp_assetMaster : [select id, ELE_Asset_status_values__c, Ele_Asset_Name__c, Ele_Team_Name__c  from Asset_Master__c where Ele_Team_Name__c IN:set_teamName])
        {
            for(Clearance_separation__c lp_cls :newListToupdate)
            {
                if(lp_assetMaster.Ele_Team_Name__c==lp_cls.ELE_Stakeholder_Designation__c){
                    Asset__c newAsset= new Asset__c();
                    newAsset.ELE_Asset_Name__c=lp_assetMaster.Ele_Asset_Name__c; 
                    newAsset.ELE_Team_Name__c = lp_assetMaster.Ele_Team_Name__c;
                    newAsset.ELE_Asset_status_values__c = lp_assetMaster.Ele_Asset_status_values__c;
                    newAsset.ELE_Clearance__c= lp_cls.id;
                    lst_asset_toCreate.add(newAsset);
                }
            }
        }
        
        try
        {
           insert lst_asset_toCreate;
        }catch(Exception e)
        {
        }
    }
    
    
    
     /** 
        Method Name  : CreateExitInterview
        Return Type  : void
        Description  : to create Create Exit Interview records
       */
    global static void CreateExitInterview(list<ELE_Separation__c> lst_ELEToWork)
    {
        List<Exit_Survey__c> list_exitTocreate= new list<Exit_Survey__c>();
        for(ELE_Separation__c lp_ELE:lst_ELEToWork )
        {
            Exit_Survey__c cExitObj= new Exit_Survey__c();
            cExitObj.ELE_Separation_Case__c=lp_ELE.id;
            cExitObj.Employee__c=lp_ELE.ELE_Contact__r.id;
            //cExitObj.RecordTypeId=label.ELE_Exit_SurvayID;
            list_exitTocreate.add(cExitObj);
         }
        
        insert list_exitTocreate;
    }
    /** 
        Method Name  : updateAddress
        Return Type  : void
        Description  : to update the address
       */
    global static void updateAddress(list<ELE_Separation__c> list_ELEToupdate)
    {
        set<String> set_location= new set<string>(); 
        For(ELE_Separation__c lp_ele:list_ELEToupdate)
        {
            set_location.add(lp_ELE.ELE_Location__c);
        }
        List<WCT_List_Of_Names__c> lst_LON= [select id, ELE_Office_address__c,ELE_Location__c from WCT_List_Of_Names__c where WCT_Type__c=:'Separation' and ELE_Location__c in:set_location];
        For(ELE_Separation__c lp_ele:list_ELEToupdate)
        {
            
            for(WCT_List_Of_Names__c lp_lon : lst_LON)
            {
            
                if(lp_lon.ELE_Location__c==lp_ele.ELE_Location__c)
                {
                    lp_ele.ELE_Office_address__c=lp_lon.ELE_Office_address__c;
                }
            }
        }
    }
    /** 
        Method Name  : updateReminderDate
        Return Type  : void
        Description  : to update the remainder date
       */
    global static void updateReminderDate(list<Clearance_separation__c> list_ClearanceToupdate)
    {
        for(Clearance_separation__c cClearanceObjToupdate : list_ClearanceToupdate)
        {  
           try{ 
               
                if(cClearanceObjToupdate.ELE_Last_Working_Day__c!=null && cClearanceObjToupdate.ELE_DOR__c!=null){
                                        // Reminder 1 calculation..
                   
                                        if(cClearanceObjToupdate.ELE_Reminder_1_Criteria__c!=null){            
                                            if(cClearanceObjToupdate.ELE_Reminder_1_Criteria__c=='LWD'){
                                                Integer cNoticePeriod= (cClearanceObjToupdate.ELE_DOR__c).daysBetween(cClearanceObjToupdate.ELE_Last_Working_Day__c);
                                                Integer cDayForReminder= cNoticePeriod+ (Integer)cClearanceObjToupdate.ELE_Reminder_1_SLA__c;
                                                cClearanceObjToupdate.ELE_Reminder1_Time__c =cClearanceObjToupdate.ELE_DOR__c.addDays(cDayForReminder);
                                                }
                                            else if(cClearanceObjToupdate.ELE_Reminder_1_Criteria__c=='DOR')
                                            {
                                              cClearanceObjToupdate.ELE_Reminder1_Time__c =cClearanceObjToupdate.ELE_DOR__c.addDays((Integer)cClearanceObjToupdate.ELE_Reminder_1_SLA__c);  
                                            }
                                        }
                                         // Reminder 2 calculation..
                                        if(cClearanceObjToupdate.ELE_Reminder_2_Criteria__c!=null){
                                            if(cClearanceObjToupdate.ELE_Reminder_2_Criteria__c=='LWD'){
                                                Integer cNoticePeriod= (cClearanceObjToupdate.ELE_DOR__c).daysBetween(cClearanceObjToupdate.ELE_Last_Working_Day__c);
                                                Integer cDayForReminder= cNoticePeriod+ (Integer)cClearanceObjToupdate.ELE_Reminder_2_SLA__c;
                                                cClearanceObjToupdate.ELE_Reminder2_Time__c =cClearanceObjToupdate.ELE_DOR__c.addDays(cDayForReminder);
                                                }
                                            else if(cClearanceObjToupdate.ELE_Reminder_2_Criteria__c=='DOR')
                                            {
                                              cClearanceObjToupdate.ELE_Reminder2_Time__c =cClearanceObjToupdate.ELE_DOR__c.addDays((Integer)cClearanceObjToupdate.ELE_Reminder_2_SLA__c);  
                                            }
                                        }
                                        
                                        
                                        // Reminder 3 calculation..
                                        if(cClearanceObjToupdate.ELE_Reminder_3_Criteria__c!=null){
                                            if(cClearanceObjToupdate.ELE_Reminder_3_Criteria__c=='LWD'){
                                                Integer cNoticePeriod= (cClearanceObjToupdate.ELE_DOR__c).daysBetween(cClearanceObjToupdate.ELE_Last_Working_Day__c);
                                                Integer cDayForReminder= cNoticePeriod+ (Integer)cClearanceObjToupdate.ELE_Reminder_3_SLA__c;
                                                cClearanceObjToupdate.ELE_Reminder3_Time__c =cClearanceObjToupdate.ELE_DOR__c.addDays(cDayForReminder);
                                                }
                                            else if(cClearanceObjToupdate.ELE_Reminder_3_Criteria__c=='DOR')
                                            {
                                              cClearanceObjToupdate.ELE_Reminder3_Time__c =cClearanceObjToupdate.ELE_DOR__c.addDays((Integer)cClearanceObjToupdate.ELE_Reminder_3_SLA__c);  
                                            }
                                        }
                                        }                
           }catch(Exception e)
           {
           }
           } 
    }  
     
    /** 
        Method Name  : SendReminderEmails
        Return Type  : void
        Description  : to send emails to primary stakeholder
       */
    global static void SendReminderEmails(map<string, Map<string, integer>> map_reminder_valueTosend){
    System.debug('>>>>>>>>>>>>>>>indise sendreminders>>>>>>>>>>>>>map_reminder_valueTosend>>>'+map_reminder_valueTosend);
      List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>(); 
        
        map<string, ELE_Stakeholder_mailbox__c >  map_Teammailbox= ELE_Stakeholder_mailbox__c.getAll();
      
        for(String Reminder_type: map_reminder_valueTosend.keySet())
        {
             map<string, Integer> map_tempReminders= new map<string, Integer>();
            boolean isEmailToSend = true;
            map_tempReminders.putAll(map_reminder_valueTosend.get(Reminder_type));
             System.debug('>>>>>>>>>>>>>>>indise Reminder_type>>map_tempReminders>>>>>>>>>'+map_tempReminders);
   
           
            for(String TeamName:map_tempReminders.keySet())
            {
                
                            Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
                    
                            // Step 2: Set list of people who should get the email
                            List<String> sendTo = new List<String>();

                            // Fetch team mail box 
                            if(map_Teammailbox.get(TeamName)!=null && map_Teammailbox.get(TeamName).Team_mailbox__c!= null){
                                  sendTo.add(map_Teammailbox.get(TeamName).Team_mailbox__c);
                            }else 
                            {
                                  isEmailToSend= false;
                            }
                            mail.setToAddresses(sendTo);
                            // Step 3: Set who the email is sent from
                            mail.setSenderDisplayName('USI India ELE Separations team');
                           
                            // Step 4. Set email contents - you can use variables!
                            mail.setSubject(Reminder_type+'- USI Exit Clearances');
                            String body = 'Dear   '+TeamName+ ' Team, <br/><br/> This is a gentle reminder to provide clearances for the USI exits available on your ‘Exit Management’ dashboard.<br/> <br/>There is/are '+map_tempReminders.get(TeamName)+'  total number of case/cases pending for your clearance. <br/><br/><br/>Thanks & Regards,<br/><b>   Deloitte USI Separations Team</b>' ;
                            mail.setHtmlBody(body);
                    
                        // Step 5. Add your email to the master list
                        if(isEmailToSend)
                            mails.add(mail);
               
            }
                
        }   
        if(mails.size()>0)
        {
            Messaging.sendEmail(mails);     
        }
        
    }

    /** 
        Method Name  : sendEmailNotifications
        Return Type  : Messaging.SingleEmailMessage
        Description  : to send emails 
       */
    global static Messaging.SingleEmailMessage sendEmailNotifications(SObject sObj,String templateId,String targetObjectId,List<String> toAddressEmails,String whatid,String displayName)
    {

        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setSenderDisplayName(dispLayName);
        email.setTemplateID(templateId);
        email.setToAddresses(toAddressEmails);
        email.setTargetObjectId(targetObjectId);
        return email; 

    }

}