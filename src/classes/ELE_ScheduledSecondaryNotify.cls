global class ELE_ScheduledSecondaryNotify implements Schedulable{

      global void execute(SchedulableContext sc) {
     
      ELE_Batch_Clerance_NotificationSend batchApex = new ELE_Batch_Clerance_NotificationSend();
      Database.executeBatch(batchApex,50);  

   }
}