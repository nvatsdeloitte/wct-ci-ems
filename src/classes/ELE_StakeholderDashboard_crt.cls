public with sharing class ELE_StakeholderDashboard_crt {

    

    public list<Clearance_separation__c> List_ClearanceToUpdate { get; set; }

public String sUserEmail{get;set;}
public String sStakeholderType{get;set;}
public list<contact> cConUser{get;set;}
public list<string> listAccessLevel{get;set;}
public String sPageURL{get;set;}
public String sPageURL2{get;set;}
public string sStakeholderDesignation {get;set;}
public String sEncryptedEmail {get;set;}
public String sEnEmail {get;set;}
public boolean bIsvalidUser{get;set;}
public String strJobName{get;set;}
//Search closed ELE cases    
public String CloseSrh{get;set;} 
public string closeView{get;set;} 
public list<Clearance_separation__c> tmpList_ClearanceToUpdate { get; set; }
  
    
      /** 
        Method Name  : ELE_StakeholderDashboard_crt
        Return Type  : list<cAsset>
        Description  : returns the list of assets to display on the page
        */
public ELE_StakeholderDashboard_crt()
    {
      closeView='';
      sEncryptedEmail = ApexPages.currentPage().getParameters().get('em');
      system.debug('email id encrypted == '+sEncryptedEmail );
      //sEncryptedEmail =URLEncode(sEncryptedEmail);
     //  bIsvalidUser= false;
      sUserEmail= cryptoHelper.decrypt(sEncryptedEmail);
       sEnEmail = cryptoHelper.encrypt(sUserEmail);
      sStakeholderType= ApexPages.currentPage().getParameters().get('Param1');      
      List_ClearanceToUpdate= new list<Clearance_separation__c>();
      cConUser= new list<contact>();
      if(sStakeholderType=='p')
      {
       if(sUserEmail.length()!=0){
       
        String filter= '%'+sUserEmail+'%';
        List_ClearanceToUpdate=[select id,ELE_Status__c,ELE_Employee_name__c, ELE_EMP_Personal_no__c,ELE_Employee_Comments__c, ELE_USI_Team_Comments__c,
        ELE_Stakeholder_Designation__c,ELE_Last_Working_Day__c, ELE_Location_Separation__c,ELE_Location__c,ELE_Service_Line__c,ELE_Service_Area__c  from Clearance_separation__c where ELE_Clearance_Authority_Type__c ='Primary Stakeholder' and (ELE_Status__c  <> 'Closed' and ELE_Status__c <> 'Revoke Resignation Approved')  And (ELE_Primary_Stakeholder_Email_id__c=:sUserEmail OR ELE_Additional_Fields__c LIKE :filter) and ELE_Separation__r.ELE_Contact__r.Email <> :sUserEmail ];
        system.debug('ELE projet site Lc'+List_ClearanceToUpdate); 
        bIsvalidUser=true;
        
        }else
        {
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You are not a authorized user. Please contact your administrator for access issue.'));
              bIsvalidUser=false;
               
        }
      }else if(sStakeholderType=='s')
      { 
        cConUser=[select id, ELE_Access_Level__c from contact where Email=:sUserEmail and ELE_Contact__c=:true and RecordType.Name = 'Employee'];
         if((cConUser.size()>0 )&& (cConUser[0].ELE_Access_Level__c!=null))
         {
          
           listAccessLevel = cConUser[0].ELE_Access_Level__c.split(';'); 
           List_ClearanceToUpdate=[select id,ELE_Status__c,ELE_Employee_name__c,ELE_EMP_Personal_no__c,ELE_Employee_Comments__c, ELE_USI_Team_Comments__c  
           ,ELE_Stakeholder_Designation__c, ELE_Location_Separation__c,ELE_Last_Working_Day__c,ELE_Location__c,ELE_Service_Line__c,ELE_Service_Area__c
           ,ELE_Clearance_Authority_Type__c from Clearance_separation__c where  ELE_Stakeholder_Designation__c IN: listAccessLevel AND ELE_Clearance_Authority_Type__c ='Secondary Stakeholders' and (ELE_Status__c  <> 'Closed' and ELE_Status__c <> 'Revoke Resignation Approved') and ELE_Separation__r.ELE_Contact__r.Email <> :sUserEmail ];
        //   List_ClearanceToUpdate[0].Clearance_Authority_Type_Separation__c ='Secondary Stakeholder';
        //   List_ClearanceToUpdate[1].Clearance_Authority_Type_Separation__c ='Primary Stakeholder';
        bIsvalidUser= true;
              }
         else{
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You are not a authorized user. Please contact your administrator for access issue.'));
           bIsvalidUser=false;
         }
         
      }
      tmpList_ClearanceToUpdate=List_ClearanceToUpdate;
        system.debug('TestC:'+tmpList_ClearanceToUpdate);
       sPageURL='/ELE_employeeDataUpdatePage?email='+sUserEmail+'&tm=';
       sPageURL2='&type='+sStakeholderType+'&id=';
    }
    
    public void RedirectToEmp()
    {
        //return null;
       
       // sEnEmail = cryptoHelper.encrypt(strJobName);
     //   System.debug('sEnEmail ===> '+sEnEmail);
        
    }
    public pagereference searchcls()
      {
        system.debug('Test:'+tmpList_ClearanceToUpdate);
        string tmp=CloseSrh+1;
        system.debug('&&'+tmp);
        system.debug(tmp !='1');
        if(tmp !='1')
        {
        system.debug('&&&&'+CloseSrh+'&&');
        closeView='Close';   
        List_ClearanceToUpdate.clear();
        if(sStakeholderType=='p')
      {
       system.debug('>>>>>>>>>>>>>>>>>>>>>>>sUserEmail>>>>>>>>>>>>>>>>>>>>>>>>>'+sUserEmail);
       if(sUserEmail.length()!=0){
       
        String filter= '%'+sUserEmail+'%';
        List_ClearanceToUpdate=[select id,ELE_Status__c,ELE_Employee_name__c, ELE_EMP_Personal_no__c,ELE_Employee_Comments__c, ELE_USI_Team_Comments__c,
        ELE_Stakeholder_Designation__c,ELE_Last_Working_Day__c, ELE_Location_Separation__c,ELE_Location__c,ELE_Service_Line__c,ELE_Service_Area__c from Clearance_separation__c where ELE_Clearance_Authority_Type__c ='Primary Stakeholder' and (ELE_Status__c  = 'Closed' or ELE_Status__c = 'Revoke Resignation Approved')  And (ELE_Primary_Stakeholder_Email_id__c=:sUserEmail OR ELE_Additional_Fields__c LIKE :filter) and ELE_Separation__r.ELE_Contact__r.Email <> :sUserEmail and ELE_EMP_Personal_no__c=:CloseSrh];
        system.debug('ELE projet site Lc'+List_ClearanceToUpdate); 
        bIsvalidUser=true;
        
        }else
        {
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You are not a authorized user. Please contact your administrator for access issue.'));
              bIsvalidUser=false;
               
        }
      }else if(sStakeholderType=='s')
      { 
         cConUser=[select id, ELE_Access_Level__c from contact where Email=:sUserEmail and ELE_Contact__c=:true and RecordType.Name = 'Employee'];
         if((cConUser.size()>0 )&& (cConUser[0].ELE_Access_Level__c!=null))
         {
          
           listAccessLevel = cConUser[0].ELE_Access_Level__c.split(';'); 
           List_ClearanceToUpdate=[select id,ELE_Status__c,ELE_Employee_name__c,ELE_EMP_Personal_no__c,ELE_Employee_Comments__c, ELE_USI_Team_Comments__c  
           ,ELE_Stakeholder_Designation__c, ELE_Location_Separation__c,ELE_Last_Working_Day__c,ELE_Location__c
           ,ELE_Clearance_Authority_Type__c,ELE_Service_Line__c,ELE_Service_Area__c from Clearance_separation__c where  ELE_Stakeholder_Designation__c IN: listAccessLevel AND ELE_Clearance_Authority_Type__c ='Secondary Stakeholders' and (ELE_Status__c  = 'Closed' or ELE_Status__c = 'Revoke Resignation Approved') and ELE_Separation__r.ELE_Contact__r.Email <> :sUserEmail and ELE_EMP_Personal_no__c=:CloseSrh ];
        //   List_ClearanceToUpdate[0].Clearance_Authority_Type_Separation__c ='Secondary Stakeholder';
        //   List_ClearanceToUpdate[1].Clearance_Authority_Type_Separation__c ='Primary Stakeholder';
        bIsvalidUser= true;
              }
         else{
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You are not a authorized user. Please contact your administrator for access issue.'));
           bIsvalidUser=false;
         }
        
      }
          return null;
        }
          else
          {
          List_ClearanceToUpdate=tmpList_ClearanceToUpdate;
          PageReference pg=new PageReference('/ELE/ELE_stakeholderdashboard?em='+sEnEmail+'&Param1='+sStakeholderType); 
          pg.setRedirect(false);    
          return pg;    
          }
    }
    public pagereference backbtn()
    {
          List_ClearanceToUpdate=tmpList_ClearanceToUpdate;
          PageReference pg=new PageReference('/ELE/ELE_stakeholderdashboard?em='+sEnEmail+'&Param1='+sStakeholderType); 
          pg.setRedirect(false);    
          return pg; 
    }
}