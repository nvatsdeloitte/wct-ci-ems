/**************************************************************************************
Batch class       :  ELE_employeeDataUpdateController
Version          : 1.0 
Created Date     : 25 April 2015
Function         : Display from clearance to employeeupdatepage
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                   25/04/2015            Original Version
*************************************************************************************/
public class ELE_employeeDataUpdateController {
  
    //Public Class variables
    public string status {get;set;}
    public Transient List<Attachment> attchList{get;set;}
    public list<Clearance_separation__c> cClearanceToupdate{get;set;}
    public list<Asset__c> cAssetToupdate{get;set;}
    public String sTeamName{get;set;}
    public Clearance_separation__c cClToUpdate{get;set;}
    public Asset__c cAstToUpdate{get;set;}
    public string customLabelValue{get;set;}
    public boolean rendering {get;set;}
    public list<Contact> conObj {get;set;}
    public string encryptedEmailString {get;set;}
    public String sEncryptedEmail{get;set;}
    public string email{get;set;}
    public string sEmailId{get;set;}
    public String stakeholderType{get;set;}
    public String sAssetToupdateID{get;set;}
    public String sClearanceId{get;set;}
    public blob blEncriptedTeamname{get;set;}    
    public string sValue {get;set;}
    public list<cAsset> listcAssetToUpdate{get;set;}
    public boolean IsVaidUser{get;set;}    
    public string sClearanceOldStatus{get;set;}    
    public set<string> setAccessLevel= new set<string>(); 
    public string sLWD {get;set;}
    public string sDOJ {get;set;}
    public attachment attNotice{get;set;}
    public attachment attSpecialLeav{get;set;}
    public attachment attRehireStatus{get;set;}
    public attachment attNoticePay{get;set;}
    public attachment attPartial{get;set;}
    public list<attachment> List_attToAdd{get;set;}
    public set<string> Attachmentsnames {get;set;}
    Public List<Attachment> List_attToDisplay{get;set;}
    public List<Asset__c> asstLst{get;set;}
    Public Map<string,string> AssetsAttachmentsMap {get;set;}    
    public ELE_Separation__c ele_septoupdate{get;set;}
    Public Document doc {get;set;}
    public List<String> docIdList = new List<string>();
    public List<AttachmentsWrapper> UploadedDocumentList {get; set; }
    public static List<AttachmentsWrapper> UploadedDocumentListLocalHost =new List<AttachmentsWrapper>();
    public boolean addAttachment{get; set;}
    public string setpopdisplay{get;set;}
    public List<Document> selectedDocumentList {get;set;}
    Public List<SelectOption> asstnameopt {get;set;}
    Public String assetselctop{get;set;}
    
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public date temp;
    public date resigtemp;
    
    public PageReference AddAsset() {
      return null;
    }
    
    /** 
        Method Name  : updateDashBoard
        Return Type  : PageReference
        Description  : Invokes when the update button is clicked on employeeupdatepage
        */
        public PageReference updateDashBoard() {
          
          attchList = new List<Attachment>();
          list<Asset__c> list_SelecetdAssetToUPdate= new list<Asset__c>();
     // conObj  = [select id,name,email,WCT_Personnel_Number__c,WCT_Gender__c from contact where email = :email limit 1];
     
     for(cAsset lp_cAssets: getList_cAssetToDisplay())
     {
       if(lp_cAssets.bSelectedAsset)
       { 
        lp_cAssets.cAsset.ELE_Asset_updatedby__c=conObj[0].Name+'---'+(string)system.now().format();
        lp_cAssets.cAsset.ELE_Asset_Status__c =lp_cAssets.sSelectedStatusValues;
        list_SelecetdAssetToUPdate.add(lp_cAssets.cAsset);
        lp_cAssets.bSelectedAsset=false;
        if(lp_cAssets.att != null && lp_cAssets.att.name != null && lp_cAssets.att.body != null)
        {
          lp_cAssets.att.parentid = lp_cAssets.cAsset.id;
          attchList.add(lp_cAssets.att);
          
        }
      }
         
    }
    
    
    
    try
    {
      //   uploadRelatedAttachment();
    update list_SelecetdAssetToUPdate;
    }catch(Exception e)
    {
      system.debug('There is an exception to update the asset from -'+sTeamname+'- dashboard. exception is '+e.getMessage());
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception is'+e));
      return null;
      
    }
   
if(cClearanceToupdate[0].ELE_Other_recoveries__c == 'Yes' && (cClearanceToupdate[0].ELE_Comments__c == '' || cClearanceToupdate[0].ELE_Comments__c == null)){
 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'If Other Recoveries are yes, Please provide Remarks/comments'));
 return null;
}else{}
  
 cClearanceToupdate[0].ELE_Last_updatedby__c = conObj[0].Name+'---'+(string)system.now().format();
 uploadRelatedAttachment();

if(!ApexPages.hasMessages()){
                
   if(cClearanceToupdate[0].ELE_Comments_For_USI_team__c !='')
   {
      cClearanceToupdate[0].Stakeholder_Comment_Unread__c=true;
   }
    if(attachmentsToInsertList!=null){
    cClearanceToupdate[0].ELE_Attachments__c +=  attachmentsToInsertList.size();
   }
    try{
    if(stakeholderType =='p' && (temp !=ele_septoupdate.ELE_Last_Date__c  || resigtemp  != ele_septoupdate.ELE_Date_of_resignation__c ))     
    update ele_septoupdate;    
    update cClearanceToupdate;
    PageReference pageRef = Page.ELE_stakeholderDashboard; 
    pageRef.getParameters().put('em',sEncryptedEmail);
    pageRef.getParameters().put('Param1',stakeholderType);
    pageRef.setRedirect(false);
    return pageRef;
    }catch(Exception e){
                           
      if(e.getMessage().contains('Kindly close all assets before closing clearance'))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Kindly close all assets before closing clearance'));
            return null;
        }     
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;                     
         }   
    
    }  
  }
            else{
                return null;
             }
}


      /** 
        Method Name  : getList_cAssetToDisplay
        Return Type  : list<cAsset>
        Description  : returns the list of assets to display on the page
        */
        
        public list<cAsset> getList_cAssetToDisplay(){
          asstLst = new List<Asset__c>();
            AssetsAttachmentsMap = new Map<string,string>();
            if(listcAssetToUpdate==null){
                            asstnameopt.add(new Selectoption('--None--','--None--'));  

            listcAssetToUpdate= new list<cAsset>();  
            asstLst = [select id,ELE_Team_Name__c,ELE_Asset_Status__c,ELE_Asset_status_values__c,ELE_Cost__c, Ele_Description__c, ELE_Number_of_Assets__c,Ele_Payable_Amount__c,
            ELE_Status__c,ELE_TeamName__c,ELE_Asset_Name__c,Ele_Total_Amount__c,ELE_Waived_Amount__c,ELE_Ref_No__c,ELE_Asset_updatedby__c,(SELECT Id, Name,Description,ContentType FROM Attachments) from Asset__c where /*ELE_Team_Name__c =: sTeamName and*/ ELE_Clearance__c =: sClearanceId order by ELE_Asset_Name__c ASC];  
            for(Asset__c cAssetToupdate : asstLst)
            {
             List<SelectOption> selectOptions = new List<SelectOption>();
             String str = cAssetToupdate.ELE_Asset_status_values__c;
             asstnameopt.add(new Selectoption(cAssetToupdate.ELE_Asset_Name__c,cAssetToupdate.ELE_Asset_Name__c));  
            listcAssetToUpdate.add(new cAsset(cAssetToupdate, str));
             for(Attachment attac : cAssetToupdate.Attachments){
              List_attToDisplay.add(attac);
                 AssetsAttachmentsMap.put(attac.id, cAssetToupdate.ELE_Asset_Name__c);
             }    
                
           }
            
             
         }  
        return listcAssetToUpdate;

       }
       
      /** 
        Method Name  : cancelDashBoard
        Return Type  : PageReference
        Description  : Invokes when the cancel button is clicked from the page
        */
        public pagereference cancelDashBoard()
        {
         PageReference pageRef = Page.ELE_stakeholderDashboard; 
         pageRef.getParameters().put('em',sEncryptedEmail);
         pageRef.getParameters().put('Param1',stakeholderType);
         pageRef.setRedirect(true);
         return pageRef;
       }
     /** 
        Method Name  : getsDashBoardView
        Return Type  : string
        Description  : To ensure to which stakeholders data needs to be display on the page i.e whether Primary or secondary stakeholder 
        */
        public String getsDashBoardView(){
         
         if(IsVaidUser){
          if(stakeholderType=='p')
          {
            return 'p';
          }
          else if(stakeholderType=='s')
          {
            if(setAccessLevel.size()>0){
              if(cClToUpdate.ELE_Stakeholder_Designation__c!='Treasury')
              {
               return 'Asset';
             }
             else
             {
               return 'Treasury';
             }
           }
           return null;
         }
         return null;
       }
       return null;
     }


     public ELE_employeeDataUpdateController ()
     {
       
        doc = new Document();        
        UploadedDocumentList = new List<AttachmentsWrapper>();
        asstnameopt = new List<Selectoption>();
        assetselctop = '--None--';
         
       attNotice= new Attachment();
       attSpecialLeav= new Attachment();
       attRehireStatus= new Attachment();
       attNoticePay= new Attachment();
       attPartial= new Attachment();
       List_attToAdd = new list<Attachment>();
       encryptedEmailString = '' ;
       sClearanceId=ApexPages.currentPage().getParameters().get('id');
       customLabelValue = System.Label.TeamNameLabel;
       List<String> strParts = customLabelValue.split('\\,');
       sTeamName= ApexPages.currentPage().getParameters().get('tm');
       
       String sCurrentSUserMail=ApexPages.currentPage().getParameters().get('cue');
       stakeholderType=ApexPages.currentPage().getParameters().get('type');
       sEncryptedEmail = ApexPages.currentPage().getParameters().get('em');
       email = cryptoHelper.decrypt(sEncryptedEmail);
       sEmailId = cryptoHelper.encrypt(email);
       attchList = new List<Attachment>();
       list<string > listAccessLevel= new list<String>();
       
       conObj  = [select id,name,email,WCT_Personnel_Number__c,WCT_Gender__c,ELE_Access_Level__c from contact where email = :email and RecordType.Name = 'Employee' limit 1];
       if((conObj.size()>0)&&(conObj[0].ELE_Access_Level__c!=null)){
        listAccessLevel=conObj[0].ELE_Access_Level__c.split(';'); 
        setAccessLevel.addAll(listAccessLevel); 
      }
      String filter= '%'+email+'%';
      ele_septoupdate=new ELE_Separation__c();
      cClearanceToupdate= [select id,ELE_Notice_Period__c,ELE_Contact_Emp_Name__c,ELE_Others_Reason_Exit__c,
      ELE_FSS__c,ELE_Comments__c,
      ELE_Stakeholder_Designation__c,
      ELE_Status__c,ELE_Notice_Period_Reason__c,
      ELE_Reason_for_Exit__c,ELE_Notice_or_Severance_pay__c ,
      ELE_Onsite_Separation__c,ELE_Special_Leaves__c,ELE_Clearance_Authority_Type__c,
      ELE_Other_recoveries__c,ELE_Notice_period_partial__c,
      ELE_Last_updatedby__c,ELE_Notice_peroid_served_days__c,ELE_Notice_peroid_waived_days__c,
      ELE_Status_of_the_payment__c,ELE_Mode_of_Payment_by_the_employee__c,ELE_DD_or_Managers_cheque_Number__c,
      ELE_Recovery_Bank_Name__c,ELE_Date_on_DD_or_Cheque__c,ELE_Transaction_Remarks__c,ELE_Transaction_reference_no__c,ELE_Recovery_Online_BankName__c,ELE_Date_Online_Transfer__c,
      ELE_Rehire_Status__c,ELE_Future_member_firm__c,ELE_Notice_period_waived__c,ELE_Partially_waived_or_recovered__c,
      ELE_Comments_For_Employee__c, ELE_Comments_For_USI_team__c,ELE_Employee_name__c,ELE_Global_Personnel_Number__c,ELE_EMP_Personal_no__c,ELE_DateOfJoining__c ,
      ELE_Deloitte_Email__c,ELE_Service_Line__c,ELE_Service_Area__c,ELE_Entity__c,ELE_Gender__c,ELE_Designation__c,ELE_Location__c,
      ELE_Last_Working_Day__c,
      ELE_Separation__r.ELE_Last_Date__c,ELE_Separation__r.ELE_Case_status__c,ELE_Separation__r.ELE_DOJ__c,ELE_Separation__r.ELE_Date_of_resignation__c, 
      ELE_Recovery_Amount1__c,ELE_Partially_recovered__c,ELE_Attachments__c,(SELECT Id, Name,Description,ContentType FROM Attachments)
      from Clearance_separation__c where ID =: sClearanceId and ((ELE_Primary_Stakeholder_Email_id__c=:email OR ELE_Additional_Fields__c LIKE :filter)OR(ELE_Stakeholder_Designation__c IN: listAccessLevel))];
      
        
     if(cClearanceToupdate.size()>0){
        cClToUpdate=cClearanceToupdate[0];
        ele_septoupdate.id=cClToUpdate.ELE_Separation__c;
        temp= cClToUpdate.ELE_Separation__r.ELE_Last_Date__c;
        ele_septoupdate.ELE_Last_Date__c=cClToUpdate.ELE_Separation__r.ELE_Last_Date__c;
        resigtemp = cClToUpdate.ELE_Separation__r.ELE_Date_of_resignation__c;
        ele_septoupdate.ELE_Date_of_resignation__c  = cClToUpdate.ELE_Separation__r.ELE_Date_of_resignation__c;
        Date LWD =  Date.valueOf(cClToUpdate.ELE_Last_Working_Day__c);
        if(LWD!= null)
        sLWD = LWD.format();
        Date DOJ =  Date.valueOf(cClToUpdate.ELE_DateOfJoining__c );
        if(DOJ != null)
        sDOJ = DOJ.format();
        sClearanceOldStatus=cClToUpdate.ELE_Status__c;
        
        IsVaidUser=true;
        }else
        {
          IsVaidUser=false;
          
        } 
        
        Attachmentsnames  = new set<string>();
        List_attToDisplay = new List<Attachment>();
        for(Attachment atta : cClToUpdate.Attachments){
              
              List<string> ss =  atta.Name.split('-');
              Attachmentsnames.add(ss[0].trim());
              List_attToDisplay.add(atta);

            
        }
        
        
        
      }
    /** 
        class Name   : cAsset
        Description  : Fetch all the assets based along with status values
        */
        public class cAsset
        {
         public boolean assetstatus{get;set;}
         public Asset__c cAsset{get;set;}
         public boolean bSelectedAsset{get;set;}
         public Attachment att{get;set;}
         public List<selectoption> cAccetstatusValues{get;set;}
         public string sSelectedStatusValues {get;set;}
         public string sValue{get;set;}
         public cAsset(Asset__c cAsst, String statusValues)
         {
          this.att = new Attachment();
          cAsset= cAsst;
          bSelectedAsset=false;
             
        
          
          List<String> stringPts = new List<String>();
          cAccetstatusValues= new List<selectoption>();
          sSelectedStatusValues='--None-';
          
          
          if(cAsst.ELE_Asset_Status__c != null){
            if (cAsst.ELE_Asset_Status__c !='--None--')  
            assetstatus=true; 
            sValue = cAsst.ELE_Asset_Status__c;
            SelectOption selectOption2 = new SelectOption(sValue,sValue);
            cAccetstatusValues.add(selectOption2 );
          }
          if(statusValues!= null )
          //stringPts = statusValues.split('\\/');
          stringPts = statusValues.split(',');
          
          for (Integer i = 0; i < stringPts.size(); i++) {
            if(sValue != stringPts[i] ){
              SelectOption selectOption1 = new SelectOption(stringPts[i],stringPts[i]);
              cAccetstatusValues.add(selectOption1);
            }
          }
        }
      }
    
    Public Boolean validateattatchuploaded(){
        system.debug('Attachmentsnames Attachmentsnames' +DupattachMap);
        
       if(cClearanceToupdate[0].ELE_Status__c == 'Closed'){
        if(cClearanceToupdate[0].ELE_Notice_period_waived__c == 'Yes'){
                    if(!DupattachMap.contains('Waived')){
                      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please provide SSL Approval to encash leaves'));
                    }
             
                }
                if(cClearanceToupdate[0].ELE_Special_Leaves__c == 'Yes'){
                   if(!DupattachMap.contains('Special Leave')){
                     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please provide SSL Approval for special leaves'));
                   }
                }
                if(cClearanceToupdate[0].ELE_Rehire_Status__c == 'No'){
                   if(!DupattachMap.contains('Rehire Status')){
                     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please provide SSL Approval for Rehire Status'));
                   }
                 }
                if(cClearanceToupdate[0].ELE_Notice_or_Severance_pay__c == 'Yes'){
                     if(!DupattachMap.contains('Notice pay')){
                      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please provide SSL Approval for Notice Pay/Severance Pay'));
                     }
                }       
                if(cClearanceToupdate[0].ELE_Notice_period_partial__c == 'Yes'){
                    if(!DupattachMap.contains('Partial waived')){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please provide SSL Approval for Partially waived/recovered peroid'));
                    }
                }
       }
        Boolean haserrors = false;
        if(ApexPages.hasMessages()) {
            haserrors = true;
        }
        else{
            haserrors = false;
        }
         return haserrors;
        
        
    }
    
    
    
    
    
    
    Public Boolean haspagerors;
    Public Boolean haserors;
    Public set<string> DupattachMap;
    Public List<Attachment> attachmentsToInsertList {get;set;}

    Public void uploadRelatedAttachment(){
        system.debug('@@########## ' +Attachmentsnames);
         assetselctop = '';
         haspagerors = false;
        haserors = false;
        DupattachMap = new Set<string>();
        DupattachMap = Attachmentsnames.clone();
        
        if(docIdList.isEmpty()) {  
             haspagerors = validateattatchuploaded ();
         }
           
        if(haspagerors == true) {
             return;
        }
       else {
         system.debug('######## ' + docIdList); 
         if(!docIdList.isEmpty()) { //If there is any Documents Attached in Web Form
            system.debug('#######' +UploadedDocumentList);             
            List<String> selectedDocumentId = new List<String>();
            for(AttachmentsWrapper aWrapper : UploadedDocumentList){
                if(aWrapper.isSelected){
                    selectedDocumentId.add(aWrapper.documentId);
                }
            }
            system.debug('24142124' + selectedDocumentId);
            /* Select Documents which are Only Active (Selected) */
            selectedDocumentList = new List<Document>();
            if(!selectedDocumentId.isEmpty()){

                selectedDocumentList = [
                                           SELECT 
                                               id,
                                               name,
                                               ContentType,
                                               Type,
                                               Body 
                                           FROM 
                                               Document 
                                           WHERE 
                                               id IN :selectedDocumentId
                                        ];
            }
             system.debug('23253535' + selectedDocumentList);
            /* Convert Documents to Attachment */
            attachmentsToInsertList = new List<Attachment>();
           
           for(Document doc : selectedDocumentList){
               string dcname = doc.Name.split(' - ')[0];
              //  Attachmentsnames.add(dcname.trim()); 
                DupattachMap.add(dcname.trim());
                Attachment a = new Attachment();
                a.body = doc.body;
                a.ContentType = doc.ContentType;
                a.Name= doc.Name;
                a.parentid = cClToUpdate.id; //Clearance Record Id which is been recently created.
                attachmentsToInsertList.add(a);   
            }  
               haserors = validateattatchuploaded() ;
            system.debug('haspgerors Attachmentsnames' +haserors);
            if(haserors == true) {
                return;
             }
             else{
                 
                  if(!attachmentsToInsertList.isEmpty()){
                        insert attachmentsToInsertList;
                    }
             
                 
            system.debug('attachmentsToInsertList ====' +attachmentsToInsertList);
            List<Document> listDocuments = new List<Document>();
            listDocuments = [
                                SELECT
                                    Id
                                FROM
                                    Document
                                WHERE
                                    Id IN :docIdList
                            ];
            if( (null != listDocuments) && (0 < listDocuments.size()) ) {
                delete listDocuments;
            }
         }
        }
         }
    }
    
    
    
     /*Invoked when Upload Button in VF page is clicked and the IDs are stored in the docIdList*/
    /*All the Files Uploaded are stored in the Documents. Documents does not require parent Id.This method is used to circumvent to upload
      the documents first and then add as Attachment to the Cases (Related List)*/

    public void uploadAttachment(){
        
        pageError = false;
        pageErrorMessage = '';
         
               if(ApexPages.hasMessages()) {
            pageErrorMessage = '';
            for(ApexPages.Message message : ApexPages.getMessages()) {
                pageErrorMessage += message.getSummary() + '\n';
            }            
                   system.debug('343243' + pageErrorMessage);
            doc = new Document();
            pageError = true;
            return;
        }                                
        System.debug('@@@@..1..' + doc.name);
        system.debug('1111111111111 ' + apexpages.currentpage().getparameters().get('assignprefix'));
        if(assetselctop == '' || assetselctop == null || assetselctop == '--None--'){
            assetselctop =  apexpages.currentpage().getparameters().get('assignprefix');
        }
        
        doc.name = assetselctop + ' - ' + doc.name;
      
        doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
        if(doc.body != null) {
        Database.insert(doc,false);
            system.debug('@@@@@@@@' + doc );
            docIdList.add(doc.id);
            doc= [SELECT id, name, bodylength FROM Document WHERE id = :doc.id];

            String size = '';
            if(1048576 < doc.BodyLength) {
              // Size greater than 1MB
                size = '' + (doc.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < doc.BodyLength) {
              // Size greater than 1KB
                size = '' + (doc.BodyLength / 1024) + ' KB';            
            }
            else {
                size = '' + doc.BodyLength + ' bytes';
            } 
            UploadedDocumentList.add(new AttachmentsWrapper(true, doc.name, doc.id,size));
            system.debug('@@@7777' +UploadedDocumentList );
            doc = new Document();
        }
               addAttachment=false;
    }       
    
    //Documents Wrapper
    public class AttachmentsWrapper {
        public Boolean isSelected {get;set;}
        public String docName {get;set;}
        public String documentId {get;set;}
        public String size {get; set;}
        
        public AttachmentsWrapper(Boolean selected, String Name, String Id, String size){
            isSelected = selected;
            docName = Name ;
            documentId = Id;
            this.size = size;
        }        
    } 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
      
    }