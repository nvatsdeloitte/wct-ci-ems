public class EmailFormerInternController {
    public string emailarea {get;set;}
    public string Eventid;
    public string Eventname;
    public string Eventstartdate;
    public string Eventenddate;
    public string venuename;
    public string frmid;
    public string rsst;
    public boolean mailsent{get;set;}
    public string fromaddress{get;set;}
    public list<string> invalidemails{get;set;}
    public boolean mailinvalid{get;set;}
    public EmailFormerInternController()
    {
        mailsent=true;
        mailinvalid=false;
        invalidemails=new list<string>();
    }
    // method to send mail to former interns
    public pagereference sendmail()
    {
        Eventid=apexpages.currentPage().getParameters().get('id');
        Eventname=apexpages.currentPage().getParameters().get('name');
        Eventstartdate=apexpages.currentPage().getParameters().get('startdate');
        Eventenddate=apexpages.currentPage().getParameters().get('enddate');
        List<string> temptext=new List<string>();
        frmid='';
        rsst='';
        //var checkvalidemail=;
        temptext=emailarea.split(';');
        list<Messaging.SingleEmailMessage> messages=new list<Messaging.SingleEmailMessage>();
        list<Messaging.SingleEmailMessage> messagespwd=new list<Messaging.SingleEmailMessage>();
        for(integer i=0;i<temptext.size();i++)
        {
            temptext[i]=temptext[i].trim();
            if(Pattern.matches('^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$',temptext[i]))
            {
                string tempEncryptemail=CryptoHelper.encrypt(temptext[i]);
                string pwd=generatepwd(temptext[i]);
                string urllink=system.Label.Site_Base_URL+'/event/FormerInternRSVP?id='+Eventid+'&em='+tempEncryptemail+'&frmid='+frmid+'&rsst='+rsst;
                List<string> tmp=new List<string>();
                tmp.add(temptext[i]);
       		    Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.setSubject(system.Label.Former_Intern_sub);
                message.setToAddresses(tmp);
            	message.setOrgWideEmailAddressId(fromaddress);
            	message.sethtmlbody('<table style="font-family:Arial"><tbody><tr><td style="padding-bottom:20px;padding-top:10px"><img src="'+system.label.Document_URL+'id='+system.label.Deloitte_Logo_Label+'&oid='+system.label.Org_ID+'" alt=""></td></tr><tr><td style="padding-bottom:20px;font-size:9pt">Deloitte LLP  |  2015</td></tr><tr><td><img src="'+system.label.Document_URL+'id='+system.label.Guitar_Label+'&oid='+system.label.Org_ID+'" alt=""></td></tr><tr><td style="COLOR: #002776; FONT-SIZE: 30pt">Amplify your future</td></tr><tr><td style="color: rgb(146, 212, 0); FONT-SIZE: 30pt">2015 intern conferences</td></tr></tbody></table><table><tbody><tr><td style="COLOR: #00a1de; FONT-SIZE: 30pt;padding-top:10px;padding-bottom:10px;font-family:Arial">You\'re Invited!</td></tr><tr><td style="FONT-SIZE: 12pt:font-family:arial">We welcome you to join your fellow interns and future colleagues for a multi-day conference at Deloitte University focusing on professional development, networking and team building. The conference will be an excellent opportunity for you to:<div><br/>1. Gain valuable insight about Deloitte’s business and culture<br/>2. Create valuable professional relationships<br/>3. Meet interns from other offices<br/>4. Have fun!</div></td><tr></table><br/><table><tr><td style="background:#00a1de;width:45px; color : white; padding: 10px ;font-weight:bold;font-size:12pt;font-family:arial">What</td><td style="width:348px; background:white ;border-bottom: 1px #00a1de solid;font-size:12pt;font-family:arial">'+Eventname+'</td></tr><tr><td style="background:#00a1de;width:45px; color : white;padding: 10px;font-size:12pt;font-family:arial;font-weight:bold">When</td><td style="width:348px; background:white; border-bottom: 1px #00a1de solid;font-size:12pt;font-family:arial">'+Eventstartdate+' through '+Eventenddate+'</td></tr><tr><td style="background:#00a1de;width:45px; color : white; padding: 10px;font-size:12pt;font-family:arial;font-weight:bold ">Where</td><td style="width:348px; background:white; border-bottom: 1px #00a1de solid;font-size:12pt;font-family:arial">Deloitte University</td></tr><tr><td style="background:#00a1de;width:45px; color : white; padding: 10px;font-size:12pt;font-family:arial;font-weight:bold ">Attire</td > <td style="width:348px ; background:white; border-bottom: 1px #00a1de solid;font-size:12pt;font-family:arial">Business Casual<td></td></tr></tbody></table><table><tr><td style="font-weight:bold;font-size:12pt;font-family:arial">Click here to <a href=\"'+urllink+'\" alt="" style="text-decoration: underline;color:#00a1de">RSVP</a> </td></tr></table><table><tr><td style="padding-top:10px;font-size:12pt;font-family:arial">Thanks and regards</td></tr><tr><td style="font-size:12pt;font-family:arial">Deloitte Campus Recruiting Team</td></tr></table><table border="0" cellpadding="0" cellspacing="0" style="width:744px;padding-top:10px">	<tbody>		<tr>			<td style="width:782px;font-size:7pt;font-family:arial">			<p style="COLOR: #00a1de"><a href="www.deloitte.com" alt="www.deloitte.com" style="COLOR: #00a1de">www.deloitte.com</a></p>			</td>		</tr>		<tr>			<td style="width:782px">			<p>&nbsp;</p>			</td>		</tr>		<tr>			<td style="width:782px;">			<table border="0" cellpadding="0" cellspacing="0">				<tbody>					<tr>						<td style="width:25px;font-size:7pt;font-family:arial">						<p><a href="https://www.yammer.com/deloitte.com"><img src="'+system.label.Document_URL+'id='+system.label.yammer_label+'&oid='+system.label.Org_ID+'" alt="" width="25px"></a></p>					</td><td width="10px"><p>&nbsp;</p></td><td style="width:25px">						<p><a href="https://www.linkedin.com/company/deloitte"><img src="'+system.label.Document_URL+'id='+system.label.LinkedIn_Label+'&oid='+system.label.Org_ID+'" alt="" width="25px"></a></p>						</td>						<td width="10px"><p>&nbsp;</p></td><td style="width:25px">						<p><a href="https://www.facebook.com/deloitte"><img src="'+system.label.Document_URL+'id='+system.label.Facebook_Label+'&oid='+system.label.Org_ID+'" alt="" width="25px"></a></p>						</td>						<td width="10px"><p>&nbsp;</p></td><td style="width:25px">						<p><a href="https://twitter.com/Deloitte"><img src="'+system.label.Document_URL+'id='+system.label.Twitter_Label+'&oid='+system.label.Org_ID+'" alt="" width="25px"></a></p>						</td>						<td width="10px"><p>&nbsp;</p></td><td style="width:25px">						<p><a href="https://plus.google.com/+Deloitte/posts"><img src="'+system.label.Document_URL+'id='+system.label.Google_Label+'&oid='+system.label.Org_ID+'" alt="" width="25px"></a></p>						</td>						<td width="10px"><p>&nbsp;</p></td><td style="width:25px">						<p><a href="https://www.youtube.com/user/deloittevideo/custom?x=go-en_responsiveglobal"><img src="'+system.label.Document_URL+'id='+system.label.Youtube_Label+'&oid='+system.label.Org_ID+'" alt="" width="25px"></a></p>						<td width="10px"><p>&nbsp;</p></td></td>						<td style="width:25px">						<p><a href="https://www.pinterest.com/deloitte/"><img src="'+system.label.Document_URL+'id='+system.label.Pinterest_Label+'&oid='+system.label.Org_ID+'" alt="" width="25px"></a></p>						</td>					</tr>				</tbody>			</table>			</td>		</tr>		<tr>			<td style="width:782px">			<p>&nbsp;</p>			</td>		</tr>		<tr>			<td style="width:782px;font-size:7pt;font-family:arial">			<p>Deloitte<br/>30 Rockefeller Plaza<br />			New York, NY 10112-0015<br />			United States</p>			<p>Deloitte refers to one or more of Deloitte Touche Tohmatsu Limited, a UK private company limited by guarantee (&ldquo;DTTL&rdquo;), its network of member firms, and their related entities. DTTL and each of its member firms are legally separate and independent entities. DTTL (also referred to as &ldquo;Deloitte Global&rdquo;) does not provide services to clients. Please see www.deloitte.com/about for a more detailed description of DTTL and its member firms.</p>			<p>Copyright &copy; 2015 Deloitte Development LLC. All rights reserved.</p>			<p>To no longer receive emails about this topic please send a return email to the sender with the word &ldquo;Unsubscribe&rdquo; in the subject line.</p>			</td>		</tr>	</tbody></table>');
                message.setsaveAsActivity(false); 
                messages.add(message);
                //send password
                Messaging.SingleEmailMessage messagesendpwd = new Messaging.SingleEmailMessage();
                messagesendpwd.setSubject(system.Label.Former_Intern_Pwd_sub);
                messagesendpwd.setToAddresses(tmp);
            	messagesendpwd.setOrgWideEmailAddressId(fromaddress);
            	messagesendpwd.setplaintextbody('Hi,\n\nplease find your password for registration\n\n'+pwd+'\n\nThanks and regards\nDeloitte Campus Recruiting Team');
                messagesendpwd.setsaveAsActivity(false); 
                messagespwd.add(messagesendpwd);
        }
            else
            {
             mailinvalid=true;
             invalidemails.add(temptext[i]);   
            }
        }
         if(!messages.isEmpty()){
                Messaging.sendEmail(messages);
            }
            if(!messagespwd.isEmpty()){
                Messaging.sendEmail(messagespwd);
            }
        mailsent=false;
        return null;
    }
    //method to get the mailboxes
        public List<SelectOption> getItemsmailbox() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption(system.label.Tax_Intern_Conf,'Tax Intern Conference (US)')); 
        options.add(new SelectOption(system.label.AERS_1_Intern_Conf,'AERS #1 Intern Conference (US)')); 
        options.add(new SelectOption(system.label.AERS_2_Intern_Conf,'AERS #2 Intern Conference (US)'));
        options.add(new SelectOption(system.label.Consulting_Summer,'Consulting Summer Scholar Conference (US)'));
        options.add(new SelectOption(system.label.Discovery_Intern_Conf,'Discovery Intern Conference (US)'));
        options.add(new SelectOption(system.label.FAS_Intern_Conf,'FAS Intern Conference (US)'));
        //options.add(new SelectOption(system.label.Test_Conference_lbl,'Admin'));
        return options; 
    }
    public static string generatepwd(string email)
    {
        string pwd;
        pwd=email.substring(0, 4)+'4712';
        return pwd;
    }
}