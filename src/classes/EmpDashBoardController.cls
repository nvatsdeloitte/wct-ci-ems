/**************************************************************************************
Apex class       :  EmpDashBoardController
Version          : 1.0 
Created Date     : 25 April 2015
Function         : Controller to display data in EmployeedashboardPage
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                   25/04/2015            Original Version
*************************************************************************************/

public class EmpDashBoardController extends SitesTodHeaderController {
    

    //Public Class Variables    
    public String types{get;set;}
    public List<wrapperclass> wrapList {get;set;}
    public boolean displayList{get;set;}
    public string emailId='';
    public set<string> setStr = new set<string>();
    public Contact contactObj = new Contact();
    public List<Clearance_separation__c> listAppForUpdt;
    public Transient List<Attachment> attchList {get;set;}   
    List<Contact> conList = new List<Contact>();
    public   ELE_Separation__c Oelerec {get;set;}   
    public string empcmmts{get;set;}

        public EmpDashBoardController()
        {
                  types='';  
                  listAppForUpdt  = new List<Clearance_separation__c>();
                  Oelerec = new ELE_Separation__c ();
                  displayList = false;
                  attchList = new List<Attachment>();
                  emailId=cryptoHelper.decrypt(ApexPages.currentPage().getParameters().get('em'));
                  conList = [select id,name,email,WCT_Personnel_Number__c,ELE_Open_ELE_case__c from contact where email = : emailId and RecordType.Name = 'Employee'];
                  contactObj= conList[0];
           
                  Oelerec = [Select id,ELE_Case_status__c,ELE_Last_Date__c,Name,ELE_IPSF_Proof_Submission_Consent__c,ELE_Comments_emp__c,Employee_Comments__c,Employee_Comment_Unread__c,ELE_Contact_Email__c From ELE_Separation__c
                             Where ELE_Contact_Email__c=:emailId order by createddate desc Limit 1];
                system.debug('@@@@@@@@@@' + Oelerec.ELE_IPSF_Proof_Submission_Consent__c);
                empcmmts=Oelerec.ELE_Comments_emp__c;
                if(Oelerec.ELE_IPSF_Proof_Submission_Consent__c == null || Oelerec.ELE_IPSF_Proof_Submission_Consent__c == ''){
                    Oelerec.ELE_IPSF_Proof_Submission_Consent__c = 'No';
                }
                if(Oelerec.Employee_Comments__c !='')
                {
                    Oelerec.Employee_Comment_Unread__c=true;    
                } 
            
            
            
        }
        /** 
        Method Name  : getWrapperList
        Return Type  : List<wrapperclass>
        Description  : getter method for wrapperlist   
       */
        public List<wrapperclass> getWrapperList()
        {
             wrapList = New List<wrapperclass>();
             for(clearance_separation__c appObj:[Select name,id,ELE_Stakeholder_Designation__c,ELE_Last_Working_Day__c,ELE_Deloitte_Email__c,
             ELE_Status__c,ELE_Contact_Email_Separation__c,ELE_Separation__c,ELE_Separation__r.ELE_IPSF_Proof_Submission_Consent__c,ELE_Separation__r.id,
             Ele_Contact_Email__c,ELE_Comments_For_Employee__c,ELE_Employee_Comments__c,ELE_Employee_comment_updated__c
             from clearance_separation__c where ELE_Deloitte_Email__c =:emailId and ELE_Stakeholder_Designation__c <> 'Treasury' and ELE_Status__c !='Revoke Resignation Approved' ]){
                if(contactObj.Email == appObj.ELE_Deloitte_Email__c)
                {
                   wrapList.add(New wrapperclass(appObj,contactObj));    
                } 
                 
          } 
         return wrapList;
        }

        /** 
        Method Name  : UpdateDetails
        Return Type  : void
        Description  : Invokes when the update button is clicked on EmployeeDashboard
       */
        public void UpdateDetails()
          {
                  List<wrapperclass> listTempWrapper = new List<WrapperClass>();
                  attchList = new List<Attachment>();
                  for(wrapperclass w : wrapList) 
                  {
                      if(w.appRec != null) 
                      { 
                         
                          w.appRec.ELE_Employee_comment_updated__c = w.appRec.ELE_Employee_Comments__c;
                          listAppForUpdt.add(w.appRec);
                          if(w.att != null && w.att.name != null && w.att.body != null)
                          {
                            w.att.parentid = w.appRec.id;
                            attchList.add(w.att);
                          }
                          
                      } 
                      else 
                      {
                          listTempWrapper.add(w);
                      }
                  }
                  if(listAppForUpdt != null && listAppForUpdt.size() > 0) 
                  {
                      update listAppForUpdt;
                      wrapList = listTempWrapper;
                      listAppForUpdt  = new List<Clearance_separation__c>();
                  } 
                 /* else 
                  {
                      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Select atleast one row to update'));
                  } */
                  
                  if(attchList != null && attchList.size() > 0)
                     insert attchList;
                 
                 update Oelerec;
           }
           public void revokeresign()
           {
            Oelerec.ELE_Case_status__c='Revoke Resignation Requested';
            update Oelerec;   
           }
         /** 
        Class Name   : wrapperclass
        Description  : Wrapper class to get the clearance separation and contact along with attachment
       */
          public class wrapperclass
          {
               public Clearance_separation__c appRec{get;set;}
               public contact con{get;set;}
               public Attachment att{get;set;}
               public wrapperclass(Clearance_separation__c app,Contact con)
               {
                  this.att = new Attachment() ;
                  this.con = con;
                  appRec = app;
                  
               }
          }
            public void fetch() 
            {
              displayList = true;
            }
        
       
}