public class EncryptionHelpController {
    // Remove this class later
    
    public String textToEncrypt {get; set;}
    public String encryptedText {get; set;}
    
    public EncryptionHelpController() {
        textToEncrypt = '';
    }
    
    public void encrypt() {
        if ('' != textToEncrypt) {
            encryptedText = CryptoHelper.encrypt(textToEncrypt);
        }
    }
    
    public void decrypt() {
        if ('' != textToEncrypt) {
            encryptedText = CryptoHelper.decryptURLEncoded(textToEncrypt);
        }
    }
}