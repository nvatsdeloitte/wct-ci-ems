public class Event_checkin_Cont {
    public string searchemail{get;set;}
    public string schoolnotinlist{get;set;}
    public String eid = ApexPages.currentPage().getParameters().get('eid');
    public list<Event_Registration_Attendance__c> evetoupdate; 
    public String tempEid { get; set;}
    Public list<contact> nir_con;
    public string confirstname{get;set;}
    public string conlastname{get;set;}
    public string conemail{get;set;}
    public string condegreetype{get;set;}
    public string conegm{get;set;}
    public string conegy{get;set;}
    public string conmajor{get;set;}
    public decimal congpa{get;set;}
    public string confp{get;set;}
    public string consp{get;set;}
    public string conpi{get;set;}
    public string conschool{get;set;}
    public string conmobile{get;set;}
    public string conlocpf{get;set;}
    public Event__c event{get;set;}
    public Boolean newregp{get;set;}
    public Boolean nirnewregp{get;set;}
    public Boolean confirmp{get;set;}
    public Boolean welcomep{get;set;}
    public Boolean nirwelcomep{get;set;}
    public Boolean mbas{get;set;}
    public Boolean thankyoup{get;set;}
    public Boolean nirthankyoup{get;set;}
    public Boolean existed{get;set;}
    public Boolean nirexisted{get;set;}
    public string confirmuser{get;set;}
    public Contact con{get;set;}
    public string objcontactid='1';
    public integer eventsize;
    public List<Event_Registration_Attendance__c> erlist = new List<Event_Registration_Attendance__c> ();
    public Event_Registration_Attendance__c eventreg{get;set;}
    public Event_Registration_Attendance__c nir_eventreg{get;set;}
    public string school;
    public School__c nschool = new School__c();
    public Boolean atmessaage{get;set;}
    public String selectedsa{get;set;} 

///////////////////////////Construtor///////////////////////////////////////////

     public Event_checkin_Cont(){
         con = new contact();
         nir_con = new list<contact>();   
         tempEid =eid;
         event = new Event__c();
         con = new Contact();
         welcomep=true;
         mbas=false;
         newregp=false;
         confirmp=false;
         thankyoup=false;
         existed=false;
         atmessaage=false;
         eventreg = new Event_Registration_Attendance__c();
         nir_eventreg = new Event_Registration_Attendance__c();
         evetoupdate = new list<Event_Registration_Attendance__c>();
         event=[select id,name,School__c,recordtype.name from event__c where id=:eid];
         
         if(event.recordtype.name =='Consulting')
         con.TRM_correspondingFSS__c='Consulting';
            
         if (event.School__c != null){
             nschool=[select id,name, School_University_Name__c from school__c where Id=:event.school__c limit 1];
             school=nschool.id;
         }
         
    
     }

//////////////////////////Service Area Options for Consulting/Non Consulting Checkin Process /////

        public List<SelectOption> getsaOptions(){
                
          List<SelectOption> optns = new List<Selectoption>();
          List<eEvent_Saoptions__c> statelist = new List<eEvent_Saoptions__c>();
          Map<String,eEvent_Saoptions__c> allpos = eEvent_Saoptions__c.getAll();// custom setting
           Statelist = allpos.values();
          List<SelectOption> options = new List<SelectOption>();
          for (Integer i = 0; i < Statelist.size(); i++) {
            optns.add(new SelectOption(Statelist[i].name, Statelist[i].name));
          }
            optns.sort();
          return optns;
               
        }

////////////////////////Grad Month Values for NIR Checkin Process//////////////////////////////////

        public list<SelectOption> getnirgradmonth(){
         List<SelectOption> optns = new List<SelectOption>();
         Schema.DescribeFieldResult fieldResult =  contact.cr_expected_grad_month__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         optns.add(new SelectOption('--None--', '--None--'));
            for( Schema.PicklistEntry f : ple)
            {
             optns.add(new SelectOption(f.getLabel(), f.getValue()));
            }
           return optns;
         }
        
        
///////////////////////Position of Interest Values for NIR Checkin Process///////////////////////////////
 
           public List<SelectOption> getposofint(){
            List<SelectOption> optns = new List<Selectoption>();
            List<eEvent_Position_of_interest__c> statelist = new List<eEvent_Position_of_interest__c>();
            Map<String,eEvent_Position_of_interest__c> allpos = eEvent_Position_of_interest__c.getAll();// custom setting
             Statelist = allpos.values();
            List<SelectOption> options = new List<SelectOption>();
            for (Integer i = 0; i < Statelist.size(); i++) {
             optns.add(new SelectOption(Statelist[i].name, Statelist[i].name));
            }
             optns.sort();
           return optns;
           }
        
///////////////////////Grad Year Values for NIR Checkin Process/////////////////////////////////////////
       
        public list<SelectOption> getnirgradyear(){
         List<SelectOption> optns = new List<SelectOption>();
          Schema.DescribeFieldResult fieldResult =  contact.cr_expected_grad_year__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         optns.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry f : ple)
        {
         optns.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return optns;
       }
       
///////////////////////Degree Type Values for NIR Checkin Process///////////////////////////////////////

        public list<SelectOption> getnirdegreetype(){
           List<SelectOption> optns = new List<SelectOption>();
           Schema.DescribeFieldResult fieldResult =  contact.Degree_Type__c.getDescribe();
           List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
           optns.add(new SelectOption('--None--', '--None--'));
           for( Schema.PicklistEntry f : ple)
           {
            optns.add(new SelectOption(f.getLabel(), f.getValue()));
           }
          return optns;
         }

//////////////////////Fss Values for NIR Checkin Process/////////////////////////////////////////////////

        public list<SelectOption> getnirfss(){
           List<SelectOption> optns = new List<SelectOption>();
           Schema.DescribeFieldResult fieldResult =  Event_Registration_Attendance__c.fss__c.getDescribe();
           List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
           optns.add(new SelectOption('--None--', '--None--'));
           for( Schema.PicklistEntry f : ple)
           {
            optns.add(new SelectOption(f.getLabel(), f.getValue()));
           }
          return optns;
        }

/////////////////////Service Area Values Depending on Fss NIR Checkin Process///////////////////////////////

       public List<SelectOption> getnirserpre(){
          List<SelectOption> optns = new List<Selectoption>();
         if(confp == 'Deloitte Advisory')
         {
          optns.add(new selectOption('--None--', '--None--'));
          optns.add(new selectOption('Accounting & Finance', 'Accounting & Finance'));
          optns.add(new selectOption('Risk Analytics', 'Risk Analytics'));
          optns.add(new selectOption('Business Risk', 'Business Risk'));
          optns.add(new selectOption('Technology Risk', 'Technology Risk'));
         }
         else 
          if(confp == 'Audit')
         {
         optns.add(new selectOption('N/A', 'N/A'));
         
         }
         else if(confp == 'Enabling Areas')
         {
         optns.add(new selectOption('--None--', '--None--'));
         optns.add(new selectOption('Human Resources', 'Human Resources'));
         optns.add(new selectOption('Internal Technology Services', 'Internal Technology Services'));
         optns.add(new selectOption('Learning and Development', 'Learning and Development'));
         optns.add(new selectOption('Marketing', 'Marketing'));
         optns.add(new selectOption('Talent Acquisition', 'Talent Acquisition'));
         }
         else if(confp == 'Tax')
         {
         optns.add(new selectOption('N/A', 'N/A'));
         }
         else if(confp == 'Consulting')
         {
         optns.add(new selectOption('--None--', '--None--'));
         optns.add(new selectOption('Human Capital', 'Human Capital'));
         optns.add(new selectOption('Technology', 'Technology'));
         optns.add(new selectOption('Strategy & Operations', 'Strategy & Operations'));
         }
         
         
         else
         {
         optns.add(new selectOption('--None--', '--None--'));
         }
        return optns;
       }

/////////////////////Location Preference Values for NIR Checkin Process///////////////////////////////

        public list<SelectOption> getlocpf(){
          List<SelectOption> optns = new List<SelectOption>();
          Schema.DescribeFieldResult fieldResult =  Event_Registration_Attendance__c.location_preference__c.getDescribe();
          List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         optns.add(new SelectOption('--None--', '--None--'));
         for( Schema.PicklistEntry f : ple)
         {
          optns.add(new SelectOption(f.getLabel(), f.getValue()));
         }
        return optns;
       }

/////////////////////////Initial Search Method/////////////////////////////////////////////////////////

        public pagereference search(){
        
             try{
              for(List<Contact> ObjContact :[select Id,Name,firstname,WCT_Candidate_School__c,Email,WCT_School_Name__c,LastName,mobilephone,Location_Preference__c,WCT_CN_Other_Institution__c,Degree_Type__c,CR_Undergrad_Major__c,CR_Expected_Grad_Month__c,CR_Expected_Grad_Year__c,CR_Undergraduate_Overall_GPA_4_00_Scale__c,TRM_serviceArea__c,TRM_correspondingFSS__c from Contact  where email=:searchemail limit 1]){
               for(Contact c : ObjContact) {
                nir_con.add(c);
                if(c.id<>null){
                 objcontactid=c.id;
                }
               }
              }
               if(event.recordtype.name ==label.NIR_Name)
            { 
                if(nir_con.size()>0){
                 nirexisted= true;
                 welcomep=false;
                 conEmail=nir_con[0].email;
                 confirstname=nir_con[0].firstname;
                 conlastname=nir_con[0].lastname;
                 conegy=nir_con[0].CR_Expected_Grad_Year__c;
                 conegm=nir_con[0].CR_Expected_Grad_Month__c;
                 schoolnotinlist=nir_con[0].WCT_Candidate_School__c;
                 condegreetype=nir_con[0].Degree_Type__c;
                 conmajor=nir_con[0].CR_Undergrad_Major__c;
                 congpa=nir_con[0].CR_Undergraduate_Overall_GPA_4_00_Scale__c;
                 conmobile=nir_con[0].mobilephone;
                 con.wct_school_name__c=nir_con[0].WCT_school_name__c;
                 evetoupdate=[select id,name,fss__c,service_area__c,location_preference__c,level__c from Event_Registration_Attendance__c where event__c=:eid and Contact__c=:nir_con[0].id];
                 
                 if(evetoupdate.size()>0){
                 system.debug('FSS*************'+evetoupdate[0].FSS__c+'Service**************'+evetoupdate[0].service_Area__c);
                  confp=evetoupdate[0].FSS__c;
                  consp=evetoupdate[0].service_Area__c;
                  conlocpf=evetoupdate[0].Location_Preference__c;
                  conpi=evetoupdate[0].level__c;
                 }
                }
                
                else{
                welcomep=false;
                nirnewregp=true;
                con.Email=searchemail;
                }
                
            }   else{
                con.Email=searchemail;
               
                if(objcontactid<>'1')
               {
                 eventreg=[SELECT id,Name,Attendee_Type__c,Event__c,Contact__c,Attending__c,Not_Attending__c,invited__C from Event_Registration_Attendance__c where event__c=:eid and Contact__c=:objcontactid limit 1];
                 erlist=[select id,name from Event_Registration_Attendance__c where event__c=:eid and Contact__c=:objcontactid];
                 confirmp=true;
                 welcomep=false;
                 newregp=false;
                 eventsize=erlist.size();
                }
               
                else {
                 newregp=true;
                 confirmp=false;
                 welcomep=false;
                }
              }
               }
             
             catch(System.QueryException e)
             {
              existed=true;
              newregp=false;
              confirmp=false;
              welcomep=false;
             }
            return null;
           
          }
          
/////////////////////////Method to Update Existing Contact///////////////////////////////////////////          
          
                public pagereference checkinconfirm(){
                 try{
                  eventReg.Attended__C=true;
                  eventReg.Not_Attending__c=false;
                  eventReg.Attending__c=true;
                  update eventreg;
                  thankyoup=true;
                  confirmp=false;
                  atmessaage=false;
                 }
                 catch(System.QueryException e){
                 }
                return null;
               }
        
///////////////////////////Method to Upsert Contact and Event Registration Attendance details////////////////////////////        
        
       public pagereference existedcontact(){
        if(event.recordtype.name ==label.NIR_Name)
         {
          Event_Registration_Attendance__c everegtoupdat = new Event_Registration_Attendance__c();
          contact contoupdate = new contact();
          pagereference p= new pagereference(label.eEvents_thankyoupage+eid); /// label
          try{
           RecordType RecordTypenirid = new RecordType();
           RecordTypenirid =[select id,name from RecordType where sobjecttype = 'Event_Registration_Attendance__c' and name=:label.NIR_Name ];
           list<Event_Registration_Attendance__c> nireventreg = new list<Event_Registration_Attendance__c>();
           nireventreg=[select id,name from Event_Registration_Attendance__c where event__c=:eid and Contact__c=:nir_con[0].id];
           contoupdate.Email=conemail;
           contoupdate.firstname=confirstname;
           contoupdate.lastname=conlastname;
           contoupdate.WCT_Candidate_School__c =schoolnotinlist;
           if(conegy =='--None--')
           conegy=null;
           contoupdate.CR_Expected_Grad_Year__c=conegy;
           if(conegm =='--None--')
           conegm=null;
           contoupdate.CR_Expected_Grad_Month__c=conegm;
           contoupdate.CR_Undergrad_Major__c=conmajor;
           contoupdate.CR_Undergraduate_Overall_GPA_4_00_Scale__c=congpa;
           if(condegreetype =='--None--')
           condegreetype=null;
           contoupdate.Degree_Type__c=condegreetype;
           if(confp =='--None--')
           confp=null;
           everegtoupdat.FSS__c=confp;
           if(consp =='--None--')
           consp=null;
           everegtoupdat.service_Area__c=consp;
           contoupdate.id=nir_con[0].id;
           contoupdate.mobilephone=conmobile;
           //contoupdate.wct_candidate_school__c='';
           contoupdate.wct_school_name__c=con.wct_school_name__c;
           if(conlocpf =='--None--')
           conlocpf=null;
           everegtoupdat.Location_Preference__c=conlocpf;
           if(conpi =='--None--')
           conpi=null;
           everegtoupdat.level__c=conpi;
           everegtoupdat.Attended__c=true;
      
           if(nireventreg.size()>0){
            everegtoupdat.id=nireventreg[0].id;
            update everegtoupdat;
            update contoupdate;
           }
           
           else  
           {
            update contoupdate;
            eventreg.level__c=conpi;
            eventreg.recordtypeid=RecordTypenirid.id;
            eventreg.fss__c=confp;
            eventreg.service_area__c=consp;
            eventreg.Location_preference__c=conlocpf;
            eventReg.Contact__c=nir_con[0].id;
            eventReg.Event__c=eid;
            eventReg.Attended__c=true;
            eventReg.Not_Attending__c=false;
            Insert eventReg;
           }
         }
          catch(System.QueryException e){
           }
         return p;
        }
   
         else {
          try{
           eventReg.Contact__c=objcontactid;
           eventReg.Event__c=eid;
           eventReg.Attended__c=true;
           eventReg.Not_Attending__c=false;
           upsert eventReg;
           thankyoup=true;
           confirmp=false;
           newregp=false;
           existed=false;
           atmessaage=false;
          }
          catch(System.QueryException e){
          }
         return null;
        }
       }
        
////////////////////////////Method to return to Checkin Page After Transaction///////////////////////////////////////////////
        
        public pagereference returntocheckin()
        {
          pagereference p= new pagereference(label.eEvents_Url+eid);
          return p;
        }

///////////////////////////Method to Insert New Contact and Event Registration Attendance///////////////////////////////////

        public pagereference newregistration(){
          
          pagereference p= new pagereference(label.eEvents_thankyoupage+eid);
          RecordType RecordTypenirid = new RecordType();
          RecordTypenirid =[select id,name from RecordType where sobjecttype = 'Event_Registration_Attendance__c' and name =: system.label.NIR_Name ];
          List<RecordType> RecordTypeids = new List<RecordType>();
          RecordTypeids =[select id,name from RecordType where sobjecttype = 'Contact' and name='Candidate Ad Hoc' limit 1];
         
         if(event.recordtype.name ==label.NIR_Name)
         {
         
         try{    
          
          con.recordtypeid=RecordTypeids[0].id;
          //con.WCT_School_Name__c=school;
          con.WCT_Candidate_School__c =schoolnotinlist;
          con.teams__c=label.NIR_Name;  
          Insert con;
          eventreg.recordtypeid=RecordTypenirid.id;
          eventreg.level__c=conpi;
          eventreg.Contact__c=Con.id;
          eventreg.Event__c=eid;
          eventreg.Attended__c=true;
          eventReg.Attending__c=true;
          eventReg.Not_Attending__c=false;
          Insert eventreg;
          thankyoup=true;
          confirmp=false;
          nirnewregp=false;
          atmessaage=false;
         }
         catch(System.QueryException e){
         }
        return p;
       }
         else{
           
          try{    
           
           con.recordtypeid=RecordTypeids[0].id;
           con.WCT_School_Name__c=school;
           if(con.Degree_Type__c =='MBA' && event.recordtype.name =='Consulting'){
            con.TRM_correspondingFSS__c='Consulting';
            if(selectedsa =='--None--')
           selectedsa=null;
            con.TRM_serviceArea__c=selectedsa;
           }
           Insert con;
           if(eventsize<0)
           {
            eventreg.Contact__c=objcontactid;
           }
           else
           {
            eventreg.Contact__c=Con.id;
           }
           eventreg.Event__c=eid;
           eventreg.Attended__c=true;
           eventReg.Attending__c=true;
           eventReg.Not_Attending__c=false;
           Insert eventreg;
           thankyoup=true;
           confirmp=false;
           newregp=false;
           atmessaage=false;
          }
          catch(System.QueryException e){
          }
         return p;
        }
       }
      }