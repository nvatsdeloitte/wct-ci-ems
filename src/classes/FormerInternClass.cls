global class FormerInternClass {
   public string FirstName {get;set;}
   public string LastName {get;set;}
   public string EmailAddress {get;set;}
   public string Office  {get;set;}
   public string RSVPStatus {get;set;}
   public string Acptdec {get;set;}
   public string Address1 {get;set;}
   public string Address2 {get;set;}
   public string City {get;set;}
   public string State {get;set;}
   public list<Former_Intern_Office__c> Statelist;
   public string Zip {get;set;}
   public string AccountType {get;set;}
   public string RoutingNumber {get;set;}
   public string phone {get;set;}
   public string AccountNumber {get;set;}
   public string BankName {get;set;}
   public Contact cnt;
   public Former_Intern__c frmInt{get;set;}
   public boolean BlockpwdVisibility{get;set;}
   public boolean Block1Visibility{get;set;}
   public boolean Block2Visibility{get;set;}
   public boolean Block3Visibility{get;set;}
   public boolean Block4Visibility{get;set;}
   public boolean SubmitPg1Enable{get;set;}
   public boolean RecordExist{get;set;}
   public boolean isLoginValidated{get;set;}
   public string PaymentMode;
   public string items;
   public string Eventid;
   public Event__c evnt;
   public string entname{get;set;}
   public string tempdecryptemail{get;set;}
   public list<Former_Intern__c> frmICh=new list<Former_Intern__c>();
   public boolean isSubmitted{get; set;}
   public integer submitted;
   public string  userMessage{get; set;}
   public string password{get;set;}  
   public string passwordcmp{get;set;}  
   public string frmid;
   public string rsst;
    public string test{get;set;}
   public FormerInternClass()
   {
       /*Temperary Contact list object to hold the query result*/
    isLoginValidated=false;
     isSubmitted=false;
     userMessage='Registration is now closed.';
     list<contact> tempCandList =new list<contact>(); 
     tempdecryptemail=apexpages.currentPage().getParameters().get('em');
     frmid=apexpages.currentPage().getParameters().get('frmid');
     rsst=apexpages.currentPage().getParameters().get('rsst');
     EmailAddress=CryptoHelper.decrypt(tempdecryptemail);
     passwordcmp=EmailFormerInternController.generatepwd(EmailAddress);
     Eventid=apexpages.currentPage().getParameters().get('id');
       evnt=[select id,Name,End_Date_Time__c from Event__c where id=:Eventid];
       entname=evnt.Name;
       BlockpwdVisibility=true;
       Block1Visibility=false;
       Block2Visibility=false;
       Block3Visibility=false;
       Block4Visibility=false;
       
       if(evnt.End_Date_Time__c<datetime.now())
       { 
        BlockpwdVisibility=false;
        Block1Visibility=false;
        Block3Visibility=true;
       }
       else{
     tempCandList=[Select id,FirstName,LastName,phone,WCT_Home_City__c,WCT_Home_Office__c,WCT_Home_Street__c,WCT_Home_Street_2__c,WCT_Home_State__c,WCT_Home_Zip__c,Email from contact where (Email=:EmailAddress or WCT_ExternalEmail__c=:EmailAddress) and ((WCT_Contact_Type__c='Employee' and WCT_Type__c='Separated') or recordtypeid=:label.FormerInternRecordtypeID) order by createddate desc limit 1]; 
       if(tempCandList.size()>0)
     {
       frmICh=[Select id,Bank_Name__c,Routing_Number__c,Account_Number__c,Account_Type__c,Payment_Mode__c,RSVP_Status__c  from Former_Intern__c where Deloitte_Event__c=:evnt.id and Former_Intern_Emp__c=:tempCandList[0].id order by createddate desc limit 1];
    //check for empty former intrn records
         if(frmICh.size()>0)
       {
           if(string.valueof(frmICh[0].id).substring(0, 15) ==frmid && frmICh[0].RSVP_Status__c=='Not Attending' && rsst=='NotAttending')
           {
            BlockpwdVisibility=true;   
            Block1Visibility=false;
            Block2Visibility=false;
            isSubmitted=true;
           }
           else
           {
            BlockpwdVisibility=false;   
            Block1Visibility=false;
            Block2Visibility=true;
           }
       }
        cnt=tempCandList[0];
        FirstName=cnt.FirstName;
        LastName=cnt.LastName;
        phone=cnt.Phone;
        RecordExist=true;
    }else
    {
        /*when no existing contact initialize the contact to fetch the data from user. */
        cnt= new Contact();
        cnt.Email=EmailAddress;
        cnt.WCT_ExternalEmail__c=EmailAddress; 
        cnt.RecordTypeId=label.FormerInternRecordtypeID;
        cnt.WCT_Is_Separated__c=true;
        //cnt.WCT_Contact_Type__c='Employee';
        cnt.WCT_Employee_Status__c='withdrawn';
        cnt.WCT_Employee_Group__c='separated';
        cnt.WCT_Region__c='EAST';
        //cnt.WCT_Contact_Type__c='Employee';
        RecordExist=false;
    }
     
       }  
   }
   public void formerInternCreate()
   {
       if(!RecordExist)
       {
        cnt.FirstName=FirstName;
        cnt.LastName=LastName;
        cnt.Email=EmailAddress;
        cnt.WCT_ExternalEmail__c=EmailAddress;
        cnt.Phone=string.valueOf(Phone);
        insert cnt;
       }
      if(frmICh.size()>0)
      {
       if(RSVPStatus=='Attending' && PaymentMode=='My pay check should be deposited into the following account:')
       {
       frmICh[0].Account_Type__c=AccountType;
       frmICh[0].Routing_Number__c=RoutingNumber;
       frmICh[0].Account_Number__c=AccountNumber;
       frmICh[0].Bank_Name__c=BankName;
       frmICh[0].Payment_Mode__c='My pay check should be deposited into the following account';
       }
       else if(RSVPStatus=='Attending' && PaymentMode=='Please issue a check to the address entered on this page.')
       {
        frmICh[0].City__c=City;
        frmICh[0].Internship_OfficeNew__c=Office;
        frmICh[0].Address_1__c=Address1;
        frmICh[0].Address_2__c=Address2;
        frmICh[0].State__c=State;
        frmICh[0].Zip__c=Zip; 
        frmICh[0].Payment_Mode__c='Please issue a check to the address entered';
       }
       frmICh[0].Offer_Status__c=Acptdec;
       frmICh[0].RSVP_Status__c=RSVPStatus; 
          update frmICh[0];
      }
       else{
       frmInt=new Former_Intern__c();
       
       if(RSVPStatus=='Attending' && PaymentMode=='My pay check should be deposited into the following account:')
       {
       frmInt.Account_Type__c=AccountType;
       frmInt.Routing_Number__c=RoutingNumber;
       frmInt.Account_Number__c=AccountNumber;
       frmInt.Bank_Name__c=BankName;
       frmInt.Payment_Mode__c='My pay check should be deposited into the following account';
       }
       else if(RSVPStatus=='Attending' && PaymentMode=='Please issue a check to the address entered on this page.')
       {
        frmInt.City__c=City;
        frmInt.Internship_OfficeNew__c=Office;
        frmInt.Address_1__c=Address1;
        frmInt.Address_2__c=Address2;
        frmInt.State__c=State;
        frmInt.Zip__c=Zip; 
        frmInt.Payment_Mode__c='Please issue a check to the address entered';
       }
       frmInt.Offer_Status__c=Acptdec;
       frmInt.RSVP_Status__c=RSVPStatus;
       frmInt.Former_Intern_Emp__c=cnt.id;
       frmInt.Deloitte_Event__c=Eventid;
       insert frmInt;
          }
       
   }
    public pagereference next()
    {
        Block1Visibility=false;
        return null;
    }
    public pagereference back()
    {
        Block1Visibility=true;
        return null;
    }
    public pagereference submit()
    {
        //frmICh=[Select id from Former_Intern__c where Deloitte_Event__c=:evnt.id and Former_Intern_Emp__c=:cnt.id];
        formerInternCreate();
        BlockpwdVisibility=false;
        Block3Visibility=true;
        Block1Visibility=false;
        return null; 
    }
    public pagereference login()
    {
     
        if(password==passwordcmp)
        {
            Block1Visibility=true;
            BlockpwdVisibility=false;
            isLoginValidated=true;
        }
        else
        {
            Block1Visibility=false;
            BlockpwdVisibility=true;
            isLoginValidated=false;
            
        }
       //return pageRef;
       return null;        
    }
       public pagereference dummy()
    {
       // Block1Visibility=true;
        return null;
    }
    public pagereference disableEnable()
    {
        if(RSVPStatus=='Yes')
        SubmitPg1Enable=false;
        else
        SubmitPg1Enable=true;    
        return null;
    }
     public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('My pay check should be deposited into the following account:','My pay check should be deposited into the following account:')); 
        options.add(new SelectOption('Please issue a check to the address entered on this page.','Please issue a check to the address entered on this page.')); 
        return options; 
    }
     public String getPaymentMode() {
        return PaymentMode;
    }
    public void setPaymentMode(String PaymentMode) { this.PaymentMode = PaymentMode; }
       public List<SelectOption> getItemsRSVP() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Attending','Attending')); 
        options.add(new SelectOption('Not Attending','Not Attending')); 
        return options; 
    }
     public String getRSVPStatus() {
        return RSVPStatus;
    }
    public void setRSVPStatus(String PaymentMode) { this.RSVPStatus = RSVPStatus; }
     public List<SelectOption> getStatelist() {
      Map<String,Former_Intern_Office__c> allStates = Former_Intern_Office__c.getAll();
      Statelist = allStates.values();
        List<SelectOption> options = new List<SelectOption>();
        for (Integer i = 0; i < Statelist.size(); i++) {
            options.add(new SelectOption(Statelist[i].name, Statelist[i].name));
        }
         options.sort();
        return options;
    }
          public List<SelectOption> getItemsAcptdec() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Accept','Accept')); 
        options.add(new SelectOption('Decline','Decline')); 
        return options; 
    }
      public String getAcptdec() {
        return Acptdec;
    }
    
    
    
   
}