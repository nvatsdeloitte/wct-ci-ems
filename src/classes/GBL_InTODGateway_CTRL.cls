/*
Apex Class: GBL_InTODGateway_CTRL
CreatedOn: 14/04/2015
Author: Balu(deloitte)

*/
public with sharing class GBL_InTODGateway_CTRL{


public GBL_InTODGateway_CTRL(){


}
public PageReference goTalantOnDemand() {
   PageReference objPageRef=null; 
   string Key= null;
   string em=null;
   string URL=null;
   string Param1=null;
   string Param2=null;
   string Param3=null;
   string Param4=null;
   string Param5=null;
   string encryptedText=null;
   try{
   Key=apexpages.currentpage().getparameters().get('Key') ;
   em=apexpages.currentpage().getparameters().get('em') ; 
   Param1=apexpages.currentpage().getparameters().get('Param1') ;
   Param2=apexpages.currentpage().getparameters().get('Param2') ;
   Param3=apexpages.currentpage().getparameters().get('Param3') ;
   Param4=apexpages.currentpage().getparameters().get('Param4') ;
   Param5=apexpages.currentpage().getparameters().get('Param5') ;
   if(Test.IsRunningTest()){
    Key='TDR';
    em='Testingdeloitte.com';
    Param1='Test';
    Param2='Test';
    Param3='Test';
    Param4='Test';
    Param5='Test';
    encryptedText='Test';  
   }else{
   em=EncodingUtil.urlEncode(em, 'UTF-8');
   system.debug('----em-----'+em);
   encryptedText = CryptoHelper.decryptURLEncoded(em);
   //encryptedText=encryptedText.substring(3,encryptedText.Length());
   encryptedText = CryptoHelper.encrypt(encryptedText);     
   }
   em=encryptedText;
   List<TOD_System_Links__c> lstTODPref=new List<TOD_System_Links__c>();
   lstTODPref=[select Id,Name,Request_URL__c from TOD_System_Links__c where Name=:Key];
   system.debug('----lstTODPref-----'+lstTODPref);
   if(!lstTODPref.IsEmpty())
   {
   URL=lstTODPref[0].Request_URL__c;
   } 
   if(string.IsNotEmpty(em) && string.IsNotEmpty(URL)){
    string ReturnURL=null;
    ReturnURL=URL+em;
    if(string.IsNotEmpty(Param1)){
    ReturnURL+='&Param1='+Param1;
    }
    if(string.IsNotEmpty(Param2)){
    ReturnURL+='&Param2='+Param2;
    }
    if(string.IsNotEmpty(Param3)){
    ReturnURL+='&Param3='+Param3;
    }
    if(string.IsNotEmpty(Param4)){
    ReturnURL+='&Param4='+Param4;
    }
    if(string.IsNotEmpty(Param5)){
    ReturnURL+='&Param5='+Param5;
    }

    objPageRef = new PageReference(ReturnURL);
    objPageRef.setRedirect(true);
    system.debug('----ReturnURL-----'+ReturnURL);
    }   
    }
    catch(Exception ex){
    system.debug('----Exception----'+ex.getMessage()+'----at Line #---'+ex.getLineNumber());
    }
    return objPageRef;   
    }
}