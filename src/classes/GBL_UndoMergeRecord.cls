public class GBL_UndoMergeRecord {

   public  string staggingId;
   public WCT_sObject_Staging_Records__c stagginRecord{get; set;}
   public boolean isError{get;set;}
   public string  errorMessage{get;set;}
   public set<string>  fieldsToQuery{get;set;}
   public Map<string, object> sourceBackup {get;set;}
   public Map<string, object> destBackup {get;set;}
   public sObject sourceRecord;
   public sObject destRecord;
   public boolean isValid=false;
   public string processActivities;
    public boolean mergeSuccesful{get;set;}
    
   
   public  GBL_UndoMergeRecord()
    {
         /*Initalization of Variables*/
        isError=false;
        mergeSuccesful=false;
        processActivities='\n'+String.valueOfGmt(System.now())+' Undo process Started \n';
        errorMessage='';
        sourceBackup= new Map<string, object>();
        destBackup= new Map<string, object>();
        fieldsToQuery= new Set<String>();
        staggingId = ApexPages.currentPage().getParameters().get('staggingId');
        if(staggingId!=null && staggingId!='')
        {
          queryAndValidateStaggingRecord(staggingId);
        }
        else
        {
            isError=true;
            errorMessage='Invalid Request';
        }
        
    }
    
    public  void queryAndValidateStaggingRecord(string staggingId)
    {
        List<WCT_sObject_Staging_Records__c> records=[select id, MR_Object_Type__c,MR_Source_Record_Id__c,MR_Destination_Record_Id__c, MR_Source_Record_Backup__c,MR_Destination_Record_Backup__c, MR_Related_List_Backup__c, MR_Future_Related_List_backup__c,MR_Merged_Status__c, MR_Merged_Date__c,MR_Activity_History__c,MR_Source_Record_Name__c,MR_Destination_Record_Name__c, CreatedBy.Name From WCT_sObject_Staging_Records__c where id=:staggingId ];
        boolean isValid=records.size()==1?records[0].MR_Merged_Status__c=='Merged':false;
        if(isValid)
        {
          	stagginRecord=records[0];
            try
            {
                sourceBackup=GBL_UndoMergeRecord.parseJson(stagginRecord.MR_Source_Record_Backup__c);
                destBackup=GBL_UndoMergeRecord.parseJson(stagginRecord.MR_Destination_Record_Backup__c);
            }
            catch(Exception e)
            {
               isError=true;
               processActivities=processActivities+' Error : Exception while parsing the backup json string '+e.getMessage()+'\n'; 
            }
            fieldsToQuery.addAll(sourceBackup.keySet());
            fieldsToQuery.addAll(destBackup.keySet());
            
            if(fieldsToQuery.size()>0)
            {
                querysObject();
                if(isValid==false)
                {
                 processActivities=processActivities+' Error : No backup data to perform undo operation \n'; 
                 errorMessage='Cant perform the Undo operation as the required record for this operation are unavailable.';
                 isError=true;
                }
            }
            else
            {
                 processActivities=processActivities+' Error : No backup data to perform undo operation \n'; 
                 errorMessage='Noting to undo.';
                 isError=true;
            }
        }
        else
        {
            isError=true;
            processActivities=processActivities+' Error : Invalid Stagging Record \n';
        	errorMessage='Invalid Request Or undo operation is already performed.';
        }
    }
    public PageReference updateStaggingRecord()
    {
        if(stagginRecord!=null)
        {
            stagginRecord.MR_Activity_History__c=stagginRecord.MR_Activity_History__c+processActivities;
            processActivities='';
            update stagginRecord;
        }
        return null;
    }
    public static Map<string, object> parseJson(string json_string)
    {
        Map<string, object> jsonMap= new Map<string, object>();
      
         system.JSONParser jsonParser= JSON.createParser(json_string);
         while (jsonParser.nextToken() != null) {
             // system.debug(' Field Name source '+(sourceParser.getCurrentName())+'::'+ sourceParser.getText()+'  Token Tyope'+sourceParser.getCurrentToken());
        	if(jsonParser.getCurrentToken()!=JSONToken.END_OBJECT && jsonParser.getCurrentToken()!=JSONToken.START_OBJECT)
              {
                  if(jsonParser.getCurrentToken()==JSONToken.FIELD_NAME)
                  {
                      jsonParser.nextToken();
                  }
                  if(jsonParser.getCurrentToken()==JSONToken.VALUE_NULL)
                  {
                    jsonMap.put(jsonParser.getCurrentName(), null);  
                  }
                  else if(jsonParser.getCurrentToken()==JSONToken.VALUE_FALSE || jsonParser.getCurrentToken()==JSONToken.VALUE_TRUE)
                  {
                    jsonMap.put(jsonParser.getCurrentName(), jsonParser.getBooleanValue());    
                  }
                  else if(jsonParser.getCurrentToken()==JSONToken.VALUE_NUMBER_INT)
                  {
                    jsonMap.put(jsonParser.getCurrentName(), jsonParser.getIntegerValue());      
                  }
                  else if(jsonParser.getCurrentToken()==JSONToken.VALUE_NUMBER_FLOAT)
                  {
                    jsonMap.put(jsonParser.getCurrentName(), jsonParser.getDecimalValue());      
                  }
                  else
                  {
                     jsonMap.put(jsonParser.getCurrentName(), jsonParser.getText()); 
                  }
             }
			}
        
        return jsonMap;
    }
    
    public void querysObject()
    {
        List<sobject> sobjects= new List<sobject>();
        string dynamicQuery='Select id';
        dynamicQuery=dynamicQuery+AR_Merge_Record_HDLR.setToCSV(fieldsToQuery);
        dynamicQuery=dynamicQuery+' From '+stagginRecord.MR_Object_Type__c;
        dynamicQuery=dynamicQuery+' Where id in ( \''+stagginRecord.MR_Source_Record_Id__c+'\',\''+stagginRecord.MR_Destination_Record_Id__c+'\')';
        system.debug('###########Query For Source And DEst REcords'+dynamicQuery);
        sobjects = Database.query(dynamicQuery);
        
           for(sObject tempObject : sobjects)
           {
               if(tempObject.get('id')==stagginRecord.MR_Source_Record_Id__c) sourceRecord=tempObject;
               if(tempObject.get('id')==stagginRecord.MR_Destination_Record_Id__c) destRecord=tempObject;
           }
        if(sobjects.size()==2) isValid=true;      
    }
    
    public PageReference undoMerge()
    {
        if(isValid)
        {
            List<sObject> sobjects= new List<sObject>();
            if(sourceRecord!=null)
            {
                for(string field: sourceBackup.keySet())
                {
                   sourceRecord.put(field, sourceBackup.get(field));
                }
                sobjects.add(sourceRecord);
            }
            if(destRecord!=null)
            {
                for(string field: destBackup.keySet())
                {
                   destRecord.put(field, destBackup.get(field));
                }
                sobjects.add(destRecord);
            }
            
            if(sobjects.size()>0)
            {
                update sobjects;
                processActivities=processActivities+' STEP : source and destination records are updated. \n';
            }
            
           parseAndUpdateRelatedList(stagginRecord.MR_Related_List_Backup__c, stagginRecord.MR_Source_Record_Id__c);
            processActivities=processActivities+' STEP : Related List is undo  \n';
            
           /*Calling the future method to handle the related list backup which was handled bu future in merge process.*/
           if(!string.isEmpty(stagginRecord.MR_Future_Related_List_backup__c) )
           {
               processActivities=processActivities+' STEP : future context Related List for  undo is intiated.\n';
           		futureUpdateRelatedList(stagginRecord.MR_Future_Related_List_backup__c, stagginRecord.MR_Source_Record_Id__c,stagginRecord.Id );
           }
           //stagginRecord.MR_Undo_Error__c=stagginRecord.MR_Undo_Error__c+processError;
           stagginRecord.MR_Activity_History__c=stagginRecord.MR_Activity_History__c+processActivities;
           stagginRecord.MR_Undo_Date__c=system.now();
           stagginRecord.MR_Merged_Status__c='Undone';
           update stagginRecord;
            
           mergeSuccesful=true;
  		}
        return null;
    }
	
    @future
    public static void futureUpdateRelatedList(string backupString, string sourceId, string staggingRecordId )
    {
        string processError=parseAndUpdateRelatedList(backupString, sourceId);
        List<WCT_sObject_Staging_Records__c> records=[select id, MR_Activity_History__c From WCT_sObject_Staging_Records__c where id=:staggingRecordId ];
       if(records.size()>0)
       {
           records[0].MR_Activity_History__c=records[0].MR_Activity_History__c+' STEP : future context related list undone is done. /n';
       }
        
    }
    public static string parseAndUpdateRelatedList(string backupString, string sourceId)
    {
        system.debug('### Backup string '+backupString);
        string processError='';
        List<sObject> lists = new List<sObject>();
        if(!string.isEmpty(backupString))
        {
            try
            {
        JSONParser parser = JSON.createParser(backupString);
		If(parser.nextToken()!=null && parser.getCurrentToken()==JSONToken.START_ARRAY)
        {
            system.debug('## if 1'+parser.getCurrentToken());
            system.debug(parser.getCurrentName()+'##'+parser.getText());
            
            
            /*Repeat Until the all the object*/
            while(parser.nextToken()!=null && parser.getCurrentToken()!=JSONToken.END_ARRAY)
            {
                system.debug(' ## while 1'+( parser.getCurrentToken()));
                
                /*Check if current token is Start Obejct*/
              if(parser.getCurrentToken()==JSONToken.START_OBJECT)
              {
                  system.debug(' ## if 2'+parser.getCurrentToken());
                  string objectName=null;
                  string apifieldName=null;
                  List<string> ids =new List<String>();
                  
                  
                  /*Repeat for all token until the current object end */
                  while(parser.nextToken()!=null && parser.getCurrentToken()!=JSONToken.END_OBJECT)
                  {
                      system.debug(' ## while 2'+parser.getCurrentToken());
                      /*If current token is objectName*/
                     if(parser.getCurrentToken()==JSONToken.FIELD_NAME && parser.getText()=='objectName')
                      {
                         parser.nextToken();
                         objectName= parser.getText();
                      } 
                      /*If current token is apiFieldName*/
                      if( parser.getCurrentToken()==JSONToken.FIELD_NAME && parser.getText()=='apiFieldName')
                      {
                         parser.nextToken();
                         apifieldName= parser.getText();
                      } 
                      /*If current token is idValue : list od ids*/
                      if(parser.getCurrentToken()==JSONToken.FIELD_NAME && parser.getText()=='idValue')
                      {
                         if(parser.nextToken()!=null && parser.getCurrentToken()==JSONToken.START_ARRAY)
                         {
                             while(parser.nextToken()!=null && parser.getCurrentToken()!=JSONToken.END_ARRAY)
                             {
                                // parser.nextToken();
                                 ids.add(parser.getText());
                             }
                         }
                      } 
                  }
                system.debug(' ## where is my object '+objectName+' :: '+apifieldName);
                  /*After the end of each object create the sobject based on the current object details*/
                  if(objectName!=null && apifieldName!=null)
                  {
                      for(string idTemp : ids)
                      {
                          sObject tempObject=null;
                          /*check if same id record already present in the list. This is for the scenarios where object has two related list of same object and their are chnaces of same record to be present in two different related List  */
                          for(sObject existingObject : lists)
                          {
                              system.debug(''+(existingObject.id==idTemp));
                              if(existingObject.id==idTemp)
                              {
                                 tempObject=existingObject;
                                 tempObject.put(apifieldName, sourceId);
                              }
                          }
						 /*If record already not present in the list*/
                          if(tempObject==null)
                          {
                              tempObject=Schema.getGlobalDescribe().get(objectName).newSObject();
                          	  tempObject.id=idTemp;
                              tempObject.put(apifieldName, sourceId);
                          	  lists.add(tempObject);
                          }
                      }
                  }
             } // END of if --> Start Object 
                
            } // END of while  untill--> END Array  
            } // END of if  ->   Start Array - main.
            }
            catch(Exception e)
            {
                processError=processError+'\n'+e.getLineNumber()+' :: '+e.getMessage()+' :: '+e.getStackTraceString();
            }
        }
        system.debug('## lists: '+lists);
        if(lists.size()>0)
        {
            Database.update(lists);
        }
        return processError;
    }
}