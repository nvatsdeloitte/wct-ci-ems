public with sharing class InTalentOnDemand{

public string reqtype=null;


public InTalentOnDemand(){

reqtype=''; // assign Reqtype here to get prepared appropriate request form;

}


  public PageReference goTalantOnDemand() {
   try{
   string Key= null;
   string em=null;
   string URL=null;
   if(Test.IsRunningTest()){
    Key='TDR';
    em='Test';
   }
   else{
   Key=apexpages.currentpage().getparameters().get('Key') ;
   em=apexpages.currentpage().getparameters().get('em') ;

   }
   List<TalentOnDemand_System_Preferences__c> lstTODPref=new List<TalentOnDemand_System_Preferences__c>();
   lstTODPref=[select Id,Name,Request_URL__c from TalentOnDemand_System_Preferences__c where Name=:Key];
   system.debug('----lstTODPref-----'+lstTODPref);
   if(!lstTODPref.IsEmpty())
   {
   
   URL=lstTODPref[0].Request_URL__c;
   
   } 
   
   if(string.IsNotEmpty(em) && string.IsNotEmpty(URL)){
   
    string ReturnURL=null;
    ReturnURL=URL+em;
    if(string.IsNotEmpty(reqtype)){
    ReturnURL+='&reqtype='+reqtype;
    }
    PageReference objPageRef = new PageReference(ReturnURL);
    objPageRef.setRedirect(true);
    system.debug('----ReturnURL-----'+ReturnURL);
    return objPageRef;
    }
    else
    {
       return null;
    }
    }
    catch(Exception ex){
    system.debug('----Exception----'+ex.getMessage()+'----at Line #---'+ex.getLineNumber());
    return null;
    }
    }

}