global class IncrementReport implements Schedulable {
  string reportName {get;set;}  
  global IncrementReport(string rptName){
    reportName  = rptName;
  }

  global void execute(SchedulableContext ctx) {
      System.debug('Entered Cron trigger');
      Schedule_Report__c r = [SELECT ReportId__c, WCT_Report_Name_with_CSV__c,ReportName__c, ReportTrigger__c FROM Schedule_Report__c where ReportName__c = :reportName LIMIT 1];
      r.ReportTrigger__c += 1;
      System.debug('updating trigger to: ' + r.ReportTrigger__c);
      update r;
      }   
}