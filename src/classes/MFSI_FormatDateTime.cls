public class MFSI_FormatDateTime {
    public DateTime dateTimeValueSt { get; set; }
    public DateTime dateTimeValueEnd { get; set; }
    public String getTimeZoneValue() {
        if( dateTimeValueSt != null ) {
            String localeFormatDT = dateTimeValueSt.format('MM/dd/yyyy hh:mm a');
            return localeFormatDT;
        }
        if( dateTimeValueEnd != null ) {
            String localeFormatDT = dateTimeValueEnd.format('hh:mm a');
            return localeFormatDT;
        }
        return null;
    }
}