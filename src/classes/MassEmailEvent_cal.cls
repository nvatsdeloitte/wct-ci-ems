/*****************************************************************************************
    Name    : MassEmailEvent_cal
    Desc    : 
    Approach: For adding multiple campaign members to campaign Lite
    Modification Log : 
---------------------------------------------------------------------------
Developer              Date            Descrilicption
---------------------------------------------------------------------------
  Rajasekhara Reddy Yeturi      09/21/2013 
******************************************************************************************/
public class MassEmailEvent_cal {
public List<Event_Registration_Attendance__c> conts {get; set;}
public Boolean displaypblock{get;set;}
public string emailTemplateId{get;set;}
Public list<id> contactids = new List<Id>();
Public List<Contact> selectedContacts = new List<Contact>();
private List<EmailTemplate> allEmailTemplates;
public string eventname;
    private List<Folder> allEmailTemplateFolders;
    public String selectedEmailTemplateFolder {get
    {
        if(selectedEmailTemplateFolder <> null && selectedEmailTemplateFolder <> '')
        {
            return selectedEmailTemplateFolder ;
        }
        else{
            selectedEmailTemplateFolder = '';
            return selectedEmailTemplateFolder ;
        }
    } set;}
    public String selectedEmailTemplate {
    
    get{
        if(selectedEmailTemplate <> null && selectedEmailTemplate <> '')
        {
            return selectedEmailTemplate ;
        }
        else{
            selectedEmailTemplate = '';
            return selectedEmailTemplate ;
        }
    }
     set;
    
    }
    public EmailTemplate chosenEmailTemplate {public get; private set;}   
   
    public List<SelectOption> getEmailTemplateFolderOpts() {
        List<SelectOption> opts = new List<SelectOption>();
        opts.add(new SelectOption('null', ' - Select - '));
        System.debug('=====>'+opts);
        for ( Folder f : allEmailTemplateFolders )
            opts.add(new SelectOption(f.Id, f.Name));
        // add an option for templates in the Unfiled Public folder
        opts.add(new SelectOption(UserInfo.getOrganizationId(), 'Unfiled Public'));
        opts.add(new SelectOption(UserInfo.getUserId(), 'My Personal Email Templates'));
        return opts;
    }
    public List<SelectOption> getEmailTemplateOpts() {
         List<SelectOption> opts = new List<SelectOption>();
        if ( selectedEmailTemplateFolder <> null && selectedEmailTemplateFolder <> '' ) {
            opts.add(new SelectOption('null', ' - Select - '));
            if(selectedEmailTemplateFolder <> 'null'){
            System.debug('the value of selectedtemplateEmailfolder is '+ selectedEmailTemplateFolder );
              Id selectedId = (Id) selectedEmailTemplateFolder;
           for ( EmailTemplate et : allEmailTemplates ) {
                if ( et.FolderId == selectedId )
                    opts.add(new SelectOption(et.Id, et.Name));
                        }
            }
        }
        return opts;
    }
    public PageReference refreshEmailTemplateSection() {
    
     //We create a new list of Contacts that we be populated only with Contacts if they are selected
       
        //We will cycle through our list of cContacts and will check to see if the selected property is set to true, if it is we add the Contact to the selectedContacts list
        
        for(cContact cCon : ContactList) {
                if(cCon.selected) {
                selectedContacts.add(cCon.con);
              
            }
        }
      
        // Now we have our list of selected contacts and can perform any type of logic we want, sending emails, updating a field on the Contact, etc
        System.debug('These are the selected Contacts...');
        for(Contact con : selectedContacts) {
            contactids.add(con.id);
            system.debug(con);
            system.debug('TotalSelected' + selectedContacts.size());
        }       
        // this is a bit ugly, but a consequence of not being able to set a SelectOption's value to null
        if ( selectedEmailTemplateFolder == null || selectedEmailTemplateFolder == 'null' ) {
            selectedEmailTemplateFolder = null;
            selectedEmailTemplate = null;
system.debug('dbugselectedEmailTemplateFolder' + selectedEmailTemplateFolder ); 
system.debug('dbugselectedEmailTemplate' + selectedEmailTemplate);            
        }
        if ( selectedEmailTemplate == null || selectedEmailTemplate == 'null' ) {
            selectedEmailTemplate = null;
            chosenEmailTemplate = null;
        } else {
        if(selectedEmailTemplate<>null && selectedEmailTemplate <> ''){
        
        system.debug('dbugselectedEmailTemplateFolder' + selectedEmailTemplateFolder ); 
        system.debug('dbugselectedEmailTemplate' + selectedEmailTemplate);     
         
            Id selectedId = (Id) selectedEmailTemplate;
                 

            for ( EmailTemplate et : allEmailTemplates ) {
                if ( et.Id == selectedId ) {
                    chosenEmailTemplate = et;
                   
                    break;
                }
            }
            }
        }
        return null;
    }
       public PageReference refreshEmailTemplateSectionid() {
        
            Id selectedId = (Id) selectedEmailTemplate;
          emailTemplateId='/'+selectedId; 
        return null;
    }
public MassEmailEvent_cal(ApexPages.StandardController controller) {
this();
        allEmailTemplates = [select Id, Name, FolderId from EmailTemplate order by Name asc];
        allEmailTemplateFolders = [select Id, Name from Folder where Type = 'Email' order by Name asc];
        displaypblock=false;
         conts = new List<Event_Registration_Attendance__c>();
    conts=[Select id,Event__c from Event_Registration_Attendance__c limit 10000];
   eropty.Event__c =ApexPages.currentPage().getParameters().get('id');
        }
     public Event_Registration_Attendance__c eropty {
        get{
         System.debug('The value of the eropty is'+eropty);
            
            if(eropty<>null && eropty.Event__c <> null){
            System.debug('If the value of the opty is'+eropty);
            }
            else{
                eropty = new Event_Registration_Attendance__c(); 
                eropty.Event__c = null;
            }
            return eropty;
                      
            }
        set;}
    public MassEmailEvent_cal() {
         displaypblock=true;
        List < SelectOption > col = new List < SelectOption > ();
        for (integer i = 0; i < 5; i++) {
            Map < String,String > rowcond = new Map < String,String > ();
            conditions.put('Line' + (i + 1), rowcond);
        }
        col.add(new SelectOption('None', '--None--'));
        columns = col;
        obj = 'Contact';
        columnNames();
        getCampaignColumns();
        newMemberAdded = false;
    }
    ApexPages.StandardSetController ssc;
   ApexPages.StandardSetController sscResults {
        get;
        set;
    }

    
    /*****************************************************************************************
        Method Name - filerResult
        Return Type - List<Contact>
        Description - Getter & Setter for filerResult 
    ******************************************************************************************/
   public List < sObject> filerResult {
        get;
        set;
    }
   /*****************************************************************************************
        Method Name - filerLeadResult
        Return Type - List<Contact>
        Description - Getter & Setter for filerLeadResult   
    ******************************************************************************************/
    public List < Lead > filerLeadResult {
        get;
        set;
    }
    
    public String iVal1 {
        get;
        set;
    }
   
    public String operator1 {
        get;
        set;
    }
   
    public List < SelectOption > columns {
        get;
        set;
    }
   
    public List < SelectOption > campaignColumns;
  
    public List < String > pickListCol {
        get;
        set;
    }
    public List < String > pickListCampaignCol {
        get;
        set;
    }
    public String colName1 {
        get;
        set;
    }
    public String colName2 {
        get;
        set;
    }

    public String iVal2 {
        get;
        set;
    }
   
    public String operator2 {
        get;
        set;
    }
    public String colName3 {
        get;
        set;
    }
  
    public String iVal3 {
        get;
        set;
    }
    public String operator3 {
        get;
        set;
    }
    public String colName4 {
        get;
        set;
    }
    public String iVal4 {
        get;
        set;
    }
  
    public String operator4 {
        get;
        set;
    }

    public String colName5 {
        get;
        set;
    }
    public String iVal5 {
        get;
        set;
    }
   
    public String operator5 {
        get;
        set;
    }

    public String obj = null;
    public List < cContact > contactList;
  
    public String campaignId {
        get;
        set;
    }
    public String status {
        get;
        set;
    }

    public String activeTab {
        get;
        set;
    }
   public boolean newMemberAdded {
        get;
        set;
    }
        

    Map < String,Map < String,String >> conditions = new Map < String,Map < String,String >> ();
    public Map < String,String > fieldresults = new Map < String,String > ();
    /*****************************************************************************************
        Method Name - columnNames
        Return Type - PageReference 
        Description - Getting columnNames based on the selected object  
    ******************************************************************************************/
    public PageReference columnNames() {
        List < SelectOption > col = new List < SelectOption > ();
        col.add(new SelectOption('None', '--None--'));
        Map < String,
        Schema.SObjectField > fieldsMap = new Map < String,
        Schema.SObjectField > ();
        /*  if (obj == 'Lead') {
            fieldsMap = Lead.sObjectType.getDescribe().fields.getMap();

        } else*/
        if (obj == 'Contact') {
            fieldsMap = Contact.sObjectType.getDescribe().fields.getMap();//get fields for Contact 
        }
        pickListCol = new List < String > ();
        List < String > keys = new List < String > (fieldsMap.keySet());
        keys.sort();
        for (String field: keys) {
            Schema.DescribeFieldResult F = fieldsMap.get(field).getDescribe();
            system.debug('Type ---- >' + F.getType());
            if (F.isFilterable() && (F.getType() == Schema.Displaytype.STRING || F.getType() == Schema.Displaytype.DATE || F.getType() == Schema.Displaytype.BOOLEAN || F.getType() == Schema.Displaytype.EMAIL || F.getType() == Schema.Displaytype.PICKLIST)) {
                system.debug('Label ---- >' + F.getLabel() + ' Realtionship --------> ' + F.getRelationshipName());
                fieldresults.put(field, F.getType().Name());
                col.add(new SelectOption(field, F.getLabel()));
                if (F.getType() == Schema.Displaytype.PICKLIST)
                    pickListCol.add(field);
            }
        }
        columns = col;
        contactList = null;
        return null;
    }

    /*****************************************************************************************
        Method Name - getCampaignColumns
        Return Type - List<SelectOption> 
        Description - gets Radio button Options  
    ******************************************************************************************/
    public List < SelectOption > getCampaignColumns() {
        List < SelectOption > col = new List < SelectOption > ();
        col.add(new SelectOption('None', '--None--'));
        Map < String,Schema.SObjectField > fieldsMap = new Map < String,Schema.SObjectField > ();
        fieldsMap = Event_Registration_Attendance__c.sObjectType.getDescribe().fields.getMap();
        pickListCampaignCol = new List < String > ();
        List < String > keys = new List < String > (fieldsMap.keySet());
        for (String field: keys) {
            Schema.DescribeFieldResult F = fieldsMap.get(field).getDescribe();
            system.debug('Type ---- >' + F.getType());
            if (F.isFilterable() && (F.getType() == Schema.Displaytype.STRING || F.getType() == Schema.Displaytype.DATE || F.getType() == Schema.Displaytype.BOOLEAN || F.getType() == Schema.Displaytype.EMAIL || F.getType() == Schema.Displaytype.PICKLIST)) {
                system.debug('Label ---- >' + F.getLabel() + ' Realtionship --------> ' + F.getRelationshipName());
                fieldresults.put(field, F.getType().Name());
               col.add(new SelectOption(field, F.getLabel()));
                if (F.getType() == Schema.Displaytype.PICKLIST)
                    pickListCampaignCol.add(field);
            }
        }

        return col;
    }
    String errormsg = 'Please select following ';
    /*****************************************************************************************
        Method Name - errorString
        Return Type - String 
        Description - Performing for Validation 
    ******************************************************************************************/
    public String errorString() {
        for (integer i = 1; i <= 5; i++) {
            conditions.get('Line' + i).put('iVal' + i, (i == 1 ? iVal1: i == 2 ? iVal2: i == 3 ? iVal3: i == 4 ? iVal4: iVal5));
            conditions.get('Line' + i).put('Operator' + i, (i == 1 ? Operator1: i == 2 ? Operator2: i == 3 ? Operator3: i == 4 ? Operator4: Operator5));
            conditions.get('Line' + i).put('colName' + i, (i == 1 ? colName1: i == 2 ? colName2: i == 3 ? colName3: i == 4 ? colName4: colName5));
        }
        if (((colName1 <>'None' && operator1 == 'None') || (colName1 == 'None' && operator1 <>'None')) || ((colName2 <>'None' && operator2 == 'None') || (colName2 == 'None' && operator2 <>'None')) || ((colName3 <>'None' && operator3 == 'None') || (colName3 == 'None' && operator3 <>'None') || (colName4 == 'None' && operator4 <>'None') || (colName5 == 'None' && operator5 <>'None')))
            errormsg = 'Invalid conditions : Column(s) or Operator(s) missing';
        else if ((colName1 <>'None' && Operator1 == 'None') || (colName2 <>'None' && Operator2 == 'None') || (colName3 <>'None' && Operator3 == 'None') || (colName4 <>'None' && Operator4 == 'None') || (colName5 <>'None' && Operator5 == 'None'))
            errormsg = 'Operator(s) missing';
        else if ((colName1 == 'None' && Operator1 <>'None') || (colName2 == 'None' && Operator2 <>'None') || (colName3 == 'None' && Operator3 <>'None') || (colName4 == 'None' && Operator4 <>'None') || (colName5 == 'None' && Operator5 <>'None'))
            errormsg = 'Column(s) missing';
        return errormsg;
    }
    /*****************************************************************************************
        Method Name - convertData
        Return Type - String 
        Description - converts the data into String type from String, Date, PickList
    ******************************************************************************************/
    public String convertData(String column, String val) {
       System.debug(''+fieldresults+'************'+column+'@@@@'+val);//change
       System.debug('####'+fieldresults.get(column));//change
        if (fieldresults.get(column).equalsIgnoreCase('String') || fieldresults.get(column).equalsIgnoreCase('PickList') || fieldresults.get(column).equalsIgnoreCase('Email'))
            {val = '\'' + val + '\'';}
        else
        if(fieldresults.get(column).equalsIgnoreCase('Date') )
        {
                val=val;
        }
        
        return val;
      }
    /*****************************************************************************************
        Method Name - condString
        Return Type - String 
        Description - Generates condition string
    ******************************************************************************************/
    public String condString() {
        String conditionstring = '';
        System.debug('The conditions map is' + conditions);
        for (String s1: conditions.keySet()) {
            Map < String,String > lineconditions = conditions.get(s1);
            String column,operator,val,originalcondition;
           for (String s: lineconditions.keySet()) {
                if (s.contains('colName')) {
                    if (lineconditions.get(s) <>NULL && !lineconditions.get(s).equalsIgnoreCase('None')) {
                        column = lineconditions.get(s);
                    }
                    System.debug('The columnName is -->' + s + lineconditions.get(s) + column);
                }
                if (s.contains('Operator')) {
                    System.debug('The Operator is ' + s + lineconditions.get(s));
                    if (lineconditions.get(s) <>NULL && !lineconditions.get(s).equalsIgnoreCase('None')) {
                        operator = operators.get(lineconditions.get(s));
                        originalcondition = lineconditions.get(s);
                    }
                    System.debug('The Operator is --> ' + s + lineconditions.get(s) + operator);
                }
                if (s.contains('iVal')) {
                    System.debug('The iVal is -->' + s + lineconditions.get(s));
                    //val = lineconditions.get(s);
                    if(lineconditions.get(s) <> null)  
                    val = lineconditions.get(s) <> '' ? (lineconditions.get(s)).trim() : 'null'; 
                }
            }
            if (column <>null && operator <>null) {
              /*  if (fieldresults.get(column) <>NULL && (fieldresults.get(column).equalsIgnorecase('String') || fieldresults.get(column).equalsIgnorecase('Email'))) {
                    system.debug('operator in 1st loop =============== ' + originalcondition);
                    val = (originalcondition == 'contains' || originalcondition == 'does not contain') ? '%' + val + '%': originalcondition == 'starts with' ? val + '%': val;
                    system.debug('val in 1st loop =============== ' + val);
                    val = convertData(column, val);
                    system.debug('after val in 1st loop =============== ' + val);
                } else if (fieldresults.get(column) <>NULL && fieldresults.get(column).equalsIgnorecase('Date')) {
                    val = convertData(column, val);
                    system.debug('val in 2nd loop =============== ' + val);
                } else if (fieldresults.get(column) <>NULL && (fieldresults.get(column).equalsIgnorecase('COMBOBOX') || fieldresults.get(column).equalsIgnorecase('Picklist') || fieldresults.get(column).equalsIgnorecase('String'))) {
                    val = val.replaceAll(',', '\',\'');
                    val = '(\'' + val + '\')';
                    if (originalcondition.equalsIgnorecase('contains') || originalcondition.equalsIgnorecase('starts with') || originalcondition.equalsIgnorecase('equals')) {
                        operator = ' IN ';
                    } else if (originalcondition.equalsIgnorecase('does not contain') || originalcondition.equalsIgnorecase('not equals to')) {
                        operator = ' NOT IN ';
                    }
                }
                if (conditionstring == '')
                    conditionstring = conditionstring + ' ' + column + ' ' + operator + ' ' + val;
                else
                    conditionstring = conditionstring + ' AND ' + column + ' ' + operator + ' ' + val;*/
            }
           System.debug('The value of the column and operator and val' + column + '--' + operator + '---' + val);
            column = '';
            operator = '';
            val = '';
            system.debug('after val in last loop =============== ' + val);
        }
       return conditionstring;
    }
    //Map to store the Operator Meaning
    Map < String, String > operators = new Map < String,String > {
        'equals' =>'=',
        'not equals to' =>'<>',
        'starts with' =>'Like',
        'contains' =>'Like',
        'does not contain' =>'not Like',
        'less than' =>'<',
        'greater than' =>'>',
        'less or equal' =>'<=',
        'greater or equal' =>'>=',
        'includes' =>'Like',
        'excludes' =>'not Like'
    };
    /*****************************************************************************************
        Method Name - go
       Return Type - PageReference
        Description - 1. Called on the click on go button
                      2. Gets  condition string & builds the dynamic query
                      3. sets the values into filerResult
    ******************************************************************************************/
     
    public Pagereference go() {
        errormsg = '';
        errormsg = errorString();
        String conditionstring = '';
        conditionstring = condString();
        String querystring = '';
       
        boolean Attended = False;
        boolean Attending = False;
        boolean Invited = False;
        boolean NoShow = False;
        boolean NotAttending = False;
        boolean Noresponse = False;
        eventname = eropty.Event__c;
        Attended = eropty.Attended__c;
        Attending = eropty.Attending__c;
        Invited = eropty.Invited__c;
        Noresponse = eropty.no_response__c;
        NoShow = eropty.No_Show__c;
        NotAttending = eropty.Not_Attending__c;
        Set<Id> sObjectIds = new Set<Id>(); 
        System.debug('--->eventname' + eventname);
        System.debug('--->Conitionstring' + conditionstring);
            List<Event__c> cmp = [Select Id from Event__c where Name =: 'Select Event'];
            String cmpid = cmp[0].Id;
          if (eventname <> cmpid && conditionstring <> '' && Invited)
            {
             for(Event_Registration_Attendance__c crm: [Select id,contact__c from Event_Registration_Attendance__c where Event__r.id =: eventname and Invited__c =: Invited])
            {
            sObjectids.add(crm.contact__c) ;
            system.debug('sobjectadd');
            }
            querystring = 'Select AccountId, Email,Name,FirstName, Id, LastName, Title from Contact where Email <> Null AND id in:sObjectIds';
            querystring = querystring + ' and' + conditionstring + ' LIMIT 1000';//add conditions to the query by appending the conditonString
            System.debug('--->query--First Condition Invited' + conditionstring);
            }
            else
            if (eventname <> cmpid && conditionstring <> '' && Noresponse)
            {
             for(Event_Registration_Attendance__c crm: [Select id,contact__c from Event_Registration_Attendance__c where Event__r.id =: eventname and No_Response__c =: NoResponse limit 9999])
            {
            sObjectids.add(crm.contact__c) ;
            system.debug('sobjectadd');
            }
            querystring = 'Select AccountId, Email,Name,FirstName, Id, LastName, Title from Contact where Email <> Null AND id in:sObjectIds';
            querystring = querystring + ' and' + conditionstring + ' LIMIT 1000';//add conditions to the query by appending the conditonString
            System.debug('--->query--First Condition Invited' + conditionstring);
            }
          else
           if (eventname <> cmpid && conditionstring <> '' && Attended)
            {
             for(Event_Registration_Attendance__c crm: [Select id,contact__c from Event_Registration_Attendance__c where Event__r.id =: eventname and Attended__c =: Attended])
            {
            sObjectids.add(crm.contact__c) ;
            system.debug('sobjectadd');
           }
            querystring = 'Select AccountId, Email,Name,FirstName, Id, LastName, Title from Contact where Email <> Null AND id in:sObjectIds';
            querystring = querystring + ' and' + conditionstring + ' LIMIT 1000';//add conditions to the query by appending the conditonString
            System.debug('--->query--First Condition Attended' + conditionstring);
            }
        
            else
            if ((eventname ==cmpid) && (conditionstring <> null && conditionstring <> ''))
            {
                        system.debug('sobjectadd');
            querystring = 'Select AccountId, Email,Name,FirstName, Id, LastName, Title from Contact where Email <> Null';
            querystring = querystring + ' Where'+ conditionstring + ' LIMIT 1000';//add conditions to the query by appending the conditonString
            System.debug('--->query--Second Condition' + conditionstring);
            System.debug('--->query' + conditionstring);
            }
            else
            if ((conditionstring <> null && conditionstring == '' && Invited))
            {
                        for(Event_Registration_Attendance__c crm: [Select id,contact__c from Event_Registration_Attendance__c where Event__r.id =: eventname  and Invited__c =: Invited])
            {
            sObjectids.add(crm.contact__c)  ;
            }
              querystring = 'Select AccountId, Email,Name,FirstName, Id, LastName, Title from Contact where Email <> Null AND id in:sObjectIds limit 1000';
               System.debug('--->query--Third Condition' + conditionstring);
            }
        else
            if ((conditionstring <> null && conditionstring == '' && Attended))
            {
                        for(Event_Registration_Attendance__c crm: [Select id,contact__c from Event_Registration_Attendance__c where Event__r.id =: eventname  and Attended__c =: Attended])
            {
            sObjectids.add(crm.contact__c)  ;
            }
              querystring = 'Select AccountId, Email,Name,FirstName, Id, LastName, Title from Contact where Email <> Null AND  id in:sObjectIds limit 1000';
               System.debug('--->query--Third Condition' + conditionstring);
            }
            else
            if ((conditionstring <> null && conditionstring == '' && Attending))
            {
                        for(Event_Registration_Attendance__c crm: [Select id,contact__c from Event_Registration_Attendance__c where Event__r.id =: eventname  and Attending__c =: Attending])
            {
            sObjectids.add(crm.contact__c)  ;
            }
              querystring = 'Select AccountId, Email,Name,FirstName, Id, LastName, Title from Contact where Email <> Null AND id in:sObjectIds limit 1000';
               System.debug('--->query--Third Condition' + conditionstring);
            }
            else
            if ((conditionstring <> null && conditionstring == '' && NotAttending))
            {
                        for(Event_Registration_Attendance__c crm: [Select id,contact__c from Event_Registration_Attendance__c where Event__r.id =: eventname  and Not_Attending__c =: NotAttending])
            {
            sObjectids.add(crm.contact__c)  ;
            }
              querystring = 'Select AccountId, Email,Name,FirstName, Id, LastName, Title from Contact where Email <> Null AND id in:sObjectIds limit 1000';
               System.debug('--->query--Third Condition' + conditionstring);
            }
             else
            if ((conditionstring <> null && conditionstring == '' && NoShow))
            {
                        for(Event_Registration_Attendance__c crm: [Select id,contact__c from Event_Registration_Attendance__c where Event__r.id =: eventname  and No_Show__c =: Noshow])
            {
            sObjectids.add(crm.contact__c)  ;
            }
              querystring = 'Select AccountId, Email,Name,FirstName, Id, LastName, Title from Contact where Email <> Null AND id in:sObjectIds limit 1000';
               System.debug('--->query--Third Condition' + conditionstring);
            }
            else
            querystring = 'Select AccountId, Email,Name,FirstName, Id, LastName, Title from Contact where Email <> Null limit 1000';
            
       if (! (errormsg.contains('Column') || errormsg.contains('Operator'))) 
         {
            System.debug('The querystring is' + querystring);
            System.debug('eventname' + eventname);
            filerResult = Database.query(querystring);//execute the query
            sscResults = new ApexPages.StandardSetController(filerResult);
            sscResults.setPageSize(50);//set the page size for pagination
        }
         else
          {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, errormsg);
            ApexPages.addMessage(myMsg);
        }
           return null;
    }
       /*****************************************************************************************
        Method Name - getObjNames
        Return Type - List<SelectOption> 
        Description - gets Radio button Options  
    ******************************************************************************************/
    public List < SelectOption > getObjNames() {
        List < SelectOption > options = new List < SelectOption > ();
        // options.add(new SelectOption('Lead','Leads')); 
        options.add(new SelectOption('Contact', 'Contacts'));
        return options;
    }
    /*****************************************************************************************
        Method Name - getContactList
        Return Type -  List<cContact> 
        Description -  Returns Existing Campign Members for the Camapign Id 
    ******************************************************************************************/
    public List < cContact > getContactList() {
        if (sscResults != null) {
           
            contactList = new List < cContact > ();
            if (obj == 'Contact') {
                List < Contact > contacts = sscResults.getRecords();
                for (Contact con: contacts) {
                    contactList.add(new cContact(con));
                }
            }
        } else {
            contactList = new List < cContact > ();
        }
                return contactList;
            }
    
   
    /*****************************************************************************************
        Method Name - next
        Description - Gets next 25 records for pagination  
    ******************************************************************************************/
    public void next() {
        ssc.next();
        System.debug('the method entered is ------> NEXT');
    }
    /*****************************************************************************************
        Method Name - nextResults
        Description - Gets next 25 records for pagination  
    ******************************************************************************************/
    public void nextResults() {
        sscResults.next();
        System.debug('the method entered is ------> NEXT');
    }
    /*****************************************************************************************
        Method Name - previous 
        Description - Gets previous 25 records for pagination  
    ******************************************************************************************/
    public void previousResults() {
        sscResults.previous();
        System.debug('the method entered is ------> PREVIOUS');
    }
    /*****************************************************************************************
        Method Name - previous 
        Description - Gets previous 25 records for pagination  
    ******************************************************************************************/
    public void previous() {
        ssc.previous();
        System.debug('the method entered is ------> PREVIOUS');
    }
    /*****************************************************************************************
       Method Name - getOperatorOptions
        Return Type - List<SelectOption> 
        Description - Creates a selection option for list of operators.  
    ******************************************************************************************/
    public List < SelectOption > getOperatorOptions() {
        List < SelectOption > options = new List < SelectOption > ();
        options.add(new SelectOption('None', '--None--'));
        options.add(new SelectOption('equals', 'equals'));
        options.add(new SelectOption('not equals to', 'not equals to'));
        options.add(new SelectOption('starts with', 'starts with'));
        options.add(new SelectOption('contains', 'contains'));
        options.add(new SelectOption('less than', 'less than'));
        options.add(new SelectOption('greater than', 'greater than'));
        options.add(new SelectOption('less or equal', 'less or equal'));
        options.add(new SelectOption('greater or equal', 'greater or equal'));
        options.add(new SelectOption('includes', 'includes'));
                return options;
    }
    /*****************************************************************************************
        Method Name - getObj
        Return Type - String   
    ******************************************************************************************/
    public String getObj() {
        return obj;
    }
    /*****************************************************************************************
        Method Name - setObj
        Return Type - void    
    ******************************************************************************************/
    public void setObj(String obj) {
        this.obj = obj;
    }
    /*****************************************************************************************
        Class Name - cContact
    ******************************************************************************************/

    public class cContact {
        public Contact con {get;set;}
        public Boolean selected {get;set;}
        /*****************************************************************************************
        Constructor Name - cContact
        Description - Creates an object and option for selection  
    ******************************************************************************************/
        public cContact(Contact c) {
            con = c;
            selected = false;
        }
    }
    public PageReference LoadContacts()
    {   
       
         
         return null;
    }
    public PageReference getSelected()
    {
        selectedContacts.clear();
        for(cContact cCon : ContactList) {
                if(cCon.selected) {
                selectedContacts.add(cCon.con);
                 System.debug('iftrue These are the selected Contacts...'); 
            }
            }
            return null;
    }
    
   public PageReference SendEmail()
   {
       List<string> toLst = new List<string>();
       toLst.add('venkat.sandeep626@gmail.com');
       List<string> ccLst = new List<string>();
       ccLst.add('masandeep@deloitte.com');
       sendEventInviteICS( 'masandeep@deloitte.com', 'Sandeep',toLst, ccLst);
       //Redirect to the contact list page.
      Pagereference p= new Pagereference('/003/o');
      p.setRedirect(true);
      return p;
       
  }  
 
  public void sendEventInviteICS(  String fromAddress, String displayName, List<String> toAddress, List<String> ccAddress){
   			   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
               String[] toAddresses = new String[]{};
               toAddresses = toAddress; 
               mail.setToAddresses(toAddresses);
               mail.setTargetObjectId(contactids[0]);
       		   mail.setTemplateId(selectedEmailTemplate);  
               Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();   
               attach.filename = 'quarter1meeting.ics'; 
               attach.ContentType = 'text/calendar';     
               attach.inline = true;     
               attach.body = invite(fromAddress);   
               mail.setFileAttachments(new Messaging.EmailFileAttachment[] {attach});   
               Messaging.SendEmailResult[] er = Messaging.sendEmail(new Messaging.Email[] { mail });   
                
  }
  private Blob invite(String fromAddress) {
        String txtInvite = '';
        txtInvite += 'BEGIN:VCALENDAR\n';
        txtInvite += 'PRODID:-//Microsoft Corporation//Outlook 12.0 MIMEDIR//EN\n';
        txtInvite += 'VERSION:2.0\n';
        txtInvite += 'METHOD:REQUEST\n';
        txtInvite += 'X-MS-OLK-FORCEINSPECTOROPEN:TRUE\n';
        txtInvite += 'BEGIN:VEVENT\n';
        txtInvite += 'CLASS:PUBLIC\n';
        txtInvite += 'UID:156677t76576\n';
        txtInvite += 'SEQUENCE:0\n';
        txtInvite += 'ORGANIZER:wct_eventrespondemailservice@9-ugqam35bt4cdjpqcx1kc87wu16nlvkfte4k4h3x92gw2gdvtt.18-4hpbeaq.cs23.apex.sandbox.salesforce.com\n';
        txtInvite += 'CN=MAILTO:wct_eventrespondemailservice@9-ugqam35bt4cdjpqcx1kc87wu16nlvkfte4k4h3x92gw2gdvtt.18-4hpbeaq.cs23.apex.sandbox.salesforce.com';
        txtInvite += 'DTSTAMP:20150812T120000Z\n'; 
        txtInvite += 'CREATED:20150812T100000Z\n';
        txtInvite += 'DTSTART:20150812T080000Z\n';
        txtInvite += 'LOCATION:Online\n';
        txtInvite += 'PRIORITY:5\n';
        txtInvite += 'SUMMARY;';
        txtInvite += 'RSVP=TRUE:mailto:masandeep@deloitte.com'; 
        txtInvite += 'LANGUAGE=en-us:Salesforce Test\n';
        txtInvite += 'TRANSP:OPAQUE\n';
        txtInvite += 'X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN"><HTML><HEAD><META NAME="Generator" CONTENT="MS Exchange Server version 08.00.0681.000"><TITLE></TITLE></HEAD><BODY><!-- Converted from text/plain format --></BODY></HTML>\n';
        txtInvite += 'X-MICROSOFT-CDO-BUSYSTATUS:BUSY\n';
        txtInvite += 'X-MICROSOFT-CDO-IMPORTANCE:1\n';
        txtInvite += 'END:VEVENT\n';
        txtInvite += 'END:VCALENDAR';
        return Blob.valueOf(txtInvite);
    }
    

}