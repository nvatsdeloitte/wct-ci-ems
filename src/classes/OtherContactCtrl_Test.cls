/*****************************************************************************************
    Name    : OtherContactCtrl_Test
    Desc    : Test class for OtherContactCtrl class                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Ashish Shishodiya                 09/11/2013         Created 

******************************************************************************************/
@isTest
public class OtherContactCtrl_Test {
  /****************************************************************************************
    * @author      - Ashish Shishodiya
    * @date        - 09/11/2013
    * @description - Test Method for all methods of OtherContactCtrl Class 
    *****************************************************************************************/

    public static testmethod void OtherContactController(){
      // creating contacts
      Test_Data_Utility.createContact();
      //querying one contact
      Contact c = [Select Id from Contact limit 1];
      //setting id and phone parameters
      System.currentPageReference().getParameters().put('id',c.Id);
      test.startTest();
      // instantiating the controller class
      OtherContactCtrl con = new OtherContactCtrl();
      con.saveContact();
      con.errorMsg = 'Error1,Error2';
      con.sub();
      con.can();
      test.stopTest();
    }
}