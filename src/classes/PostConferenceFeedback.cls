public class  PostConferenceFeedback {

    
    public boolean thankyoupage{get;set;}
    public boolean welcomepage{get;set;}
    public String eventregpi { get; set; }
    public String eventregstat { get; set; }
    public Event_Registration_Attendance__c eventreg{get;set;}
    public Event_Registration_Attendance__c eve{get;set;}
    public contact conreg{get;set;}
    public Event__c event{get;set;}
    public contact con{get;set;}
    public String eid = ApexPages.currentPage().getParameters().get('eid');
    public String conid = ApexPages.currentPage().getParameters().get('cid');
    public String erid = ApexPages.currentPage().getParameters().get('erid');

    public PostConferenceFeedback(){
    welcomepage = true;
    con = new contact();
    eve= new  Event_Registration_Attendance__c ();
    eventreg=[select id,name,contact__c,event__c,FSS__c,Service_Area__c,Convention_Rating_Category__c,Inclusion_Ambassador__c,Deloitte_Professional_Met_Interviewer__c,Communicating_with_Candidate__c,Local_Recruiter_NIR__c,Local_Recruiter_Notes__c,Convention_Activity_Notes__c,Convention_Activity__c,Level__c,status__c,Second_Location_Preference__c,Location_Preference__c,Region__c from Event_Registration_Attendance__c where contact__c=:conid and Event__c=:eid and id=:erid];
    conreg=[select id,phone,WCT_School_Name__c,mobilephone,Degree_Type__c,CR_Expected_Grad_Month__c,CR_Expected_Grad_year__c,CR_Undergrad_Major__c,CR_Undergraduate_Overall_GPA_4_00_Scale__c, firstname, lastname, name, email from contact where id=:eventreg.contact__c];
    event=[select id,name from event__c where id=:eventreg.event__c];
    con.mobilephone=conreg.mobilephone;
    con.Degree_Type__c=conreg.Degree_Type__c;
    con.CR_Expected_Grad_Month__c=conreg.CR_Expected_Grad_Month__c;
    con.CR_Expected_Grad_year__c=conreg.CR_Expected_Grad_year__c;
    con.CR_Undergrad_Major__c=conreg.CR_Undergrad_Major__c;
    con.CR_Undergraduate_Overall_GPA_4_00_Scale__c=conreg.CR_Undergraduate_Overall_GPA_4_00_Scale__c;
    eve.FSS__c=eventreg.FSS__c;
    eve.Service_Area__c=eventreg.Service_Area__c;
    eve.Location_Preference__c=eventreg.Location_Preference__c;
    eve.second_Location_Preference__c=eventreg.second_Location_Preference__c;
    eve.region__c=eventreg.region__c;
    eventregstat=eventreg.status__c;
    eve.Convention_Rating_Category__c=eventreg.Convention_Rating_Category__c;
    eve.Convention_Activity__c=eventreg.Convention_Activity__c;
    eve.Convention_Activity_notes__c=eventreg.Convention_Activity_notes__c;
    eve.Inclusion_Ambassador__c=eventreg.Inclusion_Ambassador__c;
    eve.Deloitte_Professional_Met_Interviewer__c=eventreg.Deloitte_Professional_Met_Interviewer__c;
    eve.Communicating_with_Candidate__c=eventreg.Communicating_with_Candidate__c;
    eve.Local_Recruiter_NIR__c=eventreg.Local_Recruiter_NIR__c;
    eve.Local_Recruiter_notes__c=eventreg.Local_Recruiter_notes__c;
    eventregpi=eventreg.level__c;
    con.wct_school_name__c=conreg.wct_school_name__c;
    
    }
   
/////////////////////////////////////////custom setting to show event registration poistion of interest/////////////////////////////////     
   
    public list<SelectOption> getnirpoi(){
    List<SelectOption> optns = new List<Selectoption>();
    List<eEvent_Position_of_interest__c> statelist = new List<eEvent_Position_of_interest__c>();
    Map<String,eEvent_Position_of_interest__c> allpos = eEvent_Position_of_interest__c.getAll();// custom setting
    statelist = allpos.values();
    List<SelectOption> options = new List<SelectOption>();
    
    for (Integer i = 0; i < Statelist.size(); i++) {
    
    optns.add(new SelectOption(Statelist[i].name, Statelist[i].name));
    
    }
    optns.sort();
    return optns;
    
    }
       
/////////////////////////////////////////custom setting to show event registration status/////////////////////////////////        
   
    public list<SelectOption> getnirstat(){
    
    List<SelectOption> optns = new List<Selectoption>();
    List<NIR_status__c> statelist = new List<NIR_status__c>();
    Map<String,NIR_status__c> allpos = NIR_status__c.getAll();// custom setting
    Statelist = allpos.values();
    List<SelectOption> options = new List<SelectOption>();
    
    for (Integer i = 0; i < Statelist.size(); i++) {
    
    optns.add(new SelectOption(Statelist[i].name, Statelist[i].name));
    
    }
    optns.sort();
    return optns;
    }
    
/////////////////////////////////////////Method to update contact and event registration objects and show the thankyou screen/////////////////////////////////       
   
    public PageReference savenir() {
    contact conupd = new contact();
    conupd.id= conreg.id;
    conupd.WCT_School_Name__c= con.WCT_School_Name__c;
    conupd.mobilephone=con.mobilephone;
    conupd.degree_type__c=con.degree_type__c;
    conupd.CR_Expected_Grad_Month__c=con.CR_Expected_Grad_Month__c;
    conupd.CR_Expected_Grad_year__c=con.CR_Expected_Grad_year__c;
    conupd.CR_Undergrad_Major__c=con.CR_Undergrad_Major__c;
    conupd.CR_Undergraduate_Overall_GPA_4_00_Scale__c=con.CR_Undergraduate_Overall_GPA_4_00_Scale__c;
    update conupd;
    Event_Registration_Attendance__c eventregis = new Event_Registration_Attendance__c();
    eventregis.id=eventreg.id;
    eventregis.FSS__c=eve.FSS__c;
    eventregis.Service_Area__c=eve.Service_Area__c;
    if(eventregpi =='--None--')
    eventregpi=null;
    eventregis.Level__c=eventregpi;
    eventregis.Location_preference__c=eve.Location_preference__c;
    eventregis.second_Location_preference__c=eve.second_Location_preference__c;
    eventregis.region__c=eve.region__c;
    if(eventregstat =='--None--')
    eventregstat=null;
    eventregis.status__c= eventregstat;
    eventregis.convention_rating_category__c=eve.convention_rating_category__c;
    eventregis.convention_activity__c=eve.convention_activity__c;
    eventregis.convention_activity_notes__c=eve.convention_activity_notes__c;
    eventregis.inclusion_ambassador__c=eve.inclusion_ambassador__c;
    eventregis.Deloitte_Professional_Met_Interviewer__c=eve.Deloitte_Professional_Met_Interviewer__c;
    eventregis.communicating_with_candidate__c=eve.communicating_with_candidate__c;
    eventregis.Local_Recruiter_NIR__c=eve.Local_Recruiter_NIR__c;
    eventregis.Local_Recruiter_notes__c=eve.Local_Recruiter_notes__c;
    update eventregis;
    thankyoupage=true;
    welcomepage=false;
    return null;
    }
    
}