@isTest
public class ProjectExt_test {
public static Project__c propobj;
public static testmethod void ProjectExtSavings()
{
        propobj=new Project__c();
        propobj.Impacts__c='Cost Savings;Efficiencies;Customer Sat – External;Regulatory and/or Compliance';
        insert propobj;
 		pagereference pg=page.CalculateImpacts;
        test.setCurrentPage(pg);
    	ApexPages.Standardcontroller prjstcnt = new ApexPages.Standardcontroller(propobj);
        ApexPages.currentPage().getparameters().put('id',propobj.id);
        ProjectExt prjext=new ProjectExt(prjstcnt);
    	prjext.sectionload();
}
}