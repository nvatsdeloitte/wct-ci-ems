public with sharing class ProjectsByPersonController
{

/*
 Apex Class:  ProjectsByPersonController
 Purpose: This class is used to prepare view of Projects By Project Lead.
 Created On:  6th July,2015.
 Created by:  Balu Devarapu.(Deloitte India)
*/


public set<string> lstPriority{get;set;}
public Set<string> setLeadName{get;set;}
public List<string> lstFinalPriority{get;set;}
public Map<string,Map<string,Integer>> mapAllLeadData{get;set;}
public List<ChartData> objChartData{get;set;}
List<ChartData> objChartDataClone = new List<ChartData>();

/* Constructor */
public ProjectsByPersonController(){
    lstPriority=new set<string>();
    objChartData = new List<ChartData>();
    lstFinalPriority=new List<string>();
    ProcessData();
}


    /* 
        Method:ProcessData
        Description: Data view Perparation 
    */
    public void ProcessData(){
    try{
    
    Schema.DescribeFieldResult objlstTC = Project__c.Priority__c.getDescribe();

    List<Schema.PicklistEntry> lstTC = objlstTC.getPicklistValues();
    for(Schema.picklistEntry f:lstTC)    
    {    
        lstPriority.add(f.getLabel());    
    }
    setLeadName=new Set<string>();
    mapAllLeadData=new Map<string,Map<string,Integer>>();
    AggregateResult[] PriorityUSResults
   = [select Priority__c Priority,Project_Lead_US__r.Name LeadName,Count(Id) prjCnt from Project__c where Project_Lead_US__c!=null and Priority__c!=null and Status__c='In Progress' group by Priority__c,Project_Lead_US__r.Name order by Project_Lead_US__r.Name];
    
    for (AggregateResult ar : PriorityUSResults)  {
         string LdName=String.ValueOf(ar.get('LeadName'));
         if(LdName.contains('(')){
         LdName=LdName.substring(0,LdName.indexOf('('));
         }
        LdName+=' (US)';
        setLeadName.add(LdName);
        Map<string,Integer> mapLeadProj=new Map<string,Integer>();
        if(mapAllLeadData.get(LdName)!=null){
        mapLeadProj=mapAllLeadData.get(LdName);
        mapLeadProj.put(String.ValueOf(ar.get('Priority')),integer.ValueOf(ar.get('prjCnt')));
        }else{
        mapLeadProj.put(String.ValueOf(ar.get('Priority')),integer.ValueOf(ar.get('prjCnt')));
        }
        
        mapAllLeadData.put(LdName,mapLeadProj);
    }
    
    AggregateResult[] PriorityUSIResults
   = [select Priority__c Priority,Project_Lead_USI__r.Name LeadName,Count(Id) prjCnt from Project__c where Project_Lead_USI__c!=null and Priority__c!=null and Status__c='In Progress' group by Priority__c,Project_Lead_USI__r.Name order by Project_Lead_USI__r.Name];
    
    for (AggregateResult ar : PriorityUSIResults)  {
         string LdName=String.ValueOf(ar.get('LeadName'));
         if(LdName.contains('(')){
         LdName=LdName.substring(0,LdName.indexOf('('));
         }
        LdName+=' (USI)';
        setLeadName.add(LdName);
        Map<string,Integer> mapLeadProj=new Map<string,Integer>();
        if(mapAllLeadData.get(LdName)!=null){
        mapLeadProj=mapAllLeadData.get(LdName);
        mapLeadProj.put(String.ValueOf(ar.get('Priority')),integer.ValueOf(ar.get('prjCnt')));
        }else{
        mapLeadProj.put(String.ValueOf(ar.get('Priority')),integer.ValueOf(ar.get('prjCnt')));
        }
        mapAllLeadData.put(LdName,mapLeadProj);
    }

    
    List<string> lstLeadName=new List<string>();
    lstLeadName.addAll(setLeadName);
    lstLeadName.sort();
    for(string objLead:lstLeadName){
     Map<string,Integer> objmapLeadProj=new Map<string,Integer>();
     List<string> lstLeadPriority=new List<string>();
     if(mapAllLeadData.get(objLead)!=null){
     lstLeadPriority=new List<string>();
     objmapLeadProj=mapAllLeadData.get(objLead);
     for(string objPrioty:lstPriority){
     string PriorityLead=string.ValueOf(objmapLeadProj.get(objPrioty));
     if(string.IsEmpty(PriorityLead)){
     PriorityLead=' ';}
     lstLeadPriority.add(PriorityLead);
     }
     }
     objChartData.add(new ChartData(objLead, lstLeadPriority)); 
     }
     
    objChartDataClone = objChartData;
    }catch(Exception e){
    system.debug('----------Exception-------------'+e.getMessage()+'-----at Line #----'+e.getLineNumber()); 
    }

 } 

 
    /* 
    Method:FilterResult
    Description: Populates data based on Filter conditions provided by User. 
    */
public void FilterResult(){
   
   string SearchLead='';
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('SearchLead'))){
    SearchLead=apexpages.currentpage().getparameters().get('SearchLead');
    SearchLead=SearchLead.ToLowerCase();
    }
    if(SearchLead==''){
    objChartData = new List<ChartData>();
    objChartData=objChartDataClone;
    }else{
    objChartData = new List<ChartData>();
    
    for(ChartData objCD:objChartDataClone){
     if(objCD.LeadName.ToLowerCase().contains(SearchLead)){
      objChartData.add(objCD); 
     }
    }
    }
}

 
 
     /* 
    Class:ChartData 
    Description: Wrapper class used to prepare data
    */
 
    public class ChartData {
        public String LeadName { get; set; }
        public List<string> PriorityItems{ get; set; }

        public ChartData(String LeadName1, List<string> PriorityItems1) {
            
            this.LeadName = LeadName1;
            this.PriorityItems= PriorityItems1;

        }
    }  



}