public with sharing class ProjectsByTalentChannelController{

/*
 Apex Class:  ProjectsByTalentChannelController
 Purpose: This class is used to prepare view of Projects By talent Channerl
 Created On:  3rd July,2015.
 Created by:  Balu Devarapu.(Deloitte India)
*/


public set<string> lstTalChannel{get;set;}
public List<ChartData> objChartData{get;set;}
public List<SelectOption> TcOptions{get;set;}
public string SelectedTC{get;set;}
public Map<string,string> mapTC{get;set;}
public List<integer> lstTCOccurenceCount{get;set;}

Map<string,integer> mapTcOccournace{get;set;}

/* Constructor */
public ProjectsByTalentChannelController(){
lstTCOccurenceCount=new List<integer>();
mapTcOccournace=new Map<string,integer>();
SelectedTC='-None-';
lstTalChannel=new set<string>();
ProcessData();
}

    /* 
        Method:ProcessData
        Description: Data view Perparation 
    */
public void ProcessData(){
    try{
    lstTCOccurenceCount=new List<integer>();
    mapTcOccournace=new Map<string,integer>();
    objChartData = new List<ChartData>();
    Schema.DescribeFieldResult objlstTC = Project__c.Talent_Channel__c.getDescribe();
    SelectedTC='';
    List<Schema.PicklistEntry> lstTC = objlstTC.getPicklistValues();
    
    TcOptions = new List<SelectOption>();
    TcOptions.add(new SelectOption('-None-','-None-'));
    for(Schema.picklistEntry f:lstTC)    
    {    
        if(f.getLabel()!='------------------None------------------'){
          
        lstTalChannel.add(f.getLabel());  
        TcOptions.add(new SelectOption(f.getLabel(),f.getLabel()));     
        }
    }
    mapTC=new Map<string,string>();


    for(Project__c objProj: [select Name,Talent_Channel__c from Project__c order by Name]) 
    {
        mapTC.put(objProj.Name,objProj.Talent_Channel__c);
    }
    
    for(string objPName:mapTC.keyset())
    {
        string TalentChannels=mapTC.get(objPName);
        if(string.IsNotEmpty(TalentChannels)){
        List<string> lstTalentChannels=TalentChannels.split(';');
        Set<string> setTalentChannels=new Set<string>();
        
        for(string strTC:lstTalentChannels)
        {
          if(lstTalChannel.contains(strTC)){
           setTalentChannels.add(strTC);
           Integer iCount=1;
           if(mapTcOccournace.get(strTC)!=null){
            iCount= mapTcOccournace.get(strTC);
            iCount++;
           }
           mapTcOccournace.put(strTC,iCount);
           
          }
        }
        
        setTalentChannels.remove('------------------None------------------');
        objChartData.add(new ChartData(objPName, setTalentChannels)); 
        } 
    }
     for(string strTalC:lstTalChannel){
     integer iCount=0;
     if(mapTcOccournace.get(strTalC)!=null){
        iCount= mapTcOccournace.get(strTalC);
       }
     lstTCOccurenceCount.add(iCount);
    }

 
 }catch(Exception e){
  system.debug('----------Exception-------------'+e.getMessage()+'-----at Line #----'+e.getLineNumber()); 
  }
 
 
 
} 

    /* 
    Method:FilterResult
    Description: Populates data based on Filter conditions provided by User. 
    */
public void FilterResult(){
    lstTCOccurenceCount=new List<integer>();
    mapTcOccournace=new Map<string,integer>();
    SelectedTC='-None-';
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('SelectedTC'))){
    SelectedTC=apexpages.currentpage().getparameters().get('SelectedTC');
    if(SelectedTC=='-None-'){
    ProcessData();
    }
    else{
    lstTalChannel=new set<string>();
    objChartData = new List<ChartData>();
    lstTalChannel.add(SelectedTC);
    for(string objPName:mapTC.keyset())
    {
        string TalentChannels=mapTC.get(objPName);
        if(string.IsNotEmpty(TalentChannels)){
        List<string> lstTalentChannels=TalentChannels.split(';');
        Set<string> setTalentChannels=new Set<string>();
        
        for(string strTC:lstTalentChannels)
        {
          if(lstTalChannel.contains(strTC)){
           setTalentChannels.add(strTC);
           
           Integer iCount=1;
           if(mapTcOccournace.get(strTC)!=null){
            iCount= mapTcOccournace.get(strTC);
            iCount++;
           }
           mapTcOccournace.put(strTC,iCount);
           
          }
        }
        
        setTalentChannels.remove('------------------None------------------');
        objChartData.add(new ChartData(objPName, setTalentChannels)); 
        } 
    }
     for(string strTalC:lstTalChannel){
     integer iCount=0;
     if(mapTcOccournace.get(strTalC)!=null){
        iCount= mapTcOccournace.get(strTalC);
       }
     lstTCOccurenceCount.add(iCount);
    }
    }
}
}


     /* 
    Class:ChartData 
    Description: Wrapper class used to prepare data
    */

    public class ChartData {
        public String ProjectName { get; set; }
        public set<string> ItemsPresent{ get; set; }

        public ChartData(String ProjectName1, set<string> setTalentChannels1) {
            
            this.ProjectName = ProjectName1;
            this.ItemsPresent= setTalentChannels1;

        }
    }


}