public with sharing class ProjectsByTimeLineController{
/*
 Apex Class:  ProjectsByTimeLineController
 Purpose: This class is used to prepare view of Projects By Time line with Priority
 Created On:  9th July,2015.
 Created by:  Balu Devarapu.(Deloitte India)
*/
List<string> lstMonths{get;set;} 
public List<ChartData> objChartData{get;set;}
public List<SelectOption> PriorityOptions{get;set;}
public string SelectedPriority{get;set;}
List<ChartData> objChartDataClone = new List<ChartData>();
Date MinDate;
Date MaxDate;
public string SearchLead='';
public string Priority='';
boolean IsFilterAction=false;

/* Constructor */
public ProjectsByTimeLineController(){
    SearchLead='';
    Priority='';
    IsFilterAction=false;
    ProcessData();
}

    /* 
        Method:ProcessData
        Description: Data view Perparation 
    */
public void ProcessData(){
    try{
    lstMonths=new List<string>(); 
    objChartData = new List<ChartData>();
    List<Project__c> lstProj=new List<Project__c>(); // where Project_Start_Date__c!=null and Project_Planned_End_Date__c!=null 
    if(!IsFilterAction){
    lstProj=[select Name,Project_Start_Date__c,Project_Planned_End_Date__c,Project_Lead_US__r.Name,Project_Lead_USI__r.Name,Priority__c from Project__c where Project_Start_Date__c!=null and Project_Planned_End_Date__c!=null order by Name];
    }else{
    
    if(string.IsNotEmpty(Priority)){
    lstProj=[select Name,Project_Start_Date__c,Project_Planned_End_Date__c,Project_Lead_US__r.Name,Project_Lead_USI__r.Name,Priority__c from Project__c where Project_Start_Date__c!=null 
    and Project_Planned_End_Date__c!=null and Priority__c=:Priority and (Project_Lead_US__r.Name like : '%'+SearchLead+'%' or Project_Lead_USI__r.Name like : '%'+SearchLead+'%') order by Name];
    }
    else{
    lstProj=[select Name,Project_Start_Date__c,Project_Planned_End_Date__c,Project_Lead_US__r.Name,Project_Lead_USI__r.Name,Priority__c from Project__c where Project_Start_Date__c!=null 
    and Project_Planned_End_Date__c!=null and (Project_Lead_US__r.Name like : '%'+SearchLead+'%' or Project_Lead_USI__r.Name like : '%'+SearchLead+'%') order by Name];
        
    }
    }
    system.debug('----lstProj-----'+lstProj);
    for(Project__c objProj: lstProj) 
    {
        if(MinDate==null)
        MinDate=objProj.Project_Start_Date__c;
        if(MaxDate==null)
        MaxDate=objProj.Project_Planned_End_Date__c;
        
        if(objProj.Project_Start_Date__c<MinDate)
        MinDate=objProj.Project_Start_Date__c;
      
        if(objProj.Project_Planned_End_Date__c>MaxDate)
        MaxDate=objProj.Project_Planned_End_Date__c;
    }
 

    lstMonths.AddAll(setMonthSpan(MinDate,MaxDate));
    system.debug('----lstMonths 1-----'+lstMonths);
    set<string> lstProjDuration;
    List<string> lstProjTimeSpan;
    string USName='';
    string USIName='';
    


    Schema.DescribeFieldResult objlstTC = Project__c.Priority__c.getDescribe();
    SelectedPriority='';
    List<Schema.PicklistEntry> lstPP = objlstTC.getPicklistValues();

    PriorityOptions= new List<SelectOption>();
    PriorityOptions.add(new SelectOption('-None-','-None-'));
    for(Schema.picklistEntry f:lstPP)    
    {    
        PriorityOptions.add(new SelectOption(f.getLabel(),f.getLabel()));     
    }
    set<string> setPP=new set<string>();
        
    for(Project__c objProj:lstProj){
     
        if(objProj.Priority__c=='Critical'){
        if(!setPP.contains(objProj.Priority__c)){
        setPP.add(objProj.Priority__c); 
        objChartData.add(new ChartData('Projects - Critical',true,'US Lead','USI Lead','Critical', lstMonths));
        }
        lstProjDuration = new set<string>();    
        lstProjDuration=setMonthSpan(objProj.Project_Start_Date__c,objProj.Project_Planned_End_Date__c);
        lstProjTimeSpan=new List<string>();
        system.debug('---objProj.Name-----'+objProj.Name);
        system.debug('---lstMonths-----'+lstMonths);
        for(string strDur:lstMonths){
            if(lstProjDuration.Contains(strDur))
            lstProjTimeSpan.add('yes');
            else
            lstProjTimeSpan.add(' ');
        }
        system.debug('---lstProjTimeSpan-----'+lstProjTimeSpan);
        USName=objProj.Project_Lead_US__r.Name;
        if(USName!=null && USName.Contains('('))
        USName=USName.substring(0,objProj.Project_Lead_US__r.Name.indexOf('('));
    
        USIName=objProj.Project_Lead_USI__r.Name;
        if(USIName!=null && USIName.Contains('('))
        USIName=USIName.substring(0,objProj.Project_Lead_USI__r.Name.indexOf('('));
        
        objChartData.add(new ChartData(objProj.Name,false,USName,USIName,objProj.Priority__c, lstProjTimeSpan));    
        }
    }
    
    for(Project__c objProj:lstProj){
        
        if(objProj.Priority__c=='High'){
        if(!setPP.contains(objProj.Priority__c)){
        setPP.add(objProj.Priority__c); 
        objChartData.add(new ChartData('Projects - High',true,'US Lead','USI Lead','High', lstMonths));
        }   
        lstProjDuration = new set<string>();    
        lstProjDuration=setMonthSpan(objProj.Project_Start_Date__c,objProj.Project_Planned_End_Date__c);
        lstProjTimeSpan=new List<string>();
        for(string strDur:lstMonths){
            if(lstProjDuration.Contains(strDur))
            lstProjTimeSpan.add('yes');
            else
            lstProjTimeSpan.add(' ');
        }
        USName=objProj.Project_Lead_US__r.Name;
        if(USName!=null && USName.Contains('('))
        USName=USName.substring(0,objProj.Project_Lead_US__r.Name.indexOf('('));
    
        USIName=objProj.Project_Lead_USI__r.Name;
        if(USIName!=null && USIName.Contains('('))
        USIName=USIName.substring(0,objProj.Project_Lead_USI__r.Name.indexOf('('));
        objChartData.add(new ChartData(objProj.Name,false,USName,USIName,objProj.Priority__c, lstProjTimeSpan));
        }
    }
     
     for(Project__c objProj:lstProj){
       
        if(objProj.Priority__c=='Medium'){
        if(!setPP.contains(objProj.Priority__c)){
        setPP.add(objProj.Priority__c); 
        objChartData.add(new ChartData('Projects - Medium',true,'US Lead','USI Lead','Medium', lstMonths));
        }       
        lstProjDuration = new set<string>();    
        lstProjDuration=setMonthSpan(objProj.Project_Start_Date__c,objProj.Project_Planned_End_Date__c);
        lstProjTimeSpan=new List<string>();
        for(string strDur:lstMonths){
            if(lstProjDuration.Contains(strDur))
            lstProjTimeSpan.add('yes');
            else
            lstProjTimeSpan.add(' ');
        }
        USName=objProj.Project_Lead_US__r.Name;
        if(USName!=null && USName.Contains('('))
        USName=USName.substring(0,objProj.Project_Lead_US__r.Name.indexOf('('));
    
        USIName=objProj.Project_Lead_USI__r.Name;
        if(USIName!=null && USIName.Contains('('))
        USIName=USIName.substring(0,objProj.Project_Lead_USI__r.Name.indexOf('('));
        objChartData.add(new ChartData(objProj.Name,false,USName,USIName,objProj.Priority__c, lstProjTimeSpan));}
    }
    
    for(Project__c objProj:lstProj){
        
        if(objProj.Priority__c=='Low'){
            if(!setPP.contains(objProj.Priority__c)){
            setPP.add(objProj.Priority__c); 
            objChartData.add(new ChartData('Projects - Low',true,'US Lead','USI Lead','Low', lstMonths));
            }       
        lstProjDuration = new set<string>();    
        lstProjDuration=setMonthSpan(objProj.Project_Start_Date__c,objProj.Project_Planned_End_Date__c);
        lstProjTimeSpan=new List<string>();
        for(string strDur:lstMonths){
            if(lstProjDuration.Contains(strDur))
            lstProjTimeSpan.add('yes');
            else
            lstProjTimeSpan.add(' ');
        }
        USName=objProj.Project_Lead_US__r.Name;
        if(USName!=null && USName.Contains('('))
        USName=USName.substring(0,objProj.Project_Lead_US__r.Name.indexOf('('));
    
        USIName=objProj.Project_Lead_USI__r.Name;
        if(USIName!=null && USIName.Contains('('))
        USIName=USIName.substring(0,objProj.Project_Lead_USI__r.Name.indexOf('('));
        objChartData.add(new ChartData(objProj.Name,false,USName,USIName,objProj.Priority__c, lstProjTimeSpan));}
    }   
    if(!IsFilterAction){
    objChartDataClone=objChartData;
    }
    setPP.clear();
 }catch(Exception e){
  system.debug('----------Exception-------------'+e.getMessage()+'-----at Line #----'+e.getLineNumber()); 
  }
 
 system.debug('-------objChartData----------'+objChartData);
 
}

    /* 
        Method:setMonthSpan
        Description: Returns Set of Month,Year Combinations in between two dates provided to this Method. 
    */
public set<string> setMonthSpan(Date MinDate,Date MaxDate){
    set<string> setMonthSpan=new set<string>();
    try{
    if(MinDate!=null && MaxDate!=null){
    Integer TotMonths=MinDate.monthsBetween(MaxDate);
    if(TotMonths==0)
    TotMonths=1;
    
    Integer MinMonth=MinDate.Month();
    
    Date TempYear=MinDate;
    for(integer i=0;i<TotMonths;i++){
        setMonthSpan.add(ConvertToMonth(TempYear.Month())+','+String.ValueOf(TempYear.Year()).Substring(2,4));
        TempYear=TempYear.addMonths(1);
        }
    }
     }catch(Exception e){
  system.debug('----------Exception-------------'+e.getMessage()+'-----at Line #----'+e.getLineNumber()); 
  }
    return setMonthSpan;
}




 
    /* 
    Method:FilterResult
    Description: Populates data based on Filter conditions provided by User. 
    */
public void FilterResult(){
   
system.debug('---------objChartDataClone size()--------'+objChartDataClone.size());
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('SearchLead'))){
    SearchLead=apexpages.currentpage().getparameters().get('SearchLead');
    SearchLead=SearchLead.ToLowerCase();
    
    }
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('Priority'))){
    Priority=apexpages.currentpage().getparameters().get('Priority');
    Priority=Priority.ToLowerCase();
    if(Priority=='-None-'){
      Priority='';  
    }
    }   
    
    
    if(SearchLead=='' && Priority==''){ // If both filters are null show Cloned data
    objChartData = new List<ChartData>();
    objChartData=objChartDataClone;
    }else if(SearchLead=='' && Priority!=''){ // If filter is only on Priority then loop with Priority and build new wraper class;
    objChartData = new List<ChartData>();
    for(ChartData objCD:objChartDataClone){
    if(objCD.Priority.ToLowerCase()==Priority){
        objChartData.add(objCD); 
     }
    }   
    }
    else{ // Prepare Date by Querying on Database with provided filter options
        IsFilterAction=true;
        ProcessData();
    }
    SearchLead='';
    Priority='';
}
 
     /*
     * Method Name: ConvertToFullMonth 
     * Description: Number to Month Name Conversion
     * 
     */ 
    public static string ConvertToMonth(integer Mon){
        
        
        if(Mon==1){
          return 'Jan';  
        }
        else if(Mon==2){
          return 'Feb';  
        }
        else if(Mon==3){
          return 'Mar';   
        }
        else if(Mon==4){
          return 'Apr';  
        }
        else if(Mon==5){
          return 'May';  
        }
        else if(Mon==6){
          return 'Jun';  
        }
        else if(Mon==7){
         return 'Jul';   
        }
        else if(Mon==8){
         return 'Aug';   
        }
        else if(Mon==9){
         return 'Sep';   
        }
        else if(Mon==10){
        return 'Oct';    
        }
        else if(Mon==11){
        return 'Nov';    
        }
        else if(Mon==12){
          return 'Dec';  
        }
        else
          return string.Valueof(Mon);  
    }



     /* 
    Class:ChartData 
    Description: Wrapper class used to prepare data
    */
    public class ChartData {
        public string ProjectName { get; set; }
        public boolean IsSubHeader{get;set;}
        public string USLead{get;set;}
        public string USILead{get;set;}
        public string Priority{get;set;}
        public List<string> ItemsPresent{ get; set; }
        
        public ChartData(String ProjectName1,boolean IsSubHeader1,string USLead1,string USILead1,string Priority1,List<string> ItemsPresent1) {
            
            this.ProjectName = ProjectName1;
            this.IsSubHeader= IsSubHeader1;
            this.USLead = USLead1;
            this.USILead= USILead1;
            this.Priority=Priority1;
            this.ItemsPresent= ItemsPresent1;
        }
    }


}