public class QueryCriteria {
    public String fieldName;
    public String operator;
    public Object value;
    public String unioperator;

    // constructor
    public QueryCriteria (String fieldName, String operator, Object value, String unioperator) {
        this.fieldName = fieldName;
        this.operator = operator;
        this.value = value;
        this.unioperator = unioperator;
    }

    // create where clause
    public static String getWhereClause(List<QueryCriteria> criteriaList) {
        String whereClause = '';

        // add each portion of the where clause
        if (criteriaList != null) {
            for (QueryCriteria criteria : criteriaList) {
                if (criteria.value != null) {
                    // assuming and in this implementation
                    if (whereClause != '')
                        //whereClause += ' and ';
						whereClause += ' '+criteria.unioperator+' ';        
                    if (criteria.value instanceOf String) {
                        String criteriaValue = ((String) criteria.value).replaceAll('\'', '');
                        
                        system.debug('--@--'+criteria.value+'--#--'+criteriaValue);
                        if(string.valueof(criteria.value).contains('--')){
	                        criteriaValue = criteriaValue.replace('--', '');
	                        if(criteria.operator=='INCLUDES')whereClause += criteria.fieldName + ' ' + criteria.operator + ' (\'' + criteriaValue + '\' )';
	                        else whereClause += criteria.fieldName + ' ' + criteria.operator + ' \'' + criteriaValue + '\' )';
                        }
                        else{
                        	whereClause += criteria.fieldName + ' ' + criteria.operator + ' \'' + criteria.value + '\' ';
                        }
                    } else if (criteria.value instanceOf ID) {
                        whereClause += criteria.fieldName + ' = \'' + criteria.value + '\' ';
                    } else if (criteria.value instanceOf Date) {
                        whereClause += criteria.fieldName + ' ' + criteria.operator + ' ' + formatDate((Date) criteria.value, 'yyyy-MM-dd') + ' ';
                    } else if (criteria.value instanceOf Datetime) {
                        whereClause += criteria.fieldName + ' ' + criteria.operator + ' ' + ((Datetime) criteria.value).format('yyyy-MM-dd') + 'T' + ((Datetime) criteria.value).format('hh:mm:ss') + 'Z' + ' ';
                    } else if (criteria.value instanceOf Integer || criteria.value instanceOf Decimal || criteria.value instanceOf Double || criteria.value instanceOf Boolean) {
                        whereClause += criteria.fieldName + ' ' + criteria.operator + ' ' + criteria.value + ' ';
                    }
                }
            }
        }

        // if appropriate add where to the where clause
        if (whereClause != '')
            whereClause = ' where ' + whereClause;

        return whereClause;
    }

    // format date using format string (much like a Datetime field is formatted)
    public static String formatDate(Date d, String formatString) {
        if (d != null){
            Datetime dt = Datetime.newInstance(d.year(), d.month(), d.day());
            return dt.format(formatString);
        } 
        return null;
    }
}