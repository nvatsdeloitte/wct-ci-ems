/*****************************************************************************************
    Name    : RelatedCases_Test
    Desc    : Test class for RelatedCases class                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Ashish Shishodiya                 09/12/2013         Created 

******************************************************************************************/
@isTest
public class RelatedCases_Test {
  /****************************************************************************************
    * @author      - Ashish Shishodiya
    * @date        - 09/12/2013
    * @description - Test Method for all methods of RelatedCases Class 
    *****************************************************************************************/

    public static testmethod void RelatedCasesTest(){
           //Creating Case data
      Test_Data_Utility.createCase();
      List<Case> caseList = new List<Case>();
      caseList = [select Id, ContactId from Case limit 5];
      // Creating Contact data
      Test_Data_Utility.createContact();
      Contact con = [select Id from Contact limit 1];
      //ataching contact to case
      caseList[0].ContactId = con.Id;
      caseList[1].WCT_RelatedCase__c = caseList[0].id;
      caseList[2].ParentId = caseList[0].id;
      caseList[3].WCT_Duplicate_Of__c = caseList[0].id;
      update caseList;
      test.startTest();
      ApexPages.StandardController controller0 = new ApexPages.StandardController(caseList[0]);
      RelatedCases cont0 = new RelatedCases(controller0);
      ApexPages.StandardController controller1 = new ApexPages.StandardController(caseList[1]);
      RelatedCases cont1 = new RelatedCases(controller1);
      ApexPages.StandardController controller2 = new ApexPages.StandardController(caseList[2]);
      RelatedCases cont2 = new RelatedCases(controller2);
      ApexPages.StandardController controller3 = new ApexPages.StandardController(caseList[3]);
      RelatedCases cont3 = new RelatedCases(controller3);
      test.stopTest();
        
        
        
    }
}