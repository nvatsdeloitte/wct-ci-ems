public class RenewPassTask {
public static void RenewPass(list<WCT_Assignment__c> ast)

  {
  
  
   List<Task>tasks=new List<Task>();
   Set<Id> contactIds = new Set<Id>();
   List<WCT_Mobility__c> updateeligibletravel=new List<WCT_Mobility__c>(); 
   Set<Id> mobids= new Set<Id>();
   recordtype record=[select id,name,SobjectType from recordtype where (name='Employment Visa') and (SobjectType ='WCT_Mobility__c')];//[select id,name from recordtype where name='Employment Visa'];
   recordtype cntrecd=[select id,name,SobjectType from recordtype where (name='Employee') and (SobjectType='Contact')];//[select id,name from recordtype where name='Employee'];
  // recordtype tskrecd=[select id,name from recordtype where name='Task'];
  id rcdtype=WCT_Util.getRecordTypeIdByLabel('task','Task');
  //user gmiusr=[select id from user where name='GM & I'];
  for(WCT_Assignment__c At:ast )
    {
       
        if(at.WCT_Mobility__c!=null)
        {
           mobIds.add(at.WCT_Mobility__c) ;
                    
        }
       
        
    }    
      
      
     
     
     Map<id,WCT_Mobility__c> mobEntry = new Map<id,WCT_Mobility__c>
     ([select id,name,WCT_Eligible_for_Travel__c,wct_mobility_type__c,Immigration__r.WCT_Visa_Type__c,
           Immigration__r.WCT_Visa_Expiration_Date__c,Immigration__r.WCT_Immigration_Record_Type__c,WCT_Mobility_Employee__c,
           WCT_Visa_Expiration_Date__c,Immigration__r.WCT_Maxout_Date__c,WCT_Employee_Email_Address__c,(select WCT_I94_Expiration__c from I_94__r order by CreatedDate desc) 
           from WCT_Mobility__c where id in :mobids]);
           map<id,id> conmob=new map<id,id>();
     
    for(WCT_Assignment__c At:ast)
    {
    
                  WCT_Mobility__c con2 = mobEntry.get(at.WCT_Mobility__c);
 
                    if(con2.WCT_Mobility_Employee__c!=null)
                        {
                           conmob.put(con2.id,con2.WCT_Mobility_Employee__c);
                           contactIds.add(con2.WCT_Mobility_Employee__c) ;
                        }
    
    }
     
   Map<id,Contact>  Contactentry = new Map<id,Contact>([Select id,name,WCT_Green_Card_Expiration_Date__c,wct_contact_type__c,WCT_Green_Card_Status__c,WCT_Citizenship__c,
     (Select WCT_Passport_Expiration_Date__c from Passports__r ORDER BY CreatedDate ) from Contact where id in :contactIds]);  
     
      
   
    for(WCT_Assignment__c At:ast)
    
    {
      
      id conid=conmob.get(at.WCT_Mobility__c);
      contact con=Contactentry.get(conid);
      List<WCT_Passport__c> pt=con.passports__r;
      WCT_Mobility__c mob=mobEntry.get(at.WCT_Mobility__c);
      List<I_94__c> i94 = mob.I_94__r;
      date CurrentDate=system.today();
      date newdate=CurrentDate.addMonths(6);
      //string citzp=con.WCT_Citizenship__c.toUpperCase(); 
       
        
        if((mob.wct_mobility_type__c=='Employment Visa')&&(con.wct_contact_type__c =='Employee')&&(mob.Immigration__c!=null)&&(at.WCT_Status__c == 'Active')&&(pt.size()>0))
        
        {        
            
           if(((mob.Immigration__r.WCT_Immigration_Record_Type__c =='H1 Visa')&&(mob.Immigration__r.WCT_Visa_Expiration_Date__c > at.WCT_End_Date__c)
            &&(mob.Immigration__r.WCT_Maxout_Date__c > at.WCT_End_Date__c)&&(con.WCT_Citizenship__c!='US')&&(con.WCT_Green_Card_Status__c!='Active')
            &&(pt[0].WCT_Passport_Expiration_Date__c < newdate))
            ||
            ((mob.Immigration__r.WCT_Immigration_Record_Type__c =='L1 Visa')&&(mob.Immigration__r.WCT_Visa_Expiration_Date__c > at.WCT_End_Date__c)
            &&(mob.Immigration__r.WCT_Maxout_Date__c > at.WCT_End_Date__c )&&(pt[0].WCT_Passport_Expiration_Date__c < newdate)
            &&(con.WCT_Citizenship__c!='US')&&(con.WCT_Green_Card_Status__c!='Active'))
            ||
            (((mob.Immigration__r.WCT_Visa_Type__c =='L1B Blanket')||(mob.Immigration__r.WCT_Visa_Type__c =='L1A Blanket'))
             &&((mob.Immigration__r.WCT_Visa_Expiration_Date__c > at.WCT_End_Date__c)||(i94[0].WCT_I94_Expiration__c> at.WCT_End_Date__c))&&(mob.Immigration__r.WCT_Maxout_Date__c > ast[0].WCT_End_Date__c )
             &&(pt[0].WCT_Passport_Expiration_Date__c < newdate)&&(con.WCT_Citizenship__c!='US')&&(con.WCT_Green_Card_Status__c!='Active'))
            )
            
             {
                Task b = new Task(Subject='Renew existing Passport',ActivityDate = system.today().addDays(45),WhatId=mob.Id,RecordTypeid=rcdtype,WCT_Is_Visible_in_TOD__c=true,OwnerId=System.Label.GMI_User,WCT_Business_Owner__c=mob.WCT_Employee_Email_Address__c,WCT_Encrypt_Email_Key__c=CryptoHelper.encrypt(mob.WCT_Employee_Email_Address__c),Priority='Normal',WCT_ToD_Task_Reference__c='0141',WCT_Task_Reference_Table_ID__c=System.Label.TRT_id_Renew_passport_task,WCT_Auto_Close__c=true,Status='Not Started',whoid=mob.WCT_Mobility_Employee__c);
                tasks.add(b);
             
             }
                   
         }
         
         else if ((mob.wct_mobility_type__c=='Business Visa')&&(con.wct_contact_type__c =='Employee')&&(mob.Immigration__c!=null)&&(at.WCT_Status__c == 'Active')&&(pt.size()>0))
        
         {   
            if((mob.Immigration__r.WCT_Immigration_Record_Type__c =='B1 Visa')&&(mob.Immigration__r.WCT_Visa_Expiration_Date__c > at.WCT_End_Date__c)
             &&(pt[0].WCT_Passport_Expiration_Date__c < newdate)
             &&(con.WCT_Citizenship__c!='US')&&(con.WCT_Green_Card_Status__c!='Active'))
             {
                Task b = new Task(Subject='Renew existing Passport',ActivityDate = system.today().addDays(45),WhatId=mob.Id,RecordTypeid=rcdtype,WCT_Is_Visible_in_TOD__c=true,OwnerId=System.Label.GMI_User,WCT_Business_Owner__c=mob.WCT_Employee_Email_Address__c,WCT_Encrypt_Email_Key__c=CryptoHelper.encrypt(mob.WCT_Employee_Email_Address__c),Priority='Normal',WCT_ToD_Task_Reference__c='0143',WCT_Task_Reference_Table_ID__c=System.Label.TRT_id_Renew_passport_task_business,WCT_Auto_Close__c=true,Status='Not Started',whoid=mob.WCT_Mobility_Employee__c);
                tasks.add(b);
             
             }
                   
          }
          
          
         
        if(pt.size()>0 && ((mob.Immigration__r.WCT_Visa_Expiration_Date__c != NULL)&&(String.valueOf(mob.Immigration__r.WCT_Visa_Expiration_Date__c)!=''))){  // Added this condtion to fix the case no :01115750 on 3/2/15
         if(((mob.Immigration__r.WCT_Immigration_Record_Type__c =='H1 Visa')&&(mob.Immigration__r.WCT_Visa_Expiration_Date__c > at.WCT_End_Date__c)
            &&(mob.Immigration__r.WCT_Maxout_Date__c > at.WCT_End_Date__c )&&(pt[0].WCT_Passport_Expiration_Date__c > newdate)&&(con.WCT_Green_Card_Status__c!='Active'))
            ||
            (((mob.Immigration__r.WCT_Visa_Type__c =='L1A Individual')||(mob.Immigration__r.WCT_Visa_Type__c =='L1B Individual'))&&(mob.Immigration__r.WCT_Visa_Expiration_Date__c > at.WCT_End_Date__c)
            &&(mob.Immigration__r.WCT_Maxout_Date__c > at.WCT_End_Date__c )&&(pt[0].WCT_Passport_Expiration_Date__c > newdate)
            &&(con.WCT_Green_Card_Status__c!='Active'))
            ||
            (((mob.Immigration__r.WCT_Visa_Type__c =='L1B Blanket')||(mob.Immigration__r.WCT_Visa_Type__c =='L1A Blanket'))
             &&((mob.Immigration__r.WCT_Visa_Expiration_Date__c > at.WCT_End_Date__c)||(i94[0].WCT_I94_Expiration__c> at.WCT_End_Date__c))&&(mob.Immigration__r.WCT_Maxout_Date__c > ast[0].WCT_End_Date__c )
             &&(pt[0].WCT_Passport_Expiration_Date__c > newdate)&&(con.WCT_Green_Card_Status__c!='Active'))
            ||
            ((con.WCT_Green_Card_Status__c=='Active')&&(con.WCT_Green_Card_Expiration_Date__c > at.WCT_End_Date__c)) 
            ||
            (con.WCT_Citizenship__c=='US')
            )
            
             {
                mob.WCT_Eligible_for_Travel__c=true;
                updateeligibletravel.add(mob);
                system.debug('##########true');
                system.debug('##########true1'+mob.Immigration__r.WCT_Visa_Type__c);
             
             }
             
           else
             {
                mob.WCT_Eligible_for_Travel__c=false;
                updateeligibletravel.add(mob);
                system.debug('##########false');
                system.debug('##########false1'+mob.Immigration__r.WCT_Visa_Type__c);
                
              } 
          
          
          }
          else
             {
                mob.WCT_Eligible_for_Travel__c=false;
                updateeligibletravel.add(mob);
                system.debug('##########false');
                system.debug('##########false1'+mob.Immigration__r.WCT_Visa_Type__c);
                
              } 
          
          
         
    }
    insert tasks;
    update updateeligibletravel;
  }
 }