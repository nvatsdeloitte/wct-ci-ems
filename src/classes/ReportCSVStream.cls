public class ReportCSVStream {
    public static Boolean isTest;
    public static String strEmailAddr;
    public static String strOut;
    public static Boolean restRequested;
    public String strEmail{get;set;}
    public String strRptname{get;set;}
    
    void ReportCSVStream () {
        strOut = '';        
        }
 
   public String getCSVStream() {
        restRequested = System.isFuture() || System.isScheduled();
        executeRpt();
       
        System.debug('CALLING executeRpt:  output= ' + strOut );
         return strOut;
        }
  
    public void executeRpt() {
        String requestURL;
        requestURL = '/' + strRptname + '?csv=1&exp=1&enc=utf-8';
         
        strOut = new PageReference(requestURL).getContent().toString();
        System.debug('CALLING executeRpt:  output= ' + strOut );
    }
 
}