public class ResignationFormController extends SitesTodHeaderController {
        
    public ELE_Separation__c eleList {get;set;}
          /* Error Message related variables */
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public Contact loggedInContact {get; set;}    
    public Boolean invalidEmployee {get; set;}
    public String encryptedEmailString {get; set;}
    public String contactName {get;set;}
    public String lwd {get;set;}
    public Date slwd {get;set;}
    public String dateofResign{get;set;}
    public List<AttachmentsWrapper> UploadedDocumentList {get; set; }
    public List<Contact> listContact{get;set;} 
    
    /*Attachment Related Variables*/
    public Document doc {get;set;}
    public List<String> docIdList = new List<string>();


    /*Common Fields*/
    public String estimatedLastWorkDay {get;set;}
    public String estimatedReturnWorkDay {get;set;}
    public String comments {get;set;}
    public String formType {get;set;}
    public String sContact {get;set;}
    public String empEmail{get;set;}
        public ResignationFormController()
        {
                pageError = false;
                eleList = new ELE_Separation__c();
                //eleList = [Select e.US_Career_Model_Mapping_Separation__c, e.SystemModstamp, e.Service_Line_Separation__c, e.Service_Area_Separation__c, e.SAP_Separation_code_Separation__c, e.Region_Separation__c, e.RecordTypeId, e.RC_Code_Separation__c, e.Pers_No_Separation__c, e.Period_Of_Exit_Separation__c, e.OwnerId, e.Onsite_exit_Separation__c, e.Name_Separation__c, e.Name, e.Location_Separation__c, e.Last_Date_Separation__c, e.LastViewedDate, e.LastReferencedDate, e.LastModifiedDate, e.LastModifiedById, e.IsDeleted, e.Id, e.Global_Person_Number_Separation__c, e.Gender_Separation__c, e.Function_Separation__c, e.F_F_complete_Separation__c, e.FSS_Separation__c, e.Exit_Separation__c, e.Exit_Intw_Separation__c, e.Exit_Clearance_Complete_Separation__c, e.Entity_Separation__c, e.Email_id_Separation__c, e.Eligible_for_Rehire_Separation__c, e.Eligible_for_Alumni_Separation__c, e.Designation_Separation__c, e.Date_of_resignation_Separation__c, e.Date_of_action_move_Separation__c, e.DOJ_Separation__c, e.CreatedDate, e.CreatedById, e.Clearance_Records_Created__c From ELE_Separation__c e];
        
                invalidEmployee = false;

        //Get Employee Email Id
        encryptedEmailString = ApexPages.currentPage().getParameters().get('em');

        //Decrypt and Encrypt only when Flag is updated.
        empEmail = cryptoHelper.decrypt(encryptedEmailString);
        System.debug('empEmail === '+empEmail);
        if( (null != empEmail) && ('' != empEmail) ) {
             listContact = [
                                            SELECT 
                                                Id,
                                                Name,
                                                FirstName,
                                                WCT_Middle_Name__c,
                                                WCT_ExternalEmail__c,
                                                WCT_Relationship__c,
                                                WCT_Personnel_Number__c,
                                                WCT_Internship_Start_Date__c,
                                                WCT_Entity__c,
                                                LastName,WCT_Last_Day_Worked__c,
                                                Email,ELE_Open_ELE_case__c ,
                                                Phone
                                         //       (select id,contact_name_email__c,WCT_LastWorkDate__c,date_of_resignation__c from cases where contact_name_email__c =:empEmail ) 
                                            FROM
                                                Contact
                                            WHERE
                                                Email = :empEmail and RecordType.Name = 'Employee'
                                            LIMIT
                                                1   
                                          ];
            
        
        
        
            if( (null != listContact) && (listContact.size() > 0) ) {
                sContact = listContact[0].id;
                loggedInContact = listContact[0];
                contactName = loggedInContact.Name;
                lwd = string.valueOf(loggedInContact.WCT_Last_Day_Worked__c);
                if(loggedInContact.WCT_Last_Day_Worked__c != null || lwd !='')
                    eleList.ELE_Last_Date__c = loggedInContact.WCT_Last_Day_Worked__c; 
                eleList.ELE_Contact_Email__c= empEmail;
                eleList.ELE_case_Type__c = 'Voluntary';
                eleList.ELE_Date_of_resignation__c= system.today();
                eleList.ELE_Last_Date__c= eleList.ELE_Date_of_resignation__c.addDays(59);     
               // DateTime myDateTime = (DateTime) eleList.ELE_Last_Date__c;
               // String dayOfWeek = myDateTime.format('EEEE');  
               date diffdt=Date.newinstance(1985,6,24);
               
                List<String> listDay = new List<String>{'Monday' , 'Tuesday' , 'Wednesday' , 'Thursday' , 'Friday','Saturday' , 'Sunday'};  
                date selectedDate = eleList.ELE_Last_Date__c;  
                 system.debug('>>selectedDate >>>>>>>>>>>>>>>>>>>>>>>>'+selectedDate );
                Integer remainder = Math.mod(diffdt.daysBetween(selectedDate) , 7);  
                                 system.debug('>>remainder >>>>>>>>>>>>>>>>>>>>>>>>'+remainder );

                string dayOfWeek = listDay.get(remainder); 
              
               system.debug('>>dayOfWeek >>>>>>>>>>>>>>>>>>>>>>>>'+dayOfWeek);

                if(dayOfWeek == 'Saturday'){
                    slwd = eleList.ELE_Last_Date__c.addDays(2);
                 }
                else if(dayOfWeek == 'Sunday'){
                    slwd = eleList.ELE_Last_Date__c.addDays(1);
                    invalidEmployee = false;
                 }else
                 {
                      slwd =eleList.ELE_Last_Date__c;
                 }
                 eleList.ELE_Last_Date__c = slwd;
               }
            else {
                invalidEmployee = true;
                
            }
          
        }
        else {
            invalidEmployee = true;
        }
        doc = new Document();
        UploadedDocumentList = new List<AttachmentsWrapper>();
        }
        public PageReference redirect(){ 
        if(listContact != null && listContact.size()>0 && listContact[0].ELE_Open_ELE_case__c > 0)
        { 
            PageReference pageErrRef = Page.ELE_ExitFormErrorMessagePage;
            return pageErrRef;
        }
        else{
            return null;
        }
        
        }
        public Pagereference saveResignRecord()
        {
                Boolean bSubmit =false,bSubmitFss = false;
            PageReference pageRef = Page.ELE_ThankyouPage; 
           // pageRef.getParameters().put('em',encryptedEmailString);
            System.debug('Submit Button Clicked'+eleList);
            if(UploadedDocumentList.size()==0) 
            {
                  pageErrorMessage = 'Attachment is Required';//call it from label
                  pageError = true;
            }
            else
            {
                boolean atleastOneSelected=false;
                for(AttachmentsWrapper wrapper : UploadedDocumentList)
                {
                    if(wrapper.isSelected==true)
                    {
                        atleastOneSelected=true;
                        break;
                    }
                    
                }
                if(!atleastOneSelected)
                {
                  pageErrorMessage = 'Attachment is Required';//call it from label
                  pageError = true;
                
                }
                
            } 
            
               if(eleList != null &&  eleList.ELE_Email_id__c != null && eleList.ELE_Address__c != null && eleList.ELE_Mobile_Number__c != null && eleList.ELE_Alternate_Contact_Number__c!=null && eleList.ELE_Date_of_resignation__c != null && eleList.ELE_Last_Date__c != null && eleList.ELE_Location__c != null && eleList.ELE_FSS__c != null && eleList.ELE_Consulted__c != null && eleList.ELE_Reason_for_Resignation__c != null && eleList.ELE_Work_Location__c != null )
                {
                     eleList.ELE_Region__c = 'USI';  
                     eleList.ELE_Contact__c = sContact;
                     if(eleList.ELE_Work_Location__c == 'Onsite Assignment')
                     {
                        if(eleList.ELE_Onsite_Office_Location__c != null && eleList.ELE_Future_Employer_Name__c != null && eleList.ELE_Future_Company_Start_Date__c != null && eleList.ELE_Future_company_designation__c != null)
                                bSubmit = true;
                     }
                     else
                        bSubmit = true;
                     if(eleList.ELE_FSS__c == 'Consulting' || eleList.ELE_FSS__c == 'TAX' || eleList.ELE_FSS__c == 'FAS' ||eleList.ELE_FSS__c == 'AERS')
                     {
                       if(eleList.ELE_Counselor_Name__c != '' && eleList.ELE_Counselor_Email_ID__c != '' && eleList.ELE_Project_Manager_Name__c != '' && eleList.ELE_Project_Manager_Email_ID__c != '' && eleList.ELE_Resource_Manger_Name__c != '' && eleList.ELE_Resource_Manger_Email_ID__c != '')
                                bSubmitFss = true;
                     }
                     else if(eleList.ELE_FSS__c == 'Enabling Areas')
                     {
                        if(eleList.ELE_Counselor_Name__c != '' && eleList.ELE_Counselor_Email_ID__c != '')
                                bSubmitFss = true;
                     }
                     else if(eleList.ELE_FSS__c == 'Global')
                     {
                        if(eleList.ELE_Counselor_Name__c != '' && eleList.ELE_Counselor_Email_ID__c != '' && eleList.ELE_Project_Manager_Name__c != '' && eleList.ELE_Project_Manager_Email_ID__c != '' )
                                bSubmitFss = true;
                     }
                     if(bSubmit && bSubmitFss)
                     {
                       try
                       {
                        if(eleList != null )
                        {
                            system.debug('eleList is ===== ? '+eleList);
                            insert eleList;
                        }
                        }
                        catch(Exception e)
                        {
                            System.debug('Exception == '+e);
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Some unexpected error occured, please try and submit again.');
                            ApexPages.addMessage(myMsg);
                            return null;
                        }
                     
                     }
                }
                if(eleList.id!=null){
                uploadRelatedAttachment();
                }
              System.debug('*******+++ === '+pageRef);  
              return pageRef;  
        }
        public void uploadAttachment(){
        pageError = false;
        pageErrorMessage = '';
    
        if(ApexPages.hasMessages()) {
            pageErrorMessage = '';
            for(ApexPages.Message message : ApexPages.getMessages()) {
                pageErrorMessage += message.getSummary() + '\n';
            }            
            doc = new Document();
            pageError = true;
            System.debug('Page Has Error Messages . . ');
            return;
        }                                
        
        doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
        if(doc.body != null) {
            insert doc;
            docIdList.add(doc.id);
            doc = [SELECT id, name, bodylength FROM Document WHERE id = :doc.id];

            String size = '';
            if(1048576 < doc.BodyLength) {
                // Size greater than 1MB
                size = '' + (doc.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < doc.BodyLength) {
                // Size greater than 1KB
                size = '' + (doc.BodyLength / 1024) + ' KB';            
            }
            else {
                size = '' + doc.BodyLength + ' bytes';
            }
            UploadedDocumentList.add(new AttachmentsWrapper(true, doc.name, doc.id, size));
            doc = new Document();
        }
    }
    
    public void uploadRelatedAttachment()
    {
            if(!docIdList.isEmpty()) { //If there is any Documents Attached in Web Form

            List<String> selectedDocumentId = new List<String>();
            for(AttachmentsWrapper aWrapper : UploadedDocumentList) {
                if(aWrapper.isSelected) {
                    selectedDocumentId.add(aWrapper.documentId);
                }
            }
            
            /* Select Documents which are Only Active (Selected) */
            List<Document> selectedDocumentList = new List<Document>();
            if(!selectedDocumentId.isEmpty()){
                selectedDocumentList = [
                                           SELECT 
                                               id,
                                               name,
                                               ContentType,
                                               Type,
                                               Body 
                                           FROM 
                                               Document 
                                           WHERE 
                                               id IN :selectedDocumentId
                                        ];
            }
            
            /* Convert Documents to Attachment */
            List<Attachment> attachmentsToInsertList = new List<Attachment>();
            for(Document doc : selectedDocumentList){
                Attachment a = new Attachment();
                a.body = doc.body;
                a.ContentType = doc.ContentType;
                a.Name= doc.Name;
                system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>eleList.id>>>>>>'+eleList.id);
                a.parentid = eleList.id; 
                attachmentsToInsertList.add(a);                
            }
            
            if(!attachmentsToInsertList.isEmpty()){
                insert attachmentsToInsertList;
            }
            
            List<Document> listDocuments = new List<Document>();
            listDocuments = [
                                SELECT
                                    Id
                                FROM
                                    Document
                                WHERE
                                    Id IN :docIdList
                            ];
            if( (null != listDocuments) && (0 < listDocuments.size()) ) {
                delete listDocuments;
            }
        }
    
    }
        //Documents Wrapper
    public class AttachmentsWrapper {
        public Boolean isSelected {get;set;}
        public String docName {get;set;}
        public String documentId {get;set;}
        public String size {get; set;}
        
        public AttachmentsWrapper(Boolean selected, String Name, String Id, String size){
            isSelected = selected;
            docName = Name ;
            documentId = Id;
            this.size = size;
        }        
    } 
}