/*****************************************************************************************
    Name    : SCCContactCasesInContact
    Desc    : Controller Class to Display All the Contact Cases in Service Cloud Console
              'SCCContactCasesInContacts' Page
    Approach: 
                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
 Sreenivasa Munnangi            5 Aug, 2013         Created 
******************************************************************************************/

public with sharing class  SCCContactCasesInContact {
    public Contact con; 
    public list<Case> Contact_Cases {get; set;}
    public Integer CountTotalRecords{get;set;}
    public Integer OffsetSize = 0;
    private Integer QueryLimit = 10;
        
    public SCCContactCasesInContact(ApexPages.StandardController controller){
        this.con = (Contact)controller.getRecord();
        if(con <> null && con.Id <> null){
            CountTotalRecords= [Select count() from Case where ContactId=:con.Id];
        } else {
            CountTotalRecords = 0;
        }
    }
    
    public list<Case> getCases(){
        Contact_Cases = new list<Case>();
        if(con <> null && con.Id <> null){
            Contact_Cases = [Select Id, CaseNumber,Contact.Name, Subject, Priority, CreatedDate,WCT_Category__c,Owner.Name,ClosedDate, Status from Case where ContactId=:con.Id limit :QueryLimit offset :OffsetSize];
        }
        return Contact_Cases;
    }
    
    public Boolean getDisablePrevious(){
        if(OffsetSize>0){
            return false;
        }
        else return true;
    }
 
    public Boolean getDisableNext() {
        if (OffsetSize + QueryLimit < countTotalRecords){
            return false;
        }
        else return true;
    }

    public PageReference Beginning() {
      OffsetSize = 0;
      return null;
    }  
    
    public PageReference previous() {
        OffsetSize -= QueryLimit;
        return null;
    }
  
    public PageReference next() {
        OffsetSize += QueryLimit;
        return null;
    }

    public PageReference End() {
      if(math.mod(CountTotalRecords, QueryLimit) > 0) {
          OffsetSize = CountTotalRecords - math.mod(CountTotalRecords, QueryLimit);
      } else {
          OffsetSize = CountTotalRecords - QueryLimit;
      }
      return null;
    } 
   
    public Integer getPageNumber() {
      return OffsetSize/QueryLimit + 1;
    }
 
    public Integer getTotalPages() {
      if (math.mod(CountTotalRecords, QueryLimit) > 0) {
         return CountTotalRecords/QueryLimit + 1;
      } else {
         return (CountTotalRecords/QueryLimit);
      }
    }      
}