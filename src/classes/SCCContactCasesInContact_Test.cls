/*****************************************************************************************
    Name    : SCCContactCasesInContact_Test
    Desc    : Test class for SCCContactCasesInContact class                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Rahul Agarwal                 09/11/2013         Created 
******************************************************************************************/
@isTest
/****************************************************************************************
    * @author      - Rahul Agarwal
    * @date        - 09/11/2013
    * @description - Test Method for SCCContactCasesInContact controller class
    *****************************************************************************************/
public class SCCContactCasesInContact_Test{
    public static testmethod void SCCContactCasesInContact(){
        test.startTest();
        // Creating Contact data
        List<Contact> lstContacts = Test_Data_Utility.createContact();
        Contact con = lstContacts[0];
        Test_Data_Utility.createCase(con.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(con);
        SCCContactCasesInContact cont = new SCCContactCasesInContact(controller);
        cont.getCases();
        cont.getDisablePrevious();
        cont.getDisableNext();
        cont.Beginning();
        cont.previous();
        cont.next();
        cont.End();
        cont.getPageNumber();
        cont.getTotalPages();
        
        test.stopTest();
    }
}