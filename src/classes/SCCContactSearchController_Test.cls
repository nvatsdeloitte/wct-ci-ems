/*****************************************************************************************
    Name    : SCCContactSearchController_Test
    Desc    : Test class for SCCContactSearchController class                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Rahul Agarwal                 09/11/2013         Created 
******************************************************************************************/
@isTest
/****************************************************************************************
    * @author      - Rahul Agarwal
    * @date        - 09/11/2013
    * @description - Test Method for SCCContactSearchController controller class
    *****************************************************************************************/
public class SCCContactSearchController_Test{
    public static testmethod void SCCContactSearchController(){
    	test.startTest();
    	SCCContactSearchController cont = new SCCContactSearchController();
    	// setting the parameters
    	System.currentPageReference().getParameters().put('firstname','firstname');
    	System.currentPageReference().getParameters().put('lastname','lastname');
    	System.currentPageReference().getParameters().put('personnelNumber','personnelNumber');
    	System.currentPageReference().getParameters().put('prefferedName','prefferedName');
    	System.currentPageReference().getParameters().put('priorName','priorName');
    	System.currentPageReference().getParameters().put('ssnLast4digits','1111');
    	System.currentPageReference().getParameters().put('facilityName','facilityName');
    	// creating contact data
    	Test_Data_Utility.createContact();
    	String s = cont.debugSoql;
    	cont.runSearch();
    	test.stopTest();
    }
}