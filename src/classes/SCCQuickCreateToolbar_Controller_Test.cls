/*****************************************************************************************
    Name    : SCCQuickCreateToolbar_Controller_Test
    Desc    : Test class for SCCQuickCreateToolbar_Controller class                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Rahul Agarwal                 09/11/2013         Created 
******************************************************************************************/
@isTest
public class SCCQuickCreateToolbar_Controller_Test{
    /****************************************************************************************
    * @author      - Rahul Agarwal
    * @date        - 09/11/2013
    * @description - Test Method for SCCQuickCreateToolbar_Controller controller class with Case record
    *****************************************************************************************/
    public static testmethod void SCCQuickCreateToolbar_Controller_Case(){
        // Creating Case
        Test_Data_Utility.createCase();
        Case c = [select Id from Case limit 1];
        test.startTest();
        System.currentPageReference().getParameters().put('id',c.Id);
        SCCQuickCreateToolbar_Controller cont = new SCCQuickCreateToolbar_Controller();
        String currObjectType = cont.currObjectType;
        Sobject currRecord = cont.currRecord;
        list<selectOption> caseRecordTypes = cont.caseRecordTypes;
        String caseCreateURLPrefix = cont.caseCreateURLPrefix;
        test.stopTest();
    }
    /****************************************************************************************
    * @author      - Rahul Agarwal
    * @date        - 09/11/2013
    * @description - Test Method for SCCQuickCreateToolbar_Controller controller class with Contact record
    *****************************************************************************************/
    public static testmethod void SCCQuickCreateToolbar_Controller_Contact(){
        // Creating Contact
        Test_Data_Utility.createContact();
        Contact c = [select Id from Contact limit 1];
        test.startTest();
        System.currentPageReference().getParameters().put('id',c.Id);
        SCCQuickCreateToolbar_Controller cont = new SCCQuickCreateToolbar_Controller();
        String currObjectType = cont.currObjectType;
        Sobject currRecord = cont.currRecord;
        list<selectOption> caseRecordTypes = cont.caseRecordTypes;
        String caseCreateURLPrefix = cont.caseCreateURLPrefix;
        test.stopTest();
    }
    public static testmethod void SCCQuickCreateToolbar_Controller_Case_CIC_Manager(){
        // Creating Case
        Profile p = [SELECT Id FROM Profile WHERE Name= 'System Administrator']; 
        User testuser= WCT_UtilTestDataCreation.createUser('testest',p.id,'testoffer@test.com','testoffer@test.com');
        testuser.UserRoleId=[SELECT Id,DeveloperName FROM UserRole WHERE DeveloperName='CIC_Manager'].id; 
        insert testuser;
        System.runAs ( testuser){
        Test_Data_Utility.createCase();
        Case c = [select Id from Case limit 1];
        test.startTest();
        System.currentPageReference().getParameters().put('id',c.Id);
        SCCQuickCreateToolbar_Controller cont = new SCCQuickCreateToolbar_Controller();
        String caseCreateURLPrefix = cont.caseCreateURLPrefix;
        test.stopTest();
        }
    }
     public static testmethod void SCCQuickCreateToolbar_Controller_Case_CIC_Manager_USI(){
        // Creating Case
        Profile p = [SELECT Id FROM Profile WHERE Name= 'System Administrator']; 
        User testuser= WCT_UtilTestDataCreation.createUser('testest',p.id,'testoffer@test.com','testoffer@test.com');
        testuser.UserRoleId=[SELECT Id,DeveloperName FROM UserRole WHERE DeveloperName='CIC_Manager_USI'].id; 
        insert testuser;
        System.runAs ( testuser){
        Test_Data_Utility.createCase();
        Case c = [select Id from Case limit 1];
        test.startTest();
        System.currentPageReference().getParameters().put('id',c.Id);
        SCCQuickCreateToolbar_Controller cont = new SCCQuickCreateToolbar_Controller();
        String caseCreateURLPrefix = cont.caseCreateURLPrefix;
        test.stopTest();
        }
    }
    public static testmethod void SCCQuickCreateToolbar_Controller_Case_CIC_Manager_PSN(){
        // Creating Case
        Profile p = [SELECT Id FROM Profile WHERE Name= 'System Administrator']; 
        User testuser= WCT_UtilTestDataCreation.createUser('testest',p.id,'testoffer@test.com','testoffer@test.com');
        testuser.UserRoleId=[SELECT Id,DeveloperName FROM UserRole WHERE DeveloperName='CIC_Manager_PSN'].id; 
        insert testuser;
        System.runAs ( testuser){
        Test_Data_Utility.createCase();
        Case c = [select Id from Case limit 1];
        test.startTest();
        System.currentPageReference().getParameters().put('id',c.Id);
        SCCQuickCreateToolbar_Controller cont = new SCCQuickCreateToolbar_Controller();
        String caseCreateURLPrefix = cont.caseCreateURLPrefix;
        test.stopTest();
        }
    }
}