Public class SOSLController{
public string searchText {get;set;}
public string StartDate {get;set;}
public string EndDate {get;set;}    
public boolean clearError{get;set;}
public boolean getclearError()
    {
       if(StartDate !='' || EndDate !='')
       {
       return true;
       }
       else
       {
       return true;
       }
    }
    public list<list<sObject>> soslResults {
        get {
            if(searchText != null && searchText != '') {
             if(startdate != '' && enddate != ''){
            date sdate= date.parse(startdate);
            date edate= date.parse(enddate);
            edate= edate.adddays(1);
                soslResults = [FIND :searchText IN ALL FIELDS RETURNING   attachment(id,name,Createddate where CreatedDate >=:Sdate and CreatedDate <=:Edate  ORDER BY Createddate)];
          }
          else if(startdate !='' && enddate == '')
          {
          date sdate= date.parse(startdate);
            
          soslResults = [FIND :searchText IN ALL FIELDS RETURNING   attachment(id,name,Createddate where CreatedDate >=:Sdate ORDER BY Createddate)];
          }
          else if(startdate =='' && enddate != '')
          {
          
            date edate= date.parse(enddate);
            edate= edate.adddays(1);
           soslResults = [FIND :searchText IN ALL FIELDS RETURNING   attachment(id,name,Createddate where CreatedDate <=:Edate ORDER BY Createddate)];
          }
          Else {
            soslResults = [FIND :searchText IN ALL FIELDS RETURNING   attachment(id,name,Createddate ORDER BY Createddate)];
            }
            }
            return soslResults;
        }
        set;
    }
    public list<list<sObject>> soslResults1 {
        get {
            if(searchText != null && searchText != '') {
            if(Enddate !='' && startdate !=''){
            date sdate= date.parse(startdate);
            date edate= date.parse(enddate);
            edate= edate.adddays(1);
                soslResults1 = [FIND :searchText IN ALL FIELDS RETURNING   task(id,subject,Createddate where CreatedDate >=:Sdate and CreatedDate <=:Edate ORDER BY Createddate )];
            }
             else if(startdate !='' && enddate == '')
          {
          date sdate= date.parse(startdate);
            
          soslResults1 = [FIND :searchText IN ALL FIELDS RETURNING   task(id,subject,Createddate where CreatedDate >=:Sdate ORDER BY Createddate )];
           
          }
           else if(startdate =='' && enddate != '')
          {
           date edate= date.parse(enddate);
            edate= edate.adddays(1);
           soslResults1 = [FIND :searchText IN ALL FIELDS RETURNING   task(id,subject,Createddate where CreatedDate <=:Edate ORDER BY Createddate)];
          }
            Else  {
            soslResults1 = [FIND :searchText IN ALL FIELDS RETURNING   task(id,subject,Createddate ORDER BY Createddate)];
            }
            }
            return soslResults1;
        }
        set;
    }
    
    
   @TestVisible public class searchResultRow{
        public string sObjectTypeName {get;set;}
        public sObject record {get;set;}
        
        public searchResultRow(sObject pObject) {
            record = pObject;
            sObjectTypeName = pObject.getSObjectType().getDescribe().getLabel();
        }
    }   
    
  @TestVisible public class searchResultRow1{
        public string sObjectTypeName1 {get;set;}
        public sObject record1 {get;set;}
        
        public searchResultRow1(sObject pObject) {
            record1 = pObject;
            sObjectTypeName1 = pObject.getSObjectType().getDescribe().getLabel();
        }
    } 
    
    public list<searchResultRow> searchResults {
        get {
            searchResults = new list<searchResultRow>();
            try{
            if(soslResults != null) {
                //Loop through the list of list of sObjects from our sosl query
                for(list<sObject> objectList : soslResults) {
                    for(sObject obj : objectList) {
                        searchResults.add(
                            new searchResultRow(obj)
                        );
                    }
                    
                }
                          
            }
             return searchResults;
             }
             catch(exception e)
             {
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Enter Valid Date '));
              return searchResults;
             }
            
            
            
            
        }
        set;
    }
    
    public list<searchResultRow1> searchResults1 {
        get {
            searchResults1 = new list<searchResultRow1>();
    try{        
            if(soslResults1 != null) {
                //Loop through the list of list of sObjects from our sosl query
                for(list<sObject> objectList : soslResults1) {
                    for(sObject obj : objectList) {
                        searchResults1.add(
                            new searchResultRow1(obj)
                        );
                    }
                    
                }
               
            }
            return searchResults1;
          }
          catch(exception e)
          {
              //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Date is Invalid'));
              return searchResults1;
          }  
            
            
           
            
        }
        set;
    }
}