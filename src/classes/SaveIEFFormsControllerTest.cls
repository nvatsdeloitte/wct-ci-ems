// Modified By shruthi Reddy.Gone 27 January 2015.

@isTest
public class SaveIEFFormsControllerTest
{
    public static testmethod void m1()
    {           
         test.startTest();
         
        WCT_List_Of_Names__c IEF = new WCT_List_Of_Names__c(Name='Test IEF',WCT_Type__c='IEF',WCT_User_Group__c='US',WCT_FSS__c='Consulting',WCT_Job_Type__c='Experienced',RecordTypeId=SYSTEM.Label.WCT_IEFLibraryRecordtypeId);
        insert IEF;
        
        Contact con = new Contact(LastName = 'TestContact',phone='8686322286',Email ='tintegration@deloitte.com'); 
        insert con;
        Contact con2 = new Contact(LastName = 'TestContact',phone='8686322286',Email ='tintegration2@deloitte.com'); 
        insert con2; 
        
        Contact con3 = new Contact(LastName = 'TestContact',phone='8686322286',Email ='tintegration3@deloitte.com'); 
        insert con3;
        
        WCT_Interview__c interview = new WCT_Interview__c(WCT_FSS__c='Consulting',WCT_Job_Type__c='Experienced',WCT_User_group__c='US',WCT_Clicktools_Form__c=IEF.id);
        insert interview;
        
        WCT_Requisition__c req = new WCT_Requisition__c(Name = 'Test');
        insert req;
        
        WCT_Candidate_Requisition__c canReq = new WCT_Candidate_Requisition__c(WCT_Contact__c = con.Id, WCT_Requisition__c = req.Id);
        insert canReq; 
        
        
        WCT_Interview_Junction__c intrJunct = new WCT_Interview_Junction__c(WCT_Interview__c=interview.Id, WCT_Candidate_Tracker__c = canReq.Id);
        insert intrJunct;
                
       
        list<WCT_Interview_Junction__c> list1=[SELECT Id,WCT_Job_Type2__c,WCT_FSS__c,WCT_User_group__c from WCT_Interview_Junction__c limit 10];
       
        system.debug('%%'+interview.ID+'itlist'+list1);
            
            
            PageReference pageRef = Page.WCT_select_IEF;
            Test.setCurrentPageReference(pageRef);
           
            ApexPages.StandardSetController sc = New ApexPages.StandardSetController(New List<WCT_Interview_Junction__c> ());   
            SaveIEFFormsController Controller1 = new  SaveIEFFormsController(sc); 
            pagereference prf  = Controller1.updaterec(); 
        
            ApexPages.StandardSetController c = New ApexPages.StandardSetController(list1);
            c.setSelected(list1);
            ApexPages.currentPage().getParameters().put('retURL', '/' + interview.ID);

            SaveIEFFormsController Controller = new  SaveIEFFormsController(c); 
            pagereference p  = Controller.dosomething(); 
            pagereference pr  = Controller.updaterec(); 
       
            test.stopTest();        
      }
}