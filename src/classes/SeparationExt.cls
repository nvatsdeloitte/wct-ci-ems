public class SeparationExt {
    public string elesepid;
    sobject elesepobj;
    public string objtyp{get;set;}
    ApexPages.StandardController stdController;
    
    public SeparationExt(ApexPages.StandardController Controller)
    {
      objtyp=apexpages.currentPage().getparameters().get('id');
      objtyp= objtyp.substring(0,3);
        
      stdController=Controller;
      if(objtyp==system.label.ELESepid)
      {
      this.elesepobj = (ELE_Separation__c)stdController.getRecord();
      }
      else if(objtyp==system.label.ELEclearanceid)
      this.elesepobj = (Clearance_separation__c)stdController.getRecord();    
    }
    public void updateEmpcmmt()
    {
      boolean chk;
      if(objtyp==system.label.ELESepid)
      {
      chk=(boolean)elesepobj.get('Employee_Comment_Unread__c');
      elesepobj.put('Employee_Comment_Unread__c', false);
      }
      else if(objtyp==system.label.ELEclearanceid)
      {
      chk=(boolean)elesepobj.get('Stakeholder_Comment_Unread__c');
      elesepobj.put('Stakeholder_Comment_Unread__c', false);    
      }
      system.debug('%%%%%'+chk);  
      if(chk==true)
        {
            system.debug('%%%%%'+elesepobj); 
        update elesepobj;
        }
    }
}