public class SitesTaskRedirectController extends SitesTodHeaderController {
    public String url {get; set;}
    
    public SitesTaskRedirectController() {
        url = null;
        
        if(invalidEmployee) {
            return;
        }                               
    }    
    
    public PageReference redirect() {
        String taskId = ApexPages.currentPage().getParameters().get('id');
        if(null == taskId) {
            return null;
        }
        
        List<Task> listTasks = new List<Task>();
        listTasks = [
                        SELECT
                            WCT_Task_Reference_Table_ID__c
                        FROM
                            Task
                        WHERE
                            Id = :taskId
                        LIMIT
                            1
                    ];
        if( (null != listTasks) && (0 != listTasks.size()) && 
            (null != listTasks[0].WCT_Task_Reference_Table_ID__c) ) {                            
            List<WCT_Task_Reference_Table__c> listTRT = new List<WCT_Task_Reference_Table__c>();
            listTRT = [
                          SELECT
                              WCT_Page_Name__c,WCT_Dynamic_URL__c
                          FROM
                              WCT_Task_Reference_Table__c
                          WHERE
                              Id = :listTasks[0].WCT_Task_Reference_Table_ID__c
                          LIMIT
                              1
                      ];
            if( (null != listTRT) && (0 != listTRT.size()) && (null != listTRT[0].WCT_Page_Name__c) ) {
                url = '/apex/' + listTRT[0].WCT_Page_Name__c;
                if(listTRT[0].WCT_Dynamic_URL__c==null)
                	url = url + '?taskid=' + taskId + '&em=' + CryptoHelper.encrypt(loggedInContact.Email);
                else
                	url = url + '?taskid=' + taskId + '&em=' + CryptoHelper.encrypt(loggedInContact.Email) +'&'+listTRT[0].WCT_Dynamic_URL__c;
            }
        }
            
        return new PageReference(url);
    }
}