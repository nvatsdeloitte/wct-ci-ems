public abstract class SitesTodHeaderController {

    public Contact loggedInContact {get; set;}    
    public Boolean invalidEmployee {get; set;}
    public Boolean isDeloitteIndia {get; set;}
    public String encryptedEmailString {get; set;}
    
    
    public SitesTodHeaderController() {
      try{
        invalidEmployee = true;
        isDeloitteIndia=false;
        //Get Employee Email Id
        String encryptedEmailString = ApexPages.currentPage().getParameters().get('em');
       if(String.IsEmpty(encryptedEmailString)) { 
        Cookie emValue = ApexPages.currentPage().getCookies().get('em');
        system.debug('---emValue-----'+emValue);
        if(emValue  != null) {
         encryptedEmailString =emValue.getValue();

         }

        }
        //Decrypt and Encrypt only when Flag is updated.
                system.debug('--------encryptedEmailString---------'+encryptedEmailString);
        if(String.IsNotEmpty(encryptedEmailString)){
      //  String empEmail=CryptoHelper.decryptURLEncoded(encryptedEmailString);
         String empEmail = CryptoHelper.decrypt(encryptedEmailString);

        system.debug('--------empEmail---------'+empEmail);
       
        if( (null != empEmail) && ('' != empEmail) ) {
            List<Contact> listContact = [
                                            SELECT 
                                                Id,
                                                Name,
                                                FirstName,
                                                WCT_Middle_Name__c,
                                                WCT_ExternalEmail__c,
                                                WCT_Relationship__c,
                                                WCT_Personnel_Number__c,
                                                WCT_Internship_Start_Date__c,
                                                WCT_Entity__c,
                                                LastName,
                                                Email,
                                                WCT_Function__c,
                                                Phone, 
                                                WCT_PS_Group__c,
                                                WCT_Job_Level_Text__c,
                                                Contact.Account.Name,WCT_Gender__c,
                                                WCT_Most_Recent_Rehire__c,WCT_Original_Hire_Date__c
                                            FROM
                                                Contact
                                            WHERE
                                                Email = :empEmail
                                            LIMIT
                                                1   
                                          ];
            if( (null != listContact) && (listContact.size() > 0) ) {
            
             system.debug('###############' + listContact[0].id);
             system.debug('-----------listContact Accoount Name----------' + listContact[0].Account.Name);   
                loggedInContact = listContact[0];
                invalidEmployee = false;
                if(String.IsNotEmpty(listContact[0].Account.Name) && listContact[0].Account.Name.toLowerCase().Contains('india')){
                    isDeloitteIndia=true;
                }else{
                    isDeloitteIndia=false;
                    }   
            }
            else {
                invalidEmployee = true;
            }
            
            
            
        }
        else {
            invalidEmployee = true;
        }
        }
         system.debug('-----------isDeloitteIndia----------' + isDeloitteIndia);
        
             //        system.debug('#$$$$$$$$$$##' + loggedInContact.id);
     }
    catch(Exception ex){
     isDeloitteIndia=false;
    system.debug('----Exception----'+ex.getMessage()+'----at Line #---'+ex.getLineNumber());
    }
    }
    
    
      /* Wrapper Class for Cookie */
        public class cookieWrap {
       
        // The controller extension does the work
        public cookieWrap(String emValue) {
            
            
            Cookie emCookie= new Cookie('em',emValue,null,5,false);//315569260

               
            //Set the page cookies using the setCookies() method
            ApexPages.currentPage().setCookies(new Cookie[]{emCookie});
        }
    
    }//end cookieWrap inner class
}