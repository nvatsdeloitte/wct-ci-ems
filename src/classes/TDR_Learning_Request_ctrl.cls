Public without sharing class  TDR_Learning_Request_ctrl extends SitesTodHeaderController
{

 
   /****************************************************************************************************************************************** 
    * Class Name   : TDR_Learning_Request_ctrl 
    * Description  : This Class is for Learning Request Forms  
    * Created By   : Deloitte India.(Shruthi Reddy)
  *****************************************************************************************************************************************/

    /*Public Variables*/
    
    Public Case_form_Extn__c ocaseformextn {get;set;}
    Public List<Case_form_Extn__c> licaseformextn;// {get;set;}
    Public Map<string,Case_form_Extn__c> caseformmap;// {get;set;}
    Public Case Ocase; //{get;set;} 
    Public List<RecordType> OcaseRecordType;// {get;set;}
    public String recordType {get;set;}
    public String requesttype {get;set;}
    public String reqtype {get;set;}
    public String additionalcomments;//{get;set;}
    public list<innercl> dummyinn{get;set;}
    public List<RecordType> oCaseExtnrecordtypes {get; set;}
    public List<SelectOption> orecordtypes {get; set;}
    public String dlccategorySelection{get;set;}
    Public string iscentraloginisuue{get;set;}
    
    Public Document doc {get;set;}
    public List<String> docIdList = new List<string>();
    public List<AttachmentsWrapper> UploadedDocumentList {get; set; }
    public static List<AttachmentsWrapper> UploadedDocumentListLocalHost =new List<AttachmentsWrapper>();
    public boolean addAttachment{get; set;}
    public string setpopdisplay{get;set;}
    public List<Document> selectedDocumentList {get;set;}
    
    
    /* Error Message related variables */
    public boolean pageError {get; set;}
    public boolean showlist{get; set;}
    public boolean showthankyumsg{get; set;}
    public boolean RenderFunction{get; set;}
    public boolean RenderFieldSetBlock{get; set;}
    public String pageErrorMessage {get; set;}
    public string formname{get;set;}
    public List<WCT_List_of_Names__c> listOfNames {get; set;}
    Public Map<string,WCT_List_of_Names__c> listofnamesMap {get;set;}
    Public Map<string,Id> caseformextrecordtypeMap{get;set;}
    
    public boolean displayPopup{get; set;}
    Public string contactemailid{get;set;}
    public string dlcAPI{get;set;}
   
    
    
    Public TDR_Learning_Request_ctrl(){
        
      //Create New Record in case form extension Obj      
        ocaseformextn = new Case_form_Extn__c ();
        caseformmap =  new Map<string,Case_form_Extn__c> ();
        Ocase = new case();        
        dummyinn = new list<innercl> ();
      //  doc = new Document();
        doc = null;
        UploadedDocumentList = new List<AttachmentsWrapper>();
        showlist = false;
        showthankyumsg= false;  
        RenderFieldSetBlock = false;
        requesttype = 'None';
        reqtype = '';
        iscentraloginisuue = 'false';
        RenderFunction = true;
        init();
        getCategoryList();
        getfields();
       // displayPopup = true;
        contactemailid = '';
        addAttachment = false;
        setpopdisplay = ''; 
        
        }
   
     Public void init(){
        
       // Get record types for learning request forms and display in 'Function'.
      
        SObjectType ObjType;  
        map<String, Schema.RecordTypeInfo> TypeInfoMap;
        Schema.RecordTypeInfo TypeInfo;
        Id RecTypeId;
 
        ObjType =  Case_form_Extn__c.SObjectType;
        TypeInfoMap = ObjType.getDescribe().getRecordTypeInfosByName();
        oCaseExtnrecordtypes = new List<RecordType>();
        orecordtypes = new List<selectoption>();
        List<selectoption> orecordtypesTemp = new List<selectoption>();
        caseformextrecordtypeMap = new Map<string,Id>();
        System.debug('@@@@@@@===' +TypeInfoMap.keySet());
        
        for(string rec : TypeInfoMap.keySet()){
            
             if(TypeInfoMap.get(rec).getName() == System.Label.Advisory ||  TypeInfoMap.get(rec).getName() == System.Label.Audit || TypeInfoMap.get(rec).getName() == System.Label.Services || TypeInfoMap.get(rec).getName() == System.Label.Consulting || TypeInfoMap.get(rec).getName() == System.Label.TAX ||TypeInfoMap.get(rec).getName() == System.Label.None || TypeInfoMap.get(rec).getName() == System.Label.LD||TypeInfoMap.get(rec).getName() == System.Label.Industry || TypeInfoMap.get(rec).getName() == 'DLC Learning Queries' ){                
               caseformextrecordtypeMap.put((TypeInfoMap.get(rec).getName()),TypeInfoMap.get(rec).getRecordTypeId());
                if(TypeInfoMap.get(rec).getName() != System.Label.LD  &&  TypeInfoMap.get(rec).getName() != System.Label.Industry && TypeInfoMap.get(rec).getName() != 'DLC Learning Queries'){
                    if(TypeInfoMap.get(rec).getName() == System.Label.None){
                        ocaseformextn.RecordTypeId = TypeInfoMap.get(rec).getRecordTypeId();
                    }
                  if(TypeInfoMap.get(rec).getName().Contains('-None-')){  
                  orecordtypes.add(new Selectoption(TypeInfoMap.get(rec).getRecordTypeId(), TypeInfoMap.get(rec).getName()));
                  }else{
                  orecordtypesTemp.add(new Selectoption(TypeInfoMap.get(rec).getRecordTypeId(), TypeInfoMap.get(rec).getName()));
                  }   
                }
             } 
        }
        orecordtypesTemp.Sort();
        for(selectoption so:orecordtypesTemp){
       
        orecordtypes.add(new Selectoption(so.getValue(),so.getLabel()));
        
        }
        
    }
    
    Public void getfields(){
     
        if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('Param1'))){
           RenderFieldSetBlock=true; 
           RenderFunction = false;
           requesttype = apexpages.currentpage().getparameters().get('Param1');
           formname=requesttype;
           requesttype = EncodingUtil.urlDecode(requesttype, 'UTF-8');
           ocaseformextn.TDR_Request_Type__c = requesttype ;   
           DlccategorySelection = '';
           system.debug('414214124' + requesttype);
           if(requesttype != 'None'){
               requesttype=requesttype.replace(' ','_').replace('(' , '').replace(')' , '').replace(',' , '_').replace('-' , '_').replace('/' , '_').replace('.' , '');
           }
           
           if(requesttype.length() > 41){
               
             requesttype = requesttype.substring(0,40);
             system.debug('@@@@@@@@@@@@' + requesttype );
             if(requesttype.endsWith('_')){
              requesttype= requesttype.removeEnd('_');
                              
             }
         }
       
       }
       
      }  
        
        
      // To get record type values from List of Names record 
      
      private void getCategoryList(){
         listOfNames = new List<WCT_List_of_Names__c>();
         listofnamesMap = new Map<string,WCT_List_of_Names__c>();
         List<RecordType> ListOfNamesrecordtyp = new List<RecordType>();
         List<WCT_List_of_Names__c> lstListOfNamesTemp=  new List<WCT_List_of_Names__c>();
      try{
         lstListOfNamesTemp = [SELECT ID,Name,WCT_Type__c,ToD_Case_Category_Instructions__c,TDR_Attachment_Label__c FROM WCT_List_Of_Names__c WHERE RecordTypeId =:System.Label.List_Of_Names_TDR_Recordtype_Id];
        
         system.debug('-----list of names---------'+listOfNames);
         for(WCT_List_of_Names__c ln : lstListOfNamesTemp) {
            system.debug('list of namesetc'+ln);
            if(String.IsNotEmpty(ln.ToD_Case_Category_Instructions__c)) {//null != ln.ToD_Case_Category_Instructions__c
                system.debug('beforeln.ToD_Case_Category_Instructions__c'+ln.ToD_Case_Category_Instructions__c);
                ln.ToD_Case_Category_Instructions__c = ln.ToD_Case_Category_Instructions__c.replaceAll('\r', ' ');
                ln.ToD_Case_Category_Instructions__c = ln.ToD_Case_Category_Instructions__c.replaceAll('\n', ' ');
                system.debug('ln.ToD_Case_Category_Instructions__c'+ln.ToD_Case_Category_Instructions__c);
                listOfNames.add(ln);
                
            }
                listofnamesMap.put(ln.WCT_Type__c,ln);

          }
        }catch(Exception e){}
        }
    
    
    
    
    
   // saverequest method is to save the request.
   
    Public PageReference saveRequest(){
              
            
    try{
         pageError = false;
         pageErrorMessage = '';
        
         //validation check
         
          if(ocaseformextn.TDR_Delivery_Type__c == 'Live Instructor-Led (Classroom)'){
           
              if(string.valueof(ocaseformextn.For_live_session_add_location__c) == '' || string.valueof(ocaseformextn.For_live_session_add_location__c) == Null){
                    ocaseformextn.For_live_session_add_location__c.addError( 'Please Enter Location for Live Session');
                     pageError = true;
              }
         }
         
         if(DlccategorySelection  == 'Incorrect CPE Credits / Completion Date'){
            if(ocaseformextn.TDR_Delivery_Type_1__c == 'Elearning' && ocaseformextn.Completion_date_only_for_e_learning__c == Null){
                    ocaseformextn.Completion_date_only_for_e_learning__c.addError( 'Please Enter Completion Date for elearning');
                    pageError = true;
            }
        }
        
        if(ocaseformextn.TDR_Delivery_Type2__c == 'Live Instructor-Led (Classroom)' || ocaseformextn.TDR_Delivery_Type2__c == 'Virtual Instructor-Led (Centra/Sabameeting)' ){
            
              if(string.valueof(ocaseformextn.TDR_Offering_Start_Date__c) == '' || string.valueof(ocaseformextn.TDR_Offering_Start_Date__c) == Null){
                    ocaseformextn.TDR_Offering_Start_Date__c.addError( 'Please Enter Offering start Date');
                     pageError = true;
              }
               
              if(string.valueof(ocaseformextn.TDR_End_Date__c) == '' || string.valueof(ocaseformextn.TDR_End_Date__c) == Null  ){
                   ocaseformextn.TDR_End_Date__c.addError( 'Please Enter Offering End Date');
                    pageError = true;
              }
             
            
          }
         
         
         if(ocaseformextn.TDR_Offering_Number__c != NULL && ocaseformextn.TDR_Offering_Number__c != '' && ocaseformextn.TDR_Offering_Number__c.length() != 8){
                ocaseformextn.TDR_Offering_Number__c.addError( 'Please provide the unique eight digit number assigned to your learning event');
                 pageError = true;
          }
           if(ocaseformextn.TDR_Office_Phone_No__c != NULL && ocaseformextn.TDR_Office_Phone_No__c != '' && ocaseformextn.TDR_Office_Phone_No__c.length() != 10){
                ocaseformextn.TDR_Office_Phone_No__c.addError( 'Please Provide 10 Digit Phone Number');
                 pageError = true;
          }
          
          
          
         // If there is no validation error save the request.
              
          if(pageError == false){  
            
              RecordType rectype = new RecordType();
              system.debug('=====description====>' +  apexpages.currentpage().getparameters().get('description') );
            system.debug('=====ocaseformextn.TDR_Request_Type__c====>' +  ocaseformextn.TDR_Request_Type__c); 
            system.debug('=====listofnamesMap====>' +  listofnamesMap);
             string recrdtname; 
            if(string.IsNotEmpty(ocaseformextn.TDR_Request_Type__c) && string.IsEmpty(dlcAPI)){
             recrdtname = listofnamesMap.get(ocaseformextn.TDR_Request_Type__c).Name;
            }else{
            if(string.IsNotEmpty(dlcAPI)){
             recrdtname = listofnamesMap.get(dlccategorySelection).Name;
            // ocaseformextn.TDR_Request_Type__c='DLC Learning Queries';
            }
            
            }
             
            ocaseformextn.RecordTypeId =  caseformextrecordtypeMap.get(recrdtname); 
              
              
              if(ocaseformextn.TDR_Request_Type__c == 'DLC Learning Queries'){
                ocaseformextn.TDR_Sub_Category__c = DlccategorySelection ;
              }
       
         
       
       
       
            //Insert Case
           try{
              Ocase = new case();
              Ocase.Status    = 'New';
              Ocase.Subject =  System.Label.TDR_Case_Subject;
              Ocase.WCT_Category__c =  System.Label.TDR_Case_Category;
              Ocase.Origin = System.Label.TDR_Case_Origin;
              Ocase.Description = apexpages.currentpage().getparameters().get('description') ;  
              Ocase.Contactid   = loggedInContact.id;
              
              Ocase.Gen_Request_Type__c = ocaseformextn.TDR_Request_Type__c;
              Ocase.Gen_RecordType__c = listofnamesMap.get(ocaseformextn.TDR_Request_Type__c).Name;
              Ocase.TDR_Dlc_query_sub_category__c =  ocaseformextn.TDR_Sub_Category__c;
              Ocase.RecordTypeid   = System.Label.Case_RecordTypeId;
             
           // Assigning Case using Case-Assignment Rule.
             
             Database.DMLOptions dmlOpts = new Database.DMLOptions();
             dmlOpts.assignmentRuleHeader.assignmentRuleId= Label.Case_Assignment_Rule_Id ;
             dmlOpts.EmailHeader.TriggerUserEmail = true;
             Ocase.setOptions(dmlOpts); 
            
             Database.insert(Ocase, dmlOpts);
             
             system.debug('=========Case=======' + Ocase);
           
           // Insert Case Extension Form. 
             ocaseformextn.Case_Form_App__c = System.Label.Case_Form_App;
             ocaseformextn.TDR_Employee__c  = loggedInContact.id;
             ocaseformextn.GEN_Case__c= Ocase.Id;
             if(ocaseformextn.TDR_Request_Type__c == 'TAP POR (TAP into Power of Relationships) session registration'){
             ocaseformextn.TDR_ProgramName__c = 'Tap POR';
             }
             if(ocaseformextn.TDR_Request_Type__c == 'TAP YLP (TAP into Your Leadership Potential) session registration'){
             ocaseformextn.TDR_ProgramName__c = 'Tap YLP';
             }
             
             
             
             system.debug('=========Case Extension=======' + ocaseformextn);
            
             Insert ocaseformextn;
        
             uploadRelatedAttachment();
        
             return new PageReference('/apex/TDR_TalentDevelopmentThankYou?id=' + ocaseformextn.id+'&em='+CryptoHelper.encrypt(LoggedInContact.Email)); 
             }catch(Exception e){
             return null;
             }
          }
       
       // If there is validation error display the error message.
          else{
           return null;
          }
}
catch(Exception ex){
system.debug('------getMessage----'+ex.getMessage()+'--------getLineNumber--------'+ex.getLineNumber());
 return null;
}
        
    }
    
   // Function Change
   
    Public void requesttypedef() 
    {
          
          UploadedDocumentList = new List<AttachmentsWrapper>();
          dlccategorySelection = '';
          iscentraloginisuue = 'false';
          ocaseformextn.TDR_Request_Type__c = '' ;
          requesttype = 'None';
          RenderFieldSetBlock=false;
          reqtype='None';
          requesttype='None';
          dlcAPI = '';
          ocaseformextn.RecordTypeid= apexpages.currentpage().getparameters().get('recordtype') ;
          system.debug('-----Function----RecordTypeid---------'+ocaseformextn.RecordTypeid);         
          if(apexpages.currentpage().getparameters().get('recordtype')!='-None-'){
             setrequesttype();
             
          }
    }
        
    // Request Type Change   
    public void setrequesttype() // Request Type Change
    {
          UploadedDocumentList = new List<AttachmentsWrapper>();
          dlccategorySelection ='';
          iscentraloginisuue = 'false'; 
          RenderFieldSetBlock=true;
          dlcAPI = '';
          reqtype =apexpages.currentpage().getparameters().get('reqtype') ;
          system.debug('=======reqtype ===========' + reqtype );
          requesttype = apexpages.currentpage().getparameters().get('reqtype') ;
          recordType = apexpages.currentpage().getparameters().get('recordtype') ;
          system.debug('=======recordType ===========' + recordType );
          if(requesttype == '' ){
            requesttype = 'None';
          }
          RenderFieldSetBlock=false;  
          if(string.IsNotEmpty(requesttype)){
            RenderFieldSetBlock=true;    
            requesttype = EncodingUtil.urlDecode(requesttype, 'UTF-8');  
            requesttype=requesttype.replace(' ','_').replace('(' , '').replace(')' , '').replace(',' , '_').replace('-' , '_').replace('/' , '_').replace('...' , '_');
            system.debug('13123414123' + requesttype);
              if(requesttype.length() > 41){
       
                  requesttype = requesttype.substring(0,40);
                  if(requesttype.endsWith('_')){
                     requesttype= requesttype.removeEnd('_');
                              
                  }

              }     
         }
           
         ocaseformextn = new Case_form_Extn__c ();
         if(string.isNotEmpty(recordType)){
            ocaseformextn.RecordTypeId = recordType;
            getCategoryList();  
         }
         else{
         }
         ocaseformextn.TDR_Request_Type__c= reqtype ;
         system.debug('-----Req Change----RecordTypeid---------'+ocaseformextn.RecordTypeid);
          system.debug('-----Req Change----Request Type---------'+ocaseformextn.TDR_Request_Type__c); 
                                        system.debug('2567800' + requesttype);
   
    }
        
    // call Myrequests method
    
    Public pagereference myrequestpageref(){
         return new PageReference('/apex/TDR_Mylearningrequests?em='+CryptoHelper.encrypt(LoggedInContact.Email)); 
    }
    
    /* This Method is to Display list of myrequests */
    
    Public Void myRequests(){
        
        dummyinn = new list<innercl> ();
        licaseformextn = new List<Case_form_Extn__c>();
        licaseformextn = [SELECT ID,NAME,GEN_Case__c,GEN_Case__r.CaseNumber,GEN_Case__r.WCT_ResolutionNotes__c,GEN_Case__r.TDR_Additional_Comments__c,TDR_Employee__R.Email,TDR_Request_Type__c,RecordType.Name,LastModifiedDate,GEN_Case__R.ownerId,GEN_Case__R.status From Case_form_Extn__c WHERE TDR_Employee__R.Email =: loggedInContact.Email AND Case_Form_App__c =: system.Label.Case_Form_App];
        caseformmap = new Map<string,Case_form_Extn__c>();
        for(Case_form_Extn__c li : licaseformextn ){
             caseformmap.Put(li.Id,li);
             dummyinn.add(new innercl(li,li.GEN_Case__c,''));
        }
           showlist = true;
           showthankyumsg= false;
           system.debug('=======dummyinn=======' + dummyinn);
     }   
         
   // This method is to case on providing additional comments in myrequest page.
       
   Public void Updaterecord(){
try{
           system.debug('@@@@@@@@' + apexpages.currentpage().getparameters().get('Recid'));    
           string Recid = apexpages.currentpage().getparameters().get('Recid');
           string Issite  = apexpages.currentpage().getparameters().get('sitename');
           system.debug('****Issite*****'+ Issite  );  
           system.debug('****Recid*****'+ Recid  ); 
           system.debug('****dummyinn*****'+ dummyinn  ); 
           if(Issite != ''){
             for(innercl cCon: dummyinn) {
             system.debug('****cCon.tdrrequest.Id*****'+ cCon.tdrrequest.Id  );
            
                if(cCon.tdrrequest.Id == Recid ){
                 system.debug('****Matched*****'); 
                  if(cCon.comments != '') {
                   string StrComments=cCon.comments;
                        Contact con = new Contact();
                        con.Id = cCon.tdrrequest.TDR_Employee__r.Id;
                        Ocase = new case();
                        Ocase.Id = cCon.caseId;
                        Ocase.TDR_Additional_Comments__C = StrComments;
                         Update Ocase;
                        
                        showlist = false;
                        showthankyumsg= true;
                  }
                   else{
                      Apexpages.addmessage(new apexpages.message(apexpages.severity.error,'Please Provide comments'));
                         
                    }
                }
            }
         }
           }
    catch(exception ex){
            system.debug('-------exception---------'+ex.getMessage()+'----at---Line #----'+ex.getLineNumber());
        
        }    
    
    } 
      

    Public void uploadRelatedAttachment(){

         if(!docIdList.isEmpty()) { //If there is any Documents Attached in Web Form
            system.debug('#######' +UploadedDocumentList);             
            List<String> selectedDocumentId = new List<String>();
            for(AttachmentsWrapper aWrapper : UploadedDocumentList){
                if(aWrapper.isSelected){
                    selectedDocumentId.add(aWrapper.documentId);
                }
            }
            system.debug('24142124' + selectedDocumentId);
            /* Select Documents which are Only Active (Selected) */
            selectedDocumentList = new List<Document>();
            if(!selectedDocumentId.isEmpty()){
                selectedDocumentList = [
                                           SELECT 
                                               id,
                                               name,
                                               ContentType,
                                               Type,
                                               Body 
                                           FROM 
                                               Document 
                                           WHERE 
                                               id IN :selectedDocumentId
                                        ];
            }
             system.debug('23253535' + selectedDocumentList);
            /* Convert Documents to Attachment */
            List<Attachment> attachmentsToInsertList = new List<Attachment>();
            for(Document doc : selectedDocumentList){
                Attachment a = new Attachment();
                a.body = doc.body;
                a.ContentType = doc.ContentType;
                a.Name= doc.Name;
                a.parentid = Ocase.id; //Case Record Id which is been recently created.
                attachmentsToInsertList.add(a);                
            }
            
            if(!attachmentsToInsertList.isEmpty()){
                insert attachmentsToInsertList;
            }
            
            List<Document> listDocuments = new List<Document>();
            listDocuments = [
                                SELECT
                                    Id
                                FROM
                                    Document
                                WHERE
                                    Id IN :docIdList
                            ];
            if( (null != listDocuments) && (0 < listDocuments.size()) ) {
                delete listDocuments;
            }
        }
    }

    /*Invoked when Upload Button in VF page is clicked and the IDs are stored in the docIdList*/
    /*All the Files Uploaded are stored in the Documents. Documents does not require parent Id.This method is used to circumvent to upload
      the documents first and then add as Attachment to the Cases (Related List)*/

    public void uploadAttachment(){
        
        pageError = false;
        pageErrorMessage = '';
         
               if(ApexPages.hasMessages()) {
            pageErrorMessage = '';
            for(ApexPages.Message message : ApexPages.getMessages()) {
                pageErrorMessage += message.getSummary() + '\n';
            }            
                   system.debug('343243' + pageErrorMessage);
            doc = new Document();
            pageError = true;
            return;
        }                                
        System.debug('@@@@..1..' + doc.name);
        doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
        if(doc.body != null) {
         Database.insert(doc,false);

            docIdList.add(doc.id);
            doc = [SELECT id, name, bodylength FROM Document WHERE id = :doc.id];

            String size = '';
            if(1048576 < doc.BodyLength) {
              // Size greater than 1MB
                size = '' + (doc.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < doc.BodyLength) {
              // Size greater than 1KB
                size = '' + (doc.BodyLength / 1024) + ' KB';            
            }
            else {
                size = '' + doc.BodyLength + ' bytes';
            } 
            UploadedDocumentList.add(new AttachmentsWrapper(true, doc.name, doc.id,size));
            system.debug('@@@7777' +UploadedDocumentList );
            doc = new Document();
        }
               addAttachment=false;
    }       
    
    //Documents Wrapper
    public class AttachmentsWrapper {
        public Boolean isSelected {get;set;}
        public String docName {get;set;}
        public String documentId {get;set;}
        public String size {get; set;}
        
        public AttachmentsWrapper(Boolean selected, String Name, String Id, String size){
            isSelected = selected;
            docName = Name ;
            documentId = Id;
            this.size = size;
        }        
    } 
   
    // My request Wrapper
    Public class innercl{
         Public Case_form_Extn__c tdrrequest{get; set ;}
         Public Id caseId{get; set;}
         Public string comments{get; set;}
         Public innercl(Case_form_Extn__c tdr, Id cId , string cts){
            tdrrequest = tdr ;
            caseId = cId ;
            comments = cts;
    
         }
    }
   
   // This method is to set subcategory for Dlc Learning Query and to check for central login issue.
   
       Public void setDLCcategory(){
         
        try{
         ocaseformextn = new Case_form_Extn__c ();
         ocaseformextn.TDR_Request_Type__c = 'DLC Learning Queries';
         dlccategorySelection = apexpages.currentpage().getparameters().get('setDLCcategory');
         iscentraloginisuue = apexpages.currentpage().getparameters().get('iscentraloginissue');
         dlcAPI = apexpages.currentpage().getparameters().get('dlcAPI');      
       }catch(Exception ex){
            system.debug('------getMessage----'+ex.getMessage()+'--------getLineNumber--------'+ex.getLineNumber());

      }
    }
    
    public void shwattachmentpopup(){

        setpopdisplay= '';
        setpopdisplay = apexpages.currentpage().getparameters().get('setpopdisplay');
        if(setpopdisplay == 'showpanel'){
            addAttachment=true;
            if (doc == null){
            doc = new document();
            }
        }
        else{
           addAttachment=false;
        }        
    }
}