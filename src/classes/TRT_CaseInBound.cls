global class TRT_CaseInBound implements Messaging.InboundEmailHandler 
{
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email,Messaging.InboundEnvelope env)
    {
    // Create an InboundEmailResult object for returning the result of the  
    // Apex Email Service 
    Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
    String myPlainText = email.plainTextBody;
    string subject = email.subject;
    String emailAddress = email.fromAddress;
    system.debug(emailAddress);
    string fromemail= email.fromName;
    system.debug(fromemail);
    list<contact> contact= new list<contact>();// holds the contact record
    
    // Retrieving the record type of Case
    Schema.DescribeSObjectResult Cas = Case.SObjectType.getDescribe();// getting Sobject Type
    Map<String,Schema.RecordTypeInfo> rtMapByNames = Cas.getRecordTypeInfosByName();// getting the record Type Info
    Id caseRtId =rtMapByNames.get('Case Mail Consolidation').getRecordTypeId();//particular RecordId by  Name
    
    if(emailAddress != null || emailAddress != '')
    {
        contact = [SELECT Name,email,WCT_Function__c,WCT_Job_Level_Text__c,id,
                   Account.Name FROM contact WHERE email=:emailAddress limit 1];// querying the conatct details on user entry
    }
    system.debug('contact'+contact);
    // Creating a Case
    case objCase = new case();
    system.debug(subject);
    system.debug(subject.contains('Automatic reply'));
    boolean createBoolean =true;
    if(subject != null || subject !='' )
    {
       if(subject.containsIgnoreCase('Automatic reply')||subject.containsIgnoreCase('Auto reply')|| emailAddress.containsIgnoreCase('usctstalentreportingandanalytics@deloitte.com'))
       {
           createBoolean =false;
           //break;
       }else if(subject.containsIgnoreCase('Undeliverable')||subject.containsIgnoreCase('Undelivered') ){
           createBoolean =false;
           //break;
       }
    }
    if(createBoolean){
    //if(subject.contains('Automatic reply')==false || subject.contains('Undeliverable') == false ){
        if(contact != null && !contact.isEmpty())
        {
            if(contact[0]!=null && contact[0].Email != null)
            {
                objCase.ContactId=contact[0].Id;
            }
         }
         // inserting case
         objCase.Status='New';
         objCase.RecordTypeId=caseRtId;
         objCase.WCT_Category__c='TRT Reporting';  
         objCase.Origin = 'Web';
         objCase.Gen_Request_Type__c ='';
         objCase.Priority='3 - Medium';
         objCase.Description =subject;  
         //objCase.Gen_RecordType__c =questionRecordType;
         objCase.TRT_Questions__c=true;
         objCase.Subject='A new TRT-Reporting request';
         objCase.TRT_Requestor_Suggestions__c=myPlainText;
         objCase.WCT_isEmailonCase__c=true;
         // Inserting a new case
         list<case> listCase = new list<case>();
         try{
            Database.DMLOptions dmlOpts = new Database.DMLOptions();
            dmlOpts.assignmentRuleHeader.assignmentRuleId= Label.Case_Assignment_Rule_Id ;
            dmlOpts.EmailHeader.TriggerUserEmail = true;
            objCase.setOptions(dmlOpts); 
            listCase.add(objCase);
            Database.SaveResult[] csList = Database.insert(listCase, dmlOpts);
            //insert objCase;
            system.debug('objCase'+objCase);
         }catch(exception e){
            system.debug('DML EXception'+e);
         }
     }
      return result;
  }
}