/*****
ClassName:TRT_Reporting_ctrl
Description:Controller which handles the operation for creating a request on Case_form_Extn__c object for Reporting team.
CreatedDate:23rd March 2015
Author:Deepthi Toranala(pre pod)
*****/

public class TRT_Reporting_ctrl extends SitesTodHeaderController{
          
    public Case_form_Extn__c trtReport{get;set;} //  maintain the state of the wizard and the user input is stored 
    public string fileName {get;set;}// holds the attachment name
    public string efileName {get;set;}// holds the email attachment name
    public transient Blob fileBody {get;set;}// holds the attachment body
    public list<contact> contact= new list<contact>();// holds the contact record
    public transient Blob efileBody {get;set;}// holds the email attachment body
    public boolean pageMessage{get;set;}//Variable which handles the page Messages on the page
    public String sapReport {get;set;} // holds the value of sap reporting 
    public String learningReport {get;set;}// holds the value of learning reporting sub values
    public String rManageReport {get;set;}// holds the value of Resource Management reporting sub values
    public String pManageReport {get;set;}// holds the value of Performace management reporting sub Values
    public String mainRManagementReport {get;set;}// holds the value of Resource Management reporting 
    public String mainPManagementReport {get;set;}// holds the value of Performace management reporting 
    public String mainlearningReport {get;set;}// holds the value of learning reporting 
    public String mainTalentRequest{get;set;}// holds the value of talent Accquistion reporting 
    public string mainSurvey{get;set;}// holds the value of Survey 
    public string mainSurveyReport{get;set;}// holds the value of Survey reporting 
    public string mainGMI{get;set;}// holds the value of GMI reporting 
    public string mainMultidataSources{get;set;}
    public map<string,string> mapQueueNames;
    public list<string> checkboxSelections {get;set;}
    public list<string> leaCheckboxSelections {get;set;}
    public list<string> evaCheckboxSelections {get;set;}
    public boolean Enrollments{get;set;}
    public boolean Completions{get;set;}
    public boolean Compliance{get;set;}
    public boolean CancellationsandNoshow{get;set;}
    public boolean Instructor{get;set;}
    public list<case> caseQuestionsList{get;set;}// holds the case question/suggestions list
    public list<string> leacompCheckboxSelections{get; set;}
    public Document doc {get;set;}
    public List<String> docIdList = new List<string>();
    public List<AttachmentsWrapper> UploadedDocumentList {get; set; }
    public static List<AttachmentsWrapper> UploadedDocumentListLocalHost =new List<AttachmentsWrapper>();
    public list<string>  surveyListCheckBox{get;set;}// holds the mutlicheck box list 
    public boolean addAttachment{get;set;}
    public static datetime datedelivery {get;set;}
    public list<string> surveyReportType {get;set;}
    
    
    // constructor
    public TRT_Reporting_ctrl()
    {
        init();
    }
        
    Public Void Init()
    {
        datedelivery = BusinessHours.add(system.label.TRT_BussinessHours, Datetime.now()  , 172800000);
        UploadedDocumentList = new List<AttachmentsWrapper>();
        checkboxSelections = new list<string>();
        leaCheckboxSelections = new list<string>();
        evaCheckboxSelections = new list<string>();
        surveyListCheckBox = new list<string>();
        surveyReportType =  new list<string>();
        leacompCheckboxSelections = new list<string>();//for learning competancy multiselect***added by nihal***
        caseQuestionsList = new list<case>();
        pageMessage=false;
                
        if(trtReport == null)
        {
           trtReport = new Case_form_Extn__c();
        }    
        system.debug(loggedInContact);
        if(loggedInContact != null){
            contact = [SELECT Name,email,WCT_Function__c,WCT_Job_Level_Text__c,id,
                       Account.Name FROM contact WHERE id=:loggedInContact.id limit 1];// querying the conatct details on user entry
        }
        system.debug(contact);
        if(!contact.isEmpty() && contact[0] != null )
        {
            trtReport.TRT_Requestor_Name__c = contact[0].id;// auto populating the contact name
            trtReport.TRT_Requestor_Function__c=contact[0].WCT_Function__c;
            trtReport.TRT_Requestor_Level__c=contact[0].WCT_Job_Level_Text__c;
            trtReport.TRT_Requester_Group__c=contact[0].Account.Name;
            
        }
        mapQueueNames =new map<string,string>();
        for(group g:[Select g.Id, g.Name, g.Email,g.Type from Group g where g.Type = 'Queue' and g.name like '%TRT %'])
        {
            mapQueueNames.put(g.Name,g.Id);
        }
        if(datedelivery != null)
        {
            //string sFormattedDate =datedelivery.format('MM/dd/yyyy');
            trtReport.TRT_Delivery_Date_lrng__c=date.valueOf(datedelivery);
        }
    }
    
    //method to validate the end date should be less than today **Added by nihal**
    public PageReference Enddatecheck(){
        
        if(trtReport.TRT_End_Date_RM__c<trtReport.TRT_Start_Date_RM__c )
        {
            trtReport.TRT_End_Date_RM__c.addError('End date cannot be earlier than start date');
        }
       
        if(trtReport.TRT_Delivery_Date_lrng__c  < date.today() )
        {
            
            trtReport.TRT_Delivery_Date_lrng__c.addError('Delivery date cannot be earlier than today' );
        }
        return null;
        
    }
    
    //for learning competancy multiselect***added by nihal***
    public PageReference compPannel(){
        
        String[] values = leacompCheckboxSelections;
        system.debug(leacompCheckboxSelections);
        String s1 = '';
        string s2 = '';
        string s3 = '';
        string s4 = '';
        string s5 = '';
        for(String s : values)
        {
            system.debug(s);
            if(s=='Consulting Specific'){
                s1='Consulting Specific';
            }else if(s=='FAS Specific'){
                s2='FAS Specific';
            }else if(s=='TAX Specific'){
                s3='TAX Specific';
            }else if(s=='AERS Specific'){
                s4='AERS Specific';
            }else if(s=='Enabling Area Specific'){
                s5='Enabling Area Specific';
            }
        }
        if(s1=='Consulting Specific'){
            Enrollments=true;
        }else{
            Enrollments=false;
        }
        if(s2=='FAS Specific'){
            Completions=true;
        }else{
            Completions=false;
        }if(s3=='TAX Specific'){
            Compliance=true;
        }else{
            Compliance=false;
        }if(s4=='AERS Specific'){
            CancellationsandNoshow=true;
        }else{
            CancellationsandNoshow=false;
        }if(s5=='Enabling Area Specific'){
            Instructor=true;
        }else {
            Instructor=false;
        }
        return null;
    }
    // select radio values for Learning
    public List<SelectOption> getLearning() 
    {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Catalog','Catalog')); 
        options.add(new SelectOption('Certification','Certification'));
        options.add(new SelectOption('Offering','Offering'));
        options.add(new SelectOption('Chargeback','Chargeback')); 
        options.add(new SelectOption('Common','Enrollments/Completions/Compliance/Cancellations and No show/Instructor')); 
        options.add(new SelectOption('Competency','Competency')); 
        options.add(new SelectOption('Learning Evaluation','Learning Evaluation'));
        return options; 
    }
    // Select radio values for Resource Management
    public List<SelectOption> getRManagement() 
    {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('GSS/StaffTrak','GSS/StaffTrak')); 
        return options; 
    }
    // Select radion values for Project Management
    public List<SelectOption> getPManagement() 
    {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Reinvention','Reinvention'));
        options.add(new SelectOption('DPME (Old)','DPME')); 
        return options; 
    }
    public list<selectOption> getMyCheckboxes()
    {
        list<selectOption> Options = new list<selectOption>();
        options.add(new SelectOption('Reinvention','Reinvention'));
        options.add(new SelectOption('DPME (Old)','DPME (Old)')); 
        return Options;
    }

    // method that hold trrReport record
    public Case_form_Extn__c getTrtReport() 
    {
        if(trtReport == null)
        {
            trtReport = new Case_form_Extn__c();
        }
        return trtReport;
    }
 
    // Method that redriects to SapRequestForm
    public PageReference sapReport() 
    {
        if(sapReport == 'Sap Reporting')
        {
            return Page.TRT_Sap_Reporting1;
        }else if(mainRManagementReport == 'Resource Management' && rManageReport =='GSS/StaffTrak'){
            return Page.TRT_RManagement_GSS1;
        }else if(mainSurvey == 'Survey')
        {
            return Page.TRT_Survey;
        }else if(mainPManagementReport == 'Performance Management' && pManageReport=='DPME (Old)'){
            return Page.TRT_PManagement_DPME1;
        }else if(mainGMI == 'GMI'){
            return Page.TRT_GMI;
        }else if(mainTalentRequest == 'Talent Accquisition'){
            return Page.TRT_Tacquisition;
        }else if(mainlearningReport == 'Learning Reporting' && learningReport=='Catalog' ){
            return Page.TRT_Learning_Ctlg;
        }else if(mainlearningReport == 'Learning Reporting' && learningReport=='Certification'){
            return Page.TRT_Learning_Cert; 
        }else if(mainlearningReport == 'Learning Reporting' && learningReport=='Offering'){
            return Page.TRT_Learning_Off;
        }else if(mainlearningReport == 'Learning Reporting' && learningReport=='Chargeback'){
            return Page.TRT_Learning_Chrgbck;
        }else if(mainlearningReport == 'Learning Reporting' && learningReport=='Common'){
            return Page.TRT_Learning_Comm;
        }else if(mainlearningReport == 'Learning Reporting' && learningReport=='Competency'){
            return Page.TRT_Learning_Comp;
        }else if(mainMultidataSources == 'Multi Data Source'){
            return Page.TRT_OtherRequest;
        }else if(mainPManagementReport == 'Performance Management' && pManageReport =='Reinvention'){
            return Page.TRT_PManagement_Reinvention;
        }else if(mainlearningReport == 'Learning Reporting' && learningReport=='Learning Evaluation'){
            return Page.TRT_LearningEvaluation;
        }else if(mainSurveyReport =='Survey Report'){
            return Page.TRT_Survey_Reporting;
        }
        return null;   
   }
   
   public PageReference questions()
   {
       return Page.TRT_Suggestion;
   }
   
   // Method the saves the values in salesforce for Case_form_Extn__c for sap reporting
   public PageReference submit()
   {
        contact con;
        boolean questionsCase=false;
        string questionRecordType='';
        list<case> listCase = new list<case>();
        Case objCase= new Case();
        
        // Retrieving the record type of Case_form_Extn__c
        Schema.DescribeSObjectResult Rep = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName = Rep.getRecordTypeInfosByName();// getting the record Type Info
        Id rtId =rtMapByName.get('TRT SAP Reporting').getRecordTypeId();//particular RecordId by  Name
        Id rtIdGMI =rtMapByName.get('TRT GMNI').getRecordTypeId();//particular RecordId by  Name
        Id rtIdRM =rtMapByName.get('TRT RM Reporting').getRecordTypeId();//particular RecordId by  Name
        Id rtIdPM =rtMapByName.get('TRT PM Reporting').getRecordTypeId();//particular RecordId by  Name
        Id rtIdTA =rtMapByName.get('TRT TA Reporting').getRecordTypeId();//particular RecordId by  Name
        Id rtIdSur =rtMapByName.get('TRT Survey').getRecordTypeId();//particular RecordId by  Name
        Id rtIdOth =rtMapByName.get('TRT Other Reporting').getRecordTypeId();//particular RecordId by  Name
        Id rtIdLea =rtMapByName.get('TRT Learning Reporting').getRecordTypeId();//particular RecordId by  Name
        Id rtIdLeaEva =rtMapByName.get('TRT Learning Evaluation').getRecordTypeId();//particular RecordId by  Name
        Id rtIdLeaCat =rtMapByName.get('TRT Learning Catalog').getRecordTypeId();//particular RecordId by  Name
        Id rtIdLeaOff =rtMapByName.get('TRT Learning Offering').getRecordTypeId();//particular RecordId by  Name
        Id rtIdLeaCert =rtMapByName.get('TRT Learning Certification').getRecordTypeId();//particular RecordId by  Name
        Id rtIdLeaChar =rtMapByName.get('TRT Learning Chargeback').getRecordTypeId();//particular RecordId by  Name
        Id rtIdLeaComp =rtMapByName.get('TRT Learning Competency').getRecordTypeId();//particular RecordId by  Name
        Id rtIdLeaComm =rtMapByName.get('TRT Learning Common').getRecordTypeId();//particular RecordId by  Name
        Id rtIdSurp =rtMapByName.get('TRT Survey Reporting').getRecordTypeId();//particular RecordId by  Name
        
        
        // Retrieving the record type of Case
        Schema.DescribeSObjectResult Cas = Case.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByNames = Cas.getRecordTypeInfosByName();// getting the record Type Info
        Id caseRtId =rtMapByNames.get('Case Mail Consolidation').getRecordTypeId();//particular RecordId by  Name
        system.debug(trtReport.TRT_Include_PII__c);
        system.debug(eFileBody);
        system.debug('*********'+mainPManagementReport);
        boolean learningPII=true;
        if(mainlearningReport == 'Learning Reporting'){
            
            if(learningReport =='Learning Evaluation'){
                learningPII=false;
            }else{
                learningPII=true;
            }
        }
        system.debug(mainlearningReport+'mainlearningReport');
        system.debug(learningPII +'learningPII');
        //if(trtReport.TRT_Include_PII__c && eFileBody == null && (mainPManagementReport != 'Performance Management' && (mainlearningReport != 'Learning Reporting' && learningPII)))
        //{
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please attach the PII approval email'));//if attachment is empty on Include_PII__c true
                
        //}else
        //{
          
            if(trtReport.TRT_Group_sugg__c=='SAP Report'){
                sapReport ='Sap Reporting';
                questionsCase=true;
                questionRecordType='TRT SAP Reporting';
            }else if(trtReport.TRT_Group_sugg__c=='Learning Report'){
                mainlearningReport ='Learning Reporting';
                questionsCase=true;
                questionRecordType='TRT Learning Reporting';
            }else if(trtReport.TRT_Group_sugg__c=='Global Mobility and Immigration Report'){
                mainGMI = 'GMI';
                questionsCase=true;
                questionRecordType='TRT GMNI';
            }else if(trtReport.TRT_Group_sugg__c=='Performance Management Report'){
                mainPManagementReport = 'Performance Management';
                questionsCase=true;
                questionRecordType='TRT PM Reporting';
            }else if(trtReport.TRT_Group_sugg__c=='Resource Management Report'){
                mainRManagementReport = 'Resource Management';
                questionsCase=true;
                questionRecordType='TRT RM Reporting';
            }else if(trtReport.TRT_Group_sugg__c=='Survey'){
                mainSurvey = 'Survey';
                questionsCase=true;
                questionRecordType='TRT Survey';
            }else if(trtReport.TRT_Group_sugg__c=='Talent Acquisition Report'){
                mainTalentRequest = 'Talent Accquisition';
                questionsCase=true;
                questionRecordType='TRT TA Reporting';
            }else if(trtReport.TRT_Group_sugg__c=='Multiple Sources Report'){
                mainMultidataSources = 'Multi Data Source';
                questionsCase=true;
                questionRecordType='TRT Other Reporting';
            }else if(trtReport.TRT_Group_sugg__c=='Learning  Evaluation'){
                mainlearningReport ='Learning Reporting';
                learningReport='Learning Evaluation';
                questionsCase=true;
                questionRecordType='TRT Learning Evaluation';
            }
           
            if(contact != null && !contact.isEmpty())
            {
                if(contact[0]!=null && contact[0].Email != null)
                {
                    objCase.ContactId=contact[0].Id;
                }
            }
            // inserting case
            objCase.Status='New';
            objCase.RecordTypeId=caseRtId;
            objCase.WCT_Category__c='TRT Reporting';  
            objCase.Origin = 'Web';
            objCase.Gen_Request_Type__c ='';
            objCase.Priority='3 - Medium';
            if(questionsCase && questionRecordType!='')
            {
                objCase.Description ='TRT question and suggestions request';  
                objCase.Gen_RecordType__c =questionRecordType; 
                objCase.TRT_Questions__c=true;
                objCase.Subject='A new TRT-Reporting request';
                objCase.TRT_Requestor_Suggestions__c=trtReport.TRT_ques_sugg__c;
            }
            if(sapReport == 'Sap Reporting' && questionsCase==false)
            {
                objCase.Description =trtReport.TRT_Business_need_for_the_data_request__c;  
                objCase.Gen_RecordType__c ='TRT SAP Reporting'; 
                objCase.Subject='A new TRT-Reporting request';
            }else if(mainGMI == 'GMI' && questionsCase==false)
            {
                objCase.Description =trtReport.TRT_Business_need_for_the_data_request__c;  
                objCase.Gen_RecordType__c = 'TRT GMNI';
                objCase.Subject='A new TRT-Reporting request';
            }else if(mainRManagementReport == 'Resource Management'&& questionsCase==false)
            {
                objCase.Description =trtReport.TRT_Business_need_for_the_data_request__c;  
                objCase.Gen_RecordType__c = 'TRT RM Reporting'; 
                objCase.Subject='A new TRT-Reporting request';
            }else if(mainPManagementReport == 'Performance Management' && questionsCase==false)
            {
                objCase.Description = trtReport.TRT_Business_need_for_the_data_request__c;  
                objCase.Gen_RecordType__c = 'TRT PM Reporting'; 
                objCase.Subject='A new TRT-Reporting request';
            }else if(mainSurvey == 'Survey' && questionsCase==false)
            {
                objCase.Description = trtReport.TRT_Business_need_for_the_data_request__c;  
                objCase.Gen_RecordType__c = 'TRT Survey'; 
                objCase.Subject='A new TRT-Reporting request';
            }else if(mainTalentRequest == 'Talent Accquisition' && questionsCase==false)
            {
                objCase.Description = trtReport.TRT_Business_need_for_the_data_request__c;  
                objCase.Gen_RecordType__c = 'TRT TA Reporting';
                objCase.Subject='A new TRT-Reporting request';
            }else if(mainMultidataSources == 'Multi Data Source' && questionsCase==false)
            {
                objCase.Description = trtReport.TRT_Business_need_for_the_data_request__c;  
                objCase.Gen_RecordType__c = 'TRT Other Reporting'; 
                objCase.Subject='A new TRT-Reporting request';
            }else if(mainlearningReport == 'Learning Reporting' && learningReport!='Learning Evaluation' && questionsCase==false)
            {
                objCase.Description = trtReport.TRT_Business_need_for_the_data_request__c;  
                objCase.Gen_RecordType__c = 'TRT Learning Reporting'; 
                objCase.Subject='A new TRT-Reporting request';
            }else if(mainlearningReport == 'Learning Reporting' && learningReport=='Learning Evaluation' &&  questionsCase==false)
            {
                objCase.Description = trtReport.TRT_Business_need_for_the_data_request__c;  
                objCase.Gen_RecordType__c = 'TRT Learning Evaluation'; 
                objCase.Subject='A new TRT-Reporting request';
            }else if(mainSurveyReport =='Survey Report' && questionsCase==false)
            {
                objCase.Description = trtReport.TRT_Business_need_for_the_data_request__c;  
                objCase.Gen_RecordType__c = 'TRT Survey Reporting'; 
                objCase.Subject='A new TRT-Reporting request';
            }
            Database.DMLOptions dmlOpts = new Database.DMLOptions();
            dmlOpts.assignmentRuleHeader.assignmentRuleId= Label.Case_Assignment_Rule_Id ;
            dmlOpts.EmailHeader.TriggerUserEmail = true;
            objCase.setOptions(dmlOpts); 
            listCase.add(objCase);
            Database.SaveResult[] csList = Database.insert(listCase, dmlOpts);
            system.debug(csList+'************');
            // Added by deepthi for adding attachment in question and sugestion page start
              if(csList[0].isSuccess())
              {
              	if(questionsCase==true)
                {
	            	if(fileBody != null && fileName != null)  
	                {  
	                    Attachment myAttachment  = new Attachment();  
	                    myAttachment.Body = fileBody;  
	                    myAttachment.Name = fileName;  
	                    myAttachment.ParentId = csList[0].id;
	                    insert myAttachment;  
	                }
                }
            }
            // Added by deepthi for adding attachment in question and sugestion page end
            // creating case form extension record
           
            if(csList[0].isSuccess())
            {
                trtReport.GEN_Case__c   =csList[0].id;
            }
         
            if(sapReport == 'Sap Reporting')
            {
                trtReport.RecordTypeId=rtId;
            }else if(mainGMI == 'GMI'){
                trtReport.RecordTypeId=rtIdGMI;
            }else if(mainRManagementReport == 'Resource Management'){
                trtReport.RecordTypeId=rtIdRM;
            }else if(mainPManagementReport == 'Performance Management'){
                trtReport.RecordTypeId=rtIdPM;
            }else if(mainSurvey == 'Survey')
            {
                trtReport.RecordTypeId=rtIdSur;
            }else if(mainTalentRequest == 'Talent Accquisition'){
                trtReport.RecordTypeId=rtIdTA;
            }else if(mainMultidataSources == 'Multi Data Source'){
                trtReport.RecordTypeId=rtIdOth;
            }/*else if(mainlearningReport == 'Learning Reporting' && learningReport!= 'Learning Evaluation'){
                trtReport.RecordTypeId=rtIdLea;
            }*/else if(mainlearningReport == 'Learning Reporting' && learningReport=='Catalog' ){
                trtReport.RecordTypeId=rtIdLeaCat;
            }else if(mainlearningReport == 'Learning Reporting' && learningReport=='Certification'){
                trtReport.RecordTypeId=rtIdLeaCert;
            }else if(mainlearningReport == 'Learning Reporting' && learningReport=='Offering'){
                trtReport.RecordTypeId=rtIdLeaOff;
            }else if(mainlearningReport == 'Learning Reporting' && learningReport=='Chargeback'){
                trtReport.RecordTypeId=rtIdLeaChar;
            }else if(mainlearningReport == 'Learning Reporting' && learningReport=='Common'){
                trtReport.RecordTypeId=rtIdLeaComm;
            }else if(mainlearningReport == 'Learning Reporting' && learningReport=='Competency'){
                trtReport.RecordTypeId=rtIdLeaComp;
            }
            else if(mainlearningReport == 'Learning Reporting' && learningReport == 'Learning Evaluation'){
                trtReport.RecordTypeId=rtIdLeaEva;
            }
            else if(mainSurveyReport=='Survey Report'){
                trtReport.RecordTypeId=rtIdSurp;
            }
            trtReport.Case_Form_App__c ='TRT Reporting';
            trtReport.TRT_Request_Status__c='New';
            //trtReport.TRT_Delivery_Date__c=trtReport.TRT_Delivery_Date_lrng__c;
            if(!checkboxSelections.isEmpty() && checkboxSelections != null)
            {
                trtReport.TRT_Reporting_Request_Forms_othr__c=getMultiSelect(checkboxSelections);
            }
            if(!leaCheckboxSelections.isEmpty() && leaCheckboxSelections != null)
            {
                trtReport.TRT_Common_Rtype_Lrng__c=getMultiSelect(leaCheckboxSelections );
            }
            //for learning competancy multiselect***added by nihal***
            if(!leacompCheckboxSelections.isEmpty() && leacompCheckboxSelections != null)
            {
                trtReport.TRT_Job_Role_Lrn_Comp__c=getMultiSelect(leacompCheckboxSelections);
            }
            
            if(!evaCheckboxSelections.isEmpty() && evaCheckboxSelections != null)
            {
                trtReport.TRT_survey_type_LE__c=getMultiSelect(evaCheckboxSelections);
            }
            if(!surveyListCheckBox.isEmpty() && surveyListCheckBox != null){
                trtReport.TRT_Level_of_Analysis_srvy__c=getMultiSelect(surveyListCheckBox);
            }
            if(!surveyReportType.isEmpty() && surveyReportType != null)
            {
                system.debug('values'+surveyReportType);
                trtReport.TRT_Report_Type_srvy__c=getMultiSelect(surveyReportType);
            }
           
            try
            {
                if(csList[0].isSuccess())
                {
                     if(questionsCase==false)
                     {
                        insert trtReport;
                     }
                }
            }catch(Exception e)
            {
                system.debug('DML Exception '+e);
            }
            if(trtReport.id != null)
            {
                if(fileBody != null && fileName != null)  
                {  
                    Attachment myAttachment  = new Attachment();  
                    myAttachment.Body = fileBody;  
                    myAttachment.Name = fileName;  
                    myAttachment.ParentId = trtReport.id;  
                    insert myAttachment;  
                } 
                if(eFileBody != null && eFileName != null)  
                {  
                    Attachment myAttachment  = new Attachment();  
                    myAttachment.Body = efileBody;  
                    myAttachment.Name = efileName;  
                    myAttachment.ParentId = trtReport.id;  
                    insert myAttachment;  
                } 
                if(mainSurvey == 'Survey')
                {
                    uploadRelatedAttachment();
                }
              }
              PageReference pr = new PageReference('/apex/TRT_ThankYouPage');  // redirects to the thank you page
              pr.setRedirect(true);  
              return pr; 
          //}  
          //return null; 
   }
   
   // Method with returns list of case who raised suggestrion on TRT reporting tool
   public list<Case> getQuestionsInfo()
   {  
        // List the pass the values in Apex Page 
        caseQuestionsList=[Select id,TRT_Requestor_Suggestions__c,status,WCT_ResolutionNotes__c,
                           CaseNumber  from Case where  TRT_Questions__c=true and Contact.id=:contact[0].Id];
        return caseQuestionsList;
        
   }
   // Method which to handle mutliselect values
   public string getMultiSelect(list<string> leaCheckboxSelections)
   {
        String  multiString = '';
        if(!leaCheckboxSelections.isEmpty() && leaCheckboxSelections != null)
        {
            system.debug(leacheckboxSelections);
            Boolean Start = true;
            for(string c:leacheckboxSelections)
            {
            if(Start) 
            {
                multiString = c;
                Start = false;
            }else 
            {               
                multiString = multiString + ';' + c;
            }
         }
      }
      return multiString;
   }
   // To add attachments and to delete the documents
   public void uploadRelatedAttachment()
   {
      if(!docIdList.isEmpty()) 
      { //If there is any Documents Attached in Web Form
        List<String> selectedDocumentId = new List<String>();
        for(AttachmentsWrapper aWrapper : UploadedDocumentList)
        {
            if(aWrapper.isSelected)
            {
                selectedDocumentId.add(aWrapper.documentId);
            }
        }
        /* Select Documents which are Only Active (Selected) */
        List<Document> selectedDocumentList = new List<Document>();
        if(!selectedDocumentId.isEmpty())
        {
            selectedDocumentList = [SELECT id,name,ContentType,Type,Body FROM  Document WHERE  id IN :selectedDocumentId];
        }
        /* Convert Documents to Attachment */
        List<Attachment> attachmentsToInsertList = new List<Attachment>();
        for(Document doc : selectedDocumentList)
        {   Attachment a = new Attachment();
            a.body = doc.body;
            a.ContentType = doc.ContentType;
            a.Name= doc.Name;
            a.parentid = trtReport.id; //Case Record Id which is been recently created.
            attachmentsToInsertList.add(a);                
        }
        if(!attachmentsToInsertList.isEmpty())
        {
            insert attachmentsToInsertList;
        }
        List<Document> listDocuments = new List<Document>();
        listDocuments = [SELECT Id FROM Document WHERE Id IN :docIdList];
        if( (null != listDocuments) && (0 < listDocuments.size()) )
        {
            delete listDocuments;
        }
          docIdList.clear();
      }
    }
    public void addAttachment(){
        addAttachment=true;
        doc = new document();
    }
    public void canAttachment(){
        addAttachment=false;
    }

    /*Invoked when Upload Button in VF page is clicked and the IDs are stored in the docIdList*/
    /*All the Files Uploaded are stored in the Documents. Documents does not require parent Id.This method is used to circumvent to upload
      the documents first and then add as Attachment to the Cases (Related List)*/
    public void uploadAttachment()
    {
        //System.debug('@@@@..1..' + doc.name);
        if(doc != null)
        {
            doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
            
            if(doc.body != null) 
            {
                insert doc;
                docIdList.add(doc.id);
                doc = [SELECT id, name, bodylength FROM Document WHERE id = :doc.id];
                String size = '';
                if(1048576 < doc.BodyLength)
                {
                    // Size greater than 1MB
                    size = '' + (doc.BodyLength / 1048576) + ' MB';
                }
                else if(1024 < doc.BodyLength) 
                {
                    // Size greater than 1KB
                    size = '' + (doc.BodyLength / 1024) + ' KB';            
                }
                else 
                {
                    size = '' + doc.BodyLength + ' bytes';
                } 
                UploadedDocumentList.add(new AttachmentsWrapper(true, doc.name, doc.id,size));
                doc = new Document();
            }   
        }
        
    }       
    
    //Documents Wrapper
    public class AttachmentsWrapper
    {
        public Boolean isSelected {get;set;}
        public String docName {get;set;}
        public String documentId {get;set;}
        public String size {get; set;}
        
        public AttachmentsWrapper(Boolean selected, String Name, String Id, String size)
        {
            isSelected = selected;
            docName = Name ;
            documentId = Id;
            this.size = size;
        }        
    }

  }