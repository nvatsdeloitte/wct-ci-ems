/*****
ClassName:TRT_Reportingtrigger_ctrl
Description:Controller Which calls a case trigger for validations and field Updates
CreatedDate:7th April 2015
Author:Deepthi Toranala
*****/
public class TRT_Reportingtrigger_ctrl {
    
    public static boolean isInsert=false;
    Public static void caseBeforeUpdate(Case[] lstCase,Case[] oldLstCase){
        
        // Retrieving the record type of Case
        map<id,case> oldMapCase = new map<id,case>();
        for(case c:oldLstCase)
        {
            oldMapCase.put(c.id,c);  
        }
        Schema.DescribeSObjectResult Cas = Case.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByNames = Cas.getRecordTypeInfosByName();// getting the record Type Info
        Id caseRtId =rtMapByNames.get('Case Mail Consolidation').getRecordTypeId();//particular RecordId by  Name
        system.debug('###############isInsert :: '+isInsert);
        for(Case c:lstCase)
        {
            case oldCase = new Case();
            if(oldMapCase != null && !oldMapCase.isEmpty())
            {
                oldCase=oldMapCase.get(c.id);
            }
            if(string.valueOf(c.OwnerId).startsWith('00G') && c.WCT_Category__c=='TRT Reporting' && c.RecordTypeId==caseRtId && isInsert==false && string.valueOf(oldCase.OwnerId).startsWith('00G') )
            {
                system.debug('################itsa update call:: ');
                c.adderror('Please assign the record to a user before editing ');
            }
        }
    }
    public static void caseAfterUpdate(case[] lstCase,Case[] oldLstCase)
    {
        list<id> caseIds = new list<id>();
        list<Case_form_Extn__c> cfeList = new list<Case_form_Extn__c>();
        list<Case_form_Extn__c> updateCfeList = new list<Case_form_Extn__c>();
        map<id,list<Case_form_Extn__c>> cfeMap = new map<id,list<Case_form_Extn__c>>();
        map<id,case> oldMapCase = new map<id,case>();
        for(case c:oldLstCase)
        {
            oldMapCase.put(c.id,c);  
        }
        for(case c:lstCase)
        {
            caseIds.add(c.id);
        }
        for(Case_form_Extn__c cfe:[select id,TRT_Owner_Assigned_DateTime__c,GEN_Case__c from Case_form_Extn__c where GEN_Case__c IN :caseIds])
            if(cfeMap.get(cfe.GEN_Case__c) == null)
        {
            cfeMap.put(cfe.GEN_Case__c,new list<Case_form_Extn__c>{cfe});
        }
        else{
            cfeMap.get(cfe.GEN_Case__c).add(cfe);
        }
        for(case c:lstCase){
            case oldCase = new Case();
            if(oldMapCase != null && !oldMapCase.isEmpty()){
                oldCase=oldMapCase.get(c.id);
            }
            if(cfeMap != null && !cfeMap.isEmpty())
            {
                if(c.TRT_Assigned_Owner__c != null && oldCase.ownerID != c.ownerID && string.valueOf(oldCase.ownerID).startsWith('00G') )
                {
                    cfeList=cfeMap.get(c.id);
                    for(Case_form_Extn__c cfe :cfeList)
                    {
                        cfe.TRT_Owner_Assigned_DateTime__c=System.now();
                        //cfe.TRT_Status_Checkbox__c=true;
                        updateCfeList.add(cfe);
                    }
                }
            }
            update updateCfeList;
        }
        
    }
    
    public static void caseformBeforeUpdate(Case_form_Extn__c[] cfe)
    {
        set<id> caseId = new set<id>();
        set<case> caseRec = new set<case>();
        map<id,case> caseMap = new map<id,case>();
        for(Case_form_Extn__c cf:cfe)
        {   
            caseId.add(cf.GEN_Case__c);
        }
        for(case c:[select id,TRT_Assigned_Owner__c from case where id IN :caseId])
        {         
            caseMap.put(c.id,c);
        }
        
        for(Case_form_Extn__c cf:cfe)
        {
            case objCase = new case();
            if(!caseMap.isEmpty())
            {
                objCase=caseMap.get(cf.GEN_Case__c);
            }
            if(objCase.TRT_Assigned_Owner__c== null && cf.TRT_Owner_Assigned_DateTime__c == null)
            {
                cf.adderror('Please assign the case record to a user before editing ');
            }
        }
    }
    public static void caseformAfterUpdate(Case_form_Extn__c[] cfe,Case_form_Extn__c[] oldcfe)
    {
        list<id> objIds = new list<id>();
        for(Case_form_Extn__c cf:cfe)
        {
            if(cf.GEN_Case__c != null )
            {
                objIds.add(cf.GEN_Case__c);
            }
            
        }
        
        map<id,Case_form_Extn__c> oldMapCF = new map<id,Case_form_Extn__c>();
        for(Case_form_Extn__c c:oldcfe){
            oldMapCF.put(c.id,c);  
        }
        list<Case> casList = new list<Case>();
        map<id,Case> caseMap = new map<id,Case>();
        for(Case cas:[Select ownerId,id from Case where Id in :objIds])
        {
            caseMap.put(cas.id,cas);
        }
        for(Case_form_Extn__c cf:cfe)
        {
            Case_form_Extn__c oldCF = new Case_form_Extn__c();
            if(oldMapCF != null && !oldMapCF.isEmpty())
            {
                oldCF=oldMapCF.get(cf.id);
            }
            if(oldCF.TRT_Request_Status__c != cf.TRT_Request_Status__c)
            {
            	if(cf.GEN_Case__c != null)
                {
                	Case objCas = new Case();
                    objCas=caseMap.get(cf.GEN_Case__c);
                    objCas.Status=cf.TRT_Request_Status__c;
                    if(cf.TRT_Request_Status__c =='Closed With Report'  || cf.TRT_Request_Status__c=='Closed Without Report')
                    {
                    	objCas.Status='Closed';
                    }
                        casList.add(objCas);
                        system.debug(objCas);
                    }
                    
                }
        }
        try
        {
            if(casList != null && !casList.isEmpty())
            {
                update casList;
            }
        }catch(Exception e){
            system.debug('Dml Exception'+e);
            
        }
        
    }
    
    public static void updateEscalationtime(List<Case_form_Extn__c> newList,Case_form_Extn__c[] oldcfe){
        
        map<id,Case_form_Extn__c> oldMapCF = new map<id,Case_form_Extn__c>();
        for(Case_form_Extn__c c:oldcfe)
        {
            oldMapCF.put(c.id,c);  
        }
        for(Case_form_Extn__c ext :newList )
        {
        	Case_form_Extn__c oldCF = new Case_form_Extn__c();
            if(oldMapCF != null && !oldMapCF.isEmpty())
            {
                oldCF=oldMapCF.get(ext.id);
            }
            DateTime mergedDateTime;
            DateTime mergedDateTimeOne;
            DateTime mergedDateTimesix;
            integer tempInt;
            string temp;
            DateTime rmergedDateTimeone;
            DateTime rmergedDateTimethree;
            DateTime rmergedDateTimesix;
            integer rtempInt;
            string rtemp;
            DateTime assignedtime;
            
            if(ext.TRT_Delivery_Date__c != null && oldCF.TRT_Delivery_Date__c !=ext.TRT_Delivery_Date__c)
            {
                ext.TRT_Merged_business_hour_date_time__c=ext.TRT_Delivery_Date__c;
                ext.TRT_Merged_delivery_date_time_one__c=ext.TRT_Delivery_Date__c;
                ext.TRT_Merged_delivery_date_six__c=ext.TRT_Delivery_Date__c;
                temp= ext.TRT_Delivery_Time__c;
                If(string.isNotEmpty(temp) )
                {
                    temp=temp.substring(0,2);
                }
                
                mergedDateTime= ext.TRT_Merged_business_hour_date_time__c;
                mergedDateTimeOne= ext.TRT_Merged_delivery_date_time_one__c;
                mergedDateTimesix= ext.TRT_Merged_delivery_date_six__c;
                If(string.isNotEmpty(temp) )
                {
                    tempInt=Integer.valueOf(temp);
                    
                }
            }
            if(ext.TRT_Renegotiate_Delivery_Time__c != null && oldCF.TRT_Renegotiate_Delivery_Time__c != ext.TRT_Renegotiate_Delivery_Time__c)
            {
                ext.TRT_Remerged_date_time_1__c=ext.TRT_Renegotiate_Delivery_Date__c;
                ext.TRT_Remerged_date_time_3__c=ext.TRT_Renegotiate_Delivery_Date__c;
                ext.TRT_Remerged_date_time_6__c=ext.TRT_Renegotiate_Delivery_Date__c;
                rtemp=ext.TRT_Renegotiate_Delivery_Time__c;
                if(string.isNotEmpty(rtemp)){
                    rtemp=rtemp.substring(0,2);
                }
                system.debug(' ######'+temp);
                
                
                If(string.isNotEmpty(rtemp) ){
                    rtempInt=Integer.valueOf(rtemp);
                }
                system.debug(' ######'+tempInt);
                rmergedDateTimeone= ext.TRT_Remerged_date_time_1__c;
                rmergedDateTimethree= ext.TRT_Remerged_date_time_3__c;
                rmergedDateTimesix= ext.TRT_Remerged_date_time_6__c;
            }
            
            if( (mergedDateTime != null && mergedDateTimeOne !=null && mergedDateTimesix !=null) || (rmergedDateTimesix !=null && rmergedDateTimethree !=null && rmergedDateTimeone !=null))
            {
                
                If(string.isNotEmpty(temp)  ){
                    mergedDateTime=  mergedDateTime.addHours(tempInt);
                    mergedDateTime=  mergedDateTime.addHours(6);
                    mergedDateTimesix=  mergedDateTimesix.addHours(tempInt);
                    mergedDateTimesix= mergedDateTimesix.addHours(6);
                    mergedDateTimeOne= mergedDateTimeOne.addHours(tempInt);
                    mergedDateTimeOne= mergedDateTimeOne.addHours(6);
                }
                If(string.isNotEmpty(rtemp)  ){
                    rmergedDateTimeone=  rmergedDateTimeone.addHours(rtempInt);
                    rmergedDateTimeone= rmergedDateTimeone.addHours(6);
                    rmergedDateTimethree= rmergedDateTimethree.addHours(rtempInt);
                    rmergedDateTimethree= rmergedDateTimethree.addHours(6);
                    rmergedDateTimesix= rmergedDateTimesix.addHours(rtempInt);
                    rmergedDateTimesix= rmergedDateTimesix.addHours(6);
                }
                system.debug('############'+ext.TRT_Merged_business_hour_date_time__c+'   d'+mergedDateTime);
                if (mergedDateTime != null && mergedDateTimesix != null && mergedDateTimeOne != null ) {
                    mergedDateTime=BusinessHours.add(system.label.TRT_BussinessHours_WH, mergedDateTime, 10800000);
                    mergedDateTimesix=BusinessHours.add(system.label.TRT_BussinessHours_WH, mergedDateTimesix, 21600000);
                    mergedDateTimeOne=BusinessHours.add(system.label.TRT_BussinessHours_WH, mergedDateTimeOne, 86400000);
                } 
                if(rmergedDateTimethree !=null && rmergedDateTimesix !=null && rmergedDateTimeone !=null){
                    rmergedDateTimethree=BusinessHours.add(system.label.TRT_BussinessHours_WH, rmergedDateTimethree, 10800000);
                    rmergedDateTimesix=BusinessHours.add(system.label.TRT_BussinessHours_WH, rmergedDateTimesix, 21600000);
                    rmergedDateTimeone=BusinessHours.add(system.label.TRT_BussinessHours_WH, rmergedDateTimeone, 86400000);
                }
                if(ext.TRT_Delivery_Time__c != null && oldCF.TRT_Delivery_Date__c !=ext.TRT_Delivery_Date__c){
                    ext.TRT_Merged_business_hour_date_time__c=mergedDateTime;
                    ext.TRT_Merged_delivery_date_time_one__c=mergedDateTimeOne;
                    ext.TRT_Merged_delivery_date_six__c=mergedDateTimesix;
                }
                if(ext.TRT_Renegotiate_Delivery_Time__c != null && oldCF.TRT_Renegotiate_Delivery_Time__c != ext.TRT_Renegotiate_Delivery_Time__c)
                {
                    ext.TRT_Remerged_date_time_3__c=rmergedDateTimethree;
                    ext.TRT_Remerged_date_time_6__c=rmergedDateTimesix;
                    ext.TRT_Remerged_date_time_1__c=rmergedDateTimeone;
                }
            }
            if (ext.TRT_Owner_Assigned_DateTime__c != null && oldCF.TRT_Owner_Assigned_DateTime__c != ext.TRT_Owner_Assigned_DateTime__c)
            {
                assignedtime= ext.TRT_Owner_Assigned_DateTime__c ;
                assignedtime= BusinessHours.add(system.label.TRT_BussinessHours_WH, assignedtime, 10800000);
                ext.TRT_Assigned_time_BH__c= assignedtime;
                
            }
            
        }
    }
    public static void updatecreateddate(List<Case> caselist,Case[] oldLstCase)
    {
    	// Retrieving the record type of Case
        map<id,case> oldMapCase = new map<id,case>();
        for(case c:oldLstCase)
        {
            oldMapCase.put(c.id,c);  
        }
         
        for(Case cbh:caselist)
        {
        	case oldCase = new Case();
            if(oldMapCase != null && !oldMapCase.isEmpty())
            {
                oldCase=oldMapCase.get(cbh.id);
            }
            Datetime Businesshourcreated;
            Datetime businesshourcreatedfive;
            if (cbh.TRT_BH_New_Created_date__c !=null && oldCase.TRT_BH_New_Created_date__c != cbh.TRT_BH_New_Created_date__c)
            {
            	Businesshourcreated= cbh.TRT_BH_New_Created_date__c  ;
            	businesshourcreatedfive= cbh.TRT_BH_New_Created_date__c  ;
            }
            system.debug('######################################before' +Businesshourcreated +businesshourcreatedfive);
            if (Businesshourcreated !=null && businesshourcreatedfive !=null)
            {
            	Businesshourcreated=BusinessHours.add(system.label.TRT_BussinessHours_WH, Businesshourcreated, 10800000);
            	businesshourcreatedfive=BusinessHours.add(system.label.TRT_BussinessHours_WH, businesshourcreatedfive, 18000000);
            }
            system.debug('######################################after' +Businesshourcreated +businesshourcreatedfive);
            if ( Businesshourcreated !=null && businesshourcreatedfive !=null)
            {
            	cbh.TRT_BH_Created_date_3__c=Businesshourcreated;
            	cbh.TRT_BH_Created_date_5__c=businesshourcreatedfive;
            }
            system.debug('######################################last' +Businesshourcreated +businesshourcreatedfive);
        }
    }
    
    
}