/*****************************************************************************************
    Name    : Test_Data_Utility
    Desc    : Class to create test/dummy data to be used in test classes.
    Approach: 
                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Rahul Agarwal                 09/10/2013         Created 
Rahul Agarwal                 09/11/2013         Modified
******************************************************************************************/
@isTest
public class Test_Data_Utility{
    
    /****************************************************************************************
    * @author      - Rahul Agarwal
    * @date        - 09/10/2013
    * @description - This method creates Case
    *****************************************************************************************/
    public static List<Case> createCase(){
        list<Case> caseList = new list<Case>();
        map<String, RecordType> devNameRecTypeMap = new map<String, RecordType>();
        //Querying all the case record types
        devNameRecTypeMap = Case_Utility.devNameRecType('Case');
        // creating contacts and querying one of them
        List<Contact> lstContacts = Test_Data_Utility.createContact();
        Contact con = lstContacts[0];
        // Creating a case for every record type
        for(RecordType r : devNameRecTypeMap.Values()){
            Case c = new Case(RecordTypeId = r.Id, Status = 'New', Priority = '3 - Medium', Subject = 'Sample Subject', Description = 'Sample Description', WCT_ReportedBy__c = con.Id);
            caseList.add(c);
        }
        Case c1 = new Case(contactId=con.Id,Status = 'New', Priority = '1 - Critical', Subject = 'Sample Subject', Description = 'Sample Description', WCT_ReportedBy__c = con.Id);
        caseList.add(c1);
        insert caseList;
        
        return caseList;
    }
    /****************************************************************************************
    * @author      - Rahul Agarwal
    * @date        - 09/11/2013
    * @description - This method creates Contacts
    *****************************************************************************************/
    public static List<Contact> createContact(){
        list<Contact> contactList = new list<Contact>();
        // Creating 10 contacts
        for(Integer i = 0; i < 10 ;i++){
            Contact c = new Contact(LastName = 'Contact' + i);
            contactList.add(c);
        }
        insert contactList;
            
       return contactList;
    }
    
    /****************************************************************************************
    * @author      - Ashish Shishodiya
    * @date        - 09/11/2013
    * @description - This method creates Account for contact
    *****************************************************************************************/
    public static List<Account> createAccount(){
     
    
    List<Account> accountList = new List<Account>();
      Account accountUS = new Account();
      accountUS.name = 'Deloitte US Offices';
      accountList.add(accountUS);
      
      Account accountUSI = new Account();
      accountUSI.name = 'Deloitte US-India Offices';
      accountList.add(accountUSI);
      
      Account accountNA = new Account();
      accountNA.name = 'Not Applicable';
      accountList.add(accountNA);
      
      Account accountNAv = new Account();
      accountNAv.name = 'Not Available';
      accountList.add(accountNAv);
      
      Account accountCa = new Account();
      accountCa.name = 'Candidates';
      accountList.add(accountCa);
      
      insert accountList;
      return accountList;
    }
    
    /****************************************************************************************
    * @author      - Rakesh Ala
    * @date        - 19 Dec 2013
    * @description - This method creates Bulk Attachments
    *****************************************************************************************/
    
    public static List<Attachment> createAttachments(){
        List<Attachment> attList = new List<Attachment>();
        for(integer j=0;j<200;j++){
            Attachment a = new Attachment();
            Blob myBlob = Blob.valueof('Test file for Attachment class');
            a.body = myBlob;
            a.ContentType = 'text/plain';
            a.Name= 'Attachment'+j;
            attList.add(a);
        }
        insert attList;
        return attList;
    }
    
    /****************************************************************************************
    * @author      - Vivek Sharma
    * @date        - 21 Jan 2014
    * @description - This method creates Bulk Attachments for the specified sObject
    *****************************************************************************************/
    
    public static List<Attachment> createAttachmentsForSpecificObject(sObject obj){
        List<Attachment> attList = new List<Attachment>();
        for(integer j=0;j<20;j++){
            Attachment a = new Attachment();
            Blob myBlob = Blob.valueof('Test file for Attachment class');
            a.body = myBlob;
            a.ContentType = 'text/plain';
            a.Name= 'Attachments'+j;
            a.Description = 'Test1';
            a.ParentId = obj.Id;
            attList.add(a);
        }
        return attList;
    }
        
    /****************************************************************************************
    * @author      - Rakesh Ala
    * @date        - 19 Dec 2013
    * @description - This method creates Candidate Documents
    *****************************************************************************************/
    
    public static List<WCT_Candidate_Documents__c> createCandidateDocuments(){
        List<WCT_Candidate_Documents__c> candDocList = new List<WCT_Candidate_Documents__c>();
        for(integer i=0;i<200;i++){
            WCT_Candidate_Documents__c c = new WCT_Candidate_Documents__c();
            c.WCT_File_Name__c = 'Attachment'+i;
            candDocList.add(c);
        }
        insert candDocList;
        return candDocList;
    }
        
    /****************************************************************************************
    * @author      - Rakesh Ala
    * @date        - 15 Jan 2014
    * @description - This method creates data for WCT_CreateCandidateTracker_Test class
    *****************************************************************************************/
     public static List<Contact> createContacts(){
        map<String, RecordType> ContactdevNameRecTypeMap = new map<String, RecordType>();
        ContactdevNameRecTypeMap = Case_Utility.devNameRecType('Contact');
        list<Contact> contactList = new list<Contact>();
        for(RecordType r : ContactdevNameRecTypeMap.Values()){
            Contact c = new Contact(LastName = 'Contact',recordTypeId = r.id,phone='8686322286',WCT_Taleo_Id__c ='12345');
            contactList.add(c);
        }
        insert contactList;
        return contactList;
     }
     public static WCT_Requisition__c createReq(){
          WCT_Requisition__c requisition = new WCT_Requisition__c(Name = 'Testing');
          insert requisition;
          return requisition;
     
     }
     public static WCT_List_Of_Names__c createListOfNames(String Name,Id rTypeId,String type){
        WCT_List_Of_Names__c listOfName = new WCT_List_Of_Names__c(Name=Name,recordTypeId=rTypeId,WCT_Type__c=type);
        insert listOfName;
        return listOfName;
    }
    
    /****************************************************************************************
    * @author      - Anusha
    * @date        - 20 Jan 2014
    * @description - This method creates data for ContactTriggerHandler_AT() class
    *****************************************************************************************/
     public static Contact createAdhocContacts(String AdhocEmail){
      
                
     Contact adhocContact = new Contact(LastName = 'AdhocContact',recordTypeId = label.AdhocCandidateRecordtypeID,phone='8686322286',Email =AdhocEmail); 
     insert adhocContact ;
     return adhocContact ;
     
     }
     
     public static WCT_Candidate_Requisition__c  createCandidateRequisition(Id AdhocId){
      
                
     WCT_Candidate_Requisition__c  canReq = new WCT_Candidate_Requisition__c (WCT_Contact__c = AdhocID,WCT_Requisition__c = Test_Data_Utility.createReq().id ); 
     insert canReq;
  
     return canReq;
     
     }   
     
     public static Contact createContacts1(String AdhocEmail){            
         Contact Contact1 = new Contact(LastName = 'Contact1',recordTypeId = label.CandidateRecordtypeId,phone='8686329006',Email =AdhocEmail);
         insert Contact1;
         return    Contact1;
     }  
     public static Case createCase(Id AdhocId)
     {
         Case case1 = new Case(Status = 'New', Priority = '3 - Medium', Subject = 'Sample Subject', Description = 'Sample Description', ContactId = AdhocID);
         insert case1;
         return case1;
     }
     public static Task createTask(Id AdhocId)
     {
         Task Task1 = new Task(WhoId = AdhocID);
         insert Task1;
         return Task1 ;
     }
     
     public static WCT_Candidate_Documents__c createDocument(Id AdhocId)
     {
        WCT_Candidate_Documents__c document = new WCT_Candidate_Documents__c(WCT_Candidate__c = AdhocID);
        insert document;
        return document;
     }
     
     public static Note createNotes(Id AdhocId)
     {
      Note notes = new Note(Title = 'test' , ParentId = AdhocID);
      insert notes;
      return notes;
     }
     public static Attachment createAttachment(Id AdhocId)
     {
     Attachment attachment = new Attachment();
     Blob myBlob = Blob.valueof('Test file for Attachment class');
     attachment.body = myBlob;
     attachment.ContentType = 'text/plain';
     attachment.Name= 'Attachment';
     attachment.ParentId = AdhocID ;
     insert attachment;
     return attachment;
     }
     public static Contact createContacts2(String AdhocEmail){            
         Contact Contact2 = new Contact(LastName = 'Contact1',recordTypeId = label.CandidateRecordtypeId,phone='8686329006',Email =AdhocEmail,WCT_Contact_Type__c = 'Employee',WCT_Region__c = 'india/region 10');
         insert Contact2;
         return Contact2;
               
     }  
     public static WCT_Requisition__c createReq2()
     {
         Profile p = [SELECT Id FROM Profile WHERE Name =:WCT_UtilConstants.RECRUITER_COMPANY]; 
         User recutr1 = new User(Alias = 'rUser1', Email='recuiteruser1@wct.com', LastName='Testing1', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', LocaleSidKey='en_US',TimeZoneSidKey='America/Los_Angeles', ProfileId = p.Id, UserName='recuiteruser1@wct.com');
         insert recutr1;
        
     
         WCT_Requisition__c req = new WCT_Requisition__c(WCT_Recruiter_Email_ID__c = recutr1.Email );
         
         insert req;
      return req;
     }
     
     
}