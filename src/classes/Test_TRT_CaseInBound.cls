@IsTest
public class Test_TRT_CaseInBound{

    static testMethod void myTestMethod() 
    {
        // Create a new email, envelope object and Attachment
       Messaging.InboundEmail email = new Messaging.InboundEmail();
       Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
       Messaging.InboundEmail.BinaryAttachment inAtt = new Messaging.InboundEmail.BinaryAttachment();
       email.subject = 'test';
       email.fromName = 'test';
       email.fromAddress = 'user@TestApp.com';
       email.plainTextBody='test';
       // set the body of the attachment
       inAtt.body = blob.valueOf('test');
       inAtt.fileName = 'my attachment name';
       inAtt.mimeTypeSubType = 'plain/txt';
       email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] {inAtt };
       // call the class and test it with the data in the testMethod
       TRT_CaseInBound emailServiceObj = new TRT_CaseInBound();
       emailServiceObj.handleInboundEmail(email, env );       
       System.AssertEquals(email.fromName,'test');              
    }   
}