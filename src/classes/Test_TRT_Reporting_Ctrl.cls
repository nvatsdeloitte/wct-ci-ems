@isTest
public class Test_TRT_Reporting_Ctrl 
{
	
    public static Contact con;
    public static Contact c;
    public static Case objCase;
    public static Id rtId; 
    public static Id rtIdGMI;
    public static Id rtIdRM;
    public static Id rtIdPM; 
    public static Id rtIdTA;
    public static Id rtIdSur;
    public static Id rtIdOth; 
    public static Id rtIdLea;
    public static Id rtIdLeaEva;
   	public static Account a;
    public static Case_form_Extn__c trtReports; 
        
	
	static testmethod void myTestData()
	{
        
        // Retrieving the record type of Case_form_Extn__c
        Schema.DescribeSObjectResult Rep = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName = Rep.getRecordTypeInfosByName();// getting the record Type Info
        rtId =rtMapByName.get('TRT SAP Reporting').getRecordTypeId();//particular RecordId by  Name
        rtIdGMI =rtMapByName.get('TRT GMNI').getRecordTypeId();//particular RecordId by  Name
        rtIdRM =rtMapByName.get('TRT RM Reporting').getRecordTypeId();//particular RecordId by  Name
        rtIdPM =rtMapByName.get('TRT PM Reporting').getRecordTypeId();//particular RecordId by  Name
        rtIdTA =rtMapByName.get('TRT TA Reporting').getRecordTypeId();//particular RecordId by  Name
        rtIdSur =rtMapByName.get('TRT Survey').getRecordTypeId();//particular RecordId by  Name
        rtIdOth =rtMapByName.get('TRT Other Reporting').getRecordTypeId();//particular RecordId by  Name
        rtIdLea =rtMapByName.get('TRT Learning Reporting').getRecordTypeId();//particular RecordId by  Name
        rtIdLeaEva =rtMapByName.get('TRT Learning Evaluation').getRecordTypeId();//particular RecordId by  Name
        
        Schema.DescribeSObjectResult Cas = Case.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByNames = Cas.getRecordTypeInfosByName();// getting the record Type Info
        Id caseRtId =rtMapByNames.get('Case Mail Consolidation').getRecordTypeId();//particular RecordId by  Name
        
        a = new Account(Name='Deloitte US-India Offices');
        insert a ;
        
        con = new Contact();
        con.Email = 'test@deloitte.com';
      	con.LastName = 'TestName';
      	con.accountId = a.id;
      	con.WCT_Function__c='TALENT';
      	con.WCT_Job_Level_Text__c='test level';
      	insert con;
        
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        ApexPages.CurrentPage().getParameters().put('em',encrypt);
        
        TRT_Reporting_ctrl trtReportings = new TRT_Reporting_ctrl();
       
        trtReportings.loggedInContact=con;
        
       	//recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        //con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        
        c = [SELECT Name,email,WCT_Function__c,WCT_Job_Level_Text__c,id,
                       Account.Name FROM contact WHERE id=:con.id limit 1];
        
        
        trtReportings.trtReport.TRT_Group_sugg__c='SAP Report';
        trtReportings.submit();
        
        
        objCase = new case();
        objCase.Status='New';
        objCase.RecordTypeId=caseRtId;
        objCase.WCT_Category__c='TRT Reporting';  
        objCase.Origin = 'Web';
        objCase.Gen_Request_Type__c ='';
        objCase.Priority='3 - Medium';
        objCase.Description ='TRT test';  
        objCase.Gen_RecordType__c ='TRT Learning Evaluation'; 
        objCase.Subject='A reporting request';
        objCase.contactId=con.id;
        insert objCase;
        
        trtReports = new Case_form_Extn__c();
        //trtReports.TRT_Actual_Requester_email__c=c.email;
        trtReports.TRT_Requestor_Name__c = c.id;
        trtReports.TRT_Requestor_Function__c=c.WCT_Function__c;
        trtReports.TRT_Requestor_Level__c=c.WCT_Job_Level_Text__c;
        trtReports.TRT_Requester_Group__c=c.Account.Name;
        trtReports.Case_Form_App__c ='TRT Reporting';
        trtReports.TRT_Request_Status__c='New';
        trtReports.TRT_Include_PII__c=true;
        trtReportS.GEN_Case__c =objCase.id;
        insert trtReports;
        
        
        //trtReportings.getQuestionsInfo();
                    
	}
	static testmethod void trtReportingRequest1()
	{
		myTestData();
	  	TRT_Reporting_ctrl trtReporting = new TRT_Reporting_ctrl();
	  	Test.startTest();
	    
		trtReporting.mainlearningReport= 'Learning Reporting';
		trtReporting.learningReport='Learning Evaluation';
		trtReporting.sapReport();
		trtReporting.trtReport.RecordTypeId=rtIdLeaEva;
		Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
		trtReporting.efileName='test';
		trtReporting.efileBody=bodyBlob;
		trtReporting.fileName='test';
		trtReporting.fileBody=bodyBlob;
		list<string> checkList = new list<string>{'a','b'};
		trtReporting.checkboxSelections=checkList;
		trtReporting.leaCheckboxSelections=checkList;
		trtReporting.leacompCheckboxSelections=checkList;
		trtReporting.evaCheckboxSelections=checkList;
		trtReporting.surveyListCheckBox=checkList;
		trtReporting.submit();
	    trtReporting.addAttachment();
	    trtReporting.getLearning();
	    trtReporting.getRManagement();
	    trtReporting.getPManagement();
        trtReporting.getMyCheckboxes();
	    trtReporting.getTrtReport();
        trtReporting.questions();         
        trtReporting.compPannel();
        trtReporting.Enddatecheck();
        trtReporting.canAttachment();
         trtReporting.doc=WCT_UtilTestDataCreation.createDocument();
        trtReporting.uploadAttachment();
        Test.stopTest();  
	} 
	
	static testmethod void trtReportingRequest2()
	{	
		myTestData();
		TRT_Reporting_ctrl trtReporting = new TRT_Reporting_ctrl();
		trtReporting.mainlearningReport= 'Learning Reporting';
		trtReporting.learningReport='Offering';
		trtReporting.sapReport();
		trtReporting.learningReport='Competency';
		trtReporting.sapReport();
		trtReporting.learningReport='Common';
		trtReporting.sapReport();
		trtReporting.learningReport='Chargeback';
		trtReporting.sapReport();
		trtReporting.learningReport='Offering';
		trtReporting.sapReport();
		trtReporting.learningReport='Certification';
		trtReporting.sapReport();
		trtReporting.learningReport='Catalog';
		trtReporting.sapReport();
		trtReporting.trtReport.RecordTypeId=rtIdLea;
		trtReporting.submit();
		
	}
	static testmethod void trtReportingRequest3()
	{
		myTestData();	
		TRT_Reporting_ctrl trtReporting = new TRT_Reporting_ctrl();
		trtReporting.mainMultidataSources = 'Multi Data Source';
		trtReporting.sapReport();
		trtReporting.trtReport.RecordTypeId=rtIdOth;
		trtReporting.submit();
		
	}
	static testmethod void trtReportingRequest4()
	{	
		myTestData();
		TRT_Reporting_ctrl trtReporting = new TRT_Reporting_ctrl();
		trtReporting.trtReport.RecordTypeId=rtIdTA;
		trtReporting.mainTalentRequest = 'Talent Accquisition';
		trtReporting.sapReport();
		trtReporting.submit();
		
	}
	static testmethod void trtReportingRequest5()
	{	
		myTestData();
		TRT_Reporting_ctrl trtReporting = new TRT_Reporting_ctrl();
		trtReporting.trtReport.RecordTypeId=rtIdSur;
		trtReporting.mainSurvey = 'Survey';
		trtReporting.sapReport();
		trtReporting.submit();
		
	}
	
	static testmethod void trtReportingRequest6()
	{	
		myTestData();
		TRT_Reporting_ctrl trtReporting = new TRT_Reporting_ctrl();
		trtReporting.trtReport.RecordTypeId=rtIdSur;
		trtReporting.mainSurvey = 'Survey';
		trtReporting.sapReport();
		trtReporting.submit();
		
	}
	
	static testmethod void trtReportingRequest7()
	{	
		myTestData();
		TRT_Reporting_ctrl trtReporting = new TRT_Reporting_ctrl();
		trtReporting.mainPManagementReport = 'Performance Management';
		trtReporting.pManageReport ='Reinvention';
		trtReporting.trtReport.RecordTypeId=rtIdPM;
		trtReporting.sapReport();
		trtReporting.mainPManagementReport = 'Performance Management';
		trtReporting.pManageReport='DPME (Old)';
		trtReporting.sapReport();
		trtReporting.trtReport.RecordTypeId=rtIdPM;
		trtReporting.sapReport();
		trtReporting.submit();
		
	}
	static testmethod void trtReportingRequest8()
	{	
		myTestData();
		TRT_Reporting_ctrl trtReporting = new TRT_Reporting_ctrl();
		trtReporting.mainRManagementReport = 'Resource Management';
		trtReporting.rManageReport ='GSS/StaffTrak';
		trtReporting.sapReport();
		trtReporting.trtReport.RecordTypeId=rtIdRM;
		trtReporting.submit();
		
	}
	static testmethod void trtReportingRequest9()
	{	
		myTestData();
		TRT_Reporting_ctrl trtReporting = new TRT_Reporting_ctrl();
		trtReporting.mainGMI = 'GMI';
		trtReporting.sapReport();
		trtReporting.trtReport.RecordTypeId=rtIdGMI;
		trtReporting.submit();
	}
	static testmethod void trtReportingRequest10()
	{	
		myTestData();
		TRT_Reporting_ctrl trtReporting = new TRT_Reporting_ctrl();
		trtReporting.sapReport='Sap Reporting';
	    trtReporting.sapReport();
		trtReporting.trtReport.RecordTypeId=rtId;
		trtReporting.submit();
		
	}
	static testmethod void trtReportingRequest11()
	{
		myTestData();
		TRT_Reporting_ctrl trtReporting = new TRT_Reporting_ctrl();
		Test.startTest();
		trtReporting.trtReport.TRT_Group_sugg__c='Learning  Evaluation';
    	trtReporting.submit();
        trtReporting.trtReport.TRT_Group_sugg__c='Multiple Sources Report';
        trtReporting.submit();
        trtReporting.trtReport.TRT_Group_sugg__c='Talent Acquisition Report';
        trtReporting.submit();
        trtReporting.trtReport.TRT_Group_sugg__c='Survey';
        trtReporting.submit();
        Test.stopTest();
	}
	static testmethod void trtReportingRequest12()
	{
		myTestData();
		TRT_Reporting_ctrl trtReporting = new TRT_Reporting_ctrl();
		Test.startTest();
		trtReporting.trtReport.TRT_Group_sugg__c='Resource Management Report';
        trtReporting.submit();
        trtReporting.trtReport.TRT_Group_sugg__c='Performance Management Report';
        trtReporting.submit();
        trtReporting.trtReport.TRT_Group_sugg__c='Global Mobility and Immigration Report';
        trtReporting.submit();
        trtReporting.trtReport.TRT_Group_sugg__c='Learning Report';
        trtReporting.submit();
        Test.stopTest(); 
	}
	
}