/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_Trtproj_ctrl {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
         Contact con=WCT_UtilTestDataCreation.createContact();
         con.email='test@deloitte.com';       
         insert con;
         
         String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
         Test.setCurrentPageReference(new PageReference('trtproject')); 
         System.currentPageReference().getParameters().put('em', strEncryptEmail);
         System.currentPageReference().getParameters().put('ProcessArea', 'Miscellaneous');
         trtproj_ctrl trtPage = new trtproj_ctrl();
         trtPage.requesttype = 'req';
         trtPage.inc = 'inc';
         Blob b = Blob.valueOf('Test Data');  
         trtPage.attachment.Body = b;
         trtPage.attachment.Name = 'Test Attachment';
         trtPage.getProcessArea();
         trtPage.Save();
         trtPage.Clear();
         System.currentPageReference().getParameters().put('IsCreated', 'true');
         trtPage.Preinit();
         
         trtproj_ctrl trtPage2 = new trtproj_ctrl();
         trtPage2.attachment.Name = 'Test Attachment1';
         trtPage2.Save();
         
         String strEncryptEmail1 = EncodingUtil.urlDecode(CryptoHelper.encrypt('testdeloitte@deloitte.com'), 'UTF-8');
         Test.setCurrentPageReference(new PageReference('trtproject')); 
         System.currentPageReference().getParameters().put('em', strEncryptEmail1);
         trtproj_ctrl trtPage1 = new trtproj_ctrl();
         trtPage1.attachment.Name = 'Test Attachment1';
         trtPage1.Save();
         
    }
}