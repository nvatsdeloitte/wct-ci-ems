@isTest(SeeAllData=true)
public class Test_VfSearchController
{

static testMethod void TestVfSearchController() 
  {
  
    List<selectOption> lstEditOptions;
    boolean isEditError;
    string categoryType;   
  
    string categoryFilter = '';
    String categoryCondition = '';
    String ArtType = 'FAQ';
 
    String searchquery = '';
    string searchDraftQuery ;
  //  searchDraftQuery = '';
    ArtType = ArtType+'__kav';
    ArtType = ArtType.replace(' ','_');
        
    FAQ__kav test_faq1 = new FAQ__kav(Title='Test Title1',Summary='Test Summary',UrlName='Test-Title1',Expiration_Date__c = (system.now()+1).date());
    insert test_faq1;
    FAQ__kav testqu = [select id,KnowledgeArticleId,PublishStatus from FAQ__kav where id = :test_faq1.id];
    system.debug('+++'+test_faq1);
    system.debug('+++'+testqu);
    KbManagement.PublishingService.publishArticle(testqu.KnowledgeArticleId, true);
    
    FAQ__kav test_faq2 = new FAQ__kav(Title='Test Title1',Summary='Test Summary2',UrlName='Test-Title2',Expiration_Date__c = (system.now()+1).date());
    insert test_faq2;
    FAQ__kav testqu2 = [select id,KnowledgeArticleId,PublishStatus from FAQ__kav where id = :test_faq2.id];
    
    KbManagement.PublishingService.publishArticle(testqu2.KnowledgeArticleId, true);
    
    WCT_Article_Types__c wctarticles;
    List<KnowledgeArticleVersion> lstArticle ;
    ArtType = ArtType+'__kav';
    ArtType = ArtType.replace(' ','_');  
    // lstArticle =DataBase.query(searchquery) ;
      List<KnowledgeArticleVersion> lstArticleDraft = new List<KnowledgeArticleVersion>(); 
     //  lstArticleDraft = DataBase.query(searchDraftQuery) ;
  
    FAQ__DataCategorySelection fdc1 = new FAQ__DataCategorySelection(DataCategoryGroupName='KMCallCenter',DataCategoryName='US',parentId=testqu.id);
    insert fdc1;
    
    FAQ__DataCategorySelection fdc2 = new FAQ__DataCategorySelection(DataCategoryGroupName='KMCallCenter',DataCategoryName='India',parentId=testqu.id);
    insert fdc2;
    
    FAQ__DataCategorySelection fdc3 = new FAQ__DataCategorySelection(DataCategoryGroupName='KMCallCenter',DataCategoryName='US_PSN',parentId=testqu2.id);
    insert fdc3;
    
       //ApexPages.StandardController sc = new ApexPages.StandardController(newrecevent);  
      VfSearchController createCon = new VfSearchController(); 
       VfSearchController.ArticleTable rs = new VfSearchController.ArticleTable();
             
       PageReference pageRef = Page.ArticleList;
     Test.setCurrentPage(pageRef);
     //createCon.ArtType = 'FAQ__KAV';
     createCon.setArtType('FAQ');
     createCon.getArtType();
     //createCon.search();
     createCon.isEditError = true;
     createCon.getCategoryKeyword();
     createCon.refreshSearchResult();
     createCon.categoryFilter = '';
     createCon.getItems();
     createCon.getDataCategoryGroupInfo();
     createCon.search();
     createCon.lstArticleTable[0].EditArchive = 'Edit';
     createCon.lstArticleTable[1].EditArchive = 'Archive';   
     createCon.saveArticleStatus();
     

     }
     
  static testMethod void validateCategoryKeyword()
 {
VfSearchController vfSearchControllerObj = new VfSearchController();
DataCategoryGroupInfo[] categoryGroups = DataCategoryUtil.getInstance().getAllCategoryGroups();
String categoryCondition = '';
for (DataCategoryGroupInfo categoryGroup : categoryGroups) {
ApexPages.currentPage().getParameters().put('categoryType_'+categoryGroup.getName(),'All');
if(categoryCondition==''){
categoryCondition=categoryCondition+categoryGroup.getName() + ':' +
System.currentPageReference().getParameters().Get('categoryType_'+categoryGroup.getName());
}else {
categoryCondition=categoryCondition + ',' +categoryGroup.getName() + ':' +
System.currentPageReference().getParameters().Get('categoryType_'+categoryGroup.getName());
}
}
System.assertEquals(categoryCondition, vfSearchControllerObj.getCategoryKeyword());
}
}