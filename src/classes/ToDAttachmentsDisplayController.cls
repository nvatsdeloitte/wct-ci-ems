public class ToDAttachmentsDisplayController extends SitesTodHeaderController {
    public List<Attachment> listAttachments {get; set;}
    public Map<Id, String> mapAttachmentSize {get; set;}  
   
    public ToDAttachmentsDisplayController(ApexPages.StandardController controller) {
        if(invalidEmployee) {
            return;
        }
        String parentId = ApexPages.currentPage().getParameters().get('id');  
        listAttachments = new List<Attachment>();
        listAttachments = [
                           SELECT
                               ParentId , 
                               Name, 
                               BodyLength, 
                               Id,
                               CreatedDate
                           FROM 
                               Attachment 
                           WHERE
                               ParentId = :parentId
                           ORDER BY
                               CreatedDate DESC
                           LIMIT
                               50
                   ];    
        mapAttachmentSize = new Map<Id, String>();
        for(Attachment a : listAttachments) {
            String size = null;
            if(1048576 < a.BodyLength) {
                // Size greater than 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength) {
                // Size greater than 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else {
                size = '' + a.BodyLength + ' bytes';
            }
            
            mapAttachmentSize.put(a.id, size);
        }                         
    }    
}