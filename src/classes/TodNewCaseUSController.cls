public class TodNewCaseUSController extends SitesTodHeaderController {
    
    /* Public Variables */
    public Case caseRecord {get; set;}
    public Document doc {get;set;}
    public List<String> docIdList = new List<string>();
    public List<AttachmentsWrapper> UploadedDocumentList {get; set; }
    public List<WCT_List_of_Names__c> listOfNames {get; set;}
    public Boolean isSaIndia {get;set;}
    public String ElecomQ1 {get; set;}
    public String ElecomQ1other {get; set;}
    public String ElecomQ2A {get; set;}
    public String ElecomQ2B {get; set;}
    
    // Error Message related variables
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}    
    
    private String urlTodCat = null;
    private String urlTodSupportArea = null;
    
    public TodNewCaseUSController() {
        if(invalidEmployee) {
            return;
        }

        //Set Support Area - India to be false
        isSaIndia = false;

        //Create a New Case Record
        caseRecord = new Case();

        //Populate Email        
        caseRecord.suppliedEmail = loggedInContact.email;  
        
        /*This method is used to process all the values from Parameter
         and store appropriate Info into Case Record*/
        getParameterInfo();        
        
        /*This method is used to get all the Category Names(Short Form)
          and get related Info for each Category */
        getCategoryList();
        
        // Set Case Category
        if(String.isNotEmpty(urlTodCat)){
            for(WCT_List_of_Names__c ln : listOfNames){
                if(urlTodCat.equals(ln.Name)) {
                    //Set Initial TOD Case Category when passed through Parameters
                    caseRecord.ToD_Case_Category__c = ln.ToD_Case_Category__c;
                    break;
                }
            }
        }
        
        // Set Support Area
        if(String.isNotEmpty(urlTodSupportArea)){
            if(urlTodSupportArea == 'IN'){
                System.debug('Setting up India');
                caseRecord.WCT_Support_Area__c = 'INDIA';
            }

            if(urlTodSupportArea == 'US'){
                System.debug('Setting up US');
                caseRecord.WCT_Support_Area__c = 'US';
            }

            if(urlTodSupportArea == 'PSN'){
                System.debug('Setting up PSN');
                caseRecord.WCT_Support_Area__c = 'US-PSN';
            }
            if(urlTodSupportArea == 'FIN'){
                System.debug('Setting up FIN');
                caseRecord.WCT_Support_Area__c = 'FIN';
            }
        }        
        
        doc = new Document();
        UploadedDocumentList = new List<AttachmentsWrapper>();
                
        //pageError = false;
     
    }

    private void getCategoryList(){
        listOfNames = new List<WCT_List_of_Names__c>();
        String query = 'SELECT Name, ToD_Case_Category__c, ToD_Case_Category_Instructions__c, Sub_Category_1__c, ' +
                       'WCT_Category__c, TOD_Service_Area__c FROM WCT_List_of_Names__c ' +
                       'WHERE WCT_Type__c = \'ToD\'';
        if(String.isNotEmpty(urlTodSupportArea)) {
            query = query + ' AND TOD_Service_Area__c = :urlTodSupportArea';
        }
        listOfNames = Database.query(query);
        system.debug('list of names'+listOfNames);
        for(WCT_List_of_Names__c ln : listOfNames) {
        system.debug('list of namesetc'+ln);
            if(null != ln.ToD_Case_Category_Instructions__c) {
            system.debug('beforeln.ToD_Case_Category_Instructions__c'+ln.ToD_Case_Category_Instructions__c);
                ln.ToD_Case_Category_Instructions__c = ln.ToD_Case_Category_Instructions__c.replaceAll('\r', ' ');
                ln.ToD_Case_Category_Instructions__c = ln.ToD_Case_Category_Instructions__c.replaceAll('\n', ' ');
                system.debug('ln.ToD_Case_Category_Instructions__c'+ln.ToD_Case_Category_Instructions__c);
            }
        }
    }
    
    private void getParameterInfo(){        
        // Get Category Values from 'cat'
        urlTodCat = ApexPages.currentPage().getParameters().get('cat');  
        System.debug('------########-----'+urlTodCat );   
        System.debug('-------############----'+ApexPages.currentPage().getParameters() );     
        // Get Support Area Value from 'sa'
        urlTodSupportArea = ApexPages.currentPage().getParameters().get('sa');
    }

    public PageReference saveCase() {
   
        /* Handle Validation Rules */
           if( ('' == caseRecord.WCT_ContactChannel__c)||(null == caseRecord.WCT_ContactChannel__c) ||(null == caseRecord.ToD_Case_Category__c) ||
            ((caseRecord.ToD_Case_Category__c!='USI Tax Professionals - Compensation Query')&&(null == caseRecord.subject)) || ((caseRecord.ToD_Case_Category__c!='USI Tax Professionals - Compensation Query')&&('' == caseRecord.subject)) ||
            ((caseRecord.ToD_Case_Category__c!='USI Tax Professionals - Compensation Query')&&(null == caseRecord.description)) || ((caseRecord.ToD_Case_Category__c!='USI Tax Professionals - Compensation Query')&&('' == caseRecord.description)) ) {
            System.debug('-----------'+caserecord.subject);
            
            pageErrorMessage = 'Please fill in all the required fields in the form.';
            pageError = true;
            return null;
        }
        
          /*Make Field Required on Tod Form  for subcategories */
       /* if(caseRecord.ToD_Case_Category__c=='Campus Recruiting Inquiries' || caseRecord.ToD_Case_Category__c=='Experienced Recruiting Inquiries' || caseRecord.ToD_Case_Category__c=='Compensation and Benefits Inquiries')
          {
           if(null == caseRecord.ToD_Sub_Category1__c )
             {
             pageErrorMessage = 'Please fill in all the required fields in the form.';
             pageError = true;
             return null;
             }
           } 
           
         if(caseRecord.ToD_Sub_Category1__c=='AERS' || caseRecord.ToD_Sub_Category1__c=='Consulting' ||
             caseRecord.ToD_Sub_Category1__c=='TAX'||caseRecord.ToD_Sub_Category1__c=='FAS' ||
              caseRecord.ToD_Sub_Category1__c=='Enabling Areas')
          {
           if(null == caseRecord.ToD_Sub_Category2__c)
             {
             pageErrorMessage = 'Please fill in all the required fields on the form.';
             pageError = true;
             return null;
             }
           }       */
        
        /* Set Origin */        
        caseRecord.origin = 'Web';
        caseRecord.Questions_on_my_compensation__c=ElecomQ1;
        caseRecord.request_for_a_conversation_with_the_Loca__c= ElecomQ2A;
        /* Set Contact */
        caseRecord.ContactID = loggedInContact.id;

        /* Set Salesforce Case Category */
        for(WCT_List_of_Names__c ln : listOfNames){
            if(caseRecord.ToD_Case_Category__c == ln.ToD_Case_Category__c) {

                //Set Salesforce Category
                caseRecord.WCT_Category__c = ln.WCT_Category__c;
                
                //set Salesforce Sub Category
                caseRecord.WCT_SubCategory1__c = ln.Sub_Category_1__c;
                
                break;
            }
        }

        /* Set Support Area. */
        /*if(isSaIndia == false){
            system.debug('I am inside Flag');
            for(WCT_List_of_Names__c ln : listOfNames){
                if(caseRecord.ToD_Case_Category__c == ln.ToD_Case_Category__c) {
                    if(ln.ToD_Case_Category__c == 'Benefits-Dental' || ln.ToD_Case_Category__c == 'Benefits-Medical/Prescription' || ln.ToD_Case_Category__c ==  'Benefits-Retirement' || ln.ToD_Case_Category__c ==  'Benefits-Vision' || ln.ToD_Case_Category__c ==  'Other' || ln.ToD_Case_Category__c ==  'Payroll'){
                        caseRecord.WCT_Support_Area__c = 'US-PSN';
                    }
                    
                }
            }
        }*/
        
        
        /*Using Assignment Rules for Cases. This is used for Applying corresponding Queue Values*/
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId = System.Label.Case_Assignment_Rule_Id;
        caseRecord.setOptions(dmlOpts);

        /*Insert Case Record*/
        insert caseRecord;

        /* Upload Documents as Related Attachments to Case After the Case Record is created */
        uploadRelatedAttachment();

        /*Set Page Reference After Save*/
        return new PageReference('/apex/todCaseUS?id=' + caseRecord.id + '&em=' + CryptoHelper.encrypt(loggedInContact.Email)
                            + '&sa=' + urlTodSupportArea);
    }
    

    private void uploadRelatedAttachment(){

        if(!docIdList.isEmpty()) { //If there is any Documents Attached in Web Form

            List<String> selectedDocumentId = new List<String>();
            for(AttachmentsWrapper aWrapper : UploadedDocumentList) {
                if(aWrapper.isSelected) {
                    selectedDocumentId.add(aWrapper.documentId);
                }
            }
            
            /* Select Documents which are Only Active (Selected) */
            List<Document> selectedDocumentList = new List<Document>();
            if(!selectedDocumentId.isEmpty()){
                selectedDocumentList = [
                                           SELECT 
                                               id,
                                               name,
                                               ContentType,
                                               Type,
                                               Body 
                                           FROM 
                                               Document 
                                           WHERE 
                                               id IN :selectedDocumentId
                                        ];
            }
            
            /* Convert Documents to Attachment */
            List<Attachment> attachmentsToInsertList = new List<Attachment>();
            for(Document doc : selectedDocumentList){
                Attachment a = new Attachment();
                a.body = doc.body;
                a.ContentType = doc.ContentType;
                a.Name= doc.Name;
                a.parentid = caseRecord.id; //Case Record Id which is been recently created.
                attachmentsToInsertList.add(a);                
            }
            
            if(!attachmentsToInsertList.isEmpty()){
                insert attachmentsToInsertList;
            }
            
            List<Document> listDocuments = new List<Document>();
            listDocuments = [
                                SELECT
                                    Id
                                FROM
                                    Document
                                WHERE
                                    Id IN :docIdList
                            ];
            if( (null != listDocuments) && (0 < listDocuments.size()) ) {
                delete listDocuments;
            }
        }
    }

    /*Invoked when Upload Button in VF page is clicked and the IDs are stored in the docIdList*/
    /*All the Files Uploaded are stored in the Documents. Documents does not require parent Id.This method is used to circumvent to upload
      the documents first and then add as Attachment to the Cases (Related List)*/

    public void uploadAttachment(){
        pageError = false;
        pageErrorMessage = '';
    
        if(ApexPages.hasMessages()) {
            pageErrorMessage = '';
            for(ApexPages.Message message : ApexPages.getMessages()) {
                pageErrorMessage += message.getSummary() + '\n';
            }            
            doc = new Document();
            pageError = true;
            return;
        }                                
        
        System.debug('@@@@..1..' + doc.name);
        doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
        if(doc.body != null) {
            insert doc;
            docIdList.add(doc.id);
            doc = [SELECT id, name, bodylength FROM Document WHERE id = :doc.id];

            String size = '';
            if(1048576 < doc.BodyLength) {
                // Size greater than 1MB
                size = '' + (doc.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < doc.BodyLength) {
                // Size greater than 1KB
                size = '' + (doc.BodyLength / 1024) + ' KB';            
            }
            else {
                size = '' + doc.BodyLength + ' bytes';
            }
            UploadedDocumentList.add(new AttachmentsWrapper(true, doc.name, doc.id, size));
            doc = new Document();
        }
    }       
    
    //Documents Wrapper
    public class AttachmentsWrapper {
        public Boolean isSelected {get;set;}
        public String docName {get;set;}
        public String documentId {get;set;}
        public String size {get; set;}
        
        public AttachmentsWrapper(Boolean selected, String Name, String Id, String size){
            isSelected = selected;
            docName = Name ;
            documentId = Id;
            this.size = size;
        }        
    }  
    
    // ELE Compensation form question 1
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Components in my compensation structure ','Components in my compensation structure ')); 
        options.add(new SelectOption('The variable bonus program and/or how my variable bonus has been computed','The variable bonus program and/or how my variable bonus has been computed'));
        options.add(new SelectOption('Both of the above','Both of the above'));
        options.add(new SelectOption('Thank you, I don’t have any questions','Thank you, I don’t have any questions')); 
        options.add(new SelectOption('Any other, please provide details here','Any other, please provide details here')); return options; 
    }
    
    // ELE Compensation form question 2A
    public List<SelectOption> getItems2A() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Yes','Yes')); 
        options.add(new SelectOption('No','No')); return options; 
    }  
}