Public class UserResponseextn {
public Event_Registration_Attendance__c eventReg{get;set;}
    public Event__c recevent{get;set;}
    public string upres{get;set;}
    public id contactid{get;set;}
    public Boolean thankyou{get;set;}
    public Boolean welcomep{get;set;}
    public Boolean thankyoup{get;set;}
    public Venue__c venue{get;set;}
    private Contact con;
    
      public UserResponseextn(ApexPages.StandardController stdController) {
      welcomep=true;
      thankyoup=false;
      thankyou=false;
      eventReg = new Event_Registration_Attendance__c();
      eventReg= [SELECT id,Name,UserResponse__c,Contact__c,Event__c,Attending__c,Not_Attending__c,invited__C,No_Show__c FROM Event_Registration_Attendance__c where id =:ApexPages.currentPage().getParameters().get('id')];
      recevent=[Select id,Campus_Contact__c,Location__c,Start_Date_Time__c,End_Date_Time__c,Venue_Name_in_text__c,Venue_Name__c,Event_Type__c,End_Date__c,Start_Date__c,Invite_Type__c,Name,Year_FY__c,Time__c,Team__c,System_ID__c,Status__c,Season__c,Room__c,Event_ID__c,Recruiter_Lead__c,Program__c,Practitioner_Event_Lead__c,Post_Event_Notes__c,Description__c,Venue_City__c,Venue_Street_Address__c  from Event__c where id =:eventReg.Event__c limit 1];

      contactid=eventReg.Contact__c;
      
        }
   public pagereference save()
   {
   welcomep=false;
   upres=eventReg.UserResponse__c;
   if(upres=='Attending')
   {
   eventReg.Attending__c=True;
   eventReg.UserResponse__c='Attending';
   eventReg.Not_Attending__c=False;
   welcomep=false;
   }
   else
   {
   eventReg.Not_Attending__c=True;
   eventReg.Attending__c=False;
   eventReg.UserResponse__c='Not Attending';
   welcomep=false;
   }
   
   try
   {
   update eventReg;
   welcomep=false;
   }
   catch(Exception e)
   {
   ApexPages.addMessages(e);
    return null;
   }
   //Redirect to the campaign detail page.
       if(upres=='Attending') 
      {
     thankyou=true;
     thankyoup=false;
      }
    else 
      { 
      thankyoup=true;
      thankyou=false;
      }
      return null;
   }
   public pagereference Cancel()
   {
   Pagereference p= new Pagereference('http://talent.force.com/event/EventRegistrationForContact?ID='+contactid);
      p.setRedirect(true);
      return p;
   }
     

   }