global class WCT_Accept_OR_Decline_Event{

    webservice static string sendInvite(string eventId){
        String result,evDescription,evEndDateTimeStr,evLocation,evStartDateTimeStr,emailSubject,subject;
        string evId = eventId;
        //String expTemplateName1 = 'WCT_Send_Invite_to_Employees';
        //EmailTemplate expTemplate1 = [select Id from EmailTemplate where DeveloperName = :expTemplateName1];
        DateTime evStartDateTime;
        List<EventRelation> InterviewersInEvent = new List<EventRelation>();
        Map<string,string> inviteeEmailMap = new Map<string,string>();
        InterviewersInEvent = [SELECT AccountId,EventId,IsInvitee,RelationId,relation.email,relation.name,Status, Event.Description, Event.EndDateTime, 
                                    Event.Location,  Event.StartDateTime, Event.What.Name, Event.IsDateEdited__c, Event.Subject,Event.WCT_Event_type__c
                                    FROM EventRelation WHERE EventId = :evId and IsInvitee=true and Relation.type = 'Contact' and status = 'New'];
         List<id> inviteeid = new List<id>();
                                
                                    
        if(!InterviewersInEvent.isEmpty()){
                result = 'true';
                if(InterviewersInEvent[0].Event.Description!=null)evDescription = InterviewersInEvent[0].Event.Description; 
                if(InterviewersInEvent[0].Event.EndDateTime!=null)evEndDateTimeStr = DateFormatConv(InterviewersInEvent[0].Event.EndDateTime); 
                if(InterviewersInEvent[0].Event.Location!=null)evLocation = InterviewersInEvent[0].Event.Location; 
                if(InterviewersInEvent[0].Event.StartDateTime!=null)evStartDateTimeStr = DateFormatConv(InterviewersInEvent[0].Event.StartDateTime); 
                if(InterviewersInEvent[0].Event.StartDateTime!=null)evStartDateTime = InterviewersInEvent[0].Event.StartDateTime;
                if(InterviewersInEvent[0].Event.subject!=null)emailSubject = InterviewersInEvent[0].Event.Subject;
                subject = emailSubject +'[Id:'+InterviewersInEvent[0].Event.WCT_Event_type__c+']';
                
        }else{result = 'false';}
        
                for(EventRelation er : InterviewersInEvent){
                    //inviteeEmailMap.put(er.relation.email,er.relation.Name);
                    inviteeid.add(er.relationId);
                    
                }
                for(Contact con : [Select Id, Email, Name from Contact where Id IN : inviteeid])
                {
                inviteeEmailMap.put(con.email,con.Name);
                
                }
                
                List<string> fromAdd = new List<string>();
                 fromAdd.AddAll(inviteeEmailMap.keySet());
                Messaging.SingleEmailMessage msg = new  Messaging.SingleEmailMessage();  
                //msg.setReplyTo('accept_or_decline@b4r033u5l68kknv1l6hmoataynao3rbtwit2ft3gtgb0vl6rr.f-2mgrjeao.cs16.apex.sandbox.salesforce.com');
                msg.setReplyTo(label.AcceptEvent);
                msg.setSenderDisplayName('TOD');
                msg.setToAddresses(fromAdd);
                msg.setCcAddresses(new String[] {userinfo.getUserEmail()});// it is optional, only if required
                msg.setSubject(subject);
                string strEmailBody='';
                
                strEmailBody = ' <!DOCTYPE html>';
                strEmailBody = strEmailBody +  ' <html>';
                strEmailBody = strEmailBody +  ' <head>';
             //   strEmailBody = strEmailBody +  ' <center>';
                strEmailBody = strEmailBody +  ' <style>';
              //  strEmailBody = strEmailBody +  ' table';
              //  strEmailBody = strEmailBody +  ' {';
             //   strEmailBody = strEmailBody +  ' border: none;';
             //   strEmailBody = strEmailBody +  ' width:640px;';
             //   strEmailBody = strEmailBody +  ' border-collapse:collapse;';
             //   strEmailBody = strEmailBody +  ' }';
                strEmailBody = strEmailBody +  ' .center';
                strEmailBody = strEmailBody +  ' {';
                strEmailBody = strEmailBody +  ' margin-left: auto;';
                strEmailBody = strEmailBody +  ' margin-right: auto;';
                strEmailBody = strEmailBody +  ' width: 20%;';
                strEmailBody = strEmailBody +  ' }';
                strEmailBody = strEmailBody +  ' </style>';
                strEmailBody = strEmailBody +  ' <img src="'+Label.Deloitte_Logo+'">';
             //   strEmailBody = strEmailBody +  ' </center>';
                strEmailBody = strEmailBody +  ' </head>';
                strEmailBody = strEmailBody +  ' <body>';
              //  strEmailBody = strEmailBody +  ' <center>';
              //  strEmailBody = strEmailBody +  ' <table>';
                strEmailBody = strEmailBody +  ' <div class="center">';
                strEmailBody = strEmailBody +  '<p style="font-family:arial;font-size:15px;"><br><br> Hello All,<br><br>';
                strEmailBody = strEmailBody +  'As part of your Global Mobility & Immigration process, you are required to complete the briefing session and the Q&A session.<br><br>';
                strEmailBody = strEmailBody +  'This training covers the mandatory actions that may be required for your immigration process and onsite travel. <br><br>';
                strEmailBody = strEmailBody +  'It is important for you to complete this training and confirm your attendance.<br><br>'; 
                strEmailBody = strEmailBody +  '<br>All the best!<br><br>';
                strEmailBody = strEmailBody +  'Global Mobility & Immigration – USI India offices</p>';
                strEmailBody = strEmailBody +  '<br><br><p style="font-family:arial;font-size:13px;"><a href="https://www.deloittenet.com/">DeloitteNet</a> | <a href="http://www.deloitte.com/us/security">Security</a> | <a href="http://www.deloitte.com/us/legal">Legal</a> | <a href="http://www.deloitte.com/us/privacy">Privacy</a></p>';
                strEmailBody = strEmailBody +  '<p style="font-family:arial;font-size:12px;">30 Rockefeller Plaza<br>';
                strEmailBody = strEmailBody +  'New York, NY 10112-0015<br>';
                strEmailBody = strEmailBody +  'United States<br><br>';
                strEmailBody = strEmailBody +  ' <img src="'+Label.Deloitte_Footer_Logo+'"><br><br>';
                strEmailBody = strEmailBody +  'Copyright © 2013 Deloitte Development LLC. All rights reserved.<br>';
                strEmailBody = strEmailBody +  '36 USC 220506<br>';
                strEmailBody = strEmailBody +  'Member of Deloitte Touche Tohmatsu Limited</p>';
                strEmailBody = strEmailBody +  '<a href="http://www.linkedin.com/company/deloitte"><img src="'+Label.LinkedIn_Logo+'"></a> <a href="http://www.facebook.com/YourFutureAtDeloitte"><img src="'+Label.Facebook_Logo+'"></a> <a href="http://www.youtube.com/deloittellp"><img src="'+Label.YouTube_Logo+'"></a> <a href="http://www.deloitte.com/view/en_US/us/press/social-media/index.htm"><img src="'+Label.Twitter_Logo+'"></a> <a href="http://www.deloitte.com/view/en_US/us/press/rss-feeds/index.htm"><img src="'+Label.RSS_Feed_Logo+'"></a>';
                strEmailBody = strEmailBody +  ' </div>';
             //   strEmailBody = strEmailBody +  ' </table>';
             //   strEmailBody = strEmailBody +  ' </center>';
                strEmailBody = strEmailBody +  ' </body>';
                strEmailBody = strEmailBody +  ' </html>'; 
                
              //  String HtmlBody = '<html>'+'<table>'+ '<tr><img width="300" src="https://talent--c.na2.content.force.com/servlet/servlet.ImageServer?id=01540000001BWuB&oid=00D40000000MxFD" height="90"/></tr>'+
               //     '<p style="font-family:arial;font-size:12px;"><td>Dear Employee,'+'<br><br>'+'You are pleased to attend GM&I session.' + '<br><br>'+'Thanks<br>'+'GM&I Team </td></p>'+'</table>'+ '</html>' ;
                msg.setHtmlBody(strEmailBody);
                //msg.setTargetObjectId(con.id);
                //msg.setTemplateId(expTemplate1.Id);
                // Create the attachment
                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                efa.setFileName('Meeting.ics' );
                string BodyFromPage = 'Event Cancelled';
                Blob b = doIcsAttachment(evId , evStartDateTimeStr,evEndDateTimeStr,evLocation, BodyFromPage, 
                                        emailSubject, label.AcceptEvent, 'TOD',
                                        evStartDateTime, evLocation,inviteeEmailMap);
                efa.setBody(b);
                efa.setInLine(true);//New
                efa.setContentType('text/Calendar');
                msg.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
                // Send the email you have created.
                if(!fromAdd.Isempty())
                {
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { msg });
                }
                
                return result;
        
    
    }

    
     /**********************Date to GMT convetion****************/    
    private static String DateFormatConv(DateTime dt){
        String myDate = string.valueOfGMT(dt);
        String stringDate = myDate.substring(0,4) + '' +
                            myDate.substring(5,7) + '' + 
                            myDate.substring(8,10) + '' +
                            myDate.substring(10,10) + 'T' +
                            myDate.substring(11,13) + '' +
                            myDate.substring(14,16)+ ''+ 
                            myDate.substring(17,19)+ 'Z';
       return stringDate;
    }
    
    private static Blob doIcsAttachment(String EventId, String startDate, String endDate, String location, String body, 
                                        String subject, String fromAddress, String displayName, 
                                        Datetime evStartDateTime, String evLocation,Map<string,string> inviteeEmails) {
        Map<string,string> inviteeEmailsMap = inviteeEmails;
        string Attendees = '';
        system.debug('@@@'+EventId);
        for(String str : inviteeEmailsMap.keyset()){
            Attendees += 'ATTENDEE;CN="' + inviteeEmailsMap.get(str)+'";ROLE=REQ-PARTICIPANT;RSVP=TRUE:mailto: ' +str+'\n';
        }
        system.debug('%%%1'+Attendees );
       // Attendees = Attendees.substring(0,(Attendees.length()-2));
       // system.debug('%%%'+Attendees );
        if (location== null){location='';}
        String [] icsTemplate = new List<String> {  
            'BEGIN:VCALENDAR',
            'PRODID:-//Microsoft Corporation//Outlook 14.0 MIMEDIR//EN',
            'VERSION:2.0',
            'METHOD:Request',
            'BEGIN:VEVENT',
            'CLASS:PUBLIC',
            'CREATED:20140217T210432Z',
            'DESCRIPTION: '+ body,
            'DTSTART:'+startDate,
            'DTSTAMP:'+startDate,
            'DTEND:'+endDate,
            'LAST-MODIFIED:20140217T210432Z',
            'LOCATION:'+ location,
            //'PRIORITY:5',
            //'SEQUENCE:0',
            'SUMMARY;LANGUAGE=en-us:'+ subject,
            'TRANSP:OPAQUE',
            'ORGANIZER;CN=TOD:MAILTO:' + fromAddress,
            Attendees,
            'ATTENDEE;CN="' + userInfo.getName()+ '";ROLE=OPT-PARTICIPANT;RSVP=TRUE:mailto: ' + userinfo.getUserEmail(),
            'UID:'+ EventId,
            'STATUS:CANCELLED',
            'X-ALT-DESC;FMTTYPE=text/html:'+ body,
            'X-MICROSOFT-CDO-BUSYSTATUS:BUSY',
            'X-MICROSOFT-CDO-IMPORTANCE:1',
            'X-MICROSOFT-DISALLOW-COUNTER:FALSE',
            'X-MS-OLK-AUTOFILLLOCATION:FALSE',
            'X-MS-OLK-CONFTYPE:0',
            'BEGIN:VALARM',
            'TRIGGER:-PT30M',
            'ACTION:DISPLAY',
            'DESCRIPTION:Reminder',
            'END:VALARM',
            'END:VEVENT',
            'END:VCALENDAR'
        };
        String attachment = String.join(icsTemplate, '\n');
        system.debug('%%%'+icsTemplate);
        return Blob.valueof(attachment ); 

    }


}