global class WCT_Accept_Or_DeclineEventHandler implements Messaging.InboundEmailHandler{
    //Method to handle incoming emails
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.Inboundenvelope envelope) {
        //Variable Declaration
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        String ACCEPTED_STATUS = 'Accepted';
        String DECLINED_STATUS = 'Declined';
        //This convention is used when sending invite
        String  INTERVIEWNUMBER = 'Id:';
        String EventId ;
        String emailSubject = email.subject.trim();
        String InterviewName ;
        Boolean isAccepted = false;
        Boolean isDeclined = false;
        String fromAddress = email.fromAddress;
        List<EventRelation> liEventRelations= new List<EventRelation>();
        List<EventRelation> liEventRelationsToUpdate= new List<EventRelation>();
        List<WCT_Interview_Junction__c> liInterviewJunction = new List<WCT_Interview_Junction__c>();
        List<Contact> contactList = new List<Contact>();
        Set<Id> conIdSet = new Set<Id>();
        //Get All contacts based on from Address of email , since 
        try{
            contactList = [SELECT id,email from Contact where email=:fromAddress];
        }Catch(QueryException queryException){ 
            WCT_ExceptionUtility.logException('Email Service Handler','InboundEventEmailHandler',queryException.getMessage());
            result.success = false;
            result.message = queryException.getMessage(); 
        }
        if(!contactList.IsEmpty()){
            for(Contact c: contactList){
                conIdSet.add(c.id);
            }
        }

        //Processing email subject appropriately as required
        List<String> stringSubjectList=new List<String>();
        if(emailSubject != null){
            if(emailSubject.startsWith(ACCEPTED_STATUS)){
                isAccepted = true;
            }else if(emailSubject.startsWith(DECLINED_STATUS)){
                isDeclined = true;
            }
            if(emailSubject.Contains(INTERVIEWNUMBER)){
                emailSubject = emailSubject.replace(']','');
                emailSubject = emailSubject.replace('[','');
                stringSubjectList = emailSubject.split(':');            
            }
            for(String s : stringSubjectList){
                s=s.trim();
                if(s.startsWith('EVE -')){
                    InterviewName = s;
                    //break;
                }
            }
        }

        

        try{
            liEventRelations = [SELECT EventId, RelationId, Event.WhatId,Event.What.Name,Status FROM EventRelation where IsInvitee = true AND Event.WCT_Event_type__c = :InterviewName];
        }Catch(QueryException queryException){  
            WCT_ExceptionUtility.logException('Email Service Handler','InboundEventEmailHandler',queryException.getMessage());
            result.success = false;
            result.message = queryException.getMessage();
        }  

        for(EventRelation er:liEventRelations ){

            if(conIdSet.Contains(er.RelationId)){
                if(isAccepted){
                    er.status = ACCEPTED_STATUS;
                    liEventRelationsToUpdate.add(er); 
                }else if(isDeclined){
                    er.status = DECLINED_STATUS;
                    liEventRelationsToUpdate.add(er); 
                }
            }
        }
        try{
            update liEventRelationsToUpdate;
            
            result.success = true;
            /*result.message = 'updated Event Relations'+'\n';
        for(EventRelation e:liEventRelationsToUpdate){
            result.message += e.Id + 'with'+e.status +'\n';

        }*/

        }Catch(DmlException dmlException){  
            WCT_ExceptionUtility.logException('Email Service Handler','InboundEventEmailHandler',dmlException.getMessage());
            result.success = false;
            result.message = dmlException.getMessage();
        } 
        return result;


    }
    //Method to send email if event has been declined
    
}