public class WCT_Add_Update_DepVisaInfoController extends SitesTodHeaderController{

    /* public variables */
    public WCT_Immigration__c ImmigrationRecord{get;set;}
    public Contact DependentRecord{get;set;}
    public List<Contact> ExistingDepList{get;set;}
    public Boolean isNotClose{get;set;}
    public List<ContactWrapper> showAllContactWrapper{get;set;}


    /* Upload Related Variables */
    public String employeeEncryptId{get;set;}
    public String employeeEmail{get;set;}
    public String employeeId{get;set;}
    public String employeeLastName{get;set;}
    public String employeeFirstName{get;set;}
    public String employeeRelationship{get;set;}
    public String dependentGender{get;set;}
    public String UIDNumber{get;set;}
    public String preferredLocation{get;set;}
    public String visaStatus{get;set;}
    public List<SelectOption> visaStatusOptions{set; get;}
    public List<SelectOption> dependentGenderOptions{set; get;}
    public List<SelectOption> employeeRelationshipOptions{set; get;}
    public String visaStartDate{get;set;}
    public String visaEndDate{get;set;}
    public String exVisaStartDate{get;set;}
    public String exVisaEndDate{get;set;}

    public Document doc {get;set;}
    public List<String> docIdList = new List<string>();
    public List<AttachmentsWrapper> UploadedDocumentList {get; set; }
    public List<Attachment> listAttachments {get; set;}
    public Map<Id, String> mapAttachmentSize {get; set;}  
    public Integer countattach{get;set;} 

    // Error Message related variables
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}   


    public WCT_Add_Update_DepVisaInfoController()
    {  
        
        /*Initialize all Variables*/
        init();
        
        /*Get Employee Encrypted Email from Parameter*/
        getParameterInfo();  
        
        /*Get Attachment List and Size*/
        getAttachmentInfo();
        
        /*Query to get Immigration Record*/  
        getImmigrationDetails();
        
        /*Query to get the existing Records*/
        getExistingDepInfo();
        
        visaStatusOptions=new List<SelectOption>();
        visaStatusOptions.add(new SelectOption('Applying for a new Visa','Applying for a new Visa'));
        visaStatusOptions.add(new SelectOption('Have an existing Visa','Have an existing Visa'));
        visaStatus='Applying for a new Visa';
        
        dependentGenderOptions=new List<SelectOption>();
        dependentGenderOptions.add(new SelectOption('Male','Male'));
        dependentGenderOptions.add(new SelectOption('Female','Female'));
        dependentGender = 'Male';
        
        employeeRelationshipOptions=new List<SelectOption>();
        employeeRelationshipOptions.add(new SelectOption('Husband/Wife','Husband/Wife'));
        employeeRelationshipOptions.add(new SelectOption('Son/Daughter','Son/Daughter'));
        employeeRelationship = 'Husband/Wife';
        
        isNotClose = true;
           
    }

    private void init(){

        //Custom Object Instances
        ImmigrationRecord = new WCT_Immigration__c (); 
        DependentRecord = new Contact();
        countattach = 0;
       
        //Document Related Init
        doc = new Document();
        UploadedDocumentList = new List<AttachmentsWrapper>();
        listAttachments = new List<Attachment>();
        mapAttachmentSize = new Map<Id, String>(); 
    }  
    
    public class ContactWrapper{
        public Contact contactRecord{get;set;}
        public String newStartDate{get;set;}
        public String newEndDate{get;set;}
        
        public ContactWrapper(Contact conRec, String newSDate, String newEDate){
            contactRecord = conRec;
            newStartDate = newSDate;
            newEndDate = newEDate;            
        }
    }  
    
    private void getParameterInfo(){
        employeeEncryptId = ApexPages.currentPage().getParameters().get('em');
        employeeEmail = CryptoHelper.decrypt(employeeEncryptId);
        employeeId = [SELECT id, Name, RecordTypeId, Email FROM Contact WHERE RecordType.Name = 'Employee' AND Email =:employeeEmail].Id;
    }

    private void getAttachmentInfo(){
        listAttachments = [SELECT ParentId, 
                                    Name, 
                                    BodyLength, 
                                    Id,
                                    CreatedDate
                                    FROM  Attachment 
                                    WHERE ParentId =: DependentRecord.Id
                                    ORDER BY CreatedDate DESC
                                    LIMIT 50 ];    
              
        for(Attachment a : listAttachments) {
            String size = null;

            if(1048576 < a.BodyLength){
                // Size greater than 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength){
                // Size greater than 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else{
                size = '' + a.BodyLength + ' bytes';
            }
            mapAttachmentSize.put(a.id, size);
        }
    }
   

    public void getImmigrationDetails()
    {
        List<String> activeImmigration = new List<String>();
        activeImmigration.add('Visa Approved');
        activeImmigration.add('Visa Stamped');
        activeImmigration.add('Petition Shipped/Sent');
        activeImmigration.add('Petition Received');
        activeImmigration.add('Petition Collected');
        activeImmigration.add('Visa Appointment Scheduled');
        activeImmigration.add('Yet to schedule visa appointment');
        activeImmigration.add('Yet to receive Visa Logistics Information from Practitioner');
        activeImmigration.add('Drop Box');
        activeImmigration.add('Petition Approved');
    
        ImmigrationRecord = [SELECT Id,
                                    Name,
                                    WCT_Assignment_Owner__c,
                                    WCT_Immigration_Status__c,
                                    WCT_Visa_Type__c,
                                    RecordType.Name, 
                                    ownerId
                                    FROM WCT_Immigration__c 
                                    where WCT_Assignment_Owner__c =:employeeId AND WCT_Immigration_Status__c IN : activeImmigration AND RecordType.Name != 'B1 Visa' AND RecordType.Name != 'L2 with EAD'];
    }
    
    public void getExistingDepInfo()
    {
        ExistingDepList = [SELECT Id, Name, WCT_Visa_Start_Date__c, WCT_Visa_End_Date__c, WCT_GMI_Record__c, WCT_Dependent_Visa_Type__c, WCT_UID_Number__c, WCT_Visa_Status__c, WCT_Preferred_Location__c FROM Contact WHERE RecordType.Name = 'Dependant' AND WCT_GMI_Record__c = true AND WCT_Primary_Contact__c =:employeeId]; 
        showAllContactWrapper = new List<ContactWrapper>();
        for(Contact con : ExistingDepList){
            showAllContactWrapper.add(new ContactWrapper(con, '', ''));
        }
    }


    public pageReference save()
    { 
    /*Custom Validation Rules are handled here*/

        Id depRecordTyepId = [SELECT Id, Name FROM RecordType WHERE Name = 'Dependant'].Id;
        
        if((null == employeeFirstName) ||
            ('' == employeeFirstName) ||
            ('' == employeeLastName) ||
            (null == employeeLastName) ||
            ('' == dependentGender) ||
            (null == dependentGender) ||
            ('' == employeeRelationship) ||
            (null == employeeRelationship))
        {
            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
        }

        
        if(visaStatus == 'Applying for a new Visa' && 
            (preferredLocation == null || 
             preferredLocation == '' ||
             '' == UIDNumber ||
            null == UIDNumber))
        {
            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
        }
        else if(visaStatus == 'Have an existing Visa' && (visaStartDate == null || visaStartDate == '' || visaEndDate == null || visaEndDate == ''))
        {
            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
        }
           
        if(visaStatus == 'Applying for a new Visa' && (UIDNumber.length() != 8 || (!UIDNumber.isNumeric())))
        {
            pageErrorMessage = 'Please enter a 8 digit UID Number.';
            pageError = true;
            return null;
        }  
        
        if(visaStatus == 'Have an existing Visa' && (visaStartDate != null && visaStartDate != '')){
            DependentRecord.WCT_Visa_Start_Date__c = Date.parse(visaStartDate);
        }  
        if(visaStatus == 'Have an existing Visa' && (visaEndDate != null && visaEndDate != '')){
            DependentRecord.WCT_Visa_End_Date__c = Date.parse(visaEndDate);
        } 
        
        if(visaStatus == 'Applying for a new Visa' && (preferredLocation != null && preferredLocation != '')){
            DependentRecord.WCT_Preferred_Location__c = preferredLocation;
        } 
        
        /*insert DependentRecord Record*/
        if(visaStatus == 'Have an existing Visa')
            DependentRecord.WCT_Visa_Status__c = 'Visa Approved';
        else if(visaStatus == 'Applying for a new Visa')
            DependentRecord.WCT_Visa_Status__c = 'Applied for a Visa';
        
        DependentRecord.LastName = employeeLastName;
        DependentRecord.FirstName = employeeFirstName;
        DependentRecord.RecordTypeId = depRecordTyepId;
        DependentRecord.WCT_Relationship__c = employeeRelationship;
        DependentRecord.WCT_Gender__c = dependentGender;
        DependentRecord.WCT_UID_Number__c = UIDNumber;
        
        DependentRecord.WCT_Related_Immigration__c = ImmigrationRecord.Id;
        DependentRecord.WCT_Primary_Contact__c = employeeId;
        DependentRecord.OwnerId = System.Label.GMI_User;
        DependentRecord.WCT_GMI_Record__c = true;
        upsert DependentRecord;    
        
        update ExistingDepList; 
        getExistingDepInfo();
                   
        pageError = false;
        
        tempRefresh();
        
        //PageReference retPage = Page.WCT_Add_Update_DependentVisaInfo;                 
        //retPage.setRedirect(true);
        //retPage.getParameters().put('em',employeeEncryptId);
        //return retPage;
        
        return null;
       
        //return new PageReference('/apex/WCT_Add_Update_DependentVisaInfo?em='+CryptoHelper.encrypt(LoggedInContact.Email)) ;  
    
    }
    
    public pageReference tempRefresh(){
        
        employeeLastName = '';
        employeeFirstName = '';
        UIDNumber = '';
        preferredLocation = '';
        visaStartDate = '';
        visaEndDate = '';
        
        PageReference retPage = Page.WCT_Add_Update_DependentVisaInfo;                 
        retPage.setRedirect(true);
        retPage.getParameters().put('em',employeeEncryptId);
        return retPage;
    }
    
    public pageReference updateonly(){
        
        List<Contact> newConList = new List<Contact>();
        
        for(ContactWrapper wrap : showAllContactWrapper){
            if((wrap.newStartDate != null && wrap.newStartDate != '') || (wrap.newEndDate != null && wrap.newEndDate != '')){
                if(wrap.newStartDate != null && wrap.newStartDate != ''){
                  //  wrap.newStartDate = exVisaStartDate;
                  //  wrap.newEndDate = exVisaEndDate;
                
                    wrap.contactRecord.WCT_Visa_Start_Date__c = Date.parse(wrap.newStartDate);
                  //  wrap.contactRecord.WCT_Visa_End_Date__c = Date.parse(wrap.newEndDate);
                    
                }
                if(wrap.newEndDate != null && wrap.newEndDate != ''){
                  //  wrap.newStartDate = exVisaStartDate;
                  //  wrap.newEndDate = exVisaEndDate;
                
                  //  wrap.contactRecord.WCT_Visa_Start_Date__c = Date.parse(wrap.newStartDate);
                    wrap.contactRecord.WCT_Visa_End_Date__c = Date.parse(wrap.newEndDate);
                   // newConList.add(wrap.contactRecord);
                }
                newConList.add(wrap.contactRecord);
            }
        }
        try{
            Database.update(newConList);
                
            Database.update(ExistingDepList);
        }
        catch(exception ex){
            pageErrorMessage = 'Update did not happen.' + ex.getmessage();
            pageError = true;
            return null;
        }
            
        pageError = false;
        
        getExistingDepInfo();
        
        //PageReference retPage = Page.WCT_Add_Update_DependentVisaInfo;                 
        //retPage.setRedirect(true);
        //retPage.getParameters().put('em',employeeEncryptId);
        //return retPage;
        return null;
    
    }
    
    public pageReference done()
    { 
        ImmigrationRecord.WCT_Dependent_Form_Submitted__c = true;
        update ImmigrationRecord;
        
        /* Upload Documents as Related Attachments to Case After the Case Record is created */
        uploadRelatedAttachment();  
        
        //checking for no attachment conditions
        if(countattach<=0)
        {
            pageErrorMessage = 'Attachment is required to submit the form.';
            pageError = true;
            return null;
        }
        
        getAttachmentInfo(); 
        pageError = false; 
          
 
        PageReference retPage = Page.WCT_Add_Update_DependentVisaInfoThankYou;                 
        retPage.setRedirect(true);
        retPage.getParameters().put('em',employeeEncryptId);
        return retPage; 
    }

    private void uploadRelatedAttachment(){

        if(!docIdList.isEmpty()) { //If there is any Documents Attached in Web Form

            List<String> selectedDocumentId = new List<String>();
            for(AttachmentsWrapper aWrapper : UploadedDocumentList) {
                if(aWrapper.isSelected) {
                    selectedDocumentId.add(aWrapper.documentId);
                }
            }
                
            /* Select Documents which are Only Active (Selected) */
            List<Document> selectedDocumentList = new List<Document>();
            if(!selectedDocumentId.isEmpty()){
                selectedDocumentList = [SELECT id,
                                               name,
                                               ContentType,
                                               Type,
                                               Body 
                                               FROM 
                                               Document 
                                               WHERE 
                                               id IN :selectedDocumentId];
            }
                
            /* Convert Documents to Attachment */
            List<Attachment> attachmentsToInsertList = new List<Attachment>();
            for(Document doc : selectedDocumentList){
                Attachment a = new Attachment();
                a.body = doc.body;
                a.ContentType = doc.ContentType;
                a.Name= doc.Name;
                a.Description=String.valueof(ImmigrationRecord.Id);
                a.parentid = ImmigrationRecord.Id; //Immigration Record Id which is been retrived.
                attachmentsToInsertList.add(a);                
            }
            countattach=attachmentsToInsertList.size();
            if(!attachmentsToInsertList.isEmpty()){
                insert attachmentsToInsertList;
            }
                
            List<Document> listDocuments = new List<Document>();
            listDocuments = [SELECT Id
                                    FROM
                                    Document
                                    WHERE
                                    Id IN :docIdList];
            if( (null != listDocuments) && (0 < listDocuments.size()) ) {
                delete listDocuments;
            }
        }
    }

    /*Invoked when Upload Button in VF page is clicked and the IDs are stored in the docIdList*/
    /*All the Files Uploaded are stored in the Documents. Documents does not require parent Id.This method is used to circumvent to upload
      the documents first and then add as Attachment to the Cases (Related List)*/

    public void uploadAttachment(){
        pageError = false;
        pageErrorMessage = '';
    
        if(ApexPages.hasMessages()) {
            pageErrorMessage = '';
            for(ApexPages.Message message : ApexPages.getMessages()) {
                pageErrorMessage += message.getSummary() + '\n';
            }            
            doc = new Document();
            pageError = true;
            return;
        }                                
        
        doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
        if(doc.body != null) {
            insert doc;
            docIdList.add(doc.id);
            doc = [SELECT id, name, bodylength FROM Document WHERE id = :doc.id];

            String size = '';
            if(1048576 < doc.BodyLength) {
                // Size greater than 1MB
                size = '' + (doc.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < doc.BodyLength) {
                // Size greater than 1KB
                size = '' + (doc.BodyLength / 1024) + ' KB';            
            }
            else {
                size = '' + doc.BodyLength + ' bytes';
            }
            UploadedDocumentList.add(new AttachmentsWrapper(true, doc.name, doc.id, size));
            doc = new Document();
        }
    }       
    

    //Documents Wrapper
    public class AttachmentsWrapper {
        public Boolean isSelected {get;set;}
        public String docName {get;set;}
        public String documentId {get;set;}
        public String size {get; set;}
            
        public AttachmentsWrapper(Boolean selected, String Name, String Id, String size){
            isSelected = selected;
            docName = Name ;
            documentId = Id;
            this.size = size;
        }        
    }
 
}