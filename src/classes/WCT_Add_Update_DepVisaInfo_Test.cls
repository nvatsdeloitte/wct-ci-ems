@isTest
private class WCT_Add_Update_DepVisaInfo_Test {

    static testMethod void myUnitTest() {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Id mobId=[select id, Name from recordtype where sObjectType = 'WCT_Mobility__c' AND Name='Employment Visa'].Id;
        Id immId=[select id, Name from recordtype where sObjectType = 'WCT_Immigration__c' AND Name='L1 Visa'].Id;
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
        WCT_Immigration__c immi=WCT_UtilTestDataCreation.createImmigration(con.id);
        immi.WCT_Immigration_Status__c='Visa Approved';
        immi.RecordTypeId = immId;
        immi.WCT_Visa_Type__c='L1B Individual';
        insert immi;
        
        WCT_Mobility__c mob= WCT_UtilTestDataCreation.createMobility(con.id);
        mob.WCT_Mobility_Status__c = 'New';
        mob.RecordTypeId = mobId;
        mob.Immigration__c=immi.Id;
        insert mob;
        
        Contact dep= new Contact();
        dep.WCT_Primary_Contact__c= con.id;
        dep.FirstName= 'Test 1';
        dep.LastName= 'Test 1';
        dep.WCT_UID_Number__c='T1234567';
        insert dep;
        
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');

        PageReference pageRef = Page.WCT_Add_Update_DependentVisaInfo;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);     
        ApexPages.currentPage().getParameters().put('type','Employee');
       
        datetime startdate=date.Today().adddays(10);
        datetime enddate=date.Today().adddays(20);
        Test.starttest();
        WCT_Add_Update_DepVisaInfoController controller=new WCT_Add_Update_DepVisaInfoController();
        
        controller=new WCT_Add_Update_DepVisaInfoController();
      
        controller.doc=WCT_UtilTestDataCreation.createDocument();
        controller.uploadAttachment();
        controller.visaStatus='Have an existing Visa';
        controller.visaStartDate=startdate.format('MM/dd/yyyy');
        controller.visaEndDate=enddate.format('MM/dd/yyyy');
        controller.save();
        controller.exVisaStartDate=startdate.format('MM/dd/yyyy');
        controller.exVisaEndDate=enddate.format('MM/dd/yyyy');
        controller.updateonly();
        controller.save();
        controller.visaStatus='Applying for a new Visa';
        controller.employeeFirstName='Test';
        controller.employeeLastName='Test';
        controller.employeeRelationship='Husband/Wife';
        controller.dependentGender='Female';
        controller.UIDNumber='12345';
        controller.preferredLocation='Test';
        controller.visaStartDate=startdate.format('MM/dd/yyyy');
        controller.visaEndDate=enddate.format('MM/dd/yyyy');
        controller.save();
        controller.tempRefresh();
        controller.visaStatus='Applying for a new Visa';
        controller.employeeFirstName='Test';
        controller.employeeLastName='Test';
        controller.employeeRelationship='Husband/Wife';
        controller.dependentGender='Female';
        controller.visaStartDate=startdate.format('MM/dd/yyyy');
        controller.visaEndDate=enddate.format('MM/dd/yyyy');
        controller.save();
        controller.visaStatus='Applying for a new Visa';
        controller.employeeFirstName='Test';
        controller.employeeLastName='Test';
        controller.UIDNumber='12345678';
        controller.employeeRelationship='Husband/Wife';
        controller.dependentGender='Female';
        controller.preferredLocation='Test';
        controller.visaStartDate=startdate.format('MM/dd/yyyy');
        controller.visaEndDate=enddate.format('MM/dd/yyyy');
        controller.save();
        controller.visaStatus='Have an existing Visa';
        controller.visaStartDate=startdate.format('MM/dd/yyyy');
        controller.save();
        controller.updateonly();
        controller.visaStatus='Have an existing Visa';
        controller.visaStartDate=startdate.format('MM/dd/yyyy');
        controller.visaEndDate=enddate.format('MM/dd/yyyy');
        controller.save();
        controller.exVisaStartDate=startdate.format('MM/dd/yyyy');
        controller.exVisaEndDate=enddate.format('MM/dd/yyyy');
        controller.updateonly();
        controller.done(); 
                
        
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        controller.uploadAttachment();
        controller.employeeEmail='testingmobility@test.com';
        
        controller=new WCT_Add_Update_DepVisaInfoController();
        Test.stoptest();
    }
    
     }