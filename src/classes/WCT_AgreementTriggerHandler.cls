/**************************************************************************************
Apex Trigger Name:  WCT_AgreementTriggerHandler
Version          : 1.0 
Created Date     : 25 March 2013
Function         : Handler class to  
                    -> To update Offer status field based on Agreement status field
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                   25/03/2013             Original Version
*************************************************************************************/

public class WCT_AgreementTriggerHandler{
    private static boolean run = true;
    public static boolean inFutureContext = true;
     /** 
        Method Name  : UpdateOfferStatus
        Return Type  : Void
        Description  : Update the Offer status based on Agreement status change       
     */
     
     public void OfferStatusChangeOnUpdate(List<echosign_dev1__SIGN_Agreement__c> agreementList, Map<Id , echosign_dev1__SIGN_Agreement__c> agreementoldMap){
         if(run){
             run = false;
             List<WCT_Offer__c> offerList = New List<WCT_Offer__c>();
             /* Mobility List to update with latest status */
             List<WCT_Mobility__c> mobilityList = New List<WCT_Mobility__c>();
             List<Id> signedEmployees= new List<Id>();
             system.debug('##########.....');
             List<WCT_Offer_Note__c> offernoteList = New List<WCT_Offer_Note__c>();
             
             Set<Id> offerIds = new Set<Id>();
             List<WCT_Offer__c> offerNameList = new List<WCT_Offer__c>();
             Map<Id, String> offerOldStatusMap = new Map<Id, String>();
             
             List<ELE_Separation__c> separationList =  new List<ELE_Separation__c>(); // code added for separation
             
             for(echosign_dev1__SIGN_Agreement__c agreement : agreementList){
                 if(agreement.WCT_offer__c != null){
                     offerIds.add(agreement.WCT_offer__c);
                 }
             }
             if(!offerIds.isEmpty()) {
                 offerNameList = [SELECT Id, WCT_Status__c FROM WCT_Offer__c WHERE Id IN: offerIds];
                 for(WCT_Offer__c off: offerNameList) {
                     offerOldStatusMap.put(off.Id, off.WCT_Status__c);
                 }
             }
             
             for(echosign_dev1__SIGN_Agreement__c agreement : agreementList){
                 echosign_dev1__SIGN_Agreement__c agreementOld = agreementoldMap.get(agreement.Id);
                 if(agreement.WCT_offer__c != Null){
                     WCT_Offer__c Offer = new WCT_Offer__c();
                     offer.id = agreement.WCT_offer__c;
                     offer.WCT_isOfferSentByBatch__c = agreement.WCT_isCreatedByBatch__c;
                     if(agreement.echosign_dev1__Status__c == 'Out for Signature'){   
                         offer.WCT_status__c='Offer Sent';
                         
                         /***
                             This below code was added to update appropriate fields if Offer is sent through batch
                         **/
                         if(offer.WCT_isOfferSentByBatch__c == true && offerOldStatusMap.get(offer.id) != offer.WCT_status__c) {
                             offer.WCT_Offer_Sent_Date__c = System.now(); 
                             offer.WCT_Offer_Letter_Sent__c = true;      
                             offer.WCT_Offer_Password_Sent__c = true;
                             offer.WCT_Offer_Sent_Month_Txt__c = agreement.WCT_Month__c;
                             offer.WCT_Offer_Sent_Date_Txt__c = agreement.WCT_Day__c;                             
                         }
                     }
                     if(agreement.echosign_dev1__Status__c == 'Signed'){   
                         offer.WCT_status__c='Offer Accepted';
                         system.debug('2*****'+offer.WCT_status__c);
                     }

                      if(agreement.echosign_dev1__Status__c == 'Cancelled / Declined' && agreement.WCT_Declined_Reason__c != Null && agreement.WCT_Declined_Reason__c != '' && agreementOld.WCT_Declined_Reason__c == Null){  
                         offer.Automated_Status_Change__c = True;
                         if(agreement.WCT_Declined_Reason__c.contains(agreement.name+' '+label.WCT_OfferCancelledBy)) {
                             offer.WCT_status__c='Offer Cancelled';
                             offer.WCT_Change_record_type__c = True;
                         }
                         else{
                             offer.WCT_status__c='Offer Declined';
                         }
                     }
                     
                     if(agreement.echosign_dev1__Status__c == 'Cancelled / Declined' && agreement.WCT_Declined_Reason__c != Null && agreement.WCT_Declined_Reason__c != '' && agreementOld.WCT_Declined_Reason__c == Null){
                        WCT_Offer_Note__c declinereasonNote = new WCT_Offer_Note__c();
                        declinereasonNote.WCT_Offer__c = agreement.WCT_offer__c;
                        if(agreement.WCT_Declined_Reason__c.contains('cancelled')) {
                            declinereasonNote.WCT_Offer_Status__c = 'Offer Cancelled';
                        } else {
                            declinereasonNote.WCT_Offer_Status__c = 'Offer Declined';
                        }
                        declinereasonNote.WCT_Description__c = agreement.WCT_Declined_Reason__c;
                        offernoteList.add(declinereasonNote);
                     }
                     offerList.add(Offer);
                 }
                 
                 
                 /*Determining the Deputation Letter Status in Mobility based on the agreement status*/
                if(agreement.WCT_Mobility__c != Null){
                     WCT_Mobility__c mobility = new WCT_Mobility__c();
                     mobility.id = agreement.WCT_Mobility__c ;
                     if(agreement.echosign_dev1__Status__c == 'Draft'){   
                         mobility.WCT_Deputation_Letter_Status__c='Not Sent';
                     }
                     if(agreement.echosign_dev1__Status__c == 'Out for Signature'){   
                         mobility.WCT_Deputation_Letter_Status__c='Sent';
                     }
                     if(agreement.echosign_dev1__Status__c == 'Signed'){   
                         mobility.WCT_Deputation_Letter_Status__c='Signed';
                   }
                     if(agreement.echosign_dev1__Status__c == 'Cancelled / Declined'){  
                         //mobility.Automated_Status_Change__c = True ;
                         mobility.WCT_Deputation_Letter_Status__c='Declined';
                     }
                    
                     mobilityList.add(mobility);
                 }

                /* Determining the Service Letter Status in ELE Separation Case based on the agreement status */
                if(agreement.WCT_ELE_Separation__c != Null){
                     ELE_Separation__c separation = new ELE_Separation__c();
                     separation.id = agreement.WCT_ELE_Separation__c;
                     if(agreement.echosign_dev1__Status__c == 'Draft'){   
                         separation.ELE_Separation_Letter_Status__c='Not Sent';
                     } 
                     if(agreement.echosign_dev1__Status__c == 'Out for Signature'){   
                         separation.ELE_Separation_Letter_Status__c='Sent';
                         separation.ELE_Service_Letter_Sent_Separation__c=true;
                         separation.ELE_Service_Letter_Password__c=System.Label.Separation_Letter_Password;
                     } 
                     if(agreement.echosign_dev1__Status__c == 'Signed'){   
                         separation.ELE_Separation_Letter_Status__c='Signed';
                     } 
                     if(agreement.echosign_dev1__Status__c == 'Cancelled / Declined'){  
                         separation.ELE_Separation_Letter_Status__c='Declined';
                     }
                     separationList.add(separation);
                 }                                                   
                 
             }          
             If(offerList.size()>0){
                 UPDATE offerList;
             } 
             
             /*updating Mobility record with new deputation letter status */ 
              If(mobilityList.size()>0){
                 UPDATE mobilityList;
             } 
             If(offernoteList.size()>0){
                 INSERT offernoteList;
             }
             if(separationList.size()>0){
                 UPDATE separationList;
             }                                       
         }
    }
    
    public void SetSendOnBehalfOf(List<echosign_dev1__SIGN_Agreement__c> newAgreementList){
        Set<Id> OfferIdList = new Set<Id>();
        Map<Id,Id> OfferRecordtypeMap = new Map<Id,Id>();
        List<User> UserList= new List<User>();
        List<RecordType> India_Letters= new List<RecordType>();
        
        boolean isFirst=true;
 
        for(echosign_dev1__SIGN_Agreement__c agreement: newAgreementList){
        
            if(agreement.WCT_offer__c!=null)
            {
            
            /*Gets executed only once, the code inside this 'if' will make sure the data is quried only when the agremment associated with offers is been called.*/
                if(isFirst)
                {
                    for(echosign_dev1__SIGN_Agreement__c agreement_temp: newAgreementList){
                       OfferIdList.add(agreement_temp.WCT_offer__c); 
                    }
                    
                    for(WCT_offer__c offer : [Select id, recordtypeid from WCT_offer__c WHERE id in : OfferIdList]){
                        OfferRecordtypeMap.put(offer.id,offer.recordtypeid);
                    }
                    
                    UserList= [Select id from User WHERE (Name = 'Deloitte US Offers') OR (Name = 'Deloitte India Offers') ORDER BY Name];
                    India_Letters = [SELECT Id FROM RecordType WHERE DeveloperName = 'WCT_India_Campus_Hire_Offer' OR DeveloperName = 'WCT_India_Experienced_Hire_Offer'];
                        
                }
                isFirst=false;
            
                   if(India_Letters[0].Id == OfferRecordtypeMap.get(agreement.WCT_offer__c) || India_Letters[1].Id == OfferRecordtypeMap.get(agreement.WCT_offer__c)){
                       agreement.echosign_dev1__Sender_User__c = UserList[0].id;
                   } 
                   else{
                       agreement.echosign_dev1__Sender_User__c = UserList[1].id;
                   }  
            }
            else if(agreement.WCT_mobility__c!=null)
            {
             /*Update the user after the confirmation*/
             
              UserList= [Select id from User WHERE Name = 'US India GMI (US)'];
              if(UserList.size()>0)
                agreement.echosign_dev1__Sender_User__c = UserList[0].id;
             
            } 
            else if(agreement.WCT_ELE_Separation__c!=null)
            {
                  UserList= [Select id from User WHERE Name = 'Talent Administrator'];
                  if(UserList.size()>0)
                        agreement.echosign_dev1__Sender_User__c = UserList[0].id;
            }                                       
       }  
    }
    
    public void SetPDFPassword(List<echosign_dev1__SIGN_Agreement__c> newAgreementList){
        /*Data set*/
        Set<Id> OfferIdList = new Set<Id>();
        Set<Id> OfferIdBatchList = new Set<Id>();
        Set<Id> mobilityIdList = new Set<Id>();
        Map<Id,String> OfferPasswordMap = new Map<Id,String>();
        Map<Id,String> mobilityPasswordMap = new Map<Id,String>();
        Map<Id,WCT_Mobility__c> mobilityContactMap = new Map<Id,WCT_Mobility__c>();
        WCT_Mobility__c[] mobilities=null;
       //list<WCT_Mobility__c> mobilities=new list<WCT_Mobility__c>();
        
        String month = String.valueOf(System.today().month());
        String day = String.valueOf(System.today().day());
        month = (month.length() == 1) ? '0'+month : month;
        day = (day.length() == 1) ? '0'+day : day;
        
        /*Getting all the id of Offer Or Mobility from the Agreements record*/
            for(echosign_dev1__SIGN_Agreement__c agreement: newAgreementList){
                    if(agreement.WCT_offer__c!=null)
                    {
                        if(agreement.WCT_isCreatedByBatch__c == true) {
                            OfferIdBatchList.add(agreement.WCT_offer__c);
                        } else {
                            OfferIdList.add(agreement.WCT_offer__c);
                        }
                    } 
                    else if(agreement.WCT_mobility__c!=null)
                    {
                        mobilityIdList.add(agreement.WCT_mobility__c);
                        /*system.debug('### ');
                        system.debug('### '+agreement.WCT_Mobility__r.WCT_Mobility_Employee__r.LastName);
                        string newPassword=generatePassword(String.valueOf(agreement.WCT_Mobility__c));
                        system.debug('##### '+newPassword);
                        mobilityPasswordMap.put(agreement.WCT_mobility__c,newPassword);*/
                    }
            }
          
          /*Fetching the password from the Offer or Mobility records */
        
          
          if(OfferIdList.size()>0)
          {
              for(WCT_offer__c offer : [Select id, Offer_Password__c from WCT_offer__c WHERE id in : OfferIdList])
              {
                  OfferPasswordMap.put(offer.id,offer.Offer_Password__c);
              }
          }
          
          if(OfferIdBatchList.size()>0)
          {
              for(WCT_offer__c offer : [Select id, WCT_Offer_Letter_Password__c, Offer_Password__c from WCT_offer__c WHERE id in : OfferIdBatchList])
              {                      
                  OfferPasswordMap.put(offer.id,offer.WCT_Offer_Letter_Password__c+month+day);
              }
          }
          
          if(mobilityIdList.size()>0)
          {
              integer i=0;
              for(WCT_mobility__c Mob : [Select id,WCT_Mobility_Employee__c,WCT_Mobility_Employee__r.LastName,WCT_Employee_Email_Address__c,WCT_Mobility_Employee__r.FirstName,WCT_Deputation_Password__c from WCT_Mobility__c WHERE id in : mobilityIdList])
              {
                 
                  /*Generating the new Password and creating the mobility array to update . */
                String newPassword=generatePassword(Mob.WCT_Mobility_Employee__r.LastName);                
                mobilities=mobilities!=null?mobilities:new WCT_Mobility__c[mobilityIdList.size()];
                  WCT_Mobility__c mobility=new WCT_Mobility__c();
                  mobility.id=Mob.id;
                  mobility.WCT_Deputation_Password__c=newPassword;
                  mobilities[i]=mobility;
                                  
                  //mobilityContactEmailMap.put(Mob.id, Mob.WCT_Employee_Email_Address__c);
                /*Setting the new password in the map, which further will be used to set to Agreement. */  
                  mobilityPasswordMap.put(Mob.id,newPassword);
                  mobilityContactMap.put(Mob.id,Mob);

                  i++;
               }
              
          }  
        
        if(mobilities!=null)
        {
            update mobilities;
        }
        
        /*Sending the Password Email to the Employees*/
        
      /* Set<Id> keymap= mobilityContactMap.keySet();
         if(inFutureContext)
        {
            
        inFutureContext=false;
        for(Id mobid:mobilityIdList)
        {
           
            Sendpwd(mobilityContactMap.get(mobid));
        }
            
        }*/
        
          /*Updating the password in Agreement.*/  
            for(echosign_dev1__SIGN_Agreement__c newAgreement: newAgreementList){
            
                if(newAgreement.WCT_Offer__c!=null)
                {
                    newAgreement.echosign_dev1__Password__c = OfferPasswordMap.get(newAgreement.WCT_offer__c);
                }
                else if(newAgreement.WCT_mobility__c!=null)
                {
                   newAgreement.echosign_dev1__Password__c = mobilityPasswordMap.get(newAgreement.WCT_mobility__c); 
                }
                else if(newAgreement.WCT_ELE_Separation__c!=null) 
                {    
                   newAgreement.echosign_dev1__Password__c = System.Label.Separation_Letter_Password;
                }
                
                if(newAgreement.echosign_dev1__Password__c !=null || newAgreement.echosign_dev1__Password__c !='')
                {
                    newAgreement.echosign_dev1__PasswordProtectPDF__c = true;
                    newAgreement.echosign_dev1__PasswordProtectSign__c = true;
                }
            } 
           
     }
     
     public void SetCCEmailId(List<echosign_dev1__SIGN_Agreement__c> newAgreementList){
        Set<Id> OfferIdList = new Set<Id>();
        Map<Id,String> OfferEmailCCMap = new Map<Id,String>();
            for(echosign_dev1__SIGN_Agreement__c agreement: newAgreementList){
                OfferIdList.add(agreement.WCT_offer__c); 
            }
        
            for(WCT_offer__c offer : [Select id, Send_Email_Copies_to__c from WCT_offer__c WHERE id in : OfferIdList]){
                OfferEmailCCMap.put(offer.id,offer.Send_Email_Copies_to__c);
            }
            for(echosign_dev1__SIGN_Agreement__c newAgreement: newAgreementList){
                newAgreement.echosign_dev1__Cc__c= OfferEmailCCMap.get(newAgreement.WCT_offer__c); 
            } 
    }
     
    public static string generatePassword(string lastName)
    {
        String newPassword='deloitte1';
        return newPassword;
    }
    
    /** 
       Method Name  : sendBouncedEmail
       Return Type  : void
       Description  : send an email if offer letter bounced
    */
      
    public void sendBouncedEmail(echosign_dev1__SIGN_Agreement__c agreement){
        run = false;
        String emailSubject = 'Offer Letter Bounced';
        
        String DispName;
        String[] Send_Email_copies_to = agreement.echosign_dev1__Cc__c.split(',');
        
        OrgWideEmailAddress owa = [select Id, Address from OrgWideEmailAddress where DisplayName=:agreement.WCT_Display_Name__c limit 1];
    
        Messaging.SingleEmailMessage emailOut = new Messaging.SingleEmailMessage();
        emailOut.setOrgWideEmailAddressId(owa.ID);
        emailOut.setToAddresses(Send_Email_copies_to);
        string htmlBodyTxt;
        string firstName = '';
        string lastName ='';
        htmlBodyTxt = getEmailContentTo(agreement.WCT_offer__c, agreement.Name, firstName, lastName, agreement.WCT_Bounced_Email_Address__c);
        emailOut.setHtmlBody(htmlBodyTxt);
        emailOut.setSubject(emailSubject); 
        emailOut.setSaveAsActivity(false);

        try {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[]{emailOut});
        }
        catch (EmailException ex) {
            WCT_ExceptionUtility.logException('WCT_AgreementTriggerHandler-sendBouncedEmail', null, ex.getMessage() + ' ::: ' + ex.getStackTraceString());
        }    
    }

    private string getEmailContentTo(Id offerId, String agreementName, String conFirstName, String conLastName, String bouncedEmailAddress){
        string htmlCont;
        htmlCont='<html>'+
                        '<table align="left" height="400" width="600" cellpadding="5" border="0" cellspacing="5">'+
                            '<tbody>'+
                                '<tr height="50" valign="top">'+
                                    '<td style=" color:#000000; font-size:12pt; background-color:#FFFFFF; font-family:arial; bLabel:main; bEditID:r3st1;" teditid="c1r1" locked="0" aeditid="c1r1">'+
                                        '<p class="MsoNormal" style="margin-left:4.3pt">'+
                                            '<b><span style="font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;mso-bidi-font-family:Arial;color:gray;mso-no-proof:yes">'+
                                                '<img src="'+URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.ImageServer?id='+Label.WCT_Deloitte_Logo_ID+'&oid='+UserInfo.getOrganizationId()+'" border="0" align="left" alt=""><br>'+
                                            '</span></b>'+
                                            '<span style="font-size:8.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:gray">&nbsp; &nbsp;For internal use only</span>'+
                                        '</p>'+
                                    '</td>'+
                                '</tr>'+
                                '<tr height="300" valign="top">'+
                                    '<td style=" color:#000000; font-size:12pt; background-color:#FFFFFF; font-family:arial; bLabel:main; bEditID:r3st1;" teditid="c1r2" locked="0" aeditid="c1r2">'+
                                        '<p class="MsoNormal" style="font-family: arial; font-size: 12pt; color: rgb(0, 0, 0); margin: 0in 9pt 0.0001pt;"><b><span>&nbsp;</span></b></p>'+
                                        '<p class="MsoNormal" style="font-family: arial; font-size: 12pt; color: rgb(0, 0, 0); margin-left: 9pt;">'+
                                            '<span style="color: rgb(146, 212, 0); font-family: Times New Roman, serif; font-size: 21pt;">Offer Letter Bounced</span>'+
                                        '</p>'+
                                        '<p class="MsoNormal" style="font-family: arial; color: rgb(0, 0, 0); margin: 6pt 0in 12pt 9.35pt;">'+
                                            '<span style="color: rgb(51, 51, 51); font-family: Arial, sans-serif; font-size: 9pt;"><b>Echosign was unable to deliver your '+agreementName+' document to the email address: <em>'+bouncedEmailAddress+'</em>. Please check that this is correct email address.</b></span>'+
                                            '<span style="font-size: 9pt; color: rgb(51, 51, 51); font-family: Arial, sans-serif;">&nbsp;</span>'+
                                        '</p>'+                                        
                                        '<p class="MsoNormal" style="font-family: arial; color: rgb(0, 0, 0); margin: 6pt 0in 12pt 9.35pt;">'+
                                            '<span style="color: rgb(51, 51, 51); font-family: Arial, sans-serif; font-size: 9pt;">Please log into the Talent Management System to view on their offer document.</span>'+
                                            '<span style="font-size: 9pt; color: rgb(51, 51, 51); font-family: Arial, sans-serif;">&nbsp;</span>'+
                                        '</p>'+
                                        '<p class="MsoNormal" style="font-family: arial; color: rgb(0, 0, 0); margin: 6pt 0in 12pt 9.35pt;">'+
                                            '<span style="color: rgb(51, 51, 51); font-family: Arial, sans-serif; font-size: 9pt;"><a href="'+URL.getSalesforceBaseUrl().toExternalForm()+OfferId+'">'+URL.getSalesforceBaseUrl().toExternalForm()+OfferId+'</a></span>'+
                                            '<span style="font-size: 9pt; color: rgb(51, 51, 51); font-family: Arial, sans-serif;">&nbsp;</span>'+
                                        '</p>'+                                        
                                        '<br/>'+
                                        '<p class="MsoNormal" style="font-family: arial; color: rgb(0, 0, 0); margin: 6pt 0in 12pt 9.35pt;">'+
                                            '<span style="color: rgb(51, 51, 51); font-family: Arial, sans-serif; font-size: 9pt;">Thank you,</span>'+
                                            '<span style="font-size: 9pt; color: rgb(51, 51, 51); font-family: Arial, sans-serif;">&nbsp;</span>'+
                                        '</p>'+
                                        '<p class="MsoNormal" style="color: rgb(0, 0, 0); margin: 6pt 0in 12pt 9.35pt;">'+
                                            '<span style="color: rgb(51, 51, 51); font-family: Arial, sans-serif; font-size: 9pt;">Talent Management</span>'+
                                        '</p>'+
                                        '<div style="font-family: arial; font-size: 12pt; color: rgb(0, 0, 0); border-style: none none solid; border-bottom-color: windowtext; border-bottom-width: 1pt; padding: 0in 0in 1pt;">'+
                                            '<p class="MsoNormal" style="margin-top:6.0pt;border:none;mso-border-bottom-alt: solid windowtext .75pt;padding:0in;mso-padding-alt:0in 0in 1.0pt 0in"><o:p>&nbsp;</o:p></p>'+
                                        '</div>'+
                                        '<p class="Legallinks" style="font-family: arial; font-size: 12pt; color: rgb(0, 0, 0); margin-left: 9.35pt;">'+
                                            '<a href="https://www.deloittenet.com"><b><span style="font-size:8.5pt">DeloitteNet</span></b></a> | '+
                                            '<a href="http://www.deloitte.com/us/security"><b><span style="font-size:8.5pt">Security</span></b></a> | '+
                                            '<a href="http://www.deloitte.com/us/legal"><b><span style="font-size:8.5pt">Legal</span></b></a> | '+
                                            '<b><span style="font-size:8.5pt"><a href="http://www.deloitte.com/us/privacy">Privacy</a></span></b>'+
                                        '</p>'+
                                        '<p class="Legallinks" style="font-family: arial; font-size: 12pt; color: rgb(0, 0, 0); margin-left: 9.35pt;"><b><br></b></p>'+
                                        '<p class="Legaltext" style="font-family: arial; font-size: 12pt; color: rgb(0, 0, 0); margin-left: 9.35pt;">'+
                                            '<font face="Arial, sans-serif" size="1">30 Rockefeller Plaza<br> New York, NY 10112-0015<br>United States</font>'+
                                            '<font face="arial" style="font-size: 12pt;"><o:p></o:p></font>'+
                                        '</p>'+
                                        '<p class="Legaltext" style="font-family: arial; font-size: 12pt; color: rgb(0, 0, 0); margin-left: 9.35pt;"><o:p></o:p></p>'+
                                    '</td>'+
                                '</tr>'+
                                '<tr height="50" valign="top">'+
                                    '<td style=" color:#000000; font-size:12pt; background-color:#FFFFFF; font-family:arial; bLabel:main; bEditID:r3st1;" teditid="c1r3" locked="0" aeditid="c1r3">'+
                                        '<p class="Legaltext" style="margin: 8pt 0in 12pt 0.15in;">'+
                                            '<font size="1">'+
                                                '<img src="'+URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.ImageServer?id='+Label.WCT_Sponsor_ID+'&oid='+UserInfo.getOrganizationId()+'" border="0" align="left" alt=""><br>'+
                                            '</font>'+
                                        '</p>'+
                                        '<p class="Legaltext" style="margin: 8pt 0in 12pt 0.15in;">'+
                                            '<font size="1"><br></font>'+
                                        '</p>'+
                                        '<p class="Legaltext" style="margin: 8pt 0in 12pt 0.15in;">'+
                                            '<font size="1"><br></font>'+
                                        '</p>'+
                                        '<p class="Legaltext" style="margin: 8pt 0in 12pt 0.15in;">'+
                                            '<font size="1">Copyright © 2013 Development LLC. All rights reserved.<br> 36 USC 220506<br>Member of Deloitte Touche Tohmatsu Limited</font>'+
                                        '</p>'+
                                        '<p class="Legaltext" style="margin: 8pt 0in 12pt 0.15in;">'+
                                            '<font size="1">'+
                                                '<img src="'+URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.ImageServer?id='+Label.WCT_In_Logo+'&oid='+UserInfo.getOrganizationId()+'" border="0" align="left" alt="">'+
                                                '<img src="'+URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.ImageServer?id='+Label.WCT_Facebook_Image_ID+'&oid='+UserInfo.getOrganizationId()+'" border="0" align="left" alt="">'+
                                                '<img src="'+URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.ImageServer?id='+Label.WCT_You_Tube_Image_ID+'&oid='+UserInfo.getOrganizationId()+'" border="0" align="left" alt="">'+
                                                '<img src="'+URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.ImageServer?id='+Label.WCT_Twitter_Image_ID+'&oid='+UserInfo.getOrganizationId()+'" border="0" align="left" alt="">'+
                                                '<img src="'+URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.ImageServer?id='+Label.WCT_RSS_Feed_Image_ID+'&oid='+UserInfo.getOrganizationId()+'" border="0" align="left" alt="">'+
                                                '<br>'+
                                            '</font>'+
                                        '</p>'+
                                    '</td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</html>'; 
        return htmlCont;
    }   
    
}