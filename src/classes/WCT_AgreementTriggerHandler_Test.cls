@isTest
private class WCT_AgreementTriggerHandler_Test
{
    public static testmethod void test1()
    {
       
        
        Contact con=WCT_UtilTestDataCreation.createContact();
        con.WCT_Taleo_Status__c='Offer Details Updated';    
        con.WCT_User_Group__c = 'United States';
        con.email='test@deloitte.com';       
        insert con;
        
        con.WCT_Taleo_Status__c='Offer - To be Extended';
        update con;
        WCT_Offer__c offer = [select id, WCT_status__c,WCT_Candidate__c,WCT_Address1_INTERN__c,WCT_Candidate_Tracker__c,WCT_Type_of_Hire__c from WCT_Offer__c where WCT_Candidate__c=:con.id]; 
        
        echosign_dev1__SIGN_Agreement__c agreement = new echosign_dev1__SIGN_Agreement__c();
        agreement.WCT_offer__c = offer.id;
        
        /*Mobility Sample data */
        WCT_Mobility__c testMobility= new  WCT_Mobility__c();
        testMobility.WCT_Mobility_Employee__c=con.id;
        testMobility.WCT_Assignment_Type__c='Short Term';
        testMobility.WCT_Mobility_Status__c='New';
        
        insert testMobility;
        
        system.assert(testMobility.id!=null);
               
        echosign_dev1__SIGN_Agreement__c agreementMobility = new echosign_dev1__SIGN_Agreement__c();
        agreementMobility.WCT_Mobility__c = testMobility.id;
        
        OrgWideEmailAddress owa = [select Id, Address from OrgWideEmailAddress where DisplayName= 'Deloitte US Offers' limit 1];
        
       Test.StartTest();  
           insert agreement;
           agreement.echosign_dev1__Status__c = 'Out for Signature';
           agreement.WCT_Declined_Reason__c=null;
           update agreement;
           
           agreement.echosign_dev1__Status__c = 'Cancelled / Declined';
           agreement.WCT_Declined_Reason__c='Test Reason';
           update agreement;
           
           /*Mobility Agreement*/
      /*     insert agreementMobility ;
           agreementMobility.echosign_dev1__Status__c = 'Out for Signature';
           update agreementMobility ;
           */
        Test.StopTest();
        
    }
    
    public static testmethod void test2()
    {
        
        Contact con=WCT_UtilTestDataCreation.createContact();
        con.WCT_Taleo_Status__c='Offer Details Updated';   
        con.WCT_User_Group__c = 'United States'; 
        con.email='test@deloitte.com';       
        insert con;
        
        con.WCT_Taleo_Status__c='Offer - To be Extended';
        update con;
        WCT_Offer__c offer = [select id, WCT_status__c,WCT_Candidate__c,WCT_Address1_INTERN__c,WCT_Candidate_Tracker__c,WCT_Type_of_Hire__c from WCT_Offer__c where WCT_Candidate__c=:con.id]; 
        
        echosign_dev1__SIGN_Agreement__c agreement = new echosign_dev1__SIGN_Agreement__c();
        agreement.WCT_offer__c = offer.id;
        
        Test.StartTest();
          insert agreement;
          agreement.echosign_dev1__Status__c = 'Signed';
          update agreement;
        Test.StopTest();
        
     }
    
    public static testmethod void test3()
    {
        
        Contact con=WCT_UtilTestDataCreation.createContact();
        con.WCT_User_Group__c = 'United States';
        con.WCT_Taleo_Status__c='Offer Details Updated';    
        con.email='test@deloitte.com';       
        insert con;
        
        con.WCT_Taleo_Status__c='Offer - To be Extended';
        update con;
        WCT_Offer__c offer = [select id, WCT_status__c,WCT_Candidate__c,WCT_Address1_INTERN__c,WCT_Candidate_Tracker__c,WCT_Type_of_Hire__c from WCT_Offer__c where WCT_Candidate__c=:con.id]; 
        
        echosign_dev1__SIGN_Agreement__c agreement = new echosign_dev1__SIGN_Agreement__c();
        agreement.WCT_offer__c = offer.id;
        
        Test.StartTest();
          insert agreement;
          agreement.echosign_dev1__Status__c = 'Cancelled / Declined';
          agreement.WCT_Declined_Reason__c='Test declined';
          update agreement;
        Test.StopTest();
        
     }
}