public class WCT_Apply_Dependent_Visa_FormController extends SitesTodHeaderController{
    public task taskRecord{get;set;}
    public WCT_Task_Reference_Table__c taskRefRecord{get;set;}
    public String taskSubject {get;set;}
    public String taskid{get;set;}
    public boolean display{get;set;}
    public String taskVerbiage{get;set;}
    public String siteURL{get;set;}
    
    public WCT_Apply_Dependent_Visa_FormController()
    {
        taskid = ApexPages.currentPage().getParameters().get('taskid');
        display=true;
        taskSubject = '';
        taskVerbiage = '';
        siteURL = '';
    }

    public void updateTaskFlags()
    {
        if(taskid==''|| taskid==null){
            display=false;
            return;
        }

        taskRecord=[SELECT id, Subject,Ownerid, Status,WCT_Is_Visible_in_TOD__c, WCT_Auto_Close__c, WCT_Task_Reference_Table_ID__c, Site_URL__c FROM Task WHERE Id =: taskid];
        taskRefRecord = [SELECT Id, Name, WCT_Task_Subject__c, Form_Verbiage__c, ownerId FROM WCT_Task_Reference_Table__c Where id=:taskRecord.WCT_Task_Reference_Table_ID__c];

        if(taskRecord != null){

            //Get Task Subject
            taskSubject = taskRecord.Subject;
            taskVerbiage = taskRefRecord.Form_Verbiage__c;
            siteURL = Label.Partial_Site_URL+'WCT_Add_Update_DependentVisaInfo?em='+CryptoHelper.encrypt(LoggedInContact.Email);


        }else{
            return;
        }
        
    }
}