@isTest
public class WCT_AssignRecordEmailSchedBatch_Test
{
    static testMethod void m1(){
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Mobility__c mob = WCT_UtilTestDataCreation.createMobility(con.id);
        mob.WCT_Last_Working_Day_in_US__c=system.today().addDays(16);
        mob.WCT_Travel_End_Date__c=system.today().addDays(3);
        insert mob;
        WCT_Assignment__c assign=new WCT_Assignment__c();
        assign.WCT_Mobility__c=mob.id;
        assign.WCT_Status__c='Active' ;
        assign.WCT_Initiation_Date__c=system.today().addDays(2); 
        insert assign;
        Test.startTest();
        WCT_AssignRecordEmailSchedulableBatch createCon = new WCT_AssignRecordEmailSchedulableBatch();
        system.schedule('New','0 0 2 1 * ?',createCon); 
        Test.stopTest(); 
     }       
}