@isTest

Private class WCT_Assignment_Mass_Update_Cntrlr_Test
{

    public static testmethod void test1()
    {
        Contact con = new Contact();
        con.LastName = 'test';
        INSERT con;
        
        Id EmpRecType = [SELECT Id FROM RecordType WHERE RecordType.Name='Employment Visa'].Id;
        WCT_Mobility__c mobRec1  = new WCT_Mobility__c();
        mobRec1.WCT_Mobility_Status__c = 'New';
        mobRec1.RecordTypeId = EmpRecType;
        mobRec1.WCT_Assignment_Type__c = 'Short Term';
        mobRec1.WCT_First_Working_Day_in_US__c = date.parse('10/10/2014');
        mobRec1.WCT_Last_Working_Day_in_US__c = date.parse('11/11/2014');
        mobRec1.WCT_Mobility_Employee__c = con.id;
        insert mobRec1;
        
        WCT_Assignment__c assignRec = new WCT_Assignment__c();
        assignRec.WCT_Status__c = 'New';
        assignRec.WCT_Employee__c = con.id;
        assignRec.WCT_Mobility__c = mobRec1.id;
        assignRec.WCT_Initiation_Date__c = date.parse('10/10/2014');
        assignRec.WCT_End_Date__c = date.parse('11/11/2014');
        assignRec.WCT_US_Compensation__c = 123456;
        insert assignRec;
        
        Test.startTest();
        List<WCT_Assignment__c> assignlist2 = new List<WCT_Assignment__c>();
        assignlist2.add(assignRec);
              
        WCT_Assignment_Mass_Update_Controller.assignmentWrapper assignWrap2 = new WCT_Assignment_Mass_Update_Controller.assignmentWrapper(true,assignRec);
        
        WCT_Assignment_Mass_Update_Controller controller2 = new WCT_Assignment_Mass_Update_Controller();
        controller2.selectedStatus = 'New';
        controller2.startDate = date.parse('10/10/2014');
        controller2.startDateTo = date.parse('10/12/2014');
        controller2.endDate = date.parse('11/11/2014');
        controller2.endDateTo = date.parse('11/13/2014');
        controller2.getStatus();
        controller2.getListTasks();
        controller2.checkAll();
        controller2.massClose();
        
        Test.stopTest();
    
    }  
    
    public static testmethod void test2()
    {
        Contact con = new Contact();
        con.LastName = 'test';
        INSERT con;
        
        Id EmpRecType = [SELECT Id FROM RecordType WHERE RecordType.Name='Employment Visa'].Id;
        WCT_Mobility__c mobRec1  = new WCT_Mobility__c();
        mobRec1.WCT_Mobility_Status__c = 'New';
        mobRec1.RecordTypeId = EmpRecType;
        mobRec1.WCT_Assignment_Type__c = 'Short Term';
        mobRec1.WCT_First_Working_Day_in_US__c = date.parse('10/10/2014');
        mobRec1.WCT_Last_Working_Day_in_US__c = date.parse('11/11/2014');
        mobRec1.WCT_Mobility_Employee__c = con.id;
        insert mobRec1;
        
        WCT_Assignment__c assignRec = new WCT_Assignment__c();
        assignRec.WCT_Status__c = 'New';
        assignRec.WCT_Employee__c = con.id;
        assignRec.WCT_Mobility__c = mobRec1.id;
        assignRec.WCT_Initiation_Date__c = date.parse('10/10/2014');
        assignRec.WCT_End_Date__c = date.parse('11/11/2014');
        assignRec.WCT_US_Compensation__c = 123456;
        insert assignRec;
        
        Test.startTest();
        List<WCT_Assignment__c> assignlist2 = new List<WCT_Assignment__c>();
        assignlist2.add(assignRec);
        
      
        WCT_Assignment_Mass_Update_Controller.assignmentWrapper assignWrap2 = new WCT_Assignment_Mass_Update_Controller.assignmentWrapper(true,assignRec);
        
        WCT_Assignment_Mass_Update_Controller controller2 = new WCT_Assignment_Mass_Update_Controller();
        controller2.selectedStatus = 'New';
        //controller2.startDate = date.parse('10/10/2014');
        //controller2.startDateTo = date.parse('10/12/2014');
        //controller2.endDate = date.parse('11/11/2014');
        //controller2.endDateTo = date.parse('11/13/2014');
        
        controller2.getStatus();
        controller2.getListTasks();
        controller2.checkAll();
        controller2.massClose();
        
        Test.stopTest();
    
    }    
     
}