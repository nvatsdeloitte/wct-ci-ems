/*
Class name : WCT_Assignment_Mass_Update_Controller
Description : This class is controller for WCT_Assignment_Mass_Update page.

Task number     Date            Modified By                         Description
------------    ---------       ---------------------               ----------------------
                10-Oct-14        Darshee Mehta                       Created
                                                            
*/

public class WCT_Assignment_Mass_Update_Controller{
          
    public String selectedStatus{get;set;}
    public Date startDate{set;get;}
    public Date startDateTo{set;get;}
    public Date endDate{set;get;}
    public Date endDateTo{set;get;}
    public List<WCT_Assignment__c> lstAssignmentRecord{get;set;}
    public List<AssignmentWrapper> showAllRecordList{get;set;}
    public List<WCT_Assignment__c> finalAssignment{get;set;}
    
                
   //-----------------------------------
   //    Constructor
   //-----------------------------------
    
    public WCT_Assignment_Mass_Update_Controller(){
        showAllRecordList = new List<AssignmentWrapper>();
    }

   //-----------------------------------
   //    Wrapper Class
   //-----------------------------------    
        
    public class assignmentWrapper{
        public WCT_Assignment__c asRecord{get;set;}
        public Boolean check{get;set;}
        
        public assignmentWrapper(Boolean flag, WCT_Assignment__c wctAssignment){
          
           asRecord = wctAssignment;
           check = flag;

                      
        }
    }
    

   //--------------------------------------------------------------------------
   //    Get Assignment Status By Dropdown List Value of 'Assignment Status'
   //--------------------------------------------------------------------------
                    
    public List<SelectOption> getStatus(){
        
        //new list for holding all of the picklist options   
        List<SelectOption> options = new List<SelectOption>(); 
        String first_picklist_option = '-No Status Selected-';
        
        if (first_picklist_option != null ){                                                //if there is a first value being provided
            options.add(new selectOption(first_picklist_option, first_picklist_option));     //add the first option
        }
        
            //grab the sobject that was passed
        Schema.sObjectType sobject_type = WCT_Assignment__c.getSObjectType();
         
            //describe the sobject
        Schema.DescribeSObjectResult sobject_describe = WCT_Assignment__c.getSObjectType().getDescribe();
         
            //get a map of fields for the passed sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
         
            //grab the list of picklist values for the passed field on the sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get('WCT_Status__c').getDescribe().getPickListValues();
          
            //for all values in the picklist list add the value and label to our final list
        for (Schema.PicklistEntry a : pick_list_values) { 
            options.add(new SelectOption(a.getLabel(), a.getValue()));  
        }
 
        return options;
    }
    

   //----------------------------------------------------------------------------------------------------
   //    Get a list of Assignment Records in the Table on the click of Search
   //----------------------------------------------------------------------------------------------------
   
    public PageReference getListTasks() {
        showAllRecordList = new List<AssignmentWrapper>();

        String AssignStartDateQuery = '';
        String AssignStartDateToQuery = '';
        String AssignEndDateQuery = '';
        String AssignEndDateToQuery = '';
        String conRecType = 'Employee';
        
        if(startDate != null)
            AssignStartDateQuery = 'AND WCT_Initiation_Date__c >=: startDate ';
        else
            AssignStartDateQuery = '';
            // Add Query condition for Start date To
          if(startDateTo != null)
            AssignStartDateToQuery = 'AND WCT_Initiation_Date__c <=: startDateTo ';
        else
            AssignStartDateToQuery = '';
              
            
        
        if(endDate != null)
            AssignEndDateQuery = 'AND WCT_End_Date__c >=: endDate ';
        else
            AssignEndDateQuery = '';
            
            //Add Query condition for End Date To
           if(endDateTo != null)
            AssignEndDateToQuery = 'AND WCT_End_Date__c <=: endDateTo ';
        else
            AssignEndDateToQuery = ''; 
            
            
            
            

        String query = 'SELECT Id, Name, WCT_Status__c, OwnerId, WCT_Employee__c, WCT_Initiation_Date__c, WCT_End_Date__c, WCT_Mobility__c,WCT_Mobility__r.WCT_Mobility_Status__c,WCT_Mobility__r.WCT_From_Date__c,WCT_Mobility__r.WCT_To_Date__c,WCT_Mobility__r.WCT_First_Working_Day_in_US__c,WCT_Mobility__r.WCT_Last_Working_Day_in_US__c,WCT_US_Compensation__c FROM WCT_Assignment__c WHERE WCT_Status__c =:selectedStatus AND WCT_Status__c != null AND WCT_Employee__c != null AND WCT_Employee__r.RecordType.Name =:conRecType ' + AssignStartDateQuery + AssignStartDateToQuery + AssignEndDateQuery + AssignEndDateToQuery; 
        
        lstAssignmentRecord = database.query(query);
        System.debug('#############################'+lstAssignmentRecord); 
        
        
        for(WCT_Assignment__c assignmentRec: lstAssignmentRecord){
            showAllRecordList.add(new AssignmentWrapper(false, assignmentRec));
        }

        return null;
    }

   //---------------------------------------------
   //    Select All Functionality
   //---------------------------------------------
   public PageReference checkAll(){
   
       /*for(AssignmentWrapper i: showAllRecordList){
            for(TaskWrapper recTask: i.lstOfTask){
                recTask.check = !recTask.check;
            }
            
       }*/
       return null;
   }
   
   
   
   //---------------------------------------------
   //    Field Update on the click of Override
   //--------------------------------------------- 
   
                  
   public PageReference massClose(){   
        
        finalAssignment = new List<WCT_Assignment__c>();  

        for(AssignmentWrapper i: showAllRecordList){
            if(i.check == true){
                finalAssignment.add(i.asRecord);
            }
        }
        
        Database.update(finalAssignment);
        

        Database.update(lstAssignmentRecord);
        getListTasks();
        return null;
     }
}