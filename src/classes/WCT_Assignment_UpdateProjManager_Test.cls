/**
 * Description : Test class for WCT_Assignment_Update_Project_Manager
 */
 
@isTest

Private class WCT_Assignment_UpdateProjManager_Test
{
    static testMethod void m1(){
        recordtype recType = [select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con = WCT_UtilTestDataCreation.createEmployee(recType.id);
        INSERT con;
        
        WCT_Mobility__c mobRec = WCT_UtilTestDataCreation.createMobility(con.id);
        mobRec.WCT_US_Project_Mngr__c = con.Email;
        mobRec.WCT_Project_Controller__c = con.Email;
        INSERT mobRec;
        
        WCT_Assignment__c assignRec = WCT_UtilTestDataCreation.CreateAssignment();
        assignRec.WCT_Mobility__c = mobRec.Id;
        INSERT assignRec;
        
        WCT_Assignment_Update_Project_Manager mob = new WCT_Assignment_Update_Project_Manager();
    }
}