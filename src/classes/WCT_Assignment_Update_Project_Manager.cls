/*************************************************************************************
* Name          :    WCT_Assignment_Update_Project_Manager
* Description   :    Helper Code
* Author        :    Deloitte
 
Modification Log
----------------
Date             Developer                Comments
---------------------------------------------------------------------------------------
19/06/2014       Darshee Mehta              Created


**************************************************************************************/

public class WCT_Assignment_Update_Project_Manager{

    public WCT_Assignment_Update_Project_Manager(){}
    
    public static void updateProjectManager(List<WCT_Assignment__c> lstNewAssignment){
        
        Set<String> setMobilityEmailId = new Set<String>();
        List<WCT_Mobility__c> lstMobility = new List<WCT_Mobility__c>();
        List<Contact> lstContact = new List<Contact>();
        Set<Id> mobilityIds=new Set<Id>();
        for(WCT_Assignment__c assignmentRecord : lstNewAssignment)
        {
            
            if(assignmentRecord.WCT_Mobility__c!=null)
            {
                mobilityIds.add(assignmentRecord.WCT_Mobility__c);
                setMobilityEmailId.add(assignmentRecord.WCT_Mobility__r.WCT_US_Project_Mngr__c);
                setMobilityEmailId.add(assignmentRecord.WCT_Mobility__r.WCT_Project_Controller__c);
                
            }
        }
        
       lstMobility = [SELECT Id, WCT_US_Project_Mngr__c, WCT_Project_Controller__c FROM WCT_Mobility__c WHERE (WCT_US_Project_Mngr__c != null or WCT_Project_Controller__c != null) and ID IN : mobilityIds];
        Map<Id,String> mobilityPMMap = new Map<Id,String>();
        Map<Id,String> mobilityPCMap = new Map<Id,String>();
        for(WCT_Mobility__c m : lstMobility){
            setMobilityEmailId.add(m.WCT_US_Project_Mngr__c);
            setMobilityEmailId.add(m.WCT_Project_Controller__c);
            mobilityPMMap.put(m.Id,m.WCT_US_Project_Mngr__c);
            mobilityPCMap.put(m.Id,m.WCT_Project_Controller__c);
            
        } 
        
        Map<String,Id> contactIdMap=new Map<String,Id>();
        
        for(Contact contactTempRec : [SELECT Id, Name, Email FROM Contact WHERE RecordType.Name='Employee' AND Email IN: setMobilityEmailId and Email!=null limit:Limits.getLimitQueryRows()] )
        {
            contactIdMap.put(contactTempRec.Email, contactTempRec.Id);
        }
        
        for(WCT_Assignment__c assignmentRecord : lstNewAssignment) {
        //    for(WCT_Mobility__c mobilityRecord : lstMobility){
                if(assignmentRecord.WCT_Mobility__c != null && mobilityPMMap.containskey(assignmentRecord.WCT_Mobility__c) && contactIdMap.ContainsKey(mobilityPMMap.get(assignmentRecord.WCT_Mobility__c)))
                    assignmentRecord.WCT_US_Project_Manager_Supervisor__c = contactIdMap.get(mobilityPMMap.get(assignmentRecord.WCT_Mobility__c));
                if(assignmentRecord.WCT_Mobility__c != null && mobilityPCMap.containskey(assignmentRecord.WCT_Mobility__c) && contactIdMap.ContainsKey(mobilityPCMap.get(assignmentRecord.WCT_Mobility__c)))
                    assignmentRecord.WCT_US_Project_Coordinator__c = contactIdMap.get(mobilityPCMap.get(assignmentRecord.WCT_Mobility__c));
          //  }
        }
        
    }
}