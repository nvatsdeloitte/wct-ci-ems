public with sharing class WCT_AttachmentIntviewCandidateRec {
String candidateId;
    public WCT_AttachmentIntviewCandidateRec(ApexPages.StandardController controller) {
    candidateId = System.currentPagereference().getParameters().get('id');
    }
    public List<Attachment> getAttachmentsCandidate() {
        list<String> attIds = new list<String>();

        set<Id> attParentId = new set<Id>();
		if(candidateId !='' && candidateId != null){     
	        attParentId.add(candidateId);
	        for(WCT_Candidate_Documents__c candidDocId : [SELECT Id FROM WCT_Candidate_Documents__c where WCT_Candidate__c  =: candidateId and WCT_Visible_to_Interviewer__c=true]){
	            attParentId.add(candidDocId.Id);
	        }
	        for(WCT_Candidate_Requisition__c canTrackerId : [SELECT Id FROM WCT_Candidate_Requisition__c where WCT_Contact__c  =: candidateId]){
	            attParentId.add(canTrackerId.Id);
	        }
        }
        if(!attParentId.IsEmpty()){
            return [select Id, ParentId, Parent.Name, Parent.Type, Name, ContentType, BodyLength, CreatedBy.Name, LastModifiedDate, Description  from Attachment where parentId in :attParentId];
        }
        else{
        	
            return null;
        }
    }   
    public List<Note> getNotesCandidate() {
        list<String> attIds = new list<String>();

        set<Id> notesParentId = new set<Id>();
        if(candidateId !='' && candidateId != null){
	        notesParentId.add(candidateId);
	        for(WCT_Candidate_Documents__c candidDocId : [SELECT Id FROM WCT_Candidate_Documents__c where WCT_Candidate__c  =: candidateId and WCT_Visible_to_Interviewer__c=true]){
	            notesParentId.add(candidDocId.Id);
	        }
	        for(WCT_Candidate_Requisition__c canTrackerId : [SELECT Id FROM WCT_Candidate_Requisition__c where WCT_Contact__c  =: candidateId]){
	            notesParentId.add(canTrackerId.Id);
	        }
        }
        if(!notesParentId.IsEmpty()){
            return [SELECT Body,CreatedBy.Name,Id,LastModifiedDate,ParentId,Title FROM Note where parentId in :notesParentId];
        }
        else{
            return null;
        }
    }  
}