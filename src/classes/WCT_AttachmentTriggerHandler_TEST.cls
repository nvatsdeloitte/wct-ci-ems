/*****************************************************************************************
    Name    : WCT_AttachmentTriggerHandler_test
    Desc    : Test class for WCT_AttachmentTriggerHandler class                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                  Date            Description
---------------------------------------------------------------------------
  Deloitte                 19 Dec 2013         Created 
******************************************************************************************/
@isTest
private class WCT_AttachmentTriggerHandler_TEST{

    /*******Varibale Declaration********/
    public static Id profileIdRecCoo=[Select id from Profile where Name=:WCT_UtilConstants.Recruiting_Coordinator].Id;
    public static Id profileIdIntv=[Select id from Profile where Name=:WCT_UtilConstants.INTERVIWER_PROFILE_NAME].Id;
    public static Id profileIdRecr=[Select id from Profile where Name=:WCT_UtilConstants.RECRUITER_COMPANY].Id;
    public static String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();
    public static User userRecRecCoo, userRecRecIntv, userRecRecr;
    public static Contact Candidate;
    public static WCT_Requisition__c Requisition;
    public static WCT_Candidate_Requisition__c CandidateRequisition;
    public static WCT_Interview__c Interview;
    public static WCT_Interview__c InterviewTd,InterviewPst,InterviewUpC,InterviewAll;
    public static WCT_Interview_Junction__c InterviewTracker,InterviewTrackerQueried;
    public Static WCT_List_Of_Names__c ClickToolForm;
    public static Document document;
    /***************/
    
        /** 
        Method Name  : createUserRecCoo
        Return Type  : User
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecCoo()
    {
      userRecRecCoo=WCT_UtilTestDataCreation.createUser('ReCoSchd', profileIdRecCoo, 'arunsharmaReCoSchd@deloitte.com', 'arunsharma4@deloitte.com');
      insert userRecRecCoo;
      return  userRecRecCoo;
    }
    /** 
        Method Name  : createUserRecr
        Return Type  : User
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecr()
    {
      userRecRecr=WCT_UtilTestDataCreation.createUser('RecrSchd', profileIdRecr, 'arunsharmaRecrSchd@deloitte.com', 'arunsharma4@deloitte.com');
      insert userRecRecr;
      return  userRecRecr;
    }
    /** 
        Method Name  : createUserIntv
        Return Type  : User
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserIntv()
    {
      userRecRecIntv=WCT_UtilTestDataCreation.createUser('IntvSchd', profileIdIntv, 'arunsharmaIntvSchd@deloitte.com', 'arunsharma4@deloitte.com');
      insert userRecRecIntv;
      return  userRecRecIntv;
    }
    /** 
        Method Name  : createCandidate
        Return Type  : Contact
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static Contact createCandidate()
    {
      Candidate=WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
      insert Candidate;
      return  Candidate;
    }    
    /** 
        Method Name  : createRequisition
        Return Type  : WCT_Requisition__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Requisition__c createRequisition()
    {
      Requisition=WCT_UtilTestDataCreation.createRequisition(userRecRecr.Id);
      insert Requisition;
      return  Requisition;
    }
    /** 
        Method Name  : createCandidateRequisition
        Return Type  : WCT_Candidate_Requisition__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Candidate_Requisition__c createCandidateRequisition()
    {
      CandidateRequisition=WCT_UtilTestDataCreation.createCandidateRequisition(Candidate.ID,Requisition.Id);
      insert CandidateRequisition;
      return  CandidateRequisition;
    } 
    /** 
        Method Name  : createDocument
        Return Type  : Document
        Type      : private
        Description  : Create temp records for data mapping         
    */    
    private Static Document createDocument()
    {
      document=WCT_UtilTestDataCreation.createDocument();
    insert document;
      return  document;
    }
    /** 
        Method Name  : createClickToolForm
        Return Type  : WCT_List_Of_Names__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_List_Of_Names__c createClickToolForm()
    {
      ClickToolForm=WCT_UtilTestDataCreation.createClickToolForm(document.id);
      insert ClickToolForm; 
      return  ClickToolForm;
    } 
    /** 
        Method Name  : createInterview
        Return Type  : WCT_Interview__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview__c createInterview()
    {
      Interview=WCT_UtilTestDataCreation.createInterview(ClickToolForm.Id);
      system.runAs(userRecRecCoo)
      {
        insert Interview;
      }
      return  Interview;
    }   
    /** 
        Method Name  : createInterviewTracker
        Return Type  : WCT_Interview_Junction__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview_Junction__c createInterviewTracker()
    {
      InterviewTracker=WCT_UtilTestDataCreation.createInterviewTracker(Interview.Id,CandidateRequisition.Id,userRecRecIntv.Id);
      insert InterviewTracker;
      return  InterviewTracker;
    }
    
    static testMethod void attHandler(){
        User user = WCT_UtilTestDataCreation.createUser('tinte',SYSTEM.Label.Integration_Profile_Id,'tintegration@deloitte.com.trigger','tintegration@deloitte.com');
        insert user;
               
        SYSTEM.RunAs(user){        
            List<WCT_Candidate_Documents__c> candDocListforTrigger = WCT_AttachmentTriggerHandler_TEST.createCandidateDocuments();
            insert candDocListforTrigger;
            List<Attachment> attforTrigger = WCT_AttachmentTriggerHandler_TEST.createAttachments();
            insert attforTrigger;
            List<Attachment> attforTriggerToDelete = WCT_AttachmentTriggerHandler_TEST.createAttachments();
            insert attforTriggerToDelete;
        }
        userRecRecCoo=createUserRecCoo();
        userRecRecIntv=createUserIntv();
        userRecRecr=createUserRecr();
        Candidate=createCandidate();
        Requisition=createRequisition();
        CandidateRequisition=createCandidateRequisition();
        document=createDocument();
        ClickToolForm=createClickToolForm();
        Interview=createInterview();
        InterviewTracker=createInterviewTracker();
        InterviewTrackerQueried = [select id,name,WCT_IEF_Submission_Status__c from WCT_Interview_Junction__c limit 1];
        InterviewTrackerQueried.WCT_IEF_Submission_Status__c = 'Submitted';
        update InterviewTrackerQueried;
        List<Attachment> attforIntTrak = WCT_AttachmentTriggerHandler_TEST.createAttachmentsIntvTracker();
        insert attforIntTrak;
        Blob updatedBlob = Blob.valueof('Test file for Attachment class updated');
        attforIntTrak[0].body = updatedBlob;
        update attforIntTrak[0];
        
    }
    static List<WCT_Candidate_Documents__c> createCandidateDocuments(){
        List<WCT_Candidate_Documents__c> candDocList = new List<WCT_Candidate_Documents__c>();
        for(integer i=0;i<20;i++){
            WCT_Candidate_Documents__c c = new WCT_Candidate_Documents__c();
            c.WCT_File_Name__c = 'Attachment'+i;
            if(i==10){
                Contact con = WCT_UtilTestDataCreation.createContactAsCandidate(SYSTEM.Label.CandidateRecordtypeId);
                insert con;
                c.WCT_Candidate__c = con.id;
            }
            candDocList.add(c);
        }
        return candDocList;
    
    }
    static List<Attachment> createAttachments(){
        List<Attachment> attList = new List<Attachment>();
        for(integer j=0;j<20;j++){
            Attachment a = new Attachment();
            Blob myBlob = Blob.valueof('Test file for Attachment class');
            a.body = myBlob;
            a.ContentType = 'text/plain';
            a.Name= 'Attachment'+j;
            attList.add(a);
        }
        return attList;
    }
    static List<Attachment> createAttachmentsIntvTracker(){
        List<Attachment> attList = new List<Attachment>();
        for(integer j=0;j<1;j++){
            Attachment a = new Attachment();
            Blob myBlob = Blob.valueof('Test file for Attachment class');
            a.body = myBlob;
            a.ContentType = 'text/plain';
            a.Name= InterviewTrackerQueried.Name+j;
            a.parentId = InterviewTrackerQueried.id;
            attList.add(a);
        }
        return attList;
    }

}