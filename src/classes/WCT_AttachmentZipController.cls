/*
    * Class Name  : WCT_AttachmentZipController  
    * Description : Show all attachment associated to Candidate and provide the ability to ".zip" selected attachments.  
    * Author      : Deloitte
*/
public with sharing class WCT_AttachmentZipController {
 
    public String zipFileName {get; set;}
    public String zipContent {get; set;}
    public String AttIdsArr {get; set;}  
    public Id docId {get;set;}
    public String candidateId {get;set;}

    /*
        Method Name  : Wct_AttachmentZipController - Constructor   
    */
    public WCT_AttachmentZipController(){
        if(ApexPages.currentPage().getParameters().get('Id') != null) candidateId = ApexPages.currentPage().getParameters().get('Id');
    }

    /*
        Method Name  : uploadZip   
        Return Type  : Void
    */
    public void uploadZip() {
        if (String.isEmpty(zipFileName) ||
            String.isBlank(zipFileName)) {
            zipFileName = 'BulkZip_File.zip';//Default Name for zip file.
        }
        else {
            zipFileName.replace('.', '');// To prevent file extension
            zipFileName += '.zip';
        }
         
        Document doc = new Document();
        doc.Name = zipFileName;
        doc.ContentType = 'application/zip';
        doc.FolderId = Label.Attachment_Zip_Document_Folder_Id;
        doc.Body = EncodingUtil.base64Decode(zipContent);
        
        try{
            insert doc;  
        }Catch(DMLException ex)
        {
            WCT_ExceptionUtility.logException('WCT_AttachmentZipController-uploadZip', null, ex.getMessage() + ' ::: ' + ex.getStackTraceString());
        }        
        docId = doc.Id; 
        this.zipFileName = null;
        this.zipContent = null;
    }
    
    /*
        Method Name  : reSet
        Return Type  : PageReference 
    */    
    public PageReference reSet(){
    docId = null;
    PageReference newocp = new PageReference('/apex/WCT_AttachmentZipVF');
    newocp .getParameters().put('Id',candidateId);
    newocp.setRedirect(true);
    return newocp;
    }

    /*
        Method Name  : getAttachments   
        Return Type  : List of Attachments
    */
    public List<Attachment> getAttachments() {
        list<String> attIds = new list<String>();
        if(AttIdsArr!=null)attIds = AttIdsArr.split(',');

        set<Id> attParentId = new set<Id>();
        if(candidateId != '' && candidateId != null){
            attParentId.add(candidateId);
            for(WCT_Candidate_Documents__c candidDocId : [SELECT Id FROM WCT_Candidate_Documents__c where WCT_Candidate__c  =: candidateId]){
                attParentId.add(candidDocId.Id);
            }
            for(WCT_Candidate_Requisition__c canTrackerId : [SELECT Id FROM WCT_Candidate_Requisition__c where WCT_Contact__c  =: candidateId]){
                attParentId.add(canTrackerId.Id);
            }
            for(WCT_Interview_Junction__c canReqId : [SELECT Id FROM WCT_Interview_Junction__c where WCT_Candidate_Tracker__r.WCT_Contact__c =: candidateId]){
                attParentId.add(canReqId.Id);
            }
        }
        if(attIds.isEmpty())
            return [select Id, ParentId, Parent.Name, Parent.Type, Name, ContentType, BodyLength, CreatedDate from Attachment where parentId in :attParentId and (NOT Name like '%IEF%')];
        else
            return [select Id, ParentId, Parent.Name, Parent.Type, Name, ContentType, BodyLength, CreatedDate from Attachment where Id in :attIds and (NOT Name like '%IEF%')];
        
    }


/*
        Method Name  : getIEF Attachments   
        Return Type  : List of Attachments
    */
    public List<Attachment> getIEFAttachments() {
        list<String> attIds = new list<String>();
        list<Attachment> listIEFAttachments= new list<Attachment>();
        if(AttIdsArr!=null)attIds = AttIdsArr.split(',');

        set<Id> attParentId = new set<Id>();
        if(candidateId != '' && candidateId != null){
            attParentId.add(candidateId);
            for(WCT_Candidate_Documents__c candidDocId : [SELECT Id FROM WCT_Candidate_Documents__c where WCT_Candidate__c  =: candidateId]){
                
                attParentId.add(candidDocId.Id);
              
            }
            for(WCT_Candidate_Requisition__c canTrackerId : [SELECT Id FROM WCT_Candidate_Requisition__c where WCT_Contact__c  =: candidateId]){
                attParentId.add(canTrackerId.Id);
            }
            for(WCT_Interview_Junction__c canReqId : [SELECT Id FROM WCT_Interview_Junction__c where WCT_Candidate_Tracker__r.WCT_Contact__c =: candidateId]){
                attParentId.add(canReqId.Id);
            }
        }
        if(attIds.isEmpty())
            return [select Id, ParentId, Parent.Name, Parent.Type, Name, ContentType, BodyLength, CreatedDate from Attachment where parentId in :attParentId and Name like '%IEF%'];
        else
            return [select Id, ParentId, Parent.Name, Parent.Type, Name, ContentType, BodyLength, CreatedDate from Attachment where Id in :attIds and Name like '%IEF%'];
        
    }




    @RemoteAction
    public static AttachmentWrapper getAttachment(String attId) {
         
        Attachment att = [select Id, Name, ContentType, Body
                          from Attachment
                          where Id = :attId];
         
        AttachmentWrapper attWrapper = new AttachmentWrapper();
        attWrapper.attEncodedBody = EncodingUtil.base64Encode(att.body);
        attWrapper.attName = att.Name;
        return attWrapper;
    }
    /*
        Wrapper Class: AttachmentWrapper
        Description  : Wrapper class to wrap Attachment Name and Attachment body.
    */   
    
    public class AttachmentWrapper {
        public String attEncodedBody {get; set;}
        public String attName {get; set;}
    }
}