/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class WCT_AttachmentZipController_Test {

    public static Id profileIdRecr=[Select id from Profile where Name=:WCT_UtilConstants.RECRUITER_COMPANY].Id;
    public static Id profileIdRecCoo=[Select id from Profile where Name=:WCT_UtilConstants.Recruiting_Coordinator].Id;
    public static Id profileIdIntv=[Select id from Profile where Name=:WCT_UtilConstants.INTERVIWER_PROFILE_NAME].Id;
    
    public static Contact Candidate;
    public static User userRecRecCoo, userRecRecIntv, userRecRecr;
    public static Document document;
    public static WCT_Candidate_Documents__c CandidateDocument; 
    public static WCT_Requisition__c Requisition;
    public static WCT_Candidate_Requisition__c CandidateRequisition;
    public static Attachment attachmentDoc,attachmentCandidate,attachmentReq;
    public static WCT_Interview__c Interview;
    public static WCT_Interview_Junction__c InterviewTracker;   
    public static WCT_List_Of_Names__c ClickToolForm;  
      
   	public static String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();


    /** 
        Method Name  : createCandidate
        Return Type  : Contact
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private Static Contact createCandidate()
    {
    	Candidate=WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
    	insert Candidate;
    	return  Candidate;
    }
    /** 
        Method Name  : createCandidateDocument
        Return Type  : WCT_Candidate_Documents__c
        Type 		 : private
        Description  : Create temp records for data mapping         
    */    
    private Static WCT_Candidate_Documents__c createCandidateDocument()
    {
    	CandidateDocument=WCT_UtilTestDataCreation.createCandidateDocument(Candidate.Id);
		insert CandidateDocument;
    	return  CandidateDocument;
    }    
    /** 
        Method Name  : createDocument
        Return Type  : Document
        Type 		 : private
        Description  : Create temp records for data mapping         
    */    
    private Static Document createDocument()
    {
    	document=WCT_UtilTestDataCreation.createDocument();
		insert document;
    	return  document;
    }  
    /** 
        Method Name  : createUserRecr
        Return Type  : User
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecr()
    {
    	userRecRecr=WCT_UtilTestDataCreation.createUser('RecrAtt', profileIdRecr, 'arunsharmaRecrAtt@deloitte.com', 'arunsharma4@deloitte.com');
    	insert userRecRecr;
    	return  userRecRecr;
    }    
    /** 
        Method Name  : createUserRecCoo
        Return Type  : User
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecCoo()
    {
    	userRecRecCoo=WCT_UtilTestDataCreation.createUser('RecCooAt', profileIdRecCoo, 'arunsharmaRecCooAtt@deloitte.com', 'arunsharma4@deloitte.com');
    	insert userRecRecCoo;
    	return  userRecRecCoo;
    }   
    /** 
        Method Name  : createUserIntv
        Return Type  : User
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserIntv()
    {
    	userRecRecIntv=WCT_UtilTestDataCreation.createUser('IntvAtt', profileIdIntv, 'arunsharmaIntvAtt@deloitte.com', 'arunsharma4@deloitte.com');
    	insert userRecRecIntv;
    	return  userRecRecIntv;
    }    
    /** 
        Method Name  : createRequisition
        Return Type  : WCT_Requisition__c
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Requisition__c createRequisition()
    {
    	Requisition=WCT_UtilTestDataCreation.createRequisition(userRecRecr.Id);
    	insert Requisition;
    	return  Requisition;
    }
    /** 
        Method Name  : createCandidateRequisition
        Return Type  : WCT_Candidate_Requisition__c
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Candidate_Requisition__c createCandidateRequisition()
    {
    	CandidateRequisition=WCT_UtilTestDataCreation.createCandidateRequisition(Candidate.ID,Requisition.Id);
    	insert CandidateRequisition;
    	return  CandidateRequisition;
    }  
    /** 
        Method Name  : createAttachmentDoc
        Return Type  : Attachment
        Type 		 : private
        Description  : Create temp records for data mapping         
    */    
    private Static Attachment createAttachmentDoc()
    {
    	attachmentDoc=WCT_UtilTestDataCreation.createAttachment(CandidateDocument.Id);
		insert attachmentDoc;
    	return  attachmentDoc;
    }          
     /** 
        Method Name  : createAttachmentReq
        Return Type  : Attachment
        Type 		 : private
        Description  : Create temp records for data mapping         
    */    
    private Static Attachment createAttachmentReq()
    {
    	attachmentReq=WCT_UtilTestDataCreation.createAttachment(CandidateDocument.Id);
		insert attachmentReq;
    	return  attachmentReq;
    }
    /** 
        Method Name  : createAttachmentCandidate
        Return Type  : Attachment
        Type 		 : private
        Description  : Create temp records for data mapping         
    */    
    private Static Attachment createAttachmentCandidate()
    {
    	attachmentCandidate=WCT_UtilTestDataCreation.createAttachment(Candidate.Id);
		insert attachmentCandidate;
    	return  attachmentCandidate;
    }
    
     
    /** 
        Method Name  : createClickToolForm
        Return Type  : WCT_List_Of_Names__c
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_List_Of_Names__c createClickToolForm()
    {
    	ClickToolForm=WCT_UtilTestDataCreation.createClickToolForm(document.id);
    	insert ClickToolForm; 
    	return  ClickToolForm;
    }          
    /** 
        Method Name  : createInterview
        Return Type  : WCT_Interview__c
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview__c createInterview()
    {
    	Interview=WCT_UtilTestDataCreation.createInterview(ClickToolForm.Id);
    	system.runAs(userRecRecCoo)
    	{
    		insert Interview;
    	}
    	return  Interview;
    } 
    /** 
        Method Name  : createInterviewTracker
        Return Type  : WCT_Interview_Junction__c
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview_Junction__c createInterviewTracker()
    {
    	InterviewTracker=WCT_UtilTestDataCreation.createInterviewTracker(Interview.Id,CandidateRequisition.Id,userRecRecIntv.Id);
    	insert InterviewTracker;
    	return  InterviewTracker;
    } 
         
    static testMethod void withCandidateCandReqCandDoc() {
    	userRecRecr=createUserRecr();
    	userRecRecCoo=createUserRecCoo();
    	userRecRecIntv=createUserIntv();
    	Requisition=createRequisition();
    	Candidate = createCandidate();
    	CandidateDocument=createCandidateDocument();
    	CandidateRequisition=createCandidateRequisition();
    	attachmentDoc=createAttachmentDoc();
    	attachmentCandidate=createAttachmentCandidate();
    	attachmentReq=createAttachmentReq();
    	document=createDocument();
    	ClickToolForm=createClickToolForm();
    	Interview=createInterview();
    	InterviewTracker=createInterviewTracker();
        ApexPages.currentPage().getParameters().put('Id', Candidate.Id);

		test.startTest();
        WCT_AttachmentZipController AZC = new WCT_AttachmentZipController();
        AZC.getAttachments();
        AZC.reSet();
        AZC.zipFileName='ZipFileName';
        Blob DocBody = Blob.ValueOf('Test Value for Document Body.');
        AZC.zipContent=EncodingUtil.base64Encode(DocBody);
		WCT_AttachmentZipController.getAttachment(attachmentDoc.Id);
        AZC.uploadZip();
		test.stopTest();
    }
    static testMethod void withoutDocName() {
    	userRecRecr=createUserRecr();
    	userRecRecCoo=createUserRecCoo();
    	userRecRecIntv=createUserIntv();
    	Requisition=createRequisition();
    	Candidate = createCandidate();
    	CandidateDocument=createCandidateDocument();
    	CandidateRequisition=createCandidateRequisition();
    	attachmentDoc=createAttachmentDoc();
    	attachmentCandidate=createAttachmentCandidate();
    	attachmentReq=createAttachmentReq();
    	document=createDocument();
    	ClickToolForm=createClickToolForm();
    	Interview=createInterview();
    	InterviewTracker=createInterviewTracker();   	
        ApexPages.currentPage().getParameters().put('Id', Candidate.Id);

		test.startTest();
        WCT_AttachmentZipController AZC = new WCT_AttachmentZipController();
        AZC.getAttachments();
        AZC.reSet();
        AZC.zipFileName='';
        Blob DocBody = Blob.ValueOf('Test Value for Document Body.');
        AZC.zipContent=EncodingUtil.base64Encode(DocBody);
		WCT_AttachmentZipController.getAttachment(attachmentReq.Id);
        AZC.uploadZip();
		test.stopTest();
    }
    static testMethod void withoutDocBody() {
    	userRecRecr=createUserRecr();
    	userRecRecCoo=createUserRecCoo();
    	userRecRecIntv=createUserIntv();
    	Requisition=createRequisition();
    	Candidate = createCandidate();
    	CandidateDocument=createCandidateDocument();
    	CandidateRequisition=createCandidateRequisition();
    	attachmentDoc=createAttachmentDoc();
    	attachmentCandidate=createAttachmentCandidate();
    	attachmentReq=createAttachmentReq();
    	document=createDocument();
    	ClickToolForm=createClickToolForm();
    	Interview=createInterview();
    	InterviewTracker=createInterviewTracker();   	
        ApexPages.currentPage().getParameters().put('Id', Candidate.Id);

		test.startTest();
        WCT_AttachmentZipController AZC = new WCT_AttachmentZipController();
        AZC.getAttachments();
        AZC.reSet();
        AZC.zipFileName='';
        Blob DocBody = Blob.ValueOf('');
        AZC.zipContent=EncodingUtil.base64Encode(DocBody);
		WCT_AttachmentZipController.getAttachment(attachmentReq.Id);
        AZC.uploadZip();
		test.stopTest();
    }    
    static testMethod void withoutCandReqCandDoc() {
    	userRecRecr=createUserRecr();
    	userRecRecCoo=createUserRecCoo();
    	userRecRecIntv=createUserIntv();
    	Requisition=createRequisition();
    	Candidate = createCandidate();
    	attachmentCandidate=createAttachmentCandidate();
    	document=createDocument();
    	ClickToolForm=createClickToolForm();
    	Interview=createInterview();
        ApexPages.currentPage().getParameters().put('Id', Candidate.Id);

		test.startTest();
        WCT_AttachmentZipController AZC = new WCT_AttachmentZipController();
        AZC.getAttachments();
        AZC.reSet();
        AZC.zipFileName='';
        Blob DocBody = Blob.ValueOf('Test Value for Document Body.');
        AZC.zipContent=EncodingUtil.base64Encode(DocBody);
		WCT_AttachmentZipController.getAttachment(attachmentCandidate.Id);
        AZC.uploadZip();
		test.stopTest();
    }
}