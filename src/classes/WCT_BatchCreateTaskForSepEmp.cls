global class WCT_BatchCreateTaskForSepEmp implements Database.Batchable<sObject>{
    //Variable Declaration
    
    
    public String query;
    public static final id leaveTaskRecTypeId = WCT_Util.getRecordTypeIdByLabel('Task','Leaves');
    
    //Constructor to prepare Query to fetch separated Contact Records
    global WCT_BatchCreateTaskForSepEmp(){
        query = 'SELECT id,WCT_Is_Separated__c FROM Contact where WCT_Is_Separated__c = true';
    }

    //Start Method ,it returns a list of contacts for which task needs to be created 
    global Database.QueryLocator start(Database.BatchableContext BC){
            return Database.getQueryLocator(query);
    }

    //Execute method to update contact's and create Tasks
    global void execute(Database.BatchableContext BC,List<Contact> ConList){
        set<id> empIdSet = new set<id>();
        set<id> lveIdToCreateTasks = new set<id>();
        Map<id,id> empLeaveIdMap = new Map<id,id>();
        List<Task> taskToCreate = new List<Task>();
        list<contact> liConToUpdateBatch = new list<contact>();
        for(contact con:ConList){
            empIdSet.add(con.id);
            con.WCT_Is_Separated__c = false;
            liConToUpdateBatch.add(con);
        }
        
        for(WCT_Leave__c lve:[SELECT id,WCT_Leave_Status__c,WCT_Employee__c,WCT_Related_Record__c,(SELECT id,WCT_Is_Separated__c,Status FROM Tasks 
                                WHERE WCT_Is_Separated__c = true AND status <> 'Completed') FROM WCT_Leave__c WHERE 
                                WCT_Employee__c IN :empIdSet AND WCT_Leave_Status__c <> 'Closed' AND WCT_Leave_Status__c <> 'Cancelled']){
            system.debug('entered'+lve.id);
            system.debug('enterednotasks'+lve.tasks);
            if(lve.WCT_Related_Record__c == null){
                if(lve.Tasks.isEmpty()){
                    system.debug('enterednotasks'+lve.tasks.size());
                    lveIdToCreateTasks.add(lve.id);
                    //empLeaveIdMap.put(lve.WCT_Employee__c,lve.id);
                }    
            }   
        }
        
        if(!lveIdToCreateTasks.isEmpty()){
            for(Id lveId:lveIdToCreateTasks){
                Task ta = new Task();
                ta.subject = 'Automated Task for open leaves of Separated Employee';
                ta.WCT_Is_Separated__c = true;
                ta.status = 'Not Started';
                ta.Ownerid = system.label.Leaves_User;
                ta.whatId = lveId;
                ta.RecordTypeId = leaveTaskRecTypeId;
                taskToCreate.add(ta);
            }
        }
        system.debug('sizeoftasks'+taskToCreate.size());
        if(!taskToCreate.isEmpty()){
            Database.SaveResult[] srList = Database.Insert(taskToCreate, false);
        }
        if(!liConToUpdateBatch.isEmpty()){
            Database.SaveResult[] srListCon = Database.update(liConToUpdateBatch, false);
        }
    
    }
    
    //Finish method to send email and do post execution task's on completion of batch
    global void finish(Database.BatchableContext BC ) {
        
    }

}