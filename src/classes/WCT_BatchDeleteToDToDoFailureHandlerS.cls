public class WCT_BatchDeleteToDToDoFailureHandlerS implements Schedulable {
     
    public void execute(SchedulableContext SC) {
        WCT_BatchDeleteToDToDoFailureHandler BTT =  new WCT_BatchDeleteToDToDoFailureHandler();
        Database.executeBatch(BTT, 10); 
    } 
}