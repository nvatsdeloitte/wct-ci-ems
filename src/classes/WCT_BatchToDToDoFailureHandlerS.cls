public class WCT_BatchToDToDoFailureHandlerS implements Schedulable {
     
    public void execute(SchedulableContext SC) {
        WCT_BatchToDToDoFailureHandler BTT =  new WCT_BatchToDToDoFailureHandler();
        Database.executeBatch(BTT, 10); 
    } 
}