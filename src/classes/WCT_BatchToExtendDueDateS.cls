public class WCT_BatchToExtendDueDateS implements Schedulable {
     
    public void execute(SchedulableContext SC) {
        WCT_BatchToExtendDueDate BTT =  new WCT_BatchToExtendDueDate();
        Database.executeBatch(BTT, 10); 
    } 
}