public class WCT_BatchUpdateToDToDoFailureHandler implements Database.Batchable<sObject>, Database.AllowsCallouts { 
    
    // Relation Records Failure Statuses
    private static final String[] FAILURE_STATUSES = new String[] {'Update Failed', 'Ready for Update'};    
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        // Pull the relation records with failure statuses
        String query = 'SELECT ' +
                            'Id, SFDC_Task_Id__c, ToD_TODO_Id__c, Status__c, Status_Details__c, WCT_Next_Due_Date__c ' +
                        'FROM ' +
                            'WCT_ToD_Task_ToDo_Relation__c ' +
                        'WHERE ' +
                            'Status__c in :FAILURE_STATUSES';
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<WCT_ToD_Task_ToDo_Relation__c> listFailedRecords) { 
        Map<String, WCT_ToD_Task_ToDo_Relation__c> mapUpdateFailedRecords = new Map<String, WCT_ToD_Task_ToDo_Relation__c>();
        List<String> listTaskIds = new List<String>();
        
        // Parse the relation-records and separate out the 'Delete Failed' records
        for(WCT_ToD_Task_ToDo_Relation__c failedRecord : listFailedRecords) {
            if((failedRecord.Status__c == 'Update Failed' || failedRecord.Status__c == 'Ready for Update') && failedRecord.ToD_TODO_Id__c != null) {
                mapUpdateFailedRecords.put(failedRecord.SFDC_Task_Id__c, failedRecord);
                listTaskIds.add(failedRecord.SFDC_Task_Id__c);
                system.debug('<<<<'+listTaskIds);
            }
        }
        
        // Pull all the required task-details from the DB
        Map<Id, Task> mapTasks = new Map<Id, Task>([SELECT Id, WCT_Is_Visible_in_TOD__c FROM TASK WHERE Id IN :listTaskIds]);
        
        if(0 < mapUpdateFailedRecords.size()) {
            processUpdateFailedRecords(mapUpdateFailedRecords, mapTasks);
        }
        
    }
    
    public void finish(Database.BatchableContext BC){    
    }
    
    private void processUpdateFailedRecords(Map<String, WCT_ToD_Task_ToDo_Relation__c> mapUpdateFailedRecords, Map<Id, Task> mapTasks) {
        List<WCT_ToD_Task_ToDo_Relation__c> listToBeDeleted = new List<WCT_ToD_Task_ToDo_Relation__c>();
        List<Id> listTaskIds = new List<String>();
        
        // Pick the ones for which the task is not yet complete and delete the others
        for(WCT_ToD_Task_ToDo_Relation__c failedRecord : mapUpdateFailedRecords.values()) {
            Task t = mapTasks.get(failedRecord.SFDC_Task_Id__c);
            if( t != null && (true == t.WCT_Is_Visible_in_TOD__c) ) {
                listTaskIds.add(t.id);
                System.debug('task id added'+listTaskIds);
            }
            else{
                listToBeDeleted.add(failedRecord);
            }        
        }
        System.debug('task id added'+listTaskIds);
        System.debug('task id to listtobedeleted'+listToBeDeleted);
        WCT_ToDToDoItemsManager.updateToDo(listTaskIds);        
    }

}