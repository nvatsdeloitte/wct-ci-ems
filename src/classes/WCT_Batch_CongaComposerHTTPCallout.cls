/**************************************************************************************
Apex Trigger Name:  WCT_Batch_CongaComposerHTTPCallout 
Version          : 1.0 
Created Date     : 04 April 2013
Function         : Batch class to  
                    -> To Send Bulk Offer Letters
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                   04/04/2013             Original Version
*************************************************************************************/

global class WCT_Batch_CongaComposerHTTPCallout implements Database.Batchable<String>, Database.AllowsCallouts {   

    List<String> lstBatchURL;
    
    /*********************************************************************************
    Method Name    : NVS_Batch_CongaComposerHTTPCallout
    Description    : Constructor
    Return Type    : void
    Parameter      : 1. lstURL: list of URL for Conga Composer HTTP Callouts                
    *********************************************************************************/
   
    global WCT_Batch_CongaComposerHTTPCallout(List<String> lstURL)
    {
        lstBatchURL = new List<String>();  
        lstBatchURL = lstURL;    
    }
    /*********************************************************************************
    Method Name    : start
    Description    : Start method to fetch the URLs
    Return Type    : Iterable<String>
    Parameter      : 1. BC : Context for the batch                
    *********************************************************************************/
   
    global Iterable<String> start(Database.BatchableContext BC){
        return lstBatchURL;
    }
    /*********************************************************************************
    Method Name    : execute
    Description    : Execute method to process the list of Conga Composer URLs in Batch
    Return Type    : void
    Parameter      : 1. BC : Context for the batch 
                     2. lstCongaURL : List of Conga Composer URLs               
    *********************************************************************************/
    global void execute(Database.BatchableContext BC,  List<String> lstCongaURL ){  
        for(String sURL : lstCongaURL )
        {        
            Http oHttp = new Http();          
            HttpRequest oHttpReq = new HttpRequest();            
            oHttpReq.setEndpoint(sURL);
            oHttpReq.setMethod('GET');
            oHttpReq.setTimeout(120000);
            HttpResponse oHttpRes;
            try
            {
                oHttpRes = oHttp.send(oHttpReq);
            }
            catch(CalloutException e)
            {
            // Do nothing for now
            	WCT_ExceptionUtility.logException('WCT_Batch_CongaComposerHTTPCallout-execute', null, e.getMessage() + ' ::: ' + e.getStackTraceString());
            }
        }   
        
    }
    /*********************************************************************************
    Method Name    : finish
    Description    : finish method 
    Return Type    : void
    Parameter      : 1. BC : Context for the batch                         
    *********************************************************************************/
    global void finish(Database.BatchableContext BC)
    {
    // Do nothing for now
    }

}