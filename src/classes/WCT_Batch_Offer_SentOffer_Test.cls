@isTest
private class WCT_Batch_Offer_SentOffer_Test
{
      public static testmethod void test1()
        {
            Contact con = WCT_UtilTestDataCreation.createContact();
            con.email='test@deloitte.com';
            con.WCT_Visa_Type__c='temp';
            con.MobilePhone='123';
            con.WCT_User_Group__c='United States';
            //con.WCT_Requisition_Number__c='Testing';
            con.WCT_Taleo_Status__c='Offer Details Updated';           
            insert con;
            con.WCT_Taleo_Status__c='Offer - To be Extended';
            update con;
            
                  
            
               WCT_Offer__c offerRec = new WCT_Offer__c();
               offerRec.WCT_Candidate__c = con.id;
               offerRec.WCT_status__c = 'Offer Approved';
               offerRec.WCT_Country__c ='India';
               offerRec.WCT_Lookup_Code__c = 'HYD3I330I08';
               offerRec.WCT_Sub_Area_Code__c = 'test';
               offerRec.WCT_Designation_Code__c = 'M';
               offerRec.WCT_Offer_Password_Sent__c = false;   
               offerRec.WCT_Offer_Letter_Sent__c = false;
               
               insert offerRec; 
                 
                  
                List<id> offerList = new List<id>();
               offerList.add(offerRec.id);
               system.debug('###### ' + offerList);
                system.assert(offerList.size() > 0);       
            Test.starttest();
            Test.setMock(HttpCalloutMock.class, new WCT_MockHttpResponseGenerator_Test());
            string sessionId = UserInfo.getSessionId();
            WCT_Batch_Offer_Status_SentOffer_Update controller = new WCT_Batch_Offer_Status_SentOffer_Update(sessionId);
            Database.executeBatch(controller);
            Test.stoptest();
        }
}