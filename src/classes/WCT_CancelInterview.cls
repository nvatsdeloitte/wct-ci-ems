/**
    * Class Name  : WCT_CancelInterview 
    * Description : Class will Send an email to all the interviwers on cancelation of interview. 
*/

global class WCT_CancelInterview {

    /** 
        Method Name  : sendCancelationEmail
        Return Type  : String
        Description  : Send Email Cancellation.        
    */
                    
    WebService static string sendCancelationEmail(string InterviewId)
    {
      
        List<String> CCAddressList=  new List<String>();
        string evDescription,  evLocation, interviewName, reqRecruiterName;    
        string  evEndDateTimeStr, evStartDateTimeStr, EventWhatId, EventId;
        datetime evStartDateTime;
        Integer tempSeq ; //to store sequence of event 
        set<Id> InvitIds = new set<id>();
        Set<String> declinedEmails = new Set<String>();
        list<WCT_Interview_Junction__c> InterviewerJunc = new list<WCT_Interview_Junction__c>();
        list<WCT_Interview_Junction__c> InterviewerEmail = new list<WCT_Interview_Junction__c>();
        List<Messaging.EmailFileAttachment> AttachmentListInvites =  new List<Messaging.EmailFileAttachment>();
        
            list<EventRelation> eventDis= [SELECT EventId,IsInvitee,RelationId,Relation.email,Status, Event.EndDateTime, Event.What.Name, 
                                    Event.StartDateTime, Event.WhatId ,Event.owner.email,Event.subject,Event.WCT_Invite_Sequence__c
                                    FROM EventRelation 
                                    WHERE Event.WhatId  = :InterviewId and IsInvitee=true
                                    and Relation.type = 'User' and Event.What.Type = 'WCT_Interview__c'];
            if(!eventDis.IsEmpty()){
                if(eventDis[0].Event.EndDateTime!=null)evEndDateTimeStr = DateFormatConv(eventDis[0].Event.EndDateTime); 
                if(eventDis[0].Event.StartDateTime!=null)evStartDateTimeStr = DateFormatConv(eventDis[0].Event.StartDateTime); 
                if(eventDis[0].Event.StartDateTime!=null)evStartDateTime = eventDis[0].Event.StartDateTime;
                //if(eventDis[0].Event.WhatId!=null)EventWhatId = eventDis[0].Event.WhatId;
                if(eventDis[0].Event.What.Name!=null)interviewName = eventDis[0].Event.What.Name;
                if(eventDis[0].EventId!=null)EventId= eventDis[0].EventId;
                if(eventDis[0].Event.WCT_Invite_Sequence__c!=null) {
                    tempSeq = Integer.valueOf(eventDis[0].Event.WCT_Invite_Sequence__c); 
                 }else{tempSeq = 0;}
              //  if(eventDis[0].Event.subject!=null)emailSubject = eventDis[0].Event.Subject;
               
                system.debug('Event id'+EventId);
                for(EventRelation ER: eventDis){
                        InvitIds.add(ER.RelationId);
                        if(ER.Status == 'Declined'){declinedEmails.add(ER.Relation.email);}
                }
                set<string> ReqRecruiterEmail =  new set<string> ();
                for(WCT_Interview_Junction__c intJun : [SELECT WCT_Interviewer__r.Email, WCT_Interviewer__r.Name, WCT_Requisition_RecruiterEmail__c, WCT_Requisition_RC_Email__c,
                            WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiter__r.Email, WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Email 
                                                        from WCT_Interview_Junction__c 
                                                        where WCT_Interview__c = :InterviewId and WCT_Interviewer__c in :InvitIds])
                {
                    InterviewerEmail.add(intJun);   
                    
                /*    
                    
                   if(intJun.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiter__r.Name!=null)
                        reqRecruiterName = intJun.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiter__r.Name;
                    if(intJun.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiter__r.Email!=null)
                        ReqRecruiterEmail.add(intJun.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiter__r.Email);
                   if(intJun.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Email!=null)
                        ReqRecruiterEmail.add(intJun.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Email);
                        
                }
                if(!ReqRecruiterEmail.IsEmpty()){CCAddressList.addAll(ReqRecruiterEmail);}
                    */
                    
                }
                
                
            }
           String emailSubject = 'Cancel:'+Label.Finalize_Interview_Email_Subject+interviewName;
            String fromEmailAddress = Label.Finalize_Interview_Reply_Email_Id;
            if(!InterviewerEmail.IsEmpty()){
                set<string> Interviwers = new set<string>();
                for (WCT_Interview_Junction__c toAdd : InterviewerEmail){
                    Messaging.SingleEmailMessage emailOut = new Messaging.SingleEmailMessage();   
                    AttachmentListInvites =  new List<Messaging.EmailFileAttachment>();
                    Messaging.EmailFileAttachment efaInvite = new Messaging.EmailFileAttachment();
                    
                    Set<String> CCAddressSet=  new Set<String>();
                    
                    if(eventDis[0].Event.Owner.Email!=null)
                       {
                            CCAddressSet.add(eventDis[0].Event.Owner.Email);
                       }
                    
                    if(toAdd.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiter__r.Email!=null)
                       {
                              CCAddressSet.add(toAdd.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiter__r.Email);
                      
                       } 
                     if(toAdd.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Email!=null)
                       {
                              CCAddressSet.add(toAdd.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Email);
                       }
                                          
                   
                       if(CCAddressSet.size()>0)
                       {
                          CCAddressList.addAll(CCAddressSet);
                          emailOut.setCcAddresses(CCAddressList);
                       }
                    
                    
                    efaInvite.setFileName('Cancel: '+Label.Finalize_Interview_Calender_Invite_Name+'.ics');
                    Blob b = doIcsAttachment(EventId, evStartDateTimeStr, evEndDateTimeStr, emailSubject, 
                                fromEmailAddress, toAdd.WCT_Interviewer__r.Email, toAdd.WCT_Interviewer__r.Name, interviewName, evStartDateTime, CCAddressList,tempSeq,InterviewId );  
                    
                    efaInvite.setBody(b);
                    efaInvite.setContentType('text/calendar');
                    efaInvite.setInLine(true);

                    AttachmentListInvites.add(efaInvite); 
                    
                  
                    //emailOut.setTargetObjectId(userinfo.getUserId());
                    emailOut.setTargetObjectId(toAdd.WCT_Interviewer__c);
                    emailOut.setFileAttachments(AttachmentListInvites);
                    emailOut.saveAsActivity = false;      
                    string htmlBodyTxt=EmailHTMLBody(toAdd.WCT_Interviewer__r.Name, interviewName, evStartDateTime,InterviewId);
                    emailOut.setHtmlBody(htmlBodyTxt);
                    emailOut.setSubject(emailSubject); 
                    //emailOut.setToAddresses(new String[] {toAdd.WCT_Interviewer__r.Email});
                    emailOut.setInReplyTo(fromEmailAddress);
                    if(!Interviwers.contains(toAdd.WCT_Interviewer__r.Email)){
                        if(!declinedEmails.contains(toAdd.WCT_Interviewer__r.Email)){
                        Messaging.SendEmailResult[] mailResults = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{emailOut});
                        }
                    }
                    Interviwers.add(toAdd.WCT_Interviewer__r.Email);
                    
                    toAdd.WCT_Interview_Junction_Status__c=WCT_UtilConstants.Interview_Canceled;
                    toAdd.WCT_IEF_Submission_Status__c=WCT_UtilConstants.Interview_Canceled;
                    InterviewerJunc.add(toAdd);
                }
                
            if(!InterviewerJunc.IsEmpty()){
                try{
                    update InterviewerJunc;
                    Event eve = new Event(id=EventId);//Rakesh 
                    eve.WCT_Invite_Sequence__c = tempSeq + 1;
                    update eve;
                }
                catch(DMLException ex){
                    WCT_ExceptionUtility.logException('WCT_CancelInterview-sendCancelationEmailIntJunction', null, ex.getMessage() + ' ::: ' + ex.getStackTraceString());
                }  
            }
                return 'true';
            }
            else{
                return 'false';
            }
    }

    /** 
        Method Name  : sendCancelationEmailIntJunction
        Return Type  : String
        Description  : Update Interview Junction status.        
    */

    WebService static string sendCancelationEmailIntJunction(list<string> InterJunctionIds)
    {
        list<WCT_Interview_Junction__c> InterviewerJunc = new list<WCT_Interview_Junction__c>();
           
            for (WCT_Interview_Junction__c InterJunctionRec : [SELECT WCT_Interview_Junction_Status__c 
                                                    from WCT_Interview_Junction__c 
                                                    where Id in :InterJunctionIds]){
                InterJunctionRec.WCT_Interview_Junction_Status__c=WCT_UtilConstants.Interview_Canceled;
                InterJunctionRec.WCT_IEF_Submission_Status__c=WCT_UtilConstants.Interview_Canceled;
                InterviewerJunc.add(InterJunctionRec);
            }
            if(!InterviewerJunc.IsEmpty()){
                try{
                    update InterviewerJunc;
                }
                catch(DMLException ex){
                    WCT_ExceptionUtility.logException('WCT_CancelInterview-sendCancelationEmailIntJunction', null, ex.getMessage() + ' ::: ' + ex.getStackTraceString());
                }  
            }
    return Label.WCT_InterJunctionCanceled;
    }
/**********************Date to GMT convetion****************/    
    private static String DateFormatConv(DateTime dt){
        String myDate = string.valueOfGMT(dt);
        String stringDate = myDate.substring(0,4) + '' +
                            myDate.substring(5,7) + '' + 
                            myDate.substring(8,10) + '' +
                            myDate.substring(10,10) + 'T' +
                            myDate.substring(11,13) + '' +
                            myDate.substring(14,16)+ ''+ 
                            myDate.substring(17,19)+ 'Z';
       return stringDate;
    }
/********************OutLook Invite ******************************/
    private static Blob doIcsAttachment(string EventId, String startDate, String endDate, 
                                        String subject, String fromAddress, String toAddress, string interviewerName, string interviewName, Datetime evStartDateTime, list<String> ccAddress,Integer seq,string InterviewId) {

List<user> userList = [select id,name,email from user where email IN :ccAddress OR email = :toAddress];
    Map<string,string> inviteeEmailsMap = new Map<string,string>(); 
    for(user u: userList){
        inviteeEmailsMap.put(u.email,u.name);
    }
    string Attendees = '';
        
    for(String str : inviteeEmailsMap.keyset()){
        if(str != toAddress){
        Attendees += 'ATTENDEE;CN="' + inviteeEmailsMap.get(str)+'";ROLE=OPT-PARTICIPANT;RSVP=TRUE:mailto:' +str+'\n';
        }
    }

        // Now Contruct the ICS file
        string body=EmailHTMLBody(interviewerName, interviewName, evStartDateTime,InterviewId);
        String location='';
        String [] icsTemplate = new List<String> {  'BEGIN:VCALENDAR',
                                                    'PRODID:-//Microsoft Corporation//Outlook 14.0 MIMEDIR//EN',
                                                    'VERSION:2.0',
                                                    'METHOD:CANCEL',
                                                    'BEGIN:VEVENT',
                                                    'DTSTART: ' + startDate,
                                                    'DTSTAMP: '+ startDate,
                                                    'DTEND: ' + endDate,
                                                    'LOCATION: ' + location,
                                                    'UID:' + EventId,
                                                    'SEQUENCE:'+seq,
                                                    //'UID: ' + String.valueOf(Crypto.getRandomLong()),
                                                    'DESCRIPTION: ' + body,
                                                    'X-ALT-DESC;FMTTYPE=text/html: ' + body,
                                                    'SUMMARY: ' + subject,
                                                    'ORGANIZER:MAILTO: ' + fromAddress,
                                                    'ATTENDEE;CN="' + inviteeEmailsMap.get(toAddress) + '";RSVP=TRUE:mailto:' + toAddress,
                                                     Attendees,
                                                   // 'ATTENDEE;CN="' + toAddress + '";RSVP=TRUE:mailto: ' + toAddress,
                                                    //'ATTENDEE;CN="' + userInfo.getName()+ '";ROLE=OPT-PARTICIPANT;RSVP=TRUE:mailto: ' + userinfo.getUserEmail(),
                                                    //'UID:'+ EventId,
                                                    //'X-ALT-DESC;FMTTYPE=text/html:'+ EncodingUtil.urlDecode(body, 'UTF-8'),
                                                    'BEGIN:VALARM',
                                                    'TRIGGER:-PT15M',
                                                    'ACTION:DISPLAY',
                                                    'DESCRIPTION:Reminder',
                                                    'END:VALARM', 
                                                    'END:VEVENT',
                                                    'END:VCALENDAR'
                                                 };
        String attachment = String.join(icsTemplate, '\n');
        return Blob.valueof(attachment ); 

    }    
    private static String EmailHTMLBody(string interviewerName, string interviewName,Datetime evStartDateTime,string InterviewId){
        string Emailhtml = '<html>'+
                                '<body>'+
                                    '<p>'+'<img src="'+Label.DeloitteLogoURL+'" height="90" width="100%"></img><Br><Br>'+
                                    'Dear '+interviewerName+',<br/><br/>'+ 
                                    'Your interview that was scheduled to take place on '+evStartDateTime.format('MM dd, yyyy hh:mm:ss a')+
                                    ' has been canceled.  <br/>We apologize for the inconvenience, but appreciate your willingness to support Deloitte’s Recruiting efforts.'+
                                    '<br/>We will let you know if there are any future interviews to be scheduled.<br/><br/>'+candTable(InterviewId)+
                                    'Thanks,<br/>'+
                                    'Deloitte Recruiting'+
                                '</body>'+
                            '</html>';
                                    
    return Emailhtml;
    }
    public static string candTable(string intvId){
        string htmlTable = '';
        Set<Id> candTrakIdSet = new Set<Id>();
        for(WCT_Interview_Junction__c ij:[SELECT id,WCT_Candidate_Tracker__r.WCT_Contact__r.name,WCT_Candidate_Tracker__r.WCT_Requisition__r.name 
                                            FROM WCT_Interview_Junction__c WHERE WCT_Interview__c =:intvId]){
            candTrakIdSet.add(ij.WCT_Candidate_Tracker__c);
        }
        if(!candTrakIdSet.isEmpty()){
            integer count = 0;
            
            for(WCT_Candidate_Requisition__c ct:[SELECT id,WCT_Contact__r.name,WCT_Requisition__r.name,WCT_Email__c,WCT_RMS_Taleo_ID__c FROM WCT_Candidate_Requisition__c 
                                                    WHERE Id IN :candTrakIdSet]){
                //count++;                                    
                if(htmlTable == ''){
                    htmlTable= 'Interview was scheduled with following candidates: <ul><li>'+ct.WCT_Contact__r.name+'</li>';
                 
                }else{
                    htmlTable += '<li>'+ct.WCT_Contact__r.name+'</li>';
                }            
			}
            /*if(htmlTable == ''){
                htmlTable += '</ul>';
            }*/
            if(htmlTable <> ''){
            htmlTable += '</ul>';
        }  
        }
      
        
        return htmlTable;
    }
}