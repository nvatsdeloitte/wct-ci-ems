/**  
    * Class Name  : WCT_CancelSaveTheDateEvent 
    * Description : This apex class is used to send Cancel Invite for Save the Date Event
*/
global class WCT_CancelSaveTheDateEvent{
    
    /** 
        Method Name  : sendCancelationInvite
        Return Type  : String
        Description  : Method called from on-click button on event  to send cancel invite       
    */
    
    webservice static string sendCancelationInvite(string eventId){
        String result,evDescription,evEndDateTimeStr,evLocation,evStartDateTimeStr,emailSubject,subject;
        string evId = eventId;
        DateTime evStartDateTime;
        List<EventRelation> InterviewersInEvent = new List<EventRelation>();
        string userTimeZone,usertimeZoneVal;
        Map<string,string>timezonMap = new Map<string,string>
        {
        'America/Puerto_Rico' => 'AST',
        'America/Denver' => 'MST',
        'America/Phoenix' => 'MST',
        'America/New_York' => 'EST',
        'America/Panama' => 'EST',
        'America/Chicago' => 'CST',
        'America/Indiana/Indianapolis' => 'EST',
        'America/Mexico_City' => 'CST',
        'America/Los_Angeles' => 'PST',
        'Asia/Kolkata' => 'IST',
        'America/Adak' => 'HAST',
        'America/Anchorage' => 'AKST',
        'America/Argentina/Buenos_Aires' => 'ART',
        'America/Bogota' => 'COT',
        'America/Caracas' => 'VET',
        'America/El_Salvador' => 'CST',
        'America/Halifax' => 'AST',
        'America/Lima' => 'PET',
        'America/Santiago' => 'CLT',
        'America/Sao_Paulo' => 'BRT',
        'America/Scoresbysund' => 'EGT',
        'America/St_Johns' => 'NST',
        'America/Tijuana' => 'PST'
        };  
        
         User u = [select timezonesidkey from user where id=:UserInfo.getUserId()];
         if(u.TimeZoneSidKey!=null)userTimeZone = u.TimeZoneSidKey;
        
        if(timezonMap.get(userTimeZone)!=null){
            usertimeZoneVal=timezonMap.get(userTimeZone);
        }else{usertimeZoneVal = '';}  
        system.debug('^^^'+usertimeZoneVal);    
    
        Map<string,string> inviteeEmailMap = new Map<string,string>();
        InterviewersInEvent = [SELECT AccountId,EventId,IsInvitee,RelationId,relation.email,relation.name,Status, Event.Description, Event.EndDateTime, 
                                    Event.Location,  Event.StartDateTime, Event.What.Name, Event.IsDateEdited__c, Event.Subject,Event.WCT_Save_The_Date_EventId__c 
                                    FROM EventRelation WHERE EventId = :evId and IsInvitee=true and Relation.type = 'User' ];
                                    
        if(!InterviewersInEvent.isEmpty()){
                result = 'true';
                if(InterviewersInEvent[0].Event.Description!=null)evDescription = InterviewersInEvent[0].Event.Description; 
                if(InterviewersInEvent[0].Event.EndDateTime!=null)evEndDateTimeStr = DateFormatConv(InterviewersInEvent[0].Event.EndDateTime); 
                if(InterviewersInEvent[0].Event.Location!=null)evLocation = InterviewersInEvent[0].Event.Location; 
                if(InterviewersInEvent[0].Event.StartDateTime!=null)evStartDateTimeStr = DateFormatConv(InterviewersInEvent[0].Event.StartDateTime); 
                if(InterviewersInEvent[0].Event.StartDateTime!=null)evStartDateTime = InterviewersInEvent[0].Event.StartDateTime;
                if(InterviewersInEvent[0].Event.subject!=null)emailSubject = InterviewersInEvent[0].Event.Subject;
                subject = emailSubject +'[Id: '+InterviewersInEvent[0].Event.WCT_Save_The_Date_EventId__c+']';
                
        }else{result = 'false';}
        
        if(!InterviewersInEvent.isEmpty()){
                for(EventRelation er : InterviewersInEvent){
                    inviteeEmailMap.put(er.relation.email,er.relation.Name);
                }
                
                string htmlBody =  '<html>'+
                                '<body>'+
                                    '<p>'+'<img src="'+Label.DeloitteLogoURL+'" height="90" width="100%"></img><Br><Br>'+
                                    'Dear Interviewer,<br/><br/>'+ 
                                    'The interview date that was saved on your calendar for '+evStartDateTime.format('MM/dd/yyyy HH:mm:ss a')+ ' ' +usertimeZoneVal +
                                    ' has been canceled.  <br/>We apologize for the inconvenience, but appreciate your willingness to support Deloitte’s Recruiting efforts.'+
                                    '<br/>We will let you know if there are any future interviews to be scheduled.<br/><br/>'+
                                    'Thanks,<br/>'+
                                    'Deloitte Recruiting'+
                                '</body>'+
                            '</html>';
                List<string> fromAdd = new List<string>();
                 fromAdd.AddAll(inviteeEmailMap.keySet());
                Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();  
                msg.setReplyTo(system.label.WCT_Save_The_Date_Email_Id);
                msg.setSenderDisplayName('eInterview');
                msg.setToAddresses(fromAdd);
                msg.setCcAddresses(new String[] {userinfo.getUserEmail()});// it is optional, only if required
                msg.setSubject(subject);
                msg.setHtmlBody(htmlBody);

                // Create the attachment
                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                efa.setFileName('Meeting.ics' );
                string BodyFromPage = htmlBody;
                Blob b = doIcsAttachment(evId , evStartDateTimeStr,evEndDateTimeStr,evLocation, BodyFromPage, 
                                        subject, system.label.WCT_Save_The_Date_Email_Id, 'eInterview',
                                        evStartDateTime, evLocation,inviteeEmailMap);
                efa.setBody(b);
                efa.setInLine(true);//New
                efa.setContentType('text/Calendar');
                msg.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
                // Send the email you have created.
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { msg });
                Event ev = new Event(id=evId);
                ev.WCT_Event_Status__c = 'Cancelled';
                Update ev;
        
        }
        return result;
    
    
    }

    
     /**********************Date to GMT convetion****************/    
    private static String DateFormatConv(DateTime dt){
        String myDate = string.valueOfGMT(dt);
        String stringDate = myDate.substring(0,4) + '' +
                            myDate.substring(5,7) + '' + 
                            myDate.substring(8,10) + '' +
                            myDate.substring(10,10) + 'T' +
                            myDate.substring(11,13) + '' +
                            myDate.substring(14,16)+ ''+ 
                            myDate.substring(17,19)+ 'Z';
       return stringDate;
    }
    //Construction of ICS Invite
    private static Blob doIcsAttachment(String EventId, String startDate, String endDate, String location, String body, 
                                        String subject, String fromAddress, String displayName, 
                                        Datetime evStartDateTime, String evLocation,Map<string,string> inviteeEmails) {
        Map<string,string> inviteeEmailsMap = inviteeEmails;
        string Attendees = '';
        system.debug('@@@'+EventId);
        for(String str : inviteeEmailsMap.keyset()){
            Attendees += 'ATTENDEE;CN="' + inviteeEmailsMap.get(str)+'";ROLE=REQ-PARTICIPANT;RSVP=TRUE:mailto: ' +str+'\n';
        }
        system.debug('%%%1'+Attendees );
       // Attendees = Attendees.substring(0,(Attendees.length()-2));
       // system.debug('%%%'+Attendees );
        if (location== null){location='';}
        String [] icsTemplate = new List<String> {  
            'BEGIN:VCALENDAR',
            'PRODID:-//Microsoft Corporation//Outlook 14.0 MIMEDIR//EN',
            'VERSION:2.0',
            'METHOD:CANCEL',
            //'X-MS-OLK-FORCEINSPECTOROPEN:TRUE',
            //'BEGIN:VTIMEZONE',
            //'TZID:Central Standard Time',
            //'BEGIN:STANDARD',
            //'DTSTART:16011104T020000',
            //'RRULE:FREQ=YEARLY;BYDAY=1SU;BYMONTH=11',
            //'TZOFFSETFROM:-0500',
            //'TZOFFSETTO:-0600',
            //'END:STANDARD',
            //'BEGIN:DAYLIGHT',
            //'DTSTART:16010311T020000',
            //'RRULE:FREQ=YEARLY;BYDAY=2SU;BYMONTH=3',
            //'TZOFFSETFROM:-0600',
            //'TZOFFSETTO:-0500',
            //'END:DAYLIGHT',
            //'END:VTIMEZONE',
            'BEGIN:VEVENT',
            'CLASS:PUBLIC',
            'CREATED:20140217T210432Z',
            'DESCRIPTION: '+ body,
            'DTSTART:'+startDate,
            'DTSTAMP:'+startDate,
            'DTEND:'+endDate,
            'LAST-MODIFIED:20140217T210432Z',
            'LOCATION:'+ location,
            //'PRIORITY:5',
            'SEQUENCE:10',
            'SUMMARY;LANGUAGE=en-us:'+ subject,
            'TRANSP:OPAQUE',
            'ORGANIZER;CN=eInterview:MAILTO:' + fromAddress,
            Attendees,
            'ATTENDEE;CN="' + userInfo.getName()+ '";ROLE=OPT-PARTICIPANT;RSVP=TRUE:mailto: ' + userinfo.getUserEmail(),
            'UID:'+ EventId,
            'STATUS:CANCELLED',
            'X-ALT-DESC;FMTTYPE=text/html:'+ body,
            'X-MICROSOFT-CDO-BUSYSTATUS:BUSY',
            'X-MICROSOFT-CDO-IMPORTANCE:1',
            'X-MICROSOFT-DISALLOW-COUNTER:FALSE',
            'X-MS-OLK-AUTOFILLLOCATION:FALSE',
            'X-MS-OLK-CONFTYPE:0',
            'BEGIN:VALARM',
            'TRIGGER:-PT30M',
            'ACTION:DISPLAY',
            'DESCRIPTION:Reminder',
            'END:VALARM',
            'END:VEVENT',
            'END:VCALENDAR'
        };
        String attachment = String.join(icsTemplate, '\n');
        system.debug('%%%'+icsTemplate);
        return Blob.valueof(attachment ); 

    }


}