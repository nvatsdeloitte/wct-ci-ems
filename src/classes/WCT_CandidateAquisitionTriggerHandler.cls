/**************************************************************************************
Apex Trigger Name: WCT_CandidateAquisitionTriggerHandler
Version          : 1.0 
Created Date     : 19 November 2013
Function         : This class would be called by WCT_CandidateAquisitionTrigger to implement below functionalities on Interview
                   1.Populate Owner and recommended Level fields before insertion 
                   2.Cancel Interviews on Candidate Aqisition Cancelation
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                    19/11/2013              Original Version
*************************************************************************************/
public class WCT_CandidateAquisitionTriggerHandler{
    Public Static final String OPEN_STATUS='open';
    Public Static final String CANCEL_STATUS='Canceled';
    
     /*
     * Method name  : populateFieldsonCreation
     * Description  : Populate Owner of Candidate Aquisition with the Recruiting cordinator of Requisition
                      Populate Recommended level with the Candidate hiring level
     * Return Type  : Nil
     * Parameter    : New Candidate Aquisitions list
     */
    public static void populateFieldsonCreation(List<WCT_Candidate_Requisition__c> lstNewCandidateReqs){      
               
        for(WCT_Candidate_Requisition__c newCandidateReq:lstNewCandidateReqs){
        
            if(newCandidateReq.Requisition_RC_R__c != Null)
               newCandidateReq.OwnerId =newCandidateReq.Requisition_RC_R__c; 
            if(newCandidateReq.WCT_Recommended_Level__c == Null && newCandidateReq.WCT_Hiring_Level__c != null)
                newCandidateReq.WCT_Recommended_Level__c= newCandidateReq.WCT_Hiring_Level__c;
             
            }
    }
    
    /*
     * Method name  : UpdateInterviewsOnAquisitioncancellation
     * Description  : if Candidate Aquisition is canceled then related Interviews which are in Open state should be canceled
     * Return Type  : Nil
     * Parameter    : New Candidate Aquisitions map
     */
    public static void UpdateInterviewsOnAquisitioncancellation(Map<Id,WCT_Candidate_Requisition__c> mapNewCandidateAuisitions){
    /* Candidate Requisition is removed from Interview     
        List<WCT_Interview__c> lstOpenInterviews =new list<WCT_Interview__c>();
        try{
        //getting the open interview which are related with the Candidate Aquisitions
        lstOpenInterviews=[select id,WCT_Candidate_Requistion__c,WCT_Interview_Status__c from WCT_Interview__c
                                                      where WCT_Candidate_Requistion__c in :mapNewCandidateAuisitions.keySet() and
                                                      WCT_Interview_Status__c = :OPEN_STATUS limit :Limits.getLimitQueryRows()];
         }Catch(QueryException queryException)
         {
              WCT_ExceptionUtility.logException('WCT_CandidateAquisitionTriggerHandler','UpdateInterviewsOnAquisitioncancellation',queryException.getmessage());
            
         }
        
        List<WCT_Interview__c> lstCancelNeededInterviews= new List<WCT_Interview__c>();
       
        for(WCT_Interview__c OpenInterview:lstOpenInterviews){
            
            //checking if the related Candidate Aquisition is canceled ,if its canceled then adding the open Interviews to canceled list
            if(mapNewCandidateAuisitions.get(OpenInterview.WCT_Candidate_Requistion__c).WCT_Candidate_Requisition_Status__c == CANCEL_STATUS){
                OpenInterview.WCT_Interview_Status__c=mapNewCandidateAuisitions.get(OpenInterview.WCT_Candidate_Requistion__c).WCT_Candidate_Requisition_Status__c;
                lstCancelNeededInterviews.add(OpenInterview);
            }
        }
        
        //updating the Interviews to Cancel
        if(!lstCancelNeededInterviews.isEmpty()){
            
              Database.SaveResult[] srList=Database.update(lstCancelNeededInterviews);
                // Iterate through each returned result
                for (Database.SaveResult sr : srList) {
                    if (!sr.isSuccess()) {
                        String ErrorMessage='';
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors()) {
                            ErrorMessage=ErrorMessage+err.getStatusCode() + ': ' + err.getMessage()+'';                                                
                        }
                        WCT_ExceptionUtility.logException('WCT_CandidateAquisitionTriggerHandler','UpdateInterviewsOnAquisitioncancellation', ErrorMessage);
                        mapNewCandidateAuisitions.get(Sr.getId()).addError('Record is not updated,please contact System Admin. Error Code:'+ ErrorMessage); 
                    }
               }
        }*/
    }

}