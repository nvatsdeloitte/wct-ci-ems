@isTest
public class WCT_CaseEdit_Controller_Test {
    static testMethod void test(){
        Test_Data_Utility.createCase();
        Case ca = [Select id,status,Unread_Email_on_Closed_Case__c,WCT_ResolutionNotes__c from case limit 1];
        PageReference pageRef = Page.WCT_CaseEdit_Test;      
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', ca.id);
        WCT_CaseEdit_Controller ctrl = new WCT_CaseEdit_Controller();
        ctrl.getCase();
        ctrl.save();
    }
}