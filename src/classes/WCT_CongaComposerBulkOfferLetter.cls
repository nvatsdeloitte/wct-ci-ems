/**************************************************************************************
Apex Trigger Name:  WCT_CongaComposerBulkOfferLetter
Version          : 1.0 
Created Date     : 04 April 2013
Function         : Handler class to  
                    -> To Send Bulk Offer Letters
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                   04/04/2013             Original Version
*************************************************************************************/

global class WCT_CongaComposerBulkOfferLetter{ 

  webService static void GenerateBulkOfferLetter(List<Id> offerList) { 
  
        String sSessionID = UserInfo.GetSessionId();
        String sOrgID = UserInfo.getOrganizationId().substring(0,15);
        Id userID = UserInfo.getUserId();
        
        String sCongaUrl = 'https://www.appextremes.com/apps/Conga/PM.aspx?sessionId=';
        List<String> urls = new List<String>();
        Id QueryId = label.WCT_Conga_Query_Id;
        for (WCT_Offer__c offer : [Select id, WCT_First_Name__c , WCT_Last_Name__c , WCT_Candidate__c, Offer_Letter_Template_Id__c from WCT_Offer__c where id in : offerList]){
            Id TemplateId = offer.Offer_Letter_Template_Id__c;
            String offerId = offer.ID;
            offerId = offerId.substring(0,15);
            String docUrl = sCongaUrl
                        + sSessionID
                        + '&serverUrl='
                        + EncodingUtil.urlEncode( URL.getSalesforceBaseUrl().toExternalForm(), 'UTF-8')
                        + '%2Fservices%2FSoap%2Fu%2F9.0%2F'
                        + sOrgID
                        + '&Id='
                        + offerId
                        + '&TemplateId='
                        + TemplateId 
                        + '&QueryId=[Contact]'
                        + QueryId
                        + '&DefaultPDF=1'
                        + '&OFN=Deloitte_Offer_Letter'
                        + '&ESVisible=1'
                        + '&ESExpireInXDays=120' // MUNNANGI; 08/25/2014 ; Changed from 7 to 120 Days
                        + '&ESMessage=Please_sign_the_attached_document_from_Deloitte'
                        + '&ESRemindRecipient=1' // MUNNANGI; 08/25/2014 ;Changed from 2(every day) to 1 (Never)
                        + '&ESContactId='
                        + offer.WCT_Candidate__c
                        + '&ESAgreementName=Deloitte' + '+' + 'Offer'
                        + '&ESSignatureType=2'
                        + '&ESCustomField=WCT_Offer__c'
                        + '&ESCustomFieldValue='
                        + offerId
                        + '&ESSVBA=1&DS7=14';
            urls.add(docUrl);
            System.debug('<<<<<<<'+docUrl);
        }       
        Database.executebatch(new  WCT_Batch_CongaComposerHTTPCallout(urls),5); 
    }
    
    webService static void AttachBulkOfferLetter(List<Id> offerList) { 
  
        String sSessionID = UserInfo.GetSessionId();
        String sOrgID = UserInfo.getOrganizationId().substring(0,15);
        Id userID = UserInfo.getUserId();
        
        String sCongaUrl = 'https://www.appextremes.com/apps/Conga/PM.aspx?sessionId=';
        List<String> urls = new List<String>();
        Id QueryId = label.WCT_Conga_Query_Id;
        for (WCT_Offer__c offer : [Select id, WCT_First_Name__c , WCT_Last_Name__c , WCT_Candidate__c, Offer_Letter_Template_Id__c from WCT_Offer__c where id in : offerList]){
            String First_Name = offer.WCT_First_Name__c;
            String Last_Name = offer.WCT_Last_Name__c;
            First_Name = First_Name.replaceAll('\\s+','');
            First_Name = First_Name.replaceAll(' ','');
            Last_Name = Last_Name.replaceAll('\\s+','');
            Last_Name = Last_Name.replaceAll(' ','');
            Id TemplateId = offer.Offer_Letter_Template_Id__c;
            String offerId = offer.ID;
            offerId = offerId.substring(0,15);
            String docUrl = sCongaUrl
                        + sSessionID
                        + '&serverUrl='
                        + EncodingUtil.urlEncode( URL.getSalesforceBaseUrl().toExternalForm(), 'UTF-8')
                        + '%2Fservices%2FSoap%2Fu%2F9.0%2F'
                        + sOrgID
                        + '&Id='
                        + offerId
                        + '&TemplateId='
                        + TemplateId 
                        + '&QueryId=[Contact]'
                        + QueryId
                        + '&DefaultPDF=1'
                        + '&LG3=2'
                        + '&OFN=Draft_Offer_For_'
                        + First_Name 
                        + '_'
                        + Last_Name
                        + '&DS7=1';
            urls.add(docUrl);
            System.debug('<<<<<<<'+docUrl);
        }       
        Database.executebatch(new  WCT_Batch_CongaComposerHTTPCallout(urls),5); 
    }
}