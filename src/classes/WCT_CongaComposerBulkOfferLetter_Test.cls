@isTest
private class WCT_CongaComposerBulkOfferLetter_Test
{
        
        public static testmethod void test1()
        {
        Contact con=WCT_UtilTestDataCreation.createContact();
        con.email='test@deloitte.com';
        con.WCT_Visa_Type__c='temp';
        con.MobilePhone='123';
        con.WCT_User_Group__c='Test';
        //con.WCT_Requisition_Number__c='Testing';
        con.WCT_Taleo_Status__c='Offer Details Updated';           
        insert con;
        con.WCT_Taleo_Status__c='Offer - To be Extended';
        update con;
        WCT_Offer__c offer = [select id, WCT_status__c,WCT_Candidate__c,WCT_Address1_INTERN__c,WCT_Candidate_Tracker__c,WCT_Type_of_Hire__c from WCT_Offer__c where WCT_Candidate__c=:con.id];
        List<id> offerList =new List<id>();
        offerList.add(offer.id);
        APXTConga4__Conga_Merge_Query__c testquery = new APXTConga4__Conga_Merge_Query__c();
        testquery.APXTConga4__Name__c ='WCT_Offer_Letter';
        insert testquery;
        Test.starttest();
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new WCT_MockHttpResponseGenerator_Test());
        WCT_CongaComposerBulkOfferLetter controller = new WCT_CongaComposerBulkOfferLetter(); 
        WCT_CongaComposerBulkOfferLetter.GenerateBulkOfferLetter(offerList);
        Test.stoptest();
        }
}