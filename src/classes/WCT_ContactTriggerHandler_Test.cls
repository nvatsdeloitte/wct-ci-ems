@isTest

private class WCT_ContactTriggerHandler_Test
{
    


static testMethod void Test() 
{
    test.startTest();
    
    Contact adhocContact= WCT_UtilTestDataCreation.createAdhocContactsbyEmail('abc@deloitte.com');
    insert adhocContact;
    Id AdhocID=adhocContact.id;
    try{
    insert WCT_UtilTestDataCreation.createAdhocContactsbyEmail('abc@deloitte.com');}
    catch(exception e){
    }
    
    Profile p = [SELECT Id FROM Profile WHERE Name=:WCT_UtilConstants.RECRUITER_COMPANY]; 
    Test_Data_Utility.createAccount();
    User recruitingCoordinator = WCT_UtilTestDataCreation.createUser('testest',p.id,'gjdfas@test.com','email7152@testing.com');
    insert recruitingCoordinator;
    WCT_Requisition__c Requisition_1= WCT_UtilTestDataCreation.createRequisitionbyUserEmails(null,recruitingCoordinator.Email);
    insert Requisition_1;
    insert WCT_UtilTestDataCreation.createCandidateRequisition(AdhocId,Requisition_1.id);
    insert WCT_UtilTestDataCreation.createAdhocCase(AdhocId);
    insert WCT_UtilTestDataCreation.createAdhocTask(AdhocId);
    insert WCT_UtilTestDataCreation.createAdhocCandidateDocument(AdhocId);
    insert WCT_UtilTestDataCreation.createAdhocNotes(AdhocId);
    insert WCT_UtilTestDataCreation.createAdhocAttachment(AdhocId);
    insert WCT_UtilTestDataCreation.createContactbyEmail('abc@deloitte.com');
    
    test.stopTest();      
   
}  
    


    
}