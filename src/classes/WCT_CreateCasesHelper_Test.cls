/**
    * Class Name  : WCT_CreateCasesHelper_Test
    * Description : This apex test class will use to test the WCT_CreateCasesHelper
*/
@isTest
private class WCT_CreateCasesHelper_Test {
    
    public static String caseRecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get(WCT_UtilConstants.CASE_RECORDTYPE_PRE_BI).getRecordTypeId();
    public static String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();
    public static List<WCT_PreBIStageTable__c> preBiStageList=new List<WCT_PreBIStageTable__c>();
    public static List<Contact> contactList=new List<Contact>();
    public static List<WCT_PreBIQAReferenceTable__c> refList=new List<WCT_PreBIQAReferenceTable__c>();
    public static User userRec=new User();
    public static Id profileId=[Select id from Profile where Name=:WCT_UtilConstants.RECRUITER_COMPANY].Id;
    public static Id roleId=[SELECT Id FROM UserRole where developerName ='CTS_Recruiter'].Id;
    public static WCT_Requisition__c req;
    public static WCT_Candidate_Requisition__c ct;
    
    /** 
        Method Name  : createContacts
        Return Type  : List<Contact>
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private static List<Contact> createContacts()
    {
        contactList=WCT_UtilTestDataCreation.createContactWithCandidate(candidateRecordTypeId);
        insert contactList;
        return contactList;
    }
    /** 
        Method Name  : createRequistion
        Return Type  : List<WCT_Requisition__c>
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private static WCT_Requisition__c createRequistion()
    {
        req=WCT_UtilTestDataCreation.createRequistion();
        insert req;
        return req;
    }
    /** 
        Method Name  : createCandidateRequisition
        Return Type  : List<WCT_Candidate_Requisition__c>
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private static WCT_Candidate_Requisition__c createCandidateRequisition(Id CandidateId,Id RequisitionId)
    {
        ct=WCT_UtilTestDataCreation.createCandidateRequisition(CandidateId,RequisitionId);
        insert ct;
        return ct;
    }    
    /** 
        Method Name  : createuser
        Return Type  : User
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUser()
    {
        userRec=WCT_UtilTestDataCreation.createUser('CaseHelp', profileId, 'vinBhatiaPreBICaseHelp@deloitte.com', 'Recruiter1@deloitte.com');
        return  userRec;
    }
    /** 
        Method Name  : createPreBIStageTable
        Return Type  : List<WCT_PreBIStageTable__c>
        Type         : private
        Description  : Create Pre-Bi Stage Table Records.         
    */
    private Static List<WCT_PreBIStageTable__c> createPreBIStageTable()
    {
        preBiStageList=WCT_UtilTestDataCreation.createPreBIStageTable(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        insert preBiStageList;
        return  preBiStageList;
    }
    
    /** 
        Method Name  : createPreBiReferenceRecords
        Return Type  : List<WCT_PreBIQAReferenceTable__c>
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private static List<WCT_PreBIQAReferenceTable__c> createPreBiReferenceRecords()
    {
        refList=WCT_UtilTestDataCreation.createPreBiReferenceRecords();
        insert refList;
        return refList;
    }
    /** 
        Method Name  : createCasesTestMethod
        Return Type  : void
        Description  : Create cases with all staging Table.         
    */
    static testMethod void createCasesTestMethod() {
        
        userRec=createUser();
        insert userRec;
        refList=createPreBiReferenceRecords();
        contactList=createContacts();
        req=createRequistion();
        ct=createCandidateRequisition(contactList[0].id,req.id);
        preBiStageList=createPreBIStageTable();
        Set<Id> stagingIds=new Set<id>();
        for(WCT_PreBIStageTable__c rec:preBiStageList)
        {
                stagingIds.add(rec.Id);
        }
        Test.startTest();
        WCT_CreateCasesHelper.createCases(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        List<WCT_PreBIStageTable__c> preList=[select Id from WCT_PreBIStageTable__c where Id IN:stagingIds and WCT_Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
        //System.assertEquals(preList.Size(), preBiStageList.Size());
        System.assertEquals(10, preBiStageList.Size());
    }
    /** 
        Method Name  : recuriterNotFound
        Return Type  : void
        Description  : recuriterNotFound Error check while inserting Cases.         
    */
    static testMethod void recuriterNotFound() {
        
        refList=createPreBiReferenceRecords();
        contactList=createContacts();
        req=createRequistion();
        ct=createCandidateRequisition(contactList[0].id,req.id);
        preBiStageList=createPreBIStageTable();
        Set<Id> stagingIds=new Set<id>();
        for(WCT_PreBIStageTable__c rec:preBiStageList)
        {
                stagingIds.add(rec.Id);
        }
        Test.startTest();
        WCT_CreateCasesHelper.createCases(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        List<WCT_PreBIStageTable__c> preList=[select Id,WCT_Status__c,WCT_Error_message__c from WCT_PreBIStageTable__c where Id IN:stagingIds ];
        for(WCT_PreBIStageTable__c rec: preList)
        {
            //System.assertEquals(rec.WCT_Status__c, WCT_UtilConstants.STAGING_STATUS_ERROR);
            //System.assertEquals(rec.WCT_Error_message__c, Label.Recruiter_Email_Error);
        }
    }
    /** 
        Method Name  : contactNotFound
        Return Type  : void
        Description  : Not valid contacts check         
    */
    static testMethod void contactNotFound() {
        
        userRec=createUser();
        insert userRec;
        refList=createPreBiReferenceRecords();
        req=createRequistion();
        preBiStageList=createPreBIStageTable();
        Set<Id> stagingIds=new Set<id>();
        for(WCT_PreBIStageTable__c rec:preBiStageList)
        {
                stagingIds.add(rec.Id);
        }
        Test.startTest();
        WCT_CreateCasesHelper.createCases(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        List<WCT_PreBIStageTable__c> preList=[select Id,WCT_Status__c,WCT_Error_message__c from WCT_PreBIStageTable__c where Id IN:stagingIds ];
        for(WCT_PreBIStageTable__c rec: preList)
        {
            //System.assertEquals(rec.WCT_Status__c, WCT_UtilConstants.STAGING_STATUS_ERROR);
            //System.assertEquals(rec.WCT_Error_message__c, Label.Candidate_Email_Error);
        }
        
    }
    /** 
        Method Name  : duplicateRowsError
        Return Type  : void
        Description  : Create cases with all staging Table.         
    */
    static testMethod void duplicateRowsError() {
        
        userRec=createUser();
        insert userRec;
        refList=createPreBiReferenceRecords();
        contactList=createContacts();
        req=createRequistion();
        ct=createCandidateRequisition(contactList[0].id,req.id);
        preBiStageList=createPreBIStageTable();
        List<WCT_PreBIStageTable__c> dupPreBIList=WCT_UtilTestDataCreation.createPreBIStageTable(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        insert dupPreBIList;
        System.assertEquals(dupPreBIList.Size(),10);
        Set<Id> stagingIds=new Set<id>();
        for(WCT_PreBIStageTable__c rec:preBiStageList)
        {
                stagingIds.add(rec.Id);
        }
        for(WCT_PreBIStageTable__c rec:dupPreBIList)
        {
                stagingIds.add(rec.Id);
        }
        Test.startTest(); 
        System.assertEquals(stagingIds.Size(),20);
        WCT_CreateCasesHelper.createCases(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        List<WCT_PreBIStageTable__c> preList=[select Id from WCT_PreBIStageTable__c where Id IN:stagingIds and WCT_Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
        System.assertEquals(preList.Size(), preBiStageList.Size());
        System.assertEquals(10, preBiStageList.Size());
        List<WCT_PreBIStageTable__c> dupPreList=[select Id from WCT_PreBIStageTable__c where Id IN:stagingIds and WCT_Status__c=:WCT_UtilConstants.STAGING_STATUS_ERROR];
        System.assertEquals(dupPreList.Size(), dupPreBIList.Size());
        //System.assertEquals(dupPreList.Size(), 20);
    }
    
    
}