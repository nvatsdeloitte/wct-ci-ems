/*****************************************************************************************
    Name    : WCT_EditTaskButtonController
    Desc    : WCT_EditTaskButtonController class for multiple tasks editing on open activity realted list in case details page.
    Approach: 
                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Deloitte          11/26/2013 3:51 AM       Created 
Deloitte          12/21/2013 11:53 AM       Modified

******************************************************************************************/
public class WCT_CreateEditTaskController {


    // public variable declaration.
    // ******************************************************************************************************************************

    
    public  String RelatedRecordId = System.currentPageReference().getParameters().get('Id'); 

    public  String sReturnURL= System.currentPageReference().getParameters().get(WCT_UtilConstants.WCT_EditTaskString_ret); // Retriving the Activity list view IDs from  the Url
    String sUserID = UserInfo.getUserId(); // Retriving user id 
   

    public list < Task> cNewTask {get;set;}
    public list < Task> cTrTask {
        get;
        set;
    }
    
    public boolean sError {
        get;
        set;
    }
    public String sErr {
        get;
        set;
    }
    public list < Task> cTaskList {get;set;}

   
 // Default controller constructor.
    // ******************************************************************************************************************************

    public WCT_CreateEditTaskController() {
        cNewTask = new list < Task> ();
        cTaskList =new List<Task>();
        sErr = apexpages.currentpage().getparameters().get(WCT_UtilConstants.WCT_EditTaskString_ErrorMessage);
        
        RelatedRecordId = System.currentPageReference().getParameters().get('Id');
        cTaskList=[select id, Subject, OwnerId,RecordTypeId, ActivityDate, Priority, Status,Secure_Subject__c,Secure_Comments__c, Description, Task_Notes__c from Task where WhatId =: RelatedRecordId and Status!='Completed' order by ActivityDate];
        if (sErr != null) {
            sError = true;
        } else {
            sError = false;
        }
        
    }

    
    // Add row method is user click on new task button to add task
    //******************************************************************************************************************************

    public void addRow() {
        
            task taskToadd = new task();
            taskToadd.OwnerID = sUserID;
            taskToadd.Priority = WCT_UtilConstants.Task_Priority;
            taskToadd.Status = WCT_UtilConstants.Task_Status;
            cNewTask.add(taskToadd);         

    }



   // Update Task method will update the selected tasks.
    // ******************************************************************************************************************************

    public PageReference updateTask() {
        if (cNewTask.size() > 0) {
            list < task > lstTaskToInsert = new list < task > ();
            for (Task cT: cNewTask) {

                cT.WhatId = RelatedRecordId ;
                lstTaskToInsert.add(cT);

            }try{
            insert lstTaskToInsert;
            }catch(Exception e)
            {   
                WCT_ExceptionUtility.logException('WCT_CreateEditTaskController','Insert Task',e.getMessage());          
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());  
                apexPages.addMessage(myMsg);                
                String s=e.getMessage();              
                String Err;                
                Integer i= s.lastIndexOf(WCT_UtilConstants.WCT_EditTaskUpdateError);              
                if(i!=-1){                
                    Err=s.substring(i);              
                    Err=Err.replace(WCT_UtilConstants.WCT_EditTaskUpdateError+',', '');               
                    Integer j=Err.IndexOf(WCT_UtilConstants.WCT_EditTaskUpdateRemoveErrorString);  
                    if(j!=-1){              
                        Err=Err.substring(0,j);               
                    }               
                }

            }

        }
        List < Task > lstTaskToUpdate = new List < Task > ();
         
            for (Task cTskTowork: cTaskList)
                {
                
                    lstTaskToUpdate.add(cTskTowork);                   
                }
            
            
            if (lstTaskToUpdate.size()>0){
                  try{
                update lstTaskToUpdate;
                } catch(Exception e)
            {            
                WCT_ExceptionUtility.logException('WCT_CreateEditTaskController','Update Task',e.getMessage());
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
                apexPages.addMessage(myMsg);              
                String s=e.getMessage();               
                String Err;                          
                Integer i= s.lastIndexOf(WCT_UtilConstants.WCT_EditTaskInvalidCrossRefError);       
                if(i!=-1){             
                    Err=s.substring(i);               
                    Err=Err.replace(WCT_UtilConstants.WCT_EditTaskInvalidCrossRefError+',', ''); 
                    Integer j=Err.IndexOf(WCT_UtilConstants.WCT_EditTaskUpdateRemoveErrorString);  
                    if(j!=-1){                  
                        Err=Err.substring(0,j);               
                    }              
                 }              
            }
            }
        
        //cTaskList = null;
    
        PageReference pageRef = new PageReference('/' + RelatedRecordId );
        pageRef.setRedirect(true); //Redirecting user to the case detail page. 
        return pageRef;
       

    }

   
    // getTasks method will return the list of the task related to the case and assign to the current user.
    // ******************************************************************************************************************************


    public list < Task> getTasks() {
                
           list < Task > lstTasksOnPage = new list<Task>();
            try {
                 set<String> setCaseIds= new set<String>();
                 if(RelatedRecordId!=null){
                 setCaseIds.Add(RelatedRecordId);
                 lstTasksOnPage = [select id, Subject, OwnerId,RecordTypeId, ActivityDate, Priority, Status,Secure_Subject__c,Secure_Comments__c, Description, Task_Notes__c from Task where WhatId =: RelatedRecordId and Status!='Completed' order by ActivityDate];
                 }

                 for (Task existingTask: lstTasksOnPage) {
                   
                     cTaskList.add(existingTask);
                    
                }

            } catch (QueryException e) {  
                WCT_ExceptionUtility.logException('WCT_CreateEditTaskController','Get Task',e.getMessage());          
                ApexPages.addMessages(e);              
                return null;
            }

        return cTaskList;
 
    }
    // ******************************************************************************************************************************


    //Cancel method to provide the cancel functionality on the edit page. This method will redirect user to the case detail page.
    // ******************************************************************************************************************************
    public PageReference cancel() {     
     
        PageReference pageRef = new PageReference('/' + RelatedRecordId);
        pageRef.setRedirect(true); //Redirecting user to the case detail page. 
        return pageRef;
    
    }

}