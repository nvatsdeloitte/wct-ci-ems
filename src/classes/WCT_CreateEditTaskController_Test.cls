@isTest
public class WCT_CreateEditTaskController_Test{
    public static testmethod void Test1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        Case testCase = WCT_UtilTestDataCreation.createCase(String.ValueOf(con.id));
        insert testCase;
        task t=WCT_UtilTestDataCreation.createTask(testCase.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Auto_Close__c=true;
        insert t;
        PageReference pageRef = Page.WCT_Create_EditTasks;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('id',String.valueOf(testCase.id));
        apexpages.currentpage().getparameters().put(WCT_UtilConstants.WCT_EditTaskString_ErrorMessage,'a');
        apexpages.currentpage().getparameters().put(WCT_UtilConstants.WCT_EditTaskString_ret,'a');
        Test.startTest();
        WCT_CreateEditTaskController controller = new WCT_CreateEditTaskController();
        controller.addRow();
        controller.updateTask();
        controller.getTasks();
        Test.stopTest();
    }
     public static testmethod void Test2()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        Case testCase = WCT_UtilTestDataCreation.createCase(String.ValueOf(con.id));
        insert testCase;
        task t=WCT_UtilTestDataCreation.createTask(testCase.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Auto_Close__c=true;
        insert t;
      
        PageReference pageRef = Page.WCT_Create_EditTasks;
        Test.setCurrentPage(pageRef); 
        Test.startTest();
        ApexPages.CurrentPage().getParameters().put('id',String.valueOf(con.id));
        WCT_CreateEditTaskController controller = new WCT_CreateEditTaskController();
        controller.addRow();
        controller.updateTask();
        controller.getTasks();
        controller = new WCT_CreateEditTaskController();
        controller.addRow();
        controller.updateTask();
        controller.getTasks();
        controller.cancel();
        Test.stopTest();
    }
     public static testmethod void Test3()
    {
        
        PageReference pageRef = Page.WCT_Create_EditTasks;
        Test.setCurrentPage(pageRef); 
        Test.startTest();
        
        WCT_CreateEditTaskController controller = new WCT_CreateEditTaskController();
        controller.addRow();
        controller.updateTask();
        Test.stopTest();
    }
        
}