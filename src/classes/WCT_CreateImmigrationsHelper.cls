/**  
    * Class Name  : WCT_CreateImmigrationsHelper  
    * Description : This apex class will use to Create Immigrations. 
*/
global class WCT_CreateImmigrationsHelper {
    
    
     public static void createImmigrations(Set<Id> stagingIds,String stagingStatus){
        
        List<WCT_Immigration_Stagging_Table__c> stageTableList=new List<WCT_Immigration_Stagging_Table__C>();
        Map<String,WCT_Immigration_Stagging_Table__c> stageUniqueRecordMap=new Map<String,WCT_Immigration_Stagging_Table__c>();
        List<WCT_Immigration_Stagging_Table__c> duplicateStageTableList=new List<WCT_Immigration_Stagging_Table__C>();
        Set<String> employeeEmailIds=new Set<String>();
        Set<String> InitiatorEmailIds=new Set<String>();
          Set<String> FragommenIds=new Set<String>();
        Map<String,Id> employeeIdsMap=new Map<String,Id>();
       
        String adhoccanRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CONTACT_GROUP_ADHOCCONTACTTYPE).getRecordTypeId();
        String canRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CONTACT_GROUP_CONTACTTYPE).getRecordTypeId();
        String employeeRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.Employee_RT).getRecordTypeId();
        String immigrationH1RT=Schema.SObjectType.WCT_Immigration__c.getRecordTypeInfosByName().get(WCT_UtilConstants.H1_VISA_RT).getRecordTypeId();
        String immigrationL1RT=Schema.SObjectType.WCT_Immigration__c.getRecordTypeInfosByName().get(WCT_UtilConstants.L1_VISA_RT).getRecordTypeId();
        String immigrationB1RT=Schema.SObjectType.WCT_Immigration__c.getRecordTypeInfosByName().get(WCT_UtilConstants.B1_VISA_RT).getRecordTypeId();
        
        Map<String,WCT_Immigration__c> oldImmigrationRecords=new Map<String,WCT_Immigration__c>();
        Map<String,WCT_Lca__c> oldLCARecords=new Map<String,WCT_LCA__C>();
        for(WCT_Immigration_Stagging_Table__c stateRec:[SELECT LCA_Filled_Identifier__c,Contact__r.Fragomen_Email__c,CreatedById,CreatedDate,Id,IsDeleted,Name,OwnerId,Status__c,
                                                        WCT_Action_EOS_COS_COE_Amendment__c,WCT_Case_type__c,WCT_Category__c,WCT_Copy_of_certified_H_1B_LCA_sent_date__c,WCT_CSV_Status__c,
                                                        WCT_CSV_Type__c,WCT_Date_Blanket_Sent_Out__c,WCT_Date_B_Package_Sent__c,WCT_Date_Completed_Docs_Info_Received__c,WCT_Date_Filed__c,
                                                        WCT_Date_Initiation_Received__c,WCT_Date_Initiation_Sent_to_Fragomen__c,WCT_Date_Placed_on_Hold__c,WCT_Date_Received_Suport_Leter_Valid__c,
                                                        WCT_Deloitte_India_Office__c,WCT_Dependent_Email__c,WCT_Dt_Suport_Letter_Sent_for_Validation__c,WCT_Employee_Email__c,WCT_Emp_Number__c,
                                                        WCT_Entity__c,WCT_Error_Message__c,WCT_First_Name__c,WCT_H_1B_Cap__c,WCT_H_1B_LCA_filed_with_DOL__c,WCT_Identifier__c,WCT_Immigration__c,
                                                        WCT_Initiatior__c,WCT_Initiator_Email_Address__c,WCT_Last_Action__c,WCT_Last_Name__c,WCT_LCA_Filed__c,WCT_L_1_Blanket_or_Individual__c,
                                                        WCT_Milestone_Date__c, WCT_Milestone__c,WCT_Petition_Approved__c,WCT_Receivd_H_1B_LCA_Amendment_initiatin__c,
                                                        WCT_Received_Confirmation_of_LCA_Posting__c,WCT_Received_Request_for_Evidence__c,WCT_Request_for_Evidence_Due_Date__c,
                                                        WCT_RFE_Due_Date__c,WCT_Petition_Start_Date__c,WCT_Petition_End_Date__c,WCT_Personnel_Number__c,
                                                        WCT_Response_to_Requst_for_Evidnce_Filed__c,WCT_Service_Area__c,WCT_Service_Line__c,WCT_Visa_Type__c FROM WCT_Immigration_Stagging_Table__c 
                                                        where ID IN:stagingIds and Status__c =: stagingStatus])
         
        {
            if(stateRec.WCT_Identifier__c!=null)
            {
                if(stageUniqueRecordMap.containsKey(stateRec.WCT_Identifier__c))
                {
                    duplicateStageTableList.add(stateRec);
                }
                else
                {
                    stageUniqueRecordMap.put((stateRec.WCT_Identifier__c).toLowerCase(),stateRec);
                }
                if(stateRec.WCT_Employee_Email__c!=null)
                    employeeEmailIds.add(stateRec.WCT_Employee_Email__c);
                if(stateRec.WCT_Initiator_Email_Address__c!=null)
                    InitiatorEmailIds.add(stateRec.WCT_Initiator_Email_Address__c);
                if(stateRec.Contact__r.Fragomen_Email__c!=null)
                    FragommenIds.add(stateRec.Contact__r.Fragomen_Email__c);    
                
            }
            
            
            
                
        }
       
        
        if(!employeeEmailIds.IsEmpty())
        {
            for(Contact candidateRecord: [Select id,Email from Contact where (Email IN:employeeEmailIds or Email In: InitiatorEmailIds ) and (recordTypeId=:employeeRecordTypeId or recordTypeId=:canRecordTypeId or recordTypeId=:adhoccanRecordTypeId)])
            {
                employeeIdsMap.put((candidateRecord.Email).toLowerCase(),candidateRecord.Id);
            }
             for(Contact candidateRecord: [Select id,Fragomen_Email__c from Contact where (Fragomen_Email__c IN:FragommenIds) and (recordTypeId=:employeeRecordTypeId or recordTypeId=:canRecordTypeId or recordTypeId=:adhoccanRecordTypeId)])
            {
                employeeIdsMap.put((candidateRecord.Fragomen_Email__c).toLowerCase(),candidateRecord.Id);
            }
            for(WCT_Immigration__c oldImmigrationRecord:[SELECT CreatedById,CreatedDate,Id,IsDeleted,Name,OwnerId,RecordTypeId,SystemModstamp,WCT_Assignment_Owner__c,WCT_B_Package_Sent_Date__c,WCT_CSV_Status__c,WCT_EAD_Status__c,
                            WCT_Email_Received__c,WCT_Employee_Email_Address__c,WCT_Fiscal_Year_only__c,WCT_Fragomen_External_Key__c,
                            WCT_Identifier__c,WCT_Immigration_Flag__c,WCT_Immigration_Status__c,WCT_Initiation_Received_Date__c, WCT_Petition_Status__c,
                            WCT_Last_Action_Taken__c, WCT_Gross_Salary_in_India__c, WCT_Petition_Filing_Date__c, WCT_Petition_Result__c, WCT_Milestone_Date__c,
                            WCT_Milestone__c,WCT_Petition_End_Date__c,WCT_Petition_Initiation_Date__c, WCT_Petition_Salary__c,WCT_Petition_Start_Date__c,WCT_Project_Controller__c,WCT_Reason_for_Denial__c,
                            WCT_Received_Request_for_Evidence_Date__c,WCT_Record_Visible_to_ToD__c,WCT_Related_Immigration__c,WCT_Request_for_Evidence_Due_Date__c,
                            WCT_Request_Initiation_Date__c,WCT_Resource_Manager__c,WCT_Status__c,WCT_Updated_Fragomen_record__c,WCT_Updated_Fragomen_Record_Date__c,WCT_Visa_Category__c,
                            WCT_Visa_Expiration_Date__c,WCT_Visa_Interview_Q_A__c,WCT_Visa_Start_Date__c,WCT_Visa_Status__c,WCT_Visa_Type__c FROM WCT_Immigration__c
                            where WCT_Assignment_Owner__r.Email IN: employeeEmailIds OR WCT_Assignment_Owner__r.Fragomen_Email__c IN: employeeEmailIds])
            {
                if(oldImmigrationRecord.WCT_Identifier__c!=null)
                    oldImmigrationRecords.put((oldImmigrationRecord.WCT_Identifier__c).toLowerCase(),oldImmigrationRecord);
            }
            
            for(WCT_LCA__C lCARec:[SELECT CreatedById,CreatedDate,Id,IsDeleted,Name,OwnerId,RecordTypeId,
                        SystemModstamp,WCT_Assignment_Owner__c,WCT_City__c,WCT_Expiration_Date__c,WCT_Fragomen_External_Key__c,WCT_Identifier__c,WCT_Immigration__c,WCT_Milestone_Date__c,WCT_Milestone__c,
                        WCT_Record_Visible_to_ToD__c,WCT_Start_Date__c,WCT_State__c,WCT_Status__c,WCT_Updated_Fragomen_record__c,WCT_Updated_Fragomen_Record_Date__c,WCT_Zip_Code__c FROM WCT_LCA__c where WCT_Assignment_Owner__r.Email In: employeeEmailIds OR WCT_Assignment_Owner__r.Fragomen_Email__c IN: employeeEmailIds])
            {
                if(lCARec.WCT_Identifier__c!=null)
                    oldLCARecords.put((lCARec.WCT_Identifier__c).toLowerCase(),lCARec);
            }
        }
        List<WCT_Immigration__c> updateImmigrationList=new List<WCT_Immigration__c>();
        List<WCT_Immigration__c> newImmigrationList=new List<WCT_Immigration__c>();
        List<WCT_LCA__c> newLCAList=new List<WCT_LCA__c>();
        List<WCT_LCA__c> updateLCAList=new List<WCT_LCA__c>();
        List<WCT_Immigration_Stagging_Table__c> updateStageTableList = new List<WCT_Immigration_Stagging_Table__c>();
        // logic to start creating/updating records
        if(!stageUniqueRecordMap.IsEmpty())
        {
            
            for(String str:stageUniqueRecordMap.keySet())
            {
                str=(str).toLowerCase();
                
                if((employeeIdsMap.ContainsKey(stageUniqueRecordMap.get(str).WCT_Employee_Email__c) || employeeIdsMap.ContainsKey(stageUniqueRecordMap.get(str).Contact__r.Fragomen_Email__c)) && (!(stageUniqueRecordMap.get(str).WCT_Case_type__c=='L-1' && !stageUniqueRecordMap.get(str).WCT_Employee_Email__c.Contains('@deloitte.com'))))
                {
                    
                    WCT_Immigration_Stagging_Table__c stageTable=new WCT_Immigration_Stagging_Table__c(Id=stageUniqueRecordMap.get(str).Id);
                    String visaType;
                    if(stageUniqueRecordMap.get(str).WCT_Case_type__c == 'H-1B'){
                        if(stageUniqueRecordMap.get(str).WCT_H_1B_Cap__c=='Cap')
                            visaType = 'H1B Cap';
                        else if(stageUniqueRecordMap.get(str).WCT_Action_EOS_COS_COE_Amendment__c=='Extension')
                            visaType = 'H1B Extension';
                        else if(stageUniqueRecordMap.get(str).WCT_Action_EOS_COS_COE_Amendment__c=='Transfer')
                            visaType = 'H1B Transfer';
                        else if(stageUniqueRecordMap.get(str).WCT_Action_EOS_COS_COE_Amendment__c=='Amendment')
                            visaType = 'H1B Amendment';    
                        
                    }
                          
                                
                    else if(stageUniqueRecordMap.get(str).WCT_Case_type__c=='L-1' && stageUniqueRecordMap.get(str).WCT_Employee_Email__c.Contains('@deloitte.com')){
                      
                        if(stageUniqueRecordMap.get(str).WCT_Action_EOS_COS_COE_Amendment__c=='Extension'){
                            if(stageUniqueRecordMap.get(str).WCT_Category__c=='L-1B')
                                visaType = 'L1B Extension';
                            else
                                visaType = 'L1A Extension';
                        }
                        else{
                            if(stageUniqueRecordMap.get(str).WCT_Category__c=='L-1B' && stageUniqueRecordMap.get(str).WCT_L_1_Blanket_or_Individual__c!=null)                                   
                               
                                visaType = 'L1B ' + stageUniqueRecordMap.get(str).WCT_L_1_Blanket_or_Individual__c;
                            
                            else if(stageUniqueRecordMap.get(str).WCT_Category__c=='L-1A' && stageUniqueRecordMap.get(str).WCT_L_1_Blanket_or_Individual__c!=null) 
                               
                                visaType = 'L1A ' + stageUniqueRecordMap.get(str).WCT_L_1_Blanket_or_Individual__c;
                            
                        }
                    }
                    else if(stageUniqueRecordMap.get(str).WCT_Case_type__c=='B-1'){
                        visaType = 'B1/B2 ';
                    }                     
                    if(oldImmigrationRecords.containsKey(str) && 
                        (stageUniqueRecordMap.get(str).WCT_Milestone__c!=oldImmigrationRecords.get(str).WCT_Milestone__c || 
                        stageUniqueRecordMap.get(str).WCT_RFE_Due_Date__c!=oldImmigrationRecords.get(str).WCT_Request_for_Evidence_Due_Date__c ||
                        stageUniqueRecordMap.get(str).WCT_Petition_End_Date__c!=oldImmigrationRecords.get(str).WCT_Petition_End_Date__c ||
                        stageUniqueRecordMap.get(str).WCT_Petition_Start_Date__c!=oldImmigrationRecords.get(str).WCT_Petition_Start_Date__c ||
                        stageUniqueRecordMap.get(str).WCT_Milestone_Date__c!=oldImmigrationRecords.get(str).WCT_Milestone_Date__c ||
                        visaType!=oldImmigrationRecords.get(str).WCT_Visa_Type__c ))
                    {
                        WCT_Immigration__c immigraitonRecord=new WCT_Immigration__C(id=oldImmigrationRecords.get(str).Id);
                        immigraitonRecord.WCT_Milestone__c=stageUniqueRecordMap.get(str).WCT_Milestone__c;
                        immigraitonRecord.WCT_Milestone_Date__c=stageUniqueRecordMap.get(str).WCT_Milestone_Date__c;
                        immigraitonRecord.WCT_Request_for_Evidence_Due_Date__c=stageUniqueRecordMap.get(str).WCT_RFE_Due_Date__c; 
                        immigraitonRecord.WCT_Petition_End_Date__c=stageUniqueRecordMap.get(str).WCT_Petition_End_Date__c;
                        immigraitonRecord.WCT_Petition_Start_Date__c=stageUniqueRecordMap.get(str).WCT_Petition_Start_Date__c;
                        immigraitonRecord.WCT_Visa_Type__c=visaType;
                        
                        immigraitonRecord.WCT_Updated_Fragomen_record__c=true;
                        immigraitonRecord.WCT_Updated_Fragomen_Record_Date__c=System.today();
                        updateImmigrationList.add(immigraitonRecord);
                        
                        stageTable.Status__c='Completed';
                        updateStageTableList.add(stageTable);
                        /*if(oldImmigrationRecords.get(str).recordTypeId==immigrationH1RT && WCT_UtilConstants.LCA_FILED.equalsIgnoreCase(stageUniqueRecordMap.get(str).WCT_Milestone__c) )
                        {
                            if(!oldLCARecords.containsKey(str))
                            {
                                WCT_LCA__C lcaRec=new WCT_LCA__C();
                                lcaRec.WCT_Assignment_Owner__c = employeeIdsMap.get(stageUniqueRecordMap.get(str).WCT_Employee_Email__c);
                                lcaRec.WCT_Immigration__c = oldImmigrationRecords.get(str).Id;
                                lcaRec.WCT_Identifier__c=stageUniqueRecordMap.get(str).LCA_Filled_Identifier__c;
                                //lcaRecord.WCT_Status__c = 'New';
                                //lcaRec.WCT_Start_Date__c = stageUniqueRecordMap.get(str).WCT_Date_Initiation_Sent_to_Fragomen__c;
                                newLCAList.add(lcaRec);
                                
                                
                            }
                        }*/
                       
                    }
                    else if(oldLCARecords.containsKey(str) && 
                        (stageUniqueRecordMap.get(str).WCT_Milestone__c!=oldLCARecords.get(str).WCT_Milestone__c || 
                        stageUniqueRecordMap.get(str).WCT_Milestone_Date__c!=oldLCARecords.get(str).WCT_Milestone_Date__c ))
                    {
                            WCT_LCA__C lcaRec=new WCT_LCA__C(id=oldLCARecords.get(str).Id);
                            lcaRec.WCT_Milestone__c=stageUniqueRecordMap.get(str).WCT_Milestone__c;
                            //lcaRec.WCT_Status__c=stageUniqueRecordMap.get(str).WCT_Milestone__c;
                            lcaRec.WCT_Milestone_Date__c=stageUniqueRecordMap.get(str).WCT_Milestone_Date__c;
                            lcaRec.WCT_Updated_Fragomen_record__c=true;
                            lcaRec.WCT_Updated_Fragomen_Record_Date__c=System.today();
                            updateLCAList.add(lcaRec);
                            stageTable.Status__c='Completed';
                    }
                    else if(oldImmigrationRecords.containsKey(str) && 
                        (stageUniqueRecordMap.get(str).WCT_Milestone__c==oldImmigrationRecords.get(str).WCT_Milestone__c && 
                        stageUniqueRecordMap.get(str).WCT_Milestone_Date__c==oldImmigrationRecords.get(str).WCT_Milestone_Date__c &&
                        stageUniqueRecordMap.get(str).WCT_RFE_Due_Date__c==oldImmigrationRecords.get(str).WCT_Request_for_Evidence_Due_Date__c &&
                        stageUniqueRecordMap.get(str).WCT_Petition_End_Date__c==oldImmigrationRecords.get(str).WCT_Petition_End_Date__c &&
                        stageUniqueRecordMap.get(str).WCT_Petition_Start_Date__c==oldImmigrationRecords.get(str).WCT_Petition_Start_Date__c &&
                        visaType==oldImmigrationRecords.get(str).WCT_Visa_Type__c ))
                    {
                        
                        stageTable.WCT_Error_message__c=label.WCT_Duplicate_Record;
                        stageTable.Status__c='Failed';
                        updateStageTableList.add(stageTable);
                    }
                    else if(oldLCARecords.containsKey(str) && 
                        (stageUniqueRecordMap.get(str).WCT_Milestone__c==oldLCARecords.get(str).WCT_Milestone__c && 
                        stageUniqueRecordMap.get(str).WCT_Milestone_Date__c==oldLCARecords.get(str).WCT_Milestone_Date__c ))
                    {
                        
                        stageTable.WCT_Error_message__c=label.WCT_Duplicate_Record;
                        stageTable.Status__c='Failed';
                        updateStageTableList.add(stageTable);
                    }
                    else
                    {
                        // Insertion Logic
                        
                        stageTable.Status__c='Completed';
                        updateStageTableList.add(stageTable);
                       
                        if(stageUniqueRecordMap.get(str).WCT_Case_type__c!='LCA')
                        {
                            WCT_Immigration__c immigraitonRecord=new WCT_Immigration__c();
                            immigraitonRecord.WCT_Milestone__c=stageUniqueRecordMap.get(str).WCT_Milestone__c;
                            immigraitonRecord.WCT_Milestone_Date__c=stageUniqueRecordMap.get(str).WCT_Milestone_Date__c;
                            immigraitonRecord.WCT_Request_for_Evidence_Due_Date__c=stageUniqueRecordMap.get(str).WCT_RFE_Due_Date__c;
                            immigraitonRecord.WCT_Petition_End_Date__c=stageUniqueRecordMap.get(str).WCT_Petition_End_Date__c;
                            immigraitonRecord.WCT_Petition_Start_Date__c=stageUniqueRecordMap.get(str).WCT_Petition_Start_Date__c;
                            immigraitonRecord.OwnerId=Label.GMI_Queue_Id;
                            immigraitonRecord.WCT_Identifier__c=stageUniqueRecordMap.get(str).WCT_Identifier__c;
                            immigraitonRecord.WCT_Request_Initiation_Date__c=stageUniqueRecordMap.get(str).WCT_Date_Initiation_Sent_to_Fragomen__c;
                            immigraitonRecord.WCT_Assignment_Owner__c = employeeIdsMap.get(stageUniqueRecordMap.get(str).WCT_Employee_Email__c);
                            if(employeeIdsMap.get(stageUniqueRecordMap.get(str).WCT_Initiator_Email_Address__c)!=null)
                                immigraitonRecord.WCT_Resource_Manager__c = employeeIdsMap.get(stageUniqueRecordMap.get(str).WCT_Initiator_Email_Address__c);
                            If(stageUniqueRecordMap.get(str).WCT_Case_type__c=='H-1B')
                            {
                                immigraitonRecord.recordTypeId=immigrationH1RT;
                                if(stageUniqueRecordMap.get(str).WCT_H_1B_Cap__c=='Cap')
                                    immigraitonRecord.WCT_Visa_Type__c='H1B Cap';
                                else if(stageUniqueRecordMap.get(str).WCT_Action_EOS_COS_COE_Amendment__c=='Extension')
                                    immigraitonRecord.WCT_Visa_Type__c='H1B Extension';
                                else if(stageUniqueRecordMap.get(str).WCT_Action_EOS_COS_COE_Amendment__c=='Transfer')
                                    immigraitonRecord.WCT_Visa_Type__c='H1B Transfer';
                                else if(stageUniqueRecordMap.get(str).WCT_Action_EOS_COS_COE_Amendment__c=='Amendment')
                                    immigraitonRecord.WCT_Visa_Type__c='H1B Amendment';
                                 
                               
                                
                            }
                            else if(stageUniqueRecordMap.get(str).WCT_Case_type__c=='L-1' && stageUniqueRecordMap.get(str).WCT_Employee_Email__c.Contains('@deloitte.com'))
                            {
                                
                                immigraitonRecord.recordTypeId=immigrationL1RT;
                                if(stageUniqueRecordMap.get(str).WCT_Action_EOS_COS_COE_Amendment__c=='Extension' )
                                {
                                  if(stageUniqueRecordMap.get(str).WCT_Category__c=='L-1B')
                                      immigraitonRecord.WCT_Visa_Type__c='L1B Extension';
                                    else if (stageUniqueRecordMap.get(str).WCT_Category__c=='L-1A')
                                      immigraitonRecord.WCT_Visa_Type__c='L1A Extension';
                                
                                }
                                else
                                {
                                  if(stageUniqueRecordMap.get(str).WCT_Category__c=='L-1B' )    
                                  {                   
                                    if(stageUniqueRecordMap.get(str).WCT_L_1_Blanket_or_Individual__c!=null)           
                                        immigraitonRecord.WCT_Visa_Type__c='L1B ' + stageUniqueRecordMap.get(str).WCT_L_1_Blanket_or_Individual__c;
                                      
                                    
                                  }else if (stageUniqueRecordMap.get(str).WCT_Category__c=='L-1A')
                                      {
                                        if(stageUniqueRecordMap.get(str).WCT_L_1_Blanket_or_Individual__c!=null) 
                                          immigraitonRecord.WCT_Visa_Type__c='L1A ' + stageUniqueRecordMap.get(str).WCT_L_1_Blanket_or_Individual__c;
                                        
                                      }
                                }
                            }
                            else if(stageUniqueRecordMap.get(str).WCT_Case_type__c=='B-1')
                            {
                                immigraitonRecord.recordTypeId=immigrationB1RT;
                                immigraitonRecord.WCT_Visa_Type__c='B1/B2 ';
                            }
                          
                                           
                            newImmigrationList.add(immigraitonRecord);
                            
                            
                           
                        }
                        else
                        {
                            WCT_LCA__C lcaRec=new WCT_LCA__C();
                            lcaRec.WCT_Identifier__c=stageUniqueRecordMap.get(str).WCT_Identifier__c;
                            lcaRec.WCT_Milestone__c=stageUniqueRecordMap.get(str).WCT_Milestone__c;
                            lcaRec.WCT_Status__c='LCA Initiated';//stageUniqueRecordMap.get(str).WCT_Milestone__c;
                            lcaRec.WCT_Milestone_Date__c=stageUniqueRecordMap.get(str).WCT_Milestone_Date__c;
                            lcaRec.OwnerId=Label.GMI_Queue_Id;
                            lcaRec.WCT_Assignment_Owner__c = employeeIdsMap.get(stageUniqueRecordMap.get(str).WCT_Employee_Email__c);
                            //lcaRec.WCT_Start_Date__c = stageUniqueRecordMap.get(str).WCT_Date_Initiation_Sent_to_Fragomen__c;
                            newLcalist.add(lcaRec);
                            
                        }
                        
                        
                    }
                }
                else
                {
                   
                    // Errors.....
                    if(!employeeIdsMap.ContainsKey(stageUniqueRecordMap.get(str).WCT_Employee_Email__c))
                    {
                        WCT_Immigration_Stagging_Table__c stageTable=new WCT_Immigration_Stagging_Table__c(Id=stageUniqueRecordMap.get(str).Id);
                        stageTable.WCT_Error_message__c=label.Employee_Email_Error;
                        stageTable.Status__c='Failed';
                        updateStageTableList.add(stageTable); 
                    }
                    else
                    {
                       
                       WCT_Immigration_Stagging_Table__c stageTable=new WCT_Immigration_Stagging_Table__c(Id=stageUniqueRecordMap.get(str).Id);
                       stageTable.WCT_Error_message__c=label.L1_Visa_Error;
                       stageTable.Status__c='Failed';
                       updateStageTableList.add(stageTable); 
                    }
                    
                }   
            }
            
            
            
        }
        
        if(!updateImmigrationList.IsEmpty())
            update updateImmigrationList;
        if(!newImmigrationList.IsEmpty())
            insert newImmigrationList;
        if(!newLCAList.IsEmpty())
            insert  newLCAList;
        if(!updateLCAList.IsEmpty())
            update updateLCAList;
        if(!updateStageTableList.IsEmpty())
            update updateStageTableList; 
        
        
     }
     
    //CREATING MOBILITY IMMIGRATIONS WHEN WE CHANGE VISA START DATE AND EXPIRATION DATE
    
    public static void createMobilityImmigrations(list <WCT_Mobility__c> lstMoblityRec) {
    
  
        String employeeRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.Employee_RT).getRecordTypeId();
        String immigrationB1RT=Schema.SObjectType.WCT_Immigration__c.getRecordTypeInfosByName().get(WCT_UtilConstants.B1_VISA_RT).getRecordTypeId();
        String mobilityB1RT=Schema.SObjectType.WCT_Mobility__c.getRecordTypeInfosByName().get(WCT_UtilConstants.Business_VISA_RT).getRecordTypeId();
     
     
       List<WCT_Immigration__c> oldImmigrationList=new List<WCT_Immigration__c>();
       Map<string,date> oldImmigrationMap = new map<string,date>();
       Map<string,date> oldImmigrationMap1 = new map<string,date>();
       
       Set<String> listofmobemails = new Set<String>();
      
            
        for(WCT_Mobility__c stateRec:lstMoblityRec)
        {
             if(stateRec.WCT_Employee_Email_Address__c != null || stateRec.WCT_Employee_Email_Address__c != '') {
                listofmobemails.add(stateRec.WCT_Employee_Email_Address__c); 
             
             }
                
        }
       
      List<Contact> listcts = [SELECT Id,email,(SELECT Id,Name,WCT_Immigration_Bvisa_Status__c,WCT_Employee_Email_Address__c,WCT_Assignment_Owner__c,WCT_Visa_Expiration_Date__c,WCT_Visa_Start_Date__c,WCT_Visa_Status__c,WCT_Visa_Type__c FROM Immigrations__r
                             where WCT_Visa_Type__c IN ('B1/B2') and RecordType.name IN('B1 Visa') AND  WCT_Immigration_Status__c NOT IN('Visa Denied','Visa Expired') AND WCT_Employee_Email_Address__c IN :listofmobemails  ORDER BY CreatedDate DESC ) FROM CONTACT where Recordtype.name IN('Employee') AND email IN :listofmobemails ];
       
      for(Contact con : listcts) {
       
       if(con.Immigrations__r.size() > 0)
       {
     
           oldImmigrationMap.put(con.Immigrations__r[0].WCT_Assignment_Owner__c,con.Immigrations__r[0].WCT_Visa_Start_Date__c);
           oldImmigrationMap1.put(con.Immigrations__r[0].WCT_Assignment_Owner__c,con.Immigrations__r[0].WCT_Visa_Expiration_Date__c);
       }       
       
      }
    

         List<WCT_Immigration__c> newImmigrationList=new List<WCT_Immigration__c>();
            for(WCT_Mobility__c stateRec:lstMoblityRec)
        {
            
             if(stateRec.WCT_Existing_Business_Visa__c != 'Green Card/ US Citizen') {
             
               if(stateRec.WCT_Visa_start_date__c != null && stateRec.WCT_Visa_expiration_date__c != null) {
               
            if(mobilityB1RT == stateRec.recordtypeId) {
            
             if(oldImmigrationMap.get(stateRec.WCT_Mobility_Employee__c) != stateRec.WCT_Visa_start_date__c && oldImmigrationMap1.get(stateRec.WCT_Mobility_Employee__c) != stateRec.WCT_Visa_expiration_date__c) {
                    
                      WCT_Immigration__c immigraitonRecord=new WCT_Immigration__c();
                     immigraitonRecord.WCT_Immigration_Status__c='Visa Stamped';
                     immigraitonRecord.WCT_Visa_Start_Date__c=stateRec.WCT_Visa_start_date__c;
                     immigraitonRecord.WCT_Visa_Expiration_Date__c=stateRec.WCT_Visa_expiration_date__c;
                     immigraitonRecord.WCT_Assignment_Owner__c = stateRec.WCT_Mobility_Employee__c;
                     immigraitonRecord.recordTypeId=immigrationB1RT;
                     immigraitonRecord.WCT_Visa_Type__c='B1/B2';
                     immigraitonRecord.WCT_Previous_Employer_Visa__c=true;
                     immigraitonRecord.WCT_Immigration_Bvisa_Status__c=true;
                     immigraitonRecord.Immigration_Email_status__c = true;      
                     newImmigrationList.add(immigraitonRecord);
            }  
            
            }     
            }
             
        }
        
        }
  
        if(!newImmigrationList.IsEmpty())
            insert newImmigrationList;
            
    }
    }