/**
    * Class Name  : WCT_CreateImmigrationsHelper_Test
    * Description : This apex test class will use to test the WCT_CreateImmigrationsHelper
*/
@isTest
private class WCT_CreateImmigrationsHelper_Test{

    public static List<Contact> employeeList = new List<Contact>();
    public static List<WCT_Immigration_Stagging_Table__c> immgStageList = new List<WCT_Immigration_Stagging_Table__c>();
    public static String empRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.Employee_RT).getRecordTypeId();
    String adhoccanRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CONTACT_GROUP_ADHOCCONTACTTYPE).getRecordTypeId();
    String canRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CONTACT_GROUP_CONTACTTYPE).getRecordTypeId();
     /** 
        Method Name  : createImmigrationStageTable
        Return Type  : List<WCT_Immigration_Stagging_Table__c>
        Type      : private
        Description  : Create Immigration Stage Table Records.         
    */
    private Static List<WCT_Immigration_Stagging_Table__c> createImmigrationStageTable()
    {
      immgStageList = WCT_UtilTestDataCreation.createImmigrationStageTable(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
      insert immgStageList;
      return  immgStageList;
    }
    
     /** 
        Method Name  : createEmployees
        Return Type  : List<Contact>
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private static List<Contact> createEmployees()
    {
        employeeList = WCT_UtilTestDataCreation.createContactWithEmployee(empRecordTypeId);
        insert employeeList;
        return employeeList;
    }
    
    /** 
        Method Name  : createImmigrationsTestMethod
        Return Type  : void
        Description  : Create Immigrations with all staging Table.         
    */
    static testMethod void createImmigrationsTestMethod() {
        employeeList = createEmployees();
        immgStageList = createImmigrationStageTable();
        Set<Id> stagingIds=new Set<id>();
        for(WCT_Immigration_Stagging_Table__c rec: immgStageList)
        {
                stagingIds.add(rec.Id);
        }        
        Test.startTest();
        WCT_CreateImmigrationsHelper.createImmigrations(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        List<WCT_Immigration_Stagging_Table__c> staggingList=[select Id from WCT_Immigration_Stagging_Table__c where Id IN:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
        System.assertEquals(staggingList.Size(), immgStageList.Size());
        
    }
    
    /** 
        Method Name  : createL1BImmigrationsTestMethod
        Return Type  : void
        Description  : Create L1B Immigrations with all staging Table.         
    */
    static testMethod void createL1BImmigrationsTestMethod() {
        employeeList = createEmployees();
        immgStageList = WCT_UtilTestDataCreation.createL1BImmigrationStageTable(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        insert immgStageList;
        Set<Id> stagingIds=new Set<id>();
        for(WCT_Immigration_Stagging_Table__c rec: immgStageList)
        {
                stagingIds.add(rec.Id);
        }        
        Test.startTest();
        WCT_CreateImmigrationsHelper.createImmigrations(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        List<WCT_Immigration_Stagging_Table__c> staggingList=[select Id from WCT_Immigration_Stagging_Table__c where Id IN:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
        System.assertEquals(staggingList.Size(), immgStageList.Size());
        
    }
    
    /** 
        Method Name  : createB1ImmigrationsTestMethod
        Return Type  : void
        Description  : Create L1B Immigrations with all staging Table.         
    */
    static testMethod void createB1ImmigrationsTestMethod() {
        employeeList = createEmployees();
        immgStageList = WCT_UtilTestDataCreation.createB1ImmigrationStageTable(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        insert immgStageList;
        Set<Id> stagingIds=new Set<id>();
        for(WCT_Immigration_Stagging_Table__c rec: immgStageList)
        {
                stagingIds.add(rec.Id);
        }        
        Test.startTest();
        WCT_CreateImmigrationsHelper.createImmigrations(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        List<WCT_Immigration_Stagging_Table__c> staggingList=[select Id from WCT_Immigration_Stagging_Table__c where Id IN:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
        System.assertEquals(staggingList.Size(), immgStageList.Size());
        
    }
    
    /** 
        Method Name  : createLCAImmigrationsTestMethod
        Return Type  : void
        Description  : Create LCA Immigrations with all staging Table.         
    */
    static testMethod void createLCAImmigrationsTestMethod() {
        employeeList = createEmployees();
        immgStageList = WCT_UtilTestDataCreation.createLCAImmigrationStageTable(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        insert immgStageList;
        Set<Id> stagingIds=new Set<id>();
        for(WCT_Immigration_Stagging_Table__c rec: immgStageList)
        {
                stagingIds.add(rec.Id);
        }        
        Test.startTest();
        WCT_CreateImmigrationsHelper.createImmigrations(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        List<WCT_Immigration_Stagging_Table__c> staggingList=[select Id from WCT_Immigration_Stagging_Table__c where Id IN:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
        System.assertEquals(staggingList.Size(), immgStageList.Size());
        
    }
    
    
    /** 
        Method Name  : duplicateRowsError
        Return Type  : void
        Description  : Create Immigrations with all staging Table.         
    */
    static testMethod void duplicateRowsError() {
        employeeList = createEmployees();
        immgStageList = createImmigrationStageTable();
        List<WCT_Immigration_Stagging_Table__c> dupImmigrationList = WCT_UtilTestDataCreation.createImmigrationStageTable(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        insert dupImmigrationList;
        System.assertEquals(dupImmigrationList.Size(),10);
        Set<Id> stagingIds=new Set<id>();
        for(WCT_Immigration_Stagging_Table__c rec:immgStageList)
        {
                stagingIds.add(rec.Id);
        }
        Test.startTest();
        System.assertEquals(stagingIds.Size(),10);
        WCT_CreateImmigrationsHelper.createImmigrations(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        //Test.stopTest();
        List<WCT_Immigration_Stagging_Table__c> imgList=[select Id from WCT_Immigration_Stagging_Table__c where Id IN:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
        System.assertEquals(imgList.Size(), immgStageList.Size());
        
        for(WCT_Immigration_Stagging_Table__c rec:dupImmigrationList)
        {
                stagingIds.add(rec.Id);
        }
        //Test.startTest();
        System.assertEquals(stagingIds.Size(),20);
        WCT_CreateImmigrationsHelper.createImmigrations(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        List<WCT_Immigration_Stagging_Table__c> imggList=[select Id from WCT_Immigration_Stagging_Table__c where Id IN:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
        System.assertEquals(imggList.Size(), immgStageList.Size());
        List<WCT_Immigration_Stagging_Table__c> dupImgList=[select Id from WCT_Immigration_Stagging_Table__c where Id IN:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_ERROR];
        System.assertEquals(dupImgList.Size(), dupImmigrationList.Size());
    }
}