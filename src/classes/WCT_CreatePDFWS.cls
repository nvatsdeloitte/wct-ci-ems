/**************************************************************************************
Apex Class Name: WCT_CreatePDFWS
Created Date     : 12 December 2013
Function         : Handler class for WCT_InterviewFormTrigger Trigger
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                    12/12/2013              Original Version
*************************************************************************************/
public class WCT_CreatePDFWS{
@Future( callout=true)
    public static void createPDFWSFunc(string sessionId, Id InterviewFormId, Id InterviewJunctionId){
        String EndPoint = Label.EndPoint;

        HttpRequest req = new HttpRequest(); 
        req.setEndpoint(EndPoint);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('SOAPAction', '""');
        req.setHeader('Authorization','OAuth '+SessionId);
        String strbody = '{  "InterviewFormId" : "'+InterviewFormId+'", "InterviewJunctionId" : "'+InterviewJunctionId+'" }';
        req.setBody(strbody);
        try{
            Http http = new Http();      
            HTTPResponse res = http.send(req);  
        }Catch(Exception ex)
        {
            WCT_ExceptionUtility.logException('CreatePDFWS-createPDFWSFunc', null, ex.getMessage() + ' ::: ' + ex.getStackTraceString());
        }
    }
}