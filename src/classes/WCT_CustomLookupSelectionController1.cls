public with sharing class WCT_CustomLookupSelectionController1 {

    public String compId { get; set; }

    public Contact account {
        get;
        set;
    } // new account to create
    public List < Contact > results {
        get;
        set;
    } // search results
    public List < Case > caseresults{
        get;
        set;
    } // search results
    public string searchString {
        get;
        set;
    } // search keyword
    public string ObjectName {
        get;
        set;
    }
    public String sObjectType {
        get;
        set;
    }
    public String strContactID {get;set;}
    public WCT_CustomLookupSelectionController1() {

        ObjectName = System.currentPageReference().getParameters().get('ObjType');
     //   objType = ApexPages.currentpage().getParameters().get('objectname');
        compId = ApexPages.currentpage().getParameters().get('namefield');
       
        searchString = System.currentPageReference().getParameters().get('lksrch');
        runSearch();
    }

    // performs the keyword search
    public PageReference search() {
        runSearch();
        return null;
    }

    // prepare the query and issue the search command
    private void runSearch() {
        // TODO prepare query string for complex serarches & prevent injections
        sObjectType = System.currentPageReference().getParameters().get('ObjType');
        if (sObjectType == 'case') {

            caseresults = performSearch(searchString);
        } else {
            results = performSearch(searchString);
        }
    }
    
    public void refSelectContact()
          {
             System.debug('***  strEmailTemplateID' + strContactID);
             Contact con;
             con = [SELECT id, name ,WCT_Type__c,WCT_Person_Id__c,WCT_Personnel_Number__c,WCT_Facility_Name__c,Email,Phone,WCT_Job_Text__c,WCT_Service_Area__c FROM Contact where id=: strContactID];
             //PageReference p1 = new PageReference('javascript:window.close();window.opener.location.reload()');
             //return p1;
          }

    // run the search and return the records found. 
    private List < Sobject > performSearch(string searchString) {
        sObjectType = System.currentPageReference().getParameters().get('ObjType');
        String soql;
        if (sObjectType == 'case') {
            soql = 'select id, CaseNumber, AccountId,Description from ' + sObjectType + '';
             if (searchString != '' && searchString != null)
                soql = soql + ' where CaseNumber LIKE \'%' + searchString + '%\'';
            soql = soql + ' limit 25';
            System.debug(soql);

        } else {
            soql = 'select id, name ,WCT_Type__c,WCT_Person_Id__c,WCT_Personnel_Number__c,WCT_Facility_Name__c,Email,Phone,Current_Employer__c,WCT_Candidate_School__c,WCT_Job_Text__c,WCT_Service_Area__c from ' + sObjectType + ' where Email != null';
            if (searchString != '' && searchString != null)
                soql = soql + ' AND name LIKE \'%' + searchString + '%\'';
            soql = soql + ' limit 25';
            System.debug(soql);
        }
        return database.query(soql);

    }

    // save the new account record
    public PageReference saveAccount() {
        insert account;
        // reset the account
        account = new Contact();
        return null;
    }

    // used by the visualforce page to send the link to the right dom element
    public string getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }

    // used by the visualforce page to send the link to the right dom element for the text box
    public string getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }

}