public class WCT_EINTV_IEFFormList_CNTRL {

    public List<IEFForm>  IEFForms{get; set;}
    
    public WCT_EINTV_IEFFormList_CNTRL()
    {
        IEFForms= new List<IEFForm>();
        List<Document>  IEFFormDocuments= fetchIEFForms();
        IEFForms=documentToIEFForm(IEFFormDocuments);
        
        
	}
    
    /*
    @desc Method to query the IEF Forms from the document Folder : "IEF Word Documents", "IEF Sample Question"
    @return : List of quired IEF Form (Document): List<Document>
	*/
    public List<Document> fetchIEFForms()
    {
	 List<Document> tempIEFForms= new List<Document>();
        /*Query the folder which contains IEF forms : "IEF sample Questions"  and "IEF Word Documents".*/
     List<Folder> IEFFolders= [select id, Name, type From Folder where type='Document' and Name in ('IEF sample Questions', 'IEF Word Documents')];
        if(IEFFolders.size()>0)
        {
            /*Query the document in the folder IEF sample Questions"  and "IEF Word Documents" ie.., IEF Forms. */
    	 tempIEFForms= [Select Id,Name, Description, Folder.Name,BodyLength, Type, CreatedById, LastModifiedDate From Document  where FolderId in :IEFFolders limit 1000];
        }
     return tempIEFForms;
    }
    
    public List<IEFForm> documentToIEFForm(List<Document> documents)
    {
        List<IEFForm> tempIEFForms= new List<IEFForm>();
        for(Document tempDocument : documents)
        {
            IEFForm form= new IEFForm(tempDocument.id, tempDocument.Name, tempDocument.Description , tempDocument.BodyLength , tempDocument.Type, tempDocument.Folder.Name);
            tempIEFForms.add(form);
        }
        return tempIEFForms;
    }
    public class IEFForm{
       public string name{get;set;}
       public string description{get;set;}
       public string bodyLength{get;set;}
       public string type{get;set;}
        public string id{get;set;}
        public string folderName{get;set;}
        
        public IEFForm(id docid, string tName, string tDescription,integer tBodyLength, string ttype, string tfolderName  )
        {
            id=docid;
            name=tName;
            description=tDescription;
            if(tBodyLength>0)
            {
               if(tBodyLength>1048576)
               {
                   bodyLength=String.valueOf(tBodyLength/1048576); 
                   bodyLength=bodyLength+' MB';  
                   
               }else if(tBodyLength > 1024)
               {
					bodyLength=String.valueOf(tBodyLength/1024); 
                   bodyLength=bodyLength+' KB';                   
               }
                else
                {
                    bodyLength=String.valueOf(tBodyLength);
                    bodyLength=bodyLength+' B';  
                }
            }
            
            type=ttype;
            folderName=tfolderName;
        }
        
    }
}