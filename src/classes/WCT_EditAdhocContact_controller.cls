public class WCT_EditAdhocContact_controller {
    
    //Contact Variables
    public id contactId {get;set;}
    public Contact contact {get;set;}
    public String strFirstName{get;set;}
    public String strLastName{get;set;}
    public String strMobile{get;set;}
    public String strHomePhone {get;set;}
    public String strEmail {get;set;}
    public String strSchoolName {get;set;}
    public String strSchoolNameId {get;set;}
    public String strOtherInstitution {get;set;}
    public String strCurrentEmployer {get;set;}
    public String strCurrentEmployerId {get;set;}
    public String strOtherEmployer {get;set;}
    public Boolean bIsAdhoc {get;set;}
    
    public String errorMsg{ get; set; }
    public Boolean showError { get; set; }
    
    public list<Note>  noteList{get;set;}
    public  string noteBody{get;set;}
    public WCT_EditAdhocContact_controller(){
        bIsAdhoc =false;
        contact = new contact();
        String strIsAdhoc= ApexPages.currentpage().getParameters().get('isAdhoc');
        if(strIsAdhoc=='Yes')
        {
           bIsAdhoc=true;  
        }
        contactId = ApexPages.currentpage().getParameters().get('contactId');
        contact = [Select id,Account.Name,Salutation, FirstName,LastName,Name,MobilePhone,Email,HomePhone,WCT_School_Education_Institution_lkp__c,
                        WCT_School_Education_Institution_lkp__r.name,WCT_CN_Other_Institution__c,WCT_Current_Employer_lkp__c,WCT_Current_Employer_lkp__r.name,WCT_CN_Other_Employer__c from Contact where id = :contactId];
        
        noteList= [select Title,id, body,IsPrivate from note where ParentId =:contactId ];
        system.debug('>>>>>>>nt  >>>>'+noteList);
        
        strFirstName = contact.FirstName;
        strLastName = contact.LastName;
        strMobile = contact.MobilePhone;
        strHomePhone = contact.HomePhone;
        strEmail = contact.Email;
        strSchoolName = contact.WCT_School_Education_Institution_lkp__r.name;
        strOtherInstitution = contact.WCT_CN_Other_Institution__c;
        strCurrentEmployer =  contact.WCT_Current_Employer_lkp__r.name;
        strOtherEmployer = contact.WCT_CN_Other_Employer__c;
    }
    
    public PageReference updateAdHocCandidate(){
    
      if(bIsAdhoc)
       {
        String emailRegex = '([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})';
        Pattern MyPattern = Pattern.compile(emailRegex);
        // Then instantiate a new Matcher object "MyMatcher"
        Matcher MyMatcher = MyPattern.matcher(strEmail);
    
        contact.FirstName = strFirstName;
        if(strLastName.length() == 0){
            showError = true;
            Apexpages.addmessage(new Apexpages.message(Apexpages.severity.FATAL,'You cannot Enter blank last name'));
            return null;
        }else{
        contact.LastName = strLastName;}
        contact.MobilePhone = strMobile;
        contact.HomePhone = strHomePhone;
        if(!MyMatcher.matches()){
            showError = true;
            Apexpages.addmessage(new Apexpages.message(Apexpages.severity.FATAL,'Please enter a valid email'));
            return null;
        }else{
        contact.Email  = strEmail;}
        if(strSchoolNameId.length()>0){
        contact.WCT_School_Education_Institution_lkp__c = strSchoolNameId.subString(0, 15);
        }else if(strSchoolNameId.length()<=0 && strSchoolName.length() == 0){
            contact.WCT_School_Education_Institution_lkp__c = null;
        }
        contact.WCT_CN_Other_Institution__c = strOtherInstitution;
        if(strCurrentEmployerId.length()>0){
        contact.WCT_Current_Employer_lkp__c = strCurrentEmployerId.subString(0, 15);
        }else if(strCurrentEmployerId.length()<=0 && strCurrentEmployer.length() == 0){
             contact.WCT_Current_Employer_lkp__c = null;
        }
        contact.WCT_CN_Other_Employer__c = strOtherEmployer;
        }
        try{
            update noteList;
         if(bIsAdhoc){
            update contact;
            }
            return new PageReference('/'+contactId);
        }catch(Exception e){
           showError = true;
        }
     return null;
    
    }
    public PageReference can() {
        return new PageReference('/'+contactId);
    }
     public void sub() {   
        if(errorMsg != null){
            String[] arrError = errorMsg.split(',');
            showError = true;
            for(String s:arrError){
                system.debug('Value of String is  ???????????????????'+ string.valueof(s));
                if(s != 'Test')
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,s));
            }
        }
    }
}