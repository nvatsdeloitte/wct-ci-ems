/*****************************************************************************************
    Name    : WCT_EditTaskButtonController_Test 
    Desc    : Test class for WCT_EditTaskButtonController class                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Deloitte        12/2/2013 12:01 PM         Created 
******************************************************************************************/
@isTest
public class WCT_EditTaskButtonController_Test {
  /****************************************************************************************
    * @author      - Jeetesh Bahuguna
    * @date        - 09/10/2013
    * @description - Test Method for all methods and wrapper class of  EditTaskButtonController.
    *****************************************************************************************/

public static String caseRecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get(WCT_UtilConstants.CASE_RECORDTYPE_PRE_BI).getRecordTypeId();
    public static Contact TestContact=new Contact();
    public static Case TestCase=new Case();
    public static Task TestTask=new Task();
    public static String TaskRecordTypeId=Schema.SObjectType.Task.getRecordTypeInfosByName().get('TR_Task').getRecordTypeId();
   
     /** 
        Method Name  : createContacts
        Return Type  : Contact
        Type         : private
        Description  : Create temp records for data mapping         
    **/
    private static Contact createContact()
    {
        Testcontact=WCT_UtilTestDataCreation.createContact();
        insert TestContact;
        return TestContact;
    }
    
     /** 
        Method Name  : Create Case
        Return Type  : Case
        Type         : private
        Description  : Create temp records for data mapping         
    **/
    private static case createCase()
    {
        TestCase =WCT_UtilTestDataCreation.createCase(TestContact.id);
        insert TestCase;
        return TestCase;
    }
    
    
     /** 
        Method Name  : CreateTask
        Return Type  : Contact
        Type         : private
        Description  : Create temp records for data mapping         
    **/
    private static Task createTask()
    {
        TestTask=WCT_UtilTestDataCreation.createTask(TestCase.id);
        insert TestTask;
        return TestTask;
    }
    
     /** 
        Method Name  : getTaskMethodWithException
        Return Type  : Void
        Type         : static testMethod
        Description  : It will capture the exception during update and if there is no task.       
    **/
    static testMethod void  getTaskMethodWithException()
    {
    
     Test.setMock(HttpCalloutMock.class, new WebServiceMockImpl()); 
     Contact con= createContact();
     Case  testCase1=createCase();
     Test.setCurrentPage(Page.WCT_EditTaskButton);
     system.currentPageReference().getParameters().put('caseId', testCase1.Id);
     WCT_EditTaskButtonController Controller= new WCT_EditTaskButtonController();
     Controller.updateTask();
     
   }
    
    
    
      /** 
        Method Name  : InsertFieldExceptionForActivity
        Return Type  : Void
        Type         : static testMethod
        Description  : It will capture the exception during insert       
    **/
    static testMethod void  InsertFieldExceptionForActivity()
    {
    
     Test.setMock(HttpCalloutMock.class, new WebServiceMockImpl()); 
     Contact con= createContact();
     Case  testCase1=createCase();
     Test.setCurrentPage(Page.WCT_EditTaskButton);
     Task  t1=createTask();
     test.starttest();
     Task t2= createTask();
     t1.RecordTypeId=TaskRecordTypeId;
     update t1;
     
     string tasksIds=t1.id+','+t2.id;
     Test.setCurrentPage(Page.WCT_EditTaskButton);
     system.currentPageReference().getParameters().put('TaskIDs', tasksIds);
     WCT_EditTaskButtonController ControllerFOrActivity= new WCT_EditTaskButtonController();
     WCT_EditTaskButtonController.cTask cTask1= new  WCT_EditTaskButtonController.cTask(t1); 
     WCT_EditTaskButtonController.cTask cTask2= new  WCT_EditTaskButtonController.cTask(t2); 
     WCT_EditTaskButtonController.cTask cTask3= new  WCT_EditTaskButtonController.cTask(); 
    //************** Covering insert call in update method with exception*********************************
     cTask1.tsk.Priority='None';
     ControllerFOrActivity.cNewTask.add(cTask1);
     ControllerFOrActivity.updateTask();
     test.stoptest();
   }
   
   
      /** 
        Method Name  : UpdateFieldExceptionForActivity
        Return Type  : Void
        Type         : static testMethod
          Description  : It will capture the exception during update       
   **/
    static testMethod void  UpdateFieldExceptionForActivity()
    {
    
     Test.setMock(HttpCalloutMock.class, new WebServiceMockImpl()); 
     Contact con= createContact();
     Case  testCase1=createCase();
     Test.setCurrentPage(Page.WCT_EditTaskButton);
     Task  t1=createTask();
     test.starttest();
     Task t2= createTask();
     t1.RecordTypeId=TaskRecordTypeId;
     update t1;
     
     string tasksIds=t1.id+','+t2.id;
    Test.setCurrentPage(Page.WCT_EditTaskButton);
     system.currentPageReference().getParameters().put('TaskIDs', tasksIds);
     WCT_EditTaskButtonController ControllerFOrActivity= new WCT_EditTaskButtonController();
     WCT_EditTaskButtonController.cTask cTask1= new  WCT_EditTaskButtonController.cTask(t1); 
     WCT_EditTaskButtonController.cTask cTask2= new  WCT_EditTaskButtonController.cTask(t2); 
     WCT_EditTaskButtonController.cTask cTask3= new  WCT_EditTaskButtonController.cTask(); 
    //************** Covering update call update method with exception*********************************
     cTask1.tsk.Priority='None';
     cTask2.tsk.Priority='None';
     cTask1.tsk.OwnerId=null;
     cTask2.tsk.OwnerId=null;
     ControllerFOrActivity.cTaskList = new list<WCT_EditTaskButtonController.cTask>();
     ControllerFOrActivity.cTrTask = new list<WCT_EditTaskButtonController.cTask>();
     ControllerFOrActivity.cTrTask.add(cTask1);
     ControllerFOrActivity.cTaskList.add(cTask2);
     ControllerFOrActivity.updateTask();
     test.stoptest();
    }
   
   
     /** 
        Method Name  : InsertFieldExceptionForCase
        Return Type  : Void
        Type         : static testMethod
        Description  : It will capture the exception during insert       
    **/
    static testMethod void  InsertFieldExceptionForCase()
    {
    
     Test.setMock(HttpCalloutMock.class, new WebServiceMockImpl()); 
     Contact con= createContact();
     Case  testCase1=createCase();
     Test.setCurrentPage(Page.WCT_EditTaskButton);
     Task  t1=createTask();
     test.starttest();
     Task t2= createTask();
     t1.RecordTypeId=TaskRecordTypeId;
     update t1;
     
     string tasksIds=t1.id+','+t2.id;
     Test.setCurrentPage(Page.WCT_EditTaskButton);
     system.currentPageReference().getParameters().put('caseId', testCase1.Id);
     WCT_EditTaskButtonController ControllerFOrActivity= new WCT_EditTaskButtonController();
     WCT_EditTaskButtonController.cTask cTask1= new  WCT_EditTaskButtonController.cTask(t1); 
     WCT_EditTaskButtonController.cTask cTask2= new  WCT_EditTaskButtonController.cTask(t2); 
     WCT_EditTaskButtonController.cTask cTask3= new  WCT_EditTaskButtonController.cTask(); 
    //************** Covering insert call in update method with exception*********************************
     cTask1.tsk.Priority='None';
     ControllerFOrActivity.cNewTask.add(cTask1);
     ControllerFOrActivity.updateTask();
     test.stoptest();
     
   }
   
   
      /** 
        Method Name  : UpdateFieldExceptionForCase
        Return Type  : Void
        Type         : static testMethod
          Description  : It will capture the exception during update       
   **/
    static testMethod void  UpdateFieldExceptionForCase()
    {
    
     Test.setMock(HttpCalloutMock.class, new WebServiceMockImpl()); 
     Contact con= createContact();
     Case  testCase1=createCase();
     Test.setCurrentPage(Page.WCT_EditTaskButton);
     Task  t1=createTask();
     test.starttest();
     Task t2= createTask();
     t1.RecordTypeId=TaskRecordTypeId;
     update t1;
     
     string tasksIds=t1.id+','+t2.id;
     Test.setCurrentPage(Page.WCT_EditTaskButton);
     system.currentPageReference().getParameters().put('caseId', testCase1.Id);
      WCT_EditTaskButtonController ControllerFOrActivity= new WCT_EditTaskButtonController();
     WCT_EditTaskButtonController.cTask cTask1= new  WCT_EditTaskButtonController.cTask(t1); 
     WCT_EditTaskButtonController.cTask cTask2= new  WCT_EditTaskButtonController.cTask(t2); 
     WCT_EditTaskButtonController.cTask cTask3= new  WCT_EditTaskButtonController.cTask(); 
    //************** Covering update call update method with exception*********************************
     cTask1.tsk.Priority='None';
     cTask2.tsk.Priority='None';
     cTask1.tsk.OwnerId=null;
     cTask2.tsk.OwnerId=null;
     ControllerFOrActivity.cTaskList = new list<WCT_EditTaskButtonController.cTask>();
     ControllerFOrActivity.cTrTask = new list<WCT_EditTaskButtonController.cTask>();
     ControllerFOrActivity.cTrTask.add(cTask1);
     ControllerFOrActivity.cTaskList.add(cTask2);
     ControllerFOrActivity.updateTask();
     test.stoptest();
    }
   
   
     
    
     /** 
        Method Name  : addRowMethod
        Return Type  : Void
        Type         : static testMethod
        Description  : It will capture the addmethod condition       
    **/
    static testMethod void addRowMethod()
    {
         Test.setMock(HttpCalloutMock.class, new WebServiceMockImpl());   
         Contact con= createContact();
         Case  testCase1=createCase();
         Task  testTask1=createTask();
         test.starttest();
         Test.setCurrentPage(Page.WCT_EditTaskButton);
         ApexPages.currentPage().getParameters().put('TaskIDs', TestTask1.id);
         WCT_EditTaskButtonController Et= new WCT_EditTaskButtonController();
         WCT_EditTaskButtonController.cTask controller = new WCT_EditTaskButtonController.cTask(testTask1);
         Et.cNewTask.add(controller);
         Et.addRow();
         Et.addRow();
         Et.updateTask();
         ApexPages.currentPage().getParameters().put('caseId', testCase1.id);
         WCT_EditTaskButtonController Et1= new WCT_EditTaskButtonController();
         WCT_EditTaskButtonController.cTask controller1 = new WCT_EditTaskButtonController.cTask(testTask1);
         Et1.cNewTask.add(controller);
         Et1.addRow();
         Et1.addRow();
         Et1.updateTask();
         test.stoptest();
   }
    
      /* * 
        Method Name  : CancelMethodCall
        Return Type  : Void
        Type         : static testMethod
        Description  : It will capture the code for Cancel method         
    **/
    static testMethod void CancelMethodCall()
    {
         Test.setMock(HttpCalloutMock.class, new WebServiceMockImpl());   
         Contact con= createContact();
         Case  testCase1=createCase();
         Test.setCurrentPage(Page.WCT_EditTaskButton);
         Task  testTask1=createTask();
         ApexPages.currentPage().getParameters().put('TaskIDs', testTask1.id);
         WCT_EditTaskButtonController controller1 = new WCT_EditTaskButtonController();
         controller1.cancel();
         ApexPages.currentPage().getParameters().put('caseId', testCase1.id);
         WCT_EditTaskButtonController controller = new WCT_EditTaskButtonController();
         controller.Cancel();
    }
    




 static testmethod void WCT_EditTaskButtonControllerTest()
    {
     
     Test.setMock(HttpCalloutMock.class, new WebServiceMockImpl()); 
     Contact con= createContact();
     Case  testCase1=createCase();
     Test.setCurrentPage(Page.WCT_EditTaskButton);
     Task  t1=createTask();
     test.starttest();
     Task t2= createTask();
     t1.RecordTypeId=TaskRecordTypeId;
     update t1;
     
     string tasksIds=t1.id+','+t2.id;
     Test.setCurrentPage(Page.WCT_EditTaskButton);
     system.currentPageReference().getParameters().put('TaskIDs', tasksIds);
     WCT_EditTaskButtonController ControllerFOrActivity= new WCT_EditTaskButtonController();
     WCT_EditTaskButtonController.cTask cTask1= new  WCT_EditTaskButtonController.cTask(t1); 
     WCT_EditTaskButtonController.cTask cTask2= new  WCT_EditTaskButtonController.cTask(t2); 
     WCT_EditTaskButtonController.cTask cTask3= new  WCT_EditTaskButtonController.cTask(); 
    //************** Covering update method*********************************
     ControllerFOrActivity.updateTask();
    //***********Covering AddRow Method*************************************
     ControllerFOrActivity.addRow();
     ControllerFOrActivity.addRow();
     ControllerFOrActivity.updateTask();
     test.stoptest();
    }
     
      static testmethod void WCT_EditTaskButtonControllerForCaseTest()
    {
     
     Test.setMock(HttpCalloutMock.class, new WebServiceMockImpl()); 
     
     Contact con= createContact();
     Case  testCase1=createCase();
     Task  t1=createTask();
     test.starttest();
     Task t2= createTask();
     t1.RecordTypeId=TaskRecordTypeId;
     update t1;
     
     string tasksIds=t1.id+','+t2.id;
     Test.setCurrentPage(Page.WCT_EditTaskButton);
     system.currentPageReference().getParameters().put('caseId', testCase1.Id);
     WCT_EditTaskButtonController ControllerForCase= new WCT_EditTaskButtonController();
     string s= ControllerForCase.sCaseId;
     system.assertEquals(s, testCase1.Id);
     WCT_EditTaskButtonController.cTask cTask1= new  WCT_EditTaskButtonController.cTask(t1); 
     WCT_EditTaskButtonController.cTask cTask2= new  WCT_EditTaskButtonController.cTask(t2); 
     WCT_EditTaskButtonController.cTask cTask3= new  WCT_EditTaskButtonController.cTask(); 
    //************** Covering update method*********************************
     ControllerForCase.updateTask();
    //***********Covering AddRow Method*************************************
     ControllerForCase.addRow();
     ControllerForCase.addRow();
     ControllerForCase.updateTask();
     test.stoptest();
     }
     
  
}