public class WCT_Eevents_TriggerHandler {


    
    public static void ImmUpdate(Set <Id> ImmIds) {
        // Query the relation records for the corresponding Immigratoin
           list<WCT_immigration__c> immList = new list<WCT_immigration__c>();
            immList = [select Id,WCT_Q_A_Session_attend__c,WCT_Q_A_session_Due_date__c from WCT_immigration__c where Id IN : ImmIds];
            for(WCT_immigration__c imm : immList){
                        imm.WCT_Q_A_Session_attend__c=true;
                        // imm.WCT_Q_A_session_Due_date__c = system.today();
            }
            if(!immList.isEmpty())
                update immList;
     }
     
     public static void ImmDelete(Set <Id> ImmdeleteIds) {
        // Query the relation records for the corresponding Immigratoin
           list<WCT_immigration__c> immdeleteList = new list<WCT_immigration__c>();
            immdeleteList = [select Id,WCT_Q_A_Session_attend__c,WCT_Q_A_session_Due_date__c from WCT_immigration__c where Id IN : ImmdeleteIds];
            for(WCT_immigration__c immdel : immdeleteList){
                immdel.WCT_Q_A_Session_attend__c=false;
                immdel.WCT_Q_A_session_Due_date__c = system.today();
            }
            if(!immdeleteList.isEmpty())
                update immdeleteList;
     }
    
    public static void ImmRevList(Set <Id> ImmrevIds) {
        // Query the relation records for the corresponding Immigratoin
           list<WCT_immigration__c> immaddList = new list<WCT_immigration__c>();
            immaddList = [select Id,WCT_Q_A_Session_attend__c from WCT_immigration__c where Id IN : ImmrevIds];
            for(WCT_immigration__c immadd : immaddList){
                immadd.WCT_Q_A_Session_attend__c=true;
                
            }
            if(!immaddList.isEmpty())
                update immaddList;
     }
     
     public static void ImmbriefdeleteList(Set <Id> ImmbriefdeleteIds) {
        // Query the relation records for the corresponding Immigratoin
           list<WCT_immigration__c> immdbriefdeleteList = new list<WCT_immigration__c>();
            immdbriefdeleteList = [select Id,WCT_Visa_Brief_Session__c from WCT_immigration__c where Id IN : ImmbriefdeleteIds];
            for(WCT_immigration__c immbriefdel : immdbriefdeleteList){
                        immbriefdel.WCT_Visa_Brief_Session__c=false;
             }
            if(!immdbriefdeleteList.isEmpty())
                update immdbriefdeleteList;
     }
     
     public static void ImmRevbriefdeleteList(Set <Id> RevImmbriefdeleteIds) {
        // Query the relation records for the corresponding Immigratoin
           list<WCT_immigration__c> revimmdbriefdeleteList = new list<WCT_immigration__c>();
            revimmdbriefdeleteList = [select Id,WCT_Visa_Brief_Session__c from WCT_immigration__c where Id IN : RevImmbriefdeleteIds];
            for(WCT_immigration__c immrevdel : revimmdbriefdeleteList){
                        immrevdel.WCT_Visa_Brief_Session__c=true;
             }
            if(!revimmdbriefdeleteList.isEmpty())
                update revimmdbriefdeleteList;
     }
     
        
    public static void MobUpdate(Set <Id> MobIds) {
        // Query the relation records for the corresponding Mobility
           list<WCT_mobility__c> mobList = new list<WCT_mobility__c>();
            
            mobList = [select Id,WCT_Q_A_Session_attended__c,WCT_Q_A_session_Due_date__c from WCT_mobility__c where Id IN : MobIds];
            for(WCT_mobility__c mob : mobList){
                        mob.WCT_Q_A_Session_attended__c=true;
                        // mob.WCT_Q_A_session_Due_date__c = system.today();
            }
            system.debug('mobList'+mobList);
            if(!mobList.isEmpty())
                update mobList;
     }
     
     public static void MobrevIdList(Set <Id> MobrevIds) {
        // Query the relation records for the corresponding Mobility
           list<WCT_mobility__c> mobrevList = new list<WCT_mobility__c>();
            
            mobrevList = [select Id,WCT_Q_A_Session_attended__c,WCT_Q_A_session_Due_date__c from WCT_mobility__c where Id IN : MobrevIds];
            for(WCT_mobility__c mobrev : mobrevList){
                        mobrev.WCT_Q_A_Session_attended__c=true;
                        //mobrev.WCT_Q_A_session_Due_date__c = system.today();
            }
            
            if(!mobrevList.isEmpty())
                update mobrevList;
     }
     
     public static void MobdeleteIdList(Set <Id> MobdeleteIds) {
        // Query the relation records for the corresponding Mobility
           list<WCT_mobility__c> mobdeleteList = new list<WCT_mobility__c>();
            
            mobdeleteList = [select Id,WCT_Q_A_Session_attended__c,WCT_Q_A_session_Due_date__c from WCT_mobility__c where Id IN : MobdeleteIds];
            for(WCT_mobility__c mobdel : mobdeleteList){
                mobdel.WCT_Q_A_Session_attended__c=false;
                mobdel.WCT_Q_A_session_Due_date__c = system.today();
            }
            
            if(!mobdeleteList.isEmpty())
                update mobdeleteList;
     }
     
     public static void MobbriefdeleteIdList(Set <Id> MobbriefdeleteIds) {
        // Query the relation records for the corresponding Mobility
           list<WCT_mobility__c> mobdbriefdeleteList = new list<WCT_mobility__c>();
            mobdbriefdeleteList = [select Id,WCT_Visa_Brief_Session__c from WCT_mobility__c where Id IN : MobbriefdeleteIds];
            for(WCT_mobility__c mobbriefdel : mobdbriefdeleteList){
                        mobbriefdel.WCT_Visa_Brief_Session__c=false;
             }
            if(!mobdbriefdeleteList.isEmpty())
                update mobdbriefdeleteList;
     }
     
     public static void MobRevbriefdeleteIdList(Set <Id> RevMobbriefdeleteIds) {
        // Query the relation records for the corresponding Mobility
            list<WCT_mobility__c> RevmobdbriefdeleteList = new list<WCT_mobility__c>();
            RevmobdbriefdeleteList = [select Id,WCT_Visa_Brief_Session__c from WCT_mobility__c where Id IN : RevMobbriefdeleteIds];
            for(WCT_mobility__c revmobbriefdel : RevmobdbriefdeleteList){
                        revmobbriefdel.WCT_Visa_Brief_Session__c=true;
             }
            if(!RevmobdbriefdeleteList.isEmpty())
                update RevmobdbriefdeleteList;
     }
     
   /*  public static void MobRevbriefdeleteIdList(Set <Id> RevMobbriefdeleteIds) {
        // Query the relation records for the corresponding Mobility
            list<WCT_mobility__c> RevmobdbriefdeleteList = new list<WCT_mobility__c>();
            RevmobdbriefdeleteList = [select Id,WCT_Visa_Brief_Session__c from WCT_mobility__c where Id IN : RevMobbriefdeleteIds];
            for(WCT_mobility__c revmobbriefdel : RevmobdbriefdeleteList){
                        revmobbriefdel.WCT_Visa_Brief_Session__c=true;
             }
            if(!RevmobdbriefdeleteList.isEmpty())
                update RevmobdbriefdeleteList;
     }*/
     
     public static void ImmTaskInsert(Set <Id> ImmIds) {
        
            list<task> taskList = new list<task>();
            taskList = [SELECT id,WCT_Is_Visible_in_TOD__c,WCT_Is_Closed_By_Override__c,Status from task where task.WhatId IN :ImmIds AND (task.subject='Attend Q&A session')];
            for(task t:taskList) {
                 t.WCT_Is_Visible_in_TOD__c = false;
                 t.WCT_Is_Closed_By_Override__c = true;
                 t.Status = 'Completed';
                 t.QA_Task_Completed_Date__c = system.today();
            }
            if(!taskList.isEmpty()) {
               // update taskList;
                Database.update(taskList);  
            }
     }
     
     // Event Registration
     public static void MobTaskInsert(Set <Id> MobIds) {
        
            list<task> taskList1 = new list<task>();
            taskList1 = [SELECT id,WCT_Is_Visible_in_TOD__c,WCT_Is_Closed_By_Override__c,Status from task where task.WhatId IN :MobIds AND (task.subject='Attend Q&A session')];
            for(task t1:taskList1) {
                 t1.WCT_Is_Visible_in_TOD__c = false;
                 t1.WCT_Is_Closed_By_Override__c = true;
                 t1.Status = 'Completed';
                 t1.QA_Task_Completed_Date__c = system.today();
            }
            if(!taskList1.isEmpty()) {
               // update taskList;
                Database.update(taskList1);  
            }
     }
     
     public static void ImmTaskUpdate(Set <Id> ImmUpdateIds) {
        
            list<task> taskList2 = new list<task>();
            taskList2 = [SELECT id,WCT_Is_Visible_in_TOD__c,WCT_Is_Closed_By_Override__c,Status from task where task.WhatId IN :ImmUpdateIds AND (task.subject='Attend Q&A session')];
            for(task t2:taskList2) {
                 t2.WCT_Is_Visible_in_TOD__c = false;
                 t2.WCT_Is_Closed_By_Override__c = true;
                 t2.Status = 'Completed';
                 t2.QA_Task_Completed_Date__c = system.today();
            }
            if(!taskList2.isEmpty()) {
               // update taskList;
                Database.update(taskList2);  
            }
     }
     
     public static void MobTaskUpdate(Set <Id> MobupdateIds) {
        
            list<task> taskList3 = new list<task>();
            taskList3 = [SELECT id,WCT_Is_Visible_in_TOD__c,WCT_Is_Closed_By_Override__c,Status from task where task.WhatId IN :MobupdateIds AND (task.subject='Attend Q&A session')];
            for(task t3:taskList3) {
                 t3.WCT_Is_Visible_in_TOD__c = false;
                 t3.WCT_Is_Closed_By_Override__c = true;
                 t3.Status = 'Completed';
                 t3.QA_Task_Completed_Date__c = system.today();
            }
            if(!taskList3.isEmpty()) {
               // update taskList;
                Database.update(taskList3);  
            }

     }
     
     public static void ImmcntInsert(list <Id> ImmcntIds) {
        
            list<WCT_Immigration__c> ImmigrationList = new list<WCT_Immigration__c>();
            ImmigrationList = [Select Id,WCT_Unattend_event_Count__c from WCT_Immigration__c Where Id IN :ImmcntIds];
            system.debug('test3'+ImmigrationList);
            for(WCT_Immigration__c tmpimm:ImmigrationList) {
            
                 if(tmpimm.WCT_Unattend_event_Count__c == null)
                     tmpimm.WCT_Unattend_event_Count__c = 1;
                 else    
                     tmpimm.WCT_Unattend_event_Count__c += 1;
            }   
            system.debug('test2'+ImmigrationList);  
            if(!ImmigrationList.isEmpty()) {
               // update ImmList;
                Database.update(ImmigrationList);  
            }

     }
     
     public static void ImmcntUpdate(list <Id> UpdateImmcntIds) {
        
            list<WCT_Immigration__c> ImmigrationList1 = new list<WCT_Immigration__c>();
            ImmigrationList1 = [Select Id,WCT_Unattend_event_Count__c from WCT_Immigration__c Where Id IN :UpdateImmcntIds];
            for(WCT_Immigration__c tmpimm1:ImmigrationList1) {
                if(tmpimm1.WCT_Unattend_event_Count__c == null)
                     tmpimm1.WCT_Unattend_event_Count__c = 1;
                 else 
                 tmpimm1.WCT_Unattend_event_Count__c -= 1;
            }     
            system.debug('test3'+ImmigrationList1); 
            if(!ImmigrationList1.isEmpty()) {
               // update ImmList;
                Database.update(ImmigrationList1);  
            }

     }
     
     public static void MobcntInsert(list <Id> MobcntIds) {
        
            list<WCT_Mobility__c> MobilityList = new list<WCT_Mobility__c>();
            MobilityList = [Select Id,WCT_Unattend_Mobility_Event_Count__c from WCT_Mobility__c Where Id IN :MobcntIds];
            for(WCT_Mobility__c tmpmob:MobilityList) {
                 if(tmpmob.WCT_Unattend_Mobility_Event_Count__c == null)
                     tmpmob.WCT_Unattend_Mobility_Event_Count__c = 1;
                 else    
                     tmpmob.WCT_Unattend_Mobility_Event_Count__c += 1;
                 
            }     
            if(!MobilityList.isEmpty()) {
               // update ImmList;
                Database.update(MobilityList);  
            }

     }
     
     public static void MobcntUpdate(list <Id> UpdateMobcntIds) {
        
            list<WCT_Mobility__c> MobilityList1 = new list<WCT_Mobility__c>();
            MobilityList1 = [Select Id,WCT_Unattend_Mobility_Event_Count__c from WCT_Mobility__c Where Id IN :UpdateMobcntIds];
            for(WCT_Mobility__c tmpmob1:MobilityList1) {
                 if(tmpmob1.WCT_Unattend_Mobility_Event_Count__c == null)
                     tmpmob1.WCT_Unattend_Mobility_Event_Count__c = 1;
                 else 
                 tmpmob1.WCT_Unattend_Mobility_Event_Count__c -= 1;
                 
            }     
            if(!MobilityList1.isEmpty()) {
               // update ImmList;
                Database.update(MobilityList1);  
            }

     }
     
     public static void updateEventInfo(Set <Id> eventIdsUpdate) {
        // Query the relation records for the corresponding event registration
            List<Event_Registration_Attendance__c> listRelationRecords = new List<Event_Registration_Attendance__c>();
            listRelationRecords = [
                                      SELECT
                                          Id,
                                          Event__c, 
                                          Is_Event_Information_Changed__c 
                                       
                                      FROM
                                          Event_Registration_Attendance__c
                                      WHERE
                                          Event__c IN :eventIdsUpdate
                                      LIMIT
                                          :eventIdsUpdate.size()
                                  ];
             List<Event_Registration_Attendance__c> listToUpdatedRecords = new List<Event_Registration_Attendance__c>();
             if(!listRelationRecords.isEmpty())
             {
                for(Event_Registration_Attendance__c tempEveReg:listRelationRecords){
                    tempEveReg.Is_Event_Information_Changed__c = true;
                    listToUpdatedRecords.add(tempEveReg);
                }
                if(!listToUpdatedRecords.isEmpty()){
                    update listToUpdatedRecords;        
                }
             }
     }
     
     // Added the method to set the course number on event registration
    public static void populatecourseNumber(List<Event_Registration_Attendance__c> lsteveReg){
       
       String GMIRecordTypeId=Schema.SObjectType.Event_Registration_Attendance__c.getRecordTypeInfosByName().get('GM&I').getRecordTypeId();
       

        List<eEvent_GMI_Course_Numbers__c> listIds = eEvent_GMI_Course_Numbers__c.getAll().values();
        Map<String,string> MapIds = new Map<string,string>();
        for(eEvent_GMI_Course_Numbers__c eEventIds:listIds) {
            MapIds.put(eEventIds.Name,eEventIds.eEvent_GMI_Course_Numbers__c);
        }
        
        
        for(Event_Registration_Attendance__c tmpeveReg : lsteveReg){
            if(tmpeveReg.recordtypeId == GMIRecordTypeId) {
                if(tmpeveReg.WCT_Visa_type__c != null){
                             
                    tmpeveReg.WCT_GMI_Course_Number__c = MapIds.get(tmpeveReg.WCT_Visa_type__c);
                    
                }
            }
        }
     }
     
     // populating AR formatted data for displaying in email templates
      public static void populateARformattedeventinfo(List<Event__c> lstEvent){
        
    for(Event__c tmpEve : lstEvent){
        if(tmpEve.AR_Start_Date_Time_Text__c != null){
                
            
            string Dtt = tmpEve.AR_Start_Date_Time_Text__c;
            if(Dtt.contains(' ')){
            String[] str = dtt.split(' ');
            String[] dts = str[0].split('/');
            String[] dtss = str[1].split(':');
            system.debug('test159'+dtss.size());
            if(dtss.size() < 3){ 
                dtss.add('00'); 
            }
            
            if(!dtss.isEmpty()) {
            
            
          /*
            datetime  incorrectstartGMT= datetime.newinstanceGMT(Integer.valueOf(dts[2]), Integer.valueOf(dts[0]), Integer.valueOf(dts[1]),Integer.valueOf(dtss[0]),Integer.valueOf(dtss[1]),Integer.valueOf(dtss[2]));
            system.debug('test666'+incorrectstartGMT);
            Map<String,AR_timezones__c> allzones = AR_timezones__c.getAll();
            datetime  sitestartdate1= incorrectstartGMT.addhours((integer)allzones.get(tmpEve.Time_Zone__c).hours__c);
            
          */
         
           datetime incorrectstartGMT= datetime.newinstanceGMT(Integer.valueOf(dts[2]), Integer.valueOf(dts[0]), Integer.valueOf(dts[1]),Integer.valueOf(dtss[0]),Integer.valueOf(dtss[1]),Integer.valueOf(dtss[2]));
            Map<String,AR_timezones__c> allzones = AR_timezones__c.getAll();
            Map<String,Ar_time_zones_standard__c> allzones1 = Ar_time_zones_standard__c.getAll();
            datetime  sitestartdate1;
            if(tmpEve.Time_Zone__c <>''&&(dts[0]=='3'||dts[0]=='4'||dts[0]=='5'||dts[0]=='6'||dts[0]=='7'||dts[0]=='8'||dts[0]=='9'||dts[0]=='10'||dts[0]=='03'||dts[0]=='04'||dts[0]=='05'||dts[0]=='06'||dts[0]=='07'||dts[0]=='08'||dts[0]=='09')) {
            sitestartdate1 = incorrectstartGMT.addhours((integer)allzones.get(tmpEve.Time_Zone__c).hours__c);
            }
            else if(tmpEve.Time_Zone__c <>''&&(dts[0]=='11'||dts[0]=='12'||dts[0]=='1'||dts[0]=='2'||dts[0]=='01'||dts[0]=='02')) {
            sitestartdate1 = incorrectstartGMT.addhours((integer)allzones1.get(tmpEve.Time_Zone__c).hours_S__c);
            }
            
            // system.debug('test789'+sitestartdate1);
            tmpEve.WCT_AR_Formatted_Event_date__c = sitestartdate1.format('EEEE, MMMM d, yyyy');
            
          }  
        }
        }
        if(tmpEve.AR_Start_Date_Time_Text__c != null && tmpEve.AR_End_Date_Time_Text__c != null){
            if(tmpEve.AR_Start_Date_Time_Text__c.contains(' ') && tmpEve.AR_End_Date_Time_Text__c.contains(' ')){
            Datetime dt1 = DateTime.valueOf((tmpEve.AR_Start_Date_Time_Text__c).replaceAll('/','-'));
            Datetime dt2 = DateTime.valueOf((tmpEve.AR_End_Date_Time_Text__c).replaceAll('/','-'));
            if(dt1 != null || dt2 != null) {
            tmpEve.WCT_AR_Formatted_Event_timing__c = dt1.format('hh:mm a')+' - '+dt2.format('hh:mm a');
            }
         }   
        }
        if(tmpEve.AR_RSVP_Deadline__c != null){
            tmpEve.WCT_AR_Dead_line_Date_formatted__c = tmpEve.AR_RSVP_Deadline__c.format('MMMMM d');

        }
    }
    
 }

     // populating Error msg
    public static void checkfornumber(List<Event__c> eventLst){
     
     for(Event__c tmpEve : eventLst){
        if(tmpEve.AR_Start_Date_Time_Text__c != null){
     
    Pattern MyPattern = Pattern.compile('(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])/((19|20)\\d\\d) ([0-1]?[0-9]|[2][0-3]):([0-5][0-9]):([0-7][0-9])');
    Matcher MyMatcher = MyPattern.matcher(tmpEve.AR_Start_Date_Time_Text__c);
        if(!MyMatcher.matches()){
            tmpEve.addError('Error: Start Date/Time format should be mm/dd/yyyy hh:mm:ss military format');
        }
        }
        if(tmpEve.AR_End_Date_Time_Text__c != null){
     
    Pattern MyPattern = Pattern.compile('(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])/((19|20)\\d\\d) ([0-1]?[0-9]|[2][0-3]):([0-5][0-9]):([0-7][0-9])');
    Matcher MyMatcher = MyPattern.matcher(tmpEve.AR_End_Date_Time_Text__c);
        if(!MyMatcher.matches()){
            tmpEve.addError('Error: End Date/Time format should be mm/dd/yyyy hh:mm:ss military format');
        }
        }
        }
    
    }
    
     
    
     
 
     
      
      
     
}