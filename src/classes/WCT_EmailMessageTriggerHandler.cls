public class WCT_EmailMessageTriggerHandler{
    
    public void checkEmailWithRole(EmailMessage message){
        
        User u = [Select id,userRole.DeveloperName,userRoleId from User where Id=:userinfo.getUserId()];
        System.debug('thanks'+u.userRole.DeveloperName);
        List<WCT_List_Of_Names__c> emailsAssociatedWithRoles =[SELECT WCT_Email_Address__c,isRoleActive__c FROM WCT_List_Of_Names__c WHERE WCT_Role_DeveloperName__c =:u.userRole.DeveloperName and isRoleActive__c = true];
        //WCT_List_Of_Names__c emailsAssociatedWithRoles;
        String[] emailAddresses;
        
        try{
            System.debug('Count of SOQL returned-->'+emailsAssociatedWithRoles.size());
            if(!emailsAssociatedWithRoles.IsEmpty()){
                for(WCT_List_Of_Names__c queryEmailAddress : emailsAssociatedWithRoles)
                {
                    System.debug('EmailAddress'+queryEmailAddress);
                    if(queryEmailAddress.WCT_Email_Address__c != null)
                        emailAddresses = queryEmailAddress.WCT_Email_Address__c.split(';');
                    
                }
            }
        }catch(QueryException e){
            System.debug('SOQL for emailsAssociatedWithRoles returned no results--'+e);
        }
        Set<String> emailsAddressSet=new Set<String>();
        emailsAddressSet.add(userInfo.getUserEmail());
        
        if(emailAddresses!= null){
            for(String email : emailAddresses)
            {
                emailsAddressSet.add(email);
            }
        }
        if(emailsAddressSet==null || !emailsAddressSet.contains(message.FromAddress))
        {
            message.addError('You are not allowed to use current From Email Address.');
        }
    }
    
    public set<String> setCaseSubject = new set<String>();
    
    
    public void CheckExistingCase(EmailMessage newEmail ){
        String emailSubj = newEmail.Subject;
        Id caseId = newEmail.ParentId;      //Recording caseId to delete a Case associated with newEmail if the email will attach to existing Case.
        String startWithString = 'RE:';
        
        if(emailSubj!=null ){ 
            setCaseSubject.add(emailSubj);
            if( ((emailSubj.toUpperCase()).startsWith(startWithString))) {
                emailSubj = emailSubj.replace('RE:','');    // Outlook
                emailSubj = emailSubj.replace('Re:','');    // Gmail
                emailSubj = emailSubj.replace('re:','');
                emailSubj = emailSubj.trim();               
                setCaseSubject.add(emailSubj);
            }
        }
        String contactEmail=null;
        Case caseRec;
        List<Case> oldCases=new List<Case>();
        
        if(caseId!=null) {
            caseRec=[Select contact.Email,Origin,Status,Unread_Email_on_Closed_Case__c,IsNew__c,WCT_isEmailonCase__c from Case where id=:caseId ]; 
            if(caseRec.contact.email!=null)
                contactEmail=caseRec.contact.Email;
            
        }       
        if(!setCaseSubject.IsEmpty() && contactEmail!=null) {
            
            // check old case
            /*oldCases=[Select id,Unread_Email_on_Closed_Case__c,WCT_isEmailonCase__c from case where Subject IN: setCaseSubject and contact.Email =:contactEmail and id!=:caseId order by createddate desc limit 1];
            if(!oldCases.isEmpty() && (((newEmail.subject).toUpperCase()).startsWith(startWithString)))
            {
            newEmail.ParentId=oldCases[0].id;
            Boolean tempFlag=false;
            if(!oldCases[0].Unread_Email_on_Closed_Case__c){
            oldCases[0].Unread_Email_on_Closed_Case__c=true;
            tempFlag=true;
            }
            
            if(!oldCases[0].WCT_isEmailonCase__c) {
            oldCases[0].WCT_isEmailonCase__c=true;
            tempFlag=true;
            }
            if(tempFlag)
            update oldCases[0];
            //delete caseRec;
            }
            else
            {*/
            
            Boolean tempFlagCase=false;
            if(!caseRec.Unread_Email_on_Closed_Case__c){
                caseRec.Unread_Email_on_Closed_Case__c=true;
                tempFlagCase=true;
            }  
            
            if(!caseRec.IsNew__c){
                caseRec.IsNew__c=true;
                tempFlagCase=true;
            }
            if(!caseRec.WCT_isEmailonCase__c){
                caseRec.WCT_isEmailonCase__c = true;
                tempFlagCase=true;
            }
          /* case status should be reopened when new mail comes to the resolved cases(immigration only-- For deloitte contact  */
            if(caseRec.Origin != null && caseRec.Origin != '')
               {
                 if(caseRec.Status == 'Resolved' && caseRec.Origin=='GMI - Immigration' )
                   {
                    caseRec.Status= 'Reopened';
                    tempFlagCase=true;
                    }
             }  
          
            if(tempFlagCase){
                update caseRec;
            }
          
        }
       /* case status should be reopened when new mail comes to the resolved cases(immigration only-- For non deloitte contact  */
        
        if(!setCaseSubject.IsEmpty() && contactEmail==null && caseRec.Origin=='GMI - Immigration') {
          if(caseRec.Origin != null && caseRec.Origin != '')
            {
               if(caseRec.Status == 'Resolved' )
                {   
                  Boolean tempFlagCase=false;
                  if(caseRec.Origin != null && caseRec.Origin != '')
                      {
                        if(caseRec.Status == 'Resolved' && caseRec.Origin=='GMI - Immigration' )
                          {
                           caseRec.Status= 'Reopened';
                           tempFlagCase=true;
                          }
                       }  
                                  
                    if(tempFlagCase){
                        update caseRec;
                    }
                }
            }
        }
    
    }
    
   
}