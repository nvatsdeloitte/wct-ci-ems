public with sharing class WCT_EmployeeAttendance
{    
    public Event eventRec{get;set;}   
    public List<WCT_Attendance_Tracker__c> ShowList {get;set;} 
    public List<EventRelation> eventRelationList{get;set;}    
    public List<WCT_Attendance_Tracker__c> lstAttendance{get;set;}
    public WCT_EmployeeAttendance(ApexPages.StandardController controller) 
    {                
        eventRec=(Event) controller.getRecord();        
        eventRelationList=new list<EventRelation>();
        lstAttendance = new List <WCT_Attendance_Tracker__c>();
        WCT_Attendance_Tracker__c attendance;
        ShowList = new List<WCT_Attendance_Tracker__c>();
        lstAttendance= [select WCT_Employee__c,WCT_Employee__r.name,WCT_Event_ID__c,WCT_Attend__c,WCT_status__c,WCT_Response__c,WCT_Event_Start_Date__c,WCT_Event_End_Date__c from WCT_Attendance_Tracker__c where WCT_Event_ID__c = :eventRec.Id ];        
        if(lstAttendance.isEmpty())
        {
            eventRelationList=[SELECT Relation.Name,CreatedById,CreatedDate,EventId,Id,IsDeleted,LastModifiedById,LastModifiedDate,RelationId,RespondedDate,Response,Status,SystemModstamp,Event.StartDateTime,Event.EndDateTime,Event.Subject FROM EventRelation where eventId=:eventRec.Id AND IsInvitee = true order by status];
            for(EventRelation tmpEventRelation : eventRelationList)
            {
                attendance = new WCT_Attendance_Tracker__c();
                attendance.WCT_Employee__c = tmpEventRelation.RelationId;
                attendance.WCT_Event_ID__c=tmpEventRelation.EventId;
                attendance.WCT_status__c =tmpEventRelation.Status;
                attendance.WCT_Response__c =tmpEventRelation.Response;
                attendance.WCT_Event_Start_Date__c = tmpEventRelation.Event.StartDateTime;
                attendance.WCT_Event_End_Date__c = tmpEventRelation.Event.EndDateTime;
                attendance.WCT_Event_Subject__c = tmpEventRelation.Event.Subject;
                lstAttendance.add(attendance);
                
               // ShowList.add(attendance);
            }
        }
    }

    public void markAttendance()
    {
        upsert lstAttendance;
       // return new pagereference('/'+eventRec.id);
    }
    
    
}