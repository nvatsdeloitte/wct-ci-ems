public class WCT_Enroll_Benefits_FormController extends SitesTodHeaderController{
    public task taskRecord{get;set;}
    public WCT_Task_Reference_Table__c taskRefRecord{get;set;}
    public String taskSubject {get;set;}
    public String taskid{get;set;}
    public boolean display{get;set;}
    public boolean checked{get;set;}
    public String taskVerbiage{get;set;}
    
     /* public variables */
    public WCT_Mobility__c MobilityRec {get;set;}
    
    // Error Message related variables
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}
    
    public WCT_Enroll_Benefits_FormController()
    {
         /*Initialize all Variables*/
        init();
        
        taskid = ApexPages.currentPage().getParameters().get('taskid');
        display=true;
        checked=false;
        taskSubject = '';
        taskVerbiage = '';
    }

    private void init(){

        //Custom Object Instances
        MobilityRec = new WCT_Mobility__c();

    }   
    public void updateTaskFlags()
    {
        if(taskid==''|| taskid==null){
            display=false;
            return;
        }

        taskRecord=[SELECT id, Subject, WhatId, Ownerid, Status, WCT_Is_Visible_in_TOD__c, WCT_Auto_Close__c, WCT_Task_Reference_Table_ID__c FROM Task WHERE Id =: taskid];
        taskRefRecord = [SELECT Id, Name, WCT_Task_for_Object__c, WCT_Task_Subject__c, Form_Verbiage__c, ownerId FROM WCT_Task_Reference_Table__c Where id=:taskRecord.WCT_Task_Reference_Table_ID__c];
        MobilityRec = [SELECT Id, Name, OwnerId FROM WCT_Mobility__c WHERE Id=:taskRecord.WhatId];

        //Get Task Subject
        taskSubject = taskRecord.Subject;
        taskVerbiage = taskRefRecord.Form_Verbiage__c;

    }
    
    public pageReference save()
    {

        /*Custom Validation Rules are handled here*/

        if(checked != true)
        {
            pageErrorMessage = 'Please check the checkbox in order to submit the form.';
            pageError = true;
            return null;
        }

       // update MobilityRec;
            
        /*Changing the Owner Id of Task Temporarily to avoid Integrity Exception*/
        taskRecord.OwnerId=UserInfo.getUserId();
        upsert taskRecord;

        if(string.valueOf(MobilityRec.OwnerId).startsWith('00G')){
            taskRecord.OwnerId = System.Label.GMI_User;
        }else{
            taskRecord.OwnerId = MobilityRec.Ownerid;
        }
        

        //updating task record
        if(taskRecord.WCT_Auto_Close__c == true){   
            taskRecord.status = 'Completed';
        }else{
            taskRecord.status = 'Employee Replied';  
        }

        taskRecord.WCT_Is_Visible_in_TOD__c = false; 
        upsert taskRecord;
        
        //Reset Page Error Message
        pageError = false;
        
        /*Set Page Reference After Save*/
        return new PageReference('/apex/WCT_Enroll_Benefits_ThankYou?em='+CryptoHelper.encrypt(LoggedInContact.Email)+'&taskid='+taskid);  
    }
}