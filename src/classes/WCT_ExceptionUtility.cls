global class WCT_ExceptionUtility{

    public static void logException(string className, string pageName, string exceptionMessage){
        
        Exception_Log__c exc= new Exception_Log__c();
        exc.className__c = className;
        exc.pageName__c = pageName;
        exc.Detailed_Exception__c = exceptionMessage;
        exc.Exception_Date_and_Time__c = system.now();
        exc.Running_User__c = UserInfo.getUserId();
        
        insert exc;
    
    }
    
    public static Exception_Log__c returnExcLog(string className, string pageName, string exceptionMessage){
        Exception_Log__c exc= new Exception_Log__c();
        exc.className__c = className;
        exc.pageName__c = pageName;
        exc.Detailed_Exception__c = exceptionMessage;
        exc.Exception_Date_and_Time__c = system.now();
        exc.Running_User__c = UserInfo.getUserId();
        return exc; 
    }

}