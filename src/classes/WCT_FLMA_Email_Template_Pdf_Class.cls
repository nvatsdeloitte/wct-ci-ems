global with sharing class WCT_FLMA_Email_Template_Pdf_Class 
{
     
    WebService static string InsertEmailAttachment(String id)
        {
            String leaveid;
            leaveid=id;
            WCT_Leave__c leavRec = [SELECT ID,WCT_Employee__r.Name from WCT_Leave__c where ID =: leaveid limit 1  ];
            pageReference pdf = Page.WCT_FLMA_Email_Template_Pdf;
            pdf.getParameters().put('id',leaveid);
            Blob body;
            try {
                body =  pdf.getContent();
            } catch (VisualforceException e) {
                body = Blob.valueOf('Some Text');
            }
            Attachment a = new Attachment(parentId = leaveid, body = body ,Name='FMLA Letter '+leavRec.WCT_Employee__r.Name +'.doc');
            try{
            insert a;
            return 'true';
            }catch(exception ex){
            return 'false';
            }
            
        }
        
}