public class WCT_FMLA_Search_Controller {


/*

Logic and other details
#. Search the key string in Name filed  in  Attachment And Subject Field in Task.
#. Separate pagination for each object type results.
#. Using the SOQL for Quering.
#. Logic behind the Pagination : 
    Finding the total record present. Spliting these records into equal number of blocks using the fixed limit value (say 200). These each block refers to page from now. Querying  the record of each Page using the offset and limit attributes in SOQL. 
#. Hiding the Next and Previous button not next page or previous page is available. 
#. Logic for Showing next button :
   TotalPages Are greater than One, And CurrentPage is less than (TotalPage-1)
# Logic for showing prev button : 
   TotalPages Are greater than One, And CurrentPage is greater than zero.
# Each search click will result in 4 SOQL execution.
# Each next, Prev click will result in 1 SOQL execution. 

*/
    /*Data*/
public string searchText {get;set;}
public string StartDate {get;set;}
public string EndDate {get;set;}

public string searchTextView {get;set;}
public string StartDateView {get;set;}
public string EndDateView {get;set;}

public integer limitsize=250;
    public List<Task>  taskSearchResult{get{
        if(taskSearchResult==null)
        {taskSearchResult= new List<Task>();}
        return taskSearchResult;
    }
        
        
     set;}

public List<Attachment>  attachmentSearchResult{get{
        if(attachmentSearchResult==null)
        {attachmentSearchResult= new List<Attachment>();}
        return attachmentSearchResult;
    } set;}
    
    public integer taskCurrentPage{get{if(taskCurrentPage==null){taskCurrentPage=0;}return taskCurrentPage;}   set;}
    public integer attachmentCurrentPage{get{if(attachmentCurrentPage==null){attachmentCurrentPage=0;}return attachmentCurrentPage;}  set;}
    
    public integer taskTotalPage{get{if(taskTotalPage==null){taskTotalPage=0;}return taskTotalPage;}   set;}
    public integer attachmentTotalPage{get{if(attachmentTotalPage==null){attachmentTotalPage=0;}return attachmentTotalPage;}  set;}
    
    public boolean attachmentNextStatus{get{ return canDoNext('Attachment') ;}  set;}
    public boolean taskNextStatus{get{  return canDoNext('Task') ;}  set;}
    
    public boolean attachmentPrevStatus{get{ return canDoPrev('Attachment') ;}  set;}
    public boolean taskPrevStatus{get{ return canDoPrev('Task') ;}  set;}
    
    public boolean firstSearchDone{get{if(firstSearchDone==null)firstSearchDone=false; return firstSearchDone;} set;}
    
    
 /*Process */   
    public void searchNow()
    {
    
    /*Getting string from view t*/
    
    searchText =searchTextView ;
    StartDate  =StartDateView ;
    EndDate =  EndDateView ;
        /*Search in the Task SObject*/       
       taskSearchResult= queryTaskObject(0);
        /*Search in the Attachment SObject*/
      attachmentSearchResult=  queryAttachmentObject(0);
      
      /*Total Number of Pages :: */
      
      attachmentTotalPage=totalPagesForAttachment();
     taskTotalPage= totalPagesForTask();
     /*Reset the Current Page*/
     taskCurrentPage=0;
     attachmentCurrentPage=0;
     
     firstSearchDone=true;
     
    }
    
    public List<Task> queryTaskObject(integer offset)
    {
        
        List<Task> tempTaskResult;
        
        if(searchText != null && searchText != '') {
                        
            String searchTextW='%'+searchText+'%';
             if(startdate != '' && enddate != '')
                 {
                             
                    date sdate= date.parse(startdate);
                    date edate= date.parse(enddate);
                    edate= edate.adddays(1);
                        tempTaskResult = [Select Id, Subject,CreatedDate From Task Where Subject Like :searchTextW And CreatedDate >=:Sdate And CreatedDate <=:Edate  ORDER BY Createddate LIMIT :limitsize OFFSET :offset];
                  }
             else if(startdate !='' && enddate == '')
                  {
                               
                  date sdate= date.parse(startdate);
                  tempTaskResult = [Select Id, Subject,CreatedDate From Task Where Subject Like :searchTextW And CreatedDate >=:Sdate  ORDER BY Createddate LIMIT :limitsize OFFSET :offset];
                  }
          else if(startdate =='' && enddate != '')
                  {
                                
                    date edate= date.parse(enddate);
                    edate= edate.adddays(1);
                    tempTaskResult = [Select Id, Subject,CreatedDate From Task Where Subject Like :searchTextW And CreatedDate <=:Edate  ORDER BY Createddate LIMIT :limitsize OFFSET :offset];
                  }
            else 
                {
                 
                  tempTaskResult = [Select Id, Subject,CreatedDate From Task Where Subject Like :searchTextW ORDER BY Createddate LIMIT :limitsize OFFSET :offset];
                }
        }
        else
        {
              
        }
        
        return tempTaskResult;
    }
   
     public List<Attachment> queryAttachmentObject(integer offset)
    {
        
        List<Attachment> tempAttachmentResult;
        
        if(searchText != null && searchText != '') {
                       
            String searchTextW='%'+searchText+'%';
             if(startdate != '' && enddate != '')
                 {
                              
                    date sdate= date.parse(startdate);
                    date edate= date.parse(enddate);
                    edate= edate.adddays(1);
                        tempAttachmentResult= [Select Id, Name,CreatedDate From Attachment Where Name Like :searchTextW And CreatedDate >=:Sdate And CreatedDate <=:Edate  ORDER BY Createddate LIMIT :limitsize OFFSET :offset];
                  }
             else if(startdate !='' && enddate == '')
                  {

                  date sdate= date.parse(startdate);
                  
                  tempAttachmentResult= [Select Id, Name,CreatedDate From Attachment Where Name Like :searchTextW And CreatedDate >=:Sdate  ORDER BY Createddate LIMIT :limitsize OFFSET :offset];
                  }
          else if(startdate =='' && enddate != '')
                  {
                                 
                    date edate= date.parse(enddate);
                    edate= edate.adddays(1);
                    tempAttachmentResult= [Select Id, Name,CreatedDate From Attachment Where Name Like :searchTextW And CreatedDate <=:Edate  ORDER BY Createddate LIMIT :limitsize OFFSET :offset];
                  }
            else 
                {
                 
                  tempAttachmentResult= [Select Id, Name,CreatedDate From Attachment Where Name Like :searchTextW ORDER BY Createddate LIMIT :limitsize OFFSET :offset];
                }
        }
        else
        {
                
        }
        
        return tempAttachmentResult;
    }
    
    public integer totalPagesForAttachment()
    {
          integer totalRecords;
          integer totalPages=0;
        
        if(searchText != null && searchText != '') {
                        
            String searchTextW='%'+searchText+'%';
             if(startdate != '' && enddate != '')
                 {
                               
                    date sdate= date.parse(startdate);
                    date edate= date.parse(enddate);
                    edate= edate.adddays(1);
                        totalRecords= [Select count() From Attachment Where Name Like :searchTextW And CreatedDate >=:Sdate And CreatedDate <=:Edate];
                  }
             else if(startdate !='' && enddate == '')
                  {
                                
                  date sdate= date.parse(startdate);
                  totalRecords= [Select count() From Attachment Where Name Like :searchTextW And CreatedDate >=:Sdate];
                  }
          else if(startdate =='' && enddate != '')
                  {
                                
                    date edate= date.parse(enddate);
                    edate= edate.adddays(1);
                    totalRecords= [Select count() From Attachment Where Name Like :searchTextW And CreatedDate <=:Edate];
                  }
            else 
                {
                 
                  totalRecords= [Select count() From Attachment Where Name Like :searchTextW ];
                }
        }
        else
        {
               
        }
        
        // Total records into Pages 
        if(totalRecords!=null)
        {
          totalPages  =totalRecords/limitsize;
         if(math.mod(totalRecords,limitsize)>0)
         {
             totalPages++;
         }
        }
         return totalPages  ;
        
    }
    
     public integer totalPagesForTask()
    {
         
          integer totalRecords;
          integer totalPages=0;
        
        if(searchText != null && searchText != '') {
                        
            String searchTextW='%'+searchText+'%';
             if(startdate != '' && enddate != '')
                 {
                                
                    date sdate= date.parse(startdate);
                    date edate= date.parse(enddate);
                    edate= edate.adddays(1);
                        totalRecords= [Select count() From Task Where Subject Like :searchTextW And CreatedDate >=:Sdate And CreatedDate <=:Edate];
                  }
             else if(startdate !='' && enddate == '')
                  {
                                 
                  date sdate= date.parse(startdate);
                  totalRecords= [Select count() From Task Where Subject Like :searchTextW And CreatedDate >=:Sdate];
                  }
          else if(startdate =='' && enddate != '')
                  {
                                  
                    date edate= date.parse(enddate);
                    edate= edate.adddays(1);
                    totalRecords= [Select count() From Task Where Subject Like :searchTextW And CreatedDate <=:Edate];
                  }
            else 
                {
                 
                  totalRecords= [Select count() From Task Where Subject Like :searchTextW ];
                }
        }
        else
        {
                
        }
        
        // Total records into Pages 
        if(totalRecords!=null)
        {
             totalPages  =totalRecords/limitsize;
             if(math.mod(totalRecords,limitsize)>0)
             {
                 totalPages++;
             }
         }
         
         return totalPages  ;
    }
    
    public void next()
    {
        String objectType =ApexPages.currentPage().getParameters().get('objectType');
        System.debug('### --- '+objectType );
        
        if(canDoNext(objectType))
        {
        
            if(objectType=='Task')
            {
               
                taskSearchResult= queryTaskObject(getOffsetByPage(taskCurrentPage+1));
                taskCurrentPage++;
            }
            else if(objectType=='Attachment')
            {
               
                  attachmentSearchResult= queryAttachmentObject(getOffsetByPage(attachmentCurrentPage+1));
                  attachmentCurrentPage++;
            }
            else
            {
           
            }
        
        }
    }
    
     public void prev()
    {
        String objectType =ApexPages.currentPage().getParameters().get('objectType');
        System.debug('### --- '+objectType );
        
        if(canDoPrev(objectType))
        {
        
            if(objectType=='Task')
            {
            
                 
          
                taskSearchResult= queryTaskObject(getOffsetByPage(taskCurrentPage-1));
                taskCurrentPage--;
            }
            else if(objectType=='Attachment')
            {
                
                  attachmentSearchResult= queryAttachmentObject(getOffsetByPage(attachmentCurrentPage-1));
                  attachmentCurrentPage--;
            }
            else
            {
           
            }
        
        }
    }

    
    public integer getOffsetByPage(integer pageno)
    {
        return limitsize*pageno;
    }
    
    public boolean canDoNext(string objectType)
    {
    
    boolean tempResult=false;
    
            if(objectType=='Task')
            {
             
              tempResult=taskTotalPage>1?(taskCurrentPage<(taskTotalPage-1)):false;
                        
            }
            else if(objectType=='Attachment')
            {
             tempResult=attachmentTotalPage>1?(attachmentCurrentPage<(attachmentTotalPage-1)):false;
            

            }
            else
            {
           
            }
    
    return tempResult;
    }
    
    public boolean canDoPrev(string objectType)
    {
    
    boolean tempResult=false;
    
            if(objectType=='Task')
            {
             
              tempResult=taskTotalPage>1?(taskCurrentPage>0):false;
                        
                                               
            }
            else if(objectType=='Attachment')
            {
             tempResult=attachmentTotalPage>1?(attachmentCurrentPage>0):false;
            
                        

            }
            else
            {
            
            }
    
    return tempResult;
    }
    

    
    
}