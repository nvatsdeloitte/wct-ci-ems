global class WCT_FetchUserInfo
{

    WebService static String fetchUserAccessAsInterviewer(String UserId)
    {
        list<User> UserInfo =[Select id,Name from User where id=:UserId and profile.Name = :Label.User_Interviewer_Profile_Name];
        list<PermissionSetAssignment> PerSetAssign = [SELECT AssigneeId,Id,PermissionSetId,PermissionSet.Name,SystemModstamp FROM PermissionSetAssignment
                                                where AssigneeId=:UserId and PermissionSet.Name = :Label.User_Interviewer_PermissionSet_Name];
        if(!(UserInfo.isEmpty()) || (PerSetAssign.isEmpty())){
            return 'true';
        }
        else{
            return 'false';
        }
    }

}