/**
    * Class Name  : WCT_FetchUserInfo_Test
    * Description : This apex test class will use to test the WCT_FetchUserInfo
*/
@isTest
private class WCT_FetchUserInfo_Test {

	public static User userRec=new User();
	public static Id profileId=[Select id from Profile where Name=:WCT_UtilConstants.RECRUITER_COMPANY].Id;
    
	/** 
        Method Name  : createuser
        Return Type  : User
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUser()
    {
    	userRec=WCT_UtilTestDataCreation.createUser('fethUsr', profileId, 'vinBhatiaPreBICaseHelp@deloitte.com', 'Recruiter1@deloitte.com');
    	return  userRec;
    }
    
    /** 
        Method Name  : createuser
        Return Type  : User
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    static testMethod void checkuserInfo() {
        Test.startTest();
        userRec=createUser();
        insert userRec;
        String flag=WCT_FetchUserInfo.fetchUserAccessAsInterviewer(userRec.Id);
        Test.stopTest();
    }
}