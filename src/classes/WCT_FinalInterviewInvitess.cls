global class WCT_FinalInterviewInvitess {
     
    WebService static string findInvitess(String EventId, String InterviewId)
    {
        string returnStatement;
            list<EventRelation> eventDis= [SELECT AccountId,EventId,IsInvitee,RelationId,Status, Event.Description, Event.EndDateTime, 
                                    Event.Location,  Event.StartDateTime, Event.What.Name, Event.IsDateEdited__c 
                                    FROM EventRelation 
                                    WHERE eventid= :EventId and IsInvitee=true
                                    and Relation.type = 'User' and Event.What.Type = 'WCT_Interview__c'];
            
            
        if(!eventDis.IsEmpty())
        {
            if(eventDis[0].Event.StartDateTime<system.now()){
                returnStatement='dateError';
            }else{
                returnStatement='true';
            }
        }
        else
        {
            returnStatement='false';
        
        }
        return returnStatement;
    }

}