@isTest
public class WCT_FindActivityStagesCorrected_Test
{
    public static testmethod void testmethod1()
    {
        
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='testoast@deloitte.com';
        con.WCT_Person_Id__c='104032';
        insert con;
        WCT_Outreach__c Outreach = WCT_UtilTestDataCreation.CreateOutreach();
        insert Outreach;
        WCT_Outreach_Activity__c oa = WCT_UtilTestDataCreation.createOA();
        oa.WCT_Contact__c=con.id;
        oa.WCT_Outreach__c=Outreach.id;
        insert oa;
        WCT_Outreach_Activity_Staging_Table__c stagingtable = WCT_UtilTestDataCreation.createOAStageTable();
        stagingtable.WCT_Status__c='Failed';
        insert stagingtable;
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new WCT_MockHttpResponseGenerator_Test());
        //WCT_FindActivityStagesCorrected controller = new WCT_FindActivityStagesCorrected();
        WCT_FindActivityStagesCorrected.findStagingRecords('Failed');
        Test.StopTest();
     }
      public static testmethod void testmethod2()
    {
        
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='testoast@deloitte.com';
        con.WCT_Person_Id__c='104032';
        insert con;
        WCT_Outreach__c Outreach = WCT_UtilTestDataCreation.CreateOutreach();
        insert Outreach;
        WCT_Outreach_Activity__c oa = WCT_UtilTestDataCreation.createOA();
        oa.WCT_Contact__c=con.id;
        oa.WCT_Outreach__c=Outreach.id;
        insert oa;
        WCT_Outreach_Activity_Staging_Table__c stagingtable = WCT_UtilTestDataCreation.createOAStageTable();
        stagingtable.WCT_Status__c='Failed';
        insert stagingtable;
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new WCT_MockHttpResponseGenerator_Test());
        //WCT_FindActivityStagesCorrected controller = new WCT_FindActivityStagesCorrected();
        WCT_FindActivityStagesCorrected.findStagingRecords('Corrected');
        Test.StopTest();
     }
     
}