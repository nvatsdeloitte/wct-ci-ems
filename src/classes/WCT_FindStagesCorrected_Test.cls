/**
    * Class Name  : WCT_FindStagesCorrected_Test
    * Description : This apex test class will use to test the WCT_CreateCasesHelper
*/
@isTest
private class WCT_FindStagesCorrected_Test {

    public static List<WCT_PreBIStageTable__c> preBiStageList=new List<WCT_PreBIStageTable__c>();
    public static String versionNo = '1';
    /** 
        Method Name  : createPreBIStageTable
        Return Type  : List<WCT_PreBIStageTable__c>
        Type         : private
        Description  : Create Pre-Bi Stage Table Records.         
    */
    private Static List<WCT_PreBIStageTable__c> createPreBIStageTable()
    {
        preBiStageList=WCT_UtilTestDataCreation.createPreBIStageTable(WCT_UtilConstants.STAGING_STATUS_CORRECTED);
        insert preBiStageList;
        return  preBiStageList;
    }
    
    /** 
        Method Name  : createCasesHelperTestMethod
        Return Type  : Void
        Type         : private
        Description  : Create Pre-Bi Stage Table Records and pass to CreateHelper.         
    */
    static testMethod void createCasesHelperTestMethod() {
        preBiStageList=createPreBIStageTable();
        Test.startTest();
        String returnStatement=WCT_FindStagesCorrected.findStagingRecords(WCT_UtilConstants.STAGING_STATUS_CORRECTED, '');
        returnStatement=WCT_FindStagesCorrected.findStagingRecords(WCT_UtilConstants.STAGING_STATUS_CORRECTED, versionNo);
        Test.stopTest();
        System.assertEquals(Label.WCT_Message_Processing_Starting, returnStatement);
         
    }
    /** 
        Method Name  : noRecordFoundTestMethod
        Return Type  : Void
        Type         : private
        Description  : When there is no record to process.          
    */
    static testMethod void noRecordFoundTestMethod() {
        preBiStageList=createPreBIStageTable();
        Test.startTest();
        String msg=WCT_FindStagesCorrected.findStagingRecords(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED, versionNo);
        Test.stopTest();
        System.assertEquals(WCT_UtilConstants.PROCESS_NONE, msg);
    }
    
    
}