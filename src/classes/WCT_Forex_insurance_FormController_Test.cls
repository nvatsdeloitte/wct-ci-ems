/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class WCT_Forex_insurance_FormController_Test 

{
    public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        recordtype mobRecType = [select id from recordtype where DeveloperName = 'Employment_Visa'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        mob.RecordTypeId = mobRecType.Id;
        insert mob;
        WCT_Task_Reference_Table__c taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.Form_Verbiage__c = 'Hi. This is Test.';
        taskRef.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        taskRef.WCT_Visa_Type__c = 'WCT_Mobility__c.RecordType';
        insert taskRef;
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Task_Reference_Table_ID__c = taskRef.Id;
        t.WCT_Auto_Close__c=false;
        insert t; 
        
        Test.starttest();
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_Forex_Insurance_form;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_Forex_insurance_FormController controller=new WCT_Forex_insurance_FormController();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller.updateTaskFlags();
        controller=new WCT_Forex_insurance_FormController();
        controller.updateTaskFlags();
        controller.save(); 
        controller.checked = true;
        controller.display=true;
        controller.taskSubject = t.Subject;
        controller.taskVerbiage = taskRef.Form_Verbiage__c;
        controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
    }
}