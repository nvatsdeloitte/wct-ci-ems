/*
Class name : WCT_Fragomen_Validation_Controller
Description : This class is controller for WCT_Fragomen_Validation page.

Task number     Date            Modified By                         Description
------------    ---------       ---------------------               ----------------------
                19-Aug-14        Darshee Mehta                       Created
                                                            
*/
public class WCT_Fragomen_Validation_Controller {

    public String selectedViewBy {
        get;
        set;
    }
    public String ViewValue {
        get;
        set;
    }
    public List < SelectOption > ViewValueOptions {
        get;
        set;
    }
    public List < Contact > lstContactRecord {
        get;
        set;
    }
    public List < ImmigrationWrapper > lstImmigrationRecord {
        get;
        set;
    }
    public Set < String > empEmail {
        get;
        set;
    }
    public Map < String, List < ImmigrationWrapper >> map_Email_lstImmigrationRecord {
        get;
        set;
    }

    public Blob contentFile {
        get;
        set;
    }
    public Integer noOfRows {
        get;
        set;
    }
    public Integer noOfRowsProcessed {
        get;
        set;
    }
    public String nameFile {
        get;
        set;
    }
    public Set < Id > stagingIds = new Set < Id > (); // Store all StagingIds after reading from file.
    public List < List < String >> fileLines = new List < List < String >> ();
    Map < String, Integer > questionColOrderMap = new Map < String, Integer > ();

    public String employeeName {
        get;
        set;
    }
    public String employeeEmail {
        get;
        set;
    }
    public String immName {
        get;
        set;
    }
    public String immStatus {
        get;
        set;
    }
    public String immMilestone {
        get;
        set;
    }
    public String immVisaType {
        get;
        set;
    }
    public String immInitiationDate {
        get;
        set;
    }


    WCT_parseCSV parseCSVInstance = new WCT_parseCSV();

    public map < String, list < Contact >> map_valueSelected_ListOfContact {
        get;
        set;
    }
    public map < String, map < String, list < WCT_Immigration__c >>> map_Email_map_valueSelected_ImmigrationRec {
        get;
        set;
    }
    public map < String, map < String, list < immigrationWrapper >>> map_Email_map_valueSelected_immigrationWrapperRec {
        get;
        set;
    }

    //-----------------------------------
    //    Constructor
    //-----------------------------------

    public WCT_Fragomen_Validation_Controller() {
        map_valueSelected_ListOfContact = new map < String, list < Contact >>();
        map_valueSelected_ListOfContact.put('New Records', new list < Contact >());
        map_valueSelected_ListOfContact.put('Duplicate Records', new list < Contact >());
        map_valueSelected_ListOfContact.put('Existing Records', new list < Contact >());
        
        map_Email_map_valueSelected_ImmigrationRec = new map < String, map < String, list < WCT_Immigration__c >>>();
        map_Email_map_valueSelected_ImmigrationRec.put('New Records', new map < String, list < WCT_Immigration__c >>());
        map_Email_map_valueSelected_ImmigrationRec.put('Duplicate Records', new map < String, list < WCT_Immigration__c >>());
        map_Email_map_valueSelected_ImmigrationRec.put('Existing Records', new map < String, list < WCT_Immigration__c >>());
        
        map_Email_map_valueSelected_immigrationWrapperRec = new map < String, map < String, list < immigrationWrapper >>>();
        map_Email_map_valueSelected_immigrationWrapperRec.put('New Records', new map < String, list < immigrationWrapper >>());
        map_Email_map_valueSelected_immigrationWrapperRec.put('Duplicate Records', new map < String, list < immigrationWrapper >>());
        map_Email_map_valueSelected_immigrationWrapperRec.put('Existing Records', new map < String, list < immigrationWrapper >>());
        
        setViewValues();
    }

    //-----------------------------------
    //    Wrapper Class
    //-----------------------------------    

    public class immigrationWrapper {
        public String employeeName {
            get;
            set;
        }
        public String employeeEmail {
            get;
            set;
        }
        public String immMilestone {
            get;
            set;
        }
        public String immVisaType {
            get;
            set;
        }
        public String immInitiationDate {
            get;
            set;
        }
        public boolean isNew {
            get;
            set;
        }
        public boolean isDuplicate {
            get;
            set;
        }
        public boolean isExisting {
            get;
            set;
        }
        public string id_duplicate_immi_record {
            get;
            set;
        }

        public immigrationWrapper() {
            isNew = false;
            isDuplicate = false;
            isExisting = false;
        }
    }

    //-------------------------------------------------------------------------
    //    Get different views By Dropdown List Value of 'View'
    //-------------------------------------------------------------------------  

    public void setViewValues() {
        ViewValueOptions = new List < SelectOption > ();
        ViewValueOptions.add(new SelectOption('New Records', 'New Records'));
        ViewValueOptions.add(new SelectOption('Duplicate Records', 'Duplicate Records'));
        ViewValueOptions.add(new SelectOption('Existing Records', 'Existing Records'));
        ViewValue = 'New Records';
    }

    //----------------------------------------------------------------------------------------------------
    //    Get a list of Records in the CSV and SFDC database.
    //----------------------------------------------------------------------------------------------------

    public PageReference getList() {
        List < String > statusValuesList = new List < String > ();
        statusValuesList.add('New');
        statusValuesList.add('Visa Approved');
        statusValuesList.add('Visa Stamped');
        statusValuesList.add('LCA Approved');
        statusValuesList.add('Visa Withdrawn');
        statusValuesList.add('Request For Evidence Withdrawn');

        String conRecType = 'Employee';
        //empEmail = new Set<String>();
        //empEmail.add('damehta@deloitte.com');
        String query = 'SELECT Id, Name, Email, (SELECT Id, Name, WCT_Immigration_Status__c, WCT_Request_Initiation_Date__c, RecordType.Name, WCT_Visa_Type__c, WCT_Assignment_Owner__c, WCT_Milestone__c FROM Immigrations__r WHERE WCT_Immigration_Status__c NOT IN : statusValuesList) FROM Contact WHERE Email IN :empEmail AND RecordType.Name = \'' + conRecType + '\'';
        system.debug('<><><> ' + query);
        system.debug('<<<< ' + empEmail);
        lstContactRecord = database.query(query);
        system.debug('>>>> ' + lstContactRecord);

        return null;
    }

    //----------------------------------------------------------------------------------------------------
    //    Method Name  : readFile
    //    Return Type  : PageReference
    //    Description  : Read CSV and push records into variables to be displayed on VF         
    //----------------------------------------------------------------------------------------------------

    public Pagereference readFile() {
        noOfRowsProcessed = 0;

        try {
            lstImmigrationRecord = mapCSVRecords();
            map_Email_lstImmigrationRecord = new map < String, List < ImmigrationWrapper >> ();
            for (ImmigrationWrapper l: lstImmigrationRecord) {
                if (!map_Email_lstImmigrationRecord.containsKey(l.employeeEmail)) {
                    map_Email_lstImmigrationRecord.put(l.employeeEmail, new list < ImmigrationWrapper > ());
                }
                map_Email_lstImmigrationRecord.get(l.employeeEmail).add(l);
            }
            getList();

             for (Contact c: lstContactRecord) {
                set < Id > totalImmigrationRec = new set < Id > ();
            set < Id > newImmiRec = new set < Id > ();
           
                list < ImmigrationWrapper > tempImmiWrapperList = new list < ImmigrationWrapper > ();
                tempImmiWrapperList = map_Email_lstImmigrationRecord.get(c.Email);
                for (WCT_Immigration__c w: c.Immigrations__r) {
                    totalImmigrationRec.add(w.Id);
                    for (ImmigrationWrapper i: tempImmiWrapperList) {
                        if (i.employeeEmail == c.Email && i.immVisaType == w.WCT_Visa_Type__c && i.immInitiationDate == (string.valueof(w.WCT_Request_Initiation_Date__c).substring(5, 7) + '/' + string.valueof(w.WCT_Request_Initiation_Date__c).substring(8, 10) + '/' + string.valueof(w.WCT_Request_Initiation_Date__c).substring(2, 4))) {
                            newImmiRec.add(w.Id);
                            i.isDuplicate = true;
                            if (!map_valueSelected_ListOfContact.containsKey('Duplicate Records')) {
                                map_valueSelected_ListOfContact.put('Duplicate Records', new list < Contact > ());
                            }
                            map_valueSelected_ListOfContact.get('Duplicate Records').add(c);

                            if (!map_Email_map_valueSelected_immigrationWrapperRec.containsKey('Duplicate Records')) {
                                map_Email_map_valueSelected_immigrationWrapperRec.put('Duplicate Records', new map < String, list < immigrationWrapper >> ());
                            }
                            if (!map_Email_map_valueSelected_immigrationWrapperRec.get('Duplicate Records').containsKey(c.Email)) {
                                map_Email_map_valueSelected_immigrationWrapperRec.get('Duplicate Records').put(c.Email, new list < ImmigrationWrapper > ());
                            }
                            map_Email_map_valueSelected_immigrationWrapperRec.get('Duplicate Records').get(c.Email).add(i);

                            if (!map_Email_map_valueSelected_ImmigrationRec.containsKey('Duplicate Records')) {
                                map_Email_map_valueSelected_ImmigrationRec.put('Duplicate Records', new map < String, list < WCT_Immigration__c >> ());
                            }
                            if (!map_Email_map_valueSelected_ImmigrationRec.get('Duplicate Records').containsKey(c.Email)) {
                                map_Email_map_valueSelected_ImmigrationRec.get('Duplicate Records').put(c.Email, new list < WCT_Immigration__c > ());
                            }
                            map_Email_map_valueSelected_ImmigrationRec.get('Duplicate Records').get(c.Email).add(w);
                        }
                    }
                    }
                    boolean isNew = true;
                    for (Id i: totalImmigrationRec) {
                        if (!newImmiRec.contains(i)) {
                            isNew = false;
                        }
                    }
                    if (isNew) {
                        if (!map_valueSelected_ListOfContact.containsKey('New Records')) {
                            map_valueSelected_ListOfContact.put('New Records', new list < Contact > ());
                        }
                        map_valueSelected_ListOfContact.get('New Records').add(c);
                    } else if (!isNew) {
                        if (!map_valueSelected_ListOfContact.containsKey('Existing Records')) {
                            map_valueSelected_ListOfContact.put('Existing Records', new list < Contact > ());
                        }
                        map_valueSelected_ListOfContact.get('Existing Records').add(c);
                    }

                    for (WCT_Immigration__c w1: c.Immigrations__r) {
                        if (isNew) {
                            if (!map_Email_map_valueSelected_ImmigrationRec.containsKey('New Records')) {
                                map_Email_map_valueSelected_ImmigrationRec.put('New Records', new map < String, list < WCT_Immigration__c >> ());
                            }
                            if (!map_Email_map_valueSelected_ImmigrationRec.get('New Records').containsKey(c.Email)) {
                                map_Email_map_valueSelected_ImmigrationRec.get('New Records').put(c.Email, new list < WCT_Immigration__c > ());
                            }
                            if (!newImmiRec.contains(w1.Id)) {
                                map_Email_map_valueSelected_ImmigrationRec.get('New Records').get(c.Email).add(w1);
                            }
                        } else if (!isNew) {
                            if (!map_Email_map_valueSelected_ImmigrationRec.containsKey('Existing Records')) {
                                map_Email_map_valueSelected_ImmigrationRec.put('Existing Records', new map < String, list < WCT_Immigration__c >>());
                            }
                            if (!map_Email_map_valueSelected_ImmigrationRec.get('Existing Records').containsKey(c.Email)) {
                                map_Email_map_valueSelected_ImmigrationRec.get('Existing Records').put(c.Email, new list < WCT_Immigration__c >());
                            }
                            system.debug('<><><> ' + map_Email_map_valueSelected_ImmigrationRec);
                            if (!newImmiRec.contains(w1.Id)) {
                                map_Email_map_valueSelected_ImmigrationRec.get('Existing Records').get(c.Email).add(w1);
                            }
                        }
                    }

                    for (ImmigrationWrapper i: tempImmiWrapperList) {
                        if (isNew) {
                            if (!map_Email_map_valueSelected_immigrationWrapperRec.containsKey('New Records')) {
                                map_Email_map_valueSelected_immigrationWrapperRec.put('New Records', new map < String, list < immigrationWrapper >> ());
                            }
                            if (!map_Email_map_valueSelected_immigrationWrapperRec.get('New Records').containsKey(c.Email)) {
                                map_Email_map_valueSelected_immigrationWrapperRec.get('New Records').put(c.Email, new list < ImmigrationWrapper > ());
                            }
                            if (i.isDuplicate <> true) {
                                map_Email_map_valueSelected_immigrationWrapperRec.get('New Records').get(c.Email).add(i);
                            }
                        } else if (!isNew) {
                            if (!map_Email_map_valueSelected_immigrationWrapperRec.containsKey('Existing Records')) {
                                map_Email_map_valueSelected_immigrationWrapperRec.put('Existing Records', new map < String, list < immigrationWrapper >> ());
                            }
                            if (!map_Email_map_valueSelected_immigrationWrapperRec.get('Existing Records').containsKey(c.Email)) {
                                map_Email_map_valueSelected_immigrationWrapperRec.get('Existing Records').put(c.Email, new list < ImmigrationWrapper > ());
                            }
                            if (i.isDuplicate <> true) {
                                map_Email_map_valueSelected_immigrationWrapperRec.get('Existing Records').get(c.Email).add(i);
                            }
                        }
                    }
            }
            
            
        }
        Catch(System.StringException stringException) {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error, Label.Upload_Case_String_Exception_Error + stringException);
            ApexPages.addMessage(errormsg);
        }
        Catch(System.ListException listException) {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error, Label.Columns_count_not_proper + listException);
            ApexPages.addMessage(errormsg);

        }

        Catch(Exception e) {
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error, Label.Upload_Case_Exception + e);
                ApexPages.addMessage(errormsg);

            }
            /*  if(lstImmigrationRecord.size()>Limits.getLimitQueryRows())
              {
                  ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Error_Data_File_too_large + Limits.getLimitQueryRows());
                  ApexPages.addMessage(errormsg);   
              }*/
            // Database.SaveResult[] srList = Database.insert(lstImmigrationRecord,false);
            /*  for(Database.SaveResult sr: srList)
              {
                  if(sr.IsSuccess())
                  {   
                      stagingIds.add(sr.getId());
                      noOfRowsProcessed=stagingIds.size();
                  }
                  else if(!sr.IsSuccess())
                  {
                      for(Database.Error err : sr.getErrors())
                      {
                           ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.WCT_Data_Error+err.getMessage());
                           ApexPages.addMessage(errormsg);       
                      }
                  }
              }       */
        system.debug('<><><> ' + map_valueSelected_ListOfContact);
        return new PageReference('/apex/WCT_Fragomen_Validation');
    }

    //---------------------------------------------------------------------------------------------------- 
    //    Method Name  : mapCSVRecords
    //    Description  : Map fields names with CSV col names         
    //----------------------------------------------------------------------------------------------------

    private List < ImmigrationWrapper > mapCSVRecords() {
        nameFile = contentFile.toString();
        filelines = parseCSVInstance.parseCSV(nameFile, true);
        noOfRows = fileLines.size();
        lstImmigrationRecord = new List < ImmigrationWrapper > ();
        system.debug('<><><> ' + filelines);
        empEmail = new Set < String > ();
        for (List < String > inputValues: filelines) {

            String VisaType;
            String InitiationDate;
            String EmpName;

            //Create Identifier as Emp Email+Case Type(record type)+Initiation Date
            if (inputValues[3] != null && inputValues[4] != null && inputValues[9] != null) {
                if (inputValues[4] == 'H-1B') {
                    if (inputValues[7] == 'Cap')
                        VisaType = 'H1B Cap';
                    else if (inputValues[8] == 'Extension')
                        VisaType = 'H1B Extension';
                    else if (inputValues[8] == 'Transfer')
                        VisaType = 'H1B Transfer';
                } else if (inputValues[4] == 'L-1') {
                    if (inputValues[8] == 'Extension') {
                        if (inputValues[5] == 'L-1B')
                            VisaType = 'L1B Extension';
                        else
                            VisaType = 'L1A Extension';
                    } else {
                        if (inputValues[5] == 'L-1B' && inputValues[6] != null)
                            VisaType = 'L1B ' + inputValues[6];
                        else if (inputValues[5] == 'L-1A' && inputValues[6] != null)
                            VisaType = 'L1A ' + inputValues[6];
                    }
                } else if (inputValues[4] == 'B-1') {
                    VisaType = 'B1/B2 ';
                }
            }
            if (inputValues[9] != '')
                InitiationDate = inputValues[9];

            EmpName = inputValues[2] + ' ' + inputValues[1];

            immigrationWrapper iWrap = new immigrationWrapper();
            iWrap.employeeName = EmpName;
            iWrap.employeeEmail = inputValues[3];
            iWrap.immMilestone = inputValues[12];
            iWrap.immVisaType = VisaType;
            iWrap.immInitiationDate = InitiationDate;
            lstImmigrationRecord.add(iWrap);
            empEmail.add(inputValues[3]);
            system.debug('wraplist' + iWrap);

            system.debug('empemail' + empEmail);


            // lstImmigrationRecord.add(new immigrationWrapper(employeeName = EmpName, employeeEmail = inputValues[3], immMilestone = inputValues[12], immVisaType = VisaType, immInitiationDate = InitiationDate));
        }
        return lstImmigrationRecord;
    }
}