/*
    * Class Name  : WCT_GMIAttachmentsController  
    * Description : Show all attachment associated to Candidate and provide the ability to ".zip" selected attachments.  
    * Author      : Deloitte
*/
global class WCT_GMIAttachmentsController {
 
    public String zipFileName {get; set;}
    public String zipContent {get; set;}
    public String AttIdsArr {get; set;}  
    public Id docId {get;set;}
    public String recordId {get;set;}

    /*
        Method Name  : Wct_AttachmentZipController - Constructor   
    */
    global WCT_GMIAttachmentsController(){
        if(ApexPages.currentPage().getParameters().get('Id') != null) recordId = ApexPages.currentPage().getParameters().get('Id');
    }

    /*
        Method Name  : uploadZip   
        Return Type  : Void
    */
    global void uploadZip() {
        if (String.isEmpty(zipFileName) ||
            String.isBlank(zipFileName)) {
            zipFileName = 'BulkZip_File.zip';//Default Name for zip file.
        }
        else {
            zipFileName.replace('.', '');// To prevent file extension
            zipFileName += '.zip';
        }
         
        Document doc = new Document();
        doc.Name = zipFileName;
        doc.ContentType = 'application/zip';
        doc.FolderId = Label.Attachment_Zip_Document_Folder_Id;
        doc.Body = EncodingUtil.base64Decode(zipContent);
        
        try{
            insert doc;  
        }Catch(DMLException ex)
        {
            WCT_ExceptionUtility.logException('WCT_GMIAttachmentsController-uploadZip', null, ex.getMessage() + ' ::: ' + ex.getStackTraceString());
        }        
        docId = doc.Id; 
        this.zipFileName = null;
        this.zipContent = null;
    }
    
    /*
        Method Name  : getAttachments   
        Return Type  : List of Attachments
    */
    global List<TaskAttachmentWrapper> getAttachments() {
        // Find the list of all the tasks associated with this record
        Map<Id, Task> mapTasks = new Map<Id, Task>([
                                SELECT
                                    Id,
                                    Subject
                                FROM
                                    Task
                                WHERE
                                    WhatId = :recordId
                            ]);
        Set<Id> setParentIds = new Set<Id>();
        setParentIds.add(recordId);
        if(null != mapTasks) {
            setParentIds.addAll(mapTasks.keySet());
        }
                    
        List<Attachment> listAttachments = [
                                                SELECT 
                                                    Id, 
                                                    ParentId,
                                                    Name, 
                                                    Parent.Name,
                                                    ContentType, 
                                                    BodyLength, 
                                                    CreatedDate 
                                                FROM 
                                                    Attachment 
                                                WHERE 
                                                    ParentId in :setParentIds
                                            ];                         
        List<TaskAttachmentWrapper> listTaskAttWrapper = new List<TaskAttachmentWrapper>();
        for(Attachment a : listAttachments) {
            TaskAttachmentWrapper taw = new TaskAttachmentWrapper();
            taw.taskRecord = mapTasks.get(a.parentId);
            taw.attRecord = a;
            String size = null;
            if(1048576 < a.BodyLength) {
                // Size greater than 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength) {
                // Size greater than 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else {
                size = '' + a.BodyLength + ' bytes';
            }
            taw.size = size;
            listTaskAttWrapper.add(taw);
        }
        
        return listTaskAttWrapper;
    }

    @RemoteAction
    global static AttachmentWrapper getAttachment(String fullName) {
        String attId = fullName.subString(0,18);
        String attName = '';
        if(fullName.length() > 18) {
            attName = fullName.subString(18, fullName.length());
        }
        Attachment att = [select Id, Name, ContentType, Body
                          from Attachment
                          where Id = :attId];
         
        AttachmentWrapper attWrapper = new AttachmentWrapper();
        attWrapper.attEncodedBody = EncodingUtil.base64Encode(att.body);
        attName = attName + ' - ' + att.Name;
        if(attName.length() > 255) {
            String[] nameParts = attName.split('\\.', 2);        
            attName = nameParts[0].subString(0, nameParts[0].length() - (attName.length() - 255)) + '.' + nameParts[1];
        }
        attWrapper.attName = attName;
        return attWrapper;
    }
    /*
        Wrapper Class: AttachmentWrapper
        Description  : Wrapper class to wrap Attachment Name and Attachment body.
    */       
    global class AttachmentWrapper {
        public String attEncodedBody {get; set;}
        public String attName {get; set;}
    }
    
    /**
     * Class to wrap the Attachment along with the task name
     */
    global class TaskAttachmentWrapper {
        public Task taskRecord {get; set;}
        public Attachment attRecord {get; set;}
        public String size {get; set;}
    }
}