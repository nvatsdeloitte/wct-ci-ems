@isTest

private class WCT_GMIAttachmentsController_Test
{
    public static testmethod void m1()
    {
  
        Document doc = WCT_UtilTestDataCreation.createDocument(); 
        insert doc;
        
        Contact conRec = WCT_UtilTestDataCreation.createContactRec();
        insert conRec;
        
        Case caseRec = WCT_UtilTestDataCreation.createCase(conRec.id);
        insert caseRec;
        
        Task taskRec = WCT_UtilTestDataCreation.createTask(caseRec.id);
        insert taskRec;
        
         ApexPages.currentPage().getParameters().put('ID',taskRec.id);
        
        Attachment att = WCT_UtilTestDataCreation.createAttachment(taskRec.id);
        insert att;
        
        WCT_GMIAttachmentsController.AttachmentWrapper attachmentWrapClass = new WCT_GMIAttachmentsController.AttachmentWrapper();
        WCT_GMIAttachmentsController.TaskAttachmentWrapper  taskAttachmentWrapClass = new WCT_GMIAttachmentsController.TaskAttachmentWrapper();
        Test.startTest();
        WCT_GMIAttachmentsController controller = new WCT_GMIAttachmentsController();
       
        controller.zipContent = 'Test';
        controller.uploadZip();
        controller.getAttachments(); 
        WCT_GMIAttachmentsController.getAttachment(string.valueOf(att.id));
        Test.stopTest();
        
    }
}