@isTest
public class WCT_GMIEventsReminderBatch_Test
{
  static Id employeeRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
  static Id Event_Registration_RecordTypeId = Schema.SObjectType.Event_Registration_Attendance__c.getRecordTypeInfosByName().get('GM&I').getRecordTypeId();
  
  static String CRON_EXP = '0 0 0 15 3 ? 2022';

  static testMethod void WCT_GMIEventsReminderBatch_Test()  
  {
      Test.startTest();
      Contact con = WCT_UtilTestDataCreation.createEmployee(employeeRecordTypeId);
      insert con;
      recordtype rt1=[select id from recordtype where DeveloperName = 'Event_GM_I'];
            Event__c gmiEvent = new Event__c();
            gmiEvent.Name = 'GM&I Event';
            gmiEvent.Start_Date_Time__c = DateTime.now().addDays(1);
            gmiEvent.End_Date_Time__c = DateTime.now().addDays(1);
            gmiEvent.RecordTypeId = rt1.id;
            gmiEvent.Is_Event_Information_Changed__c = false;
            gmiEvent.RecordTypeId=Schema.SObjectType.Event__c.getRecordTypeInfosByName().get('GM&I').getRecordTypeId();
            insert gmiEvent;
            
            
            recordtype rt2=[select id from recordtype where DeveloperName = 'Event_Regristration_GM_I'];
            Event_Registration_Attendance__c Eventreg = new Event_Registration_Attendance__c();
            Eventreg.Event__c = gmiEvent.id;
            Eventreg.Contact__c = con.id;
            // Eventreg.Event_Reminder_Date__c = Date.today();
            Eventreg.WCT_GMI_Event_Reminder__c = false; 
            Eventreg.RecordTypeId = rt2.id;
            insert Eventreg;
      
      
      System.schedule('ScheduleApexClassTest',CRON_EXP, new WCT_GMIEventsReminderSchedulableBatch()); 
      Test.stopTest();
  }
  
  
   
  
   }