//1 day before the 'eEvents Start Date' field
global class WCT_GMIEventsReminderSchedulableBatch implements Database.Batchable<sObject>,Schedulable, Database.Stateful{

    public string strSql{get;set;}
//-------------------------------------------------------------------------------------------------------------------------------------
//    Constructor
//-------------------------------------------------------------------------------------------------------------------------------------
    
    public WCT_GMIEventsReminderSchedulableBatch(){

    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Scheduler Execute 
//-------------------------------------------------------------------------------------------------------------------------------------\
   global void execute(SchedulableContext SC) {
        WCT_GMIEventsReminderSchedulableBatch batch = new  WCT_GMIEventsReminderSchedulableBatch();
        ID batchprocessid = Database.executeBatch(batch,200);
    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Set the query
//-------------------------------------------------------------------------------------------------------------------------------------
   global Database.QueryLocator start(Database.BatchableContext bc){
        // system.debug('Hi Come ON !!!'); 
        string strSql = 'SELECT Id, Event_Reminder_Date__c, WCT_GMI_Event_Reminder__c'+
                        ' FROM Event_Registration_Attendance__c'+
                        ' WHERE Event_Reminder_Date__c = TODAY'+ 
                        ' AND Event_Registration_Attendance__c.RecordType.Name = \'GM&I\''+
                        ' AND WCT_GMI_Event_Reminder__c = false';
       // system.debug('strSql'+strSql);       
        return database.getQuerylocator(strSql);   
       
    }
        
//-------------------------------------------------------------------------------------------------------------------------------------
//    Database execute method to Process Query results for email notification
//-------------------------------------------------------------------------------------------------------------------------------------
    global void execute(Database.batchableContext bc, List<sObject> scope){
    
        List<Event_Registration_Attendance__c> lstEventReg = new List<Event_Registration_Attendance__c>();
        for(sObject tmp : scope){
            Event_Registration_Attendance__c tmpEve = new Event_Registration_Attendance__c();
            tmpEve = (Event_Registration_Attendance__c)tmp;
            lstEventReg.add(tmpEve);
            if(tmpEve.WCT_GMI_Event_Reminder__c == false)
                tmpEve.WCT_GMI_Event_Reminder__c = true;
                             
        }
        Database.SaveResult[] sr = Database.update(lstEventReg, false);
    }
    
    global void finish(Database.batchableContext info){     
   
    }     
}