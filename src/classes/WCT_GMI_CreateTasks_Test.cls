/**
 * Description : Test class for WCT_GMI_CreateTasks
 */
 
@isTest

Private class WCT_GMI_CreateTasks_Test
{

      static testMethod void test1() 
      {
          
          WCT_Task_Reference_Table__c  taskRefParent = WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
          taskRefParent.WCT_Status__c = 'Petition Collected';
          insert taskRefParent;
          
          recordtype recType = [select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact assignmentOwner = WCT_UtilTestDataCreation.createEmployee(recType.id);
          insert assignmentOwner;
          
          ID immigRecType = [SELECT Id,name FROM RecordType WHERE NAME  = 'L1 Visa' and SobjectType = 'WCT_Immigration__c'].ID;
          
          ID taskRecType = [SELECT Id,name FROM RecordType WHERE NAME  = 'Immigration' and SobjectType = 'Task'].ID;
          
          WCT_Immigration__c  immigRecOld = WCT_UtilTestDataCreation.createImmigration(assignmentOwner.id);
          immigRecOld.RecordTypeId = immigRecType;
          insert immigRecOld ;
          
          WCT_Immigration__c  immigRecNew = WCT_UtilTestDataCreation.createImmigration(assignmentOwner.id);
          immigRecNew.RecordTypeId = immigRecType;
          insert immigRecNew ;
          immigRecNew.WCT_Immigration_Status__c = 'Petition Collected';
          
          Task taskRec = WCT_UtilTestDataCreation.createTaskwithRef(immigRecNew.id,taskRefParent.id);
          taskRec.RecordTypeId = taskRecType;
          insert taskRec;
          
          update immigRecNew ;
          
          
          
          WCT_GMI_CreateTasks createTask = new WCT_GMI_CreateTasks();
         
  
      }
      
      static testMethod void test2() 
      {
        
        recordtype rt = [select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact conRec = WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert conRec;      
        
        WCT_Task_Reference_Table__c  taskRefParent = WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
        taskRefParent.WCT_Status__c = 'New';
        taskRefParent.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        taskRefParent.WCT_Visa_Type__c = 'Business Visa';
        insert taskRefParent;
        
        ID mobRecType = [SELECT Id,name FROM RecordType WHERE NAME  = 'Business Visa' and SobjectType = 'WCT_Mobility__c'].ID;
        ID taskRecType = [SELECT Id,name FROM RecordType WHERE NAME  = 'Mobility' and SobjectType = 'Task'].ID;
        WCT_Mobility__c mobRec = WCT_UtilTestDataCreation.createMobility(conRec.id);
        mobRec.RecordTypeId = mobRecType; 
        insert mobRec;
        mobRec.WCT_Mobility_Status__c = 'New';
        
        
       
        Task taskRec = WCT_UtilTestDataCreation.createTaskwithRef(mobRec.id,taskRefParent.id);
        taskRec.RecordTypeId = taskRecType;
        insert taskRec;
          
        update mobRec;
      
      }
      
      static testMethod void test3() 
      {
        recordtype rt = [select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact conRec = WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert conRec;
        
        WCT_Task_Reference_Table__c  taskRefParent = WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
        taskRefParent.WCT_Status__c = 'New';
        taskRefParent.WCT_Task_for_Object__c = 'WCT_LCA__c';
        taskRefParent.WCT_Visa_Type__c = 'LCA Record type';
        insert taskRefParent;
        
        ID lcaRecType = [SELECT Id,name FROM RecordType WHERE NAME  = 'LCA Record type' and SobjectType = 'WCT_LCA__c'].ID;
        ID taskRecType = [SELECT Id,name FROM RecordType WHERE NAME  = 'LCA' and SobjectType = 'Task'].ID;
        WCT_LCA__c lcaRec = WCT_UtilTestDataCreation.CreateLCA(conRec.id);
        lcaRec.RecordTypeId = lcaRecType; 
        insert lcaRec ;
        lcaRec.WCT_Status__c = 'LCA Filed';
       
        Task taskRec = WCT_UtilTestDataCreation.createTaskwithRef(lcaRec.id,taskRefParent.id);
        taskRec.RecordTypeId = taskRecType;
        insert taskRec;
          
        update lcaRec;
      
      }
      static testMethod void test4() 
      {
      
         WCT_Task_Reference_Table__c taskRef =  WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
         
         insert taskRef;
         taskRef.WCT_Assigned_to__c = label.Fragomen_user_name;
         update taskRef;
         recordtype rt = [select id from recordtype where DeveloperName = 'WCT_Employee'];
         Contact conRecTwo =  WCT_UtilTestDataCreation.createEmployee(rt.id);
         insert conRecTwo;
         WCT_Mobility__c mobRecTwo = WCT_UtilTestDataCreation.createMobility(conRecTwo.id);
         insert mobRecTwo;
         
         
                
      }
      static testmethod void test5(){

        recordtype cntrecd=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        
       Contact immcontact = WCT_UtilTestDataCreation.createEmployee(cntrecd.id);
        
       insert immcontact;
        
        WCT_Passport__c psst=new WCT_Passport__c(WCT_Passport_First_Name__c='Nihal',Passport_Last_Name__c='Vats' ,WCT_Passport__c='test12345' ,WCT_Passport_Issuance_Date__c=date.parse('11/7/2013'), WCT_Passport_Expiration_Date__c=date.parse('11/7/2014') ,WCT_Country_of_Passport__c='INDIA' ,WCT_Passport_Issuing_City__c='Hyderabad' );
        psst.WCT_Employee__c=immcontact.id;
        
        insert psst;
        
        recordtype immrecd=[select id,name from recordtype where name='H1 Visa'];
        
        WCT_Immigration__c imminsert = new WCT_Immigration__c(recordtypeid=immrecd.id ,WCT_Assignment_Owner__c=immcontact.id ,WCT_Immigration_Status__c='New' ,WCT_Visa_Type__c='H1B CAP' ,WCT_Visa_Interview_Date__c=datetime.now() ,WCT_OFC_Appointment_Date__c=datetime.now());
        
        insert imminsert ;
        
        recordtype Mobrecd=[select id,name from recordtype where name='Employment Visa'];
        
        WCT_Mobility__c mobinsert= new WCT_Mobility__c(recordtypeid=Mobrecd.id ,WCT_Mobility_Status__c='New' ,WCT_Mobility_Employee__c=immcontact.id  ,Immigration__c=imminsert.id);
        
        insert mobinsert;
         WCT_Mobility__c mobinsertnocnt= new WCT_Mobility__c(recordtypeid=Mobrecd.id,WCT_Mobility_Status__c='New');
         insert mobinsertnocnt;
        
}
}