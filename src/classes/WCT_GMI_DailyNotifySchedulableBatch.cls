//-------------------------------------------------------------------------------------------------------------------------------------
//    Description : WCT_GMI_NotifySchedulableBatch  - This class will send the G M & I email alerts for the upcoming and overdue tasks
//
//------------------------------------------------------------------------------------------------------------------------------------- 
global class WCT_GMI_DailyNotifySchedulableBatch  implements Database.Batchable<sObject>,Schedulable, Database.Stateful{

    global void execute(SchedulableContext SC) {
        WCT_GMI_DailyNotifySchedulableBatch  batch = new WCT_GMI_DailyNotifySchedulableBatch  ();
            ID batchprocessid = Database.executeBatch(batch,200); 
    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Set the query 
//-------------------------------------------------------------------------------------------------------------------------------------
    global Database.QueryLocator start(Database.BatchableContext bc){ 
    
// Get Next date after 4 days      
       date dt =  Date.today().addDays(4);    
       string nextDate = string.valueof(dt); 
        String todayDate=String.valueof(Date.Today().addDays(0));
       
// Get incomplete tasks          
       string strSql ='select WCT_DailyNotificationSent__c,subject ,whoId,who.name,who.email,Owner.email,whatId,what.name,ActivityDate, RecordType.Name,' +
        ' WCT_Business_Owner__c,WCT_Employee_Email__c,' +
        ' WCT_R10_Resource_Manager__c,WCT_Resource_Manager_Email_ID__c,WCT_USI_Resource_Manager__c,WCT_US_Project_Mngr__c ' +
        ' from task ' +
        ' where RecordType.Name in ( \'Immigration\',\'Mobility\',\'LCA\') and (Status <> \'Completed\' AND Status <> \'Canceled\' AND Status <> \'Employee Replied\') and (whoId <> null)'+
        ' and (ActivityDate>='+todayDate+' and ActivityDate  <=' + nextDate+ ' )  and WCT_Daily_Notification__c=true and WCT_DailyNotificationSent__c=false';      
      system.debug('test123'+strSql);
       return database.getQuerylocator(strSql);
    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Process Query results for email notification of tasks
//-------------------------------------------------------------------------------------------------------------------------------------
    global void execute(Database.batchableContext bc, List<sObject> scope){
        Map <integer,string> mapNumberMonth = new Map <integer,string>();
        mapNumberMonth.put(1,'January');       
        mapNumberMonth.put(2,'February');       
        mapNumberMonth.put(3,'March');       
        mapNumberMonth.put(4,'April');       
        mapNumberMonth.put(5,'May');       
        mapNumberMonth.put(6,'June');       
        mapNumberMonth.put(7,'July');       
        mapNumberMonth.put(8,'August');       
        mapNumberMonth.put(9,'September');       
        mapNumberMonth.put(10,'October');       
        mapNumberMonth.put(11,'November');       
        mapNumberMonth.put(12,'December');   
        
        Messaging.singleEmailMessage mail;
        List<string> lstEmail=new  List<string>();
        
        // Map of contact Id and Contact name , required for setTargetobject method of mail and for Contact name
        Map<id,string> mapIdContactName = new Map<id,string>();
        
        // Map to hold employye name and its corresponding tasks        
        Map<id,List<Task>> mapContactIdTask = new Map<id,List<Task>>();       
        List<Task> lstMapTasks;
        
        Set<Id> contactIdSet=new Set<Id>();
        Set<Id> tasksIdsUpdate=new Set<Id>();
        for(sObject tmptask:scope)
        {
            Task task=new Task();
            task=(Task)tmptask;
            contactIdSet.add(task.WhoId);
        
        }
        Set<Id> deloitteContactId=new Set<Id>();
        List<Contact> contactList=new List<Contact>();
        if(!contactIdSet.IsEmpty())
        {
            contactList=[Select id,Email from contact where Id IN:contactIdSet and email like '%@deloitte.com' and RecordType.Name='Employee'];
            if(!contactList.IsEmpty())
            {
                for(contact con:contactList)
                    deloitteContactId.add(con.Id);
            }
        }
               
        List<OrgWideEmailAddress> lstOWE = [SELECT Address,DisplayName,Id FROM OrgWideEmailAddress where DisplayName = 'GMI Team'];
        List <Messaging.SingleEmailMessage> lstSendMails=new  List <Messaging.SingleEmailMessage>();
       // set list of User names and map of user name and associated tasks           
        for(sObject tmpTsk : scope) {
            Task gmiTask  = new Task();
            gmiTask = (Task)tmpTsk;
            //    Prepare email object
            mail = new Messaging.singleEmailMessage(); 
            if(deloitteContactId.Contains(gmiTask.whoId))
            {
                tasksIdsUpdate.add(tmpTsk.Id);
                mail.setTargetObjectId(gmiTask.whoId);
                string strFont='';
                if(gmiTask.ActivityDate < date.today()){
                    strfont = 'style="color : red; font-weight:bold"';
                }             
                string strEmailBody='';
                string strDueDate = mapNumberMonth.get(gmiTask.ActivityDate.Month())+' '+ gmiTask.ActivityDate.Day() +', '+ gmiTask.ActivityDate.year();
                strEmailBody = ' <html>';
                strEmailBody = strEmailBody +  ' <head>';
                strEmailBody = strEmailBody +  ' <center>';
                strEmailBody = strEmailBody +  ' <style>';
                strEmailBody = strEmailBody +  ' table';
                strEmailBody = strEmailBody +  ' {';
                strEmailBody = strEmailBody +  ' border: none;';
                strEmailBody = strEmailBody +  ' }';
                strEmailBody = strEmailBody +  ' </style>';
                strEmailBody = strEmailBody +  ' <img src="'+Label.Deloitte_Logo+'">';
                strEmailBody = strEmailBody +  ' </center>';
                strEmailBody = strEmailBody +  ' </head>';
                strEmailBody = strEmailBody +  ' <body>';
                strEmailBody = strEmailBody +  ' <center>';
                strEmailBody = strEmailBody +  ' <table width="650">';
                strEmailBody = strEmailBody +  '<p style="font-family:times new roman;color:#92D43A;font-size:30px;"> Notice of New To-Do on Talent on Demand � Due soon!</p><br>';
                strEmailBody = strEmailBody +  '<p style="font-family:arial;font-size:12px;"> Dear '+ gmiTask.who.name +',</p>';
                strEmailBody = strEmailBody +  '<p style="font-family:arial;font-size:12px;">As part of your Global Mobility and Immigration process, there are a number of mandatory tasks that you are required to complete.';
                strEmailBody = strEmailBody +  'These will appear as alerts in <a href="https://talentondemand.deloittenet.deloitte.com">Talent on Demand</a> and new tasks will be generated as your case progresses.';
                strEmailBody = strEmailBody +  'As such, it is imperative that you check back frequently for any new alerts that may appear over time.<br></p>';
                strEmailBody = strEmailBody +  '<p style="font-family:arial;font-size:12px;">Our records indicate that the Task: <b>'+ gmiTask.subject +'</b> is due on <b '+strFont+' >'+ strDueDate  +'</b>.<br>';
                strEmailBody = strEmailBody +  '<p style="font-family:arial;font-size:12px;">Got a Question? Call 2222 (1800 Deloitte if you are in the US) <br></p>'; 
                strEmailBody = strEmailBody +  '<p style="font-family:arial;font-size:12px;"><br>Regards,<br>';
                strEmailBody = strEmailBody +  'USI Global Mobility and Immigration Team</p>';
                strEmailBody = strEmailBody +  '<p style="font-family:arial;font-size:12px;"><i>&lt;Please ignore this email if you have already completed your task&gt;</i></p>';
                strEmailBody = strEmailBody +  '<br><br><p style="font-family:arial;font-size:10px;"><a href="https://www.deloittenet.com/">DeloitteNet</a> | <a href="http://www.deloitte.com/us/security">Security</a> | <a href="http://www.deloitte.com/us/legal">Legal</a> | <a href="http://www.deloitte.com/us/privacy">Privacy</a><br></p>';
                strEmailBody = strEmailBody +  '<p style="font-family:arial;font-size:9px;">30 Rockefeller Plaza<br>';
                strEmailBody = strEmailBody +  'New York, NY 10112-0015<br>';
                strEmailBody = strEmailBody +  'United States<br></p>';
                strEmailBody = strEmailBody +  ' <img src="'+Label.Deloitte_Footer_Logo+'">';
                strEmailBody = strEmailBody +  '<p style="font-family:arial;font-size:9px;">Copyright � 2013 Deloitte Development LLC. All rights reserved.<br>';
                strEmailBody = strEmailBody +  '36 USC 220506<br>';
                strEmailBody = strEmailBody +  'Member of Deloitte Touche Tohmatsu Limited<br></p>';
                strEmailBody = strEmailBody +  '<a href="http://www.linkedin.com/company/deloitte"><img src="'+Label.LinkedIn_Logo+'"></a> <a href="http://www.facebook.com/YourFutureAtDeloitte"><img src="'+Label.Facebook_Logo+'"></a> <a href="http://www.youtube.com/deloittellp"><img src="'+Label.YouTube_Logo+'"></a> <a href="http://www.deloitte.com/view/en_US/us/press/social-media/index.htm"><img src="'+Label.Twitter_Logo+'"></a> <a href="http://www.deloitte.com/view/en_US/us/press/rss-feeds/index.htm"><img src="'+Label.RSS_Feed_Logo+'"></a>';
                strEmailBody = strEmailBody +  ' </table>';
                strEmailBody = strEmailBody +  ' </center>';
                strEmailBody = strEmailBody +  ' </body>';
                strEmailBody = strEmailBody +  ' </html>'; 
                mail.setHTMLBody(strEmailBody );  
                if(!lstOWE.isEmpty())              
                    mail.setOrgWideEmailAddressId(lstOWE.get(0).id) ;    
                mail.setSaveAsActivity(false);
                mail.setSubject('ACTION REQUIRED: Your Task associated to '+ gmiTask.subject +' is coming due on '+ strDueDate +' [ID:'+gmiTask.What.Name+']');
                lstSendMails.add(mail);
            }   
        }
        try{
            Messaging.SendEmailResult[] mailResults = Messaging.sendEmail(lstSendMails); 
            List<Task> taskListUpdate=new List<Task>();
            if(!tasksIdsUpdate.IsEmpty())
            {
                for(Id taskRecID:tasksIdsUpdate)
                {
                    Task taskRec=new Task(Id=taskRecID);
                    taskRec.WCT_DailyNotificationSent__c=true;
                    taskListUpdate.add(taskRec);
                }
                
            }
            if(!taskListUpdate.IsEmpty())
            {
                Database.SaveResult[] sr = Database.update(taskListUpdate, false);
            }
        }
        catch(Exception ex){
            WCT_ExceptionUtility.logException('WCT_GMI_DailyNotifySchedulableBatch','Update Tasks',ex.getMessage());
        }                
    }
    
    global void finish(Database.batchableContext info){     
   
    } 
}