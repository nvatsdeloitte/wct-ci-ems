public class WCT_GMI_Manual_task
{
    public class Select_Task_Wrapper
    {
        public WCT_Task_Reference_Table__c reftable {get; set;}
        public boolean selected {get; set;}
        
        public Select_Task_Wrapper(WCT_Task_Reference_Table__c ref)
        {
            reftable = ref;
            selected = false;
        }
    }
    public List<Id> IdList{get;set;} 
    public List<Select_Task_Wrapper> STW_List{get;set;}
    public Id SelectedId; 
    public String ErrorMsg {get;set;}
    public String ObjectType;
    public string rtype;
    map<String,Task> taskMap = new map<String,Task>();
    public String sUserID = UserInfo.getUserId();     
    public boolean showtasksflag{get;set;}
    public boolean isAddError {get;set;}
    public List <Task> cNewTask {get;set;} 
    List<Task> lstTasksTobeInserted=new List<Task>();
    Map<id,id> ImmigrationMap = new Map<id,id>();     
    public Task newTask{get;set;}  
    List<WCT_Task_Reference_Table__c > wrapList;
    Public Map<string,id> mapUserId;
    public String taskImmigrationRecordTypeId=Schema.SObjectType.Task.getRecordTypeInfosByName().get('Immigration').getRecordTypeId();
    public String taskLCARecordTypeId=Schema.SObjectType.Task.getRecordTypeInfosByName().get('LCA').getRecordTypeId();
    public String taskMobilityRecordTypeId=Schema.SObjectType.Task.getRecordTypeInfosByName().get('Mobility').getRecordTypeId();
    public WCT_GMI_Manual_task()
    {
        IdList = new list<Id>(); 
        ErrorMsg = '';
        showtasksflag=true;  
        newTask = new task(); 
        SelectedID = ApexPages.currentPage().getParameters().get('id'); 
        ObjectType = ApexPages.currentPage().getParameters().get('objectType');
        rtype = ApexPages.currentPage().getParameters().get('rtype');
        if(SelectedID==null || ObjectType==null || rtype==null)
        return;
        cNewTask = new list < Task>(); 
        wrapList = new List<WCT_Task_Reference_Table__c >();    
        STW_List = new List<Select_Task_Wrapper>();
        List<WCT_Task_Reference_Table__c> TaskRef;
        List<Task> lstTasks = [select id,WCT_Task_Reference_Table_ID__c from task where whatid=:SelectedID ]  ;
        set<Id> setTaskIds = new set<Id>();
        for(Task tmpTask : lstTasks  ){                   
            setTaskIds.add(tmpTask.WCT_Task_Reference_Table_ID__c );
        }
        TaskRef = [select WCT_Due_Date__c,Name,WCT_Task_Subject__c,WCT_Priority__c,WCT_ToD_Task_Reference__c,
                    OwnerId,WCT_Task_for_Object__c ,WCT_Notification_1__c,WCT_Notification_2__c,
                    WCT_Notification_3__c,WCT_Notification_4__c,WCT_Notification_5__c,
                    WCT_Auto_Create__c,WCT_Auto_Close__c,WCT_Assigned_to__c,WCT_Status__c,WCT_Visa_Type__c,
                    WCT_Parent_Task__c,(Select id FROM  Task_Reference_Table__r)                      
                    from WCT_Task_Reference_Table__c 
                    where WCT_Auto_Create__c = false AND WCT_Task_for_Object__c =:ObjectType AND WCT_Visa_Type__c =:rtype and WCT_IS_Active__c=:true];
        
        mapUserId = new Map<string,id>();
        
        List<User> lstAssignedUser = [select id,name from user where name in('GM & I','Fragomen')];
        for(User tmpUser : lstAssignedUser ){
            mapUserId.put(tmpUser.name,tmpUser.id);
        }
        set<String> trtAutonumSet = new set<String>();
        if(!TaskRef.IsEmpty())
        {
            for(WCT_Task_Reference_Table__c ref : TaskRef )
            {
                STW_List.add(new Select_Task_Wrapper(ref));
                trtAutonumSet.add(ref.id);
                
                if(!trtAutonumSet.IsEmpty())
                {
                    List<task> tasks = [Select id,WCT_Task_Reference_Table_ID__c,WCT_ToD_Task_Reference__c from task where WCT_Task_Reference_Table_ID__c  IN : trtAutonumSet AND whatId =: SelectedId ];
                    for(Task taskFor : tasks )
                    {
                        taskMap.put(taskFor.WCT_Task_Reference_Table_ID__c,taskFor); 
                    }
                }
            }
        }
    }
    public PageReference Save()
    {
        wrapList.clear();
        ErrorMsg = '';
        for(Select_Task_Wrapper stw :  STW_List)
        {
            if(stw.selected==true)
            { 
                wrapList.add(stw.reftable);
                
            }            
        }  
        
        // getting business hour id for USI.
        BusinessHours stdBusinessHours = [select id from businesshours where Name='USI'];
        Integer working_hours=12;
        DateTime actualSLADate;
        
        String caseConatctID;
        String ownerIdStr;
        String contactEmailID;
        if(ObjectType == 'WCT_Immigration__c'){
            list<WCT_Immigration__c> lstsObjectImig=[select id,OwnerId,WCT_Assignment_Owner__c,WCT_Assignment_Owner__r.Email from WCT_Immigration__c where id =: SelectedID limit 1]; 
            if(lstsObjectImig.size()>0 ){
                caseConatctID =   lstsObjectImig[0].WCT_Assignment_Owner__c;
                ownerIdStr=lstsObjectImig[0].OwnerId;
                contactEmailID=lstsObjectImig[0].WCT_Assignment_Owner__r.Email;
            }
        }
        else if(ObjectType == 'WCT_Mobility__c')
        {
            list<WCT_Mobility__c> lstsObjectMob=[select id,OwnerId,WCT_Mobility_Employee__c,WCT_Mobility_Employee__r.Email from WCT_Mobility__c where id =: SelectedID limit 1]; 
            if(lstsObjectMob.size()>0){
                caseConatctID =   lstsObjectMob[0].WCT_Mobility_Employee__c;
                ownerIdStr=lstsObjectMob[0].OwnerId;
                contactEmailID=lstsObjectMob[0].WCT_Mobility_Employee__r.Email;
                
            }  
            
        }
        
        else if(ObjectType == 'WCT_LCA__c')
        {
            list<WCT_LCA__c> lstsObjectLCA=[select id,OwnerId,WCT_Assignment_Owner__c,WCT_Assignment_Owner__r.Email from WCT_LCA__c where id =: SelectedID limit 1]; 
            if(lstsObjectLCA.size()>0){
                caseConatctID =   lstsObjectLCA[0].WCT_Assignment_Owner__c;
                ownerIdStr=lstsObjectLCA[0].OwnerId;
                contactEmailID=lstsObjectLCA[0].WCT_Assignment_Owner__r.Email;
            }  
            
        }
        for(WCT_Task_Reference_Table__c stwNew : wrapList )
        {
            if(taskMap.containsKey(stwNew.id))
            {
                if(ErrorMsg=='')
                {
                    ErrorMsg = 'Task is already Created for Task Number:'+ stwNew.id+ ',';
                }
                else{
                    ErrorMsg += 'Task is already Created for Task Number:'+ stwNew.id + ',';
                }
                
            }
            
            Date CurrentDate = system.today();
            task taskToadd = new task();
            if(stwNew.WCT_Due_Date__c !=null)
            {
                actualSLADate = BusinessHours.add(stdBusinessHours.id, system.now(), (((stwNew.WCT_Due_Date__c)*working_hours*3600000).longValue()));
                taskToadd.ActivityDate = date.newinstance(actualSLADate.year(), actualSLADate.month(), actualSLADate.day());
            }
            taskToadd.Subject=stwNew.WCT_Task_Subject__c;
            taskToadd.WCT_Notification_1__c = System.today().addDays(Integer.valueOf(stwNew.WCT_Notification_1__c));
            taskToadd.WCT_Notification_2__c = System.today().addDays(Integer.valueOf(stwNew.WCT_Notification_2__c));
            taskToadd.WCT_Notification_3__c = System.today().addDays(Integer.valueOf(stwNew.WCT_Notification_3__c));
            taskToadd.WCT_Notification_4__c=  System.today().addDays(Integer.valueOf(stwNew.WCT_Notification_4__c));
            taskToadd.WCT_Notification_5__c = System.today().addDays(Integer.valueOf(stwNew.WCT_Notification_5__c));
            taskToadd.WCT_Is_Visible_in_TOD__c=true;
            taskToadd.WhatId = SelectedID;
            taskToadd.WCT_ToD_Task_Reference__c= stwNew.WCT_ToD_Task_Reference__c;
            taskToadd.WCT_Encrypt_Email_Key__c =CryptoHelper.encrypt(contactEmailID);
            taskToadd.WCT_Business_Owner__c = contactEmailID;
          //  taskToadd.WCT_Resource_Manager_Email_ID__c=contactEmailID;
            if(ObjectType == 'WCT_Immigration__c' || ObjectType == 'WCT_Mobility__c' || ObjectType == 'WCT_LCA__c'  )
            {
                if(ObjectType == 'WCT_Immigration__c')
                    taskToadd.RecordTypeID=taskImmigrationRecordTypeId;
                else if(ObjectType == 'WCT_Mobility__c')
                    taskToadd.RecordTypeID=taskMobilityRecordTypeId;
                else
                    taskToadd.RecordTypeID=taskLCARecordTypeId;
                if(stwNew.WCT_Assigned_to__c == 'Employee')
                {
                    taskToadd.whoId  =caseConatctID;
                    if(ownerIdStr.startsWith('00G'))
                        taskToadd.OwnerId = mapUserId.get('GM & I');
                    else
                        taskTOadd.OwnerId=ownerIdStr;
                }
                else if(stwNew.WCT_Assigned_to__c=='Fragomen') 
                            taskToadd.OwnerId = mapUserId.get('Fragomen');
                else if((stwNew.WCT_Assigned_to__c).contains('GM&I'))
                {
                            taskToadd.OwnerId = mapUserId.get('GM & I');
                            if(ownerIdStr.startsWith('00G'))
                                taskToadd.OwnerId = mapUserId.get('GM & I');
                            else
                                taskTOadd.OwnerId=ownerIdStr;
                }
            }
            //taskToadd.OwnerId = sUserID;
            taskToadd.WCT_Task_Reference_Table_ID__c = stwNew.id; 
            if(stwNew.WCT_Priority__c == null)
            {
                taskToadd.Priority = '2-Normal';
            }
            
            if(stwNew.WCT_Status__c == null)
            {
                taskToadd.Status = 'Not Started';
            }
            
            taskToadd.Subject = stwNew.WCT_Task_Subject__c;
            taskToadd.Task_Type__c = stwNew.WCT_Status__c;
            if(!(stwNew.Task_Reference_Table__r).IsEmpty())
                taskToadd.WCT_HasChildrens__c=true; 
            lstTasksTobeInserted.add(taskToadd);
        }
        try
        {
            
            if(!lstTasksTobeInserted.isEmpty())
            {
                insert lstTasksTobeInserted;

            } 
        }
        catch(Exception e)
        {
            WCT_ExceptionUtility.logException('WCT_GMI_Manual_task','Insertion Failed',e.getMessage());
        }
        PageReference pageRef = new PageReference('/' + SelectedId );
        pageRef.setRedirect(true); 
        return pageref;
        
    } 
    public PageReference Cancel()
    {
        PageReference pageRef = new PageReference('/' + SelectedId );
        pageRef.setRedirect(true); 
        return pageref;
    }    
}