//-------------------------------------------------------------------------------------------------------------------------------------
//    Description : WCT_GMI_NotifySchedulableBatch  - This class will send the G M & I email alerts for the upcoming and overdue tasks
//
//-------------------------------------------------------------------------------------------------------------------------------------
global class WCT_GMI_NotifySchedulableBatch  implements Schedulable{

    global void execute(SchedulableContext SC) {
// Get Next date after 7 days      
       date dt =  Date.today().addDays(7);   
       string nextDate = string.valueof(dt); 
       
// Get incomplete tasks          
        List<Task> lstTask = [select subject,Site_URL__c,whoId,who.name,who.email,Owner.email,whatId,what.name,ActivityDate, RecordType.Name,WCT_Business_Owner__c,WCT_Employee_Email__c,WCT_R10_Resource_Manager__c,WCT_USI_Resource_Manager__c,WCT_US_Project_Mngr__c from task where RecordType.Name in ( 'Immigration','Mobility','LCA') and (Status <> 'Completed' AND Status <> 'Canceled' AND Status <> 'Employee Replied') and (whoId <> null) and (ActivityDate  <= :dt)];
        
        Map <integer,string> mapNumberMonth = new Map <integer,string>();
        mapNumberMonth.put(1,'January');       
        mapNumberMonth.put(2,'February');       
        mapNumberMonth.put(3,'March');       
        mapNumberMonth.put(4,'April');       
        mapNumberMonth.put(5,'May');       
        mapNumberMonth.put(6,'June');       
        mapNumberMonth.put(7,'July');       
        mapNumberMonth.put(8,'August');       
        mapNumberMonth.put(9,'September');       
        mapNumberMonth.put(10,'October');       
        mapNumberMonth.put(11,'November');       
        mapNumberMonth.put(12,'December');    
           
        Messaging.singleEmailMessage mail;
        List<string> lstEmail=new  List<string>();
        
        // Map of contact Id and Contact name , required for setTargetobject method of mail and for Contact name
        Map<id,string> mapIdContactName = new Map<id,string>();
        
        // Map to hold employye name and its corresponding tasks        
        Map<id,List<Task>> mapContactIdTask = new Map<id,List<Task>>();       
        List<Task> lstMapTasks;
        
        Set<Id> contactIdSet=new Set<Id>();
        
        for(Task tmptask : lstTask)
        {
            contactIdSet.add(tmptask.WhoId);        
        }
        
        Set<Id> deloitteContactId=new Set<Id>();
        List<Contact> contactList=new List<Contact>();
        if(!contactIdSet.IsEmpty())
        {
            contactList=[Select id,Email from contact where Id IN:contactIdSet and email like '%@deloitte.com' and RecordType.Name='Employee'];
            if(!contactList.IsEmpty())
            {
                for(contact con:contactList)
                    deloitteContactId.add(con.Id);
            }
        }
               
    // set list of User names and map of user name and associated tasks  
         
        for(Task gmiTask : lstTask ) {
            if(!mapContactIdTask.containsKey(gmiTask.whoId)){
                lstMapTasks = new  List<Task>();
                lstMapTasks.add(gmiTask); 
                if(deloitteContactId.contains(gmiTask.whoId))
                    mapContactIdTask.put(gmiTask.whoId,lstMapTasks);
            }
            else{
                lstMapTasks = new  List<Task>();
                lstMapTasks = mapContactIdTask.get(gmiTask.whoId);
                lstMapTasks.add(gmiTask);
                if(deloitteContactId.contains(gmiTask.whoId))
                    mapContactIdTask.put(gmiTask.whoId,lstMapTasks);
            }
            if(deloitteContactId.contains(gmiTask.whoId))
                mapIdContactName.put(gmiTask.whoId,gmiTask.who.name) ;

        }
        
        // For each employee id, get list of tasks from map and put in table
        List <Messaging.SingleEmailMessage> lstSendMails=new  List <Messaging.SingleEmailMessage>();
        List<OrgWideEmailAddress> lstOWE = [SELECT Address,DisplayName,Id FROM OrgWideEmailAddress where DisplayName = 'GMI Team'];
        for(Id tmpConId : mapIdContactName.keyset()){
      
            //    Prepare email object
            mail = new Messaging.singleEmailMessage();
            mail.setTargetObjectId(tmpConId);
            String strEmailBody;
            strEmailBody = ' <html>';
            strEmailBody = strEmailBody +  ' <head>';
            strEmailBody = strEmailBody +  ' <center>';
            strEmailBody = strEmailBody +  ' <style>';
            strEmailBody = strEmailBody +  ' table,th,td';
            strEmailBody = strEmailBody +  ' {';
            strEmailBody = strEmailBody +  ' width:640px;';
            strEmailBody = strEmailBody +  ' border-collapse:collapse;';
            strEmailBody = strEmailBody +  ' }';
            strEmailBody = strEmailBody +  ' th,td';
            strEmailBody = strEmailBody +  ' {';
            strEmailBody = strEmailBody +  ' padding:5px;';
            strEmailBody = strEmailBody +  ' }';
            strEmailBody = strEmailBody +  ' </style>';
            strEmailBody = strEmailBody +  ' <img src="'+Label.Deloitte_Logo+'">';
            strEmailBody = strEmailBody +  ' </center>';
            strEmailBody = strEmailBody +  ' </head>';
            strEmailBody = strEmailBody +  ' <body>';
            strEmailBody = strEmailBody +  ' <center>';
            strEmailBody = strEmailBody +  ' <table>';
            strEmailBody = strEmailBody +  '<p style="font-family:times new roman;color:#92D43A;font-size:29px;"> Notice of Critical Outstanding Tasks on Talent on Demand</p><br>';
            strEmailBody = strEmailBody +  '<p style="font-family:arial;font-size:12px;"> Dear ' + mapIdContactName.get(tmpConId)+',<p>';
            strEmailBody = strEmailBody +  '<p style="font-family:arial;font-size:12px;">As part of your Global Mobility and Immigration process, there are a number of mandatory tasks that you are required to complete. These will appear as alerts in <a href="https://talentondemand.deloittenet.deloitte.com">Talent on Demand </a>and new tasks will be generated as your case progresses. As such, it is imperative that you check back frequently for any new alerts that may appear over time.<br></p>';
            strEmailBody = strEmailBody +  '<p style="font-family:arial;font-size:12px;">The below tasks require your immediate action. Please ensure to complete corresponding tasks to each of your alerts in <a href="https://talentondemand.deloittenet.deloitte.com">Talent on Demand</a>.<br></p>';
            strEmailBody = strEmailBody +  ' </table>';
            strEmailBody = strEmailBody +  ' <p><br><table border="1" cellspacing="0">';
            strEmailBody = strEmailBody +  ' <tr>';
            strEmailBody = strEmailBody +  '   <th style="font-family:arial;font-size:12px;">Task Description</th>';
            strEmailBody = strEmailBody +  '   <th style="font-family:arial;font-size:12px;">Due Date </th>    '  ;
            strEmailBody = strEmailBody +  '   <th style="font-family:arial;font-size:12px;">Task URL </th>    '  ; 
            strEmailBody = strEmailBody +  '   </tr>';
            
        // Get map list of tasks for an employee
            if(mapContactIdTask.get(tmpConId)!=null){
                 lstMapTasks =  mapContactIdTask.get(tmpConId);
                 for(Task tmpTsk : lstMapTasks ) { 
                    lstEmail = new List<String>(); 
                    if(tmpTsk.subject!=null){
                        strEmailBody = strEmailBody +  ' <tr>';
                        strEmailBody = strEmailBody +  '   <td style="font-family:arial;font-size:12px;">'+tmpTsk.subject+'</td>';
                        string strfont='';
                        string strOverDue='';
                        if(tmpTsk.ActivityDate < date.today()){
                            strfont = 'style=" font-weight:bold"';
                            strOverDue = '(Overdue!)';
                        }    
                        //strEmailBody = strEmailBody +  '   <td '+strfont+'>'+mapNumberMonth.get(tmpTsk.ActivityDate.Month())+' '+ tmpTsk.ActivityDate.Day() +','+ tmpTsk.ActivityDate.year()+ '. ' + strOverDue +'</td>';
                        strEmailBody = strEmailBody +  '   <td style="font-family:arial;font-size:12px;">'+mapNumberMonth.get(tmpTsk.ActivityDate.Month())+' '+ tmpTsk.ActivityDate.Day() +', '+ tmpTsk.ActivityDate.year()+ ' <b style="color:red;font-weight:bold;font-family:arial;font-size:12px;">' + strOverDue +'</b></td>';
                        strEmailBody = strEmailBody +  '   <td style="font-family:arial;font-size:12px;"><a href="'+tmpTsk.Site_URL__c+'">Click Here!</a></td>';
                        strEmailBody = strEmailBody +  ' </tr>';
                    }               
                 }
            }
            strEmailBody = strEmailBody +  ' </table>';
            strEmailBody = strEmailBody +  ' <table>';
            strEmailBody = strEmailBody +  ' <br><br><p style="font-family:arial;font-size:12px;">Got a Question? Call 2222 (1800 Deloitte if you are in the US) <br></p>';
            strEmailBody = strEmailBody +  '<p style="font-family:arial;font-size:12px;"><br>Regards,<br>';
            strEmailBody = strEmailBody +  'USI Global Mobility and Immigration Team</p>';
            strEmailBody = strEmailBody +  '<br><br><p style="font-family:arial;font-size:10px;"><a href="https://www.deloittenet.com/">DeloitteNet</a> | <a href="http://www.deloitte.com/us/security">Security</a> | <a href="http://www.deloitte.com/us/legal">Legal</a> | <a href="http://www.deloitte.com/us/privacy">Privacy</a><br></p>';
            strEmailBody = strEmailBody +  '<p style="font-family:arial;font-size:9px;">30 Rockefeller Plaza<br>';
            strEmailBody = strEmailBody +  'New York, NY 10112-0015<br>';
            strEmailBody = strEmailBody +  'United States<br></p>';
            strEmailBody = strEmailBody +  ' <img src="'+Label.Deloitte_Footer_Logo+'">';
            strEmailBody = strEmailBody +  '<p style="font-family:arial;font-size:9px;">Copyright © 2013 Deloitte Development LLC. All rights reserved.<br>';
            strEmailBody = strEmailBody +  '36 USC 220506<br>';
            strEmailBody = strEmailBody +  'Member of Deloitte Touche Tohmatsu Limited<br></p>';
            strEmailBody = strEmailBody +  '<a href="http://www.linkedin.com/company/deloitte"><img src="'+Label.LinkedIn_Logo+'"></a> <a href="http://www.facebook.com/YourFutureAtDeloitte"><img src="'+Label.Facebook_Logo+'"></a> <a href="http://www.youtube.com/deloittellp"><img src="'+Label.YouTube_Logo+'"></a> <a href="http://www.deloitte.com/view/en_US/us/press/social-media/index.htm"><img src="'+Label.Twitter_Logo+'"></a> <a href="http://www.deloitte.com/view/en_US/us/press/rss-feeds/index.htm"><img src="'+Label.RSS_Feed_Logo+'"></a>';
            strEmailBody = strEmailBody +  ' </table>';
            strEmailBody = strEmailBody +  ' </center>';
            strEmailBody = strEmailBody +  ' </body>';
            strEmailBody = strEmailBody +  ' </html>'; 

            mail.setHTMLBody(strEmailBody );  
            if(!lstOWE.isEmpty())              
                mail.setOrgWideEmailAddressId(lstOWE.get(0).id) ;    
            mail.setSaveAsActivity(false);
            mail.setSubject('ACTION REQUIRED: Global Mobility and Immigration Process');
            lstSendMails.add(mail);

        }
        try{
            Messaging.SendEmailResult[] mailResults = Messaging.sendEmail(lstSendMails); 
        }
        catch(Exception ex){
            WCT_ExceptionUtility.logException('WCT_GMI_NotifySchedulableBatch','Mail Results',ex.getMessage());
        }           
    }   
}