public class WCT_GMI_createSLAvalues {
    public static boolean isAlreadyRun = false;
    public static boolean isTaskMethodRun = false;
    public static final string GMISLAImmRtypeId = WCT_Util.getRecordTypeIdByLabel('WCT_GMI_SLA__c', 'Immigration');
    public static final string GMISLAMobRtypeId = WCT_Util.getRecordTypeIdByLabel('WCT_GMI_SLA__c', 'Mobility');
  

    // Creating SLA values if there is no task associated with it.
    public static void createGMIvalues(list < sObject > newList, list < sObject > oldList, string sObjectType) {

        // Getting business hour ID for USI  
        BusinessHours stdBusinessHours = [select id from businesshours where Name = 'USI'];
        Integer working_hours = Integer.valueOf(system.label.USI_working_Hours);
        // Variable assignment
        set < String > set_immStatus = new set < String > ();
        set < String > set_immRecordTypeIds = new set < String > ();
        set < String > set_immRecordTypeName = new set < String > ();
        //map<id, string> map_id_objectType= new map<id,String>();
        map < id, id > map_id_rdt = new map < id, id > ();
        map < id, String > map_Rdid_rdName = new map < id, String > ();
        map < String, id > map_id_SLAD = new map < String, id > ();
        set < id > set_sObjectID = new set < id > ();
        list < WCT_Immigration__c > list_immToWork;
        list < WCT_Mobility__c > list_MobToWork;
        

    for (sObject s: newList) {
            if (s.getSObjectType() == WCT_Immigration__c.sObjectType) {
                list_immToWork = newList; //new list<WCT_Immigration__c>();
                for (WCT_Immigration__c lpImm: list_immToWork) {
                    //map_id_objectType.put(lpImm.id, 'WCT_Immigration__c');
                    map_id_rdt.put(lpImm.id, lpImm.RecordTypeId);
                    set_sObjectID.add(lpImm.id);
                    set_immStatus.add(lpImm.WCT_Immigration_Status__c);
                    set_immRecordTypeIds.add(lpImm.RecordTypeid);
                }
            } else if (s.getSObjectType() == WCT_Mobility__c.sObjectType) {
                list_MobToWork = newList; //new list<WCT_Immigration__c>();
                for (WCT_Mobility__c lpImm: list_MobToWork) {
                    //map_id_objectType.put(lpImm.id, 'WCT_Immigration__c');
                    map_id_rdt.put(lpImm.id, lpImm.RecordTypeId);
                    set_sObjectID.add(lpImm.id);
                    set_immStatus.add(lpImm.WCT_Mobility_Status__c);
                    set_immRecordTypeIds.add(lpImm.RecordTypeid);
                }
            }

        }

        for (Recordtype rt: [select name, id from RecordType where Id IN: set_immRecordTypeIds]) {
            set_immRecordTypeName.add(rt.name);
            map_Rdid_rdName.put(rt.id, rt.name);
        }
        
        set < String > setSLADescription = new set < String > ();
        for (Master_GMI_SLA_with_Status__c lst_GMI_SLA: [select SAL_description__c, Object_type__c,
            RecordType__c, Map_Status__c
            from Master_GMI_SLA_with_Status__c where Map_Status__c IN: set_immStatus and
            RecordType__c IN: set_immRecordTypeName and Object_type__c = : sObjectType
        ]) {
            setSLADescription.add(lst_GMI_SLA.SAL_description__c);
        }
        system.debug('Ids'+set_sObjectID);
        List<WCT_GMI_SLA__c> SLA_ValueList = new List<WCT_GMI_SLA__c> ();
        if(sObjectType == 'WCT_Immigration__c'){
            SLA_ValueList = [select id, WCT_Immigration__c, WCT_Mobility__c, WCT_SLA_Description__c
                                                    from WCT_GMI_SLA__c where RecordTypeId = :GMISLAImmRtypeId AND 
                                                     WCT_Immigration__c IN: set_sObjectID  AND WCT_Is_Closed__c = false];
        
        }else if(sObjectType == 'WCT_Mobility__c'){
            SLA_ValueList = [select id, WCT_Immigration__c, WCT_Mobility__c, WCT_SLA_Description__c
                                                    from WCT_GMI_SLA__c where RecordTypeId = :GMISLAMobRtypeId AND  
                                                      WCT_Mobility__c IN: set_sObjectID AND WCT_Is_Closed__c = false];
        }
        
        for (WCT_GMI_SLA__c SLA_ExistingSLAValues: SLA_ValueList) {
            if (SLA_ExistingSLAValues.WCT_Immigration__c != null) {
                map_id_SLAD.put(SLA_ExistingSLAValues.WCT_SLA_Description__c, SLA_ExistingSLAValues.WCT_Immigration__c);
            } else if (SLA_ExistingSLAValues.WCT_Mobility__c != null) {
                map_id_SLAD.put(SLA_ExistingSLAValues.WCT_SLA_Description__c, SLA_ExistingSLAValues.WCT_Mobility__c);
            }
        }


        list < GMI_SLA_criteria__c > lst_GMIC = [select id, type__c, Object_type__c, Owner__c, Process__c, Record_Type__c, SLA_Description__c, Actual_SLA__c from GMI_SLA_criteria__c where SLA_Description__c IN: setSLADescription];
        list < WCT_GMI_SLA__c > SLA_toinsert = new list < WCT_GMI_SLA__c > ();
        for (sObject s: newList) {
            for (GMI_SLA_criteria__c SLA_cri: lst_GMIC) {

                Id sId = s.id;
              
                if (sId != map_id_SLAD.get(SLA_cri.SLA_Description__c)) {
                    id IdRecordType = map_id_rdt.get(sId);
                    if ((SLA_cri.Object_type__c == sObjectType) && (SLA_cri.Record_Type__c == map_Rdid_rdName.get(IdRecordType))) {
                        WCT_GMI_SLA__c gm = new WCT_GMI_SLA__c();
                        gm.WCT_SLA_Description__c = SLA_cri.SLA_Description__c;
                        gm.Is_Status_master_used__c = true;
                        gm.WCT_start_Date__c = system.now();
                        gm.Process__c = SLA_cri.Process__c;
                        gm.Type_from_Criteria__c = SLA_cri.Type__c;
                        if (SLA_cri.Actual_SLA__c != null) {
                            gm.Expected_SLA_date__c = BusinessHours.add(stdBusinessHours.id, system.now(), (SLA_cri.Actual_SLA__c * working_hours * 3600000).longValue());
                        }
                        //gm.Object_type__c=;
                        //gm.WCT_Assign_To__c= SLA_cri.
                        if (sObjectType == 'WCT_Immigration__c') {
                            //gm.RecordTypeId=label.GMI_SLA_Immigration_RD_id;  
                            gm.RecordTypeId = Schema.SObjectType.WCT_GMI_SLA__c.getRecordTypeInfosByName().get('Immigration').getRecordTypeId();
                            gm.WCT_Immigration__c = sId;
                            gm.WCT_GMI_Status__c = (String) s.get(WCT_Immigration__c.WCT_Immigration_Status__c);
                        } else if (sObjectType == 'WCT_Mobility__c') {
                            //gm.RecordTypeId=label.GMI_SLA_Mobility_RD_id; 
                            gm.RecordTypeId = Schema.SObjectType.WCT_GMI_SLA__c.getRecordTypeInfosByName().get('Mobility').getRecordTypeId();
                            gm.WCT_Mobility__c = sId;
                            gm.WCT_GMI_Status__c = (String) s.get(WCT_Mobility__c.WCT_Mobility_Status__c);
                        }

                        gm.Parent_Record_Type_Name__c = map_Rdid_rdName.get(IdRecordType);
                        gm.Actual_SLA__c = SLA_cri.Actual_SLA__c;
                        SLA_toinsert.add(gm);
                    }
                }

            }

        }
        if (SLA_toinsert.size() > 0) {
            insert SLA_toinsert;
        }

        set < String > set_oldStatus = new set < String > ();
        set < id > set_parentRecordId = new set < id > ();
        list < WCT_Immigration__c > newImmList;
        list < WCT_Mobility__c > newMobList;
        Map <Id,WCT_Immigration__c> newImmMap = new Map <Id,WCT_Immigration__c>();
        Map <Id,WCT_Mobility__c> newMobMap = new Map <Id,WCT_Mobility__c>();
        if (oldList != null) {
            
        
            if(sObjectType == 'WCT_Immigration__c'){
                list_immToWork = oldList; //new list<WCT_Immigration__c>();
                newImmList = newList;
                for(WCT_Immigration__c newImm: newImmList){
                    newImmMap.put(newImm.id,newImm);
                }
                for(WCT_Immigration__c lpImm: list_immToWork){
                    if(newImmMap.containsKey(lpImm.id)){
                        if(lpImm.WCT_Immigration_Status__c != newImmMap.get(lpImm.id).WCT_Immigration_Status__c){
                            set_oldStatus.add(lpImm.WCT_Immigration_Status__c);
                            set_parentRecordId.add(lpImm.id);
                        }                   
                    }
                }
            }else if(sObjectType == 'WCT_Mobility__c'){
                list_MobToWork = oldList; //new list<WCT_Immigration__c>();
                newMobList = newList;
                
                for(WCT_Mobility__c NewMob: newMobList){
                    newMobMap.put(NewMob.id,NewMob);
                }
                
                for (WCT_Mobility__c lpImm: list_MobToWork) {
                    if(newMobMap.containsKey(lpImm.id)){
                        if(lpImm.WCT_Mobility_Status__c !=newMobMap.get(lpImm.id).WCT_Mobility_Status__c){
                            set_oldStatus.add(lpImm.WCT_Mobility_Status__c);
                            set_parentRecordId.add(lpImm.id);
                        }
                    }
                }
            }
            
            list < WCT_GMI_SLA__c > lst_GMI_SLA_toupdate = [select id, WCT_Days_Spent__c, WCT_start_Date__c, wct_Closed_Date__c
                                                                from WCT_GMI_SLA__c where(WCT_Immigration__c IN: set_parentRecordId OR 
                                                                    WCT_Mobility__c IN: set_parentRecordId) and 
                                                                    Is_Status_master_used__c = : true and 
                                                                    WCT_GMI_Status__c IN: set_oldStatus AND WCT_Is_Closed__c = false];
            for (WCT_GMI_SLA__c lp_toWork: lst_GMI_SLA_toupdate) {

                if (lp_toWork.wct_Closed_Date__c == null) {

                    lp_toWork.wct_Closed_Date__c = system.now();
                    if (lp_toWork.WCT_start_Date__c != null) {
                        long d = ((BusinessHours.diff(stdBusinessHours.id, lp_toWork.WCT_start_Date__c, lp_toWork.wct_Closed_Date__c)) / 3600000);
                        lp_toWork.WCT_Days_Spent__c = d / working_hours;
                    }
                }

            }
            if (lst_GMI_SLA_toupdate.size() > 0) {
                update lst_GMI_SLA_toupdate;
            }
        }
        isAlreadyRun = true;


    }

    //// Drive from Task



    public static void Create_GMI_SLA_value(List < Task > taskToWork) {

        // getting business hour id for USI.
        BusinessHours stdBusinessHours = [select id from businesshours where Name = 'USI'];
        Integer working_hours = Integer.valueOf(system.label.USI_working_Hours);

        set < String > SlaDescription = new set < String > ();
        list < string > listWhatid = new list < String > ();
        set < String > setWhatid = new set < String > ();

        set < String > set_immigration_ids = new set < string > ();
        set < String > set_Mobility_ids = new set < string > ();
        set < String > set_LCA_ids = new set < string > ();
        map < id, String > map_id_rtid = new map < id, String > ();
        map < id, String > map_id_Status = new map < id, String > ();
        for (Task t: taskToWork) {
            String sObjName = t.whatId.getSObjectType().getDescribe().getName();
            if (sObjName == 'WCT_Immigration__c') {
                set_immigration_ids.add(t.whatId);
            } else if (sObjName == 'WCT_Mobility__c') {
                set_Mobility_ids.add(t.whatId);
            } else if (sObjName == 'WCT_LCA__c') {
                set_LCA_ids.add(t.whatId);
            }
            SlaDescription.add(t.WCT_SLA_description__c);
            listWhatid.add(t.WhatId);
            setWhatid.add(t.WhatId);
        }
        list < WCT_GMI_SLA__c > GMISLA = new list < WCT_GMI_SLA__c > ();

        if (set_immigration_ids.size() > 0) {
            list < WCT_GMI_SLA__c > GMISLA_Immi = [select id, WCT_Assign_To__c, WCT_Days_Spent__c, GMI_Object_type__c, WCT_Immigration__c,
                WCT_LCA__c, WCT_Mobility__c, WCT_SLA_Description__c, WCT_GMI_Status__c, wct_Closed_Date__c, WCT_start_Date__c
                from WCT_GMI_SLA__c where WCT_Immigration__c IN: set_immigration_ids
            ];
            GMISLA.addall(GMISLA_Immi);
            for (WCT_Immigration__c lst_imm: [select id, RecordTypeID, WCT_Immigration_Status__c, RecordType.name from WCT_Immigration__c where id IN: set_immigration_ids]) {
                map_id_rtid.put(lst_imm.id, lst_imm.RecordType.name);
                map_id_Status.put(lst_imm.id, lst_imm.WCT_Immigration_Status__c);
            }
        }
        if (set_Mobility_ids.size() > 0) {
            list < WCT_GMI_SLA__c > GMISLA_mob = [select id, WCT_Assign_To__c, WCT_Days_Spent__c, GMI_Object_type__c, WCT_Immigration__c,
                WCT_LCA__c, WCT_Mobility__c, WCT_SLA_Description__c, WCT_GMI_Status__c, wct_Closed_Date__c, WCT_start_Date__c
                from WCT_GMI_SLA__c where WCT_Mobility__c IN: set_Mobility_ids
            ];
            GMISLA.addall(GMISLA_mob);
            for (WCT_Mobility__c lst_mob: [select id, WCT_Mobility_Status__c, RecordTypeID, RecordType.name from WCT_Mobility__c where id IN: set_Mobility_ids]) {
                map_id_rtid.put(lst_mob.id, lst_mob.RecordType.name);
                map_id_Status.put(lst_mob.id, lst_mob.WCT_Mobility_Status__c);
            }
        }
        if (set_LCA_ids.size() > 0) {
            list < WCT_GMI_SLA__c > GMISLA_LCA = [select id, WCT_Assign_To__c, WCT_Days_Spent__c, GMI_Object_type__c, WCT_Immigration__c,
                WCT_LCA__c, WCT_Mobility__c, WCT_SLA_Description__c, WCT_GMI_Status__c, wct_Closed_Date__c, WCT_start_Date__c
                from WCT_GMI_SLA__c where WCT_LCA__c IN: set_LCA_ids
            ];
            GMISLA.addall(GMISLA_LCA);
            for (WCT_LCA__c lst_LCA: [select id, RecordTypeID, WCT_Status__c, RecordType.name from WCT_LCA__c where id IN: set_LCA_ids]) {
                map_id_rtid.put(lst_LCA.id, lst_LCA.RecordType.name);
                map_id_Status.put(lst_LCA.id, lst_LCA.WCT_Status__c);
            }
        }




        for (WCT_GMI_SLA__c gT: GMISLA) {
            SlaDescription.add(gT.WCT_SLA_Description__c);

        }
        list < GMI_SLA_criteria__c > lst_GMIC = [select id, type__c, Object_type__c, Owner__c, Process__c, Record_Type__c, SLA_Description__c, Actual_SLA__c from GMI_SLA_criteria__c where SLA_Description__c IN: SlaDescription];

        list < Task > AllGMITask = [select id, WCT_SLA_description__c, CreatedDate, WCT_Closed_Date__c, WhatId, ActivityDate from Task where WhatID IN: setWhatid];
        // AllGMITask.addAll(taskToWork);

        list < WCT_GMI_SLA__c > tempSLAToupdate = new list < WCT_GMI_SLA__c > ();
        list < WCT_GMI_SLA__c > tempSLAToInsert = new list < WCT_GMI_SLA__c > ();


        if (GMISLA.size() > 0) {
            for (WCT_GMI_SLA__c gT: GMISLA) {
                for (String whatIds: setWhatid) {
                    if ((gT.WCT_Immigration__c == whatIds) || (gT.WCT_LCA__c == whatIds) || (gT.WCT_Mobility__c == whatIds)) {
                        for (String sL: SlaDescription) {
                            if (sL == gT.WCT_SLA_Description__c) {
                                list < DateTime > LststartTime = new list < DateTime > ();
                                list < DateTime > LstclosedTime = new list < DateTime > ();
                                // WCT_GMI_SLA__c tempGMI= new WCT_GMI_SLA__c();
                                for (task t: AllGMITask) {
                                    if ((t.WCT_SLA_description__c == sL) && (t.WhatId == whatIds)) {
                                        LststartTime.add(t.CreatedDate);
                                        LstclosedTime.add(t.WCT_Closed_Date__c);

                                    }
                                }



                                LststartTime.sort();
                                LstclosedTime.sort();
                                if (LststartTime.size() > 0) {
                                    gT.WCT_start_Date__c = LststartTime[0];
                                }
                                if (LstclosedTime.size() > 0) {
                                    gT.wct_Closed_Date__c = LstclosedTime[LstclosedTime.size() - 1];
                                }

                                if (gT.wct_Closed_Date__c != null) {

                                    long d = ((BusinessHours.diff(stdBusinessHours.id, gT.WCT_start_Date__c, gT.wct_Closed_Date__c)) / 3600000);
                                    gT.WCT_Days_Spent__c = d / working_hours;
                                    //   gT.WCT_Days_Spent__c  =  ((BusinessHours.diff(stdBusinessHours.id, gT.WCT_start_Date__c, gT.wct_Closed_Date__c ))/3600000)/working_hours;

                                    //  gT.WCT_Days_Spent__c  =  ((BusinessHours.diff(stdBusinessHours.id, tempGMI.WCT_start_Date__c, tempGMI.wct_Closed_Date__c ))/(working_hours*3600000))+1;
                                }
                                tempSLAToupdate.add(gT);
                                SlaDescription.remove(sL);
                            }
                        }
                    }
                }


            }

            update tempSLAToupdate;
        }


        if (SlaDescription.size() > 0) {

            for (String whatIds: setWhatid) {
                for (String sL: SlaDescription) {
                    WCT_GMI_SLA__c tempGMI = new WCT_GMI_SLA__c();
                    list < DateTime > LststartTime = new list < DateTime > ();
                    list < DateTime > LstclosedTime = new list < DateTime > ();
                    list < DateTime > LstDueDate = new list < DateTime > ();

                    for (task t: taskToWork) {
                        if ((t.WCT_SLA_description__c == sL) && (t.WhatId == whatIds)) {
                            LststartTime.add(t.CreatedDate);
                            LstclosedTime.add(t.WCT_Closed_Date__c);
                            LstDueDate.add(t.ActivityDate);
                            //  tempGMI.WCT_GMI_Status__c = t.GMI_Status__c;
                        }
                    }
                    LststartTime.sort();
                    LstclosedTime.sort();
                    LstDueDate.sort();
                    id myId = whatIds;
                    String sObjName = myId.getSObjectType().getDescribe().getName();
                    String sObjectRtName = map_id_rtid.get(myId);
                    if (sObjName == 'WCT_Immigration__c') {
                        tempGMI.WCT_Immigration__c = whatIds;
                        tempGMI.RecordTypeId = Schema.SObjectType.WCT_GMI_SLA__c.getRecordTypeInfosByName().get('Immigration').getRecordTypeId();
                        //tempGMI.RecordTypeId=label.GMI_SLA_Immigration_RD_id;  

                    } else if (sObjName == 'WCT_Mobility__c') {
                        tempGMI.WCT_Mobility__c = whatIds;
                        tempGMI.RecordTypeId = Schema.SObjectType.WCT_GMI_SLA__c.getRecordTypeInfosByName().get('Mobility').getRecordTypeId();
                    } else if (sObjName == 'WCT_LCA__c') {
                        tempGMI.WCT_LCA__c = whatIds;
                        tempGMI.RecordTypeId = Schema.SObjectType.WCT_GMI_SLA__c.getRecordTypeInfosByName().get('LCA').getRecordTypeId();

                    }

                    tempGMI.WCT_SLA_Description__c = sL;
                    if(LststartTime.size()>0){
                        tempGMI.WCT_start_Date__c = LststartTime[0];
                    }
                    if(LstclosedTime.size()>0){
                        tempGMI.wct_Closed_Date__c = LstclosedTime[LstclosedTime.size() - 1];
                    }
                    if(LstDueDate.size()>0){
                        tempGMI.Expected_SLA_date__c = LstDueDate[LstDueDate.size() - 1];
                    }
                    if ((tempGMI.WCT_GMI_Status__c == null) || (tempGMI.WCT_GMI_Status__c == '')) {
                        tempGMI.WCT_GMI_Status__c = map_id_Status.get(myId);
                    }
                    if (tempGMI.wct_Closed_Date__c != null) {
                        long d = ((BusinessHours.diff(stdBusinessHours.id, tempGMI.WCT_start_Date__c, tempGMI.wct_Closed_Date__c)) / 3600000);
                        tempGMI.WCT_Days_Spent__c = d / working_hours;
                    }
                    tempGMI.Parent_Record_Type_Name__c = sObjectRtName;
                    for (GMI_SLA_criteria__c lp_GmIC: lst_GMIC) {
                        if ((lp_GmIC.SLA_Description__c == tempGMI.WCT_SLA_Description__c) && (lp_GmIC.Object_type__c == sObjName) && (sObjectRtName == lp_GmIC.Record_Type__c)) {
                            tempGMI.GMI_SLA_Criteria__c = lp_GmIC.id;
                            tempGMI.WCT_Assign_To__c = lp_GmIC.Owner__c;
                            tempGMI.Process__c = lp_GmIC.Process__c;
                            tempGMI.Actual_SLA__c = lp_GmIC.Actual_SLA__c;
                            tempGMI.Type_from_Criteria__c = lp_GmIC.type__c;
                        }
                    }

                    tempSLAToInsert.add(tempGMI);

                }
            }

            if (tempSLAToInsert.size() > 0) {
                insert tempSLAToInsert;
            }
        }
        isTaskMethodRun = true;
    }


}