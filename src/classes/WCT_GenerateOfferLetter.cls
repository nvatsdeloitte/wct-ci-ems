global class WCT_GenerateOfferLetter {

   WebService static string generateOfferLetter(List<string> offerLetterId)
    {
     /*   Map<String,String> offerMergeFieldMap = new Map<String,String>();    
        list<WCT_Offer__c> offerRecLst = new list<WCT_Offer__c>();
        for (WCT_Offer__c offRec : [select WCT_Hiring_Partner__c, WCT_Hiring_Location__c, WCT_Salary__c, WCT_Start_Date__c, 
                                    WCT_Review_Year__c, WCT_Candidate_Entity__c, WCT_offerEmployeeLevel_Title__c, WCT_Fiscal_Year__c,
                                    WCT_First_Name__c, WCT_Last_Name__c, WCT_Address1__c, WCT_Address2__c, WCT_City__c, WCT_State__c,
                                    WCT_Zip__c, WCT_Service_Area__c, WCT_CandidateService_Line__c, WCT_Hiring_State__c, WCT_Sign_on_Bonus__c,
                                    WCT_Recruiter_Name__c, WCT_Recruiter_Phone__c
                                    from WCT_Offer__c where Id in :offerLetterId])
        {
           for (WCT_Offer_Letter_Merge_Fields__c olm : WCT_Offer_Letter_Merge_Fields__c.getAll().values()){
                offerMergeFieldMap.put(offRec.id+''+olm.Name, (String) string.valueOf(offRec.get(olm.Merge_Field_API_Name__c)) );
            }
        }
        set<id> offerLetterIds = new set<id>();
        for(string offId: offerLetterId){
            offerLetterIds.add(offId);
        }
        
        list<WCT_Offer_Master__c> offerLetterMaster = [SELECT WCT_Paragraph_Abbribation__c, WCT_Paragraph_Content__c, 
                                                        WCT_Paragraph_Number__c, WCT_Paragraph_Page_No__c, 
                                                        WCT_IsNeeded__c, WCT_IsCondition__c, WCT_Condition__c, WCT_Country__c,
                                                        WCT_Hiring_State__c, WCT_Office_State__c, WCT_Job_Type__c
                                                        from WCT_Offer_Master__c 
                                                        where WCT_IsNeeded__c = true
                                                        ORDER BY WCT_Paragraph_Page_No__c ASC NULLS FIRST, WCT_Paragraph_Number__c ASC NULLS FIRST];
                                                        
        for(WCT_offer__c OfferRec : [select WCT_Hiring_Partner__c, WCT_Hiring_Location__c, WCT_Salary__c, WCT_Sign_on_Bonus__c, 
                                    WCT_Start_Date__c, WCT_Fiscal_Year__c, WCT_First_Name__c, WCT_Last_Name__c, WCT_City__c,WCT_State__c,
                                    WCT_Review_Year__c, WCT_Candidate_Entity__c, WCT_Address1__c, WCT_Address2__c, WCT_Zip__c,
                                    WCT_Country__c, WCT_offerEmployeeLevel_Title__c, WCT_Relocation_Amt__c, WCT_Travel_Required__c,
                                    WCT_Service_Area__c, WCT_CandidateService_Line__c, WCT_Hiring_State__c,
                                    WCT_Recruiter_Name__c, WCT_Recruiter_Phone__c,
                                    WCT_Job_Type__c
                                    from WCT_Offer__c where id in :offerLetterIds]){
            WCT_Offer__c offerLetter = new WCT_Offer__c(Id=OfferRec.Id);
            for(WCT_Offer_Master__c offLetMasRec : offerLetterMaster){
                if(OfferRec.WCT_Country__c==offLetMasRec.WCT_Country__c
                    //&& (string.valueOf(offLetMasRec.WCT_Job_Type__c).contains(OfferRec.WCT_Job_Type__c))
                    //&& OfferRec.WCT_Hiring_State__c==offLetMasRec.WCT_Hiring_State__c && OfferRec.WCT_Office_State__c==offLetMasRec.WCT_Office_State__c){
                    ){
                    string offerMasterRecVal = '';

                    if(offLetMasRec.WCT_IsCondition__c==true){
                        if((offLetMasRec.WCT_Condition__c=='FEDERAL' && OfferRec.WCT_Candidate__r.WCT_Federal_Flag__c == 'Y')
                            || (offLetMasRec.WCT_Condition__c=='BeforeMarch' && (System.now().Month()<=3 && (System.now().Month()==3 && System.now().Day()==1)))
                            || (offLetMasRec.WCT_Condition__c=='AfterMarch' && ((System.now().Month()>=3) || ((System.now().Month()==3 && System.now().Day()>=1))))
                            || (offLetMasRec.WCT_Condition__c=='SignOnBonus' && OfferRec.WCT_Sign_on_Bonus__c!=null)
                            || (offLetMasRec.WCT_Condition__c=='RelocationAmt' && OfferRec.WCT_Relocation_Amt__c!=null)
                            || (offLetMasRec.WCT_Condition__c=='TravelRequired' && OfferRec.WCT_Travel_Required__c==true)
                            || (offLetMasRec.WCT_Condition__c=='NOT_Deloitte_Consulting_LLP' && OfferRec.WCT_Candidate_Entity__c != 'Deloitte Consulting LLP')
                            || (offLetMasRec.WCT_Condition__c=='New_York' && OfferRec.WCT_Hiring_State__c=='New York')
                            || (offLetMasRec.WCT_Condition__c=='NOT_Deloitte_Touche_LLP' && OfferRec.WCT_Candidate_Entity__c != 'Deloitte & Touche LLP')
                            || (offLetMasRec.WCT_Condition__c== 'SignOnBonus/RelocationAmt' && (OfferRec.WCT_Relocation_Amt__c!=null || OfferRec.WCT_Sign_on_Bonus__c!=null))
                            || (offLetMasRec.WCT_Condition__c== 'SignOnBonus_RelocationAmt' && (OfferRec.WCT_Relocation_Amt__c!=null && OfferRec.WCT_Sign_on_Bonus__c!=null))
                            ){
                            if(!offerMergeFieldMap.isEmpty()){
                            string singonBon_ReloacBon;
                            if(offLetMasRec.WCT_Condition__c== 'SignOnBonus/RelocationAmt' && (OfferRec.WCT_Relocation_Amt__c!=null || OfferRec.WCT_Sign_on_Bonus__c!=null)){
                                if(OfferRec.WCT_Relocation_Amt__c!=null){singonBon_ReloacBon='Relocation';}
                                if(OfferRec.WCT_Sign_on_Bonus__c!=null){singonBon_ReloacBon='Sign-on Bonus';}
                                offerMasterRecVal += offerMasterRecVal+' '+singonBon_ReloacBon;
                            }
                                for (string offMerMap : offerMergeFieldMap.keySet()){
                                    List<String> offMerMapKeyName = new List<String>();
                                    offMerMapKeyName = offMerMap.split(OfferRec.Id);
                                    if(offMerMapKeyName.size()>1){
                                        if(offerMasterRecVal==''){
                                            if(offerMergeFieldMap.get(offMerMap)!=null)offerMasterRecVal = offLetMasRec.WCT_Paragraph_Content__c.replaceAll(offMerMapKeyName[1],offerMergeFieldMap.get(offMerMap));
                                           }
                                        else{
                                            if(offerMergeFieldMap.get(offMerMap)!=null)offerMasterRecVal = offerMasterRecVal.replaceAll(offMerMapKeyName[1],offerMergeFieldMap.get(offMerMap));
                                        }
                                    }
                                }
                            }
                            else{
                                offerMasterRecVal = offLetMasRec.WCT_Paragraph_Content__c;
                            }
                        }
                    }
                    //system.debug('-Before else-'+offLetMasRec.WCT_IsCondition__c);
                    else if (offLetMasRec.WCT_IsCondition__c==false){system.debug('-After else-'+offLetMasRec.WCT_IsCondition__c);
                        if(!offerMergeFieldMap.isEmpty()){
                            for (string offMerMap : offerMergeFieldMap.keySet()){
                                List<String> offMerMapKeyName = new List<String>();
                                offMerMapKeyName = offMerMap.split(OfferRec.Id);
                                if(offMerMapKeyName.size()>1){
                                    if(offerMergeFieldMap.get(offMerMap)!=null){
                                        if(offerMasterRecVal==''){
                                            system.debug('--1--'+offMerMapKeyName[1]);
                                            system.debug('--2--'+offerMergeFieldMap.get(offMerMap));
                                            
                                            if(offerMergeFieldMap.get(offMerMap)!=null)offerMasterRecVal = offLetMasRec.WCT_Paragraph_Content__c.replaceAll(offMerMapKeyName[1],offerMergeFieldMap.get(offMerMap));
                                           }
                                        else{
                                            if(offerMergeFieldMap.get(offMerMap)!=null)offerMasterRecVal = offerMasterRecVal.replaceAll(offMerMapKeyName[1],offerMergeFieldMap.get(offMerMap));
                                        }
                                    }
                                }
                            }
                        }
                        else{
                            offerMasterRecVal = offLetMasRec.WCT_Paragraph_Content__c;
                        }
                    }
                    offerMasterRecVal = offerMasterRecVal.replaceAll('<<CURRENT_DATE>>',String.valueOf(system.today()));
                    if(offerMasterRecVal.contains('<<<<'))offerMasterRecVal = offerMasterRecVal.replaceAll('<<<<','{{');
                    if(offerMasterRecVal.contains('>>>>'))offerMasterRecVal = offerMasterRecVal.replaceAll('>>>>','}}');
                    
                    if(offLetMasRec.WCT_Paragraph_Page_No__c == 1 && offerMasterRecVal != null){
                        if(offerLetter.WCT_Page1OfferLetter__c != null){offerLetter.WCT_Page1OfferLetter__c += offerMasterRecVal;}
                        else{offerLetter.WCT_Page1OfferLetter__c = offerMasterRecVal;}
                    }
                    if(offLetMasRec.WCT_Paragraph_Page_No__c == 2 && offerMasterRecVal != null){
                        if(offerLetter.WCT_Page2OfferLetter__c != null){offerLetter.WCT_Page2OfferLetter__c += offerMasterRecVal;}
                        else{offerLetter.WCT_Page2OfferLetter__c = offerMasterRecVal;}
                    }
                    if(offLetMasRec.WCT_Paragraph_Page_No__c == 3 && offerMasterRecVal != null){
                        if(offerLetter.WCT_Page3OfferLetter__c != null){offerLetter.WCT_Page3OfferLetter__c += offerMasterRecVal;}
                        else{offerLetter.WCT_Page3OfferLetter__c = offerMasterRecVal;}
                    }
                    if(offLetMasRec.WCT_Paragraph_Page_No__c == 4 && offerMasterRecVal != null){
                        if(offerLetter.WCT_Page4OfferLetter__c != null){offerLetter.WCT_Page4OfferLetter__c += offerMasterRecVal;}
                        else{offerLetter.WCT_Page4OfferLetter__c = offerMasterRecVal;}
                    }
                    if(offLetMasRec.WCT_Paragraph_Page_No__c == 5 && offerMasterRecVal != null){
                        if(offerLetter.WCT_Page5OfferLetter__c != null){offerLetter.WCT_Page5OfferLetter__c += offerMasterRecVal;}
                        else{offerLetter.WCT_Page5OfferLetter__c = offerMasterRecVal;}
                    }
                    if(offLetMasRec.WCT_Paragraph_Page_No__c == 6 && offerMasterRecVal != null){
                        if(offerLetter.WCT_Page6OfferLetter__c != null){offerLetter.WCT_Page6OfferLetter__c += offerMasterRecVal;}
                        else{offerLetter.WCT_Page6OfferLetter__c = offerMasterRecVal;}
                    }
                    if(offLetMasRec.WCT_Paragraph_Page_No__c == 7 && offerMasterRecVal != null){
                        if(offerLetter.WCT_Page7OfferLetter__c != null){offerLetter.WCT_Page7OfferLetter__c += offerMasterRecVal;}
                        else{offerLetter.WCT_Page7OfferLetter__c = offerMasterRecVal;}
                    }
                    if(offLetMasRec.WCT_Paragraph_Page_No__c == 8 && offerMasterRecVal != null){
                        if(offerLetter.WCT_Page8OfferLetter__c != null){offerLetter.WCT_Page8OfferLetter__c += offerMasterRecVal;}
                        else{offerLetter.WCT_Page8OfferLetter__c = offerMasterRecVal;}
                    }
                }
            }
            offerRecLst.add(offerLetter);
        }
            if(!offerRecLst.IsEmpty())update offerRecLst;*/
            return 'true';
    }
}