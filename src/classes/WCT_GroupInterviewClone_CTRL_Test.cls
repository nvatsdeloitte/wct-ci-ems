@isTest(SeeAllData=false)

public class WCT_GroupInterviewClone_CTRL_Test{
/*static testmethod void WCT_GroupInterviewClone_CTRL_testmethod(){
WCT_GroupInterviewClone_CTRL WCTGIC = new WCT_GroupInterviewClone_CTRL();
WCT_Interview__c intrrec = new WCT_Interview__c();
intrrec.WCT_Interview_Status__c ='Open';
intrrec.WCT_Interview_Type__c= 'true';
intrrec.WCT_User_group__c ='India';
intrrec.WCT_Clicktools_Form__c ='India_Experienced_Consulting_Lateral IEF';


insert intrrec;

User userrec = CTBulkDownloadSearch_Test.createUserRecCoo();


Interview_Candidate_Tracker_Junction__c intejun = new Interview_Candidate_Tracker_Junction__c ();
intejun.WCT_Interview_Junction_Status__c = 'Open';
intejun.WCT_Interview__c  = ;
intejun.WCT_Candidate_Tracker__c = ;
intejun.WCT_Interviewer__c = userrec.id ;
intejun.WCT_Requisition_RC_Email__c = 'sboojala=deloitte.com@example.com';
intejun.WCT_IEF_Submission_Status__c = 'Pending';
//intejun.WCT_Interviewer_Recommendation__c = ;



ApexPages.currentPage().getParameters().put('id',intrrec.id);
//WCT_GroupInterviewClone_CTRL.NEW_STATUS='New';
//WCT_GroupInterviewClone_CTRL.OPEN_STATUS='Open';
//WCT_GroupInterviewClone_CTRL.INPROGRESS_STATUS='In Progress';
WCTGIC.doSearch();
//WCTGIC.InviteeWrapper();
//WCTGIC.doSearchUser();
//WCTGIC.addUser();
WCTGIC.removeUser();
//WCTGIC.addCandidate();
//WCTGIC.removeCandidate();
//WCTGIC.cloneIntv();
//WCTGIC.cancel();
//WCTGIC.searchInterview();
//WCTGIC.proClonePage();
}*/
    public static Id profileIdRecCoo=[Select id from Profile where Name=:WCT_UtilConstants.Recruiting_Coordinator].Id;
    public static Id profileIdIntv=[Select id from Profile where Name=:WCT_UtilConstants.INTERVIWER_PROFILE_NAME].Id;
    public static Id profileIdRecr=[Select id from Profile where Name=:WCT_UtilConstants.RECRUITER_COMPANY].Id;
    public static String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();
    public static Event InterviewEvent;
    public static EventRelation InterviewEventRelation;
    public static User userRecRecCoo, userRecRecIntv, userRecRecr;
    public static WCT_Interview__c Interview;
    public static Document document;
    public static WCT_List_Of_Names__c ClickToolForm;
    public static WCT_Interview_Junction__c InterviewTracker;
    public static Contact Candidate,Candidate1,Candidate2;
    public static WCT_Requisition__c Requisition;
    public static WCT_Candidate_Requisition__c CandidateRequisition;
    public static contact Con;
    public static WCT_Candidate_Requisition__c CanReq;  
    public static WCT_Requisition__c Reqs;
    Public static Event Eventrel;
    
    
    
    /** 
        Method Name  : createUserRecCoo
        Return Type  : User
        Type         : private
        Description  : Create temp records for data mapping         
    */
       private Static User createUserRecCoo()
    {
        userRecRecCoo=WCT_UtilTestDataCreation.createUser('RecCFinl', profileIdRecCoo, 'arunsharmaRecCooFinl@deloitte.com', 'arunsharma4@deloitte.com');
        userRecRecCoo.isActive =true;
        insert userRecRecCoo;
        return  userRecRecCoo;
    } 
    /** 
        Method Name  : createUserRecr
        Return Type  : User
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecr()
    {
        userRecRecr=WCT_UtilTestDataCreation.createUser('RecrFinl', profileIdRecr, 'arunsharmaRecrFinl@deloitte.com', 'arunsharma4@deloitte.com');
        userRecRecr.isActive =true;
        insert userRecRecr;
        return  userRecRecr;
    }
  
    /** 
        Method Name  : createUserIntv
        Return Type  : User
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserIntv()
    {
        userRecRecIntv=WCT_UtilTestDataCreation.createUser('IntvFinl', profileIdIntv, 'arunsharmaIntvFinl@deloitte.com', 'arunsharma4@deloitte.com');
        userRecRecIntv.isActive =true;
        insert userRecRecIntv;
        return  userRecRecIntv;
    }  
    /** 
        Method Name  : createDocument
        Return Type  : Document
        Type         : private
        Description  : Create temp records for data mapping         
    */    
    private Static Document createDocument()
    {
        document=WCT_UtilTestDataCreation.createDocument();
        insert document;
        return  document;
    }     
    /** 
        Method Name  : createClickToolForm
        Return Type  : WCT_List_Of_Names__c
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_List_Of_Names__c createClickToolForm()
    {
        ClickToolForm=WCT_UtilTestDataCreation.createClickToolForm(document.id);
        insert ClickToolForm; 
        return  ClickToolForm;
    }      
    /** 
        Method Name  : createCandidate
        Return Type  : Contact
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static Contact createCandidate()
    {
        Candidate=WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
        insert Candidate;
        return  Candidate;
    }

    private Static Contact createCandidate1()
    {
        Candidate1=WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
        Candidate1.WCT_Taleo_Id__c = '301989';
        insert Candidate1;
        return  Candidate1;
    }
    
    private Static Contact createCandidate2()
    {
        Candidate2=WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
        Candidate2.WCT_Taleo_Id__c = '301991';
        insert Candidate2;
        return  Candidate2;
    }
    /** 
        Method Name  : createRequisition
        Return Type  : WCT_Requisition__c
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Requisition__c createRequisition()
    {
        Requisition=WCT_UtilTestDataCreation.createRequisition(userRecRecr.Id);
        insert Requisition;
        return  Requisition;
    }
    

    /** 
        Method Name  : createInterview
        Return Type  : WCT_Interview__c
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview__c createInterview()
    {
        Interview=WCT_UtilTestDataCreation.createInterview(ClickToolForm.Id);
        system.runAs(userRecRecCoo)
        {
            insert Interview;
        }
        return  Interview;
    }   
    
     
     /** 
        Method Name  : createCandidateRequisition
        Return Type  : WCT_Candidate_Requisition__c
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Candidate_Requisition__c createCandidateRequisition()
    {
        CandidateRequisition=WCT_UtilTestDataCreation.createCandidateRequisition(Candidate.ID,Requisition.Id);
        //CandidateRequisition.WCT_Related_Interview_Tracker__c =Interview.Id;
        insert CandidateRequisition;
        return  CandidateRequisition;
    }

    /** 
        Method Name  : createInterviewTracker
        Return Type  : WCT_Interview_Junction__c
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview_Junction__c createInterviewTracker()
    {
        InterviewTracker=WCT_UtilTestDataCreation.createInterviewTracker(Interview.Id,CandidateRequisition.Id,userRecRecIntv.Id);
        InterviewTracker.WCT_Interviewer_Recommendation__c ='Select';
       
        //InterviewTracker.WCT_Requisition_ID__c = CandidateRequisition.Id;
       Requisition.Name = 'XYZ';
       update Requisition;
     //  Requisition = new WCT_Requisition__c (Interview=InterviewTracker.Id);
       // update Requisition;
        CandidateRequisition.WCT_Requisition__c = Requisition.Id;
        update CandidateRequisition;
        createCandidateRequisition();
        
        insert InterviewTracker;
        return  InterviewTracker;
    }  
    
    /** 
        Method Name  : createEvent
        Return Type  : Event
        Type         : private
        Description  : Create temp records for data mapping         
    */    
    private Static Event createEvent()
    {
        InterviewEvent=WCT_UtilTestDataCreation.createEvent(Interview.Id);

        Eventrel = new Event(StartDateTime=system.date.today()-1,enddateTime=system.date.today());
        insert Eventrel;
                         
         system.runAs(userRecRecCoo)
        {
            insert InterviewEvent;
        }
        return  InterviewEvent;
    }     
    /** 
        Method Name  : createEventRelation
        Return Type  : EventRelation
        Type         : private
        Description  : Create temp records for data mapping         
    */    
    private Static EventRelation createEventRelation()
    {
        InterviewEventRelation=WCT_UtilTestDataCreation.createEventRelation(InterviewEvent.Id,userRecRecIntv.Id);
        insert InterviewEventRelation;
        return  InterviewEventRelation;
    }  
    private static void contactCon(){
        con= new contact(FirstName = 'ABC', LastName='XYZ',HomePhone = '112345',Email = 'abc5@xzy.com');
    insert con;
            }   
    private static void requistion(){
        Reqs = new WCT_Requisition__c(Name='ZXD');
            insert reqs;
    }
    private static void  CandidateReq(){
        CanReq = new WCT_Candidate_Requisition__c(WCT_Select_Candidate_For_Interview__c=true,WCT_Taleo_Id__c='1244335',WCT_Requisition__c=Reqs.Id,WCT_Contact__c = con.Id);
    insert CanReq;
            }   
    private static void Eventr(){
        Eventrel = new Event(StartDateTime=system.date.today(),enddateTime=system.date.today());
            insert Eventrel;
            }   
    
    static testMethod void withInvitees() {
        test.startTest();
        
        userRecRecCoo=createUserRecCoo();
        userRecRecIntv=createUserIntv();
        userRecRecr=createUserRecr();
        document=createDocument();
        ClickToolForm=createClickToolForm();
        Candidate=createCandidate();
        Candidate1=createCandidate1();
        Requisition=createRequisition();        
        Interview=createInterview();
        CandidateRequisition=WCT_UtilTestDataCreation.createCandidateRequisition(Candidate1.ID,Requisition.Id);
        //CandidateRequisition.WCT_Related_Interview_Tracker__c =Interview.Id;
        insert CandidateRequisition;
        
        InterviewEvent=createEvent();
        InterviewEventRelation=createEventRelation();
        InterviewTracker=createInterviewTracker();
        list<string> intJunLst = new list<string>();
        intJunLst.add(InterviewTracker.Id);
        system.debug('=================>Interview.id1'+Interview.id);
        ApexPages.currentPage().getParameters().put('id',Interview.id);
        WCT_GroupInterviewClone_CTRL WCTGIC= new  WCT_GroupInterviewClone_CTRL();       
        system.debug('=================>Interview.id2'+Interview.id);
         contactCon();
       requistion();
        CandidateReq();
        WCTGIC.contactName = 'ABC'; 
          WCTGIC.contactphone = '112345'; 
         WCTGIC.contactEmail = 'abc5@xzy.com';
        //  WCTGIC.LastName='XYZ';
        WCTGIC.reqNum = 'ZXD';
        WCTGIC.rmsID = '1244335';
        WCTGIC.doSearch();
        //WCTGIC.InviteeWrapper();
        WCTGIC.userName= 'Testing1';
        WCTGIC.Email = 'arunsharma4@deloitte.com';
        WCTGIC.addCandidate();
        WCTGIC.doSearchUser();
        WCTGIC.addCandidate();
        
        WCTGIC.availableInvitees[0].isSelected = true;
        WCTGIC.selectedInvitees[0].isSelected = true;
        WCTGIC.addCandidate();
        WCTGIC.doSearch();
        
        WCTGIC.availableInvitees[0].isSelected = true;
        WCTGIC.selectedInvitees[0].isSelected = true;
        WCTGIC.addCandidate();
        WCTGIC.addUser();
        WCTGIC.removeUser();
        
        WCTGIC.removeCandidate();
        WCTGIC.cloneIntv();
        WCTGIC.eventToCreate.StartDateTime = null;
        WCTGIC.cloneIntv();
        WCTGIC.eventToCreate.EndDateTime = null;
        WCTGIC.cloneIntv();
        WCTGIC.eventToCreate.StartDateTime = system.now()+1;
        WCTGIC.eventToCreate.EndDateTime = system.now()-1;
        WCTGIC.cloneIntv();
        WCTGIC.eventToCreate.EndDateTime = system.now()+1;
        WCTGIC.eventToCreate.StartDateTime = system.now()+2;
        WCTGIC.cloneIntv();
        WCTGIC.ctSelectedList.clear();
        WCTGIC.cloneIntv();  
        WCTGIC.cancel();
        WCTGIC.searchInterview();
        if(!WCTGIC.addIntvWrapperList.isEmpty()){
        WCTGIC.addIntvWrapperList[0].isSelected=true;
        }
        WCTGIC.proClonePage();
        test.stopTest();
    }
    static testMethod void withInvitees2() {
        test.startTest();
        
        userRecRecCoo=createUserRecCoo();
        userRecRecIntv=createUserIntv();
        userRecRecr=createUserRecr();
        document=createDocument();
        ClickToolForm=createClickToolForm();
        Candidate=createCandidate();
        Candidate2=createCandidate2();
        Requisition=createRequisition();        
        Interview=createInterview();
        CandidateRequisition=WCT_UtilTestDataCreation.createCandidateRequisition(Candidate2.ID,Requisition.Id);
        //CandidateRequisition.WCT_Related_Interview_Tracker__c =Interview.Id;
        insert CandidateRequisition;
        
        InterviewEvent=createEvent();
        InterviewEventRelation=createEventRelation();
        InterviewTracker=createInterviewTracker();
        list<string> intJunLst = new list<string>();
        intJunLst.add(InterviewTracker.Id);
        system.debug('=================>Interview.id1'+Interview.id);
        ApexPages.currentPage().getParameters().put('id',Interview.id);
        WCT_GroupInterviewClone_CTRL WCTGIC= new  WCT_GroupInterviewClone_CTRL();       
        system.debug('=================>Interview.id2'+Interview.id);
         contactCon();
       requistion();
        CandidateReq();
        WCTGIC.contactName = 'ABC'; 
          WCTGIC.contactphone = '112345'; 
         WCTGIC.contactEmail = 'abc5@xzy.com';
        //  WCTGIC.LastName='XYZ';
        WCTGIC.reqNum = 'ZXD';
        WCTGIC.rmsID = '1244335';
        WCTGIC.doSearch();
        WCTGIC.addCandidate();
        
        WCTGIC.availableInvitees[0].isSelected = true;
        WCTGIC.selectedInvitees[0].isSelected = true;
        WCTGIC.addCandidate();
        WCTGIC.doSearch();
       
        WCTGIC.availableInvitees[0].isSelected = true;
        WCTGIC.selectedInvitees[0].isSelected = true;
        WCTGIC.addCandidate();
        //WCTGIC.InviteeWrapper();
        WCTGIC.userName= 'Testing1';
         WCTGIC.Email = 'arunsharma4@deloitte.com';
         WCTGIC.addCandidate();
        WCTGIC.doSearchUser();
        WCTGIC.addUser();
        WCTGIC.removeUser();
       
        WCTGIC.removeCandidate();
        WCTGIC.cloneIntv();
        WCTGIC.eventToCreate.StartDateTime = null;
        WCTGIC.cloneIntv();
        WCTGIC.eventToCreate.EndDateTime = null;
        WCTGIC.cloneIntv();
        WCTGIC.eventToCreate.StartDateTime = system.now()+1;
        WCTGIC.eventToCreate.EndDateTime = system.now()-1;
        WCTGIC.cloneIntv();
        WCTGIC.eventToCreate.EndDateTime = system.now()+1;
        WCTGIC.eventToCreate.StartDateTime = system.now()+2;
        WCTGIC.cloneIntv();
        WCTGIC.ctSelectedList.clear();
        WCTGIC.cloneIntv();  
        WCTGIC.cancel();
        WCTGIC.searchInterview();
        if(!WCTGIC.addIntvWrapperList.isEmpty()){
        WCTGIC.addIntvWrapperList[0].isSelected=false;
        }
        WCTGIC.proClonePage();
        test.stopTest();
    }

}