/**
 * Description : This is a test class for WCT_HomePageComponentsController
 */
@isTest
private class WCT_HomePageComponentsController_test {

    static testMethod void getCaseFilterTest() {
    	Test.startTest();
    	WCT_HomePageComponentsController homePage = new WCT_HomePageComponentsController();
    	homePage.getCaseFilter();
    	Test.stopTest();
    }
    
    
    static testMethod void getCandidateRequisitionFilterTest() {
    	Test.startTest();
    	WCT_HomePageComponentsController homePage = new WCT_HomePageComponentsController();
    	homePage.getCandidateRequisitionFilter();
    	Test.stopTest();
    }
    
    
    static testMethod void getInterviewsFilterTest() {
    	Test.startTest();
    	WCT_HomePageComponentsController homePage = new WCT_HomePageComponentsController();
    	homePage.getInterviewsFilter();
    	Test.stopTest();
    }
    
    
    static testMethod void getInterviewListTest() {
    	Test.startTest();
    	WCT_HomePageComponentsController homePage = new WCT_HomePageComponentsController();
    	homePage.filterId='Cancelled';
    	homePage.getInterviewList();
    	homePage.filterId='New';
    	homePage.getInterviewList();
    	homePage.filterId='In Progress';
    	homePage.getInterviewList();
    	homePage.filterId='Invite Sent';
    	homePage.getInterviewList();
    	homePage.filterId='Complete';
    	homePage.getInterviewList();
    	homePage.filterId='Today';
    	homePage.getInterviewList();
    	homePage.filterId='Tomorrow';
    	homePage.getInterviewList();
    	homePage.filterId='Next 7 Days';
    	homePage.getInterviewList();
    	homePage.filterId='This Month';
    	homePage.getInterviewList();
    	homePage.filterId='All';
    	homePage.getInterviewList();
    	Test.stopTest();
    }
    
    
    static testMethod void getCandidateRequisitionsTest() {
    	Test.startTest();
    	WCT_HomePageComponentsController homePage = new WCT_HomePageComponentsController();
    	homePage.filterId = 'Cancelled';
    	homePage.getCandidateRequisitions();
    	homePage.filterId = 'New';
    	homePage.getCandidateRequisitions();
    	homePage.filterId = 'In Progress';
    	homePage.getCandidateRequisitions();
    	homePage.filterId = 'Completed';
    	homePage.getCandidateRequisitions();
    	Test.stopTest();
    }
    
    
    static testMethod void getCases() {
    	Test.startTest();
    	WCT_HomePageComponentsController homePage = new WCT_HomePageComponentsController();
    	homePage.filterId = 'Open';
		homePage.getCases();
    	homePage.filterId = 'Closed';
		homePage.getCases();
    	homePage.filterId = 'All';
		homePage.getCases();
    	Test.stopTest();
    }
}