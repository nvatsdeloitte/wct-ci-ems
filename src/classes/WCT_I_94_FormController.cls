public class WCT_I_94_FormController extends SitesTodHeaderController 
{
    /* public variables */
    public WCT_Mobility__c MobilityRecord{get;set;}
    public I_94__c I94Record{get;set;}
    
    /* Upload Related Variables */
    public task t{get;set;}
    public String taskid{get;set;}
    public string I94Expiration{get;set;}
    
    // Error Message related variables
     public boolean pageError {get; set;}
     public String pageErrorMessage {get; set;}
     public String supportAreaErrorMesssage {get; set;}  
    
    public WCT_I_94_FormController ()
    {
        
         /*Initialize all Variables*/
        init();

        /*Get Task ID from Parameter*/
        getParameterInfo();  
        
        /*Task ID Null Check*/
        if(taskid=='' || taskid==null)
        {
           invalidEmployee=true;
           return;
        }

        /*Get Task Instance*/
        getTaskInstance();
        
       
        /*Query to get Immigration Record*/  
        getMobilityDetails();
        
    }
    private void init(){

        pageError = false;
        pageErrorMessage = '';
        MobilityRecord= new WCT_Mobility__c (); 
        I94Record = new I_94__c();
      
       
    }   
    
  private void getParameterInfo(){
        taskid = ApexPages.currentPage().getParameters().get('taskid');
    }

    private void getTaskInstance(){
        t=[SELECT Status, OwnerId, WhatId, WCT_Auto_Close__c, WCT_Is_Visible_in_TOD__c  FROM Task WHERE Id =: taskid];
    }
  
        


public void getMobilityDetails(){
             MobilityRecord= [SELECT   Id,
                                     Name,
                                     WCT_First_Working_Day_in_US__c,
                                     WCT_Last_Working_Day_in_US__c,
                                     WCT_USI_Resource_Manager__c,
                                     WCT_I_94_Expiration__c,
                                     WCT_US_Project_Mngr__c,
                                     WCT_Mobility_Employee__c,
                                     WCT_Purpose_of_Travel__c
                                     FROM WCT_Mobility__c
                                     where id=:t.WhatId ];  
  
}
   public pagereference insertorupdate()
   {
      /*Custom Validation Rules are handled here*/
    
        if( (null == I94Expiration) ||('' == I94Expiration))
            {
            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
        }
        
       //Update Dates
        if(String.isNotBlank(I94Expiration)){
           MobilityRecord.WCT_I_94_Expiration__c = Date.parse(I94Expiration);
        }
      update MobilityRecord;
      
      //creating I-94 record and association it with mobility record updated above.
        I94Record.WCT_Mobility__c = MobilityRecord.id;
        I94Record.WCT_Employee__c = MobilityRecord.WCT_Mobility_Employee__c;
        I94Record.WCT_I94_Expiration__c = MobilityRecord.WCT_I_94_Expiration__c;
        insert I94Record;
        
      if(t.WCT_Auto_Close__c == true){   
           t.status = 'completed';
       }else{
           t.status = 'Employee Replied';  
       }
       t.WCT_Is_Visible_in_TOD__c = false; 
       upsert t;
       
      return (new pagereference('/apex/WCT_I_94_FormThankYou?em='+CryptoHelper.encrypt(LoggedInContact.Email)+'&taskid='+taskid) );
     // return null;
   }
}