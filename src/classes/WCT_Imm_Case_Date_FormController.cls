public class WCT_Imm_Case_Date_FormController extends SitesTodHeaderController{
    public task taskRecord{get;set;}
    public WCT_Task_Reference_Table__c taskRefRecord{get;set;}
    public String taskSubject {get;set;}
    public String taskid{get;set;}
    public Boolean display{get;set;}
    public String caseSubmissionDate{get;set;}
    public String caseSubmissionLocation{get;set;}
    public Date caseDate {get;set;}
    public String taskVerbiage{get;set;}
    
     /* public variables */
    public WCT_Immigration__c ImmigrationRec {get;set;}
    
    // Error Message related variables
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}
    
    public WCT_Imm_Case_Date_FormController()
    {
         /*Initialize all Variables*/
        init();
        
        taskid = ApexPages.currentPage().getParameters().get('taskid');
        display=true;
        caseSubmissionDate = '';
        taskSubject = '';
        taskVerbiage = '';
    }

    private void init(){

        //Custom Object Instances
        ImmigrationRec = new WCT_Immigration__c();

    }   
    public void updateTaskFlags()
    {
        if(taskid==''|| taskid==null){
            display=false;
            return;
        }

        taskRecord=[SELECT id, Subject, WhatId, Ownerid, Status, WCT_Is_Visible_in_TOD__c, WCT_Auto_Close__c, WCT_Task_Reference_Table_ID__c FROM Task WHERE Id =: taskid];
        taskRefRecord = [SELECT Id, Name, WCT_Task_for_Object__c, WCT_Task_Subject__c, Form_Verbiage__c, ownerId FROM WCT_Task_Reference_Table__c Where id=:taskRecord.WCT_Task_Reference_Table_ID__c];
        ImmigrationRec = [SELECT Id, Name, OwnerId FROM WCT_Immigration__c WHERE Id=:taskRecord.WhatId];

        //Get Task Subject
        taskSubject = taskRecord.Subject;
        taskVerbiage = taskRefRecord.Form_Verbiage__c;

    }
    
    public pageReference save()
    {

        /*Custom Validation Rules are handled here*/

        if(caseSubmissionDate == '' || caseSubmissionDate == null || caseSubmissionLocation == '' || caseSubmissionLocation == null)
        {
            pageErrorMessage = 'Please submit all the information in order to submit the form.';
            pageError = true;
            return null;
        }

        caseDate = date.parse(caseSubmissionDate);
        ImmigrationRec.WCT_Visa_Interview_Date__c = caseDate.addDays(12);
        ImmigrationRec.WCT_OFC_Appointment_Date__c = caseDate.addDays(12);
        ImmigrationRec.WCT_OFC_Location__c = caseSubmissionLocation;
        ImmigrationRec.WCT_Visa_Interview_Location__c = caseSubmissionLocation;
        update ImmigrationRec;
            
        /*Changing the Owner Id of Task Temporarily to avoid Integrity Exception*/
        taskRecord.OwnerId=UserInfo.getUserId();
        upsert taskRecord;

        if(string.valueOf(ImmigrationRec.OwnerId).startsWith('00G')){
            taskRecord.OwnerId = System.Label.GMI_User;
        }else{
            taskRecord.OwnerId = ImmigrationRec.Ownerid;
        }
        

        //updating task record
        if(taskRecord.WCT_Auto_Close__c == true){   
            taskRecord.status = 'Completed';
        }else{
            taskRecord.status = 'Employee Replied';  
        }

        taskRecord.WCT_Is_Visible_in_TOD__c = false; 
        upsert taskRecord;
        
        //Reset Page Error Message
        pageError = false;
        
        /*Set Page Reference After Save*/
        return new PageReference('/apex/WCT_Imm_CaseSubmission_Date_FormThankYou?em='+CryptoHelper.encrypt(LoggedInContact.Email)+'&taskid='+taskid);  
    }
}