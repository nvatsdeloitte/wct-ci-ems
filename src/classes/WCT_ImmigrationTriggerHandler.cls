// Description : This class is called by Immigration Trigger
//
//
public class WCT_ImmigrationTriggerHandler{

//------------------------------------------------------------------------------------------
// Description : This method populates visa type to L2 when immigration record type is 'EAD'
//-------------------------------------------------------------------------------------------
public static void prePopulateVisaType(List<WCT_Immigration__c> lstImmigration){

    set<string> setRecordTypeIds = new set<string>();

    for(WCT_Immigration__c tmpImm : lstImmigration){
        setRecordTypeIds.add(tmpImm.RecordTypeId); 
    }

    List<RecordType> lstRecType = [SELECT Id,Name FROM RecordType WHERE id in : setRecordTypeIds];
    set<string> setRecordTypeName = new set<string>();
    Map<string,id> mapNameTypeId = new Map<string,id>();
    for(RecordType tmpRecTyp : lstRecType ){
        setRecordTypeName.add(tmpRecTyp.name);
        mapNameTypeId.put(tmpRecTyp.name,tmpRecTyp.id);
    }
    for(WCT_Immigration__c tmpImm : lstImmigration){
        if(tmpImm.RecordTypeId==mapNameTypeId.get('L2 with EAD')){
            tmpImm.WCT_Visa_Type__c = 'L2';
        }
        else if(tmpImm.RecordTypeId==mapNameTypeId.get('B1 Visa')){
            tmpImm.WCT_Visa_Type__c = 'B1 Individual';
        }
    }
  }
  
  
 public static void updateIsStatusChanged(Map<Id,WCT_Immigration__c> mapOldImmigration, List<WCT_Immigration__c > lstNewImmigration){
     List<WCT_Immigration__c> lstImmigration = [select id,WCT_Immigration_Status__c,WCT_Is_Immigration_Status_Changed__c,(select id, subject, status from Tasks where status<>'Completed')
                                                  from WCT_Immigration__c where id in : mapOldImmigration.keySet()];
     Map<Id,WCT_Immigration__c> mapIdImm = new Map<Id,WCT_Immigration__c> (lstImmigration );
     for(WCT_Immigration__c tmpImm : lstNewImmigration){
         if(mapIdImm.get(tmpImm.id)!=null 
         && tmpImm.WCT_Immigration_Status__c!=mapIdImm.get(tmpImm.id).WCT_Immigration_Status__c 
         && mapIdImm.get(tmpImm.id).Tasks !=null && mapIdImm.get(tmpImm.id).Tasks.size()>0 ){
             tmpImm.WCT_Is_Immigration_Status_Changed__c=true;
         }    
     }
     
 }  


 public static void populateISTDateTime(List<WCT_Immigration__c> lstImmigration){

    for(WCT_Immigration__c tmpImm : lstImmigration){
        if(tmpImm.WCT_Visa_Interview_Date__c != null){
            tmpImm.Visa_Interview_Date_Time_IST__c = tmpImm.WCT_Visa_Interview_Date__c.format('MM/dd/yyyy h:mm a', System.Label.WCT_IST_Time_Zone);
        }
        if(tmpImm.WCT_OFC_Appointment_Date__c != null){
            tmpImm.WCT_OFC_Appointment_Date_Time_IST__c= tmpImm.WCT_OFC_Appointment_Date__c.format('MM/dd/yyyy h:mm a', System.Label.WCT_IST_Time_Zone);
        }
    }
 }
 
 public static void dependentFormSubmitted(List<WCT_Immigration__c> lstImmigration){
     for(WCT_Immigration__c tmpImm : lstImmigration){
         if(tmpImm.WCT_Dependent_Form_Submitted__c == true){
             List<WCT_Mobility__c> mobRec = new List<WCT_Mobility__c>();
             mobRec = [SELECT Id, Name, WCT_Mobility_Employee__c, WCT_Mobility_Status__c, RecordTypeId, ownerId FROM WCT_Mobility__c WHERE WCT_Mobility_Type__c = 'Employment Visa' AND Immigration__c =: tmpImm.Id LIMIT 1];
             List<Task> lstTask = new List<Task>();
             if(!mobRec.isEmpty()){
                 lstTask = [SELECT Id, WhatId, Status, Subject FROM Task WHERE WhatId =: mobRec[0].Id and Subject = 'Apply for dependent Visa'];
                 for(Task t : lstTask){
                     t.Status = 'Completed';
                 }
                 if(!lstTask.isEmpty())
                     update lstTask;
             }
             tmpImm.WCT_Dependent_Form_Submitted__c = false;
         }
     }       
 }

}