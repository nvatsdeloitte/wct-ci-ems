@isTest
private class WCT_ImmigrationTriggerHandler_Test
{
    public static testmethod void m1()
    {
        Test.startTest();
        WCT_Immigration__c imRec = new WCT_Immigration__c();
        imRec.WCT_Immigration_Status__c = 'New';
        INSERT imRec;
        
        Task tempTask = new Task();
        tempTask.WhatId = imRec.Id;
        tempTask.Status = 'Not Started';
        INSERT tempTask;
        
        imRec.WCT_Immigration_Status__c = 'Initiate Onboarding';
        UPDATE imRec;
        Test.stopTest();
    }
    
}