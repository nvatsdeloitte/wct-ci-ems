@isTest
public class WCT_ImmigrationUpdateSchedBatch_Test 
{

static testMethod void TestWCT_ImmigrationUpdateSchedulableBatch()

  {
  Test.startTest();
         
    Set<Id> stagingIds = new Set<id>();
   
   WCT_Immigration__c lstImmigration = new WCT_Immigration__c();
   
   List<RecordType> lstRecType = [SELECT Id,Name FROM RecordType WHERE name='L1 Visa'];
   lstImmigration.RecordTypeId = lstRecType.get(0).id;
   lstImmigration.UID_Number__c = '12345678';
    insert lstImmigration;         
       
      WCT_ImmigrationUpdateSchedulableBatch createCon = new WCT_ImmigrationUpdateSchedulableBatch();
   
      system.schedule('New','0 0 2 1 * ?',createCon); 
    
          Test.stopTest();   
        

     }
     
 static testMethod void Test_WCT_ImmigrationUpdateSchedulableBatch()
 
  {
  Test.startTest();
  
        WCT_Mobility__c mobRec = new WCT_Mobility__c();
        mobRec.WCT_Total_Family_Size_at_Home__c = 2;
        mobRec.WCT_Mobility_Status__c = 'New';
        insert mobRec ;
        
        Contact ct = WCT_UtilTestDataCreation.createEmployee(WCT_Util.getRecordTypeIdByLabel('Contact','Employee'));
         insert ct;
    Set<Id> stagingIds = new Set<id>();
   
        WCT_Immigration__c immi = new WCT_Immigration__c();
       // immi.WCT_Assignment_Owner__c=im;
       // immi.ownerId=UserInfo.getUserId();
        immi.WCT_Visa_Type__c = 'L1A Individual';
        immi.UID_Number__c = '12345678';
        
        immi.WCT_Request_Initiation_Date__c = system.today().addDays(-160);
        immi.WCT_Immigration_Status__c = 'Petition In Progress';
        immi.WCT_Petition_Start_Date__c = Date.today();
                
        insert  immi;
        
        immi.WCT_Immigration_Status__c = 'Petition On Hold';
        
        update immi;
        
        immi.WCT_Petition_End_Date__c = Date.today();
        update immi;
        immi.WCT_Request_for_Evidence_Due_Date__c = system.today();
        update immi;
        immi.WCT_RFE_Date__c = system.today().addDays(-45);
        immi.WCT_Immigration_Status__c = 'Request For Evidence';
        
        Update immi;

       
      WCT_ImmigrationUpdateSchedulableBatch createCon = new WCT_ImmigrationUpdateSchedulableBatch();
      Task task = new Task();
      system.schedule('New','0 0 2 1 * ?',createCon); 
    
          Test.stopTest();   
        

     }
     
      
  }