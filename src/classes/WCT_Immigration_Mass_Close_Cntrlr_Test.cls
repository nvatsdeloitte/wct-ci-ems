@isTest

Private class WCT_Immigration_Mass_Close_Cntrlr_Test
{

    public static testmethod void test1()
    {
           
        WCT_Immigration__c immigRec1  = new WCT_Immigration__c();
        immigRec1.WCT_Immigration_Status__c = 'New';
        insert immigRec1;
        
        WCT_Task_Reference_Table__c refRec = WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
        refRec.WCT_Assigned_to__c = 'Fragomen';
        insert refRec;
        
        Task t1= new Task();
        t1.Subject='Send Visa questionnaire to employee';
        t1.Status='Not Started';
        t1.OwnerId = Label.Fragomen_UserID;
        t1.WCT_Task_Reference_Table_ID__c= refRec.id;
        t1.WhatId = immigRec1.Id;
        t1.Task_Type__c = 'New';
        insert t1;
        
        Test.startTest();
        List<Task> tasklist1 = new List<Task>();
        tasklist1.add(t1);
        
        WCT_Immigration_Mass_Close_Controller.taskListWrapper tasklistWrap1 = new WCT_Immigration_Mass_Close_Controller.taskListWrapper(tasklist1);
        
        
        List<WCT_Immigration_Mass_Close_Controller.taskListWrapper> wraplist1 = new List<WCT_Immigration_Mass_Close_Controller.taskListWrapper>();
        wraplist1.add(tasklistWrap1);
        
        WCT_Immigration_Mass_Close_Controller.taskWrapper taskWrap1 = new WCT_Immigration_Mass_Close_Controller.taskWrapper(true,'test',wraplist1);       
        
        List<WCT_Immigration_Mass_Close_Controller.taskWrapper> taskwraplist1 = new List<WCT_Immigration_Mass_Close_Controller.taskWrapper>();
        taskwraplist1.add(taskWrap1);
       
        WCT_Immigration_Mass_Close_Controller.immigrationWrapper immigWrap1 = new WCT_Immigration_Mass_Close_Controller.immigrationWrapper(immigRec1,taskwraplist1);
        
        WCT_Immigration_Mass_Close_Controller controller1 = new WCT_Immigration_Mass_Close_Controller();
        controller1.selectedStatus = 'New';
        controller1.selectedViewBy = 'Fragomen';
        controller1.RecordTypeValue = 'L1 Visa';
        controller1.VisaTypeValue = 'L1A Blanket';
        controller1.getViewBy();
        controller1.getStatus();
        controller1.setRecordType();
        controller1.setVisaType();
        controller1.getListTasks();
        controller1.checkAll();
        controller1.massClose();
        
        Test.stopTest();
    
    }   
    
    public static testmethod void test2()
    {
           
        WCT_Immigration__c immigRec2  = new WCT_Immigration__c();
        immigRec2.WCT_Immigration_Status__c = 'New';
        insert immigRec2;
        
        Contact con = new Contact();
        con.LastName = 'test';
        INSERT con;
        
        WCT_Task_Reference_Table__c refRec2 = WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
        refRec2.WCT_Assigned_to__c = 'Employee';
        insert refRec2;

        Task t2= new Task();
        t2.Subject='Send Visa questionnaire to employee';
        t2.Status='Not Started';
        t2.WCT_Task_Reference_Table_ID__c= refRec2.id;
        t2.WhatId = immigRec2.Id;
        t2.Task_Type__c = 'New';
        t2.whoId = con.Id;
        insert t2;
        
        Test.startTest();
        List<Task> tasklist2 = new List<Task>();
        tasklist2.add(t2);
        
        WCT_Immigration_Mass_Close_Controller.taskListWrapper tasklistWrap2 = new WCT_Immigration_Mass_Close_Controller.taskListWrapper(tasklist2);
        
        
        List<WCT_Immigration_Mass_Close_Controller.taskListWrapper> wraplist2 = new List<WCT_Immigration_Mass_Close_Controller.taskListWrapper>();
        wraplist2.add(tasklistWrap2);
        
        WCT_Immigration_Mass_Close_Controller.taskWrapper taskWrap2 = new WCT_Immigration_Mass_Close_Controller.taskWrapper(true,'test',wraplist2);
        
        List<WCT_Immigration_Mass_Close_Controller.taskWrapper> taskwraplist2 = new List<WCT_Immigration_Mass_Close_Controller.taskWrapper>();
        taskwraplist2.add(taskWrap2);
       
        WCT_Immigration_Mass_Close_Controller.immigrationWrapper immigWrap2 = new WCT_Immigration_Mass_Close_Controller.immigrationWrapper(immigRec2,taskwraplist2);
        
        WCT_Immigration_Mass_Close_Controller controller2 = new WCT_Immigration_Mass_Close_Controller();
        controller2.selectedStatus = 'New';
        controller2.selectedViewBy = 'Employee';
        controller2.RecordTypeValue = 'H1 Visa';
        controller2.VisaTypeValue = 'H1B Cap';
        controller2.getViewBy();
        controller2.getStatus();
        controller2.setRecordType();
        controller2.setVisaType();
        controller2.getListTasks();
        controller2.checkAll();
        controller2.massClose();
        
        Test.stopTest();
    
    }   
    
    public static testmethod void test3()
    {
           
        WCT_Immigration__c immigRec3  = new WCT_Immigration__c();
        immigRec3.WCT_Immigration_Status__c = 'New';
        insert immigRec3;
        
        WCT_Task_Reference_Table__c refRec3 = WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
        refRec3.WCT_Assigned_to__c = 'GM&I Team';
        insert refRec3;

        Task t3= new Task();
        t3.Subject='Send Visa questionnaire to employee';
        t3.Status='Not Started';
        t3.WCT_Task_Reference_Table_ID__c= refRec3.id;
        t3.WhatId = immigRec3.Id;
        t3.Task_Type__c = 'New';
        insert t3;
        
        Test.startTest();
        List<Task> tasklist3 = new List<Task>();
        tasklist3.add(t3);
        
        WCT_Immigration_Mass_Close_Controller.taskListWrapper tasklistWrap3 = new WCT_Immigration_Mass_Close_Controller.taskListWrapper(tasklist3);
        
        
        List<WCT_Immigration_Mass_Close_Controller.taskListWrapper> wraplist3 = new List<WCT_Immigration_Mass_Close_Controller.taskListWrapper>();
        wraplist3.add(tasklistWrap3);
        
        WCT_Immigration_Mass_Close_Controller.taskWrapper taskWrap3 = new WCT_Immigration_Mass_Close_Controller.taskWrapper(true,'test',wraplist3);
        
        List<WCT_Immigration_Mass_Close_Controller.taskWrapper> taskwraplist3 = new List<WCT_Immigration_Mass_Close_Controller.taskWrapper>();
        taskwraplist3.add(taskWrap3);
       
        WCT_Immigration_Mass_Close_Controller.immigrationWrapper immigWrap3 = new WCT_Immigration_Mass_Close_Controller.immigrationWrapper(immigRec3,taskwraplist3);
        
        WCT_Immigration_Mass_Close_Controller controller3 = new WCT_Immigration_Mass_Close_Controller();
        controller3.selectedStatus = 'New';
        controller3.selectedViewBy = 'GM&I Team';
        controller3.RecordTypeValue = 'B1 Visa';
        controller3.VisaTypeValue = 'B1/B2';
        controller3.getViewBy();
        controller3.getStatus();
        controller3.setRecordType();
        controller3.setVisaType();
        controller3.getListTasks();
        controller3.checkAll();
        controller3.massClose();
        
        Test.stopTest();
    
    }   
}