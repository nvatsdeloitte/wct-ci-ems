/*
Class name : WCT_Immigration_Mass_Close_Controller
Description : This class is controller for WCT_Immigration_Mass_Close page.

Task number     Date            Modified By                         Description
------------    ---------       ---------------------               ----------------------
                7-Apr-14        Darshee Mehta                       Created
                                                            
*/

public class WCT_Immigration_Mass_Close_Controller{
          
    public String selectedViewBy{get;set;}
    public String selectedStatus{get;set;}
    public String RecordTypeValue{set; get;}
    public String VisaTypeValue{set;get;}
    public Date fromRID{set;get;}
    public Date toRID{set;get;}
    public List<SelectOption> RecordTypeValueOptions{set; get;}
    public List<SelectOption> VisaTypeValueOptions{set; get;}
    public List<WCT_Immigration__c> lstImmigrationRecord{get;set;}
    public List<ImmigrationWrapper> showAllRecordList{get;set;}
    public List<TaskWrapper> showAllTaskList{get;set;}
    public List<taskListWrapper> showAllTaskListWrapper{get;set;}
    public List<Task> finalTask{get;set;}
    
                
   //-----------------------------------
   //    Constructor
   //-----------------------------------
    
    public WCT_Immigration_Mass_Close_Controller(){
        setRecordType();
        setVisaType();
        showAllRecordList = new List<ImmigrationWrapper>();
        showAllTaskList = new List<TaskWrapper>();
        showAllTaskListWrapper=new List<taskListWrapper>();
    }

   //-----------------------------------
   //    Wrapper Class
   //-----------------------------------    
        
    public class immigrationWrapper{
        public WCT_Immigration__c imRecord{get;set;}
        public List<TaskWrapper> lstOfTask{get;set;}
        public Boolean isThere{get;set;}
        
        public immigrationWrapper(WCT_Immigration__c wctImmigration, List<TaskWrapper> tskWrapper){
          
           imRecord = wctImmigration;
           lstOfTask = tskWrapper;
           if(lstOfTask.isEmpty()){
               isThere = false;
           }else isThere = true;
                      
        }
    }
    
    public class taskWrapper{
        public Boolean check{get;set;}
        public String Task_Type{get;set;}
        public List<taskListWrapper> taskRec{get;set;}
        
        public taskWrapper(Boolean flag, String taskType, List<taskListWrapper> taskListWrapper){
            check = flag;
            Task_Type = taskType;
            taskRec = taskListWrapper;
        }
    }
    
    public class taskListWrapper{
        public List<Task> taskFinalList{get;set;}
        
        public taskListWrapper(List<Task> taskFinalListW){
                taskFinalList = taskFinalListW;
        }
    }

   //-----------------------------------------------------
   //    Get User By Dropdown List Value of 'Assigned To'
   //----------------------------------------------------- 
       
    public List<SelectOption> getViewBy(){
        
        //new list for holding all of the picklist options   
        List<SelectOption> options = new List<SelectOption>(); 
        String first_picklist_option = 'All';
        
        if (first_picklist_option != null ){                                                 //if there is a first value being provided
            options.add(new selectOption(first_picklist_option, first_picklist_option));     //add the first option
        }
            
            //grab the sobject that was passed
        Schema.sObjectType sobject_type = WCT_Task_Reference_Table__c.getSObjectType();
            
            //describe the sobject
        Schema.DescribeSObjectResult sobject_describe = WCT_Task_Reference_Table__c.getSObjectType().getDescribe();
            
            //get a map of fields for the passed sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
            
            //grab the list of picklist values for the passed field on the sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get('WCT_Assigned_to__c').getDescribe().getPickListValues();
            
            //for all values in the picklist list add the value and label to our final list
        for (Schema.PicklistEntry a : pick_list_values) { 
            options.add(new SelectOption(a.getLabel(), a.getValue()));  
        }
               
        return options;
    }

   //--------------------------------------------------------------------------
   //    Get Immigration Status By Dropdown List Value of 'Immigration Status'
   //--------------------------------------------------------------------------
                    
    public List<SelectOption> getStatus(){
        
        //new list for holding all of the picklist options   
        List<SelectOption> options = new List<SelectOption>(); 
        String first_picklist_option = '-No Status Selected-';
        
        if (first_picklist_option != null ){                                                //if there is a first value being provided
            options.add(new selectOption(first_picklist_option, first_picklist_option));     //add the first option
        }
        
            //grab the sobject that was passed
        Schema.sObjectType sobject_type = WCT_Immigration__c.getSObjectType();
         
            //describe the sobject
        Schema.DescribeSObjectResult sobject_describe = WCT_Immigration__c.getSObjectType().getDescribe();
         
            //get a map of fields for the passed sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
         
            //grab the list of picklist values for the passed field on the sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get('WCT_Immigration_Status__c').getDescribe().getPickListValues();
          
            //for all values in the picklist list add the value and label to our final list
        for (Schema.PicklistEntry a : pick_list_values) { 
            options.add(new SelectOption(a.getLabel(), a.getValue()));  
        }
 
        return options;
    }
    
   //-------------------------------------------------------------------------
   //    Get Immigration Record Type By Dropdown List Value of 'Record Type'
   //-------------------------------------------------------------------------  
                    
    public void setRecordType(){
        RecordTypeValueOptions = new List<SelectOption>();
        RecordTypeValueOptions.add(new SelectOption('All','All'));
        RecordTypeValueOptions.add(new SelectOption('L1 Visa','L1 Visa'));
        RecordTypeValueOptions.add(new SelectOption('H1 Visa','H1 Visa'));
        RecordTypeValueOptions.add(new SelectOption('B1 Visa','B1 Visa'));
        RecordTypeValue = 'All';
    }
    
   //-------------------------------------------------------------------------
   //    Get Immigration Visa Type By Dropdown List Value of 'Record Type'
   //-------------------------------------------------------------------------  
                    
    public void setVisaType(){
        VisaTypeValueOptions = new List<SelectOption>();
        if(RecordTypeValue == 'L1 Visa')
        {
            VisaTypeValueOptions.add(new SelectOption('All','All'));
            VisaTypeValueOptions.add(new SelectOption('L1A Blanket','L1A Blanket'));
            VisaTypeValueOptions.add(new SelectOption('L1A Extension','L1A Extension'));
            VisaTypeValueOptions.add(new SelectOption('L1A Individual','L1A Individual'));
            VisaTypeValueOptions.add(new SelectOption('L1B Blanket','L1B Blanket'));
            VisaTypeValueOptions.add(new SelectOption('L1B Extension','L1B Extension'));
            VisaTypeValueOptions.add(new SelectOption('L1B Individual','L1B Individual'));
            VisaTypeValue = 'All';
        }
        else if(RecordTypeValue == 'H1 Visa')
        {
            VisaTypeValueOptions.add(new SelectOption('All','All'));
            VisaTypeValueOptions.add(new SelectOption('H1B CAP','H1B CAP'));
            VisaTypeValueOptions.add(new SelectOption('H1B Extension','H1B Extension'));
            VisaTypeValueOptions.add(new SelectOption('H1B Transfer','H1B Transfer'));
            VisaTypeValue = 'All';
        }
        else if(RecordTypeValue == 'B1 Visa')
        {
            VisaTypeValueOptions.add(new SelectOption('B1/B2','B1/B2'));
            VisaTypeValue = 'B1/B2';
        }
    }
    
   //----------------------------------------------------------------------------------------------------
   //    Get a list of Immigration Records and corresponding Tasks in the Table on the click of Search
   //----------------------------------------------------------------------------------------------------
   
    public PageReference getListTasks() {
        showAllRecordList = new List<ImmigrationWrapper>();

        Map<WCT_Immigration__c,Map<String,List<Task>>> immigrationMainMap = new Map<WCT_Immigration__c,Map<String,List<Task>>>();
        Map<String,List<Task>> taskTypeMap = new Map<String,List<Task>>();
        
        List<String> statusValuesList = new List<String>();
        statusValuesList.add('Completed');
        statusValuesList.add('Cancelled');

        String queryWhereClause = '';
        String RecordTypeQuery = '';
        String VisaTypeQuery = '';
        String fromReqInitDateQuery = '';
        String toReqInitDateQuery = '';
        String conRecType = 'Employee';
        Id FragomenId;
        FragomenId = Label.Fragomen_UserID;
        
        if(selectedViewBy=='Employee'){
                queryWhereClause='AND whoId != null';     
        }
        else if(selectedViewBy=='GM&I Team' ){
                queryWhereClause='AND whoId = null AND ownerId !=: FragomenId';
        }
        else if(selectedViewBy=='Fragomen'){
                queryWhereClause='AND whoId = null AND ownerId =: FragomenId';
        }
        
        if(RecordTypeValue != 'All')
            RecordTypeQuery = 'AND RecordType.Name =:RecordTypeValue ';
        else
            RecordTypeQuery = '';
        
        if(VisaTypeValue != 'All' && RecordTypeValue != 'All')
            VisaTypeQuery = 'AND WCT_Visa_Type__c =:VisaTypeValue ';
        else
            VisaTypeQuery = '';
        
        if(fromRID != null)
            fromReqInitDateQuery = 'AND WCT_Request_Initiation_Date__c >=: fromRID ';
        else
            fromReqInitDateQuery = '';
        
        if(toRID != null)
            toReqInitDateQuery = 'AND WCT_Request_Initiation_Date__c <=: toRID';
        else
            toReqInitDateQuery = '';

        String query = 'SELECT Id, Name, WCT_Immigration_Status__c, RecordType.Name, WCT_Visa_Type__c, OwnerId, WCT_Assignment_Owner__c, WCT_Milestone__c, WCT_Request_Initiation_Date__c, (SELECT Id, Status, Task_Type__c, Subject, WhoId, OwnerId, WCT_Is_Visible_in_TOD__c, WCT_Is_Closed_By_Override__c FROM Tasks WHERE status!=:statusValuesList ' + queryWhereClause + ' order by createdDate) FROM WCT_Immigration__c WHERE WCT_Immigration_Status__c =:selectedStatus AND WCT_Immigration_Status__c != null AND WCT_Assignment_Owner__c != null AND WCT_Assignment_Owner__r.RecordType.Name =:conRecType ' + RecordTypeQuery + VisaTypeQuery + fromReqInitDateQuery + toReqInitDateQuery; 
        
        lstImmigrationRecord = database.query(query);
        
        for(WCT_Immigration__c immigrationRec: lstImmigrationRecord){
        
            taskTypeMap = new Map<String,List<Task>>();
            List<Task> listTempList = new List<Task>();
            if(!(immigrationRec.tasks).IsEmpty()){
                for(Task taskRec: immigrationRec.tasks){
                    if(taskTypeMap.ContainsKey(taskRec.Task_Type__c)){
                                
                        listTempList = new List<Task>();
                        listTempList = taskTypeMap.get(taskRec.Task_Type__c);
                        listTempList.add(taskRec);
                        taskTypeMap.put(taskRec.Task_Type__c, listTempList);
                    }
                    else if(!taskTypeMap.ContainsKey(taskRec.Task_Type__c)){

                        listTempList=new List<Task>();
                        listTempList.add(taskRec);
                        taskTypeMap.put(taskRec.Task_Type__c, listTempList);
                    }                
                }
   
            }
            immigrationMainMap.put(immigrationRec, taskTypeMap);
   
        }
        
        for(WCT_Immigration__c immigrationRec : immigrationMainMap.KeySet() ){
        
            if(immigrationMainMap.get(immigrationRec)!=null){

                showAllTaskList = new List<TaskWrapper>();
                for(String str: immigrationMainMap.get(immigrationRec).KeySet()){
                
                    showAllTaskListWrapper = new List<taskListWrapper>();
                    showAllTaskListWrapper.add(new taskListWrapper(immigrationMainMap.get(immigrationRec).get(str)));
                                
                    showAllTaskList.add(new TaskWrapper(false, str, showAllTaskListWrapper));            
                }
                               
               // if(!showAllTaskList.IsEmpty()){
                    showAllRecordList.add(new ImmigrationWrapper(immigrationRec, showAllTaskList));
                //}
                
            }
        }

        return null;
    }

   //---------------------------------------------
   //    Select All Functionality
   //---------------------------------------------
   public PageReference checkAll(){
   
       /*for(ImmigrationWrapper i: showAllRecordList){
            for(TaskWrapper recTask: i.lstOfTask){
                recTask.check = !recTask.check;
            }
            
       }*/
       return null;
   }
   
   
   
   //---------------------------------------------
   //    Field Update on the click of Override
   //--------------------------------------------- 
   
                  
   public PageReference massClose(){   
        
        finalTask = new List<Task>();  

        for(ImmigrationWrapper i: showAllRecordList){
            for(TaskWrapper recTask: i.lstOfTask){
                if(recTask.check == true){
                    for(taskListWrapper LWrap: recTask.taskRec){
                        for(Task t: LWrap.taskFinalList){
            
                            t.Status = 'Completed';
                            t.WCT_Is_Closed_By_Override__c = true;
                            t.WCT_Is_Visible_in_TOD__c = false;
                            finalTask.add(t);
                        }
                    }
                }
            }         
        }
        
        Database.update(finalTask);
        

        Database.update(lstImmigrationRecord);
        getListTasks();
        return null;
     }
}