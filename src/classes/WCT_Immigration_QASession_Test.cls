@isTest
private class WCT_Immigration_QASession_Test {
    
    static Contact con;
    static WCT_Immigration__c imRec1;
    public static Boolean allBool ;
    public static testMethod void UnitTest(){
            
            PageReference pageRef = Page.WCT_Immigration_QASession;
            Test.setCurrentPage(pageRef); 
            
            WCT_Immigration_QASession controller=new WCT_Immigration_QASession();
            test.startTest();
            recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
            con=WCT_UtilTestDataCreation.createEmployee(rt.id);
            insert con;
            
             imRec1 = new WCT_Immigration__c();
            imRec1.WCT_Immigration_Status__c = 'Visa Appointment Scheduled';
            
            imRec1.WCT_Assignment_Owner__c = con.Id;
            imRec1.WCT_Visa_Interview_Date__c = DateTime.now().AddDays(3); 
            imRec1.WCT_Visa_Interview_Location__c = 'Chennai';
            imRec1.WCT_OFC_Appointment_Date__c =DateTime.now().AddDays(3);
            imRec1.WCT_Visa_Type__c = 'B1/B2';
            imRec1.WCT_OFC_Location__c = 'Chennai';
            imRec1.WCT_Q_A_Session_attend__c=true;
            imRec1.WCT_Visa_Brief_Session__c=false;
            system.debug('imRec1'+imRec1);
            insert imRec1;
            
             
            WCT_Task_Reference_Table__c refRec2 = WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
            refRec2.WCT_Assigned_to__c = 'Employee';
            insert refRec2;
            
            
            Task t1= new Task();
            t1.Subject='Attend Q&A session';
            t1.Status='Not Started';
            t1.WCT_Task_Reference_Table_ID__c= refRec2.id;
            t1.WhatId = imRec1.Id;
            t1.Task_Type__c = 'New';
            t1.whoId = con.Id;
            t1.WCT_Is_Visible_in_TOD__c=true;
            t1.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Immigration').getRecordTypeId();
            insert t1;
            
            
            Task t2= new Task();
            t2.Subject='Attend Visa briefing session';
            t2.Status='Completed';
            t2.WCT_Task_Reference_Table_ID__c= refRec2.id;
            t2.WhatId = imRec1.Id;
            t2.Task_Type__c = 'New';
            t2.whoId = con.Id;
            t2.WCT_Is_Visible_in_TOD__c=false;
            t2.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Immigration').getRecordTypeId();
            insert t2;
            
            controller.getContacts();
            // WCT_Immigration_QASession.cContact cmp = new WCT_Immigration_QASession.cContact(imRec1);
            list<WCT_Immigration_QASession.cContact> cmp =controller.getContacts();
            
            for(WCT_Immigration_QASession.cContact c1 : cmp)
            {
                c1.selected = true;
                
            }
            //system.debug('cmp'+cmp);
            //cmp.selected = true;
            //system.debug('cmp1'+cmp);
            controller.processSelected();
            controller.geteventRegObj();
            
            recordtype rt1=[select id from recordtype where DeveloperName = 'Event_GM_I'];
            Event__c gmiEvent = new Event__c();
            gmiEvent.Name = 'Immigration Event';
            gmiEvent.Start_Date_Time__c = DateTime.now();
            gmiEvent.End_Date_Time__c = DateTime.now();
            gmiEvent.RecordTypeId = rt1.id;
            gmiEvent.Is_Event_Information_Changed__c = false;
            gmiEvent.RecordTypeId=Schema.SObjectType.Event__c.getRecordTypeInfosByName().get('GM&I').getRecordTypeId();
            insert gmiEvent;
            
            recordtype rt2=[select id from recordtype where DeveloperName = 'Event_Regristration_GM_I'];
            Event_Registration_Attendance__c Eventreg = new Event_Registration_Attendance__c();
            Eventreg.Event__c = gmiEvent.id;
            Eventreg.Contact__c = con.id;
            Eventreg.WCT_ImmigrationName__c = imRec1.id;
            Eventreg.WCT_Is_Immigration_record__c = true;
            Eventreg.Is_Event_Information_Changed__c = false;
            Eventreg.RecordTypeId = rt2.id;
            insert Eventreg;
            
                      
            
           
                
            controller.AddeventRegistrations();
            controller.getSelectedSize();
            
                                 test.stopTest();
          //  controller.selectAll();
           
           
             
    }

    

}