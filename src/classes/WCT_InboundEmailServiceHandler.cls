/*
Class name : WCT_InboundEmailServiceHandler
Description : This class is an Email Service Handler for Immigrations, Leaves and Mobility.

Task number     Date            Modified By                         Description
------------    ---------       ---------------------               ----------------------
                28-Mar-14       Darshee Mehta                       Created
                                                            
*/

global class WCT_InboundEmailServiceHandler implements Messaging.InboundEmailHandler{
    
   //-------------------------------------
   //   Method to handle incoming emails 
   //-------------------------------------  
   
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.Inboundenvelope envelope) {
        //Variable Declaration
        system.debug('email'+email);
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        String taskRecordType=Schema.SObjectType.Task.getRecordTypeInfosByName().get('GMITask').getRecordTypeId();
        //This convention is used when sending invite
        String identifier;
        String CaseIdentifier = 'ref:';
        String objectName;
        String emailIdentifier = 'RE:';
        String emailSubject = email.subject.trim();
        String identifierString;
        String emailBody;
        List<String> emailToAddress = new List<String>();
        if(email.toAddresses <> null){
            emailToAddress = email.toAddresses ;
        }
        List<String> emailCCAddress = new List<String>();
        if(email.ccAddresses <> null){
            emailCCAddress = email.ccAddresses ;
        }
        set<String> emailToAddSet = new set<String>(); 
        if(!emailToAddress.isEmpty()){
            emailToAddSet.addAll(emailToAddress);
        }
        if(!emailCCAddress.isEmpty()){
            emailToAddSet.addAll(emailCCAddress);
        }
        boolean caseCreateFornoRec = false;
        //store this in label
        string leavesMailBox = Label.Email_service_Address_for_Leaves;
        if(emailToAddSet.contains(leavesMailBox)){
            caseCreateFornoRec = true;
        }
        List<Contact> ContactId = new List<Contact>();
        List<Case> liCaseNames = new List<Case>();
        String ownerIDSObject=null;
        list<sObject> liRecordNames = new list<sObject>();
        list<WCT_Email_Handler_Sub_To_Obj_Mapping__c > customSettingMap = new list<WCT_Email_Handler_Sub_To_Obj_Mapping__c >();
        customSettingMap = [select Id, Name, WCT_Object__c from WCT_Email_Handler_Sub_To_Obj_Mapping__c];
   
      system.debug('Email Subject : '+emailSubject );
      system.debug('Email FROM: '+email.fromAddress);
      system.debug('Email TO: '+email.toAddresses);
      system.debug('Email CC: '+email.ccAddresses);
      system.debug('Email Time: '+system.now());
        
        if(email.plainTextBody != null && email.plainTextBody !=''){
            emailBody = 'FromAddress: ' + email.fromAddress +'\n' + email.plainTextBody;
        }

   //---------------------------------------------------------
   //    Processing email subject appropriately as required
   //---------------------------------------------------------
           
     // if(emailIdentifier.equalsIgnoreCase(emailSubject)){
     
            List<String> stringSubjectList=new List<String>();
                if(emailSubject != null){
                    for(WCT_Email_Handler_Sub_To_Obj_Mapping__c w: customSettingMap){
                        identifier = w.Name;
                        objectName = w.WCT_Object__c;
                        
                        if(emailSubject.Contains(identifier) || emailSubject.Contains(CaseIdentifier)){
                            emailSubject = emailSubject.replace(']','');
                            emailSubject = emailSubject.replace('[','');
                            if(emailSubject.Contains(CaseIdentifier)){
                                stringSubjectList = emailSubject.split('-'); 
                            }
                            else{
                                stringSubjectList = emailSubject.split('ID:');            
                            }
                        
                            for(String s : stringSubjectList){
                                s=s.trim();
                                identifierString = s; 
                                if(emailSubject.Contains(CaseIdentifier)){
                                    identifierString='[ '+identifierString+' ]';
                                }
                                
                            }
                            break;                           
                         
                        }
                    }
                }
            
      //}
 system.debug('identifier string'+identifierString);
   //----------------------------------------------------------------------------
   //    Querying a list of fields from the corresponding object for comparison
   //---------------------------------------------------------------------------- 

        try{
            if(emailSubject.Contains(CaseIdentifier)){
                liCaseNames = [SELECT Id, CaseNumber, OwnerId, RecordTypeId,WCT_Case_Thread_Id__c FROM Case where WCT_Case_Thread_Id__c = :identifierString ];
            }else {
                String query = 'Select Id, Name, OwnerId, WCT_Email_rcvd_task_date__c, WCT_Email_Received__c FROM ' + objectName + ' WHERE Name = \'' + identifierString + '\'';
                liRecordNames = database.query(query);
                if(!liRecordNames.isEmpty()){
                    ownerIDSObject=(String)liRecordNames[0].get('OwnerId');
                }
            }
           
        }Catch(Exception qe){
              
            WCT_ExceptionUtility.logException('WCT_InboundEmailServiceHandler', 'Generic Email Service R3', 'There is no record for '+identifierString +'\n'+ emailSubject + '\n'+ emailBody);
        }
        
   //-----------------------------------------------------------------------------------------------------------------
   //    Updating the checkbox field on the corresponding object and creating a new task where email body is stored
   //    and creating a record in exception log in case of invalid or no Id 
   //-----------------------------------------------------------------------------------------------------------------        
        try{
            if(liCaseNames!=null && !liCaseNames.isempty()){
            for(Case ca: liCaseNames){
            if(ca.WCT_Case_Thread_Id__c.equals(identifierString)){
                EmailMessage eMsg = new EmailMessage();
                eMsg.Subject = emailSubject;
                eMsg.FromAddress = email.fromAddress;
                eMsg.TextBody = email.plainTextBody;
                eMsg.HtmlBody = email.htmlBody;
                eMsg.ParentId = ca.Id;
                eMsg.MessageDate = system.now();
                eMsg.FromName = email.fromName;
                eMsg.Status = '0';
                eMsg.Incoming = true;
                insert eMsg;
                
                List<Attachment> attachmentList = new List<Attachment>();
                    
                if(email.textAttachments != null){
                    for (Messaging.InboundEmail.TextAttachment textAttachment : email.textAttachments) {
                        Attachment attachment = new Attachment();
                        attachment.Name = textAttachment.fileName;
                        attachment.Body = Blob.valueOf(textAttachment.body);
                        attachment.ParentId = eMsg.Id;
                        attachment.OwnerId = ca.OwnerId;
                        attachmentList.add(attachment);
                    }
                }
                    
                if(email.binaryAttachments != null){
                    for (Messaging.InboundEmail.BinaryAttachment binayAttachment : email.binaryAttachments) {
                        Attachment attachment = new Attachment();
                        attachment.Name = binayAttachment.fileName;
                        attachment.Body = binayAttachment.body;
                        attachment.ParentId = eMsg.Id;
                        attachment.OwnerId = ca.OwnerId;
                        attachmentList.add(attachment);
                    }
                }
                insert attachmentList;
            }
            }
            }
            else if(!liRecordNames.isempty()){
                ContactId = [Select Id, Name, Email FROM Contact WHERE Email =: email.fromAddress LIMIT 1]; 
                liRecordNames[0].put('WCT_Email_Received__c', true);
                liRecordNames[0].put('WCT_Email_rcvd_task_date__c', system.today()); //Capturing Email recieved date in mobility Object
                update liRecordNames[0];
                task t = new task();
                t.subject = emailSubject;
                t.status = 'Completed';
                if(!ContactId.isEmpty()){
                    if(objectName!='WCT_leave__c'){
                        t.WhoId = ContactId[0].Id;
                    }
                    t.WCT_Email_From_Contact__c = ContactId[0].Name;
                }
                t.WhatId = liRecordNames[0].Id;
                t.WCT_Task_for_object__c = objectName;
                t.ActivityDate = system.today();
                t.OwnerId = userinfo.getuserId();
                t.Description = emailBody;
                t.RecordTypeID=taskRecordType;
                if(ownerIDSObject.startsWith('00G')  )
                {
                    if(objectName!='WCT_leave__c')
                        t.OwnerId = label.GMI_User;
                    else
                        t.OwnerId=label.Leaves_User;
                }
                else
                    t.ownerId=ID.valueOf(ownerIDSObject);
                                  
                /* To avoid duplicate task creation for leaves team when a email routes from ppsshermitage@deloitte.com  */
                  
                   // if(objectName!='WCT_leave__c' && email.fromAddress != Label.PPS_ORG_Name)
                     //  {
                     system.debug('objectName************'+objectName+'email.fromAddress***********'+email.fromAddress);
                    insert t;
                   // }   
               
                List<Attachment> attachmentList = new List<Attachment>();
                if(email.textAttachments != null){
                    for (Messaging.InboundEmail.TextAttachment textAttachment : email.textAttachments) {
                        Attachment attachment = new Attachment();
                        attachment.Name = textAttachment.fileName;
                        attachment.Body = Blob.valueOf(textAttachment.body);
                        attachment.ParentId = t.Id;
                        attachment.OwnerId = t.OwnerId;
                        attachmentList.add(attachment);
                    }
                }
                if(email.binaryAttachments != null){
                    for (Messaging.InboundEmail.BinaryAttachment binayAttachment : email.binaryAttachments) {
                        Attachment attachment = new Attachment();
                        attachment.Name = binayAttachment.fileName;
                        attachment.Body = binayAttachment.body;
                        attachment.ParentId = t.Id;
                        attachment.OwnerId = t.OwnerId;
                        attachmentList.add(attachment);
                    }
                }
                insert attachmentList;
                
            }
            else{
                if(caseCreateFornoRec){
                    List<Contact> contFromSys = new List<Contact>();
                    contFromSys = [Select Id, Email FROM Contact WHERE Email =: email.fromAddress LIMIT 1];
                    
                    Case ca = new Case();
                    ca.recordTypeId = WCT_Util.getRecordTypeIdByLabel('Case','Leaves');
                    ca.subject = email.subject;
                    ca.description = email.plainTextBody;
                    if(!contFromSys.isEmpty()){
                        ca.contactId = contFromSys[0].id;
                    }
                    ca.ownerId = Label.Leaves_Queue_Id;
                    ca.WCT_Category__c = 'Leaves and Disabilities - US';
                    ca.Origin = 'Email';           //Added according to Feb enhancement 
                    insert ca;
                    
                    //Creation of email and attachment
                    EmailMessage eMsg = new EmailMessage();
                    eMsg.Subject = emailSubject;
                    eMsg.FromAddress = email.fromAddress;
                    eMsg.TextBody = email.plainTextBody;
                    if(email.htmlBody<> null ){
                        if(email.htmlBody.length()>31950){
                            eMsg.HtmlBody = email.htmlBody.substring(0,31900);
                        }else{
                            eMsg.HtmlBody = email.htmlBody;
                        }
                    }
                    eMsg.ParentId = ca.Id;
                    eMsg.MessageDate = system.now();
                    eMsg.FromName = email.fromName;
                    eMsg.Status = '0';
                    eMsg.Incoming = true;
                    insert eMsg;
                    
                    List<Attachment> attachmentList = new List<Attachment>();
                        
                    if(email.textAttachments != null){
                        for (Messaging.InboundEmail.TextAttachment textAttachment : email.textAttachments) {
                            Attachment attachment = new Attachment();
                            attachment.Name = textAttachment.fileName;
                            attachment.Body = Blob.valueOf(textAttachment.body);
                            attachment.ParentId = eMsg.Id;
                            //attachment.OwnerId = ca.OwnerId;
                            attachmentList.add(attachment);
                        }
                    }
                        
                    if(email.binaryAttachments != null){
                        for (Messaging.InboundEmail.BinaryAttachment binayAttachment : email.binaryAttachments) {
                            Attachment attachment = new Attachment();
                            attachment.Name = binayAttachment.fileName;
                            attachment.Body = binayAttachment.body;
                            attachment.ParentId = eMsg.Id;
                            //attachment.OwnerId = ca.OwnerId;
                            attachmentList.add(attachment);
                        }
                    }
                    insert attachmentList;
                }
                if(identifierString <> null){
                    WCT_ExceptionUtility.logException('WCT_InboundEmailServiceHandler', 'Generic Email Service R3', 'Else Loop There is no record for '+identifierString +'\n'+ emailSubject + '\n'+ emailBody);
                }
            }

        }Catch(DmlException e){  
            
        }

        return result;
          
    }
    
}