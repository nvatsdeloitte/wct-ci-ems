@isTest
private class  WCT_InboundEventEmailHandler_TEST{
    /*******Varibale Declaration********/
    public static Id profileIdRecCoo=[Select id from Profile where Name=:WCT_UtilConstants.Recruiting_Coordinator].Id;
    public static Id profileIdIntv=[Select id from Profile where Name=:WCT_UtilConstants.INTERVIWER_PROFILE_NAME].Id;
    public static Id profileIdRecr=[Select id from Profile where Name=:WCT_UtilConstants.RECRUITER_COMPANY].Id;
    public static String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();
    public static User userRecRecCoo, userRecRecIntv, userRecRecr;
    public static Contact Candidate;
    public static WCT_Requisition__c Requisition;
    public static WCT_Candidate_Requisition__c CandidateRequisition;
    public static WCT_Interview__c Interview;
    public static WCT_Interview_Junction__c InterviewTracker;
    public static Event InterviewEvent;
    public static EventRelation InterviewEventRelation;
    public static WCT_List_Of_Names__c ClickToolForm;
    public static boolean isBlankIEF, isBlankSampleIEF;
    public static WCT_Candidate_Documents__c CandidateDocument;
    public static Attachment attachment;
    public static Document document;
    /***************/

    /** 
        Method Name  : createUserRecCoo
        Return Type  : User
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecCoo()
    {
        userRecRecCoo=WCT_UtilTestDataCreation.createUser('RecCoo', profileIdRecCoo, 'arunsharmaRecCoo@deloitte.com', 'arunsharma4@deloitte.com');
        insert userRecRecCoo;
        return  userRecRecCoo;
    }
    /** 
        Method Name  : createUserRecr
        Return Type  : User
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecr()
    {
        userRecRecr=WCT_UtilTestDataCreation.createUser('Recr', profileIdRecr, 'arunsharmaRecr@deloitte.com', 'arunsharma4@deloitte.com');
        insert userRecRecr;
        return  userRecRecr;
    }
    /** 
        Method Name  : createUserIntv
        Return Type  : User
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserIntv()
    {
        userRecRecIntv=WCT_UtilTestDataCreation.createUser('Intv', profileIdIntv, 'arunsharmaIntv@deloitte.com', 'arunsharma4@deloitte.com');
        insert userRecRecIntv;
        return  userRecRecIntv;
    }
    /** 
        Method Name  : createCandidate
        Return Type  : Contact
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static Contact createCandidate()
    {
        Candidate=WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
        insert Candidate;
        return  Candidate;
    }    
    /** 
        Method Name  : createRequisition
        Return Type  : WCT_Requisition__c
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Requisition__c createRequisition()
    {
        Requisition=WCT_UtilTestDataCreation.createRequisition(userRecRecr.Id);
        insert Requisition;
        return  Requisition;
    }
    /** 
        Method Name  : createCandidateRequisition
        Return Type  : WCT_Candidate_Requisition__c
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Candidate_Requisition__c createCandidateRequisition()
    {
        CandidateRequisition=WCT_UtilTestDataCreation.createCandidateRequisition(Candidate.ID,Requisition.Id);
        insert CandidateRequisition;
        return  CandidateRequisition;
    } 
    /** 
        Method Name  : createInterview
        Return Type  : WCT_Interview__c
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview__c createInterview()
    {
        Interview=WCT_UtilTestDataCreation.createInterview(ClickToolForm.Id);
        system.runAs(userRecRecCoo)
        {
            insert Interview;
        }
        return  Interview;
    }   
    /** 
        Method Name  : createInterviewTracker
        Return Type  : WCT_Interview_Junction__c
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview_Junction__c createInterviewTracker()
    {
        InterviewTracker=WCT_UtilTestDataCreation.createInterviewTracker(Interview.Id,CandidateRequisition.Id,userRecRecIntv.Id);
        insert InterviewTracker;
        return  InterviewTracker;
    }  
    /** 
        Method Name  : createEvent
        Return Type  : Event
        Type         : private
        Description  : Create temp records for data mapping         
    */    
    private Static Event createEvent()
    {
        InterviewEvent=WCT_UtilTestDataCreation.createEvent(Interview.Id);
        system.runAs(userRecRecCoo)
        {
            insert InterviewEvent;
        }
        return  InterviewEvent;
    }     
    /** 
        Method Name  : createEventRelation
        Return Type  : EventRelation
        Type         : private
        Description  : Create temp records for data mapping         
    */    
    private Static EventRelation createEventRelation()
    {
        InterviewEventRelation=WCT_UtilTestDataCreation.createEventRelation(InterviewEvent.Id,userRecRecIntv.Id);
        insert InterviewEventRelation;
        return  InterviewEventRelation;
    }   
    /** 
        Method Name  : cretaEmailBody
        Return Type  : string
        Type         : private
        Description  : Create temp Email body String         
    */   
    private static string cretaEmailBody()
    {
        return '<html><body><p>Email Body Top</p><p><ol><li>Email Body Center</li></ol></p><p>Email Body Bottom</p></body></html>';
    }  
    /** 
        Method Name  : createCandidateDocument
        Return Type  : WCT_Candidate_Documents__c
        Type         : private
        Description  : Create temp records for data mapping         
    */    
    private Static WCT_Candidate_Documents__c createCandidateDocument()
    {
        CandidateDocument=WCT_UtilTestDataCreation.createCandidateDocument(Candidate.Id);
        insert CandidateDocument;
        return  CandidateDocument;
    }
    /** 
        Method Name  : createAttachment
        Return Type  : Attachment
        Type         : private
        Description  : Create temp records for data mapping         
    */    
    private Static Attachment createAttachment()
    {
        attachment=WCT_UtilTestDataCreation.createAttachment(CandidateDocument.Id);
        insert attachment;
        return  attachment;
    } 
    /** 
        Method Name  : createDocument
        Return Type  : Document
        Type         : private
        Description  : Create temp records for data mapping         
    */    
    private Static Document createDocument()
    {
        document=WCT_UtilTestDataCreation.createDocument();
        insert document;
        return  document;
    }     
    /** 
        Method Name  : createClickToolForm
        Return Type  : WCT_List_Of_Names__c
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_List_Of_Names__c createClickToolForm()
    {
        ClickToolForm=WCT_UtilTestDataCreation.createClickToolForm(document.id);
        insert ClickToolForm; 
        return  ClickToolForm;
    }    
    
    static testMethod void myUnitTest() {
        userRecRecCoo=createUserRecCoo();
        userRecRecIntv=createUserIntv();
        userRecRecr=createUserRecr();
        Candidate=createCandidate();
        Requisition=createRequisition();
        CandidateRequisition=createCandidateRequisition();
        document=createDocument();
        ClickToolForm=createClickToolForm();
        Interview=createInterview();
        InterviewEvent=createEvent();
        InterviewEventRelation=createEventRelation();
        InterviewTracker=createInterviewTracker();
        CandidateDocument=createCandidateDocument();
        Interview.WCT_Collaborator__c = userInfo.getUserId();
        Interview.WCT_Collaborator_2__c = userInfo.getUserId();
        Interview.WCT_Collaborator_3__c = userInfo.getUserId();
        update Interview;
        WCT_Interview__c intv = [SELECT id,name from WCT_Interview__c  where id = :interview.id];
        
        test.startTest();
        WCT_InboundEventEmailHandler emailHandler = new WCT_InboundEventEmailHandler();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope(); 
        email.subject = 'Accepted: Interview Id:'+intv.name ;
        email.fromAddress = userRecRecIntv.email;
        emailHandler.handleInboundEmail(email, env);
        test.stopTest();
    }
    
    static testMethod void myUnitTestDeclined() {
        userRecRecCoo=createUserRecCoo();
        userRecRecIntv=createUserIntv();
        userRecRecr=createUserRecr();
        Candidate=createCandidate();
        Requisition=createRequisition();
        CandidateRequisition=createCandidateRequisition();
        document=createDocument();
        ClickToolForm=createClickToolForm();
        Interview=createInterview();
        InterviewEvent=createEvent();
        InterviewEventRelation=createEventRelation();
        InterviewTracker=createInterviewTracker();
        CandidateDocument=createCandidateDocument();
        Interview.WCT_Collaborator__c = userInfo.getUserId();
        Interview.WCT_Collaborator_2__c = userInfo.getUserId();
        Interview.WCT_Collaborator_3__c = userInfo.getUserId();
                
        update Interview;
        WCT_Interview__c intv = [SELECT id,name from WCT_Interview__c  where id = :interview.id];
        
        test.startTest();
        WCT_InboundEventEmailHandler emailHandler = new WCT_InboundEventEmailHandler();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope(); 
        email.subject = 'Declined: Interview Id:'+intv.name ;
        email.fromAddress = userRecRecIntv.email;
        emailHandler.handleInboundEmail(email, env);
        test.stopTest();
    }
    
    static testMethod void myUnitTestTentative() {
        userRecRecCoo=createUserRecCoo();
        userRecRecIntv=createUserIntv();
        userRecRecr=createUserRecr();
        Candidate=createCandidate();
        Requisition=createRequisition();
        CandidateRequisition=createCandidateRequisition();
        document=createDocument();
        ClickToolForm=createClickToolForm();
        Interview=createInterview();
        InterviewEvent=createEvent();
        InterviewEventRelation=createEventRelation();
        InterviewTracker=createInterviewTracker();
        CandidateDocument=createCandidateDocument();
        Interview.WCT_Collaborator__c = userInfo.getUserId();
        Interview.WCT_Collaborator_2__c = userInfo.getUserId();
        Interview.WCT_Collaborator_3__c = userInfo.getUserId();
        
        update Interview;
        WCT_Interview__c intv = [SELECT id,name from WCT_Interview__c  where id = :interview.id];
        
        test.startTest();
        WCT_InboundEventEmailHandler emailHandler = new WCT_InboundEventEmailHandler();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope(); 
        email.subject = 'Tentative: Interview Id:'+intv.name ;
         email.htmlBody = 'test';
        email.fromAddress = userRecRecIntv.email;
        emailHandler.handleInboundEmail(email, env);
        test.stopTest();
    }
        static testMethod void myUnitTestReply() {
        userRecRecCoo=createUserRecCoo();
        userRecRecIntv=createUserIntv();
        userRecRecr=createUserRecr();
        Candidate=createCandidate();
        Requisition=createRequisition();
        CandidateRequisition=createCandidateRequisition();
        document=createDocument();
        ClickToolForm=createClickToolForm();
        Interview=createInterview();
        InterviewEvent=createEvent();
        InterviewEventRelation=createEventRelation();
        InterviewTracker=createInterviewTracker();
        CandidateDocument=createCandidateDocument();
        Interview.WCT_Collaborator__c = userInfo.getUserId();
        Interview.WCT_Collaborator_2__c = userInfo.getUserId();
        Interview.WCT_Collaborator_3__c = userInfo.getUserId();
        ClickToolForm.WCT_Job_Type__c='India-Experienced';
        ClickToolForm.WCT_User_Group__c='India';
        update ClickToolForm;
        Interview.WCT_Job_Type__c='India-Experienced';
        Interview.WCT_User_Group__c='India';
        update Interview;
        WCT_Interview__c intv = [SELECT id,name from WCT_Interview__c  where id = :interview.id];
        
        test.startTest();
        WCT_InboundEventEmailHandler emailHandler = new WCT_InboundEventEmailHandler();
        WCT_InboundEventEmailHandler.isreply=true;
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope(); 
        email.subject = 'RE: Interview Id:'+intv.name ;
         email.htmlBody = 'test';
        email.fromAddress = userRecRecIntv.email;
        list<string> ccadd=new list<string>();
        ccadd.add('psatpathy@deloitte.com') ;   
        email.ccaddresses=ccadd;
        emailHandler.handleInboundEmail(email, env);
        test.stopTest();
    }
}