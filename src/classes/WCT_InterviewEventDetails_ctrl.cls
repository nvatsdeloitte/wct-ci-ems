public class WCT_InterviewEventDetails_ctrl {
    public list<WCT_Interview_Junction__c > allDataLst {get;set;}
    public set<Id> intIds  {get;set;}
    public Map<Id,Event> intvIdEventMap  {get;set;}
    public List<intEventWrapper> intWrapList {get;set;}
    
    public class intEventWrapper{
        public WCT_Interview_Junction__c intJun {get;set;}
        public EventRelation evrel {get;set;}
        
        public intEventWrapper(WCT_Interview_Junction__c ij,EventRelation er){
            intJun = ij;
            evrel = er;
        }   
    }

    public WCT_InterviewEventDetails_ctrl()
    {
        allDataLst = new list<WCT_Interview_Junction__c >();
        String RC_Email= UserInfo.getUserEmail();
        intWrapList = new List<intEventWrapper>();
        intvIdEventMap = new Map<Id,Event>();
        intIds = new set<Id>();
        
        allDataLst = [SELECT Id, Name, WCT_Candidate_Name__c, WCT_Hiring_Level__c, WCT_Start_Date_Time__c, WCT_End_Date_Time__c, WCT_Interview_Type__c, WCT_Interview_Medium__c, 
                                WCT_Requisition_RC_Email__c, WCT_IEF_Submission_Status__c, WCT_Candidate_Tracker__r.WCT_Contact__r.Id, WCT_Interview__c, WCT_Interview__r.Name,
                                WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Name, WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Email,
                                WCT_Interview__r.WCT_Interview_Location_Details__c, WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiter__r.Name, 
                                    WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiter__r.Email ,WCT_IEF_URL_Base__c,WCT_Candidate_Name_Hyperlink__c,WCT_Interviewer__c,WCT_Interviewer__r.name,
                                WCT_Attachment_Link__c,WCT_Attachment_URL__c, wct_candidate_email__c,RMS_ID__c
                                FROM WCT_Interview_Junction__c 
                            where (WCT_Requisition_Recruiter_Email__c= :RC_Email 
                            or WCT_Requisition_Recruiter_Coor_Email__c = :RC_Email or WCT_Interview__r.owner.email = :RC_Email )and WCT_Start_Date_Time__c >: system.now()
                            order by WCT_Start_Date_Time__c desc
                            limit:Limits.getLimitQueryRows()];
        
        for(WCT_Interview_Junction__c JunInter : allDataLst )
        {
            intIds.add(JunInter.WCT_Interview__c);
        }
        List<EventRelation> evRelations = new List<EventRelation>();
        if(!intIds.isEmpty()){
        
            for(Event ev : [SELECT id,whatId,(SELECT id,status,relationId,eventId,event.whatId FROM EventRelations where IsInvitee = true) FROM Event 
                             WHERE WhatId IN :intIds]){
                intvIdEventMap.put(ev.whatId,ev);
                if(ev.eventRelations.size()>0){
                    evRelations.addAll(ev.eventRelations);
                }
            }   
        }
        
        for(WCT_Interview_Junction__c ij : allDataLst){
            for(eventRelation er : evRelations){
                
                if(ij.WCT_Interview__c == er.event.whatId){
                if(ij.WCT_Interviewer__c == er.relationId){
                    intWrapList.add(New intEventWrapper(ij,er));
                }
                }
                
            }
            
        }
        
    }
}