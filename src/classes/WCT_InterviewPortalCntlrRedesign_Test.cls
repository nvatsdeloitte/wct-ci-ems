@isTest
public class WCT_InterviewPortalCntlrRedesign_Test {
/*******Varibale Declaration********/
    public static Id profileIdRecCoo=[Select id from Profile where Name=:WCT_UtilConstants.Recruiting_Coordinator].Id;
    public static Id profileIdIntv=[Select id from Profile where Name=:WCT_UtilConstants.INTERVIWER_PROFILE_NAME].Id;
    public static Id profileIdRecr=[Select id from Profile where Name=:WCT_UtilConstants.RECRUITER_COMPANY].Id;
     public static String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();
    public static User userRecRecCoo, userRecRecIntv, userRecRecr;
    public static Contact Candidate;
    public static WCT_Requisition__c Requisition;
    public static WCT_Candidate_Requisition__c CandidateRequisition;
    public static WCT_Interview__c Interview;
    public static WCT_Interview__c InterviewTd,InterviewPst,InterviewUpC,InterviewAll,InterviewPend6month;
    public static WCT_Interview_Junction__c InterviewTracker;
    public static WCT_Interview_Junction__c InterviewTrackerTd,InterviewTrackerPst,InterviewTrackerUpC,InterviewTrackerAct,InterviewTrackerPend6month;
    public static list<WCT_Interview_Junction__c> InterviewTrackerAll;
    public Static WCT_List_Of_Names__c ClickToolForm;
    public static Document document;
  /***************/

    /** 
        Method Name  : createUserRecCoo
        Return Type  : User
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecCoo()
    {
      userRecRecCoo=WCT_UtilTestDataCreation.createUser('ReCoSchd', profileIdRecCoo, 'arunsharmaReCoSchd@deloitte.com', 'arunsharma4@deloitte.com');
      insert userRecRecCoo;
      return  userRecRecCoo;
    }
    /** 
        Method Name  : createUserRecr
        Return Type  : User
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecr()
    {
      userRecRecr=WCT_UtilTestDataCreation.createUser('RecrSchd', profileIdRecr, 'arunsharmaRecrSchd@deloitte.com', 'arunsharma4@deloitte.com');
      insert userRecRecr;
      return  userRecRecr;
    }
    /** 
        Method Name  : createUserIntv
        Return Type  : User
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserIntv()
    {
      userRecRecIntv=WCT_UtilTestDataCreation.createUser('IntvSchd', profileIdIntv, 'arunsharmaIntvSchd@deloitte.com', 'arunsharma4@deloitte.com');
      insert userRecRecIntv;
      return  userRecRecIntv;
    }
    /** 
        Method Name  : createCandidate
        Return Type  : Contact
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static Contact createCandidate()
    {
      Candidate=WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
      insert Candidate;
      return  Candidate;
    }    
    /** 
        Method Name  : createRequisition
        Return Type  : WCT_Requisition__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Requisition__c createRequisition()
    {
      Requisition=WCT_UtilTestDataCreation.createRequisition(userRecRecr.Id);
      insert Requisition;
      return  Requisition;
    }
    /** 
        Method Name  : createCandidateRequisition
        Return Type  : WCT_Candidate_Requisition__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Candidate_Requisition__c createCandidateRequisition()
    {
      CandidateRequisition=WCT_UtilTestDataCreation.createCandidateRequisition(Candidate.ID,Requisition.Id);
      insert CandidateRequisition;
      return  CandidateRequisition;
    } 
    /** 
        Method Name  : createDocument
        Return Type  : Document
        Type      : private
        Description  : Create temp records for data mapping         
    */    
    private Static Document createDocument()
    {
      document=WCT_UtilTestDataCreation.createDocument();
    insert document;
      return  document;
    }
    /** 
        Method Name  : createClickToolForm
        Return Type  : WCT_List_Of_Names__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_List_Of_Names__c createClickToolForm()
    {
      ClickToolForm=WCT_UtilTestDataCreation.createClickToolForm(document.id);
      insert ClickToolForm; 
      return  ClickToolForm;
    } 
    /** 
        Method Name  : createInterview
        Return Type  : WCT_Interview__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview__c createInterview()
    {
      Interview=WCT_UtilTestDataCreation.createInterview(ClickToolForm.Id);
      system.runAs(userRecRecCoo)
      {
        insert Interview;
      }
      return  Interview;
    }   
    /** 
        Method Name  : createInterviewTracker
        Return Type  : WCT_Interview_Junction__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview_Junction__c createInterviewTracker()
    {
      InterviewTracker=WCT_UtilTestDataCreation.createInterviewTracker(Interview.Id,CandidateRequisition.Id,userRecRecIntv.Id);
      insert InterviewTracker;
      return  InterviewTracker;
    }  

    /** 
        Method Name  : createInterviewTd
        Return Type  : WCT_Interview__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview__c createInterviewTd()
    {
      InterviewTd=WCT_UtilTestDataCreation.createInterview(ClickToolForm.Id);
      system.runAs(userRecRecCoo)
      {
        insert InterviewTd;
    InterviewTD.WCT_Interview_Start_Time__c = System.now();
    InterviewTD.WCT_Interview_Status__c = 'Invite Sent';
      update InterviewTD; 
      }
      return  InterviewTd;
    }   
    /** 
        Method Name  : createInterviewTrackerTd
        Return Type  : WCT_Interview_Junction__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview_Junction__c createInterviewTrackerTd()
    {
      InterviewTrackerTd=WCT_UtilTestDataCreation.createInterviewTracker(InterviewTD.Id,CandidateRequisition.Id,userRecRecIntv.Id);
      insert InterviewTrackerTd;
      return  InterviewTrackerTd;
    }  
    /** 
        Method Name  : createInterviewPst
        Return Type  : WCT_Interview__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview__c createInterviewPst()
    {
      InterviewPst=WCT_UtilTestDataCreation.createInterview(ClickToolForm.Id);
      system.runAs(userRecRecCoo)
      {
        insert InterviewPst;
    InterviewPst.WCT_Interview_Start_Time__c = System.now();
    InterviewPst.WCT_Interview_Status__c = 'Complete';
      update InterviewPst; 
      }
      return  InterviewPst;
    }   
    /** 
        Method Name  : createInterviewTrackerPst
        Return Type  : WCT_Interview_Junction__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview_Junction__c createInterviewTrackerPst()
    {
      InterviewTrackerPst=WCT_UtilTestDataCreation.createInterviewTracker(InterviewPst.Id,CandidateRequisition.Id,userRecRecIntv.Id);
      insert InterviewTrackerPst;
      return  InterviewTrackerPst;
    }  
    /** 
        Method Name  : createInterviewUpC
        Return Type  : WCT_Interview__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview__c createInterviewUpC()
    {
      InterviewUpC=WCT_UtilTestDataCreation.createInterview(ClickToolForm.Id);
      system.runAs(userRecRecCoo)
      {
        insert InterviewUpC;
    InterviewUpC.WCT_Interview_Start_Time__c = System.now();
    InterviewUpC.WCT_Interview_Status__c = 'Complete';
      update InterviewUpC; 
      }
      return  InterviewUpC;
    }   
    /** 
        Method Name  : createInterviewTrackerUpC
        Return Type  : WCT_Interview_Junction__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview_Junction__c createInterviewTrackerUpC()
    {
      InterviewTrackerUpC=WCT_UtilTestDataCreation.createInterviewTracker(InterviewUpC.Id,CandidateRequisition.Id,userRecRecIntv.Id);
      insert InterviewTrackerUpC;
        system.debug('###$$$'+InterviewTrackerUpC);
      return  InterviewTrackerUpC;
    }  

    /** 
        Method Name  : createInterviewTrackerpend6months
        Return Type  : WCT_Interview_Junction__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview_Junction__c createInterviewTrackerPend6month()
    {
      InterviewTrackerPend6month=WCT_UtilTestDataCreation.createInterviewTracker(InterviewPend6month.Id,CandidateRequisition.Id,userRecRecIntv.Id);
      insert InterviewTrackerPend6month;
        system.debug('###$$$'+InterviewTrackerPend6month);
      return  InterviewTrackerPend6month;
    }  






    static testMethod void interviewPortalRecordsTest() {
    userRecRecCoo=createUserRecCoo();
    userRecRecIntv=createUserIntv();
    userRecRecr=createUserRecr();
    Candidate=createCandidate();
    Requisition=createRequisition();
    CandidateRequisition=createCandidateRequisition();
    document=createDocument();
    ClickToolForm=createClickToolForm();
    Interview=createInterview();
    InterviewTracker=createInterviewTracker();

    InterviewTD=createInterviewTd();
    update InterviewTD;
    InterviewPst=createInterviewPst();
    InterviewPst.WCT_Interview_Start_Time__c = system.now()-1;
    update InterviewPst;
    InterviewUpC=createInterviewUpC();
    InterviewUpC.WCT_Interview_Start_Time__c = system.now()+1;
    InterviewUpC.WCT_Interview_End_Time__c = system.now()+2;
    update InterviewUpC;
    InterviewPend6month=createInterviewPst();
    InterviewPend6month.WCT_Interview_Start_Time__c = system.now().addmonths(-6)-1;
    update InterviewPend6month;
    InterviewTrackerTd=createInterviewTrackerTd();    
    InterviewTrackerPst=createInterviewTrackerPst();
    InterviewTrackerUpC=createInterviewTrackerUpC();
    InterviewTrackerAct = createInterviewTrackerPst(); 
    InterviewTrackerPend6month = createInterviewTrackerPend6month();
    InterviewTrackerPend6month.WCT_IEF_Submission_Status__c= 'Pending';
    update InterviewTrackerPend6month;
    InterviewTrackerAct.WCT_IEF_Submission_Status__c = 'Pending';
    update InterviewTrackerAct;    
    InterviewTrackerAll = new list<WCT_Interview_Junction__c>();
        list <id> lid=new list<id>();
            lid.add(InterviewTrackerTd.id);
            lid.add(InterviewTrackerPst.id);
            lid.add(InterviewTrackerUpC.id);
            lid.add(InterviewTrackerAct.id);
            lid.add(InterviewPend6month.id);
        InterviewTrackerAll=[select Id, Name, WCT_Candidate_Name__c, WCT_Hiring_Level__c, WCT_Start_Date_Time__c, WCT_End_Date_Time__c, WCT_Interview_Type__c, WCT_Interview_Medium__c, 
                        WCT_Requisition_RC_Email__c, WCT_IEF_Submission_Status__c, WCT_Candidate_Tracker__r.WCT_Contact__r.Id, WCT_Interview__c, WCT_Interview__r.Name,
                        WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Name, WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Email,
                        WCT_Interview__r.WCT_Interview_Location_Details__c, WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiter__r.Name, 
                        WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiter__r.Email ,WCT_IEF_URL_Base__c  
                        FROM WCT_Interview_Junction__c where id in:(lid)];
    /*InterviewTrackerAll.add(InterviewTrackerTd);
    InterviewTrackerAll.add(InterviewTrackerPst);
    InterviewTrackerAll.add(InterviewTrackerUpC);
    InterviewTrackerAll.add(InterviewTrackerAct);*/
    test.startTest();

    system.runAs(userRecRecIntv){
    WCT_InterviewPortalControllerRedesign InvtPortalCont = new WCT_InterviewPortalControllerRedesign();
    InvtPortalCont = new WCT_InterviewPortalControllerRedesign();
    InvtPortalCont.allDataLst=InterviewTrackerAll;
    InvtPortalCont.rtnsize(InvtPortalCont.allDataLst,'ActionItems');
    InvtPortalCont.rtnsize(InvtPortalCont.allDataLst,'UpComItems');
    InvtPortalCont.rtnsize(InvtPortalCont.allDataLst,'PendingmItems');
    InvtPortalCont.actionItemLst();
    InvtPortalCont.todayLst();    
    InvtPortalCont.upComingLst();    
    InvtPortalCont.pastLst();    
    InvtPortalCont.allLst();
    InvtPortalCont.pendingmLst();
    
    InvtPortalCont.upComLst();
    InvtPortalCont.setAddSection();
    InvtPortalCont.cancelAddSection();
    
    interPrototypeLandingPageController landCtrl = new interPrototypeLandingPageController();
    landCtrl.todayLst();
    landCtrl.upComingLst();
    landCtrl.pastLst();
    landCtrl.allLst();
    
    }
        test.stopTest();

    }
}