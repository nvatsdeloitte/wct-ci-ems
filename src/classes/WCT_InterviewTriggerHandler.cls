/**************************************************************************************
Apex Trigger Name: WCT_InterviewTriggerHandler
Version          : 1.0 
Created Date     : 18 November 2013
Function         : This class will be called from "WCT_InterviewTrigger" to implement below functionalities on Interview
                       1.Populate Interviewer fields from Related contact before insertion & updation
                       2.Populate Requisition fields on Interview, which would be used in geretating the required IEF Id before Insertion & updation
                       3.Populate Owner of the Interview with Owner of the Candidate Aquisition on Insertion of Interview
                       4.Update Candidate Aquisition Status to 'In-progress' on creation of Interview
    
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                    18/11/2013              Original Version
*************************************************************************************/
public class WCT_InterviewTriggerHandler{
    
    Public Static final String NEW_STATUS='Open';
    Public Static final String INPROGRESS_STATUS='In Progress';
    Public Static final String COMPLETE_STATUS='Complete';
    Public Static final String CANCELED_STATUS='Canceled';
    Public Static final ID employeeRecordtypeID=WCT_Util.getRecordTypeIdByLabel('Contact','Employee');       

    /*
     * Method name  : populateInterviewerDetailsFromRelatedContact
     * Description  : Compares Interviewer Email from Contacts and gets the Service line and Job level from Contact and
                      assigns it to Interviewer.
     * Return Type  : Nil
     * Parameter    : New Interviews list
     */    
    public static void populateInterviewerDetailsFromRelatedContactOnUpdate(List<WCT_Interview_Junction__c> lstInterview,map<id,WCT_Interview_Junction__c> oldInterviewJunction )
    {
        Set<String> setInterviewerEmail = new Set<String>();
        //getting the Interviewer Email from Interview
        for(WCT_Interview_Junction__c newInterviewJunction : lstInterview ){
            if( newInterviewJunction .WCT_Interviewer__c != Null && newInterviewJunction .WCT_Interviewer__c !=oldInterviewJunction.get(newInterviewJunction .id).WCT_Interviewer__c){
               setInterviewerEmail.add(newInterviewJunction .WCT_Interviewer_Email__c);
            }
        }     
        
        Map<String,Contact> mapContacts=new Map<String,Contact>();
        
        if(!setInterviewerEmail.isempty()){
        //filling the Contact map with Contact Email as key and Contact as Value
            for(Contact employeecontact:[select Email,WCT_Job_Level_Text__c,WCT_Service_Line__c from contact where email in :setInterviewerEmail and recordtypeid=:employeeRecordtypeID limit :Limits.getLimitQueryRows()])
            {        
              mapContacts.put(employeecontact.Email,employeecontact);
            }
        }
         //Assigning the Interviewer Level and Service Line with related Contact values
        if(!mapContacts.isempty()){
            for(WCT_Interview_Junction__c newInterview : lstInterview )        
            {
            if(mapContacts.get(newInterview.WCT_Interviewer_Email__c)!= Null )
             newInterview.WCT_Interviewer_Level__c=mapContacts.get(newInterview.WCT_Interviewer_Email__c).WCT_Job_Level_Text__c;
            if(mapContacts.get(newInterview.WCT_Interviewer_Email__c)!= Null )  
             newInterview.WCT_Interviewer_Service_Line__c=mapContacts.get(newInterview.WCT_Interviewer_Email__c).WCT_Service_Line__c;   
            }
        }
        
    }
    public static void populateInterviewerDetailsFromRelatedContactOnInsert(List<WCT_Interview_Junction__c> lstInterview)
    {
        Set<String> setInterviewerEmail = new Set<String>();
        
        //getting the Interviewer Email from Interview
        for(WCT_Interview_Junction__c newInterviewJunction : lstInterview ){
            if( newInterviewJunction.WCT_Interviewer__c != Null){
               setInterviewerEmail.add(newInterviewJunction .WCT_Interviewer_Email__c);
            }
        }     
       
        Map<String,Contact> mapContacts=new Map<String,Contact>();
        
        if(!setInterviewerEmail.isempty()){
        //filling the Contact map with Contact Email as key and Contact as Value
            for(Contact employeecontact:[select Email,WCT_Job_Level_Text__c,WCT_Service_Line__c from contact where email in :setInterviewerEmail and recordtypeid=:employeeRecordtypeID limit :Limits.getLimitQueryRows()])
            {        
              mapContacts.put(employeecontact.Email,employeecontact);
            }
        }
        //Assigning the Interviewer Level and Service Line with related Contact values
        if(!mapContacts.isempty()){
            for(WCT_Interview_Junction__c newInterview : lstInterview )        
            {
            if(mapContacts.get(newInterview.WCT_Interviewer_Email__c)!= Null )
             newInterview.WCT_Interviewer_Level__c=mapContacts.get(newInterview.WCT_Interviewer_Email__c).WCT_Job_Level_Text__c;
            if(mapContacts.get(newInterview.WCT_Interviewer_Email__c)!= Null )  
             newInterview.WCT_Interviewer_Service_Line__c=mapContacts.get(newInterview.WCT_Interviewer_Email__c).WCT_Service_Line__c;   
            }
        }
        
    }  
          
     /*
     * Method name  : UpdateCandidateRequisitionStatus
     * Description  : Update Candidate Aquisition Status to 'In-progress' on creation of Interview                      
     * Return Type  : Nil
     * Parameter    : New Interviews list
     */
     public static void UpdateCandidateRequisitionStatus(List<WCT_Interview_Junction__c> lstNewInterviewJunctions)
     {
        Set<ID> setInterviewJunctionCandidateRequisition = new Set<ID>();
        List<WCT_Candidate_Requisition__c> lstUpdateNeededCRs= new List<WCT_Candidate_Requisition__c>();
        List<WCT_Candidate_Requisition__c> lstInterviewCRs=new List<WCT_Candidate_Requisition__c>();
        
        for(WCT_Interview_Junction__c newInterviewJunction : lstNewInterviewJunctions){
            setInterviewJunctionCandidateRequisition.add(newInterviewJunction.WCT_Candidate_Tracker__c);
        }
        
        if(!setInterviewJunctionCandidateRequisition.isEmpty()){
        //getting Candidate Aquisitions which are not in "In progress" status
            try{
                lstInterviewCRs=[select id,WCT_Candidate_Requisition_Status__c from WCT_Candidate_Requisition__c
                                                                   where Id in :setInterviewJunctionCandidateRequisition
                                                                   and WCT_Candidate_Requisition_Status__c!=:INPROGRESS_STATUS limit :Limits.getLimitQueryRows()];
               }
               Catch(QueryException queryException){
                    System.debug(queryException.getMessage());                
                }
        }
        
        for(WCT_Candidate_Requisition__c tempCR:lstInterviewCRs){
           tempCR.WCT_Candidate_Requisition_Status__c=INPROGRESS_STATUS;
           lstUpdateNeededCRs.add(tempCR);
        }
       
       //updating the related Candidate Aquisitions 
       if(!lstUpdateNeededCRs.isempty()){
          
                Database.SaveResult[] srList=  Database.update(lstUpdateNeededCRs,false);           

                // Iterate through each returned result
                for (Database.SaveResult sr : srList) {
                    if (!sr.isSuccess()) {
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());                    
                        }
                    }
               }
        } 
     }
     
     
     /*
     * Method name  : UpdateInterviewJunctionsOnInterviewOnCancelAndComplete
     * Description  : Update all related Interview Juction Status to Complete/Cancel on Interview Cancel/Complete                      
     * Return Type  : Nil
     * Parameter    : New Interviews Junction map
     */
        public static void UpdateInterviewJunctionsOnInterviewOnCancelAndComplete(Map<Id,WCT_Interview__c> mapNewInterviews, Map<Id,WCT_Interview__c> mapOldInterviews){
        
        List<WCT_Interview_Junction__c> lstRelatedInterviewJunctions=new list<WCT_Interview_Junction__c>();
        try{
        //getting the open interview which are related with the Candidate Aquisitions
        lstRelatedInterviewJunctions=[select id,WCT_Interview__c,WCT_Interview_Junction_Status__c from WCT_Interview_Junction__c
                                                      where WCT_Interview__c in :mapNewInterviews.keySet() limit :Limits.getLimitQueryRows()];
         }Catch(QueryException queryException)
         {
              WCT_ExceptionUtility.logException('WCT_InterviewJunctionTriggerHandler','UpdateInterviewJunctionsOnInterviewcancellation',queryException.getmessage());
            
         }
        
        List<WCT_Interview_Junction__c> lstUpdateNeededInterviewJunctions= new List<WCT_Interview_Junction__c>();
       
        for(WCT_Interview_Junction__c InterviewJunction:lstRelatedInterviewJunctions){
            
            //checking if the related Interview status is canceled/completed ,then asigning the junction status with Interview status
            if(mapNewInterviews.get(InterviewJunction.WCT_Interview__c).WCT_Interview_Status__c == COMPLETE_STATUS ||
              mapNewInterviews.get(InterviewJunction.WCT_Interview__c).WCT_Interview_Status__c == CANCELED_STATUS){
                InterviewJunction.WCT_Interview_Junction_Status__c=mapNewInterviews.get(InterviewJunction.WCT_Interview__c).WCT_Interview_Status__c;
                lstUpdateNeededInterviewJunctions.add(InterviewJunction);
            }
        }
            
      //Updating the Interview Junction when event date is changed to have new Notification email in action in queue. 
         
        for(WCT_Interview_Junction__c InterviewJunction:lstRelatedInterviewJunctions)
        {
            system.debug('### upadting the values. '+lstRelatedInterviewJunctions);
            if(mapOldInterviews.get(InterviewJunction.WCT_Interview__c).IsEvent_Updated__c!=mapNewInterviews.get(InterviewJunction.WCT_Interview__c).IsEvent_Updated__c)
             {
                 boolean isinList=false;
                 for(WCT_Interview_Junction__c temp:lstUpdateNeededInterviewJunctions)
                 {
                     if(temp.id==InterviewJunction.id)
                     {
                         isinList=true;
                         //InterviewJunction.WCT_Interviewer_Recommendation__c=mapNewInterviews.get(InterviewJunction.WCT_Interview__c).IsEvent_Updated__c?InterviewJunction.WCT_Interviewer_Recommendation__c+'a':InterviewJunction.WCT_Interviewer_Recommendation__c+'b';
                         InterviewJunction.WCT_Interview_Event_Updated_Flag__c=mapNewInterviews.get(InterviewJunction.WCT_Interview__c).IsEvent_Updated__c;
                     }
                 }
                 if(isinList==false)
                 {
                     //InterviewJunction.WCT_Interviewer_Recommendation__c=mapNewInterviews.get(InterviewJunction.WCT_Interview__c).IsEvent_Updated__c?InterviewJunction.WCT_Interviewer_Recommendation__c+'a':InterviewJunction.WCT_Interviewer_Recommendation__c+'b';
                     InterviewJunction.WCT_Interview_Event_Updated_Flag__c=mapNewInterviews.get(InterviewJunction.WCT_Interview__c).IsEvent_Updated__c;
                     lstUpdateNeededInterviewJunctions.add(InterviewJunction);
                     
                 }
                 
             }
           
        }
            
        
        //updating the Interviews to Cancel
        if(!lstUpdateNeededInterviewJunctions.isEmpty()){
            
              Database.SaveResult[] srList=Database.update(lstUpdateNeededInterviewJunctions);
                // Iterate through each returned result
                for (Database.SaveResult sr : srList) {
                    if (!sr.isSuccess()) {
                        String ErrorMessage='';
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors()) {
                            ErrorMessage=ErrorMessage+err.getStatusCode() + ': ' + err.getMessage()+'';                                                
                        }
                        WCT_ExceptionUtility.logException('WCT_InterviewJunctionTriggerHandler','UpdateInterviewJunctionsOnInterviewcancellation', ErrorMessage);
                        mapNewInterviews.get(Sr.getId()).addError('Record is not updated,please contact System Admin. Error Code:'+ ErrorMessage); 
                    }
               }
        }
     }
     
      /*
     * Method name  : UpdateCandidateRequisitionTimes
     * Description  : Update Candidate Aquisition Related Tracker                      
     * Return Type  : Nil
     * Parameter    : New Interview Trackers list
     */
     public static void UpdateCandidateRelTracker(List<WCT_Interview_Junction__c> lstNewInterviewJunctions)
     {
        Set<ID> setInterviewJunctionCandidateRequisition = new Set<ID>();
        Map<ID,List<ID>> CTRelIntvJunMap = new Map<ID,List<ID>>();
        List<WCT_Candidate_Requisition__c> liCandReqToUpdate = new list<WCT_Candidate_Requisition__c>();
        //Map of Candidate Tracker Id and List of Interview Tracker ids with that CT        
        for(WCT_Interview_Junction__c ij : lstNewInterviewJunctions){
            List<ID> tempIJIds = new List<ID>();
            setInterviewJunctionCandidateRequisition.add(ij.WCT_Candidate_Tracker__c);  
            if(!CTRelIntvJunMap.containsKey(ij.WCT_Candidate_Tracker__c)){
                tempIJIds.add(ij.id);
                CTRelIntvJunMap.put(ij.WCT_Candidate_Tracker__c,tempIJIds);
            }else{
                tempIJIds = CTRelIntvJunMap.get(ij.WCT_Candidate_Tracker__c);
                tempIJIds.add(ij.id);
                CTRelIntvJunMap.put(ij.WCT_Candidate_Tracker__c,tempIJIds);
            }
        }
        Date tempDate = date.newinstance(2014,5,6);
        for(WCT_Candidate_Requisition__c caReq :[SELECT Id,WCT_Related_Interview_Tracker__c,createdDate from WCT_Candidate_Requisition__c
                                                    WHERE Id IN :setInterviewJunctionCandidateRequisition AND (createdDate > :tempDate)]){
            if(caReq.WCT_Related_Interview_Tracker__c == null){                                        
                WCT_Candidate_Requisition__c cr = new WCT_Candidate_Requisition__c();
                cr = caReq;
                cr.WCT_Related_Interview_Tracker__c = CTRelIntvJunMap.get(caReq.id)[0];
                liCandReqToUpdate.add(cr);
            }
        }
       
       if(!liCandReqToUpdate.isEmpty()){
            Database.SaveResult[] srList=Database.update(liCandReqToUpdate);
       }
     }
    /*
     * Method name  : Prepare List of Trakers
     * Description  : Update Candidate Aquisition Related Tracker                      
     * Return Type  : Nil
     * Parameter    : New Interviews list
     */
     public static void PrepareIntTrackerList(List<WCT_Interview__c> lstNewInterviews,Map<id,WCT_Interview__c> oldInterviewsMap){
        
        List<Id> UpdatedInterviewIds = new List<Id>();
        List<WCT_Interview_Junction__c> intvTrackersToUpdateCT = new List<WCT_Interview_Junction__c>();
        
        for(WCT_Interview__c intv : lstNewInterviews){
            if(intv.WCT_Interview_Start_Time__c <> oldInterviewsMap.get(intv.id).WCT_Interview_Start_Time__c){
                UpdatedInterviewIds.add(intv.id);
            }
        }
        
        intvTrackersToUpdateCT = [Select id,WCT_Candidate_Tracker__c,createdDate from WCT_Interview_Junction__c where WCT_Interview__c IN:UpdatedInterviewIds order by createdDate asc];
        if(!intvTrackersToUpdateCT.isEmpty()){
            UpdateCandidateRelTracker(intvTrackersToUpdateCT);
        }       

    }    
    
    /*
     * Method name  : InsertInterviewShareRecords
     * Description  : Update sharing on Interview                     
     * Return Type  : Nil
     * Parameter    : New Tracker list
    
    */
    public static void InsertInterviewShareRecords(List<WCT_Interview_Junction__c> newTrackerList){
        
        List<WCT_Interview__Share> intSharesToCreate = new List<WCT_Interview__Share>();
        Set<ID> candTrackIdSet = new Set<ID>();
        Set<Id> currentIntIdSet = new Set<Id>();
        Set<Id> currentIntvrIdSet = new Set<Id>();
        
        for(WCT_Interview_Junction__c ij: newTrackerList){
            candTrackIdSet.add(ij.WCT_Candidate_Tracker__c);
            currentIntIdSet.add(ij.WCT_Interview__c);
            if(ij.WCT_Interviewer__c<>null){
                currentIntvrIdSet.add(ij.WCT_Interviewer__c);
            }
        }
        
        for(WCT_Interview_Junction__c ij:[SELECT id,WCT_Interviewer__c,WCT_Restricted_Interview__c,WCT_Interview__c,WCT_Candidate_Tracker__c,
                                            WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__c,
                                            WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiter__c FROM WCT_Interview_Junction__c 
                                             WHERE WCT_Candidate_Tracker__c IN :candTrackIdSet]){
            
            if(currentIntIdSet.contains(ij.WCT_Interview__c)){
                if(ij.WCT_Restricted_Interview__c){
                    if(ij.WCT_Interviewer__c <> null){
                        WCT_Interview__Share shareRecInt = new WCT_Interview__Share();
                        shareRecInt.ParentId = ij.WCT_Interview__c;
                        shareRecInt.UserOrGroupId  = ij.WCT_Interviewer__c;
                        shareRecInt.AccessLevel = 'Edit';
                        intSharesToCreate.add(shareRecInt);
                    }
                    if(ij.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__c <> null){
                        WCT_Interview__Share shareRecRC = new WCT_Interview__Share();
                        shareRecRC.ParentId = ij.WCT_Interview__c;
                        shareRecRC.UserOrGroupId  = ij.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__c;
                        shareRecRC.AccessLevel = 'Edit';
                        intSharesToCreate.add(shareRecRC);
                    }
                    if(ij.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiter__c <> null){
                        WCT_Interview__Share shareRecRecr = new WCT_Interview__Share();
                        shareRecRecr.ParentId = ij.WCT_Interview__c;
                        shareRecRecr.UserOrGroupId  = ij.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiter__c;
                        shareRecRecr.AccessLevel = 'Edit';
                        intSharesToCreate.add(shareRecRecr);
                    }
                }
            }else if(ij.WCT_Restricted_Interview__c){
                for(Id intvrId:currentIntvrIdSet){
                    WCT_Interview__Share shareRecIntPre = new WCT_Interview__Share();
                    shareRecIntPre.ParentId = ij.WCT_Interview__c;
                    shareRecIntPre.UserOrGroupId  = intvrId;
                    shareRecIntPre.AccessLevel = 'Edit';
                    intSharesToCreate.add(shareRecIntPre);
                }           
            }
            
        }
        if(!intSharesToCreate.isEmpty()){
            //insert intSharesToCreate;
            DataBase.insert(intSharesToCreate,false);
        }   
            
    }
    /*
     * Method name  : DeleteIntvShareRecords
     * Description  : Delete Manual sharing on Interview                     
     * Return Type  : Nil
     * Parameter    : Old Tracker list
    
    */
    public static void DeleteIntvShareRecords(List<WCT_Interview_Junction__c> oldTrackerList){
        Set<String> intvIntvrCombToDelete = new Set<String>();
        Set<String> intvIntvrCombToRetain = new Set<String>();
        Set<Id> InterviewIds = new Set<Id>();
        List<WCT_Interview__Share> intvShareRecToDelete = new  List<WCT_Interview__Share>();
    
        for(WCT_Interview_Junction__c ij:oldTrackerList){
            if(ij.WCT_Restricted_Interview__c){
                if(ij.WCT_Interviewer__c <> null){
                    intvIntvrCombToDelete.add(ij.WCT_Interviewer__c+'-'+ij.WCT_Interview__c);
                    InterviewIds.add(ij.WCT_Interview__c);
                }
            }
        }

        for(WCT_Interview_Junction__c ijn : [SELECT Id,WCT_Interviewer__c,WCT_Interview__c,WCT_Restricted_Interview__c FROM WCT_Interview_Junction__c
                                               WHERE WCT_Interview__c IN :InterviewIds]){
            if(ijn.WCT_Restricted_Interview__c){
                if(ijn.WCT_Interviewer__c <> null){
                    intvIntvrCombToRetain.add(ijn.WCT_Interviewer__c+'-'+ijn.WCT_Interview__c);
                }
            }       
        
        }
        
        for(WCT_Interview__Share intShr:[SELECT id,UserOrGroupId,AccessLevel,ParentId FROM WCT_Interview__Share 
                                            WHERE ParentId IN :InterviewIds AND AccessLevel = 'Edit']){
                
            if(intvIntvrCombToDelete.contains(intShr.UserOrGroupId+'-'+intShr.ParentId)){
                if(!intvIntvrCombToRetain.contains(intShr.UserOrGroupId+'-'+intShr.ParentId)){
                    intvShareRecToDelete.add(intShr);
                }
            }
        
        }
        
        if(!intvShareRecToDelete.isEmpty()){
            Delete intvShareRecToDelete;
        }
    
    }
     
}