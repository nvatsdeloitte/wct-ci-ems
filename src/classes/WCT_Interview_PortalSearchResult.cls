public with sharing class WCT_Interview_PortalSearchResult {
private string srchStr {get;set;}
public list<WCT_Requisition__c> ReqResultLst {get;set;}
public list<WCT_Interview__c> IntResultLst {get;set;}
public list<Contact> ContResultLst {get;set;}
public Boolean isError {get;set;}
private list<WCT_Interview_Junction__c> junctionRecOnly {get;set;}
private set<id> candidateIds, interviewIds, reqIds; 

    public WCT_Interview_PortalSearchResult(){
    srchStr = Apexpages.currentpage().getparameters().get('q');
    isError = false;
    candidateIds = new set <id>();
    interviewIds = new set<id>();
    reqIds = new set<id>();
    for(WCT_Interview_Junction__c junctionRecOnly :[select WCT_Candidate_Tracker__r.WCT_Contact__c, WCT_Candidate_Tracker__r.WCT_Requisition__r.Id, WCT_Interview__c from WCT_Interview_Junction__c where WCT_Interviewer__c = :UserInfo.getUserId() and WCT_Interview_Status__c IN ('Complete','Invite Sent')]){
        candidateIds.add(junctionRecOnly.WCT_Candidate_Tracker__r.WCT_Contact__c);
        interviewIds.add(junctionRecOnly.WCT_Interview__c);
        reqIds.add(junctionRecOnly.WCT_Candidate_Tracker__r.WCT_Requisition__r.Id);    
    }

        try{
            if(srchStr!='' && srchStr != null){
                String searchqueryRequisition='select Name, WCT_Recruiter__r.name, WCT_Service_Area__c , WCT_Service_Line__c, WCT_Recruiting_Coordinator__r.Name from WCT_Requisition__c where id in :reqIds and  (Name LIKE \'%'+srchStr+'%\' or WCT_Recruiter__r.name LIKE \'%'+srchStr+'%\' or WCT_Service_Area__c LIKE \'%'+srchStr+'%\'  or WCT_Service_Line__c LIKE \'%'+srchStr+'%\' or WCT_Recruiting_Coordinator__r.Name LIKE \'%'+srchStr+'%\')';
                String searchqueryInterview='select Name, WCT_FSS__c, WCT_Interview_Type__c, WCT_User_group__c, WCT_Job_Type__c from WCT_Interview__c where id in :interviewIds and (Name LIKE \'%'+srchStr+'%\')';
                String searchqueryCandidate='select Name, WCT_Preferred_Name__c,WCT_Candidate_School__c, WCT_School_Education_Institution_lkp__r.Name, WCT_ExternalEmail__c from Contact where id in : candidateIds and (Name LIKE \'%'+srchStr+'%\' or WCT_Preferred_Name__c LIKE \'%'+srchStr+'%\' or WCT_Candidate_School__c LIKE \'%'+srchStr+'%\'  or WCT_School_Education_Institution_lkp__r.Name LIKE \'%'+srchStr+'%\' or WCT_ExternalEmail__c LIKE \'%'+srchStr+'%\')';
                ReqResultLst = Database.query(searchqueryRequisition);
                IntResultLst = Database.query(searchqueryInterview);
                ContResultLst = Database.query(searchqueryCandidate);
                if(ReqResultLst.isEmpty() && IntResultLst.isEmpty() && ContResultLst.isEmpty()){
                    isError = true;
                    ApexPages.Message message = new ApexPages.message(ApexPages.severity.ERROR,'No Record Found.');
                    ApexPages.addMessage(message);
                }
            }
            else{
                isError = true;
                ApexPages.Message message = new ApexPages.message(ApexPages.severity.ERROR,Label.Interview_Portal_Search_Exception);
                ApexPages.addMessage(message);
            }
        }
        catch(exception e){
                isError = true;
                ApexPages.Message message = new ApexPages.message(ApexPages.severity.ERROR,Label.Interview_Portal_Search_Exception);
                ApexPages.addMessage(message);
        }
    }
}