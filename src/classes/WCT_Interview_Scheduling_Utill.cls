public class WCT_Interview_Scheduling_Utill {
    public static void handelInvities(Set<Id> EventIds){
        map<Id, set<Id>> eventInvitiesMap = new map<Id, set<Id>>();
        set<Id> invitesIds=new set<Id>();
        Set<Id> eventWhatIds =new Set<Id>();
        for(Event eventRecord : [SELECT ID, WhatId, 
                                        (SELECT EventId, RelationId, Event.WhatId FROM EventRelations where IsInvitee = true
                                         and Relation.type = 'User') 
                                        FROM Event 
                                        where Id in :EventIds])
        {
            if(!eventRecord.EventRelations.IsEmpty())
            {
                invitesIds=new set<Id>();
                for(EventRelation eventRelation : eventRecord.EventRelations)
                {
                    
                    if(eventRelation.RelationId!=null)
                        invitesIds.add(eventRelation.RelationId);
                    
                        
                }
            }
            if(!invitesIds.IsEmpty())
                eventInvitiesMap.put(eventRecord.WhatID,invitesIds);
            else if(invitesIds.IsEmpty())
                eventWhatIds.add(eventRecord.WhatID);
                
        }
        
        Map<Id, List<WCT_Interview_Junction__c>> intJunctionMap=new Map<Id, List<WCT_Interview_Junction__c>> ();
        List<WCT_Interview_Junction__c> toBeDeletedInvite = new List<WCT_Interview_Junction__c>();
        List<WCT_Interview_Junction__c> toBeAddInvite = new List<WCT_Interview_Junction__c>();
        List<WCT_Interview_Junction__c> intJunctionLst=new LIst<WCT_Interview_Junction__c>();
        for(WCT_Interview_Junction__c junct: [SELECT Id, WCT_Interview__c ,WCT_Candidate_Tracker__c,WCT_Interviewer__c FROM WCT_Interview_Junction__c 
                                    			where WCT_Interview__c in : eventInvitiesMap.KeySet()] )
        {
            if(intJunctionMap.ContainsKey(junct.WCT_Interview__c ))
            {
                intJunctionLst=new LIst<WCT_Interview_Junction__c>();
                intJunctionLst=intJunctionMap.get(junct.WCT_Interview__c );
                intJunctionLst.add(junct);
                intJunctionMap.put(junct.WCT_Interview__c,intJunctionLst);
                
            }
            else
            {
                intJunctionLst=new LIst<WCT_Interview_Junction__c>();
                intJunctionLst.add(junct);
                intJunctionMap.put(junct.WCT_Interview__c,intJunctionLst);
            }
        }
        
        
        map<Id, Id> intervieweTBR = new map<Id, Id>();
        for(id whtId : eventInvitiesMap.keySet())
        {
            set<Id> tmpER = new set<Id>();
            tmpER.addAll(eventInvitiesMap.get(whtId));
            Set<Id> tempCT=new Set<Id>();
            for(WCT_Interview_Junction__c innerJunct : intJunctionMap.get(whtId))
            {
                tempCT.add(innerJunct.WCT_Candidate_Tracker__c);
                // --- toBeDeletedInvite.add(innerJunct);

                if(innerJunct.WCT_Interviewer__c!=null && (!eventInvitiesMap.get(whtId).contains(innerJunct.WCT_Interviewer__c)))
                {
                    system.debug('I am here in Delete list');
                    system.debug('>>>>>>>>>>'+innerJunct);
                    toBeDeletedInvite.add(innerJunct);
                }
                else if(innerJunct.WCT_Interviewer__c!=null && eventInvitiesMap.get(whtId).contains(innerJunct.WCT_Interviewer__c))
                {
                    tmpER.remove(innerJunct.WCT_Interviewer__c);
                }
                else if(innerJunct.WCT_Interviewer__c==null)
                {
                	// ---- 
                }
                
                
            }
            for(Id idTemp: tmpER){
                
                for(Id idCT:tempCT)
                {
                    WCT_Interview_Junction__c tbc = new WCT_Interview_Junction__c();
                    tbc.WCT_Interview__c = whtId;
                    tbc.WCT_Interviewer__c = idTemp;
                    tbc.WCT_Candidate_Tracker__c = idCT;
                    toBeAddInvite.add(tbc);
                }
            }
            
        }
        if(!toBeAddInvite.IsEmpty()) 
            insert toBeAddInvite;
        if(!toBeDeletedInvite.IsEmpty()) delete toBeDeletedInvite;
        toBeDeletedInvite=new List<WCT_Interview_Junction__c>();
        for(WCT_Interview_Junction__c tempJun:[SELECT Id, WCT_Interview__c ,WCT_Candidate_Tracker__c,WCT_Interviewer__c FROM WCT_Interview_Junction__c 
                                    			where WCT_Interview__c in : eventInvitiesMap.KeySet() and WCT_Interviewer__c=null])
        {
        	toBeDeletedInvite.add(tempJun);
        }
        if(!toBeDeletedInvite.IsEmpty()) delete toBeDeletedInvite;
        
        
        // When there is no invities...
        Set<Id> candidateJuncExistingSet=new Set<Id>();
        List<WCT_Interview_Junction__c> existingJunToBeDeleted=new List<WCT_Interview_Junction__c>();
        List<WCT_Interview_Junction__c> newJunToBeInserted=new List<WCT_Interview_Junction__c>();
        Map<Id, Set<Id>> existingJunctionMap=new Map<Id, Set<Id>> ();
        List<WCT_Interview_Junction__c> existingJunList=new List<WCT_Interview_Junction__c>();
        Set<Id> candidateSet=new set<Id>();
        
        if(!eventWhatIds.IsEmpty())
        {
            for(WCT_Interview_Junction__c junct: [SELECT Id, WCT_Interview__c ,WCT_Candidate_Tracker__c,WCT_Interviewer__c FROM WCT_Interview_Junction__c 
                                    where WCT_Interview__c in : eventWhatIds] )
            {
                existingJunToBeDeleted.add(junct);
                if(existingJunctionMap.ContainsKey(junct.WCT_Interview__c ))
                {
                    candidateSet=new set<Id>();
                    candidateSet.addAll(existingJunctionMap.get(junct.WCT_Interview__c ));
                    candidateSet.add(junct.WCT_Candidate_Tracker__c);
                    existingJunctionMap.put(junct.WCT_Interview__c,candidateSet);
                    
                }
                else
                {
                    candidateSet=new set<Id>();
                    candidateSet.add(junct.WCT_Candidate_Tracker__c);
                    existingJunctionMap.put(junct.WCT_Interview__c,candidateSet);
                }   
            }
            
            for(Id whatIdTemp:eventWhatIds)
            {
                if(existingJunctionMap.get(whatIdTemp)!=null)
                {
                    for(Id candidateId:existingJunctionMap.get(whatIdTemp))
                    {
                        WCT_Interview_Junction__c tbc = new WCT_Interview_Junction__c();
                        tbc.WCT_Interview__c = whatIdTemp;
                        tbc.WCT_Candidate_Tracker__c = candidateId;
                        newJunToBeInserted.add(tbc);
                    }
                }
                
            }
            if(!newJunToBeInserted.IsEmpty())
            {
                try{
                    insert newJunToBeInserted;
                }Catch(DMLException e)
                {
                    throw e;
                }
            }
            if(!existingJunToBeDeleted.IsEmpty())
            {
                try{
                    delete existingJunToBeDeleted;
                }Catch(DMLException e)
                {
                    throw e;
                }
            }
        }
        
    }
    
     public static void handleCandidates(Set<Id> EventIds){
     	map<Id, set<Id>> eventInvitiesMap = new map<Id, set<Id>>();
        set<Id> invitesIds=new set<Id>();
        Set<Id> eventWhatIds =new Set<Id>();
        system.debug(EventIds+'EventSet');
        for(Event eventRecord : [SELECT ID, WhatId, 
                                        (SELECT EventId, RelationId, Event.WhatId FROM EventRelations where IsInvitee = true
                                         and Relation.type = 'User') 
                                        FROM Event 
                                        where Id in :EventIds])
        {
            if(!eventRecord.EventRelations.IsEmpty())
            {
                invitesIds=new set<Id>();
                for(EventRelation eventRelation : eventRecord.EventRelations)
                {
                    
                    if(eventRelation.RelationId!=null)
                        invitesIds.add(eventRelation.RelationId);
                    
                        
                }
            }
            if(!invitesIds.IsEmpty())
                eventInvitiesMap.put(eventRecord.WhatID,invitesIds);
            else if(invitesIds.IsEmpty())
                eventWhatIds.add(eventRecord.WhatID);
                
        }
        system.debug(eventInvitiesMap+'eventInvitiesMap');
        Map<Id, List<WCT_Interview_Junction__c>> intJunctionMap=new Map<Id, List<WCT_Interview_Junction__c>> ();
        List<WCT_Interview_Junction__c> toBeDeletedInvite = new List<WCT_Interview_Junction__c>();
        List<WCT_Interview_Junction__c> toBeAddInvite = new List<WCT_Interview_Junction__c>();
        List<WCT_Interview_Junction__c> intJunctionLst=new LIst<WCT_Interview_Junction__c>();
        for(WCT_Interview_Junction__c junct: [SELECT Id, WCT_Interview__c ,WCT_Candidate_Tracker__c,WCT_Interviewer__c FROM WCT_Interview_Junction__c 
                                    			where WCT_Interview__c in : eventInvitiesMap.KeySet() and WCT_Interviewer__c=null] )
        {
            if(intJunctionMap.ContainsKey(junct.WCT_Interview__c ))
            {
                intJunctionLst=new LIst<WCT_Interview_Junction__c>();
                intJunctionLst=intJunctionMap.get(junct.WCT_Interview__c );
                intJunctionLst.add(junct);
                intJunctionMap.put(junct.WCT_Interview__c,intJunctionLst);
                
            }
            else
            {
                intJunctionLst=new LIst<WCT_Interview_Junction__c>();
                intJunctionLst.add(junct);
                intJunctionMap.put(junct.WCT_Interview__c,intJunctionLst);
            }
            toBeDeletedInvite.add(junct);
        }
        system.debug(intJunctionMap+'intJunctionMap'); 
        for(id whtId : eventInvitiesMap.keySet())
        {
        	set<Id> tmpER = new set<Id>();
            tmpER.addAll(eventInvitiesMap.get(whtId));
            Set<Id> tempCT=new Set<Id>();
            for(WCT_Interview_Junction__c innerJunct : intJunctionMap.get(whtId))
            {
                tempCT.add(innerJunct.WCT_Candidate_Tracker__c);
            }
            
            for(Id idTemp: tmpER){
                
                for(Id idCT:tempCT)
                {
                    WCT_Interview_Junction__c tbc = new WCT_Interview_Junction__c();
                    tbc.WCT_Interview__c = whtId;
                    tbc.WCT_Interviewer__c = idTemp;
                    tbc.WCT_Candidate_Tracker__c = idCT;
                    toBeAddInvite.add(tbc);
                }
            }
        }
        if(!toBeAddInvite.IsEmpty()) 
            insert toBeAddInvite;
        if(!toBeDeletedInvite.IsEmpty()) delete toBeDeletedInvite;
        
     }
    
    
}