/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class WCT_Interview_Scheduling_Utill_Test {


	/*******Varibale Declaration********/
    public static Id profileIdRecCoo=[Select id from Profile where Name=:WCT_UtilConstants.Recruiting_Coordinator].Id;
    public static Id profileIdIntv=[Select id from Profile where Name=:WCT_UtilConstants.INTERVIWER_PROFILE_NAME].Id;
    public static Id profileIdRecr=[Select id from Profile where Name=:WCT_UtilConstants.RECRUITER_COMPANY].Id;
   	public static String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();
    public static User userRecRecCoo, userRecRecIntv, userRecRecr;
    public static Contact Candidate;
    public static WCT_Requisition__c Requisition;
    public static WCT_Candidate_Requisition__c CandidateRequisition;
    public static WCT_Interview__c Interview;
    public static WCT_Interview_Junction__c InterviewTracker,InterviewTrackerRec,InterviewTrackerIntv,InterviewTrackerIntvRec;
    public static Event InterviewEvent;
    public static EventRelation InterviewEventRelation;
    public Static WCT_List_Of_Names__c ClickToolForm;
    public static Document document;
	/***************/

    /** 
        Method Name  : createUserRecCoo
        Return Type  : User
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecCoo()
    {
    	userRecRecCoo=WCT_UtilTestDataCreation.createUser('ReCoSchd', profileIdRecCoo, 'arunsharmaReCoSchd@deloitte.com', 'arunsharma4@deloitte.com');
    	insert userRecRecCoo;
    	return  userRecRecCoo;
    }
    /** 
        Method Name  : createUserRecr
        Return Type  : User
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecr()
    {
    	userRecRecr=WCT_UtilTestDataCreation.createUser('RecrSchd', profileIdRecr, 'arunsharmaRecrSchd@deloitte.com', 'arunsharma4@deloitte.com');
    	insert userRecRecr;
    	return  userRecRecr;
    }
    /** 
        Method Name  : createUserIntv
        Return Type  : User
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserIntv()
    {
    	userRecRecIntv=WCT_UtilTestDataCreation.createUser('IntvSchd', profileIdIntv, 'arunsharmaIntvSchd@deloitte.com', 'arunsharma4@deloitte.com');
    	insert userRecRecIntv;
    	return  userRecRecIntv;
    }
    /** 
        Method Name  : createCandidate
        Return Type  : Contact
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private Static Contact createCandidate()
    {
    	Candidate=WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
    	insert Candidate;
    	return  Candidate;
    }    
    /** 
        Method Name  : createRequisition
        Return Type  : WCT_Requisition__c
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Requisition__c createRequisition()
    {
    	Requisition=WCT_UtilTestDataCreation.createRequisition(userRecRecr.Id);
    	insert Requisition;
    	return  Requisition;
    }
    /** 
        Method Name  : createCandidateRequisition
        Return Type  : WCT_Candidate_Requisition__c
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Candidate_Requisition__c createCandidateRequisition()
    {
    	CandidateRequisition=WCT_UtilTestDataCreation.createCandidateRequisition(Candidate.ID,Requisition.Id);
    	insert CandidateRequisition;
    	return  CandidateRequisition;
    } 
    /** 
        Method Name  : createDocument
        Return Type  : Document
        Type 		 : private
        Description  : Create temp records for data mapping         
    */    
    private Static Document createDocument()
    {
    	document=WCT_UtilTestDataCreation.createDocument();
		insert document;
    	return  document;
    }
    /** 
        Method Name  : createClickToolForm
        Return Type  : WCT_List_Of_Names__c
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_List_Of_Names__c createClickToolForm()
    {
    	ClickToolForm=WCT_UtilTestDataCreation.createClickToolForm(document.id);
    	insert ClickToolForm; 
    	return  ClickToolForm;
    } 
    /** 
        Method Name  : createInterview
        Return Type  : WCT_Interview__c
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview__c createInterview()
    {
    	Interview=WCT_UtilTestDataCreation.createInterview(ClickToolForm.Id);
    	system.runAs(userRecRecCoo)
    	{
    		insert Interview;
    	}
    	return  Interview;
    }   
    /** 
        Method Name  : createInterviewTracker
        Return Type  : WCT_Interview_Junction__c
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview_Junction__c createInterviewTracker()
    {
    	InterviewTracker=WCT_UtilTestDataCreation.createInterviewTracker(Interview.Id,CandidateRequisition.Id,userRecRecIntv.Id);
    	insert InterviewTracker;
    	return  InterviewTracker;
    }  
    private Static WCT_Interview_Junction__c createInterviewTrackerIntv()
    {
    	InterviewTrackerIntv=WCT_UtilTestDataCreation.createInterviewTracker(Interview.Id,CandidateRequisition.Id);
    	insert InterviewTrackerIntv;
    	return  InterviewTrackerIntv;
    }  
    /** 
        Method Name  : createEvent
        Return Type  : Event
        Type 		 : private
        Description  : Create temp records for data mapping         
    */    
    private Static Event createEvent()
    {
    	InterviewEvent=WCT_UtilTestDataCreation.createEvent(Interview.Id);
    	system.runAs(userRecRecCoo)
    	{
    		insert InterviewEvent;
    	}
    	return  InterviewEvent;
    }     
    /** 
        Method Name  : createEventRelation
        Return Type  : EventRelation
        Type 		 : private
        Description  : Create temp records for data mapping         
    */    
    private Static EventRelation createEventRelation()
    {
    	InterviewEventRelation=WCT_UtilTestDataCreation.createEventRelation(InterviewEvent.Id,userRecRecIntv.Id);
		insert InterviewEventRelation;
    	return  InterviewEventRelation;
    }   

    static testMethod void myUnitTestwithInterviwer() {
    	userRecRecCoo=createUserRecCoo();
		userRecRecIntv=createUserIntv();
		userRecRecr=createUserRecr();
		Candidate=createCandidate();
		Requisition=createRequisition();
		CandidateRequisition=createCandidateRequisition();
		document=createDocument();
		ClickToolForm=createClickToolForm();
		Interview=createInterview();
		InterviewEvent=createEvent();
		InterviewEventRelation=createEventRelation();
		InterviewTracker=createInterviewTracker();
		InterviewTrackerRec=createInterviewTracker();
		test.startTest();
		set<Id> invtIds = new set<id>();
		invtIds.add(InterviewEvent.Id);
		set<id> interviewIds = new set<id>();
		interviewIds.add(Interview.Id);
        WCT_Interview_Scheduling_Utill.handelInvities(invtIds);
        WCT_Interview_Scheduling_Utill.handleCandidates(interviewIds);
        test.stopTest();
	}
    static testMethod void myUnitTestwithoutInterviwer() {
    	userRecRecCoo=createUserRecCoo();
		userRecRecIntv=createUserIntv();
		userRecRecr=createUserRecr();
		Candidate=createCandidate();
		Requisition=createRequisition();
		CandidateRequisition=createCandidateRequisition();
		document=createDocument();
		ClickToolForm=createClickToolForm();
		Interview=createInterview();
		InterviewEvent=createEvent();
		InterviewEventRelation=createEventRelation();
		InterviewTrackerIntv=createInterviewTrackerIntv();
		InterviewTrackerIntvRec=createInterviewTrackerIntv();
		test.startTest();
		set<Id> invtIds = new set<id>();
		invtIds.add(InterviewEvent.Id);
		set<id> interviewIds = new set<id>();
		interviewIds.add(Interview.Id);
        WCT_Interview_Scheduling_Utill.handelInvities(invtIds);
        WCT_Interview_Scheduling_Utill.handleCandidates(interviewIds);
        test.stopTest();
	}
    static testMethod void myUnitTestwithoutInvitee() {
    	userRecRecCoo=createUserRecCoo();
		userRecRecIntv=createUserIntv();
		userRecRecr=createUserRecr();
		Candidate=createCandidate();
		Requisition=createRequisition();
		CandidateRequisition=createCandidateRequisition();
		document=createDocument();
		ClickToolForm=createClickToolForm();
		Interview=createInterview();
		InterviewEvent=createEvent();
		InterviewTrackerIntv=createInterviewTrackerIntv();
		InterviewTrackerIntvRec=createInterviewTrackerIntv();
		test.startTest();
		set<Id> invtIds = new set<id>();
		invtIds.add(InterviewEvent.Id);
		set<id> interviewIds = new set<id>();
		interviewIds.add(Interview.Id);
        WCT_Interview_Scheduling_Utill.handelInvities(invtIds);
        WCT_Interview_Scheduling_Utill.handleCandidates(interviewIds);
        test.stopTest();
	}
}