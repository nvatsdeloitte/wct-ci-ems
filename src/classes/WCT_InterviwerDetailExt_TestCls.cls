@isTest
public class WCT_InterviwerDetailExt_TestCls {
/*------ Variable Declaration ------*/
	public static String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();
	
	public static Contact Candidate;
	public static WCT_Candidate_Documents__c CandidateDocument;
    public static Attachment attachment;
    public static WCT_Requisition__c Requisition;
    public static WCT_Candidate_Requisition__c CandidateRequisition;
    public static WCT_Interview__c Interview;
    public static WCT_Interview_Junction__c InterviewTracker;
    public Static WCT_List_Of_Names__c ClickToolForm;
    public static Document document;
    
    public static User userRecRecr,userRecRecCoo,userRecRecIntv;
    
    public static Id profileIdRecr=[Select id from Profile where Name=:WCT_UtilConstants.RECRUITER_COMPANY].Id;
    public static Id profileIdRecCoo=[Select id from Profile where Name=:WCT_UtilConstants.Recruiting_Coordinator].Id;
    public static Id profileIdIntv=[Select id from Profile where Name=:WCT_UtilConstants.INTERVIWER_PROFILE_NAME].Id;
/*------End Variable Declaration------*/  
     /** 
        Method Name  : createUserRecCoo
        Return Type  : User
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecCoo()
    {
      userRecRecCoo=WCT_UtilTestDataCreation.createUser('ReCoSchd', profileIdRecCoo, 'arunsharmaReCoSchd@deloitte.com', 'arunsharma4@deloitte.com');
      insert userRecRecCoo;
      return  userRecRecCoo;
    }
     /** 
        Method Name  : createUserRecr
        Return Type  : User
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecr()
    {
      userRecRecr=WCT_UtilTestDataCreation.createUser('Recr', profileIdRecr, 'arunsharmaRecr@deloitte.com', 'arunsharma4@deloitte.com');
      insert userRecRecr;
      return  userRecRecr;
    }
    /** 
        Method Name  : createUserIntv
        Return Type  : User
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserIntv()
    {
      userRecRecIntv=WCT_UtilTestDataCreation.createUser('IntvSchd', profileIdIntv, 'arunsharmaIntvSchd@deloitte.com', 'arunsharma4@deloitte.com');
      insert userRecRecIntv;
      return  userRecRecIntv;
    }
	
	 /** 
        Method Name  : createCandidate
        Return Type  : Contact
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static Contact createCandidate()
    {
      Candidate=WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
      //Candidate=new Contact(lastName='LastName', firstName='FirstName',Email='candidate@deloitte.com',RecordTypeId=candidateRecordTypeId,WCT_Taleo_Id__c='123455');
      insert Candidate;
      return  Candidate;
    }  
    
     /** 
        Method Name  : createCandidateDocument
        Return Type  : WCT_Candidate_Documents__c
        Type      : private
        Description  : Create temp records for data mapping         
    */    
    private Static WCT_Candidate_Documents__c createCandidateDocument()
    {
      	CandidateDocument = new WCT_Candidate_Documents__c(WCT_Candidate__c= Candidate.Id,WCT_File_Name__c='CandidateResume', WCT_File_Path__c='c:/', WCT_RMS_ID__c='1234', WCT_Created_Date__c=system.today(),WCT_Visible_to_Interviewer__c = True);
    	insert CandidateDocument;
      	return  CandidateDocument;
     }
    /** 
        Method Name  : createAttachment
        Return Type  : Attachment
        Type      : private
        Description  : Create temp records for data mapping         
    */    
    private Static Attachment createAttachment()
    {
      	attachment=WCT_UtilTestDataCreation.createAttachment(CandidateDocument.Id);
    	insert attachment;
      	return  attachment;
    } 
     /** 
        Method Name  : createDocument
        Return Type  : Document
        Type      : private
        Description  : Create temp records for data mapping         
    */    
    private Static Document createDocument()
    {
      document=WCT_UtilTestDataCreation.createDocument();
   	  insert document;
      return  document;
    }
       /** 
        Method Name  : createRequisition
        Return Type  : WCT_Requisition__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Requisition__c createRequisition()
    {
      Requisition=WCT_UtilTestDataCreation.createRequisition(userRecRecr.Id);
      insert Requisition;
      return  Requisition;
    }
    /** 
        Method Name  : createCandidateRequisition
        Return Type  : WCT_Candidate_Requisition__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Candidate_Requisition__c createCandidateRequisition()
    {
      CandidateRequisition=WCT_UtilTestDataCreation.createCandidateRequisition(Candidate.ID,Requisition.Id);
      insert CandidateRequisition;
      return  CandidateRequisition;
    }   
        /** 
        Method Name  : createClickToolForm
        Return Type  : WCT_List_Of_Names__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_List_Of_Names__c createClickToolForm()
    {
      ClickToolForm=WCT_UtilTestDataCreation.createClickToolForm(document.id);
      insert ClickToolForm; 
      return  ClickToolForm;
    } 
    /** 
        Method Name  : createInterview
        Return Type  : WCT_Interview__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview__c createInterview()
    {
      Interview=WCT_UtilTestDataCreation.createInterview(ClickToolForm.Id);
      system.runAs(userRecRecCoo)
      {
        insert Interview;
      }
      return  Interview;
    }   
    /** 
        Method Name  : createInterviewTracker
        Return Type  : WCT_Interview_Junction__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview_Junction__c createInterviewTracker()
    {
      InterviewTracker=WCT_UtilTestDataCreation.createInterviewTracker(Interview.Id,CandidateRequisition.Id,userRecRecIntv.Id);
      insert InterviewTracker;
      return  InterviewTracker;
    }
    
         
   public static testmethod void testConstruct(){
   	
   	//------ Set up test data ------
   		userRecRecCoo=createUserRecCoo();
	    userRecRecIntv=createUserIntv();
	    userRecRecr=createUserRecr();
	    Candidate=createCandidate();
	    Requisition=createRequisition();
	    CandidateRequisition=createCandidateRequisition();
	    CandidateDocument=createCandidateDocument();
	    attachment=createAttachment();
	    document=createDocument();
	    ClickToolForm=createClickToolForm();
	    Interview=createInterview();
	    InterviewTracker=createInterviewTracker();
	    
    	
    	
    	//set<id> attids = new set<id>();
    	//attids.add(Candidate.id);
    	//attids.add(CandidateDocument.id);
    	
    //------ Start test execution ------	
    	Test.startTest();
    	ApexPages.StandardController stdController = new ApexPages.StandardController(InterviewTracker);
    	    	
    	System.debug('-------**********INTERVIEW TRACKER TEST CLASS****------'+InterviewTracker.Id);
    	
    	PageReference pageRef = new PageReference('/apex/WCT_InterviewDetail?id='+InterviewTracker.Id); 
    	//pageRef.getParameters().put('id', String.valueOf(Candidate.Id));
       	
       	Test.setCurrentPage(pageRef);
        WCT_InterviwerDetailExt obj = new WCT_InterviwerDetailExt(stdController);
    	obj.getAttachmentsCandidate();
    	obj.setAttachmentSection();
    	obj.nameFile = 'testNameAttachment';
    	obj.contentFile = Blob.valueof('Test Value for Attachment Body.');
    	obj.readFile();
    	obj.getAttachmentIEFs();
    	obj.getNotesCandidate();
    	obj.getCandidateLst();
    	obj.getInvtEventId();
    	obj.cancelAttachment();
    	//System.assertNotEquals(obj.getAttachmentsCandidate(), null);
    	//obj.getNotesCandidate();
    	Test.stopTest(); 
    //------ Stop test execution ------
    } 
   public static testmethod void withoutBlobBody(){
   	
   	//------ Set up test data ------
   		userRecRecCoo=createUserRecCoo();
	    userRecRecIntv=createUserIntv();
	    userRecRecr=createUserRecr();
	    Candidate=createCandidate();
	    Requisition=createRequisition();
	    CandidateRequisition=createCandidateRequisition();
	    CandidateDocument=createCandidateDocument();
	    attachment=createAttachment();
	    document=createDocument();
	    ClickToolForm=createClickToolForm();
	    Interview=createInterview();
	    InterviewTracker=createInterviewTracker();
	   
    //------ Start test execution ------	
    	Test.startTest();
    	ApexPages.StandardController stdController = new ApexPages.StandardController(InterviewTracker);
    	PageReference pageRef = new PageReference('/apex/WCT_InterviewDetail?id='+InterviewTracker.Id); 
    	
       	Test.setCurrentPage(pageRef);
        WCT_InterviwerDetailExt obj = new WCT_InterviwerDetailExt(stdController);
    	obj.getAttachmentsCandidate();
    	obj.setAttachmentSection();
    	//obj.nameFile = 'testNameAttachment';
    	//obj.contentFile = Blob.valueof('');
    	obj.readFile();
    	obj.getAttachmentIEFs();
    	obj.getNotesCandidate();
    	obj.getCandidateLst();
    	obj.getInvtEventId();
    	obj.cancelAttachment();
    	Test.stopTest(); 
    //------ Stop test execution ------
    }     
}