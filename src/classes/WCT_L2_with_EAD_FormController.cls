public class WCT_L2_with_EAD_FormController  extends SitesTodHeaderController{

/* public variables */
public WCT_Immigration__c ImmigrationRecord{get;set;}
public WCT_Mobility__c MobilityRecord{get;set;}
public List<RecordType> recordlist{get;set;}

 /* Upload Related Variables */
public Document doc {get;set;}
public List<String> docIdList = new List<string>();
public List<AttachmentsWrapper> UploadedDocumentList {get; set; }
public task t{get;set;}
public String taskid{get;set;}
public List<Attachment> listAttachments {get; set;}
public Map<Id, String> mapAttachmentSize {get; set;}  
public Integer countattach{get;set;} 
Public Map<id,string> maprecord{get;set;}

// Error Message related variables
public boolean pageError {get; set;}
public String pageErrorMessage {get; set;}
public String supportAreaErrorMesssage {get; set;}   

public String l1visaStartDate {get;set;}
public String l1visaEndDate {get;set;}
public String l2visaStartDate {get;set;}
public String l2visaEndDate {get;set;}
public String eadstart {get;set;}
public String eadexpire {get;set;}
public String applicationdate{get;set;} 
public String OwnerIdStr {get;set;} 
 
 public WCT_L2_with_EAD_FormController ()
{  
    
    
        /*Initialize all Variables*/
        init();

        /*Get Task ID from Parameter*/
        getParameterInfo();  
        
        /*Task ID Null Check*/
        if(taskid=='' || taskid==null)
        {
           invalidEmployee=true;
           return;
        }

        /*Get Task Instance*/
        getTaskInstance();
        MobilityRecord = [SELECT Id,Immigration__c,OwnerId,name, WCT_Mobility_Status__c FROM WCT_Mobility__c where id=:t.WhatId ];
        OwnerIdStr = MobilityRecord.OwnerId;
        /*Get Attachment List and Size*/
        getAttachmentInfo();
        
        getrecordtype();
        
        pageError=false;
}



 private void init(){

        //Custom Object Instances
        ImmigrationRecord= new WCT_Immigration__c (); 
        maprecord=new Map<id,string>();
    
         countattach = 0;
        //Document Related Init
        doc = new Document();
        UploadedDocumentList = new List<AttachmentsWrapper>();
        listAttachments = new List<Attachment>();
        mapAttachmentSize = new Map<Id, String>();
        
        //recordtype RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'WCT_Immigration__c' AND DeveloperName = 'L2_with_EAD' Limit 1];
        //Immigrationrecord.recordtypeid =RecordTypeId.id ;
      
        MobilityRecord=new WCT_Mobility__c ();
        ImmigrationRecord.WCT_Immigration_Status__c='L2 with EAD';
       
       
    }   
    
    private void getParameterInfo(){
        taskid = ApexPages.currentPage().getParameters().get('taskid');
    }

    private void getTaskInstance(){
        t=[SELECT Status,OwnerId,WhatId  FROM Task WHERE Id =: taskid];
    }
    private void getrecordtype()
    {
        recordlist=[SELECT id,DeveloperName,Name,SobjectType FROM RecordType WHERE SobjectType = 'WCT_Immigration__c'];
    }

    private void getAttachmentInfo(){
        listAttachments = [SELECT ParentId , 
                                    Name, 
                                    BodyLength, 
                                    Id,
                                    CreatedDate
                                    FROM  Attachment 
                                    WHERE ParentId = :taskid
                                    ORDER BY CreatedDate DESC
                                    LIMIT 50 ];    
              
        for(Attachment a : listAttachments) {
            String size = null;

            if(1048576 < a.BodyLength){
                // Size greater than 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength){
                // Size greater than 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else{
                size = '' + a.BodyLength + ' bytes';
            }
            mapAttachmentSize.put(a.id, size);
        }
    }
   


public pageReference save()
{   

       /*Custom Validation Rules are handled here*/  
      if( (null == l1visaStartDate) ||('' == l1visaStartDate) || (null == l1visaEndDate) ||('' == l1visaEndDate))
        {
            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
        }
        if( (null == l2visaStartDate) ||('' == l2visaStartDate) || (null == l2visaEndDate) ||('' == l2visaEndDate))
        {
            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
        }
        if( (null == eadstart) ||('' == eadstart) || (null == eadexpire) ||('' == eadexpire))
        {
            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
        }
        if( (null == applicationdate) ||('' == applicationdate) )
        {
            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
        }
        if(null == Immigrationrecord.WCT_EAD_Status__c)
        {
            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
        }

        /*ImmigrationRecord.WCT_Immigration_Status__c = L2dropDownValue;
        ImmigrationRecord.L1_Visa_Status__c = L1dropDownValue;
        Immigrationrecord.L1_Visa_Type__c = 'L1';*/
    
        //Set Record Type
        String immigrationRecordTypeId = Schema.SObjectType.WCT_Immigration__c.RecordTypeInfosByName.get('L2 with EAD').RecordTypeId;
        Immigrationrecord.RecordTypeid = immigrationRecordTypeId;
        //Update Dates
        if(String.isNotBlank(l2visaStartDate)){
            ImmigrationRecord.WCT_Visa_Start_Date__c = Date.parse(l2visaStartDate);
        }
        if(String.isNotBlank(l2visaEndDate)){
            ImmigrationRecord.WCT_Visa_Expiration_Date__c = Date.parse(l2visaEndDate);
        }
        if(String.isNotBlank(l1visaStartDate)){
            ImmigrationRecord.L1_Visa_Start_Date__c= Date.parse(l1visaStartDate);
        }
        if(String.isNotBlank(l1visaEndDate)){
            ImmigrationRecord.L1_Visa_Expiration_Date__c = Date.parse(l1visaEndDate);
        }
        if(String.isNotBlank(eadstart)){
            ImmigrationRecord.WCT_EAD_Valid_On__c= Date.parse(eadstart);
        }
        if(String.isNotBlank(eadexpire)){
            ImmigrationRecord.WCT_EAD_Card_Expires_On__c = Date.parse(eadexpire);
        }
        if(String.isNotBlank(applicationdate)){
            ImmigrationRecord.WCT_Request_Initiation_Date__c= Date.parse(applicationdate);
        }
        

         if(ImmigrationRecord.WCT_Visa_Expiration_Date__c < ImmigrationRecord.WCT_Visa_Start_Date__c)
        {
            pageErrorMessage = 'Visa Expiration Date cannot be less than Visa Start Date.';
            pageError = true;
            return null;
        }
        
           if(ImmigrationRecord.L1_Visa_Expiration_Date__c<ImmigrationRecord.L1_Visa_Start_Date__c)
        {
            pageErrorMessage = 'Visa Expiration Date cannot be less than Visa Start Date.';
            pageError = true;
            return null;
        }
        
           if(ImmigrationRecord.WCT_EAD_Card_Expires_On__c<ImmigrationRecord.WCT_EAD_Valid_On__c)
        {
            pageErrorMessage = 'EAD Card Expiration Date cannot be less than Valid Date.';
            pageError = true;
            return null;
        }
        ImmigrationRecord.WCT_Assignment_Owner__c=LoggedInContact.id;
        
        /*Changing the Owner Id of Task Temporarily to avoid Integrity Exception*/
        t.OwnerId=UserInfo.getUserId();
        upsert t;



    
        //insert immigration record.
        Map<string,id> mapUserId = new Map<string,id>(); 
        List<User> lstAssignedUser = [select id,name from user where name in('GM & I','Fragomen')];
        for(User tmpUser : lstAssignedUser ){
            mapUserId.put(tmpUser.name,tmpUser.id);
        } 



       
        /* Upload Documents as Related Attachments to Case After the Case Record is created */
        uploadRelatedAttachment();
        //checking for no attachment conditions
       //checking for no attachment conditions
          if(countattach<=0)
            {
                pageErrorMessage = 'Attachment is required to submit the form.';
                pageError = true;
                return null;
            }

        ImmigrationRecord.ownerId=mapUserId.get('GM & I'); 
        ImmigrationRecord.WCT_Immigration_Status__c='Visa Approved';
        ImmigrationRecord.WCT_Visa_Status__c='Visa Approved';
        insert ImmigrationRecord ;
        
        //updating mobility record.
        MobilityRecord.Immigration__c=ImmigrationRecord.id;
        update MobilityRecord;

        //updating task record
        If (t.WCT_Auto_Close__c == true){   
            t.status = 'Completed';
        }else{
            t.status = 'Employee Replied';  
        }
        
        if(ownerIdStr.startsWith('00G'))
            t.OwnerId = mapUserId.get('GM & I');
        else
            t.OwnerId=ownerIdStr;

        t.WCT_Is_Visible_in_TOD__c=false; 
        upsert t;
       
          getAttachmentInfo();
        //  /*Set Page Reference After Save
          return new PageReference('/apex/WCT_L2_with_EAD_FormThankYou?em='+CryptoHelper.encrypt(LoggedInContact.Email)+'&taskid='+taskid) ;  
       //  return new PageReference('/apex/WCT_todPractionerDetails?id='+ImmigrationRecord.id+'taskid='+taskid) ;  
             
    
}
private void uploadRelatedAttachment(){

    if(!docIdList.isEmpty()) { //If there is any Documents Attached in Web Form

            List<String> selectedDocumentId = new List<String>();
            for(AttachmentsWrapper aWrapper : UploadedDocumentList) {
                if(aWrapper.isSelected) {
                    selectedDocumentId.add(aWrapper.documentId);
                }
            }
            
      //  /* Select Documents which are Only Active (Selected) 
            List<Document> selectedDocumentList = new List<Document>();
            if(!selectedDocumentId.isEmpty()){
                selectedDocumentList = [
                                           SELECT 
                                               id,
                                               name,
                                               ContentType,
                                               Type,
                                               Body 
                                           FROM 
                                               Document 
                                           WHERE 
                                               id IN :selectedDocumentId
                                        ];
            }
            
           //  Convert Documents to Attachment 
            List<Attachment> attachmentsToInsertList = new List<Attachment>();
            for(Document doc : selectedDocumentList){
                Attachment a = new Attachment();
                a.body = doc.body;
                a.ContentType = doc.ContentType;
                a.Name= doc.Name;
                a.Description=String.valueof(taskid);
                a.parentid = taskid; //immigration Record Id which is been retrived.
                attachmentsToInsertList.add(a);                
            }
            countattach=attachmentsToInsertList.size();
            if(!attachmentsToInsertList.isEmpty()){
                insert attachmentsToInsertList;
            }
            
            List<Document> listDocuments = new List<Document>();
            listDocuments = [
                                SELECT
                                    Id
                                FROM
                                    Document
                                WHERE
                                    Id IN :docIdList
                            ];
            if( (null != listDocuments) && (0 < listDocuments.size()) ) {
                delete listDocuments;
            }
        }
    }

    //Invoked when Upload Button in VF page is clicked and the IDs are stored in the docIdList
    //All the Files Uploaded are stored in the Documents. Documents does not require parent Id.This method is used to circumvent to upload
    //  the documents first and then add as Attachment to the Cases (Related List)

    public void uploadAttachment(){
        pageError = false;
        pageErrorMessage = '';
    
        if(ApexPages.hasMessages()) {
            pageErrorMessage = '';
            for(ApexPages.Message message : ApexPages.getMessages()) {
                pageErrorMessage += message.getSummary() + '\n';
            }            
            doc = new Document();
            pageError = true;
            return;
        }                                
        
        doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
        if(doc.body != null) {
            insert doc;
            docIdList.add(doc.id);
            doc = [SELECT id, name, bodylength FROM Document WHERE id = :doc.id];

            String size = '';
            if(1048576 < doc.BodyLength) {
                // Size greater than 1MB
                size = '' + (doc.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < doc.BodyLength) {
                // Size greater than 1KB
                size = '' + (doc.BodyLength / 1024) + ' KB';            
            }
            else {
                size = '' + doc.BodyLength + ' bytes';
            }
            UploadedDocumentList.add(new AttachmentsWrapper(true, doc.name, doc.id, size));
            doc = new Document();
        }
    }       
    

//Documents Wrapper
public class AttachmentsWrapper {
public Boolean isSelected {get;set;}
public String docName {get;set;}
public String documentId {get;set;}
public String size {get; set;}
        
 public AttachmentsWrapper(Boolean selected, String Name, String Id, String size){
  isSelected = selected;
  docName = Name ;
  documentId = Id;
  this.size = size;
   }        
 }
 }