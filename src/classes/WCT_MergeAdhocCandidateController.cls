/***********
Class Name:WCT_MergeAdhocCandidateController
Description:This controller merge the Adhoc Candidate with the Candidate Record
Author:Deloitte
**********/

public class WCT_MergeAdhocCandidateController {

    Public String MergeContactId{get;set;}  
    Public Id MasterContactId = ApexPages.currentPage().getParameters().get('id');

    public WCT_MergeAdhocCandidateController() {
        MasterContactId = ApexPages.currentPage().getParameters().get('id');
    }   
    
    public PageReference MergeAdhoc() {
    
        List <WCT_Candidate_Requisition__c> lstAdhocCandidateTrackers= new List<WCT_Candidate_Requisition__c> ();
        List<Case> lstAdhocCases=new List<Case>();
        List<Task> lstAdhocTasks=new List<Task>();
        List<WCT_Candidate_Documents__c> lstAdhocCandidateDocs= new List<WCT_Candidate_Documents__c>();
        List<Note> lstAdhocNotes=new List<Note>();
        List<Attachment> lstAdhocAttachments=new List<Attachment>();
        List<Contact> lstContacts=new List <Contact>();
        Contact mergeCon = new Contact();
        
        Id IdMergeContact=MergeContactId.subString(0, 15);
        if(IdMergeContact <> null){
            mergeCon = [SELECT id,Email,recordtypeID,Phone,Degree_Type__c,CR_Expected_Grad_Month__c,CR_Expected_Grad_Year__c,
                                CR_Undergraduate_Overall_GPA_4_00_Scale__c,TRM_correspondingFSS__c,TRM_serviceArea__c,CR_ACT__c,
                                CR_Average_Events_Feedback_Rating__c,CR_Closed_List_Outcome__c,CR_Contact_Category_Ranking__c,
                                CR_Dropped_Resume__c,CR_GMAT__c,CR_Graduate_GPA__c,GRE_Post_8_1_2011__c,GRE_Pre_8_1_2011__c,
                                CR_High_Potential_Program_Peer__c,WCT_Hiring_Partner__c,WCT_State_of_Hiring_Location__c,
                                WCT_Internship_Start_Date__c,LeadSource,TRM_leadSourceDetail__c,Location_Preference__c,CR_Other_GPA__c,
                                CR_Program_Interest_for_Interview__c,CR_Program_Interest__c,SAT_Quantitative__c,SAT_Total__c,SAT_Verbal__c,
                                SAT_Writing__c,CR_School_Recruiter__c,School_Program__c,TRM_Source_Detail_Name__c,WCT_Start_Date__c,
                                CR_Undergrad_Major__c,CR_Undergrad_Other_Major__c,CR_Undergraduate_Major_GPA_4_00_Scale__c 
                                                       from Contact where id=:IdMergeContact];  
        }
            
        Contact MaseterContact=[select WCT_Ad_Hoc_Contact__c from Contact where id=:MasterContactId limit 1];
                MaseterContact.WCT_Ad_Hoc_Contact__c=IdMergeContact;
                MaseterContact.Degree_Type__c=mergeCon.Degree_Type__c;
                MaseterContact.CR_Expected_Grad_Month__c=mergeCon.CR_Expected_Grad_Month__c;
                MaseterContact.CR_Expected_Grad_Year__c=mergeCon.CR_Expected_Grad_Year__c;
                MaseterContact.CR_Undergraduate_Overall_GPA_4_00_Scale__c=mergeCon.CR_Undergraduate_Overall_GPA_4_00_Scale__c;
                MaseterContact.TRM_correspondingFSS__c=mergeCon.TRM_correspondingFSS__c;
                MaseterContact.TRM_serviceArea__c=mergeCon.TRM_serviceArea__c;
                MaseterContact.CR_ACT__c=mergeCon.CR_ACT__c;
                MaseterContact.CR_Closed_List_Outcome__c=mergeCon.CR_Closed_List_Outcome__c;
                MaseterContact.CR_Contact_Category_Ranking__c=mergeCon.CR_Contact_Category_Ranking__c;
                MaseterContact.CR_Dropped_Resume__c=mergeCon.CR_Dropped_Resume__c;
                MaseterContact.CR_GMAT__c=mergeCon.CR_GMAT__c;
                MaseterContact.CR_Graduate_GPA__c=mergeCon.CR_Graduate_GPA__c;
                MaseterContact.GRE_Post_8_1_2011__c=mergeCon.GRE_Post_8_1_2011__c;
                MaseterContact.GRE_Pre_8_1_2011__c=mergeCon.GRE_Pre_8_1_2011__c;
                MaseterContact.CR_High_Potential_Program_Peer__c=mergeCon.CR_High_Potential_Program_Peer__c;
                
                /*
                *  @Commented by  - Piramanayagam(Jeevan Team)
                *  @Date - 11/7/2014
                *  @Reason - Field value not populating after merge in Candidate record
                
                MaseterContact.Phone=mergeCon.Phone;
                MaseterContact.WCT_Hiring_Partner__c=mergeCon.WCT_Hiring_Partner__c;
                MaseterContact.WCT_State_of_Hiring_Location__c=mergeCon.WCT_State_of_Hiring_Location__c;
                MaseterContact.WCT_Internship_Start_Date__c=mergeCon.WCT_Internship_Start_Date__c;
                MaseterContact.WCT_Start_Date__c=mergeCon.WCT_Start_Date__c;
                */
                
                MaseterContact.LeadSource=mergeCon.LeadSource;                
                MaseterContact.TRM_leadSourceDetail__c=mergeCon.TRM_leadSourceDetail__c;
                MaseterContact.Location_Preference__c=mergeCon.Location_Preference__c;
                MaseterContact.CR_Other_GPA__c=mergeCon.CR_Other_GPA__c;
                MaseterContact.CR_Program_Interest_for_Interview__c=mergeCon.CR_Program_Interest_for_Interview__c;
                MaseterContact.CR_Program_Interest__c=mergeCon.CR_Program_Interest__c;
                MaseterContact.SAT_Quantitative__c=mergeCon.SAT_Quantitative__c;
                MaseterContact.SAT_Total__c=mergeCon.SAT_Total__c;
                MaseterContact.SAT_Verbal__c=mergeCon.SAT_Verbal__c;
                MaseterContact.SAT_Writing__c=mergeCon.SAT_Writing__c;
                MaseterContact.CR_School_Recruiter__c=mergeCon.CR_School_Recruiter__c;
                MaseterContact.School_Program__c=mergeCon.School_Program__c;
                MaseterContact.TRM_Source_Detail_Name__c=mergeCon.TRM_Source_Detail_Name__c;
                MaseterContact.CR_Undergrad_Major__c=mergeCon.CR_Undergrad_Major__c;
                MaseterContact.CR_Undergrad_Other_Major__c=mergeCon.CR_Undergrad_Other_Major__c;
                MaseterContact.CR_Undergraduate_Major_GPA_4_00_Scale__c=mergeCon.CR_Undergraduate_Major_GPA_4_00_Scale__c;
            lstContacts.add(MaseterContact);    
        
        Map<Id,Contact> mapCandidatesWithAdhocCandidate= new Map<Id,Contact>();
        mapCandidatesWithAdhocCandidate.put(IdMergeContact,MaseterContact);
        
        if(!mapCandidatesWithAdhocCandidate.isempty()){
            Database.update(lstContacts,false);
            if(new ContactTriggerHandler_AT().MergeAdhocCandidate(mapCandidatesWithAdhocCandidate,true)){
               return new pagereference('/'+MasterContactId); 
            }
            else{
                return  new PageReference('/apex/WCT_Exception_Page');
            }                                                     
        }
        else
         return null;

    }
               
 
    public PageReference Cancel() {
        PageReference pageRef = new PageReference('/' +MasterContactId);
        pageRef.setRedirect(true);
        return pageRef;
    }

}