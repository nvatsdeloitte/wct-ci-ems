@isTest
public class WCT_MobClosedTaskEmailSchedBatch_Test
{

    static testMethod void Test_WCT_MobClosedTaskEmailSchedulableBatch()
    {
        Test.startTest();
  
        WCT_Mobility__c mobRec = new WCT_Mobility__c();
        mobRec.WCT_Total_Family_Size_at_Home__c = 2;
        mobRec.WCT_Mobility_Status__c = 'New';
        insert mobRec ;
        
        Contact ct = WCT_UtilTestDataCreation.createEmployee(WCT_Util.getRecordTypeIdByLabel('Contact','Employee'));
        insert ct;
        Set<Id> stagingIds = new Set<id>();
        List<EmailTemplate> lstEmailTemplate = [select id, developerName from EmailTemplate]; 
        
        WCT_Task_Reference_Table__c taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        taskRef.WCT_Visa_Type__c = 'WCT_Mobility__c.RecordType';
        insert taskRef;
    
        Task t1= new Task();
        t1.Subject='call';
        t1.WCT_Notification_1_Template_API_Name__c = lstEmailTemplate[0].developerName  ;
        t1.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t1.WhatId=mobRec.Id;
        t1.whoId=ct.id;     
        // t1.ActivityDate =today();
        t1.Status='Completed';
        t1.WCT_ToD_Task_Reference__c='0046';
        t1.WCT_GMI_Closed_Task_Notification_Date__c = Date.today();
        t1.recordTypeId = WCT_Util.getRecordTypeIdByLabel('Task','Mobility');
        t1.WCT_Is_Closed_Task_Notification__c = true;
        insert t1;
        
        Task t2= new Task();
        t2.Subject='call';
        t2.WCT_Notification_1_Template_API_Name__c = lstEmailTemplate[0].developerName  ;
        t2.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t2.WhatId=mobRec.Id;
        t2.whoId=ct.id;     
        t2.Status='Completed';
        t2.WCT_ToD_Task_Reference__c='0051';
        t2.WCT_GMI_Closed_Task_Notification_Date__c = Date.today();
        t2.recordTypeId = WCT_Util.getRecordTypeIdByLabel('Task','Mobility');
        t2.WCT_Is_Closed_Task_Notification__c = true;
        insert t2;
       
         
       
        WCT_MobClosedTaskEmailSchedulableBatch createCon = new WCT_MobClosedTaskEmailSchedulableBatch();
        Task task = new Task();
        system.schedule('New','0 0 2 1 * ?',createCon); 
    
        Test.stopTest();   
        

    }
     
}