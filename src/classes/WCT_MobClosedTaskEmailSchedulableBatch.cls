global class WCT_MobClosedTaskEmailSchedulableBatch  implements Database.Batchable<sObject>,Schedulable, Database.Stateful{
    
    global void execute(SchedulableContext SC) {
        WCT_MobClosedTaskEmailSchedulableBatch batch = new WCT_MobClosedTaskEmailSchedulableBatch();
        ID batchprocessid = Database.executeBatch(batch,200); 
    }
   
    global Database.QueryLocator start(Database.BatchableContext bc){
// Get current date        
       date dt =  Date.today();   
       string currentDate = string.valueof(dt); 
       
// Get complete tasks with notification on         
       string strSql ='select WCT_ToD_Task_Reference__c,subject ,whoId,who.name,who.email,Owner.email,whatId,what.name,ActivityDate, WCT_Notification_1__c,  ' +
        ' WCT_Notification_1_Template_API_Name__c,WCT_Business_Owner__c,' +
        ' WCT_R10_Resource_Manager__c,WCT_Resource_Manager_Email_ID__c,WCT_USI_Resource_Manager__c,WCT_US_Project_Mngr__c ' +
        ' from task ' +
        ' where RecordType.Name = \'Mobility\' and Status = \'Completed\' and whoId <> null'+
        ' and WCT_GMI_Closed_Task_Notification_Date__c <=' + currentDate +' and WCT_Is_Closed_Task_Notification__c = true';    
       return database.getQuerylocator(strSql);
    }
    
    global void execute(Database.batchableContext bc, List<sObject> scope){
    // Get all email templates
        List<EmailTemplate> lstEmailTemplate = [select id, developerName from EmailTemplate];
        
    // Create map of email template API name and its id
        Map<string,id> mapApiNameId = new Map<string,id>();
        for(EmailTemplate tmpEmailTemp : lstEmailTemplate ){
            mapApiNameId.put(tmpEmailTemp.developerName,tmpEmailTemp.id);
        }    
        Task tmpTask;
        List<Task> lstTask = new List<Task>();
        Messaging.singleEmailMessage mail;
        List<Messaging.singleEmailMessage> lstSendEmail = new List<Messaging.singleEmailMessage>();
        List<OrgWideEmailAddress> lstOWE = [SELECT Address,DisplayName,Id FROM OrgWideEmailAddress where DisplayName = 'GMI Team'];        
        List<string> lstEmail;
        
         for(sObject tmpMobTask : scope) { 
            lstEmail = new List<String>(); 
            tmpTask = new Task();
            tmpTask = (Task)tmpMobTask ;
            tmpTask.WCT_GMI_Closed_Task_Notification_Date__c=null;
            lstTask.add(tmpTask);
            string strEmailTemplateId;
            
            if(tmpTask.whoId !=null && tmpTask.WCT_Notification_1_Template_API_Name__c !=null){
        // Check notification date is current date and template exists            
                if(tmpTask.WCT_Notification_1_Template_API_Name__c !=null)
                    strEmailTemplateId = mapApiNameId.get(tmpTask.WCT_Notification_1_Template_API_Name__c);
        //    Prepare email object
                    mail = new Messaging.singleEmailMessage();
                    mail.setTemplateId(strEmailTemplateId);
                    
        // Check record type of task for populating to addresses
                    /*if(tmpTask.WCT_Resource_Manager_Email_ID__c !=null)
                        lstEmail.add(tmpTask.WCT_Resource_Manager_Email_ID__c);
                    if(tmpTask.WCT_Business_Owner__c !=null)    
                        lstEmail.add(tmpTask.WCT_Business_Owner__c);
                    if(tmpTask.WCT_R10_Resource_Manager__c !=null)    
                        lstEmail.add(tmpTask.WCT_R10_Resource_Manager__c);
                    if(tmpTask.WCT_USI_Resource_Manager__c !=null)    
                        lstEmail.add(tmpTask.WCT_USI_Resource_Manager__c);
                    if(tmpTask.WCT_US_Project_Mngr__c !=null)    
                        lstEmail.add(tmpTask.WCT_US_Project_Mngr__c);*/
                    //mail.setTargetObjectId(tmpTask.whoId);
                   
                    if(tmpTask.WCT_ToD_Task_Reference__c=='0046' || tmpTask.WCT_ToD_Task_Reference__c=='0052' )
                    {
                            mail.setTargetObjectId(label.WCT_DND_USI_9SupportCenterMailbox);
                            mail.setWhatId(tmpTask.whatId);
                            
                    }
                        
                    if(tmpTask.WCT_ToD_Task_Reference__c=='0051')
                    {
                        mail.setTargetObjectId(Label.WCT_DND_useleseparations);
                        List<String> pLIst=new List<String>();
                        pLIst.add(label.WCT_DND_USIIndiaExitManagement);
                        
                        mail.setCcAddresses(pLIst) ;
                        mail.setWhatId(tmpTask.whatId);
                    }
                   
                  //  if(!lstEmail.isEmpty())    
                   //     mail.setCCAddresses(lstEmail);
                    mail.setSaveAsActivity(false);
                    mail.setWhatId(tmpTask.whatId);
                    if(!lstOWE.isEmpty())              
                        mail.setOrgWideEmailAddressId(lstOWE.get(0).id) ;                   
                    lstSendEmail.add(mail);
               }     
         }
        Messaging.SendEmailResult[] mailResults = Messaging.sendEmail(lstSendEmail);
        Database.SaveResult[] sr = Database.update(lstTask, false);      
    }
    
    global void finish(Database.batchableContext info){     
   
    } 

}