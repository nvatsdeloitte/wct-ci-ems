public class WCT_Mob_Two_Checkbox_LCA_FormController extends SitesTodHeaderController{
    public task taskRecord{get;set;}
    public WCT_Task_Reference_Table__c taskRefRecord{get;set;}
    public String taskSubject {get;set;}
    public String taskid{get;set;}
    public boolean display{get;set;}
    public boolean validLCA{get;set;}
    public boolean newLCA{get;set;}
    public String taskVerbiage{get;set;}
    
     /* public variables */
    public WCT_Mobility__c MobilityRec {get;set;}
    public WCT_Assignment__C AssignRec {get;set;}
    
    // Error Message related variables
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}
    
    public WCT_Mob_Two_Checkbox_LCA_FormController()
    {
         /*Initialize all Variables*/
        init();
        
        taskid = ApexPages.currentPage().getParameters().get('taskid');
        display=true;
        validLCA=false;
        newLCA=false;
        taskSubject = '';
        taskVerbiage = '';
    }

    private void init(){

        //Custom Object Instances
        MobilityRec = new WCT_Mobility__c();
        AssignRec = new WCT_Assignment__c();

    }   
    public void updateTaskFlags()
    {
        if(taskid==''|| taskid==null){
            display=false;
            return;
        }

        taskRecord=[SELECT id, Subject, WhatId, Ownerid, Status, WCT_Is_Visible_in_TOD__c, WCT_Auto_Close__c, WCT_Task_Reference_Table_ID__c FROM Task WHERE Id =: taskid];
        taskRefRecord = [SELECT Id, Name, WCT_Task_for_Object__c, WCT_Task_Subject__c, Form_Verbiage__c, ownerId FROM WCT_Task_Reference_Table__c Where id=:taskRecord.WCT_Task_Reference_Table_ID__c];
        MobilityRec = [SELECT Id, Name, OwnerId FROM WCT_Mobility__c WHERE Id=:taskRecord.WhatId];
        AssignRec = [Select Id, Name, WCT_Status__c, WCT_Mobility__c FROM WCT_Assignment__c WHERE WCT_Mobility__c =: MobilityRec.Id AND (WCT_Status__c = 'Active' OR WCT_Status__c = 'New')];

        //Get Task Subject
        taskSubject = taskRecord.Subject;
        taskVerbiage = taskRefRecord.Form_Verbiage__c;

    }
    
    public pageReference save()
    {

        /*Custom Validation Rules are handled here*/

        if(newLCA != true && validLCA != true)
        {
            pageErrorMessage = 'Please check any one of the checkboxes in order to submit the form.';
            pageError = true;
            return null;
        }
        if(newLCA == true && validLCA == true)
        {
            pageErrorMessage = 'Please check any ONE checkbox in order to submit the form.';
            pageError = true;
            return null;
        }
        if(newLCA == true)
            AssignRec.WCT_Assignment_LCA_Status__c = 'Applied for a New LCA';
        else if(validLCA == true)
            AssignRec.WCT_Assignment_LCA_Status__c = 'Hold a valid LCA';

        update MobilityRec;
        update AssignRec;
            
        /*Changing the Owner Id of Task Temporarily to avoid Integrity Exception*/
        taskRecord.OwnerId=UserInfo.getUserId();
        upsert taskRecord;

        if(string.valueOf(MobilityRec.OwnerId).startsWith('00G')){
            taskRecord.OwnerId = System.Label.GMI_User;
        }else{
            taskRecord.OwnerId = MobilityRec.Ownerid;
        }
        

        //updating task record
        if(taskRecord.WCT_Auto_Close__c == true){   
            taskRecord.status = 'Completed';
        }else{
            taskRecord.status = 'Employee Replied';  
        }

        taskRecord.WCT_Is_Visible_in_TOD__c = false; 
        upsert taskRecord;
        
        //Reset Page Error Message
        pageError = false;
        
        /*Set Page Reference After Save*/
        return new PageReference('/apex/WCT_Mob_Two_Checkbox_LCA_FormThankYou?em='+CryptoHelper.encrypt(LoggedInContact.Email)+'&taskid='+taskid);  
    }
}