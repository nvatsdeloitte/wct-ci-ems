/*
Class name : WCT_Mobility_Mass_Close_Controller 
Description : This class is controller for WCT_Mobility_Mass_Close page.

Task number     Date            Modified By                         Description
------------    ---------       ---------------------               ----------------------
                6-OCT-14        Sunil Kati                      Created
                                                            
*/
public class WCT_Mobility_Mass_Close_Controller {
    public String selectedViewBy{get;set;}
    public String selectedStatus{get;set;}
    public String AssignmentTypeValue{get;set;}
    public String MobilityTypeValue{get;set;}
    public Date firstWorkingDay{set;get;}
    public Date firstWorkingDayTo{set;get;}
    public Date lastWorkingDay{set;get;}
    public Date lastWorkingDayTo{set;get;}
    public Date travelStartDate{set;get;}
    public Date travelStartDateTo{set;get;}
    public Date travelEndDate{set;get;}
    public Date travelEndDateTo{set;get;}
    //add two variable for from date and to date
    public Date FromDate{set;get;}
    public Date ToDate{set;get;}
    
    public List<WCT_Mobility__c> lstMobilityRecord{get;set;}
    public List<mobilityWrapper> showAllRecordList{get;set;}
    public list<SelectOption> AssignmentTypeValueOptions {get;set;}
    public list<SelectOption> MobilityTypeValueOptions {get;set;}
    public boolean isassignmenttypeVisible {get; set;}
    public boolean firstlastWorkingDayVisible {get; set;}
    public boolean travelStartEndDateVisible {get; set;}
    public List<TaskWrapper> showAllTaskList{get;set;}
    public List<taskListWrapper> showAllTaskListWrapper{get;set;}
    public List<Task> finalTask{get;set;}
    
    public WCT_Mobility_Mass_Close_Controller (){
        showAllRecordList = new List<MobilityWrapper>();
        getassignmentTypes();
        getmobilityTypes();
        isassignmenttypeVisible = True;
        firstlastWorkingDayVisible = true;
        travelStartEndDateVisible = false;
        showAllTaskList = new List<TaskWrapper>();
        showAllTaskListWrapper = new List<taskListWrapper>();
    }

    /*public void getmobilitystatus() {
        selectoptions = new list<SelectOption>();
        selectoptions.add(new selectOption('New', 'New'));
        selectoptions.add(new selectOption('Initiate Onboarding', 'Initiate Onboarding'));
        selectoptions.add(new selectOption('New', 'New'));
    }*/


    public void getmobilityTypes(){
        //new list for holding all of the picklist options   
        MobilityTypeValueOptions = new List<SelectOption>(); 
        
        MobilityTypeValueOptions.add(new SelectOption('Employment Visa','Employment Visa'));
        MobilityTypeValueOptions.add(new SelectOption('Business Visa','Business Visa'));
        MobilityTypeValue = 'Employment Visa';
    }

    public void getassignmentTypes(){
        //new list for holding all of the picklist options   
        AssignmentTypeValueOptions = new List<SelectOption>();
        AssignmentTypeValueOptions.add(new SelectOption('All', 'All')); 
        AssignmentTypeValueOptions.add(new SelectOption('Short Term', 'Short Term'));
        AssignmentTypeValueOptions.add(new SelectOption('Medium Term', 'Medium Term'));
        AssignmentTypeValueOptions.add(new SelectOption('Long Term', 'Long Term'));
        AssignmentTypeValue = 'All';
    }
   /* public List<SelectOption> getmobilitystatus(){
        selectoptions = new list<SelectOption>();
        selectoptions.add(new selectOption('New', 'New'));
        selectoptions.add(new selectOption('Initiate Onboarding', 'Initiate Onboarding'));
        selectoptions.add(new selectOption('Onboarding Completed', 'Onboarding Completed'));
        selectoptions.add(new selectOption('Ready for Travel', 'Ready for Travel'));
        selectoptions.add(new selectOption('Deployment on Hold', 'Deployment on Hold'));
        selectoptions.add(new selectOption('Deployment Cancelled', 'Deployment Cancelled'));
        selectoptions.add(new selectOption('Employee On Assignment', 'Employee On Assignment'));
        selectoptions.add(new selectOption('Employee On Assignment: Assignment Extended', 'Employee On Assignment: Assignment Extended'));
        selectoptions.add(new selectOption('OEmployee On Assignment: Rollover', 'Employee On Assignment: Rollover'));
        selectoptions.add(new selectOption('Employee on Travel', 'Employee on Travel'));
        selectoptions.add(new selectOption('Ready for Departure', 'Ready for Departure'));
        selectoptions.add(new selectOption('Employee On Assignment: Separated', 'Employee On Assignment: Separated'));
        selectoptions.add(new selectOption('Repatriation completed', 'Repatriation completed'));
        selectoptions.add(new selectOption('Travel Completed', 'Travel Completed'));
        selectoptions.add(new selectOption('Closed', 'Closed'));
        selectoptions.add(new selectOption('Permanent Transfer to US firm', 'Permanent Transfer to US firm'));
        selectoptions.add(new selectOption('Converted to Long Term Assignment', 'Converted to Long Term Assignment'));
        selectoptions.add(new selectOption('Converted from Short term to Medium Term', 'Converted from Short term to Medium Term'));
        selectoptions.add(new selectOption('Converted from Medium term to Short Term', 'Converted from Medium term to Short Term'));
        selectoptions.add(new selectOption('Employee On Assignment : Rollover – Assignment Extended', 'Employee On Assignment : Rollover – Assignment Extended'));
        
        return selectoptions;
    }*/
     public List<SelectOption> getmobilitystatus(){
        
        //new list for holding all of the picklist options   
        List<SelectOption> options = new List<SelectOption>(); 
        String first_picklist_option = '-No Status Selected-';
        
        if (first_picklist_option != null ){                                                //if there is a first value being provided
            options.add(new selectOption(first_picklist_option, first_picklist_option));     //add the first option
        }
        
            //grab the sobject that was passed
        Schema.sObjectType sobject_type = WCT_Mobility__c.getSObjectType();
         
            //describe the sobject
        Schema.DescribeSObjectResult sobject_describe = WCT_Mobility__c.getSObjectType().getDescribe();
         
            //get a map of fields for the passed sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
         
            //grab the list of picklist values for the passed field on the sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get('WCT_Mobility_Status__c').getDescribe().getPickListValues();
          
            //for all values in the picklist list add the value and label to our final list
        for (Schema.PicklistEntry a : pick_list_values) { 
            options.add(new SelectOption(a.getLabel(), a.getValue()));  
        }
 
        return options;
    }

    public pagereference displaydependentfilters(){

        if(MobilityTypeValue == 'Employment Visa'){
            isassignmenttypeVisible = true;
            firstlastWorkingDayVisible = true;
            travelStartEndDateVisible = false;
        }
        else if(MobilityTypeValue == 'Business Visa'){ 
            isassignmenttypeVisible = false;  
            firstlastWorkingDayVisible = false;  
            travelStartEndDateVisible = true;
        }
        
        system.debug('Test----- :'+ isassignmenttypeVisible  + MobilityTypeValue);       
        return null;
    }

    //-----------------------------------------------------------------
    //   Wrapper Class
    //-----------------------------------------------------------------
    public class mobilityWrapper{
        public WCT_Mobility__c mobilityRecord{get;set;}
        public List<TaskWrapper> lstOfTask{get;set;}
        public Boolean isThere{get;set;}
        public mobilityWrapper(WCT_Mobility__c wctMobility, List<TaskWrapper> tskWrapper){
            
            mobilityRecord = wctMobility;
            lstOfTask = tskWrapper;
            if(lstOfTask.isEmpty()){
                isThere = false;
            }else isThere = true;
            
        }
    }
    public class taskWrapper{
        
        public String Task_Type{get;set;}
        public List<taskListWrapper> taskRec{get;set;}
        
        public taskWrapper(String taskType, List<taskListWrapper> taskListWrapper){
            Task_Type = taskType;
            taskRec = taskListWrapper;
        }
    }
    
    public class taskListWrapper{
        public Boolean check{get;set;}
        public String Status{get;set;}
        public String Subject{get;set;}
        public String Id{get;set;}
        public String AssignedTo{get;set;}
        public Task taskRecord{get;set;}
        
        public taskListWrapper(Boolean flag, String taskStatus, String taskSubject, String taskId, String assigned, Task tRecord){
            check = flag;
            Status = taskStatus;
            Subject = taskSubject;
            Id = taskId;
            AssignedTo = assigned;
            taskRecord = tRecord;
            
        }
    } 
     
    
    //----------------------------------------------------------------------------------------------------
    //    Get a list of Mobility Records and corresponding Tasks in the Table on the click of Search
    //----------------------------------------------------------------------------------------------------
    public PageReference getListTasks() {
        showAllRecordList = new List<mobilityWrapper>();
        Map<WCT_Mobility__c, Map<String,List<Task>>> MobilityMainMap = new Map<WCT_Mobility__c ,Map<String,List<Task>>>();
        Map<String,List<Task>> taskTypeMap = new Map<String,List<Task>>();
        List<String> statusValuesList = new List<String>();
        statusValuesList.add('Completed');
        statusValuesList.add('Cancelled');
        String queryWhereClause = '';
        String MobilityTypeQuery = '';
        String assignmentTypeQuery = '';
        String firstWorkingDayQuery = '';
        String firstWorkingDayToQuery = '';
        String lastWorkingDayQuery = '';
        String lastWorkingDayToQuery = '';
        //Add two variables
        String FromDateQuery = '';
        String ToDateQuery = '';
        
        String conRecType = 'Employee';
        String query ='';
        String AssignedToUser;
        
        Id FragomenId;
        FragomenId = Label.Fragomen_UserID;
        
        if(MobilityTypeValue =='Employment Visa' && firstWorkingDay != null)
            firstWorkingDayQuery = ' AND WCT_First_Working_Day_in_US__c =: firstWorkingDay';
        else if(MobilityTypeValue =='Business Visa' && travelStartDate != null)
            firstWorkingDayQuery = ' AND WCT_Travel_Start_Date__c =: travelStartDate'; 
            
            //Add  firstWorkingDayTo
        if(MobilityTypeValue =='Employment Visa' && firstWorkingDay != null)
            firstWorkingDayQuery = ' AND WCT_First_Working_Day_in_US__c >=: firstWorkingDay';
        else if(MobilityTypeValue =='Business Visa' && travelStartDate != null)
            firstWorkingDayQuery = ' AND WCT_Travel_Start_Date__c >=: travelStartDate';   
        
        if(MobilityTypeValue =='Employment Visa' && firstWorkingDayTo != null)
            firstWorkingDayToQuery = ' AND WCT_First_Working_Day_in_US__c <=: firstWorkingDayTo';
        else if(MobilityTypeValue =='Business Visa' && travelStartDateTo != null)
            firstWorkingDayToQuery = ' AND WCT_Travel_Start_Date__c <=: travelStartDateTo';  
        
        ////////////////////////////
        
        
        
        if(MobilityTypeValue =='Employment Visa' && lastWorkingDay != null)
            lastWorkingDayQuery = ' AND WCT_Last_Working_Day_in_US__c >=: lastWorkingDay';
        else if(MobilityTypeValue =='Business Visa' && travelEndDate != null)
            lastWorkingDayQuery = ' AND WCT_Travel_End_Date__c >=: travelEndDate';  
            
            //Add Lastworking date To
        if(MobilityTypeValue =='Employment Visa' && lastWorkingDayTo != null)
            lastWorkingDayToQuery = ' AND WCT_Last_Working_Day_in_US__c <=: lastWorkingDayTo';
        else if(MobilityTypeValue =='Business Visa' && travelEndDate != null)
            lastWorkingDayToQuery = ' AND WCT_Travel_End_Date__c <=: travelEndDateTo';  
            
            
            
            
        if(AssignmentTypeValue == 'Short Term' || AssignmentTypeValue == 'Medium Term' || AssignmentTypeValue == 'Long Term')
            assignmentTypeQuery = ' AND WCT_Assignment_Type__c =: AssignmentTypeValue';
        else if(AssignmentTypeValue == 'All')
            assignmentTypeQuery = '';
            
        // Add from date and to date to query
     /*    if(FromDate != null)
            FromDateQuery = ' AND WCT_From_Date__c >=: FromDate';
        else
            FromDateQuery = '';
        
        if(ToDate != null)
            ToDateQuery = ' AND WCT_To_Date__c <=: ToDate';
        else
            ToDateQuery = '';   */
            
                
        
        system.debug('@@@____'+query+'____'+AssignmentTypeValue+'____'+MobilityTypeValue);
        if(MobilityTypeValue =='Employment Visa'){            
            
            //AssignmentTypeQuery = 'AND WCT_Assignment_Type__c=:AssignmentTypeValue ';
            query = 'SELECT Id, Name,WCT_From_Date__c,WCT_To_Date__c,WCT_Employee_Name__c, WCT_Mobility_Status__c,WCT_First_Working_Day_in_US__c,WCT_Last_Working_Day_in_US__c, RecordType.Name, OwnerId, (SELECT Id, Status, Task_Type__c, Subject, WhoId, OwnerId, WCT_Is_Visible_in_TOD__c, WCT_Is_Closed_By_Override__c FROM Tasks WHERE status!=:statusValuesList  order by createdDate) FROM WCT_Mobility__c WHERE WCT_Mobility_Status__c =:selectedStatus AND WCT_Mobility_Status__c != null AND WCT_Mobility_Type__c =:MobilityTypeValue AND WCT_Mobility_Type__c != null' + assignmentTypeQuery + firstWorkingDayQuery + firstWorkingDayToQuery + lastWorkingDayQuery + lastWorkingDayToQuery;
        }
        if(MobilityTypeValue =='Business Visa'){
            query = 'SELECT Id, Name,WCT_Employee_Name__c, WCT_Mobility_Status__c,WCT_Travel_Start_Date__c,WCT_Travel_End_Date__c,RecordType.Name, OwnerId, (SELECT Id, Status, Task_Type__c, Subject, WhoId, OwnerId, WCT_Is_Visible_in_TOD__c, WCT_Is_Closed_By_Override__c FROM Tasks WHERE status!=:statusValuesList  order by createdDate) FROM WCT_Mobility__c WHERE WCT_Mobility_Status__c =:selectedStatus AND WCT_Mobility_Status__c != null AND WCT_Mobility_Type__c =:MobilityTypeValue AND WCT_Mobility_Type__c != null' + firstWorkingDayQuery + firstWorkingDayToQuery + lastWorkingDayQuery + lastWorkingDayToQuery;
        }
        system.debug('@@@____'+query+'____'+AssignmentTypeValue +'____'+MobilityTypeValue);
        lstMobilityRecord= database.query(query);
        
        if(lstMobilityRecord.size() >0){
            for(WCT_Mobility__c mobilityRecord : lstMobilityRecord){
                
                taskTypeMap = new Map<String,List<Task>>();
                
                if(!(mobilityRecord.tasks).IsEmpty()){
                    for(Task taskRec: mobilityRecord.tasks){
                        if(!taskTypeMap.ContainsKey(taskRec.Task_Type__c)){
                            taskTypeMap.put(taskRec.Task_Type__c, new list<Task>());
                        }  
                        taskTypeMap.get(taskRec.Task_Type__c).add(taskRec);                     
                    }        
                }
                MobilityMainMap.put(mobilityRecord, taskTypeMap);      
            }
        }
        for(WCT_Mobility__c mobilityRec : MobilityMainMap.KeySet()){        
            if(MobilityMainMap.get(mobilityRec)!=null){
                showAllTaskList = new List<TaskWrapper>();
                
                //for(String str: MobilityMainMap.get(mobilityRec).KeySet()){
                    for(List<Task> taskRecList: MobilityMainMap.get(mobilityRec).Values()){
                        String str;
                        showAllTaskListWrapper = new List<taskListWrapper>();
                        for(Task taskRec: taskRecList){
                            if(taskRec.whoID!=null)
                                AssignedToUser = 'Employee';
                            else if(taskRec.whoID==null && taskRec.ownerId!=Label.Fragomen_UserID)
                                AssignedToUser = 'GM&I';
                            else
                                AssignedToUser = 'Fragomen';
                                
                            showAllTaskListWrapper.add(new taskListWrapper(false, taskRec.Status, taskRec.Subject, taskRec.Id, AssignedToUser, taskRec));    
                            str = taskRec.Task_Type__c;   
                          
                        }   
                        showAllTaskList.add(new TaskWrapper(str, showAllTaskListWrapper));       
                    }
                    
                //}
                
                // if(!showAllTaskList.IsEmpty()){
                showAllRecordList.add(new mobilityWrapper(mobilityRec, showAllTaskList));
                //}                
            }
        }
        return null;
    }
    public PageReference massClose(){ 
        finalTask = new List<Task>();  

        for(mobilityWrapper i: showAllRecordList){
            for(TaskWrapper recTask: i.lstOfTask){
                for(taskListWrapper LWrap: recTask.taskRec){
                    if(LWrap.check == true){
                        finalTask.add(LWrap.taskRecord);
                    }
                }
            }         
        }

        for(Task t : finalTask){
            t.WCT_Is_Visible_in_TOD__c = false;
            t.WCT_Is_Closed_By_Override__c = true;
            t.Status = 'Completed';
        }
        Database.update(finalTask);        

        Database.update(lstMobilityRecord);
        getListTasks();
        return null;
    }
    public PageReference checkAll(){
        return null;
    }
    
}