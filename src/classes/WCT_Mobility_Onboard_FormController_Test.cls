@isTest
private class WCT_Mobility_Onboard_FormController_Test {

    static testMethod void myUnitTest() {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        rt=[select id from recordtype where DeveloperName = 'Business_Visa'];
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');

        PageReference pageRef = Page.WCT_Mobility_Onboarding_Form;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);     
        ApexPages.currentPage().getParameters().put('type','Employee');
       
        datetime startdate=date.Today().adddays(10);
        datetime enddate=date.Today().adddays(20);
        
        Test.starttest();
        WCT_Mobility_Onboarding_FormController controller=new WCT_Mobility_Onboarding_FormController();
        ApexPages.currentPage().getParameters().put('type','Business');
        controller=new WCT_Mobility_Onboarding_FormController();
        controller.init();
 //       controller.redirectBusiness();
      //  controller.redirectPOE();
        controller.employeeEmail='';
        controller.saveMobilityRecord();
        controller.MobilityRecord.WCT_Notes__c = 'Hi this is test comments.';
        controller.MobilityRecord.WCT_USI_Resource_Manager__c='test@testing.com';
        controller.MobilityRecord.WCT_Purpose_of_Travel__c='Business Meetings/Trainings';
        controller.MobilityRecord.WCT_Deloitte_US_Office_City__c = 'Texas';
        controller.MobilityRecord.WCT_Deloitte_US_Office_State__c = 'Dallas';
        
        controller.MobilityRecord.WCT_Deloitte_US_Office_City2__c = 'Tennesse';
        controller.MobilityRecord.WCT_Deloitte_US_Office_State2__c = 'New york';
        controller.MobilityRecord.WCT_Deloitte_US_Office_City3__c = 'Chicago';
        controller.MobilityRecord.WCT_Deloitte_US_Office_State3__c = 'New Jersey';
        
        controller.MobilityRecord.WCT_Deloitte_US_Office_City4__c = 'Ohio';
        controller.MobilityRecord.WCT_Deloitte_US_Office_State4__c = 'Canada';
        
        controller.MobilityRecord.WCT_Deloitte_US_Office_City5__c = 'California';
        controller.MobilityRecord.WCT_Deloitte_US_Office_State5__c = 'San Francisco';
        
        controller.BusinessVisa='Yes';
        controller.MobilityRecord.WCT_Existing_Business_Visa__c = 'Yes';
        controller.selectedDropDownValue=String.valueOf(rt.id);
        controller.travelstartdate =enddate.format('MM/dd/yyyy');
        controller.travelenddate=startdate.format('MM/dd/yyyy');
        controller.saveMobilityRecord();
        controller.visastart=enddate.format('MM/dd/yyyy');
        controller.visaexpiration=startdate.format('MM/dd/yyyy');
        controller.saveMobilityRecord();
        controller.saveMobilityRecord();
        controller.travelstartdate=startdate.format('MM/dd/yyyy');
        controller.travelenddate =enddate.format('MM/dd/yyyy');
        controller.MobilityRecord.WCT_traveling_as_per_the_date_mentioned__c = 'No';
        controller.MobilityRecord.WCT_GivenName__c = 'GMI Guest';
        controller.MobilityRecord.WCT_Sur_Name__c = 'User';
        controller.MobilityRecord.WCT_Assignment_Type__c ='';
        
        controller.getEmployeeDetails();
        controller.employeeEmail=con.email;
        controller.getEmployeeDetails();
       
        controller.saveMobilityRecord(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        controller.employeeEmail='gmiuatmobility@test.com';
        try{
             controller.getEmployeeDetails();
        }
        catch(Exception e)
        {}
        controller.invalidEmployee=true;
        controller=new WCT_Mobility_Onboarding_FormController();
        Test.stoptest();
    }
}