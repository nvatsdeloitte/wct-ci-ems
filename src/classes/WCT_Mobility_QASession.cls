public class WCT_Mobility_QASession {

//Our collection of the class/wrapper objects cContact 
    public List<cContact> contactList {get; set;}
    public List<Contact> selectedContacts;
    public List<wct_mobility__c> selectedMobility;
    public Boolean allBool {get; set;}
    
    public Event_Registration_Attendance__c eventRegObj;
    public Integer selectedSize;
    
    Set<Id> sObjectIds = new Set<Id>();
    
    
    /* *   item in context from the page */
    public String contextItem{get;set;}
    
    
    
    List<Event_Registration_Attendance__c> eventRegistrationsToCreate = new List<Event_Registration_Attendance__c>();
    Map<Id,Event_Registration_Attendance__c> eventRegistrationsExisting = new Map<Id,Event_Registration_Attendance__c>();
    Map<Id,Id> MobilityMap = new Map<Id,Id>();
    Map<Id,String> MobilityTypeMap = new Map<Id,String>();
    /*    set controller */
    public ApexPages.StandardSetController setCon;
    
    /*  the contact ids selected by the user */
    private Set<Id> selectedContactIds = new Set<Id>();
    
    
    
    public Integer getSelectedSize() {
        if(selectedSize == null) selectedSize = 0;
        return selectedSize;
    }
    
    public Event_Registration_Attendance__c geteventRegObj() {
        if(eventRegObj == null) eventRegObj = new Event_Registration_Attendance__c();
        return eventRegObj;
    }
    
    
    //This method uses a simple SOQL query to return a List of Contacts
    public List<cContact> getContacts() {
        if(contactList == null) {
            contactList = new List<cContact>();
            set<String> ContactId = new set<String>();
        for(wct_mobility__c  con : [select id,Name,WCT_Mobility_Type__c,WCT_Mobility_Status__c,WCT_Mobility_Employee__r.Id,WCT_Mobility_Employee__r.Name from wct_mobility__c where (WCT_Mobility_Status__c IN ('Onboarding Completed','Ready for Travel') AND WCT_Q_A_Session_attended__c=true AND WCT_Visa_Brief_Session__c=false AND WCT_First_Working_Day_in_US__c > TODAY AND recordtype.name = 'Employment Visa') OR ((WCT_Mobility_Status__c IN ('Onboarding Completed','Ready for Travel') AND WCT_Q_A_Session_attended__c=true AND WCT_Visa_Brief_Session__c=false AND WCT_Travel_Start_Date__c > TODAY AND recordtype.name = 'Business Visa'))]) {
                ContactId.add(con.WCT_Mobility_Employee__r.Id);
                contactList.add(new cContact(con));
        }
        
      }
        
        return contactList;
    }
    
    public PageReference processSelected() {

        //We create a new list of Contacts that we be populated only with Contacts if they are selected
        selectedContacts = new List<Contact>();
        selectedMobility = new List<wct_mobility__c>();
        
        Set<Id> sConIds = new Set<Id>();
        
        //We will cycle through our list of cContacts and will check to see if the selected property is set to true, if it is we add the Contact to the selectedContacts list
        for(cContact cCon: getContacts()) {
            if(cCon.selected == true) {
                selectedMobility.add(cCon.con);
            }
        }
        
        for(wct_mobility__c  con : [select id,Name,WCT_Mobility_Type__c,WCT_Mobility_Status__c,WCT_Mobility_Employee__r.Id,WCT_Mobility_Employee__r.Name from wct_mobility__c where id=:selectedMobility]) {
               sConIds.add(con.WCT_Mobility_Employee__r.Id);
               MobilityMap.put(con.WCT_Mobility_Employee__r.Id,con.id);
               MobilityTypeMap.put(con.WCT_Mobility_Employee__r.Id,con.WCT_Mobility_Type__c);
               
        }
        for(Contact c: [select id, FirstName, LastName, Title, Phone,name,email from Contact where id=:sConIds]) {
                // As each contact is processed we create a new cContact object and add it to the contactList
                selectedContacts.add(c);
            }
        
        
       selectedSize = selectedContacts.size();
      
       //If no contacts are selected, display an error message.
       if(selectedSize==0){  
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least one contact'));        
       } 
       
        for(sObject s:selectedContacts) 
            sObjectIds.add(s.id);
       
        Pagereference p=Page.WCT_Mobility_AddToEvent;
        p.setRedirect(false);
        return p;
    }


    // This is our wrapper/container class. A container class is a class, a data structure, or an abstract data type whose instances are collections of other objects. In this example a wrapper class contains both the standard salesforce object Contact and a Boolean value
    public class cContact {
        public WCT_mobility__c con {get; set;}
        public Boolean selected {get; set;}

        //This is the contructor method. When we create a new cContact object we pass a Contact that is set to the con property. We also set the selected value to false
        public cContact(WCT_mobility__c c) {
            con = c;
            selected = false;
        }
    }
    
      //This method adds the selected contacts as Event Registrants.
    public pagereference AddeventRegistrations(){
     //Fetch existing Event Registrants with the same contact/lead and Recruiting Event Ids.
      for(Event_Registration_Attendance__c c:[Select Id,Contact__c,recordtypeid,WCT_MobilityName__c from Event_Registration_Attendance__c where Contact__c in:sObjectIds and Event__c =: eventRegObj.Event__c])
             eventRegistrationsExisting.put(c.Contact__c,c);
      
      for(sObject con:selectedContacts){
        Event_Registration_Attendance__c er = new Event_Registration_Attendance__c();
        if(eventRegistrationsExisting.containsKey(con.Id)){
          er = eventRegistrationsExisting.get(con.id);
                   
        }else{
          er.Event__c = eventRegObj.Event__c;
          er.Contact__c = con.Id;
          er.recordtypeid = System.Label.Event_Registration_GMI_Record_type;
          er.WCT_MobilityName__c = MobilityMap.get(con.Id);
          er.WCT_Mobiity_Type__c = MobilityTypeMap.get(con.Id);
          er.WCT_Is_Mobility_record__c = true;
          
        }         
         eventRegistrationsToCreate.add(er);
      }
      try{ 
        //Update or Insert campaign members
       upsert eventRegistrationsToCreate;       
      }catch(Exception e){
        ApexPages.addMessages(e);
        return null;
      } 
      
      //Redirect to the campaign detail page.
      Pagereference p= new Pagereference('/'+eventRegObj.Event__c);
      p.setRedirect(true);
      return p;
    }
    
    public void selectAll(){
        
        if(allBool) {
            for(cContact w : contactList) {
                w.selected = true;
            }
        } else {
            for(cContact w : contactList) {
                w.selected = false;
            }
        }
    }
    
     /* *   handle item selected */
    public void doSelectItem(){
         this.selectedContactIds.add(this.contextItem);
    }
    
    /* *   handle item deselected*/
    public void doDeselectItem(){
        this.selectedContactIds.remove(this.contextItem);
    }
    
    /*  return count of selected items */
  /*  public Integer getSelectedCount(){
        return this.selectedContactIds.size();
    }*/
    
    /* *   advance to next page*/
 /*  public void doNext(){
        if(this.setCon.getHasNext())
            this.setCon.next();
    }*/
       
    /* *   advance to previous page */
   /* public void doPrevious(){
        if(this.setCon.getHasPrevious())
            this.setCon.previous();
    }*
    
    
    /* *   return whether previous page exists */
   /* public Boolean getHasPrevious(){
        return this.setCon.getHasPrevious();
    }*/
    
    /* *   return whether next page exists */
   /* public Boolean getHasNext(){
        return this.setCon.getHasNext();
    }*/
    
    /* *   return page number  */
  /*  public Integer getPageNumber(){
        return this.setCon.getPageNumber();
    }*/
    
    /* *    return total pages */
  /*  Public Integer getTotalPages(){
        Decimal totalSize = this.setCon.getResultSize();
        Decimal pageSize = this.setCon.getPageSize();
        Decimal pages = totalSize/pageSize;
        
        return (Integer)pages.round(System.RoundingMode.CEILING);
    }*/


}