public class WCT_Mobility_Request_FormController extends SitesTodHeaderController{

    /* public variables */
    public WCT_Mobility__c MobilityRecord{get;set;} 

    // Error Message related variables
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    
    /* Upload Related Variables */
     public Document doc {get;set;}
     public List<String> docIdList = new List<string>();
     public List<AttachmentsWrapper> UploadedDocumentList {get; set; }
     public List<Attachment> listAttachments {get; set;}
     public Map<Id, String> mapAttachmentSize {get; set;} 

    //Controller Related Variables
    public String employeeType;
    public Contact Employee{get;set;}
    public Map<String,String> recordTypeMap {get;set;}
    public String selectedDropDownValue {get;set;}
    public String firstworkingday {get;set;}
    public String lastworkingday {get;set;}
    public String travelstartdate{get;set;}
    public String travelenddate{get;set;}
    public String VisaStart {get;set;}
    public String VisaExpiration {get;set;}
    public List<SelectOption> BusinessVisaOptions{set; get;}
    public String BusinessVisa{set; get;}
    public List<SelectOption> L2withEADOptions{set; get;}
    public String L2withEAD{set; get;}
    public String employeeEmail{set;get;}
    public boolean employeePresent{set;get;}
    public boolean employeePrevious{set;get;}

    public WCT_Mobility_Request_FormController(){
        init();
        getParameterInfo();
        getMobilityTypeDropDownValues();

        if(invalidEmployee)
        {
           return;
        }
    }

    public void init(){
        MobilityRecord= new WCT_Mobility__c();
        Employee=new Contact();
        employeeType = '';
        employeePresent=false;
        employeePrevious=false;
        recordTypeMap = new Map<String,String>();
        BusinessVisaOptions=new List<SelectOption>();
        BusinessVisaOptions.add(new SelectOption('Yes','Yes'));
        BusinessVisaOptions.add(new SelectOption('No','No'));
        BusinessVisaOptions.add(new SelectOption('Green Card/ US Citizen','Green Card/ US Citizen'));
        L2withEADOptions=new List<SelectOption>();
        L2withEADOptions.add(new SelectOption('None','None'));
        L2withEADOptions.add(new SelectOption('Yes','Yes'));
        L2withEADOptions.add(new SelectOption('No','No'));
        BusinessVisa='No';
        L2withEAD='None';
        //Document Related Init
        doc = new Document();
        UploadedDocumentList = new List<AttachmentsWrapper>();
        listAttachments = new List<Attachment>();
        mapAttachmentSize = new Map<Id, String>();   
        
        }

    private void getParameterInfo(){
        employeeType = ApexPages.currentPage().getParameters().get('type');
    }

    public list<SelectOption> getMobilityTypeDropDownValues(){

        List<SelectOption> options = new List<SelectOption>();

        for(RecordType rt: [SELECT ID, Name FROM RecordType WHERE sObjectType = 'WCT_Mobility__c']){

            //Create Record Map
            recordTypeMap.put(rt.ID, rt.Name);

            if(String.isNotEmpty(employeeType)){

                if(employeeType == 'Business'){
                    if(rt.Name == 'Business Visa'){
                        options.add(new SelectOption(rt.ID, rt.Name));
                        mobilityRecord.recordTypeId = rt.Id;
                    }                    
                }

                if(employeeType == 'Employee'){
                    if(rt.Name == 'Employment Visa'){                    
                        options.add(new SelectOption(rt.ID, rt.Name));
                        mobilityRecord.recordTypeId = rt.Id;
                    }
                }
            }
        }
        return options;
    }
    public pageReference getEmployeeDetails()
    {
        //ID employeeRecordtypeID=WCT_Util.getRecordTypeIdByLabel('Contact','Employee'); 
        try{
            recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
            employeeEmail=employeeEmail.trim();
            if((employeeEmail==null)||(employeeEmail==''))
            {
                pageErrorMessage = 'Please enter a email ID';
                pageError = true;
                employee=new contact();
                employeePresent=false;
                return null;
            }
            else
            {
                Employee=[select id,name from contact where recordtypeid=:rt.id and email =: employeeEmail limit 1];
                employeePresent=true;   
            }
        }
        catch(Exception e)
        {
            WCT_ExceptionUtility.logException('WCT_Mobility_Request_FormController','Get Employee Details',e.getMessage());
            pageErrorMessage = 'Email Id is not associated with any employee';
            pageError = true;
            employee=new contact();
            employeePresent=false;
            return null;
        }
        pageError = false;
        return null;
    }
    public pageReference saveMobilityRecord()
    { 

        //Check for Validation
        if( (null == MobilityRecord.WCT_USI_Report_Mngr__c) || 
            ('' == MobilityRecord.WCT_USI_Report_Mngr__c) ||
            (null == mobilityRecord.WCT_USI_RCCode__c) || 
            ('' == mobilityRecord.WCT_USI_RCCode__c) ||
            ('' == MobilityRecord.WCT_USI_Resource_Manager__c) ||
            (null == MobilityRecord.WCT_USI_Resource_Manager__c) ||
            ('' == MobilityRecord.WCT_Purpose_of_Travel__c) ||
            (null == MobilityRecord.WCT_Purpose_of_Travel__c)
            ){

                pageErrorMessage = 'Please fill in all the required fields on the form.';
                pageError = true;
                return null;
        }
        MobilityRecord.WCT_Existing_Business_Visa__c=BusinessVisa;
        
        if(MobilityRecord.WCT_Existing_Business_Visa__c == 'Yes')
        {
            if((null == VisaExpiration ) || ('' == VisaExpiration ) ||
               (null == VisaStart) || ('' == VisaStart) )
            {
                pageErrorMessage = 'Please fill in all the required fields on the form.';
                pageError = true;
                return null;
            }
            if(String.isNotBlank(VisaStart)){
                MobilityRecord.WCT_Visa_start_date__c= Date.parse(VisaStart);
            }
            if(String.isNotBlank(VisaExpiration )){
                MobilityRecord.WCT_Visa_expiration_date__c= Date.parse(VisaExpiration );
            }
            
            MobilityRecord.WCT_Previous_Employer_Visa__c= employeePrevious;
            
            if(MobilityRecord.WCT_Visa_start_date__c > MobilityRecord.WCT_Visa_expiration_date__c) {
                pageErrorMessage = 'Visa End Date cannot be before the Visa Start Date. Please correct.';
                pageError = true;
                return null;
            }
        }
        RecordType rt1 = [SELECT ID, Name FROM RecordType WHERE sObjectType = 'WCT_Mobility__c'and id=:mobilityRecord.recordTypeId];
        if(rt1.Name == 'Employment Visa'){
            if(('None' != L2withEAD) ||
                (null != firstworkingday ) || ('' != firstworkingday ) ||
                (null != lastworkingday ) || ('' != lastworkingday )){       
                MobilityRecord.WCT_EmployeeTravel__c=L2withEAD;
                if(String.isNotBlank(firstworkingday )){
                    MobilityRecord.WCT_First_Working_Day_in_US__c = Date.parse(firstworkingday );
                }
                if(String.isNotBlank(lastworkingday )){
                    MobilityRecord.WCT_Last_Working_Day_in_US__c = Date.parse(lastworkingday );
                }
                
                if(MobilityRecord.WCT_First_Working_Day_in_US__c > MobilityRecord.WCT_Last_Working_Day_in_US__c) {
                        pageErrorMessage = 'The Last Working Day in the US cannot be before the First Working Day in the US. Please correct.';
                        pageError = true;
                        return null;
                }
                if(MobilityRecord.WCT_Last_Working_Day_in_US__c < system.today() || MobilityRecord.WCT_First_Working_Day_in_US__c < system.today()) {
                        pageErrorMessage = 'The First Working Day in US and Last Working Day in the US has to be a future date. Please correct.';
                        pageError = true;
                        return null;
                }
                
            }
            else
            {
                 pageErrorMessage = 'Please fill in all the required fields on the form.';
                 pageError = true;
                 return null;
            }
       }
       if(rt1.Name == 'Business Visa'){
            if( (null != travelstartdate) || ('' != travelstartdate) ||
                (null != travelenddate) || ('' != travelenddate)){       
                if(String.isNotBlank(travelstartdate)){
                    MobilityRecord.WCT_Travel_Start_Date__c = Date.parse(travelstartdate);
                }
                if(String.isNotBlank(travelenddate)){
                    MobilityRecord.WCT_Travel_End_Date__c = Date.parse(travelenddate);
                }
                
                if(MobilityRecord.WCT_Travel_Start_Date__c > MobilityRecord.WCT_Travel_End_Date__c ) {
                        pageErrorMessage = 'Travel End Date cannot be before the Travel Start Date. Please correct.';
                        pageError = true;
                        return null;
                }
                
            }
            else
            {
                 pageErrorMessage = 'Please fill in all the required fields on the form.';
                 pageError = true;
                 return null;
            }
       }
        //Update Record Type based on the type of Leaves Selected.
        if(selectedDropDownValue != ''){
            MobilityRecord.RecordTypeId = selectedDropDownValue;
        }

        //Set Employee Information
        MobilityRecord.WCT_Mobility_Employee__c = Employee.id;

        insert MobilityRecord;
        
        /* Upload Documents as Related Attachments to Case After the Case Record is created */
        uploadRelatedAttachment();        
        pageError = false;
        return new PageReference('/apex/WCT_Mobility_Request_Form_ThankYou?id=' + MobilityRecord.id+'&em='+CryptoHelper.encrypt(LoggedInContact.Email)); //
    }
    private void uploadRelatedAttachment(){
  
    if(!docIdList.isEmpty()) { //If there is any Documents Attached in Web Form

            List<String> selectedDocumentId = new List<String>();
            for(AttachmentsWrapper aWrapper : UploadedDocumentList) {
                if(aWrapper.isSelected) {
                    selectedDocumentId.add(aWrapper.documentId);
                }
            }
            
        /* Select Documents which are Only Active (Selected) */
            List<Document> selectedDocumentList = new List<Document>();
            if(!selectedDocumentId.isEmpty()){
                selectedDocumentList = [
                                           SELECT 
                                               id,
                                               name,
                                               ContentType,
                                               Type,
                                               Body 
                                           FROM 
                                               Document 
                                           WHERE 
                                               id IN :selectedDocumentId
                                        ];
            }
            
            /* Convert Documents to Attachment */
            List<Attachment> attachmentsToInsertList = new List<Attachment>();
            for(Document doc : selectedDocumentList){
                Attachment a = new Attachment();
                a.body = doc.body;
                a.ContentType = doc.ContentType;
                a.Name= doc.Name;
                a.Description=String.valueof(mobilityrecord.id);
                a.parentid = mobilityrecord.id; //immigration Record Id which is been retrived.
                attachmentsToInsertList.add(a); 
                               
            }
            if(!attachmentsToInsertList.isEmpty()){
                insert attachmentsToInsertList;
            }
            
            List<Document> listDocuments = new List<Document>();
            listDocuments = [
                                SELECT
                                    Id
                                FROM
                                    Document
                                WHERE
                                    Id IN :docIdList
                            ];
            if( (null != listDocuments) && (0 < listDocuments.size()) ) {
                delete listDocuments;
            }
        }
    }

    /*Invoked when Upload Button in VF page is clicked and the IDs are stored in the docIdList*/
    /*All the Files Uploaded are stored in the Documents. Documents does not require parent Id.This method is used to circumvent to upload
      the documents first and then add as Attachment to the Cases (Related List)*/

    public void uploadAttachment(){
        pageError = false;
        pageErrorMessage = '';
    
        if(ApexPages.hasMessages()) {
            pageErrorMessage = '';
            for(ApexPages.Message message : ApexPages.getMessages()) {
                pageErrorMessage += message.getSummary() + '\n';
            }            
            doc = new Document();
            pageError = true;
            return;
        }                                
        
        doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
        if(doc.body != null) {
            insert doc;
            docIdList.add(doc.id);
            doc = [SELECT id, name, bodylength FROM Document WHERE id = :doc.id];

            String size = '';
            if(1048576 < doc.BodyLength) {
                // Size greater than 1MB
                size = '' + (doc.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < doc.BodyLength) {
                // Size greater than 1KB
                size = '' + (doc.BodyLength / 1024) + ' KB';            
            }
            else {
                size = '' + doc.BodyLength + ' bytes';
            }
            UploadedDocumentList.add(new AttachmentsWrapper(true, doc.name, doc.id, size));
            doc = new Document();
        }
    }       
    

 //Documents Wrapper
public class AttachmentsWrapper {
 public Boolean isSelected {get;set;}
 public String docName {get;set;}
 public String documentId {get;set;}
 public String size {get; set;}
        
 public AttachmentsWrapper(Boolean selected, String Name, String Id, String size){
  isSelected = selected;
  docName = Name ;
  documentId = Id;
  this.size = size;
   }        
 }
 
}