@isTest
private class WCT_Mobility_TriggerHandler_Test
{
    public static testmethod void m1()
    {
        Test.startTest();
        WCT_Mobility_TriggerHandler mobTrig = new WCT_Mobility_TriggerHandler();
        
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
        
        WCT_Mobility__c mobRec = new WCT_Mobility__c();
        recordtype rt1=[select id from recordtype where name='Employment Visa'];
        mobRec.RecordTypeId = rt1.id;
        mobRec.WCT_Mobility_Employee__c = con.Id;
        mobRec.WCT_Mobility_Status__c = 'New';
        INSERT mobRec;
        
        Task tempTask = new Task();
        tempTask.WhatId = mobRec.Id;
        tempTask.Status = 'Not Started';
        INSERT tempTask;
        
        mobRec.WCT_Mobility_Status__c = 'Employee on Travel';
        mobRec.WCT_Travel_End_Date_Extended__c = true;
        UPDATE mobRec;
        
        mobRec.WCT_Accompanied_Status__c = true; 
        mobRec.WCT_DependentVisaTask_Created__c = false;
        mobRec.WCT_Mobility_Status__c = 'Onboarding Completed';
        
        
        Update mobRec;
        Test.stopTest();
    }
    public static testmethod void m2()
    {
        Test.startTest();
        WCT_Mobility_TriggerHandler mobTrig = new WCT_Mobility_TriggerHandler();
        
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
        
        WCT_Mobility__c mobRec = new WCT_Mobility__c();
        recordtype rt1=[select id from recordtype where name='Employment Visa'];
        mobRec.RecordTypeId = rt1.id;
        mobRec.WCT_Mobility_Employee__c = con.Id;
        mobRec.WCT_Mobility_Status__c = 'New';
        INSERT mobRec;
        
        Task tempTask = new Task();
        tempTask.WhatId = mobRec.Id;
        tempTask.Status = 'Not Started';
        INSERT tempTask;
        
        mobRec.WCT_Accompanied_Status__c = true; 
        mobRec.WCT_DependentVisaTask_Created__c = false;
        mobRec.WCT_Mobility_Status__c = 'Onboarding Completed';
        
        Update mobRec;
        Test.stopTest();
    }
}