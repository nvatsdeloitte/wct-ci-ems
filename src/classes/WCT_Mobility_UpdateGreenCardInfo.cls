/*
Class name : WCT_Mobility_UpdateGreenCardInfo
Description : This class is controller for WCT_Update_Green_Card_Info page.

Task number     Date            Modified By                         Description
------------    ---------       ---------------------               ----------------------
                24-Jun-14       Darshee Mehta                       Created
                                                            
*/

public class WCT_Mobility_UpdateGreenCardInfo {

    public Id SelectedId; 
    public WCT_Mobility__c mobilityContact{set;get;}
    public Contact contactRecord{set;get;}
    
    public WCT_Mobility_UpdateGreenCardInfo(){
        SelectedID = ApexPages.currentPage().getParameters().get('id'); 
        mobilityContact = new WCT_Mobility__c();
        Set<Id> contactId = new Set<Id>();
        contactRecord = new Contact();
        
        try{
            mobilityContact = [SELECT Id, WCT_Mobility_Employee__c, WCT_Contact_Green_Card_Status__c, WCT_Contact_Green_Card_Expiration_Date__c FROM WCT_Mobility__c WHERE Id=:SelectedID];
        }catch(QueryException queryException){
            WCT_ExceptionUtility.logException('WCT_Mobility_UpdateGreenCardInfo','Mobility Query',queryException.getMessage());
        }
    }
    
    public pageReference saveContactUpdates(){

        try{
            UPDATE mobilityContact;
        }Catch(DmlException dmlException){  
            WCT_ExceptionUtility.logException('WCT_Mobility_UpdateGreenCardInfo','Update Contacts',dmlException.getMessage());
        }
        
        try{
            contactRecord = [SELECT Id, WCT_Green_Card_Status__c, WCT_Green_Card_Expiration_Date__c FROM Contact WHERE Id =: mobilityContact.WCT_Mobility_Employee__c];
        }catch(QueryException queryException){
            WCT_ExceptionUtility.logException('WCT_Mobility_UpdateGreenCardInfo','Contact Query',queryException.getMessage());
        }
        contactRecord.WCT_Green_Card_Expiration_Date__c = mobilityContact.WCT_Contact_Green_Card_Expiration_Date__c;
        contactRecord.WCT_Green_Card_Status__c = mobilityContact.WCT_Contact_Green_Card_Status__c;
        
        try{
            UPDATE contactRecord; 
        }Catch(DmlException dmlException){  
            WCT_ExceptionUtility.logException('WCT_Mobility_UpdateGreenCardInfo','Update Contacts',dmlException.getMessage());
        }
        
        pagereference p = apexpages.Currentpage();
        apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.CONFIRM,'The Contact Information has been updated.');
        apexpages.addmessage(msg);
        return p; 
    }
    
    public pageReference backToMobility(){
        return new pageReference('/'+SelectedID);
    }
}