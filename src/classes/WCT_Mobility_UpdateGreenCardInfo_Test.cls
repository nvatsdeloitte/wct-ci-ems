/**
 * Description : Test class for WCT_Mobility_UpdateGreenCardInfo
 */
 
@isTest

Private class WCT_Mobility_UpdateGreenCardInfo_Test
{
    static testMethod void m1(){
        Test.startTest();
        recordtype recType = [select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con = WCT_UtilTestDataCreation.createEmployee(recType.id);
        INSERT con;
        
        WCT_Mobility__c mobRec = WCT_UtilTestDataCreation.createMobility(con.id);
        mobRec.WCT_Mobility_Employee__c = con.Id;
        mobRec.WCT_Contact_Green_Card_Status__c = 'Active';
        mobRec.WCT_Contact_Green_Card_Expiration_Date__c = System.Today();
        INSERT mobRec;
        
       // con.WCT_Green_Card_Expiration_Date__c = System.Today();
       // Update con;
        
        WCT_Mobility_UpdateGreenCardInfo controller = new WCT_Mobility_UpdateGreenCardInfo();
        controller.saveContactUpdates();
        controller.backToMobility();
        Test.stopTest();
        
    }
}