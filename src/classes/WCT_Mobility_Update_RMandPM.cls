/*************************************************************************************
* Name          :    WCT_Mobility_Update_RMandPM .
* Description   :    Helper Code
* Author        :    Deloitte
 
Modification Log
----------------
Date             Developer                Comments
---------------------------------------------------------------------------------------
6/03/2014       Darshee Mehta              Created


**************************************************************************************/

public class WCT_Mobility_Update_RMandPM{

    public WCT_Mobility_Update_RMandPM(){}
    
    public static void updateRMandPM(List<WCT_Mobility__c > lstNewMobility){
        
        Set<String> setMobilityEmailId = new Set<String>();
        List<Contact> lstContact = new List<Contact>();

        for(WCT_Mobility__c m : lstNewMobility){
            if(m.WCT_USI_Resource_Manager__c!=null)
                setMobilityEmailId.add(m.WCT_USI_Resource_Manager__c);
            if(m.WCT_USI_Report_Mngr__c!=null)
                setMobilityEmailId.add(m.WCT_USI_Report_Mngr__c);
        }
        Map<String,Id> contactIdMap=new Map<String,Id>();
        
        for(Contact contactTempRec : [SELECT Id, Name, Email FROM Contact WHERE RecordType.Name='Employee' AND Email IN: setMobilityEmailId] )
        {
            contactIdMap.put(contactTempRec.Email, contactTempRec.Id);
        }
        
        for(WCT_Mobility__c mobilityRecord : lstNewMobility) {
        
            if(mobilityRecord.WCT_USI_Report_Mngr__c!=null && contactIdMap.ContainsKey(mobilityRecord.WCT_USI_Report_Mngr__c))
                mobilityRecord.WCT_Project_Manager__c = contactIdMap.get(mobilityRecord.WCT_USI_Report_Mngr__c);
            if(mobilityRecord.WCT_USI_Resource_Manager__c!=null && contactIdMap.ContainsKey(mobilityRecord.WCT_USI_Resource_Manager__c))
                mobilityRecord.WCT_Resource_Manager__c = contactIdMap.get(mobilityRecord.WCT_USI_Resource_Manager__c);
            if(mobilityRecord.WCT_USI_Report_Mngr__c == null)
                mobilityRecord.WCT_Project_Manager__c = null;
            if(mobilityRecord.WCT_USI_Resource_Manager__c == null)
                mobilityRecord.WCT_Resource_Manager__c = null;
        }
        
    }
}