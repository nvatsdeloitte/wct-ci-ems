public class WCT_NumberFormatAsString {
	public static String formatDouble(Decimal myNumber, Boolean isCurrencyDollar, Boolean isCurrencyINR){
	String formattedString = '';
	String myNumberAsStringAct = String.valueOf(myNumber);
	String myNumberAsStringNew = myNumberAsStringAct.replace('.','---');
	List<string> myNumberDecimalPart =  myNumberAsStringNew.split('---',2);
	String myNumberAsString ;
	if(myNumberDecimalPart[0]!=Null){
		myNumberAsString = myNumberDecimalPart[0];
	}
	else{
		myNumberAsString = myNumberAsStringAct;
	}
		
	system.debug('-1-'+myNumberDecimalPart[0]);
	system.debug('-1-'+myNumberDecimalPart[1]);
		if (!isCurrencyDollar && isCurrencyINR){
			//formattedString = 'INR';
			if(myNumber>999999999){
				formattedString += myNumberAsString.substring(0,myNumberAsString.length()-9)+ ',' + myNumberAsString.substring(myNumberAsString.length()-9,myNumberAsString.length()-7) +','+ myNumberAsString.substring(0,myNumberAsString.length()-7)+ ',' + myNumberAsString.substring(myNumberAsString.length()-7,myNumberAsString.length()-5) + ',' + myNumberAsString.substring(myNumberAsString.length()-5,myNumberAsString.length()-3) + ',' + myNumberAsString.substring(myNumberAsString.length()-3,myNumberAsString.length());
			}else if(myNumber>9999999){
				formattedString += myNumberAsString.substring(0,myNumberAsString.length()-7)+ ',' + myNumberAsString.substring(myNumberAsString.length()-7,myNumberAsString.length()-5) + ',' + myNumberAsString.substring(myNumberAsString.length()-5,myNumberAsString.length()-3) + ',' + myNumberAsString.substring(myNumberAsString.length()-3,myNumberAsString.length());
			} else if(myNumber>99999){
				formattedString += myNumberAsString.substring(0,myNumberAsString.length()-5)+ ',' + myNumberAsString.substring(myNumberAsString.length()-5,myNumberAsString.length()-3) + ',' + myNumberAsString.substring(myNumberAsString.length()-3,myNumberAsString.length());
			} else if(myNumber>999){
				formattedString += myNumberAsString.substring(0,myNumberAsString.length()-3)+ ',' + myNumberAsString.substring(myNumberAsString.length()-3,myNumberAsString.length());
			} else {
				formattedString += myNumberAsString;
			}
		}
		else if (isCurrencyDollar && !isCurrencyINR){
			//formattedString = '$';
			if(myNumber>999999999){
				formattedString += myNumberAsString.substring(0,myNumberAsString.length()-9)+ ',' + myNumberAsString.substring(myNumberAsString.length()-9,myNumberAsString.length()-6) + ',' + myNumberAsString.substring(myNumberAsString.length()-6,myNumberAsString.length()-3) + ',' + myNumberAsString.substring(myNumberAsString.length()-3,myNumberAsString.length());
			} else if(myNumber>999999){
				formattedString += myNumberAsString.substring(0,myNumberAsString.length()-6)+ ',' + myNumberAsString.substring(myNumberAsString.length()-6,myNumberAsString.length()-3) + ',' + myNumberAsString.substring(myNumberAsString.length()-3,myNumberAsString.length());
			} else if(myNumber>999){
				formattedString += myNumberAsString.substring(0,myNumberAsString.length()-3)+ ',' + myNumberAsString.substring(myNumberAsString.length()-3,myNumberAsString.length());
			} else {
				formattedString += myNumberAsString;
			}
		}
		if((isCurrencyDollar && !isCurrencyINR) && (myNumberDecimalPart[1]!=Null)){
			string DecVal;
			if(myNumberDecimalPart[1].length()==1){DecVal = myNumberDecimalPart[1];DecVal += '0';}
			else{DecVal = myNumberDecimalPart[1];}
			formattedString+='.'+DecVal;
		}
	return formattedString;
	}
}