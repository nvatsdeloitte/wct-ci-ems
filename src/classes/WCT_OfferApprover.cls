public with sharing class WCT_OfferApprover {
String OfferId;
	public WCT_OfferApprover(){
		    OfferId = System.currentPagereference().getParameters().get('id');
	}
	public PageReference RedirectToInterview(){
		WCT_Offer__c upOffStatus = new WCT_Offer__c (Id = OfferId);
		upOffStatus.WCT_status__c = 'Offer Approved';
		update upOffStatus;
		return new PageReference('/apex/WCT_OfferApproverThankYou');
	}
}