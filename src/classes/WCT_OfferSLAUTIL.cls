public class WCT_OfferSLAUTIL {

//Variable Declaration
public static boolean isBeforeTrigExecuted = false; 
Public static final Set<String> CLOSED_STATUS = new Set<String>{'Offer Sent','Offer Accepted','Offer Declined','Offer Revoked','Offer Cancelled'};

    //Method to Insert Offer Status Insert on creation of Offer 
    public static void createOfferStateOnOfferInsert(List<WCT_Offer__c> offerList){
        List<WCT_Offer_Status__c> offerStatesToInsert = new List<WCT_Offer_Status__c>();
        for(WCT_Offer__c offer : offerList){
            WCT_Offer_Status__c tempState = new WCT_Offer_Status__c();
            tempState.WCT_Enter_State_Date_Time__c = system.Now();
            tempState.WCT_Offer_Current_Status__c = offer.WCT_status__c;
            tempState.WCT_Related_Offer__c = offer.id;
            tempState.WCT_Status__c = 'Open';
            
            //Add the offer state record to a list
            offerStatesToInsert.add(tempState);
        }
        
        if(!offerStatesToInsert.isEmpty()){
            DataBase.SaveResult[] srList = DataBase.Insert(offerStatesToInsert,false);
        }
    
    }
    
    //Method to Insert/Update Offer Status Records on Update of offer Status
    public static void createAndUpdateOfferStateRecords(Map<Id,WCT_Offer__c> offerOldMap , List<WCT_Offer__c> newOfferList){
        if(!isBeforeTrigExecuted){
            Map<Id,List<WCT_Offer_Status__c>> offerwithOfferStateMap = new Map<Id,List<WCT_Offer_Status__c>>();
            List<WCT_Offer_Status__c> offerStatesToCreate = new List<WCT_Offer_Status__c>();
            List<WCT_Offer_Status__c> offerStatesToUpdate = new List<WCT_Offer_Status__c>();
            
            for(WCT_Offer_Status__c offStatus : [SELECT id,WCT_Enter_State_Date_Time__c,WCT_Leave_State_Date_Time__c,WCT_Offer_Current_Status__c,
                                                 WCT_Offer_Previous_Status__c,WCT_Related_Offer__c,WCT_Status__c,WCT_Related_Offer__r.id FROM WCT_Offer_Status__c WHERE WCT_Related_Offer__c IN :offerOldMap.keySet() ORDER BY createdDate DESC]){
                if(offerwithOfferStateMap.ContainsKey(offStatus.WCT_Related_Offer__c)){
                    List<WCT_Offer_Status__c> tempList = new List<WCT_Offer_Status__c>();
                    tempList = offerwithOfferStateMap.get(offStatus.WCT_Related_Offer__c);
                    tempList.add(offStatus);
                    offerwithOfferStateMap.put(offStatus.WCT_Related_Offer__c,tempList);
                }else{
                    List<WCT_Offer_Status__c> tempList = new List<WCT_Offer_Status__c>();
                    tempList.add(offStatus);
                    offerwithOfferStateMap.put(offStatus.WCT_Related_Offer__c,tempList);
                }
            }
            
            for(WCT_Offer__c offer : newOfferList){
                //Check if status Changed
                if(offer.WCT_status__c != offerOldMap.get(offer.id).WCT_status__c){
                    WCT_Offer_Status__c tempRec = new WCT_Offer_Status__c();
                    if(offerwithOfferStateMap.ContainsKey(offer.id)){
                        tempRec = offerwithOfferStateMap.get(offer.id)[0];
                        tempRec.WCT_Leave_State_Date_Time__c = system.Now();
                        if(tempRec.WCT_Status__c == 'Open'){
                            tempRec.WCT_Status__c = 'Closed';
                        }
                        Decimal seconds = BusinessHours.diff(Label.WCT_Default_Business_Hours_ID, tempRec.WCT_Enter_State_Date_Time__c, tempRec.WCT_Leave_State_Date_Time__c)/ 1000;
                        Decimal hrs = seconds / 3600;
                        tempRec.WCT_SLA_TIme_in_Status__c = hrs.setScale(3);                        
                        offerStatesToUpdate.add(tempRec);
                    }
                    
                    //Provide condition for which new record need to be created
                    if(!CLOSED_STATUS.contains(offer.WCT_status__c)){
                        WCT_Offer_Status__c tempState = new WCT_Offer_Status__c();
                        tempState.WCT_Enter_State_Date_Time__c = system.Now();
                        tempState.WCT_Offer_Current_Status__c = offer.WCT_status__c;
                        tempState.WCT_Offer_Previous_Status__c = offerOldMap.get(offer.id).WCT_status__c;
                        tempState.WCT_Related_Offer__c = offer.id;
                        if(offer.WCT_Waiting_for_inputs_from__c <> null){
                            tempState.WCT_Waiting_for_inputs_from__c = offer.WCT_Waiting_for_inputs_from__c;
                        }
                        tempState.WCT_Status__c = 'Open';
                        offerStatesToCreate.add(tempState);
                    }else{
                        WCT_Offer_Status__c tempState = new WCT_Offer_Status__c();
                        tempState.WCT_Enter_State_Date_Time__c = system.Now();
                        tempState.WCT_Offer_Current_Status__c = offer.WCT_status__c;
                        tempState.WCT_Offer_Previous_Status__c = offerOldMap.get(offer.id).WCT_status__c;
                        tempState.WCT_Related_Offer__c = offer.id;
                        if(offer.WCT_Waiting_for_inputs_from__c <> null){
                            tempState.WCT_Waiting_for_inputs_from__c = offer.WCT_Waiting_for_inputs_from__c;
                        }
                        tempState.WCT_Status__c = 'Post Sent';
                        offerStatesToCreate.add(tempState);
                    
                    }
                }
            
            }
            
            if(!offerStatesToUpdate.isEmpty()){
                DataBase.SaveResult[] srList = DataBase.Update(offerStatesToUpdate,false);
            }
            
            if(!offerStatesToCreate.isEmpty()){
                DataBase.SaveResult[] srList = DataBase.Insert(offerStatesToCreate,false);
            }
            
            isBeforeTrigExecuted = true;   
        }
    
    }

}