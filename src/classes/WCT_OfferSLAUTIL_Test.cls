/*****************************************************************************************
    Name    : WCT_OfferSLAUTIL_Test
    Desc    : Test class for WCT_OfferSLAUTIL class                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Lakshmi Madhuri               08/05/2014         Created 

******************************************************************************************/
@isTest

public class WCT_OfferSLAUTIL_Test {
  /****************************************************************************************
    * @author      - Lakshmi Madhuri
    * @date        - 08/05/2014
    * @description - Test Method for all methods of WCT_OfferSLAUTIL Class 
    *****************************************************************************************/
    public static Contact con;
    public static WCT_Offer__c offer;
    public static WCT_Offer_Status__c tempOffSt;
        
    public static void createData(){
        con=WCT_UtilTestDataCreation.createContact();
        con.email='test@deloitte.com';
        con.WCT_Visa_Type__c='temp';
        con.MobilePhone='123';
        con.WCT_User_Group__c = 'United States';
        insert con;
        
        offer =new WCT_Offer__c();
        offer.WCT_status__c = 'Offer to Be Extended'; 
        offer.WCT_Candidate__c=con.id;
        offer.WCT_Recruiter_Email__c='test@test.com';
        offer.WCT_Recruiting_Coordinator_Email__c='testco@test.com';
        insert offer;
        
        tempOffSt = new WCT_Offer_Status__c(WCT_Related_Offer__c=offer.id);
        insert tempOffSt;
    
    }
    
    public static testmethod void test1()
    {
        Test.starttest();
        WCT_OfferSLAUTIL_Test.createData();
        WCT_Offer__c off = [SELECT id,WCT_status__c FROM WCT_Offer__c where id=:tempOffSt.WCT_Related_Offer__c LIMIT 1];
        off.WCT_status__c = 'Draft in Progress';      
        update off; 
        off.WCT_status__c = 'Offer Accepted'; 
        update off;      
        Test.stoptest();
    }
      
    public static testmethod void test2()
    {
        Test.starttest();
        WCT_OfferSLAUTIL_Test.createData();
        WCT_Offer__c off = [SELECT id,WCT_status__c FROM WCT_Offer__c where id=:tempOffSt.WCT_Related_Offer__c LIMIT 1];
        off.WCT_status__c = 'Draft In Progress';      
        update off; 
        off.WCT_status__c = 'Offer Accepted'; 
        update off;              
        Test.stoptest();
    }
}