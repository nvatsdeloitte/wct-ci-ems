@isTest
private class WCT_OfferTriggerHandler_Test
{
    public static testmethod void test1()
    {
        Test.starttest();
        Id LONid=[SELECT Id,Name FROM recordtype WHERE sObjectType='WCT_List_Of_Names__c' AND Name='Offer State'].Id;
        Contact con=WCT_UtilTestDataCreation.createContact();
        con.email='test@deloitte.com';
        con.WCT_Visa_Type__c='temp';
        con.MobilePhone='123';
        con.WCT_User_Group__c = 'United States';
        con.WCT_State_of_Hiring_Location__c='test';
        insert con;
        
        WCT_List_Of_Names__c LON= new WCT_List_Of_Names__c();
        LON.RecordTypeId= LONid;
        LON.Name= 'California';
        LON.WCT_Type__c= 'US';
        LON.WCT_Hiring_State__c= 'California';
        LON.WCT_State_Abbreviation__c='CA';
        insert LON;
        
        WCT_Offer__c offer=new WCT_Offer__c();
        offer.WCT_status__c = 'Offer to Be Extended'; 
        offer.WCT_Candidate__c=con.id;
        offer.WCT_Recruiter_Email__c='test@test.com';
        offer.WCT_Recruiting_Coordinator_Email__c='testco@test.com';
        insert offer;
        
        con.WCT_State_of_Hiring_Location__c='California';
        update con;
        update offer;
        
        Profile p = [SELECT Id FROM Profile WHERE Name= 'System Administrator']; 
        User testuser= WCT_UtilTestDataCreation.createUser('testest',p.id,'testoffer@test.com','testoffer@test.com');
        insert testuser;
        System.runAs ( testuser){
        /*offer.WCT_status__c = 'Ready for Review';           
        update offer;*/  
        OrgWideEmailAddress owa = [select Id, Address from OrgWideEmailAddress where DisplayName= 'Deloitte US Offers' limit 1];
        
        offer.WCT_status__c = 'Offer Sent';      
        update offer; 
        }
        Test.stoptest();
    }
    public static testmethod void test2()
    {
        Test.starttest();
        Profile p = [SELECT Id FROM Profile WHERE Name=:WCT_UtilConstants.RECRUITER_COMPANY]; 
        User recruitingCoordinator = WCT_UtilTestDataCreation.createUser('testest',p.id,'testoffer@test.com','testoffer@test.com');
        insert recruitingCoordinator;
        
        PermissionSet ps=[select id from PermissionSet where Name= 'Offer_Analyst_Permission'];
        System.runAs ( new User(Id = recruitingCoordinator.id ) ) {
        PermissionSetAssignment psa= new PermissionSetAssignment();
        psa.PermissionSetId=ps.id;
        psa.AssigneeId = UserInfo.getUserId();
        insert psa;
        }
        
        Contact con=WCT_UtilTestDataCreation.createContact();
        con.email='test@deloitte.com';
        con.WCT_Visa_Type__c='temp';
        con.MobilePhone='123';
        con.WCT_User_Group__c='United States';
        //con.WCT_Requisition_Number__c='Testing';
        con.WCT_Taleo_Status__c='Offer Details Updated';           
        insert con;
         
        
        Contact conrec=WCT_UtilTestDataCreation.createContact();
        conrec.email='testoffer@test.com';
        insert conrec;
        
        WCT_Requisition__c req=WCT_UtilTestDataCreation.createRequistion();
        req.WCT_Job_Type__c='Experienced';
        req.WCT_Requisition_Number__c='Testing';
        req.WCT_Recruiter__c=recruitingCoordinator.id;
        insert req;

        con.WCT_Taleo_Status__c='Offer - To be Extended';
        con.WCT_Requisition_Number__c='Testing'; 
        update con;
        
        WCT_Candidate_Requisition__c CandidateRequisition=WCT_UtilTestDataCreation.createCandidateRequisition(con.id,req.id);
        CandidateRequisition.WCT_Job_Type__c='Experienced';
        insert CandidateRequisition;
        system.debug('#####' + CandidateRequisition.id);        
        WCT_Offer__c offer = [select id, WCT_status__c,WCT_Candidate__c,WCT_Address1_INTERN__c,WCT_Candidate_Tracker__c,WCT_Type_of_Hire__c from WCT_Offer__c where WCT_Candidate__c=:con.id];         
        //WCT_Offer__c offer=new WCT_Offer__c();
        offer.WCT_status__c = 'Offer to Be Extended'; 
        offer.WCT_Candidate__c=con.id;
        offer.WCT_Address1_INTERN__c='Testing Offer Trigger';
        offer.WCT_Candidate_Tracker__c=CandidateRequisition.id;
        offer.WCT_Title2__c='Test1';
        offer.WCT_Recruiter_Email__c='test@test.com';
        offer.WCT_Recruiting_Coordinator_Email__c='testco@test.com';  
        //offer.WCT_Job_Type__c='Experienced';
        offer.WCT_Type_of_Hire__c='Part Time';
        //insert offer; 
        Attachment attach=WCT_UtilTestDataCreation.createAttachment(offer.id);
        insert attach; 
        APXTConga4__Conga_Template__c template = WCT_UtilTestDataCreation.CreateCongaTemplate();
        insert template;
        
        Test.stoptest();
    }
    
   public static testmethod void test3()
    {
    WCT_OfferTriggerHandler handler = new WCT_OfferTriggerHandler();
    Contact conRec = WCT_UtilTestDataCreation.createContact();
    insert conRec;
    recordtype rt=[select id from recordtype where DeveloperName = 'India_Campus_Hire_Offer_Intern'];
    WCT_Offer__c offRec = WCT_UtilTestDataCreation.createOfferRec(conRec.Id);
    offRec.RecordTypeId = rt.Id;
    offRec.WCT_Global_Mobility__c = true;
    insert offRec;
    recordtype rtlof=[select id from recordtype where DeveloperName = 'WCT_Offer_Signature'];
    WCT_List_Of_Names__c  lofName = WCT_UtilTestDataCreation.createListOfNames('Name',rtlof.id,'Test');
    lofName.WCT_Global_Mobility__c = true;
    lofName.WCT_User_Group__c='United States';
    insert lofName;
    update offRec;
       
    List<WCT_Offer__c> offList = new List<WCT_Offer__c>();
    offList.add(offRec);
    handler.InterCandidateValues (offList);
    handler.getWhereClauseCri(OffRec);
    handler.UpdateTemplateIdNonIntern(offList);
    handler.UpdateTemplateIdIntern(offList);
    handler.UpdateTemplateAsperInternNonIntern(offList);
    List<WCT_Offer__c> newOffList = new List<WCT_Offer__c>();
    newOffList.add(offRec);
    WCT_OfferTriggerHandler.GetCandidateTracker(newOffList);
    handler.UpdateFieldParam(newOffList);
    }
   public static testmethod void test4()
   {
    WCT_OfferTriggerHandler handler = new WCT_OfferTriggerHandler();
    Contact conRec = WCT_UtilTestDataCreation.createContact();
    conRec.WCT_Requisition_Number__c = '12345';
    insert conRec;
    recordtype rt=[select id from recordtype where DeveloperName = 'India_Campus_Hire_Offer_Intern'];
    WCT_Offer__c offRec = WCT_UtilTestDataCreation.createOfferRec(conRec.Id);
    offRec.RecordTypeId = rt.Id;
    offRec.WCT_Global_Mobility__c = true;
    insert offRec;
    recordtype rtlof=[select id from recordtype where DeveloperName = 'WCT_Offer_Signature'];
    WCT_List_Of_Names__c  lofName = WCT_UtilTestDataCreation.createListOfNames('Name',rtlof.id,'Test');
    lofName.WCT_Global_Mobility__c = true;
    lofName.WCT_User_Group__c='US GLS India';
    insert lofName;
    update offRec;  
    offRec.WCT_Signatory_Name__c = 'None';
    update offRec;
    
    WCT_Requisition__c reqRec = WCT_UtilTestDataCreation.createRequistion();
    insert reqRec;
    WCT_Offer__c offRec2 = WCT_UtilTestDataCreation.createOfferRec(conRec.Id);
    offRec2.RecordTypeId = rt.Id;
    offRec2.WCT_Global_Mobility__c = true;
    offRec2.WCT_Candidate__c = conRec.Id;
    insert offRec2;
    
    List<WCT_Offer__c> newOffList = new List<WCT_Offer__c>();
    newOffList.add(offRec2);
    
    WCT_Candidate_Requisition__c candReq = WCT_UtilTestDataCreation.createCandidateRequisition(conRec.Id,reqRec.id);
    //candReq.WCT_Requisition_Number__c ='12345';
    insert candReq;
    WCT_Offer_Data_Fields_Parameters__c  offData = WCT_UtilTestDataCreation.CreateOfferDataFieldsParameter();
    insert offData;
    handler.UpdateFieldParam(newOffList);
    WCT_OfferTriggerHandler.GetCandidateTracker(newOffList);
   }
   
   public static testmethod void test5()
   {
    WCT_OfferTriggerHandler handler = new WCT_OfferTriggerHandler();
    Contact conRec = WCT_UtilTestDataCreation.createContact();
    conRec.WCT_Requisition_Number__c = '12345';
    insert conRec;
    recordtype rt=[select id from recordtype where DeveloperName = 'India_Campus_Hire_Offer_Intern'];
    
    WCT_Offer__c offRec = WCT_UtilTestDataCreation.createOfferRec(conRec.Id);
    offRec.RecordTypeId = rt.Id;
    offRec.WCT_Global_Mobility__c = true;
    offRec.WCT_Offer_Letter_Reviewed__c = true;
    insert offRec;
    offRec.WCT_Offer_Letter_Reviewed__c = true;
    offRec.WCT_status__c = 'Offer Approved';
    try{
    update offRec;
    }
    Catch(exception e){
    //System.AssertEquals(e.getMessage(),'Please click on \'Attach Draft Offer for Review\' prior to marking as \'Ready for Review\' or \'Offer Approved\'');
    }
   }
   
   public static testmethod void test6(){
   WCT_OfferTriggerHandler handler = new WCT_OfferTriggerHandler();
   Contact conRec = WCT_UtilTestDataCreation.createContact();
   conRec.WCT_Requisition_Number__c = '12345';
   conRec.WCT_Relocation_Amt__c = 2000;
   conRec.WCT_Relocation_Amt_Formatted__c = '2000';
   insert conRec;
   
   WCT_Salary_Calculator__c sc = new WCT_Salary_Calculator__c();
   sc.WCT_Function__c = 'test';
   sc.WCT_Designation__c = 'test1';
   sc.WCT_Career_level__c = 'test2';
   sc.WCT_Communication_Allowance__c = '2000';
   sc.WCT_Driver_Code__c = 'ABCD001';
   sc.WCT_Entity_Designation__c = 'test';
   sc.WCT_Medical_Insurance_Premium__c = '1200';
   sc.WCT_Medical_Insurance_Premium_2__c = '24000';
   sc.WCT_Medical_Insurance_Premium_Num__c = 1200;
   sc.WCT_Variable_Percentage__c = '0-10';
   sc.WCT_Vehicle_Running_Expenses__c = '2000';
   insert sc;
   
   Id LONid=[SELECT Id,Name FROM recordtype WHERE sObjectType='WCT_List_Of_Names__c' AND Name='Offer Message'].Id;
   WCT_List_Of_Names__c LON= new WCT_List_Of_Names__c();
   LON.RecordTypeId= LONid;
   LON.WCT_Type__c= 'US';
   LON.WCT_Conditional_Verbiage__c= 'Default';
   LON.WCT_Message__c= 'Please sign';
   LON.Name= 'LOI Default';
   insert LON;   
    
   WCT_Offer_Data_Fields_Parameters__c FieldParameter = new WCT_Offer_Data_Fields_Parameters__c();
    FieldParameter.WCT_Designation_Code__c = '001';
    FieldParameter.WCT_Sub_Area_Code__c = '002';
    FieldParameter.WCT_Look_Up_Code__c = '003';
   
   /*PermissionSet PermissionSet = new PermissionSet();
   PermissionSet.Name = 'Offer_Analyst_Permission';
   Insert PermissionSet;
   
   PermissionSetAssignment PermissionSetAssign = new PermissionSetAssignment();
   PermissionSetAssign.AssigneeId = UserInfo.getUserId();
   Insert PermissionSetAssign;*/
    
   // recordtype rt=[select id from recordtype where DeveloperName = 'India_Campus_Hire_Offer_Intern'];
   recordtype rt=[select id from recordtype where DeveloperName = 'WCT_India_Experienced_Hire_Offer'];
   
    WCT_Offer__c offRec = WCT_UtilTestDataCreation.createOfferRec(conRec.Id);
    offRec.RecordTypeId = rt.Id;
    offRec.WCT_Global_Mobility__c = true;
    offRec.Conditional_Verbiage__c = 'Default';
    offRec.WCT_Relocation_Type__c = 'India Hire';
    insert offRec;
    offRec.WCT_Designation_Code__c = '001';
    offRec.WCT_Sub_Area_Code__c = '002';
    offRec.WCT_Lookup_Code__c = '003';
    offRec.WCT_Salary_Calculator_function__c = 'test';
    offRec.WCT_Salary_Calculator_Designation__c = 'test1';
    offRec.WCT_Salary_Calculator_Career_Level__c = 'test2';
    offRec.WCT_Hourly_Rate__c = '2000';
    offRec.WCT_ENTER_1ST_INSTALL_AMT__c = '2000';
    offRec.WCT_ENTER_2ND_INSTALL_AMT__c = '2000';
    offRec.WCT_ENTER_3RD_INSTALL_AMT__c = '2000';
    offRec.WCT_Sign_On_Amt_In_Figures__c = '2000';
    offRec.WCT_Annual_Medical_Insurance_Premium__c = '2000';
    offRec.WCT_StateSupplementaryRateforPromissrynt__c = '2000';
    offRec.WCT_Salary_after_premium__c = '2000';
    offRec.WCT_Annual_Basic__c = '2000';
    offRec.WCT_Annual_HRA__c = '2000';
    offRec.WCT_Annual_LTA__c = '2000';
    offRec.WCT_Annual_Provident_Fund__c = '2000';
    offRec.WCT_Annual_Spcl_Allowance__c = '2000';
    // offRec.WCT_Basic__c = '2000';
    offRec.WCT_HRA__c = '2000';
    offRec.WCT_LTA__c = '2000';
    offRec.WCT_Provident_Fund__c = '2000';
    offRec.WCT_Spcl_Allowance__c = '2000';
    offRec.WCT_Amount_Eligible_Month__c = '2000'; 
    offRec.WCT_Communication_Expenses__c = '2000';
    offRec.WCT_Monthly_Premium__c = '2000';
    offRec.WCT_Sign_On_Bonus_2__c = '2000';
    offRec.WCT_Housing_Allowance__c = '2000';
    offRec.WCT_Computer_Allowance__c = '2000';
    offRec.WCT_AdvanceLoan__c = '2000';
    offRec.WCT_AdvanceLoan_IFA__c = '2000';
    offRec.WCT_X50_Sign_on_Bonus__c = '2000';  
    offRec.WCT_X50_Relocation_Amt__c = '2000';
    offRec.WCT_Double_Time_Rate__c = '2000';
    offRec.WCT_Offer_Overtime_Rate__c = '2000';
    offRec.WCT_Promisory_Note_IFA_Amount__c = '2000';
    offRec.WCT_NextGen_Amount__c = '2000';
    offRec.WCT_Stipend_Amount__c = '2000';
    offRec.WCT_Accomodation_Expenses__c = '2000';
    offRec.WCT_X50_Incentive_Amt__c = '2000';  
    offRec.WCT_SigningbonusPromissoryNoteamount_Fig__c = 2000;
    offRec.WCT_Total_Salary_Annually__c = 700008;
    offRec.WCT_Incentive_Amount__c = 2000;
    update offRec;
   }
   
   public static testmethod void test7() {
       recordtype rt=[select id from recordtype where DeveloperName = 'WCT_India_Experienced_Hire_Offer'];
       Contact conRec = WCT_UtilTestDataCreation.createContact();
       WCT_Offer__c offRec = WCT_UtilTestDataCreation.createOfferRec(conRec.Id);
       offRec.WCT_status__c = 'Offer to Be Extended';
       offRec.RecordTypeId = rt.Id;
       insert offRec;
       WCT_Offer_Note__c OfferNote = WCT_UtilTestDataCreation.CreateOfferNote(offRec.id);
       offRec.WCT_status__c = 'Offer Declined';
       offRec.WTC_Latest_Offer_Note_Status__c = 'Offer Declined';
       Update offRec;
   }
}