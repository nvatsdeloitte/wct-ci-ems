public class WCT_OnboardingInitiationForm  extends SitesTodHeaderController{

    /* public variables */
    public WCT_Assignment__c assignmentRec {get;set;}
    public WCT_Mobility__c mobilityRecord{get;set;}
    

    /* Upload Related Variables */
    public Document doc {get;set;}
    public List<String> docIdList = new List<string>();
    public List<AttachmentsWrapper> UploadedDocumentList {get; set; }
    public task t{get;set;}
    public String taskid{get;set;}
    public List<Attachment> listAttachments {get; set;}
    public Map<Id, String> mapAttachmentSize {get; set;}  
    
    //Controller Related Variables
    public String employeeType;
    public Contact Employee{get;set;}
    public Map<String,String> recordTypeMap {get;set;}
    public String assignmentstartdate {get;set;}
    public String assignmentenddate {get;set;}
    public String dateOfArrival {get;set;}
    public String lastWorkingDayUS {get;set;}

    // Error Message related variables
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}
    private boolean isAllFieldsValid;
 
 
    public WCT_OnboardingInitiationForm()
    {
        /*Initialize all Variables*/
        init();

        /*Get Task ID from Parameter*/
        getParameterInfo();  

        getMobilityTypeDropDownValues();
        
        /*Task ID Null Check*/
        if(taskid=='' || taskid==null)
        {
           invalidEmployee=true;
           return;
        }

        /*Get Task Instance*/
        getTaskInstance();
        
        /*Get Attachment List and Size*/
        getAttachmentInfo();
        
        /*Query to get Immigration Record*/  
        getMobilityDetails();
    }

    private void init(){

        //Custom Object Instances
        mobilityRecord = new  WCT_Mobility__c();
        assignmentRec = new  WCT_Assignment__c();

        Employee=new Contact();
        employeeType = '';
        recordTypeMap = new Map<String,String>();

        //Document Related Init
        doc = new Document();
        UploadedDocumentList = new List<AttachmentsWrapper>();
        listAttachments = new List<Attachment>();
        mapAttachmentSize = new Map<Id, String>();

        //Set Default Validation Flag to Pass All Validation
        isAllFieldsValid = true;
    }   

    private void getParameterInfo(){
        taskid = ApexPages.currentPage().getParameters().get('taskid');
    }

    private void getTaskInstance(){
        t=[SELECT Status,OwnerId,WhatId,WCT_Is_Visible_in_TOD__c,WCT_ToD_Task_Reference__c, WCT_Auto_Close__c  FROM Task WHERE Id =: taskid];
    }

    public list<SelectOption> getMobilityTypeDropDownValues(){

        List<SelectOption> options = new List<SelectOption>();

        for(RecordType rt: [SELECT ID, Name FROM RecordType WHERE sObjectType = 'WCT_Mobility__c']){

            //Create Record Map
            recordTypeMap.put(rt.ID, rt.Name);

            if(String.isNotEmpty(employeeType)){

                if(employeeType == 'Business'){
                    if(rt.Name == 'Business Visa'){
                        options.add(new SelectOption(rt.ID, rt.Name));
                        mobilityRecord.recordTypeId = rt.Id;
                    }                    
                }

                if(employeeType == 'Employee'){
                    if(rt.Name == 'Employment Visa'){                    
                        options.add(new SelectOption(rt.ID, rt.Name));
                        mobilityRecord.recordTypeId = rt.Id;
                    }
                }
            }
        }
        return options;
    }

    private void getAttachmentInfo(){
        listAttachments = [SELECT ParentId , 
                                    Name, 
                                    BodyLength, 
                                    Id,
                                    CreatedDate
                                    FROM  Attachment 
                                    WHERE ParentId = :taskid
                                    ORDER BY CreatedDate DESC
                                    LIMIT 50 ];    
              
        for(Attachment a : listAttachments) {
            String size = null;

            if(1048576 < a.BodyLength){
                // Size greater than 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength){
                // Size greater than 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else{
                size = '' + a.BodyLength + ' bytes';
            }
            mapAttachmentSize.put(a.id, size);
        }
    }

    public void getMobilityDetails()
    {
        mobilityRecord = [SELECT WCT_Number_Of_Children__c, recordTypeId, WCT_Accompanying_Dependents__c,WCT_Date_of_Arrival_in_US__c, WCT_India_Cellphone__c,WCT_Accompanied_Status__c,WCT_Deloitte_US_Office_City__c,WCT_Deloitte_US_Office_State__c,WCT_Email_Received__c,
                            WCT_First_Working_Day_in_US__c,WCT_Last_Working_Day_in_India__c,WCT_Last_Working_Day_in_US__c,WCT_Mobility_Employee__c,WCT_Mobility_Status__c,WCT_Mobility_Type__c, 
                            WCT_Project_Controller__c,WCT_USI_Resource_Manager__c,WCT_US_Project_Mngr__c,WCT_To_Country__c,WCT_Travel_Duration__c,WCT_Travel_End_Date__c,WCT_Travel_Insurance_End_Date__c,WCT_Travel_Start_Date__c,
                            WCT_Purpose_of_Travel__c,WCT_US_Benefits_Effective_Date__c,WCT_US_Contact_Number__c,ownerid,WCT_Visa_Type__c,WCT_Regional_Dest__c,WCT_US_RCCode__c,WCT_USI_RCCode__c
                            FROM WCT_Mobility__c
                            where id=:t.WhatId];   
                           
    }


    public pageReference save()
    {

        /*Custom Validation Rules are handled here*/

        if( (null == mobilityRecord.WCT_India_Cellphone__c) ||
            ('' == mobilityRecord.WCT_India_Cellphone__c) ||
            (null == assignmentRec.WCT_Client_Office_Name__c) ||
            ('' == assignmentRec.WCT_Client_Office_Name__c) ||
            (null == assignmentRec.WCT_Client_Office_City__c) ||
            ('' == assignmentRec.WCT_Client_Office_City__c) ||
            (null == mobilityRecord.WCT_Project_Controller__c) ||
            ('' == mobilityRecord.WCT_Project_Controller__c) ||
            (null == assignmentRec.WCT_Client_Office_State__c) ||
            ('' == assignmentRec.WCT_Client_Office_State__c) ||
            (null == mobilityRecord.WCT_US_Project_Mngr__c) ||
            ('' == mobilityRecord.WCT_US_Project_Mngr__c)||
            (null == assignmentRec.WCT_Regional_Dest__c) ||
            ('' == assignmentRec.WCT_Regional_Dest__c)||
            //(null == mobilityRecord.WCT_USI_RCCode__c) ||
            //('' == mobilityRecord.WCT_USI_RCCode__c)||
            (null == dateOfArrival) ||('' == dateOfArrival)) 
            {

            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
           }
        
        lastWorkingDayUS = String.valueOf(mobilityRecord.WCT_Last_Working_Day_in_US__c);
        
        RecordType rt1 = [SELECT ID, Name FROM RecordType WHERE sObjectType = 'WCT_Mobility__c'and id=:mobilityRecord.recordTypeId];
        if(rt1.Name == 'Employment Visa'){
            if((null != assignmentstartdate ) || ('' != assignmentstartdate ) ||
                (null != assignmentenddate ) || ('' != assignmentenddate )){       
                if(String.isNotBlank(assignmentstartdate )){
                    assignmentRec.WCT_Initiation_Date__c = Date.parse(assignmentstartdate );
                }
                if(String.isNotBlank(assignmentenddate )){
                    assignmentRec.WCT_End_Date__c = Date.parse(assignmentenddate );
                }
                
                if(assignmentRec.WCT_Initiation_Date__c > assignmentRec.WCT_End_Date__c) {
                        pageErrorMessage = 'The Assignment End Date cannot be before the Assignment End Date. Please correct.';
                        pageError = true;
                        return null;
                }
                if(assignmentRec.WCT_Initiation_Date__c < system.today() || assignmentRec.WCT_End_Date__c < system.today()){
                        pageErrorMessage = 'The Assignment Start Date and End Date cannot be a past date. Please correct.';
                        pageError = true;
                        return null;   
                }
                 
                if(String.valueOf(assignmentRec.WCT_End_Date__c) != lastWorkingDayUS){
                        pageErrorMessage = 'The Assignment End Date must be same as Last Working Day in US which is: '+lastWorkingDayUS+'. Please correct.';
                        pageError = true;
                        return null;   
                }
            }
            else
            {
                 pageErrorMessage = 'Please fill in all the required fields on the form.';
                 pageError = true;
                 return null;
            }
       }

        /*Changing the Owner Id of Task Temporarily to avoid Integrity Exception*/
        t.OwnerId=UserInfo.getUserId();
        upsert t;
        
        pageError = false;
           
        /*update mobility Record*/
         if(String.isNotBlank(dateOfArrival)){
            mobilityRecord.WCT_Date_of_Arrival_in_US__c = Date.parse(dateOfArrival);
        }
        if(mobilityRecord.WCT_Date_of_Arrival_in_US__c < system.today()){
                        pageErrorMessage = 'The Date of Arrival in US cannot be a past date. Please correct.';
                        pageError = true;
                        return null;   
                }
        if(mobilityRecord.WCT_Mobility_Type__c=='Business Visa')
        {
            mobilityRecord.WCT_Client_Office_Name__c = assignmentRec.WCT_Client_Office_Name__c;
            mobilityRecord.WCT_Deloitte_US_Office_City__c = assignmentRec.WCT_Client_Office_City__c;
            mobilityRecord.WCT_Deloitte_US_Office_State__c = assignmentRec.WCT_Client_Office_State__c;
            
        }
        mobilityRecord.WCT_Regional_Dest__c = assignmentRec.WCT_Regional_Dest__c;
        
        
        if(t.WCT_ToD_Task_Reference__c!='0013')
          mobilityRecord.WCT_Mobility_Status__c='Onboarding Completed';
        update mobilityRecord;
            
        //creating assignment record and association it with mobility record updated above.
        if(mobilityRecord.WCT_Mobility_Type__c=='Employment Visa')
        {
          assignmentRec.WCT_Mobility__c=mobilityRecord.id;
          assignmentRec.WCT_Employee__c = mobilityRecord.WCT_Mobility_Employee__c;
          assignmentRec.OwnerId = mobilityRecord.OwnerId;
          insert assignmentRec;

        }
        /* Upload Documents as Related Attachments to Case After the Case Record is created */
        uploadRelatedAttachment();

        if(string.valueOf(mobilityRecord.Ownerid).startsWith('00G')){
            //owner is Queue. So set it to GM&I User
           // t.Ownerid = '005f0000001AWth';
            t.Ownerid = System.Label.GMI_User;
        }else{
            t.OwnerId = mobilityRecord.Ownerid;
        }
        

        //updating task record
        if(t.WCT_Auto_Close__c == true){   
            t.status = 'completed';
        }else{
            t.status = 'Employee Replied';  
        }

        t.WCT_Is_Visible_in_TOD__c = false; 
        upsert t;
        
        //Reset Page Error Message
        pageError = false;
        
        getAttachmentInfo();
        /*Set Page Reference After Save*/
        return new PageReference('/apex/WCT_OnboardingInitiationForm_ThankYou?em='+CryptoHelper.encrypt(LoggedInContact.Email)+'&taskid='+taskid) ;  
        
    }

    private void uploadRelatedAttachment(){
        if(!docIdList.isEmpty()) { //If there is any Documents Attached in Web Form

            List<String> selectedDocumentId = new List<String>();
            for(AttachmentsWrapper aWrapper : UploadedDocumentList) {
                if(aWrapper.isSelected) {
                    selectedDocumentId.add(aWrapper.documentId);
                }
            }
            
            /* Select Documents which are Only Active (Selected) */
            List<Document> selectedDocumentList = new List<Document>();
            if(!selectedDocumentId.isEmpty()){
                selectedDocumentList = [
                                           SELECT 
                                               id,
                                               name,
                                               ContentType,
                                               Type,
                                               Body 
                                           FROM 
                                               Document 
                                           WHERE 
                                               id IN :selectedDocumentId
                                        ];
            }
            
            /* Convert Documents to Attachment */
            List<Attachment> attachmentsToInsertList = new List<Attachment>();
            for(Document doc : selectedDocumentList){
                Attachment a = new Attachment();
                a.body = doc.body;
                a.ContentType = doc.ContentType;
                a.Name= doc.Name;
                a.Description=String.valueof(taskid);
                a.parentid = taskid; //immigration Record Id which is been retrived.
                attachmentsToInsertList.add(a);                
            }
            
            if(!attachmentsToInsertList.isEmpty()){
                insert attachmentsToInsertList;
            }
            
            List<Document> listDocuments = new List<Document>();
            listDocuments = [
                                SELECT
                                    Id
                                FROM
                                    Document
                                WHERE
                                    Id IN :docIdList
                            ];
            if( (null != listDocuments) && (0 < listDocuments.size()) ) {
                delete listDocuments;
            }
        }
    }

    /*Invoked when Upload Button in VF page is clicked and the IDs are stored in the docIdList*/
    /*All the Files Uploaded are stored in the Documents. Documents does not require parent Id.This method is used to circumvent to upload
      the documents first and then add as Attachment to the Cases (Related List)*/

    public void uploadAttachment(){
        pageError = false;
        pageErrorMessage = '';
    
        if(ApexPages.hasMessages()) {
            pageErrorMessage = '';
            for(ApexPages.Message message : ApexPages.getMessages()) {
                pageErrorMessage += message.getSummary() + '\n';
            }            
            doc = new Document();
            pageError = true;
            return;
        }                                
        
        doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
        if(doc.body != null) {
            insert doc;
            docIdList.add(doc.id);
            doc = [SELECT id, name, bodylength FROM Document WHERE id = :doc.id];

            String size = '';
            if(1048576 < doc.BodyLength) {
                // Size greater than 1MB
                size = '' + (doc.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < doc.BodyLength) {
                // Size greater than 1KB
                size = '' + (doc.BodyLength / 1024) + ' KB';            
            }
            else {
                size = '' + doc.BodyLength + ' bytes';
            }
            UploadedDocumentList.add(new AttachmentsWrapper(true, doc.name, doc.id, size));
            doc = new Document();
        }
    }       
    

    //Documents Wrapper
    public class AttachmentsWrapper {
        public Boolean isSelected {get;set;}
        public String docName {get;set;}
        public String documentId {get;set;}
        public String size {get; set;}
            
        public AttachmentsWrapper(Boolean selected, String Name, String Id, String size){
            isSelected = selected;
            docName = Name ;
            documentId = Id;
            this.size = size;
        }        
    }
}