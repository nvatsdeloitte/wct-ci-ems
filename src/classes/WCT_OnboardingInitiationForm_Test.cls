/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class WCT_OnboardingInitiationForm_Test 

{
    public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        recordtype mobRecType = [select id from recordtype where DeveloperName = 'Employment_Visa'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        String employeeType = 'Employee';
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        mob.WCT_India_Cellphone__c='12345678';
        mob.WCT_Accompanied_Status__c=true;
        mob.WCT_Number_Of_Children__c=1;
        mob.WCT_Accompanying_Dependents__c=2;
        mob.WCT_US_Project_Mngr__c='Testing@mobility.com';
        mob.WCT_Regional_Dest__c='Testing';
        mob.WCT_USI_RCCode__c='Testing';
        mob.WCT_Project_Controller__c = 'test@mobility.com';
        mob.RecordTypeId = mobRecType.Id;
        insert mob;
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Auto_Close__c=false;
        insert t;
        datetime startdate=date.Today().adddays(10);
        datetime assignstartdate=date.Today().adddays(2);
        datetime assignenddate=date.Today().adddays(5);
        
        Test.starttest();
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_OnboardingInitiationForm;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_OnboardingInitiationForm controller=new WCT_OnboardingInitiationForm();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller=new WCT_OnboardingInitiationForm();
        controller.save(); 
        controller.dateOfArrival=startdate.format('MM/dd/yyyy');
        controller.assignmentRec.WCT_Client_Office_Name__c='test';
        controller.assignmentRec.WCT_Client_Office_City__c='test';
        controller.assignmentRec.WCT_Client_Office_State__c='test';
        controller.assignmentstartdate = assignstartdate.format('MM/dd/yyyy');
        controller.assignmentenddate = assignenddate.format('MM/dd/yyyy');
        controller.employeeType = 'Employee';
        controller.doc=WCT_UtilTestDataCreation.createDocument();
        controller.uploadAttachment();
        controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        controller.uploadAttachment();
    }
}