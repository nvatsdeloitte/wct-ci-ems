// Description : This class is called by POETrigger
//
//
public class WCT_POETriggerHandler{

public static void populateContactInfo(List<WCT_Port_of_Entry__c> lstPoe){
        
    for(WCT_Port_of_Entry__c tmpPoe : lstPoe){
         List<Contact> portobj = [SELECT Name,Email,WCT_Function__c FROM contact where Email =: tmpPoe.WCT_Email_Address__c ORDER BY createddate DESC LIMIT 1 ]; 
         for(Contact c :portobj) {
                tmpPoe.WCT_Employee_Name__c = c.Name;
                tmpPoe.WCT_Employee_Function__c =  c.WCT_Function__c;
        }
    }
}

}