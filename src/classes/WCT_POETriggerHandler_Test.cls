@isTest
private class WCT_POETriggerHandler_Test
{
    public static testmethod void m1()
    {
        Test.startTest();
         contact con = new contact();
          con.FirstName='Test';
          con.LastName='contact';
         
          con.WCT_Function__c = 'Tax';
          con.Email = 'test@deloitte.com'; 
          Insert con;
         
        
        WCT_Port_of_Entry__c poeRec = new WCT_Port_of_Entry__c();
        poeRec.WCT_Email_Address__c = con.Email;
        poeRec.WCT_Employee_Function__c = con.WCT_Function__c;
        
        INSERT poeRec;
        
        
        Test.stopTest();
    }
    
}