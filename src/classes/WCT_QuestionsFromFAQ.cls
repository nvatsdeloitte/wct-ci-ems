public class WCT_QuestionsFromFAQ{
    
    //Variable declaration
    public string caseId {get;set;}
    public case currCase {get;set;}
    public boolean appendQuestions {get;set;}
    public list<KAVWrapper> FAQkavWrapList {get;set;}
    
    
    
    public WCT_QuestionsFromFAQ(){
        FAQkavWrapList = new list<KAVWrapper>();
        caseId = ApexPages.currentpage().getParameters().get('CaseId');
        currCase = [SELECT id,WCT_Questions_from_FAQs__c FROM Case where id=:caseId Limit 1];
        
        Set<Id> FAQKAIds = new Set<Id>();
        Set<Id> ArtIds = new Set<Id>();
        for(CaseArticle ca: [SELECT KnowledgeArticleId,CaseId FROM CaseArticle where CaseId = :caseId]){
            ArtIds.add(ca.KnowledgeArticleId);
            
        }
        
        if(!ArtIds.isEmpty()){
            for(KnowledgeArticleVersion kav:[SELECT id,KnowledgeArticleId,articleType FROM KnowledgeArticleVersion
                                                WHERE PublishStatus='Online' AND Language = 'en_US' and KnowledgeArticleId IN :ArtIds]){
                if(kav.ArticleType == 'FAQ__KAV'){
                    FAQKAIds.add(kav.KnowledgeArticleId);
                }
            
            }       
        }
        
        if(!FAQKAIds.isEmpty()){
            for( FAQ__KAV fq : [SELECT id,Questions__c,Title FROM FAQ__KAV where publishstatus = 'online' 
                                    And language = 'en_US' and knowledgearticleid IN :FAQKAIds]){
                FAQkavWrapList.add(new KAVWrapper(fq,false));
            }
        }
        
    
    }
    
    public class KAVWrapper{
        public FAQ__KAV faq {get;set;}
        public boolean isSelected {get;set;}
        
        public KAVWrapper(FAQ__KAV fq,Boolean sel){
            faq = fq;
            isSelected = sel;
        }
    
    }
    
    public pageReference updateQuestions(){
        
        for(KAVWrapper kaw:FAQkavWrapList){
            if(kaw.isSelected){
                if(kaw.faq.Questions__c <> null){
                    if(currCase.WCT_Questions_from_FAQs__c <> null){
                        currCase.WCT_Questions_from_FAQs__c += '<Br/><b>Questions From Article:</b>'+kaw.faq.Title+'<Br/>----------------------';
                        currCase.WCT_Questions_from_FAQs__c += '<Br/><Br/>'+kaw.faq.Questions__c;
                    }else{
                        currCase.WCT_Questions_from_FAQs__c = '<b>Questions From Article:</b>'+kaw.faq.Title+'<Br/>----------------------';
                        currCase.WCT_Questions_from_FAQs__c += '<Br/><Br/>'+kaw.faq.Questions__c;
                    }
                }
            }       
        }
        update currCase;
        return new PageReference('/'+currCase.id);
    
    }
    
    public pageReference cancel(){
        return new PageReference('/'+currCase.id);
    }

}