@isTest
private class WCT_QuestionsFromFAQ_Test{
    
    public static contact testContact {get;set;}
    public static case testCase {get;set;}
    
    
    static testMethod void quesFromFAQ(){
        testContact = WCT_UtilTestDataCreation.createContact();
        insert testContact;
        
        testCase =  WCT_UtilTestDataCreation.createCase(testContact.id);
        insert testCase;
        date d = System.Today();
        List<FAQ__kav> FAQList = new List<FAQ__kav>();
        FAQ__kav FAQ = new FAQ__kav(Title = 'Testing Article-FAQ',Questions__c='Test Article',UrlName = 'Testing-Article-FAQ',Data_Source_Link__c = 'www.google.com',Expiration_Date__c = d.adddays(10));
        FAQList.add(FAQ);
        insert FAQList;
        
        Id TestId;
        TestId = [select KnowledgeArticleId from KnowledgeArticleVersion where Id =: FAQList[0].Id and PublishStatus = 'Draft' and Language = 'en_US'  limit 1].KnowledgeArticleid;
        KbManagement.PublishingService.publishArticle(TestId,True);
        
        CaseArticle ca = new CaseArticle(caseId=testCase.id,KnowledgeArticleId=TestId);
        insert ca;
        
        test.startTest();
        ApexPages.currentpage().getParameters().put('CaseId',testCase.id);
        WCT_QuestionsFromFAQ qfaq = new WCT_QuestionsFromFAQ();
        qfaq.FAQkavWrapList[0].isSelected = true;
        qfaq.updateQuestions();
        qfaq.cancel();
        qfaq.currCase.WCT_Questions_from_FAQs__c  = 'Test Article for Questions';
        qfaq.updateQuestions();
        test.stopTest();
    
    
    }

}