public class WCT_RC_homepageSumbmittedIEFLink {

    public String allLst { get; set; }
     String RC_Email= UserInfo.getUserEmail();
    public  list<WCT_Interview_Junction__c >  allDataLst{get;set;} 
     
                    
         public WCT_RC_homepageSumbmittedIEFLink ()
         {
          allDataLst = [SELECT Id, Name, WCT_Candidate_Name__c, WCT_Hiring_Level__c, WCT_Start_Date_Time__c, WCT_End_Date_Time__c, WCT_Interview_Type__c, WCT_Interview_Medium__c, 
                        WCT_Requisition_RC_Email__c, WCT_IEF_Submission_Status__c, WCT_Candidate_Tracker__r.WCT_Contact__r.Id, WCT_Interview__c, WCT_Interview__r.Name,
                        WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Name, WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Email,
                        WCT_Interview__r.WCT_Interview_Location_Details__c, WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiter__r.Name, 
                        WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiter__r.Email ,WCT_IEF_URL_Base__c,WCT_Candidate_Name_Hyperlink__c,WCT_Interviewer__c,WCT_Interviewer__r.name,
                        WCT_Attachment_Link__c,WCT_Attachment_URL__c, wct_candidate_email__c,RMS_ID__c
                        FROM WCT_Interview_Junction__c 
                    where (WCT_Requisition_Recruiter_Email__c= :RC_Email
                    or WCT_Requisition_Recruiter_Coor_Email__c =: RC_Email )
                    and WCT_IEF_Submission_Status__c =: 'Submitted'
                    order by WCT_Start_Date_Time__c desc
                    limit:Limits.getLimitQueryRows()];
                    //and WCT_Interview_Status__c IN ('Complete','Invite Sent')
         }  
                    
}