@isTest
private class WCT_RequisitionHandler_Test
{

static testMethod void Test(){
  

  Profile p = [SELECT Id FROM Profile WHERE Name=:WCT_UtilConstants.RECRUITER_COMPANY];  
  User recruiter_1 = WCT_UtilTestDataCreation.createUser('test12',p.id,'usernameWCTPRD@test.com','email1@testing.com');
  insert recruiter_1;
  
  WCT_Requisition__c Requisition_1= WCT_UtilTestDataCreation.createRequisitionbyUserEmails(recruiter_1.Email,null);
  insert Requisition_1;
  
  User recruiter_2 = WCT_UtilTestDataCreation.createUser('recr',p.id,'SFUFAY1@test.com','email22211@testing.com');
  insert recruiter_2;
  
  User recruitingCoordinator = WCT_UtilTestDataCreation.createUser('recco',p.id,'gjdfas1@test.com','email71521@testing.com');
  insert recruitingCoordinator;
  
  WCT_Requisition__c Requisition_2= WCT_UtilTestDataCreation.createRequisitionbyUserEmails(null,recruitingCoordinator.Email);
  insert Requisition_2;
  
  Requisition_1.WCT_Recruiter__c= recruiter_2.id;
  Requisition_1.WCT_Recruiting_Coordinator__c = recruitingCoordinator.id;  
  update Requisition_1; 
 
  Contact adhocContact= WCT_UtilTestDataCreation.createAdhocContactsbyEmail('abc1@deloitte.com'); 
     insert adhocContact;
    Id AdhocID=adhocContact.id;
    
    
    WCT_Candidate_Requisition__c  candidateReq = WCT_UtilTestDataCreation.createCandidateRequisition(AdhocId,Requisition_1.id);
    insert candidateReq;
    
    insert WCT_UtilTestDataCreation.CreateDocumentbyFolder('IEFDocument',label.WCT_IEFSampleDocumentsFolder);
    
    insert WCT_UtilTestDataCreation.CreateDocumentbyFolder('IEFDocument',label.WCT_IEFWordDocumentsFolder);
    
    WCT_List_Of_Names__c ClicktoolForm = WCT_UtilTestDataCreation.createClickToolFormbyName('IEFDocument');
    insert ClicktoolForm ;
    
    WCT_Interview__c interview = WCT_UtilTestDataCreation.createInterview(ClicktoolForm.id);
    insert interview;

    User Interviwer = WCT_UtilTestDataCreation.createUser('inter',p.id,'interviwer1@test.com','interviweremail1@testing.com');
    insert Interviwer;
    
    insert WCT_UtilTestDataCreation.createInterviewTracker(interview.ID ,candidateReq.ID ,Interviwer.ID);     
    
    Requisition_1.WCT_Recruiter__c=recruitingCoordinator.id;
    Requisition_1.WCT_Recruiting_Coordinator__c = recruiter_2.id ;
  
   update Requisition_1; 
   Requisition_1.WCT_Recruiter_Email_ID__c = recruiter_2.Email ;
   Requisition_1.WCT_Recruiting_Coordinator_Email_ID__c=recruitingCoordinator.Email;
   update Requisition_1; 
}


    
}