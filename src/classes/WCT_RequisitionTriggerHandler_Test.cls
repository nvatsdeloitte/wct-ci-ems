/*****************************************************************************************
    Name    : WCT_RequisitionTriggerHandler_Test
    Desc    : Test class for RequisitionTriggerHandler class                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Deloitte                11/22/2013         Created 

******************************************************************************************/
@isTest
private class WCT_RequisitionTriggerHandler_Test{
    
    
    /****************************************************************************************
    * @author      - Deloitte
    * @date        - 11/22/2013
    * @description - Test Method for WCT_RequisitionTrigger 
    *****************************************************************************************/
    
    public static testmethod void UpdateOwner(){
        
        test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name=:WCT_UtilConstants.RECRUITER_COMPANY]; 
        UserRole r1 = [SELECT Id FROM UserRole Where Name='CTS Recruiter'];
        UserRole r2 = [SELECT Id FROM UserRole Where Name='CIC Manager'];
        //User recutr1 = new User(Alias = 'rUser1', Email='recuiteruser1@wct.com', LastName='Testing1', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='recuiteruser1@wct.com', UserRoleId=r1.Id);
        User recutr1 = WCT_UtilTestDataCreation.createUser('rUser1',p.Id,'recuiteruser1@wct.com','recuiteruser1@wct.com');
        
        system.runAs(recutr1){
            WCT_Requisition__c req = new WCT_Requisition__c();    
            req.WCT_Recruiter__c = recutr1.Id;
            req.Ownerid = recutr1.Id;
            insert req;
                        
            //User recutr2 = new User(Alias = 'rUser2', Email='recuiteruser2@wct.com', LastName='Testing2', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='recuiteruser2@wct.com', UserRoleId=r2.Id);        
            User recutr2 = WCT_UtilTestDataCreation.createUser('rUser2',p.Id,'recuiteruser2@wct.com','recuiteruser2@wct.com');
            system.debug('Recuiter2  '+recutr2.Id);
            
            system.runAs(recutr2){
                req.WCT_Recruiter__c = recutr2.Id;
                update req;
                system.assertNotEquals(req.WCT_Recruiter__c, req.OwnerId);
                WCT_Requisition__c req2 = new WCT_Requisition__c();
                req.WCT_Recruiter__c = recutr1.Id;
                req.Ownerid = recutr2.Id;
                test.stopTest();
            }   
        }
    }
    
}