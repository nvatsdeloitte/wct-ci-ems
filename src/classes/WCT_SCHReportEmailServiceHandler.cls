global class WCT_SCHReportEmailServiceHandler implements Messaging.InboundEmailHandler{

    //-------------------------------------
    //   Method to handle incoming emails 
    //-------------------------------------  

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.Inboundenvelope envelope) {
        //Variable Declaration
        system.debug('email'+email);
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        //This convention is used when sending invite
        String identifier;
        String emailSubject = email.subject.trim();
        String identifierString;
        String emailBody;
        string schReportIdentifier = 'SCH-';
        boolean schReport = false;
        if(email.plainTextBody != null && email.plainTextBody !=''){
            emailBody = 'FromAddress: ' + email.fromAddress +'\n' + email.plainTextBody;
        }

        //---------------------------------------------------------
        //    Processing email subject appropriately as required
        //---------------------------------------------------------

        List<String> stringSubjectList=new List<String>();
        if(emailSubject != null){

            if(emailSubject.Contains(schReportIdentifier)){
                emailSubject = emailSubject.replace(']','');
                emailSubject = emailSubject.replace('[','');
                stringSubjectList = emailSubject.split('ID:');            

                for(String s : stringSubjectList){
                    s=s.trim();
                    identifierString = s; 
                }
                schReport = true;
            }

        }


        try{
            if(schReport){
                system.debug('emailschrep2');
                List<Document> docToInsert = new List<Document>();
                for (Messaging.InboundEmail.BinaryAttachment binayAttachment : email.binaryAttachments) {
                    Document doc = new Document();
                    doc.Name = binayAttachment.fileName;
                    doc.Description = 'Document created for schedule report '+ binayAttachment.fileName+' created at '+system.now().format('MM/dd/yyyy HH:mm:ss');
                    doc.Body = binayAttachment.body;
                    doc.folderId = SYSTEM.LABEL.Schedule_Reports_folder;
                    //doc.OwnerId = t.OwnerId;
                    docToInsert.add(doc);
                }
                if(!docToInsert.isEmpty()){
                    insert docToInsert;
                }
            }


        }Catch(DmlException e){  
            WCT_ExceptionUtility.logException('WCT_SCHReportEmailServiceHandler', 'SCH Report Email Service', 'Exception in doc insert'+ emailSubject + '\n'+ emailBody);
        }

        return result;

    }

}