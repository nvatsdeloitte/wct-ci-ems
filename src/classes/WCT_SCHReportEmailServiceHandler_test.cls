@isTest
public class WCT_SCHReportEmailServiceHandler_test{
    static testMethod void testES(){
    Test.startTest();
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope envelope= new Messaging.InboundEnvelope();
        
        email.fromAddress = 'someaddress@email.com';
        email.plainTextBody = 'email body\n2225256325\nTitle';
        email.subject = 'SCH-';
        
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
        
        WCT_SCHReportEmailServiceHandler sches = new WCT_SCHReportEmailServiceHandler();
        sches.handleInboundEmail(email,envelope);
    Test.stopTest();
    }
    
}