/*
    * Class Name  : WCT_SaveTheDateEmailServiceHandler 
    * Description : This is the email service class to handle responses for Meeting 
    *               Invite sent to Interviewers 

*/
global class WCT_SaveTheDateEmailServiceHandler implements Messaging.InboundEmailHandler{
    
    global static  Boolean isAccepted = false;
    global static  Boolean isDeclined = false;
    //Method to handle incoming emails    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.Inboundenvelope envelope) {
        //Variable Declaration
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        String ACCEPTED_STATUS = 'Accepted';
        String DECLINED_STATUS = 'Declined';
        //This convention is used when sending invite
        String  EventIdentifier = 'STD-';
        String EventId ;
        String emailSubject = email.subject.trim();
        String EventIdentifierString ;
        String fromAddress = email.fromAddress;
        List<EventRelation> liEventRelations= new List<EventRelation>();
        List<EventRelation> liEventRelationsToUpdate= new List<EventRelation>();
        string emailBody;
        List<User> userList = new List<User>();
        Set<Id> userIdSet = new Set<Id>();
        system.debug('plaintext'+email.plainTextBody);
        system.debug('Htmltext'+email.htmlBody);
        if(email.plainTextBody != null && email.plainTextBody !=''){
            emailBody = email.plainTextBody;
        }else if(email.htmlBody !=null){
                string html=email.htmlBody;
                string resultStr = html.replaceAll('<br/>', '\n');
                resultStr = resultStr.replaceAll('<br />', '\n');
                resultStr = resultStr.replaceAll('&nbsp;', '');
                resultStr = resultStr.replaceAll('&gt;', '');
                resultStr = resultStr.replaceAll('&lt;', '');
                //string HTML_TAG_PATTERN = '<.*?>';
                string HTML_TAG_PATTERN = '<[^>]+>|&nbsp;';
                pattern myPattern = pattern.compile(HTML_TAG_PATTERN);
                matcher myMatcher = myPattern.matcher(resultStr);
                resultStr = myMatcher.replaceAll('');
                emailBody = resultStr ;
        }
        //Get All users based on from Address of email , since 
        try{
            userList = [SELECT id,email from User where email=:fromAddress];
        }Catch(QueryException queryException){ 
            WCT_ExceptionUtility.logException('Email Service Handler','InboundEventEmailHandler',queryException.getMessage());
            
        }
        if(!userList.IsEmpty()){
            for(User u:userList){
                userIdSet.add(u.id);
            }
        }

        //Processing email subject appropriately as required
        List<String> stringSubjectList=new List<String>();
        if(emailSubject != null){
            if(emailSubject.startsWith(ACCEPTED_STATUS)){
                isAccepted = true;
            }else if(emailSubject.startsWith(DECLINED_STATUS)){
                isDeclined = true;
            }
            if(emailSubject.Contains(EventIdentifier)){
                emailSubject = emailSubject.replace(']','');
                emailSubject = emailSubject.replace('[','');
                stringSubjectList = emailSubject.split(':');            
            }
            for(String s : stringSubjectList){
                s=s.trim();
                if(s.startsWith(EventIdentifier)){
                    EventIdentifierString = s;
                    //break;
                }
            }
        }

        try{
            liEventRelations = [SELECT EventId, RelationId, Event.WCT_Save_The_Date_EventId__c,Event.WhatId,Event.What.Name,Status FROM EventRelation where IsInvitee = true AND Event.WCT_Save_The_Date_EventId__c = :EventIdentifierString];
        }Catch(QueryException queryException){  
            WCT_ExceptionUtility.logException('Email Service Handler','InboundEventEmailHandler',queryException.getMessage());
        }  

        for(EventRelation er:liEventRelations ){

            if(userIdSet.Contains(er.RelationId)){
                if(isAccepted){
                    er.status = ACCEPTED_STATUS;
                    if(emailBody!=null && emailBody!=''){ er.Response = emailBody;}
                    liEventRelationsToUpdate.add(er); 
                }else if(isDeclined){
                    er.status = DECLINED_STATUS;
                    if(emailBody!=null && emailBody!=''){ er.Response = emailBody;}
                    liEventRelationsToUpdate.add(er); 
                }
            }
        }
        try{
            update liEventRelationsToUpdate;
            result.success = true;
            
        }Catch(DmlException dmlException){  
            WCT_ExceptionUtility.logException('Email Service Handler','InboundEventEmailHandler',dmlException.getMessage());
        } 
        return result;


    }
    
}