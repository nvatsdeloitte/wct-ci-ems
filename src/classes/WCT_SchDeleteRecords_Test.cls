/**
    * Class Name  : WCT_StageTableBatchDelete_Test
    * Description : This apex test class will use to test the StageTableBatchDelete
*/
@isTest
private class WCT_SchDeleteRecords_Test {
	
	/** 
        Method Name  : schedulingBatch
        Return Type  : testMethod
        Type 		 : private
        Description  : Create Pre-Bi Stage Table Records.         
    */
    static testMethod void schedulingBatch() {
        Test.startTest();
        WCT_SchDeleteRecords m = new WCT_SchDeleteRecords();
		String sch = '20 30 8 10 2 ?';
		String jobID = system.schedule('Merge Job', sch, m);
		Test.stopTest();
    }
}