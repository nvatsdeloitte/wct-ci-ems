global class WCT_Select_Contacts
{
  public List<Id> IdList{get;set;} 
  public List<Select_Contacts_Wrapper> SCW_List{get;set;}
  public Id SelectedId; 
  public String sUserID = UserInfo.getUserId();
  public String Heading{get;set;}
  public String actionTobePerformed='';    
  public string ReplytoEmail; 
  public EmailServicesAddress EmailSerAdd;
  public boolean showcontactsflag{get;set;}
  public boolean renderselectbuttonflag{get;set;}
  public boolean showTasksflag{get;set;}
  public boolean showEmailsflag{get;set;}
  public List <Task> cNewTask {get;set;} 
  Public Map<id,WCT_Outreach_Activity__c> Contact_OaMap{get;set;}    
  public Task newTask{get;set;} 
    public class Select_Contacts_Wrapper
    {
        public WCT_Outreach_Activity__c oavar {get; set;}
        public boolean selected {get; set;}
        
        public Select_Contacts_Wrapper(WCT_Outreach_Activity__c oaarg)
        {
            oavar=oaarg;
            selected=false;
         }
    }
   
  public WCT_Select_Contacts()
       {
        IdList = new list<Id>();       
        showcontactsflag=true;  
        newTask = new task(); 
        SelectedID=ApexPages.currentPage().getParameters().get('Id');
        actionTobePerformed= ApexPages.currentPage().getParameters().get('Type');
        cNewTask = new list < Task>();  
        List<Task> lstTasksTobeInserted=new List<Task>();    
        SCW_List = new List<Select_Contacts_Wrapper>();
         for(WCT_Outreach_Activity__c oa : [select Name, WCT_Outreach__c, WCT_Outreach_Contact_First_Name__c, WCT_Outreach_Contact_Last_Name__c,WCT_Outreach_Contact_Email__c ,WCT_Taleo_ID__c,WCT_Personnel_Number__c,WCT_Phone__c,WCT_Mobile__c,WCT_Contact__c from WCT_Outreach_Activity__c where WCT_Outreach__c=:SelectedID ])
         {
        SCW_List.add(new Select_Contacts_Wrapper(oa));
         }
            
         Contact_OaMap= new  Map<id,WCT_Outreach_Activity__c>();
         
    subjectLine= '';
    ShowAdditionalFieldValue='';
    ToFieldId='';
    ToField='';
    ToFieldEmail='';
    templateId='';
    cDoc= new Document();
    ShowselectTemplatepopup= false;
    ShowAttchPopUp=false;
    ShowAttachmentlist= new list<String>();
    ListAttachment = new List<AttachmentList>();   
    folderid = UserInfo.getOrganizationId();
    Searchtemplfiles(); 
    renderselectbuttonflag=true; 
       emailseradd = [SELECT EmailDomainName,LocalPart FROM EmailServicesAddress where LocalPart ='outreach_email_transactions' limit 1];
       ReplytoEmail = emailseradd.localpart+'@'+emailseradd.EmailDomainName;
       }
   
    public PageReference Previous()
       {
        PageReference pageRef = new PageReference('/' + SelectedId );
        pageRef.setRedirect(true); 
        return pageref;
       }            
   
    public void Next()
      {   
      
       if(!SCW_List.isempty())
          
       for(Select_Contacts_Wrapper scw :  SCW_List)
      {
        if(scw.selected==true)
         {
           //IdList.add(scw.oavar.WCT_Contact__c);
           Contact_OaMap.put(scw.oavar.WCT_Contact__c,scw.oavar);          
         }            
       }  
       
      //if(Idlist.size()>0)
        if(!Contact_OaMap.isempty())      
        {      
         showcontactsflag=false;
         renderselectbuttonflag=false;
        if(actionTobePerformed=='Create Task'){
            showTasksflag=true; 
            Heading ='Create Task';
            newTask.OwnerID = sUserID;
            newTask.WhatId=SelectedId; 
               
        } 
        if(actionTobePerformed=='Send Email'){
                showEmailsflag=true; 
                Heading='Send Email';               
        } 
    
 
         }  
        else
         {
         
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select atleast one contact');
           apexPages.addMessage(myMsg); 
        }
                     
    } 
    
        
      
    public PageReference SaveTasks()
    {
      List<Task> lstTasksTobeInserted= new List<Task>();       
      for(id conid:Contact_OaMap.KeySet())
       {
            task taskToadd = new task();
            taskToadd.OwnerID = newTask.OwnerId;
            taskToadd.Subject=newTask.Subject; 
            taskToadd.ActivityDate=newTask.ActivityDate;
            taskToadd.Description=newTask.Description;           
            taskToadd.WhatId=Contact_OaMap.get(conid).id;
            taskToadd.WhoId=conid;
            //system.debug('###########' + taskToadd.Whoid);            
            taskToadd.Status=newTask.Status;
            taskToadd.Priority=newTask.Priority;
            taskToadd.IsReminderSet=newTask.IsReminderSet;
            taskToadd.ReminderDateTime=newTask.ReminderDateTime;            
            lstTasksTobeInserted.add(taskToadd);        
       }  
         
        try
         {       
            Database.insert(lstTasksTobeInserted);
          }
        catch(Exception e)
        {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
        apexPages.addMessage(myMsg);     
        }         
        PageReference pageRef = new PageReference('/' + SelectedId );
        pageRef.setRedirect(true); //Redirecting user to the case detail page. 
        return pageRef;   
    } 
           public PageReference Cancel()
       {
        PageReference pageRef = new PageReference('/' + SelectedId );
        pageRef.setRedirect(true); 
        return pageref;
       }
        public list<Document> cTempDocToinsert= new list<Document>();
    
    global Document cDoc{get;set;}
    public String ToFieldEmail { get; set; }
    public list<string> ShowAttachmentlist{get;set;} 
    public String RelatedToFieldId { get; set; }
    public String RelatedToField { get; set; }
    public boolean IsAttachmentRetrive= false;
    public list < SelectOption > RelatedToSelectOptionValues { 
    get
    {
     list<SelectOption> tempOptions= new list<SelectOption>();
     tempOptions.add(new selectOption('case','Case'));
     return  tempOptions;       
    } 
    set; }

    public String SelectedRetaedTovalue { get; set; }
    //public String Case_TR_EM_RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get(Label.Case_TR_EM_RecordTypeId).getRecordTypeId();
    //public String Case_ELE_ES_RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get(Label.Case_ELE_ES_RecordTypeId).getRecordTypeId();
    public list <string> ccid   = new list<String>(); 
    public list<string> bccId = new list<String>();  
    public list<String>    toEmailAddress= new list<string>();     
    User usr = [Select Id, Name, Profile.Name from User where Id = :UserInfo.getUserId()];  
   list<string> DocIds=new list<string>();
   public string templateId='';
   public String sEmailTemplateID {get;set;}
   public Boolean ShowselectTemplatepopup{get;set;}
   public Boolean ShowAttchPopUp{get;set;}
   public String fileName {get; set;}
   public Blob attachedFile {get; set;} 
   public List<EmailTemplate> Templatelist { get; set; }
   public list<selectOption> Folders { 
    get
    {
             list<selectOption> option= new list<selectoption>();
             option.add(new selectOption('','--Select--'));
             option.add(new SelectOption(UserInfo.getOrganizationId(), 'Unfiled Public Email Templates'));
                    for(folder f:[SELECT Id,Name,Type FROM Folder where Type='Email'])
                    {
                            if(f.name !='CIC Reports')
                            option.add(new selectOption(f.id,f.name));
                    }
             return option;
    }
     set; 
    }
    public String folderid { get; set; }
    public String BodyHiddenContentvalue { get; set; }
    public String BodyContent { get; set; }
    public String subjectLine { get; set; }    
    public String AdditionalToFieldValue { get; set; }
    public String ShowAdditionalFieldValue { get; set; }
    public String ToFieldId { get; set; }
    public String ToField { get; set; }
    public string SelectedFromId{get;set;}
    public String caseid;
    public EmailTemplate emailTemp {get;set;}
    public string caseThreadId {get;set;}    
    public boolean clearToField {get;set;}
    public EmailMessage em {get;set;}
    public void clearToField(){
        ToFieldId = '';
        ToField = '';
        ToFieldEmail = '';
        clearToField = false;
    }

     public list < SelectOption > fromAddressValues{
        get {
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
            list < SelectOption > tempOption = new list < SelectOption > ();
                
            tempOption.add(new selectOption(UserInfo.getUserEmail(),UserInfo.getName()+'<'+UserInfo.getUserEmail()+'>'));
            tempOption.add(new selectOption(system.label.PSN_Benifits,'PSN Benefits<'+system.label.PSN_Benifits+'>'));
            if(profilename=='11_CTS_PSN_BDC' || profilename=='11_CTS_PSN_ERC'|| profilename=='System Administrator'|| profilename=='2_CIC_Manager')
            tempOption.add(new selectOption(system.label.US_Hermitage_PSN_Tier_2_Outreach,'US Hermitage PSN Tier 2 Outreach<'+system.label.US_Hermitage_PSN_Tier_2_Outreach+'>'));
            
            return tempOption;
        }
        set;
    }
 //****************************************************************************************************************
 //********************** send email method to send email**********************************************************   
    public PageReference SendEmail() {
    
         String s= ToFieldId;
         boolean edited = false; 
         toEmailAddress= new list<string>();
         list<String> AdditionalTo= new list<String>();
         List<Contact> conList = new List<Contact>();
          set<String> TempToEmails= new  set<string>();
          for(AttachmentList ListSelectedAttachment:ListAttachment )
        {
        if(ListSelectedAttachment.checkbox==true)
             {
                DocIds.add(ListSelectedAttachment.AttachmentId);
             }
        }
        list <Messaging.SingleEmailMessage> lstSendMails=new  list <Messaging.SingleEmailMessage>();
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: SelectedFromId];  
        list<EmailTemplate> et = [Select id, body, DeveloperName,encoding, HtmlValue,TemplateType from EmailTemplate where id=: sEmailTemplateID];
       for(id conid:Contact_OaMap.KeySet())
       {
         Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
         
          if(!fromAddressValues.isEmpty()){
            //message.setSenderDisplayName(SelectedFromId);
            //OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: SelectedFromId];
            if ( owea.size() > 0 ) {
                message.setOrgWideEmailAddressId(owea.get(0).Id);
            }
        }
   //**********************  Adding Custom message if there is no subject with the email. ******************
         
         if((templateId.length()==0)&&(subjectLine.length()==0))
         {
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SendEmail_subject_fieldErrorMessage);
              ApexPages.addMessage(myMsg); 
              return null;
         }  
        
   
             if(et.isempty()){ 
               String subjectText= subjectLine+''+Contact_OaMap.get(conid).name;               
               message.setSubject(subjectText);
                String BodyContentText=BodyContent+'\n\n'+Contact_OaMap.get(conid).name ;                
               message.setHtmlBody(BodyContentText);               
             if(BodyContent.length()==0)
             {
                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SendEmail_body_fieldErrorMessage);
                  ApexPages.addMessage(myMsg); 
                  return null;
              }}
              else{
              message.setTemplateId(et[0].id); 
              }
         
 //**********************  adding attachments to the email ************************************
        
        if(DocIds.size()>0)
        {
           message.setDocumentAttachments(DocIds);
        }
 //**********************  Sending email ************************************
            
            System.debug('**********'+Contact_OaMap.get(conid)+'***********'+conid); 
            message.setWhatId(Contact_OaMap.get(conid).id);
            message.setReplyTo (replytoemail);//('outreach_email_transactions@9-2gqwden8m9peoiiywozpzre5kc2j08bb8jnky24hmkdgxwpveo.e-5pa4ceaw.cs15.apex.sandbox.salesforce.com');
            message.setTargetObjectId(conid);
            message.setSaveAsActivity(false);
            System.debug('**************message'+message);                
            lstSendMails.add(message );        
       } 
       try{
       
        Messaging.SendEmailResult [] r = Messaging.sendEmail(lstSendMails);
        List<Task> lstEmailTasksTobeInserted=new List<Task>();
        for(Messaging.SingleEmailMessage email:lstSendMails){
        String taskDescription='';
            if(email.getHtmlbody()!=null){
                string html=email.getHtmlbody();
                string result = html.replaceAll('<br/>', '\n');
                result = result.replaceAll('<br />', '\n');
                result = result.replaceAll('&nbsp;', '');
                result = result.replaceAll('&gt;', '');
                result = result.replaceAll('&lt;', '');
                //string HTML_TAG_PATTERN = '<.*?>';
                string HTML_TAG_PATTERN = '<[^>]+>|&nbsp;';
                pattern myPattern = pattern.compile(HTML_TAG_PATTERN);
                matcher myMatcher = myPattern.matcher(result);
                result = myMatcher.replaceAll('');
                taskDescription = result;
                //em.TextBody = result;
                }
                if(email.getPlainTextbody() != null){
                taskDescription= email.getPlainTextbody();}
                task taskToadd = new task();            
                taskToadd.Subject=email.getSubject(); 
                taskToadd.Description=taskDescription;           
                taskToadd.WhatId=email.getWhatId();
                taskToadd.WhoId=email.gettargetObjectId();                        
                taskToadd.Status='Completed';
                taskToadd.Email_status__c='Sent';
                taskToadd.Email_Read_Unread__c=true;
                //taskToadd.Priority=newTask.Priority;
                       
            lstEmailTasksTobeInserted.add(taskToadd);
            
            }
        System.debug('**************rrrrrrrrrrr'+r);
        if(!lstEmailTasksTobeInserted.isempty()){
            insert lstEmailTasksTobeInserted;
        }
               if(tofieldId==''){
                system.debug('entered1');
                /*task t = new task();
                t.subject = subjectLine;
                t.OwnerId = userinfo.getuserId();
                t.status = 'Completed';
                t.WhatId = RelatedToFieldId;
                t.ActivityDate = system.today();
                t.recordtypeid = '01240000000DuEb';
                insert t;
                system.debug('entered2'+t);*/
               
          
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, r+'>Message Id>'+lstSendMails));   
     
         }}catch(Exception e )
         {
          if(e.getMessage().contains('INVALID_EMAIL_ADDRESS'))
          {
                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SendEmail_Invalid_Email_Id);
                  ApexPages.addMessage(myMsg); 
                  return null;
          }else{
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
           return null;
           }           
         }  
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Support Request submitted successfully.')); 
        list<Document> DocToDelete=[select id from Document where id In : DocIds];
        delete DocToDelete;  
        pagereference PageRef = new pagereference('/'+SelectedId);
        PageRef.setRedirect(true);
        return PageRef;
    
    }


//**********************  Method to close Attachment Popup  ************************************
 
    public PageReference CloseAttachmentPopup() {
        ShowAttchPopUp=false;
        return null;
    }
     public PageReference CloseTempPopup() {
        ShowselectTemplatepopup=false;
        return null;
    }
//**********************  Method to open Attachment Popup  ************************************


    public PageReference OpenAttachmentPopup() {
        ShowAttchPopUp=true;
        return null;
    }
//**********************  Method to attach a file/ createing document  ************************************

    public PageReference save() {
      
    if(cDoc.Name!=null){
        cDoc.FolderId=Label.Attachment_Zip_Document_Folder_Id;
        insert cDoc;
        ShowAttachmentlist.add(cDoc.Name);
       // DocIds.add(cDoc.id);
        ListAttachment.add(new AttachmentList(true,cDoc.Name,cDoc.id));
        cDoc= new Document();
      }
    return null;
    }


//**********************  Method to attach a file/ createing document  ************************************
  public PageReference OpenSelectTemplatePopup() {
        ShowselectTemplatepopup=true;
        return null;
    }
 //**********************  Setting selected template on VF page  ************************************
  
   public PageReference SelectTemplate() {
           
            emailTemp = new EmailTemplate();
         for(EmailTemplate temp : [SELECT Id,Name,Body,subject,FolderId,HtmlValue,Encoding,TemplateType FROM EmailTemplate where id=: sEmailTemplateID ]){
               
               Messaging.SingleEmailMessage tempMessage = new Messaging.SingleEmailMessage();
               tempMessage.setTemplateId(temp.id);
               //tempMessage.setTargetObjectId('003f000000DcI49'); 
               //tempMessage.setTargetObjectId('003f000000E15tM');
               List<WCT_Outreach_Activity__c> colors = new List<WCT_Outreach_Activity__c>();
               colors = Contact_OaMap.values();  
               tempMessage.setWhatId(colors[0].id);
               Savepoint sp = Database.setSavepoint();
               if(ToFieldId==''){
               //contact c = new contact(recordtypeid='01240000000DuEW',lastname='.');
               contact c = new contact(recordtypeid='01240000000DuEZ',lastname='.');
               c.email='test@gmail.com';
               //Savepoint sp = Database.setSavepoint();
               insert c;
               tempMessage.setTargetObjectId(c.id);
               }else{
               //templateid = ToFieldId.substring(0,15);
               system.debug('tofield'+ToFieldId.substring(0,15)+'&&'+ToFieldId);
               tempMessage.setTargetObjectId(ToFieldId.substring(0,15));}
               Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {tempMessage});
               
               //body = tempMessage.gethtmlbody();
               //subject=tempMessage.subject;
               //database.rollback(sp);
               
               
               if(temp.TemplateType=='HTML'){ BodyContent=tempMessage.gethtmlbody();
                //BodyContent=BodyContent.replaceAll(']]>','\n\n\n');
                //string tempBodyContent=EncodingUtil.urlEncode(temp.HtmlValue, temp.Encoding);
                //BodyContent='<html>'+EncodingUtil.urlDecode(tempBodyContent, temp.Encoding)+'</html>';
                subjectLine=tempMessage.subject;
                //templateId=temp.Id;
                //emailTemp.HtmlValue = temp.HtmlValue;
                //emailTemp.body = temp.body;
                }else if(temp.TemplateType=='Text')
                {
                BodyContent=tempMessage.getPlaintextBody();
                subjectLine=tempMessage.subject;
                //templateId=temp.Id;
                //emailTemp.body = temp.body;
                }
                 database.rollback(sp);          
                }
                ShowselectTemplatepopup=false;
                return null;
    }


//********************** Searching templates  ************************************
 

    public void Searchtemplfiles() {
        Templatelist = new list<EmailTemplate>();
                if(folderid!= null && folderid!='')
                    {
                       for(EmailTemplate et:[ SELECT Id,Name,Body,FolderId,Description,HtmlValue,TemplateType FROM EmailTemplate where FolderId=:folderid])
                       {
                         Templatelist.add(et); 
                        } 
                
                    }
          // return null;
        }


  
public list<AttachmentList> ListAttachment {get; set;}
public void AttachementListFormCase()
     {
         if(!IsAttachmentRetrive){
          list<Attachment> list_Case_attachment=[select id, Name, Body  from Attachment where Attachment.ParentId =:caseid];
          for(Attachment at : list_Case_attachment){
           //  ShowAttachmentlist.add(at.Name);
           if(at.body!=null){
             Document doc= new Document();
             doc.Name=at.Name;
             doc.body=at.body;
             doc.FolderId=Label.Attachment_Zip_Document_Folder_Id;
             cTempDocToinsert.add(doc);
             }
          }
          list<WCT_Notes__c> list_Case_note=[select  WCT_Description__c, id, WCT_Subject__c  from WCT_Notes__c  where Case__c =:caseid];
           for(WCT_Notes__c  at : list_Case_note){
            if(at.WCT_Description__c!=null){
             //ShowAttachmentlist.add(at.WCT_Subject__c);
             Document doc= new Document();
             doc.Name=at.WCT_Subject__c;
             doc.body=Blob.valueof(at.WCT_Description__c);
             doc.FolderId=Label.Attachment_Zip_Document_Folder_Id;
             doc.ContentType='text/plain';
             doc.type = 'txt';
             cTempDocToinsert.add(doc);
             }
          }
          
          insert cTempDocToInsert;
          for(Document d: cTempDocToInsert)
          {
     //          DocIds.add(d.id);
             ListAttachment.add(new AttachmentList(false, d.Name,d.id));
           
          }
         cTempDocToInsert.clear();
         IsAttachmentRetrive=true;
         }
         
     }
     //Convert List of strings to string
     public string conListOfStr(list<string> strList){
        string retString = '';
        for(string s : strList){
            retString += s + ';'+'\n';
        }   
        return retString;
     }
     public void checkall(){
     return ;
     }
     public class AttachmentList
     {
        public boolean checkbox{get;set;}
        public String  AttachmentName{get;set;}
        public String  AttachmentId{get;set;}
        
        public AttachmentList(Boolean isSelected, String Name,String id)
        {
             checkbox= isSelected;
             AttachmentName=name;
             AttachmentId= id;
             
        }
        
     }      
 }