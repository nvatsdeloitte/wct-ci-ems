@isTest
public class WCT_Select_Contacts_Test
{
    public static testmethod void nocontactsselected()
    {
        Test.StartTest();
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='testoast@deloitte.com';
        con.WCT_Person_Id__c='104032';
        insert con; 
        PageReference pageRef = Page.WCT_Select_Contacts;
        Test.setCurrentPage(pageRef); 
        WCT_Outreach__c Outreach = WCT_UtilTestDataCreation.CreateOutreach();
        insert Outreach;
        WCT_Outreach_Activity__c oa = WCT_UtilTestDataCreation.createOA();
        oa.WCT_Contact__c=con.id;
        oa.WCT_Outreach__c=Outreach.id;
        insert oa;
        ApexPages.CurrentPage().getParameters().put('id',Outreach.id);
        WCT_Select_Contacts controller = new WCT_Select_Contacts();
        //controller.outreachId=Outreach.Id;
        controller.Previous();
        controller.Next();
        controller.Cancel();
        Test.StopTest();
     }
       public static testmethod void contactselectedcreatetask()
    {
        Test.StartTest();
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='testoast@deloitte.com';
        con.WCT_Person_Id__c='104032';
        insert con; 
        PageReference pageRef = Page.WCT_Select_Contacts;
        Test.setCurrentPage(pageRef); 
        WCT_Outreach__c Outreach = WCT_UtilTestDataCreation.CreateOutreach();
        insert Outreach;
        WCT_Outreach_Activity__c oa = WCT_UtilTestDataCreation.createOA();
        oa.WCT_Contact__c=con.id;
        oa.WCT_Outreach__c=Outreach.id;
        insert oa;
        ApexPages.CurrentPage().getParameters().put('id',Outreach.id);
        WCT_Select_Contacts controller = new WCT_Select_Contacts();
        //controller.outreachId=Outreach.Id;
        controller.Previous();
        controller.SCW_List[0].selected=true;
        controller.actionTobePerformed='Create Task';
        controller.Next();
        controller.SaveTasks();
        Test.StopTest();
     }
     public static testmethod void contactselectedsendemail()
    {
        Test.StartTest();
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='testoast@deloitte.com';
        con.WCT_Person_Id__c='104032';
        insert con; 
        Case testCase = WCT_UtilTestDataCreation.createCase(String.ValueOf(con.id));
        //insert testCase;
        Attachment testAttachment = WCT_UtilTestDataCreation.createAttachment(testCase.id);
        //insert testAttachment;
        WCT_Notes__c testNote = new WCT_Notes__c();
        testNote.Case__c = testCase.id;
        testNote.WCT_Description__c='TestDescription';
        testNote.WCT_Subject__c='TestSubject';
        insert testNote;
        /*EmailTemplate et = new EmailTemplate();
        et.body='Test';
        et.DeveloperName='Test';
        et.encoding='UTF-8';
        //et.HtmlValue,TemplateType
        insert et;*/
        String fId = [Select Id from Folder where DeveloperName='CIC_Email_Template'].Id;
        list<EmailTemplate> em= [SELECT Id,Name,Body,FolderId,HtmlValue,TemplateType FROM EmailTemplate where FolderId =:fId Limit 1];
        PageReference pageRef = Page.WCT_Select_Contacts;
        Test.setCurrentPage(pageRef); 
        WCT_Outreach__c Outreach = WCT_UtilTestDataCreation.CreateOutreach();
        insert Outreach;
        WCT_Outreach_Activity__c oa = WCT_UtilTestDataCreation.createOA();
        oa.WCT_Contact__c=con.id;
        oa.WCT_Outreach__c=Outreach.id;
        insert oa; 
        ApexPages.CurrentPage().getParameters().put('id',Outreach.id);
        WCT_Select_Contacts controller = new WCT_Select_Contacts();
        //controller.outreachId=Outreach.Id;
        controller.BodyContent='Test';
        controller.sEmailTemplateID=em[0].id;
        controller.templateId=em[0].id;
        controller.Previous();
        controller.SCW_List[0].selected=true;
        controller.actionTobePerformed='Send Email';
        controller.Next();
        controller.cDoc = WCT_UtilTestDataCreation.createDocument();
        controller.save();
        controller.SelectTemplate();
        controller.SendEmail();
        controller.AttachementListFormCase();
        
        }
        Test.StopTest();
     }
     public static testmethod void contactselectedsendemailwithattachment()
    {
        Test.StartTest();
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='testoast@deloitte.com';
        con.WCT_Person_Id__c='104032';
        insert con; 
        Case testCase = WCT_UtilTestDataCreation.createCase(String.ValueOf(con.id));
        insert testCase;
        Attachment testAttachment = WCT_UtilTestDataCreation.createAttachment(testCase.id);
        insert testAttachment;
       
        WCT_Notes__c testNote = new WCT_Notes__c();
        testNote.Case__c = testCase.id;
        testNote.WCT_Description__c='TestDescription';
        testNote.WCT_Subject__c='TestSubject';
        insert testNote;
        /*EmailTemplate et = new EmailTemplate();
        et.body='Test';
        et.DeveloperName='Test';
        et.encoding='UTF-8';
        //et.HtmlValue,TemplateType
        insert et;*/
        String fId = [Select Id from Folder where DeveloperName='CIC_Email_Template'].Id;
        //list<EmailTemplate> em= [SELECT Id,Name,Body,FolderId,HtmlValue,TemplateType FROM EmailTemplate where FolderId =:fId Limit 1];
        PageReference pageRef = Page.WCT_Select_Contacts;
        Test.setCurrentPage(pageRef); 
        WCT_Outreach__c Outreach = WCT_UtilTestDataCreation.CreateOutreach();
        insert Outreach;
        WCT_Outreach_Activity__c oa = WCT_UtilTestDataCreation.createOA();
        oa.WCT_Contact__c=con.id;
        oa.WCT_Outreach__c=Outreach.id;
        insert oa; 
        ApexPages.CurrentPage().getParameters().put('id',Outreach.id);
        WCT_Select_Contacts controller = new WCT_Select_Contacts();
        Controller.caseid = testCase.id;
        //controller.outreachId=Outreach.Id;
        controller.BodyContent='Test';
        //controller.sEmailTemplateID=em[0].id;
        //controller.templateId=em[0].id;
        controller.Previous();
        controller.SCW_List[0].selected=true;
        controller.actionTobePerformed='Send Email';
        controller.Next();
        controller.cDoc = WCT_UtilTestDataCreation.createDocument();
        controller.save();
        controller.SelectTemplate();
        controller.SendEmail();
        controller.AttachementListFormCase();
        
        }
        Test.StopTest();
     }
     public static testmethod void testWCT_OutReachTrg()
    {
        Test.StartTest();
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='testoast@deloitte.com';
        con.WCT_Person_Id__c='104032';
        insert con; 
        PageReference pageRef = Page.WCT_Select_Contacts;
        Test.setCurrentPage(pageRef); 
        WCT_Outreach__c Outreach = WCT_UtilTestDataCreation.CreateOutreach();
        insert Outreach;
        WCT_Outreach_Activity__c oa = WCT_UtilTestDataCreation.createOA();
        oa.WCT_Contact__c=con.id;
        oa.WCT_Outreach__c=Outreach.id;
        insert oa;
        ApexPages.CurrentPage().getParameters().put('id',Outreach.id);
        WCT_Select_Contacts controller = new WCT_Select_Contacts();
        //controller.outreachId=Outreach.Id;
        controller.SCW_List[0].selected=true;
        controller.actionTobePerformed='Create Task';
        try
        {
        Outreach.WCT_Status__c = '5. Completed';
        controller.newTask.WhatId=Outreach.id;
        controller.newTask.Status='Deferred';
        update Outreach; 
        }
        catch(Exception e)
        {
            System.assert(e.getMessage().contains('All Open Tasks must be "Completed" before Outreach can be "Completed".'));
        } 
        
        controller.Next();
        controller.SaveTasks();
        Test.StopTest();
     }
     public static testmethod void testWCT_OutreachActivityTigger()
    {
        Test.StartTest();
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='testoast@deloitte.com';
        con.WCT_Person_Id__c='104032';
        insert con; 
        PageReference pageRef = Page.WCT_Select_Contacts;
        Test.setCurrentPage(pageRef); 
        WCT_Outreach__c Outreach = WCT_UtilTestDataCreation.CreateOutreach();
        insert Outreach;
        WCT_Outreach_Activity__c oa = WCT_UtilTestDataCreation.createOA();
        oa.WCT_Contact__c=con.id;
        oa.WCT_Outreach__c=Outreach.id;
        insert oa;
        Task t=new Task();
        t.status='Deferred';
        t.whatid=oa.id;
        insert t;
        ApexPages.CurrentPage().getParameters().put('id',Outreach.id);
        WCT_Select_Contacts controller = new WCT_Select_Contacts();
        //controller.outreachId=Outreach.Id;
        controller.SCW_List[0].selected=true;
        controller.actionTobePerformed='Create Task';
        try
        {
        oa.WCT_Status__c = 'Completed';
        controller.newTask.WhatId=oa.id;
        controller.newTask.Status='Deferred';
        update oa; 
        }
        catch(Exception e)
        {
            System.assert(e.getMessage().contains('All Open Tasks must be "Completed" before Outreach Activity can be "Completed".'));
        } 
        
        controller.Next();
        controller.SaveTasks();
        Test.StopTest();
        
    }
    static testMethod void SendEmailWithoutTemplateUnitTest() {
        Test.StartTest();
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='testoast@deloitte.com';
        con.WCT_Person_Id__c='104032';
        insert con; 
        PageReference pageRef = Page.WCT_Select_Contacts;
        Test.setCurrentPage(pageRef); 
        WCT_Outreach__c Outreach = WCT_UtilTestDataCreation.CreateOutreach();
        insert Outreach;
        WCT_Outreach_Activity__c oa = WCT_UtilTestDataCreation.createOA();
        oa.WCT_Contact__c=con.id;
        oa.WCT_Outreach__c=Outreach.id;
        insert oa; 
        ApexPages.CurrentPage().getParameters().put('id',Outreach.id);
        WCT_Select_Contacts controller = new WCT_Select_Contacts();
        //controller.outreachId=Outreach.Id;
        controller.SCW_List[0].selected=true;
        controller.subjectLine='Test';
        Controller.BodyContent='Test Email body';
        controller.actionTobePerformed='Send Email';
        controller.SendEmail();
        Test.StopTest();
     }
      static testMethod void AttachmentUnitTest() {
         Test.StartTest();
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='testoast@deloitte.com';
        con.WCT_Person_Id__c='104032';
        insert con; 
        PageReference pageRef = Page.WCT_Select_Contacts;
        Test.setCurrentPage(pageRef); 
        WCT_Outreach__c Outreach = WCT_UtilTestDataCreation.CreateOutreach();
        insert Outreach;
        WCT_Outreach_Activity__c oa = WCT_UtilTestDataCreation.createOA();
        oa.WCT_Contact__c=con.id;
        oa.WCT_Outreach__c=Outreach.id;
        insert oa; 
        ApexPages.CurrentPage().getParameters().put('id',Outreach.id);
        WCT_Select_Contacts controller = new WCT_Select_Contacts();
        String TestStringBody = 'StringToBlob';
        Blob TestBody = Blob.valueof(TestStringBody);
        Controller.OpenAttachmentPopup();
        Controller.cDoc.Body=TestBody;
        Controller.cDoc.Name='Test';
        Controller.save();
        system.assertNotEquals(Controller.ShowAttachmentlist.size(),0);
        Controller.CloseAttachmentPopup();
        Controller.CloseTempPopup();
        Test.StopTest();
    }   
     static testMethod void AddTemplateUnitTest() {
        Test.StartTest();
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='testoast@deloitte.com';
        con.WCT_Person_Id__c='104032';
        insert con; 
        //OrgWideEmailAddress owea=new OrgWideEmailAddress(Address='Tan@x.com', DisplayName='US ELE Transfers', IsAllowAllProfiles=true);
        /*OrgWideEmailAddress owea = new OrgWideEmailAddress();
        owea.Address='testoast@deloitte.com'; 
        insert owea;*/
        WCT_Outreach__c Outreach = WCT_UtilTestDataCreation.CreateOutreach();
        insert Outreach;
        WCT_Outreach_Activity__c oa = WCT_UtilTestDataCreation.createOA();
        oa.WCT_Contact__c=con.id;
        oa.WCT_Outreach__c=Outreach.id;
        insert oa;
        PageReference pageRef = Page.WCT_Select_Contacts;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('id',Outreach.id);
        WCT_Select_Contacts controller = new WCT_Select_Contacts();
        controller.SelectedFromId='testoast@deloitte.com';
        controller.SCW_List[0].selected=true;
        controller.actionTobePerformed='Send Email';
        List<Selectoption> sb = Controller.Folders;
        Controller.OpenSelectTemplatePopup();
        Controller.folderid=Label.Attachment_Zip_Document_Folder_Id;
        system.assertNotEquals(Controller.folderid,null);
        Controller.Searchtemplfiles();
        list<EmailTemplate> em= [SELECT Id,Name,Body,FolderId,HtmlValue,TemplateType FROM EmailTemplate];
        String TextEmailTempId;
        for(EmailTemplate lpEm : em)
        {
            if(lpEm.TemplateType=='HTML')
            {
                Controller.sEmailTemplateID=lpEm.Id;
            }else
            {
                TextEmailTempId=lpEm.Id;
            }
        }
        //Controller.SelectTemplate();
        //Controller.sEmailTemplateID=TextEmailTempId; 
        //Controller.SelectTemplate();
        Test.StopTest();
        }
    }
    static testMethod void SendEmailExceptionUnitTest() {
       
        test.StartTest();
        test.setCurrentPage(page.WCT_Select_Contacts);
        
        WCT_Select_Contacts controller = new WCT_Select_Contacts();
        system.assertEquals(Controller.ToFieldEmail, '');
        Controller.clearToField();
        /*Controller.ToFieldEmail='';
        Controller.ToField='';
        Controller.ToFieldId='';
        Controller.SendEmail();*/
        Test.StopTest();
    
    } 
    static testMethod void SendEmailSubjectExceptionUnitTest() {
       
       test.StartTest();
        test.setCurrentPage(page.WCT_Select_Contacts);
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='testoast@deloitte.com';
        con.WCT_Person_Id__c='104032';
        insert con;
        PageReference pageRef = Page.WCT_Select_Contacts;
        Test.setCurrentPage(pageRef); 
        WCT_Outreach__c Outreach = WCT_UtilTestDataCreation.CreateOutreach();
        insert Outreach;
        WCT_Outreach_Activity__c oa = WCT_UtilTestDataCreation.createOA();
        oa.WCT_Contact__c=con.id;
        oa.WCT_Outreach__c=Outreach.id;
        insert oa; 
        ApexPages.CurrentPage().getParameters().put('id',Outreach.id);
        WCT_Select_Contacts controller = new WCT_Select_Contacts();
        
        Controller.ToFieldId=con.id;
        Controller.subjectLine='';
        Controller.templateID='';
        system.assertEquals(Controller.subjectLine, '');
        
        Controller.SendEmail();
        Test.StopTest();
   }  
   static testMethod void SendEmailWithoutBodyExceptionUnitTest() {
       
        test.StartTest();        
        test.setCurrentPage(page.WCT_Select_Contacts);
        
        WCT_Select_Contacts controller = new WCT_Select_Contacts();
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='testoast@deloitte.com';
        con.WCT_Person_Id__c='104032';
        insert con;
        Controller.ToFieldId=con.id;
        Controller.subjectLine='Test Email';
        system.assertEquals(Controller.BodyContent, null);
        Controller.BodyContent='';
        Controller.SendEmail();
        test.StopTest();
            
   }
    public static testmethod void contactselectedsendemailwithouttemplate()
    {
        Test.StartTest();
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='testoast@deloitte.com';
        con.WCT_Person_Id__c='104032';
        insert con; 
        Case testCase = WCT_UtilTestDataCreation.createCase(String.ValueOf(con.id));
        //insert testCase;
        Attachment testAttachment = WCT_UtilTestDataCreation.createAttachment(testCase.id);
        //insert testAttachment;
        WCT_Notes__c testNote = new WCT_Notes__c();
        testNote.Case__c = testCase.id;
        testNote.WCT_Description__c='TestDescription';
        testNote.WCT_Subject__c='TestSubject';
        insert testNote;
        /*EmailTemplate et = new EmailTemplate();
        et.body='Test';
        et.DeveloperName='Test';
        et.encoding='UTF-8';
        //et.HtmlValue,TemplateType
        insert et;*/
        String fId = [Select Id from Folder where DeveloperName='CIC_Email_Template'].Id;
        list<EmailTemplate> em= [SELECT Id,Name,Body,FolderId,HtmlValue,TemplateType FROM EmailTemplate where FolderId =:fId Limit 1];
        PageReference pageRef = Page.WCT_Select_Contacts;
        Test.setCurrentPage(pageRef); 
        WCT_Outreach__c Outreach = WCT_UtilTestDataCreation.CreateOutreach();
        insert Outreach;
        WCT_Outreach_Activity__c oa = WCT_UtilTestDataCreation.createOA();
        oa.WCT_Contact__c=con.id;
        oa.WCT_Outreach__c=Outreach.id;
        insert oa; 
        ApexPages.CurrentPage().getParameters().put('id',Outreach.id);
        WCT_Select_Contacts controller = new WCT_Select_Contacts();
        //controller.outreachId=Outreach.Id;
        controller.BodyContent='Test';
        //controller.sEmailTemplateID=em[0].id;
        controller.templateId=em[0].id;
        controller.Previous();
        controller.SCW_List[0].selected=true;
        controller.actionTobePerformed='Send Email';
        controller.Next();
        controller.cDoc = WCT_UtilTestDataCreation.createDocument();
        controller.save();
        controller.SelectTemplate();
        controller.SendEmail();
        controller.AttachementListFormCase();
        
        }
        Test.StopTest();
     }
    public static testmethod void testconListOfStr()
    {
        List<String> TestStringList = new List<String>();
        TestStringList.add('abcde');
        Test.StartTest();
        WCT_Select_Contacts controller = new WCT_Select_Contacts();
        //controller.outreachId=Outreach.Id;
        controller.conListOfStr(TestStringList);
        controller.checkall();
        Test.StopTest();
    }
    
}