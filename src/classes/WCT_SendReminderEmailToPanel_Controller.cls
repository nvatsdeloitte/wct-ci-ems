/**
 * Class Name  : WCT_SendReminderEmailToPanel_Controller   
 * Description : To send Reminder Email to Panel of Interview 
 */
 public class WCT_SendReminderEmailToPanel_Controller{
 
    //Variable Declaration
    public string IntvId {get;set;}
    public Event eventOnInterview {get;set;}
    public list<EventRelation> inviteeList {get;set;}
    public list<InviteeWrapper> inviteeWrapperList {get;set;}
    public String templateBody {get;set;}
    public EmailTemplate et;
    public List<Messaging.SingleEmailMessage> emailList ;
    public List<String> toAdd;
    
    //Controller
    public WCT_SendReminderEmailToPanel_Controller(){
        
        IntvId = ApexPages.currentPage().getParameters().get('id');
        if(IntvId == null){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Provide Interview Id.'));
            return;
        }
        inviteeWrapperList = new list<InviteeWrapper>();
        inviteeList = new list<EventRelation>();
        try{
            eventOnInterview = [SELECT id,Owner.name,EndDateTime,StartDateTime,Subject FROM Event where WhatId = :IntvId Limit 1];
        
            
             inviteeList = [SELECT AccountId,EventId,IsInvitee,RelationId,Relation.Email,Relation.name,Status, Event.Description, Event.EndDateTime, 
                                    Event.Location,  Event.StartDateTime, Event.What.Name,Event.WCT_Invite_Sequence__c, Event.IsDateEdited__c, Event.Subject 
                                    FROM EventRelation 
                                    WHERE Event.WhatId = :IntvId and IsInvitee=true
                                    and Relation.type = 'User'];     
             //et = [SELECT id,HtmlValue,FolderId FROM EmailTemplate where Name='Reminder Mail for Panel'];
            //templateBody = et.HtmlValue;                        
        }catch(Exception ex){}
        if(!inviteeList.isEmpty()){
        for(EventRelation evRel:inviteeList){
            if(evRel.Status != 'Declined'){
                inviteeWrapperList.add(new InviteeWrapper(true,evRel));
            }else{
                inviteeWrapperList.add(new InviteeWrapper(false,evRel));
            }       
        }
        }
    }
    
    //Wrapper Class
    public class InviteeWrapper{
        public boolean isSelected {get;set;}
        public EventRelation inviteeRec {get;set;}
        
        public InviteeWrapper(boolean selected,EventRelation ev){
            isSelected = selected;
            inviteeRec = ev;
        }
    
    }
    //Convert List of strings to string
     public string convListOfStr(list<string> strList){
        string retString = '';
        for(string s : strList){
            retString += s + ';'+'\n';
        }   
        return retString;
     }
    
    public string candTable(Id intvId){
        string htmlTable = '';
        Set<Id> candTrakIdSet = new Set<Id>();
        for(WCT_Interview_Junction__c ij:[SELECT id,WCT_Candidate_Tracker__r.WCT_Contact__r.name,WCT_Candidate_Tracker__r.WCT_Requisition__r.name 
                                            FROM WCT_Interview_Junction__c WHERE WCT_Interview__c =:intvId]){
            candTrakIdSet.add(ij.WCT_Candidate_Tracker__c);
        }
        if(!candTrakIdSet.isEmpty()){
            integer count = 0;
            for(WCT_Candidate_Requisition__c ct:[SELECT id,WCT_Contact__r.name,WCT_Requisition__r.name FROM WCT_Candidate_Requisition__c 
                                                    WHERE Id IN :candTrakIdSet]){
                count++;                                    
                if(htmlTable == ''){
                    htmlTable= 'Interview is scheduled with following candidates.<Br/><Br/>'+
                     '<table id="tableRecords" border="1">'+
                        '<thead>'+
                            '<tr>'+
                                '<th >S.No</th>'+
                                '<th >Candidate Name</th>'+
                                '</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody>';
                    
                }//else{
                    htmlTable += '<tr><td>'+count+'</td>'+
                                  '<td>'+ct.WCT_Contact__r.name+'</td></tr>';
                //}
            }
        }
        if(htmlTable <> ''){
            htmlTable += '</tbody></table><br>';
        }   
        
        return htmlTable;
    }
    public pageReference sendReminderEmail(){
        emailList = new List<Messaging.SingleEmailMessage>();
        List<String> toAdd = new List<string>();
        for(InviteeWrapper iw: inviteeWrapperList){
            if(iw.isSelected){
                
                toAdd.add(iw.inviteeRec.Relation.Email);
                Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();  
                msg.setTargetObjectId(iw.inviteeRec.relationId);
                //msg.setToAddresses(toAdd);
                //msg.setWhatId(iw.inviteeRec.EventId);
                 msg.setHtmlBody('<table><tr><img alt="" src="https://talent--c.na2.content.force.com/servlet/servlet.ImageServer?id=01540000001BWuB&oid=00D40000000MxFD" align="left" border="0"></img></tr></table><Br/><Br/>'+
                 'Dear '+iw.inviteeRec.Relation.Name+',<br><b><br>'+
                 'Reminder for Interview Event '+ eventOnInterview.subject+ ' Id:'+
                    iw.inviteeRec.Event.What.Name+'.</b><br><br>'+
                    candTable(iw.inviteeRec.Event.whatId)+                 
                    '<br> Please Accept/Decline the Invite you have received with subject  <b>' +iw.inviteeRec.Event.Subject+' [Id: '+iw.inviteeRec.Event.What.Name+' ]</b>.'+ 
                    '<br><br>Thanks,<br>Deloitte Recruiting'+
                    '<p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin-top: 0.17in; margin-bottom: 0in;">'+
                    '<font face="Arial,Sans-Serif" size="2"><a class="western" href="https://www.deloitte.com/us">'+
                    '<font color="#002776"><font style="font-size: 8pt;"><b>Deloitte.com</b></font></font></a>&nbsp;|&nbsp;'+
                    '<a class="western" href="http://www.deloitte.com/us/security"><font color="#002776"><font style="font-size: 8pt;">'+
                    '<b>Security</b></font></font></a>&nbsp;|&nbsp;'+
                    '<a class="western" href="http://www.deloitte.com/us/legal"><font color="#002776"><font style="font-size: 8pt;">'+
                    '<b>Legal</b></font></font></a>&nbsp;|&nbsp;<a class="western" href="http://www.deloitte.com/us/privacy">'+
                    '<font color="#002776"><font style="font-size: 8pt;">'+
                    '<b>Privacy</b></font></font></a></font></p>'+
                    '<p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin: 0in 0in 0pt;" class="MsoNoSpacing"><font face="Times New Roman">'+
                    '<p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin: 0in 0in 0pt;" class="MsoNoSpacing"><o:p>'+
                    '<font face="Calibri">&nbsp;</font></o:p></p>'+
                    '<p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin: 0in 0in 0pt;" class="MsoNoSpacing">'+
                    '<font face="Calibri"><font size="1">30 Rockefeller Plaza<o:p></o:p></font></font></p>'+
                    '<p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin: 0in 0in 0pt;" class="MsoNoSpacing">'+
                    '<font face="Calibri"><font size="1">New York, NY 10112-0015<o:p></o:p></font></font></p>'+
                    '<p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin: 0in 0in 0pt;" class="MsoNoSpacing">'+
                    '<font face="Calibri"><font size="1">United States<o:p></o:p></font></font></p>'+
                    '<p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin: 0in 0in 0pt;" class="MsoNoSpacing"><br></p>'+
                    '<p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin: 0in 0in 0pt;" class="MsoNoSpacing"><o:p>'+
                    '<img src="https://talent--c.na2.content.force.com/servlet/servlet.ImageServer?id=01540000000hJP9&amp;oid=00D40000000MxFD" style="font-size: 12pt;">'+
                    '<font face="Calibri">&nbsp;</font></o:p></p><p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin: 0in 0in 0pt;" class="MsoNoSpacing"><o:p>'+
                    '<font face="Calibri">&nbsp;</font></o:p><span style="font-family: Calibri; font-size: 12pt;">&nbsp;</span></p>'+
                    '<p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin: 0in 0in 0pt;" class="MsoNoSpacing">'+
                    '<font face="Calibri"><font size="1">Copyright © 2013 Deloitte Development LLC. All rights reserved.<o:p></o:p></font>'+
                    '</font></p><p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin: 0in 0in 0pt;" class="MsoNoSpacing">'+
                    '<font face="Calibri"><font size="1">36 USC 220506<o:p></o:p></font></font></p>'+
                    '<p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin: 0in 0in 0pt;" class="MsoNoSpacing">'+
                    '<font face="Calibri"><font size="1">Member of Deloitte Touche Tohmatsu Limited<o:p></o:p></font></font></p>'+
                    '<p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin: 0in 0in 0pt;" class="MsoNoSpacing">'+
                    '<font face="Calibri"><font size="1"><br></font></font></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin: 6pt 0in 12pt 9.35pt;"><font face="Times New Roman">'+
                    '<tr>'+
                    '<td>'+
                    '<p style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt; margin: 0in 0in 0pt;" class="MsoNoSpacing">'+
                    '<a href="http://www.linkedin.com/company/deloitte" style="font-size: 12pt; background-color: rgb(255, 255, 255);">'+
                    '<img src="https://talent--c.na2.content.force.com/servlet/servlet.ImageServer?id=01540000000hJPM&amp;oid=00D40000000MxFD" alt="Right click on the image to save it to your computer.">'+
                    '</a>'+
                    '<span style="font-size: 12pt; background-color: rgb(255, 255, 255);">&nbsp;</span>'+
                    '<a href="http://www.facebook.com/YourFutureAtDeloitte" style="font-size: 12pt; background-color: rgb(255, 255, 255);">'+
                    '<img src="https://talent--c.na2.content.force.com/servlet/servlet.ImageServer?id=01540000000hJPH&amp;oid=00D40000000MxFD" alt="Right click on the image to save it to your computer." style="font-size: 12pt;"></a>'+
                    '<a href="http://www.youtube.com/deloittellp" style="font-size: 12pt; background-color: rgb(255, 255, 255);">'+
                    '<img src="https://talent--c.na2.content.force.com/servlet/servlet.ImageServer?id=01540000000hJPW&amp;oid=00D40000000MxFD" alt="Right click on the image to save it to your computer." style="font-size: 12pt;"></a>'+
                    '<a href="http://www.deloitte.com/view/en_US/us/press/social-media/index.htm" style="font-size: 12pt; background-color: rgb(255, 255, 255);">'+
                    '<img src="https://talent--c.na2.content.force.com/servlet/servlet.ImageServer?id=01540000000hJPT&amp;oid=00D40000000MxFD" alt="Right click on the image to save it to your computer." style="font-size: 12pt;"></a>'+
                    '<a href="http://www.deloitte.com/view/en_US/us/press/rss-feeds/index.htm" style="font-size: 12pt; background-color: rgb(255, 255, 255);">'+
                    '<img src="https://talent--c.na2.content.force.com/servlet/servlet.ImageServer?id=01540000000hJPI&amp;oid=00D40000000MxFD" alt="Right click on the image to save it to your computer." style="font-size: 12pt;"></a>'+
                    '</p>'
                    
                    );
                msg.setSubject('Reminder for Interview Event '+ eventOnInterview.subject+ ' Id:'+iw.inviteeRec.Event.What.name);
                msg.setsaveAsActivity(false);
                //msg.setTemplateId(et.id);
                emailList.add(msg);
            }       
        }
        
        if(!emailList.isEmpty()){
            Messaging.sendEmail(emailList);
            task taskToadd = new task();
            String toAddr = convListOfStr(toAdd);
            if(emailList[0].getHtmlbody()!=null){
                string html=emailList[0].getHtmlbody();
                string result = html.replaceAll('<br/>', '\n');
                result = result.replaceAll('<br />', '\n');
                result = result.replaceAll('&nbsp;', '');
                result = result.replaceAll('&gt;', '');
                result = result.replaceAll('&lt;', '');
                
                string HTML_TAG_PATTERN = '<[^>]+>|&nbsp;';
                pattern myPattern = pattern.compile(HTML_TAG_PATTERN);
                matcher myMatcher = myPattern.matcher(result);
                result = myMatcher.replaceAll('');
                taskToadd.Description = toAddr +'\n'+'\n'+ result;
                
            }
                         
             taskToadd.Subject=emailList[0].getSubject(); 
             //taskToadd.Description=taskDescription;           
             taskToadd.WhatId=IntvId;
             taskToadd.ownerId = userInfo.getUserId();                           
             taskToadd.Status='Completed';
             insert taskToadd;
        }
        return new pageReference('/'+IntvId);
        
    } 
    public pageReference cancel(){
        return new pageReference('/'+IntvId);
    }
 }