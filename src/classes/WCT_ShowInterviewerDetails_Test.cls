/*
 Name: This is the test class for "WCT_ShowInterviewerDetailsController".
*/
@isTest
private class WCT_ShowInterviewerDetails_Test {

    public static Id profileIdRecCoo=[Select id from Profile where Name=:WCT_UtilConstants.Recruiting_Coordinator].Id;
    public static Id profileIdIntv=[Select id from Profile where Name=:WCT_UtilConstants.INTERVIWER_PROFILE_NAME].Id;
    public static Event InterviewEvent;
    public static EventRelation InterviewEventRelation;
    public static User userRecRecCoo, userRecRecIntv, userRecRecr;
    public static WCT_Interview__c Interview;       
    
    
    /** 
        Method Name  : createUserRecCoo
        Return Type  : User
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecCoo()
    {
      userRecRecCoo=WCT_UtilTestDataCreation.createUser('RecCFinl', profileIdRecCoo, 'arunsharmaRecCooFinl@deloitte.com', 'arunsharma4@deloitte.com');
      insert userRecRecCoo;
      return  userRecRecCoo;
    }    
    /** 
        Method Name  : createUserIntv
        Return Type  : User
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserIntv()
    {
      userRecRecIntv=WCT_UtilTestDataCreation.createUser('IntvFinl', profileIdIntv, 'arunsharmaIntvFinl@deloitte.com', 'arunsharma4@deloitte.com');
      insert userRecRecIntv;
      return  userRecRecIntv;
    }  
         
    /** 
        Method Name  : createInterview
        Return Type  : WCT_Interview__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview__c createInterview()
    {
     WCT_List_Of_Names__c ClicktoolForm = WCT_UtilTestDataCreation.createClickToolFormbyName('IEFDocument');
     insert ClicktoolForm ;  
     Update ClicktoolForm ;
     Interview=WCT_UtilTestDataCreation.createInterview(ClickToolForm.Id);
     system.runAs(userRecRecCoo)
     {
        insert Interview;
     }
      return  Interview;
    }   
     
    /** 
        Method Name  : createEvent
        Return Type  : Event
        Type      : private
        Description  : Create temp records for data mapping         
    */    
    private Static Event createEvent()
    {
      InterviewEvent=WCT_UtilTestDataCreation.createEvent(Interview.Id);
      system.runAs(userRecRecCoo)
      {
        insert InterviewEvent;
      }
      return  InterviewEvent;
    }     
    /** 
        Method Name  : createEventRelation
        Return Type  : EventRelation
        Type      : private
        Description  : Create temp records for data mapping         
    */    
    private Static EventRelation createEventRelation()
    {
      InterviewEventRelation=WCT_UtilTestDataCreation.createEventRelation(InterviewEvent.Id,userRecRecIntv.Id);
      insert InterviewEventRelation;
      return  InterviewEventRelation;
    }      
    static testMethod void withInvitees() {
        userRecRecCoo=createUserRecCoo();
        userRecRecIntv=createUserIntv();        
        Interview=createInterview();
        InterviewEvent=createEvent();
        InterviewEventRelation=createEventRelation();

    test.startTest();
    ApexPages.StandardController stdController= new ApexPages.StandardController(InterviewEvent);
    WCT_ShowInterviewerDetailsController interviewDetailController= new  WCT_ShowInterviewerDetailsController(stdController);
    interviewDetailController.getlstInterviewers_Contact();
    interviewDetailController.Cancel();
    test.stopTest();
    }
    
}