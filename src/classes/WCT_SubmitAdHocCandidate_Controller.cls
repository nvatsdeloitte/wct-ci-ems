/**
 * Class Name  : WCT_SubmitAdHocCandidate_Controller   
 * Description : To create Adhoc candidate from Phone screen 
 */
public without sharing class WCT_SubmitAdHocCandidate_Controller{
    //Variable Declaration
    public String externalEmail { get; set; }
    public static final string CR_PREFIX = WCT_Candidate_Requisition__c.sObjectType.getDescribe().getKeyPrefix();
    public Static final string NOTENAMEPREFIX = System.Label.Note_Name_Prefix_in_Phonescreen;
    public String candidateRequisitionName { get; set; }
   
    public String errorMsg{get;set;}
    public String TechnicalScreener { get; set; }
    public String TechnicalScreener1 { get; set; }
    public String TechnicalScreener2 { get; set; }
    public String TechnicalScreenerId { get; set; }
    public String TechnicalScreener1Id { get; set; }
    public String TechnicalScreener2Id { get; set; }
    public String AdditionalNotes { get; set; }
    public String CandidateAvailiblity { get; set; }
    public String InitialContact { get; set; }
    public String officeLocation { get; set; }
    public String emailAddress { get; set; }
    public String phone { get; set; }
    public String mobile { get; set; }
    public String homePhone { get; set; }
    public String currentEmployer { get; set; }
    public String otherEmployer { get; set; }
    public String otherInstitution { get; set; }
    public String school { get; set; }
    public String lastname { get; set; }
    public String firstname { get; set; }
    public Boolean showError { get; set; }
    public Contact candidate { get;set; }
    public Document doc {get;set;}
    public Document docNew {get;set;}
    public String docId {get;set;}
    public String schoolName { get; set; }
    public String currentEmployerName { get; set; }
    public boolean block1visibility { get; set;}

     public boolean block2visibility { get; set;}
     
     public boolean firstentry =true;
         
    //Document and Attachment List
    public List<string> docIdList = new List<string>();
    public List<string> SelecteddocIdList = new List<string>();
    public List<Document> selectedDocumentList = new List<Document>();
    public List<AttachmentsOnCandidateWrapper> UploadedDocumentList {get;set;}
    public List<Attachment> attachmentsToInsertList = new List<Attachment>();
    
    //Attachment Variables
    public Attachment attachment = new Attachment();
    public static Attachment attachment1 = new Attachment();
    public boolean attachmentSection {get; set;}
    public string attFileName{get;set;}
    public Transient Blob attFile{get;set;}
    public String attFileType{get;set;}
    public boolean removeAttachment{get; set;}
    //Adhoc candidate Validation variables
    Set<Id> setRecordtypeIDs=new Set<ID>{label.AdhocCandidateRecordtypeID, label.CandidateRecordtypeId,label.OthersContactRecordtypeID}; 
    Public List<Contact> liExistingCont = new List<Contact>();
    
    // Page2 Variables
    public WCT_Candidate_Requisition__c candidateReq {get;set;}
    public Id reqId {get;set;}
    public boolean booAdhocCand {get;set;}
    public boolean booCand {get;set;}
    public Contact contact {get;set;}
    
    //Documents Wrapper
        public class AttachmentsOnCandidateWrapper{
        public boolean isSelected {get;set;}
        public string docName {get;set;}
        public string documentId {get;set;}
        
        public AttachmentsOnCandidateWrapper(boolean selected,String Name,String Id){
            isSelected = selected;
            docName = Name ;
            documentId = Id;
        }
        
    }
    
    //Controller's construction
    public WCT_SubmitAdHocCandidate_Controller(){
    
    /*Setting the block visibility*/
    
    block1visibility =true;
    block2visibility =false;
        doc = new document();
        candidate = new Contact();
        UploadedDocumentList = new List<AttachmentsOnCandidateWrapper>();
        candidateReq = new WCT_Candidate_Requisition__c();
        attachmentSection = false;
        removeAttachment = false;
    }
    //Cancel method
    public PageReference can() {
        return new PageReference('/home/home.jsp');
    }
    public PageReference saveContact() {
        return null;
    }
   
   /** 
   Method Name: Backform
   Return Type: Takes back to the first page
   
   **/
   
   Public void backform(){
    block1visibility =true;
    block2visibility =false;
   
   }
    
    public pageReference nextPage()
    {
         
        /*Checking the exostance of email id*/
        
            try{
            liExistingCont = [Select id from Contact where email = :emailAddress AND recordtypeID in :setRecordtypeIDs];
            
        }Catch(QueryException ex){
            Apexpages.addmessage(new Apexpages.message(Apexpages.severity.FATAL,ex.getMessage()));
        }
         if (firstentry){
       if(!liExistingCont.IsEmpty()){
            showError = true;
            Apexpages.addmessage(new Apexpages.message(Apexpages.severity.FATAL,'A  Contact with this email address already exists.Please change the Email Address'));
            return null;
        }
        }
        
        
        /*Checking the attachments */
          if(docIdList.isEmpty()){
            showError = true;
            Apexpages.addmessage(new Apexpages.message(Apexpages.severity.FATAL,'Attachment is required for Adhoc Candidate.'));
            return null;
        }  
        if(!docIdList.isEmpty()){
            for(AttachmentsOnCandidateWrapper attWrap : UploadedDocumentList){
                if(attWrap.isSelected){
                    SelecteddocIdList.add(attWrap.documentId);
                }
            }
        }
        if(SelecteddocIdList.isEmpty()){
            showError = true;
            Apexpages.addmessage(new Apexpages.message(Apexpages.severity.FATAL,'Please select atleast one of the uploaded Attachment for Adhoc Candidate.'));
            return null;
        } 
        
        
        block1visibility =false;
        block2visibility =true;
        return null;
    }
   
    /** 
        Method Name  : saveCandidate   
        Return Type  : Takes to Candidate Tracker page       
     */
        public void saveCandidate() {
        

        //Id recordTypeId = [Select r.Name, r.Id From RecordType r where name = 'Candidate Ad Hoc'].id;
        Id recordTypeId = System.Label.AdhocCandidateRecordtypeID;

        candidate.firstname = firstname ;
        candidate.lastname = lastname ;
        candidate.email = emailAddress ;
        candidate.MobilePhone = mobile ;
        candidate.homePhone = homePhone;
        candidate.phone = phone;
        candidate.RecordTypeId  = recordTypeId  ;
        candidate.WCT_CN_Other_Institution__c = otherInstitution;
        candidate.WCT_CN_Other_Employer__c = otherEmployer;
        if(school.length()>0){
            if(schoolName != null && schoolName != ''){
                candidate.WCT_School_Education_Institution_lkp__c = school.subString(0, 15);
            }
        }
        if(currentEmployer.length()>0){
            if(currentEmployerName != null && currentEmployerName != ''){
                candidate.WCT_Current_Employer_lkp__c = currentEmployer.subString(0, 15);
            } 
        }
        if(TechnicalScreenerId.length()>0){
            candidate.WCT_Technical_Screener__c = TechnicalScreenerId.subString(0, 15);
        }
        if(TechnicalScreener1Id.length()>0){
            candidate.WCT_Technical_Screener2__c = TechnicalScreener1Id.subString(0, 15);
        }
        if(TechnicalScreener2Id.length()>0){
            candidate.WCT_Technical_Screener3__c = TechnicalScreener2Id.subString(0, 15);
        } 

        insert candidate;
       
        
        //Creation Of Note
        Note n = new Note();
        n.parentid = candidate.id;
        n.title = NOTENAMEPREFIX +' '+ firstname + ' '+lastname;
        if(n.title.length()>79){
            n.title = (NOTENAMEPREFIX +' '+ firstname + ' '+lastname).subString(0, 80);
        }
        //n.title = firstname + ' '+lastname + ' Notes';
        n.body = 'Office Location :'+ officeLocation + '\r\n' +'Initial Contact :'+ InitialContact + '\r\n'
        +'Candidate Availiblity  :'+ CandidateAvailiblity + '\r\n'
        +'Additional Notes :'+ AdditionalNotes + '\r\n' +
        'Technical Screener 1 :'+ TechnicalScreener + '\r\n'+
        'Technical Screener 2 :'+ TechnicalScreener1 + '\r\n'+
        'Technical Screener 3 :'+ TechnicalScreener2 + '\r\n';
        insert n;
        //Creation of Attachment
        if(!docIdList.isEmpty()){
           
            if(!SelecteddocIdList.isEmpty()){
                selectedDocumentList = [SELECT id,name,ContentType,Type,Body FROM Document where id IN :SelecteddocIdList];
            }
            for(Document doc : selectedDocumentList){
                attachment = new Attachment();
                attachment.body = doc.body;
                attachment.ContentType = doc.ContentType;
                attachment.Name= doc.Name;
                attachment.parentid = candidate.id;
                attachmentsToInsertList.add(attachment);
                
            }
               
            if(!attachmentsToInsertList.isEmpty()){
                insert attachmentsToInsertList;
                delete selectedDocumentList;
                firstentry=false;
            }
            
           

        }
}

    public void setAttachmentSection(){
        if(attachmentSection ){
            attachmentSection = false;
        }else{
            attachmentSection = true;}

    }

    public void uploadAttachment(){

        
        doc.folderId=SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
        
        if(doc.body != null){
            insert doc;
            docIdList.add(doc.id);
            UploadedDocumentList.add(new AttachmentsOnCandidateWrapper(true,doc.name,doc.id));
            doc = new Document();
        }


        //removeAttachment = false;
        setAttachmentSection();




    }
    public void cancelAttachment(){
        setAttachmentSection();

    }


    //Creation of Error Messages
    public void sub() {

        if(errorMsg != null){
            String[] arrError = errorMsg.split(',');
            showError = true;
            for(String s:arrError){
                if(s != 'Test')
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,s));
            }
        }

    }
    //Page 2 Methods
    
    /** 
        Method Name  : saveCandTracker   
        Return Type  : Inserts Candidate Tracker       
*/
    
    public PageReference save(){
    try{
     
    saveCandidate();
    
    candidateReq.WCT_Contact__c = candidate.id;
    insert candidateReq;
    }catch(DMLException ex){
         Apexpages.addmessage(new Apexpages.message(Apexpages.severity.FATAL,ex.getMessage()));
         return null;
    }
    return new Pagereference('/'+candidate.id);
}

}