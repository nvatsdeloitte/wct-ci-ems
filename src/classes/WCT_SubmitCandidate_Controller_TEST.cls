/**
 * Class Name  : WCT_SubmitCandidate_Controller_Test
 * Description : This apex test class will use to test the WCT_SubmitCandidate_Controller
 */

@isTest
private class WCT_SubmitCandidate_Controller_TEST 
{
    public static Id profileIdRecCoo=[Select id from Profile where Name=:WCT_UtilConstants.Recruiting_Coordinator].Id;  

    /** 
        Method Name  : testSubmitExistingTaleoId
        Return Type  : void
        Type      : Test Method
        Description  : Test will check Inserting Candidate with 
        existing TaleoId
     **/  
    static testMethod void testSubmitExistingTaleoId()
    {
        User user =  WCT_UtilTestDataCreation.createUser('recoo',profileIdRecCoo,'ralaRecCo@deloitte.com','rala@deloitte.com');
        insert user;
        //Create Contacts
        Contact contact= WCT_UtilTestDataCreation.createContactAsCandidate(System.Label.CandidateRecordtypeId);
        contact.WCT_Taleo_Id__c = 'taleoId';
        Insert contact;

        String TechnicalScreenerId = user.id;
        WCT_SubmitCandidate_Controller conExisting = new WCT_SubmitCandidate_Controller();
        conExisting.doc.body = Blob.valueOf('Unit Test Attachment Body');
        conExisting.doc.Name = 'test';
        conExisting.doc.ContentType = 'test';
        conExisting.lastname = 'test';
        conExisting.InitialContact = 'test';
        conExisting.TechnicalScreener = 'TechnicalScreener';
        conExisting.TechnicalScreener1  = 'TechnicalScreener1 ';
        conExisting.TechnicalScreener2 = 'TechnicalScreener2';
        conExisting.TechnicalScreenerId = user.id;
        conExisting.TechnicalScreener1Id = user.id;
        conExisting.TechnicalScreener2Id = user.id;
        conExisting.CandidateAvailiblity = 'CandidateAvailiblity';
        conExisting.AdditionalNotes = 'AdditionalNotes';
        conExisting.officeLocation = 'officeLocation ';
        conExisting.taleoId = 'taleoId';
        conExisting.candidateReqNumber = 'candidateReqNumber ';
        conExisting.email = 'a@test.com';
        conExisting.firstname = 'test';
        conExisting.phone = '1234567';
        conExisting.contactTitle = 'contactTitle ';
        conExisting.docId = 'test';
        conExisting.errorMsg = 'Invalid Phone,Invalid Email';

        PageReference pgref = new PageReference('/apex/WCT_SubmitCandidate_Page');
        Test.setCurrentPage(pgref);
        conExisting.can();
        conExisting.uploadAttachment();
        conExisting.saveCandidate();
        conExisting.setAttachmentSection();
        conExisting.removeAttachment();
        conExisting.sub();
        conExisting.cancelAttachment();
        conExisting.docIdList = new list<string>();
        conExisting.saveCandidate();  
    }
    /** 
        Method Name  : testNewTaleoId
        Return Type  : void
        Type      : Test Method
        Description  : Test will check Inserting Candidate with 
        New TaleoId
     **/  
    static testMethod void testNewTaleoId()
    {
        User user =  WCT_UtilTestDataCreation.createUser('recoo',profileIdRecCoo,'ralaRecCo@deloitte.com','rala@deloitte.com');
        insert user;
        WCT_SubmitCandidate_Controller conNew = new WCT_SubmitCandidate_Controller();
        conNew.doc.body = Blob.valueOf('Unit Test Attachment Body');
        conNew.doc.Name = 'test';
        conNew.doc.ContentType = 'test';
        conNew.lastname = 'testInstitutionforMoreThanEightyCharectors';
        conNew.InitialContact = 'test';
        conNew.email = 'a@test.com';
        conNew.taleoId = '12345';
        conNew.TechnicalScreener = 'TechnicalScreener';
        conNew.TechnicalScreener1  = 'TechnicalScreener1 ';
        conNew.TechnicalScreener2 = 'TechnicalScreener2';
        conNew.TechnicalScreenerId = user.id;
        conNew.TechnicalScreener1Id = user.id;
        conNew.TechnicalScreener2Id = user.id;
        conNew.uploadAttachment();
        conNew.nextpage();
        conNew.Save();
        conNew.taleoId = '1234567';
        conNew.docIdList = new list<string>();
        conNew.nextpage();
        conNew.Save();  
    }

}