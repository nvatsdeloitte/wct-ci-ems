global class WCT_TaskImmigrationEmailSchedulableBatch implements Database.Batchable<sObject>,Schedulable, Database.Stateful{

    public string strSql{get;set;}
//-------------------------------------------------------------------------------------------------------------------------------------
//    Constructor
//-------------------------------------------------------------------------------------------------------------------------------------
    
    public WCT_TaskImmigrationEmailSchedulableBatch(){

    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Scheduler Execute 
//-------------------------------------------------------------------------------------------------------------------------------------
    global void execute(SchedulableContext SC) {
        WCT_TaskImmigrationEmailSchedulableBatch batch = new  WCT_TaskImmigrationEmailSchedulableBatch();
        ID batchprocessid = Database.executeBatch(batch,200);
    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Set the query
//-------------------------------------------------------------------------------------------------------------------------------------
    global Database.QueryLocator start(Database.BatchableContext bc){
    
        string strSql = 'SELECT Id, WCT_Immigration_Status__c,'+
                        ' WCT_Assignment_Owner__c,'+
                        ' (SELECT Id, WhatId, ActivityDate, Status, Subject FROM Tasks WHERE Subject = \'Submit Visa logistics information\''+
                        ' AND Status = \'Not Started\' AND ActivityDate < '+ string.valueof(system.today()) + ')'+
                        ' FROM WCT_Immigration__c WHERE WCT_Assignment_Owner__r.RecordType.Name = \'Employee\'';

        return database.getQuerylocator(strSql);  
       
    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Database execute method to Process Query results for email notification
//-------------------------------------------------------------------------------------------------------------------------------------
    global void execute(Database.batchableContext bc, List<sObject> scope){
    
        List<WCT_Immigration__c> lstImmigration = new List<WCT_Immigration__c>();

        for(sObject tmp : scope){
            WCT_Immigration__c tmpImm = new WCT_Immigration__c();
            tmpImm = (WCT_Immigration__c)tmp;
            lstImmigration.add(tmpImm);
            for(Task t : tmpImm.Tasks)
            {
                if(t!=null){
                    if(t.ActivityDate < system.today())
                    {
                        tmpImm.WCT_Immigration_Status__c = 'Yet to receive Visa Logistics Information from Practitioner';
                    }
                }
            }     
        }
        if(!lstImmigration.isEmpty())
            Database.SaveResult[] st = Database.update(lstImmigration, false);
    }
    
    global void finish(Database.batchableContext info){     
   
    }     
}