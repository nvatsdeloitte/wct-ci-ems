@isTest
public class WCT_TaskRecordEmailSchedBatch_Test
{
    static testMethod void m1(){
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        mob.WCT_Last_Working_Day_in_US__c=system.today().addDays(-3);
        insert mob;
        Task t = WCT_UtilTestDataCreation.createTask(mob.id);
        t.Subject = 'Return firm property';
        insert t;
        Test.startTest();
        WCT_TaskRecordEmailSchedulableBatch controller = new WCT_TaskRecordEmailSchedulableBatch();
        system.schedule('New','0 0 2 1 * ?',controller); 
        Test.stopTest(); 
     }       
}