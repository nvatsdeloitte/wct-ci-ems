global class WCT_TaskRecordEmailSchedulableBatch implements Database.Batchable<sObject>,Schedulable, Database.Stateful{

    public string strSql{get;set;}
//-------------------------------------------------------------------------------------------------------------------------------------
//    Constructor
//-------------------------------------------------------------------------------------------------------------------------------------
    
    public WCT_TaskRecordEmailSchedulableBatch(){

    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Scheduler Execute 
//-------------------------------------------------------------------------------------------------------------------------------------
    global void execute(SchedulableContext SC) {
        WCT_TaskRecordEmailSchedulableBatch batch = new  WCT_TaskRecordEmailSchedulableBatch();
        ID batchprocessid = Database.executeBatch(batch,200);
    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Set the query
//-------------------------------------------------------------------------------------------------------------------------------------
    global Database.QueryLocator start(Database.BatchableContext bc){
    
        string strSql = 'SELECT Id, WCT_Last_Working_Day_in_US__c,'+
                        ' WCT_Mobility_Employee__c,'+
                        ' (SELECT Id, WhatId, Status, Subject FROM Tasks WHERE Subject = \'Return firm property\' AND Status = \'Not Started\') FROM WCT_Mobility__c'+
                        ' WHERE WCT_Last_Working_Day_in_US__c < '+ string.valueof(system.today()) + ' AND WCT_Mobility_Employee__r.RecordType.Name = \'Employee\'';
        return database.getQuerylocator(strSql);   
       
    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Database execute method to Process Query results for email notification
//-------------------------------------------------------------------------------------------------------------------------------------
    global void execute(Database.batchableContext bc, List<sObject> scope){
    
        List<WCT_Mobility__c> lstMobility = new List<WCT_Mobility__c>();
        List<Task> lstTask = new List<Task>();
        for(sObject tmp : scope){
            WCT_Mobility__c tmpMob = new WCT_Mobility__c();
            tmpMob = (WCT_Mobility__c)tmp;
            lstMobility.add(tmpMob);
            for(Task t : tmpMob.Tasks)
            {
                if(t!=null){
                    if(tmpMob.WCT_Last_Working_Day_in_US__c < system.today())
                    {
                        t.Status = 'Completed';
                        lstTask.add(t);
                    }
                }
            }     
        }
        if(!lstTask.isEmpty())
            Database.SaveResult[] st = Database.update(lstTask, false);
    }
    
    global void finish(Database.batchableContext info){     
   
    }     
}