public class WCT_Task_Completion_formController{
    public task taskRecord{get;set;}
    public String taskSubject {get;set;}
    public String taskid{get;set;}
    public boolean display{get;set;}
    
    public WCT_Task_Completion_formController()
    {
        taskid = ApexPages.currentPage().getParameters().get('taskid');
        display=true;
        taskSubject = '';
    }

    public void updateTaskFlags()
    {
        if(taskid==''|| taskid==null){
            display=false;
            return;
        }

        taskRecord=[SELECT id, Subject,Ownerid, Status,WCT_Is_Visible_in_TOD__c, WCT_Auto_Close__c FROM Task WHERE Id =: taskid];

        if(taskRecord != null){

            //Get Task Subject
            taskSubject = taskRecord.Subject;

            //Set Visibity of Tod
          //  taskRecord.WCT_Is_Visible_in_TOD__c=false;

            //Set Task Status
            if(taskRecord.WCT_Auto_Close__c == true){
                taskRecord.status='Completed';
            }
            if(taskRecord.WCT_Auto_Close__c == false){
            //  taskRecord.status='Employee Replied';
            taskRecord.status='Not Started';
            }

            update taskRecord;
        }else{
            return;
        }
        
    }
}