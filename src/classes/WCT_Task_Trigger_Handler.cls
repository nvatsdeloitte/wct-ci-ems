//----------------------------------------------------------------
// Description :  WCT_Task_Trigger_Handler class will be called by Task trigger
//----------------------------------------------------------------
public class WCT_Task_Trigger_Handler {

public WCT_Task_Trigger_Handler (){
    
}

public static void createGMIDependentTasks(Map<id,Task> mapGMITasks,List<Task> lstTask){
    string strRecType;
// Get the parent task ref table  ids    
    set<Id> setParentTaskIds = new set<Id>();
    for(Task tmpTask : lstTask){
        setParentTaskIds.add(tmpTask.WCT_Task_Reference_Table_ID__c);
    }
   
// Get childern tasks    
    List<WCT_Task_Reference_Table__c> lstTaskRefTable = [Select w.WCT_Visa_Type__c, w.WCT_Task_for_Object__c, w.WCT_Task_Subject__c, w.WCT_Status__c, w.WCT_Priority__c,w.WCT_SLA_Description__c, 
                                                            w.WCT_Notification_5__c, w.WCT_Notification_4__c, w.WCT_Notification_3__c, w.WCT_Notification_2__c, w.WCT_Notification_1__c, 
                                                            w.WCT_Is_Active__c, w.WCT_Due_Date__c, w.WCT_Comment__c, w.WCT_Auto_Flag__c, w.WCT_Auto_Create__c, w.WCT_Auto_Close__c,
                                                            w.WCT_Assigned_to__c, w.OwnerId, w.Name, w.IsDeleted, w.Id, w.CreatedDate ,w.WCT_ToD_Task_Reference__c ,w.wct_Parent_Task__c,(Select id FROM  Task_Reference_Table__r)
                                                            From WCT_Task_Reference_Table__c w 
                                                            where WCT_Auto_Create__c=true and WCT_Is_Active__c=true and wct_Parent_Task__c in :setParentTaskIds];
    for(WCT_Task_Reference_Table__c t : lstTaskRefTable)
    
                                                            
    Map<Id,WCT_Task_Reference_Table__c> mapTaskRefTable = new Map<Id,WCT_Task_Reference_Table__c>(lstTaskRefTable);
    
    Map<Id,Id> mapTaskIdWhatId = new Map<Id,Id>();
    set<Id> setWhatId = new set<Id>();
    List<RecordType> lstRecType = [SELECT Id,name FROM RecordType WHERE NAME in('Immigration','Mobility','LCA') and SobjectType = 'Task'];
    
    boolean isImmigration=false;
    boolean isMobility=false;
    boolean isLca=false;
    Map<string,id> mapRecTypeNameId = new Map<string,id>();
    for(Task tmpTask : lstTask){
        mapTaskIdWhatId.put(tmpTask.id,tmpTask.whatId);
        setWhatId.add(tmpTask.whatId);
        for(RecordType tmpRecType : lstRecType ){
            if(tmpTask.RecordTypeId == tmpRecType.id && tmpRecType.Name=='Immigration'){
                isImmigration=true;
                mapRecTypeNameId.put(tmpRecType.Name,tmpRecType.id);    
                break;
            }    
            else if(tmpTask.RecordTypeId == tmpRecType.id && tmpRecType.Name=='Mobility'){
                isMobility=true;
                mapRecTypeNameId.put(tmpRecType.Name,tmpRecType.id);    
                break;
            }    
            else  if(tmpTask.RecordTypeId == tmpRecType.id && tmpRecType.Name=='LCA'){
                isLca=true;
                mapRecTypeNameId.put(tmpRecType.Name,tmpRecType.id);    
                break;
            } 
        }
    }
    
    Map<id,WCT_Immigration__c> mapIdImmigration;
    Map<id,WCT_Mobility__c> mapIdMobility;
    if(isImmigration==true){
      List<WCT_Immigration__c> lstImmigration = [select  id, wct_Immigration_status__C,WCT_Assignment_Owner__c,ownerId ,WCT_Assignment_Owner__r.Email 
                                                  from WCT_Immigration__c where id in :setWhatId];
      mapIdImmigration = new Map<id,WCT_Immigration__c>(lstImmigration);
      
    }
    else if(isMobility==true){
      List<WCT_Mobility__c> lstMobility = [select  id, wct_Mobility_status__C,WCT_Mobility_Employee__c from WCT_Mobility__c where id in :setWhatId];
      mapIdMobility = new Map<id,WCT_Mobility__c>(lstMobility);
    
    }
     // For initial assignment of Task, Tasks are assigned to 'GMI' user and 'Fragoman' user
    Map<string,id> mapUserId = new Map<string,id>();
    List<User> lstAssignedUser = [select id,name from user where name in('GM & I','Fragomen')];
    for(User tmpUser : lstAssignedUser ){
        mapUserId.put(tmpUser.name,tmpUser.id);
    }
    
    Task immTask;
    Date CurrentDate = system.today();
    string strTmpImmOwnerId;
    string strTmpMobOwnerId;
    List<Task> lstNewTask = new List<Task>();
    for(WCT_Task_Reference_Table__c tmpTaskRefTable : lstTaskRefTable ){

        for(Task tmpTask : lstTask){
    
            if(tmpTaskRefTable.WCT_Parent_Task__c==tmpTask.WCT_Task_Reference_Table_ID__c && tmpTask.status=='Completed' && tmpTask.WCT_HasChildrens__c==true){
                immTask = new Task();
                immTask.Subject = tmpTaskRefTable.WCT_Task_Subject__c;
                immTask.WhatId = tmpTask.WhatId; 
                immTask.Priority = 'Normal';
                immTask.WCT_Task_Reference_Table_ID__c = tmpTaskRefTable.id;
                
                immTask.WCT_Auto_Close__c= tmpTaskRefTable.WCT_Auto_Close__c;
                if(tmpTaskRefTable.WCT_Due_Date__c !=null)
                    immTask.ActivityDate = CurrentDate.addDays(Integer.valueOf(tmpTaskRefTable.WCT_Due_Date__c));
                if(tmpTaskRefTable.WCT_Notification_1__c !=null)
                    immTask.WCT_Notification_1__c = CurrentDate.addDays(Integer.valueOf(tmpTaskRefTable.WCT_Notification_1__c));
                    
                if(tmpTaskRefTable.WCT_Notification_2__c !=null)
                    immTask.WCT_Notification_2__c = CurrentDate.addDays(Integer.valueOf(tmpTaskRefTable.WCT_Notification_2__c));
                    
                if(tmpTaskRefTable.WCT_Notification_3__c !=null)
                    immTask.WCT_Notification_3__c = CurrentDate.addDays(Integer.valueOf(tmpTaskRefTable.WCT_Notification_3__c));
                    
                if(tmpTaskRefTable.WCT_Notification_4__c !=null)
                    immTask.WCT_Notification_4__c = CurrentDate.addDays(Integer.valueOf(tmpTaskRefTable.WCT_Notification_4__c));
                    
                if(tmpTaskRefTable.WCT_Notification_5__c !=null)
                    immTask.WCT_Notification_5__c = CurrentDate.addDays(Integer.valueOf(tmpTaskRefTable.WCT_Notification_5__c));
                if(tmpTaskRefTable.WCT_Parent_Task__c !=null)
                    immTask.WCT_HasChildrens__c=true;
                
                //sla description
                if(tmpTaskRefTable.WCT_SLA_Description__c <> null){
                    immTask.WCT_SLA_description__c = tmpTaskRefTable.WCT_SLA_Description__c;
                }
                    
                if(tmpTask.RecordTypeId==mapRecTypeNameId.get('Immigration')){
                    strTmpImmOwnerId = mapIdImmigration.get(tmpTask.whatId).ownerId;
                    immTask.RecordTypeId = mapRecTypeNameId.get('Immigration');
                    immTask.Task_Type__c = mapIdImmigration.get(tmpTask.whatId).WCT_Immigration_Status__c;
                    immTask.WCT_Business_Owner__c =mapIdImmigration.get(tmpTask.whatId).WCT_Assignment_Owner__r.Email;
                    immTask.WCT_Encrypt_Email_Key__c =CryptoHelper.encrypt(mapIdImmigration.get(tmpTask.whatId).WCT_Assignment_Owner__r.Email);

                     if(tmpTaskRefTable.WCT_Assigned_to__c=='Employee')
                        {
                            immTask.whoId = mapIdImmigration.get(tmpTask.whatId).WCT_Assignment_Owner__c;
                            if(immTask.whoId !=null)
                                immTask.WCT_Is_Visible_in_TOD__c = true;
                            if(strTmpImmOwnerId.StartsWith('00G')) 
                                immTask.OwnerId = mapUserId.get('GM & I');
                            else
                                immTask.OwnerId=mapIdImmigration.get(tmpTask.whatId).ownerId ;
                        }
                      else if(tmpTaskRefTable.WCT_Assigned_to__c=='Fragomen') 
                          immTask.OwnerId = mapUserId.get('Fragomen');
                      else if(tmpTaskRefTable.WCT_Assigned_to__c=='GM&I Team'){
                          if(strTmpImmOwnerId.StartsWith('00G')) 
                              immTask.OwnerId = mapUserId.get('GM & I');
                          else
                              immTask.OwnerId=mapIdImmigration.get(tmpTask.whatId).ownerId ;
                       }
                }
                else if(tmpTask.RecordTypeId==mapRecTypeNameId.get('Mobility')){
                    strTmpMobOwnerId = mapIdMobility.get(tmpTask.whatId).ownerId;
                    immTask.RecordTypeId = mapRecTypeNameId.get('Mobility');
                    immTask.Task_Type__c = mapIdMobility.get(tmpTask.whatId).WCT_Mobility_Status__c;
                    if(tmpTaskRefTable.WCT_Assigned_to__c=='Employee')
                        {
                            immTask.whoId = mapIdMobility.get(tmpTask.whatId).WCT_Mobility_Employee__c;
                            if(immTask.whoId !=null)
                                immTask.WCT_Is_Visible_in_TOD__c = true;
                          if(strTmpMobOwnerId.StartsWith('00G')) 
                                immTask.OwnerId = mapUserId.get('GM & I');
                            else
                                immTask.OwnerId=mapIdMobility.get(tmpTask.whatId).ownerId;
                            
                        }
                        else if(tmpTaskRefTable.WCT_Assigned_to__c=='Fragomen') 
                            immTask.OwnerId = mapUserId.get('Fragomen');
                        else if(tmpTaskRefTable.WCT_Assigned_to__c=='GM&I Team'){
                            if(strTmpMobOwnerId.StartsWith('00G')) 
                                immTask.OwnerId = mapUserId.get('GM & I');
                            else
                                immTask.OwnerId=mapIdMobility.get(tmpTask.whatId).ownerId;
                        }
                }
                else if(tmpTask.RecordTypeId==mapRecTypeNameId.get('LCA')){
                    immTask.RecordTypeId = mapRecTypeNameId.get('LCA');
                    
                }

                lstNewTask.add(immTask); 
                           
            }
        }
    }
    if(!lstNewTask.isEmpty()){
        insert lstNewTask;
    }
    
}
    


    
    
}