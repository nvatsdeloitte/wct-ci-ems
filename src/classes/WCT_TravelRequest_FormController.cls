public class WCT_TravelRequest_FormController extends SitesTodHeaderController{

   //CREATING A PORT OF ENTRY PROPERTY 
    
   public WCT_Port_of_Entry__c portRecord {get;set;} 

   //PORT OF ENTRY  VARIABLES DEFINED  
   
    public WCT_Port_of_Entry__c getFields{get;set;}
    public String PortStart{set;get;}
    public String PortEnd{set;get;}
    public String PortStart2{set;get;}
    public String PortEnd2{set;get;}
    public String PortStart3{set;get;}
    public String PortEnd3{set;get;}
    public String PortStart4{set;get;}
    public String PortEnd4{set;get;}
    public String PortStart5{set;get;}
    public String PortEnd5{set;get;}
   public String travelstartdate {get; set;}     
    public String travelenddate { get; set;}  
    
  //  public String poeStatus;
   
   //ERROR MESSAGE VARIABLES DEFINED  
   
    public boolean pageError {get; set;}
    public String  pageErrorMessage {get; set;}

   //EMPLOYEE RELATED VARIABLES
   
    public Contact Employee{get;set;}
    public String  employeeEmail{set;get;}
    public boolean employeePresent{set;get;}
    public boolean employeePrevious{set;get;}
    public boolean getEmployeeInfo{set;get;}
    
    public String  employeeType;
    
    //INITIALIZED CONSTRUCTOR AND METHODS CALLED
    
    public WCT_TravelRequest_FormController(){
      init();
      getParameterInfo();
      getEmployeeInfo = true;
      if(invalidEmployee)
        {
           return;
        }
       
    }
 
    //METHOD FOR CREATING PORT OF ENTRY INSTANCE 
    
    public void init(){
       portRecord = new WCT_Port_of_Entry__c ();
       }
       
    //PRIVATE METHOD FOR GETTING URL PARAMETERS
    
    private void getParameterInfo(){
        employeeType = ApexPages.currentPage().getParameters().get('type');
      //  poeStatus    = ApexPages.currentPage().getParameters().get('poe_status');     
    }

    //METHOD FOR FETCHING THE EMPLOYEE RECORD TYPE CONTACTS
    
    public pageReference getEmployeeDetails()
    {
        try{
            recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
            employeeEmail=employeeEmail.trim();
            
            if((employeeEmail==null)||(employeeEmail==''))
            {
                pageErrorMessage = 'Please enter a email ID';
                pageError = true;
                Employee =new contact();
                employeePresent=false;
                return null;
            }
            else
            {
           
                Employee=[select id,name,FirstName,LastName,Email,WCT_Personnel_Number1__c,WCT_Function__c,WCT_Service_Area__c,WCT_Service_Line__c,WCT_Region__c,WCT_Designation_Code__c from contact where recordtypeid=: rt.id and email =: employeeEmail limit 1];
                employeePresent=true;
                  getEmployeeInfo = false;   
                
            }
        }
        catch(Exception e)
        {
  
            WCT_ExceptionUtility.logException('WCT_TravelRequest_FormController','Get Employee Details',e.getMessage());
            pageErrorMessage = 'Email Id is not associated with any employee';
            pageError = true;
            employee=new contact();
            employeePresent=false;
            return null;
        }
        pageError = false;
        return null;
    }
    

    //METHOD FOR REDIRECTING THE FORM WHEN CANCEL BUTTON IS CLICKED
    
    public pageReference cancelPortRecord() {
    
     return new PageReference('/apex/WCT_Mobility_Onboarding_Form?type=' + employeeType +'&em='+CryptoHelper.encrypt(employeeEmail));
    
    }
    
    //METHOD FOR REDIRECTING THE FORM WHEN WHEN POE IS SELECTED NO
    
   /* public PageReference redirectPOE() {
    
    return new PageReference('/apex/WCT_Travel_Request_Form?type=' + employeeType +'&em='+CryptoHelper.encrypt(employeeEmail)); 

    }*/
    
   //METHOD FOR SAVING THE PORT OF ENTRY  RECORD 
     
    public pageReference savePortRecord()
    { 
    
        if((null != PortStart)&&
               ('' !=  PortStart) 
               &&
            (null != PortEnd)&&
             ('' != PortEnd)&&
                 (null != travelstartdate) &&
            ('' !=  travelstartdate)&&
               (null != travelenddate) &&
            ('' !=  travelenddate)&&
             (null != portRecord.WCT_Purpose__c) &&
            ('' !=  portRecord.WCT_Purpose__c)&&
            (null != portRecord.WCT_Port_of_Entry_City__c) &&
            ('' !=  portRecord.WCT_Port_of_Entry_City__c)&&
            (null != portRecord.WCT_Port_of_Entry_State__c)&&
            ('' !=  portRecord.WCT_Port_of_Entry_State__c)){
           
            portRecord.WCT_Email_Address__c = employeeEmail;
              pageError = false;
              
              
              if (String.isNotBlank(travelstartdate)) {
                    portRecord.WCT_Travel_Start_Date__c= Date.parse(travelstartdate);
                }
                else {
                portRecord.WCT_Travel_Start_Date__c= null;
                }
                if (String.isNotBlank(travelenddate)) {
                    portRecord.WCT_Travel_End_Date__c= Date.parse(travelenddate);
                }
                else {
                     portRecord.WCT_Travel_End_Date__c= null;
                }
              
              if (String.isNotBlank(PortStart)) {
                    portRecord.WCT_Start_Date__c = Date.parse(PortStart);
                }
                else {
                portRecord.WCT_Start_Date__c = null;
                }
                if (String.isNotBlank(PortEnd)) {
                    portRecord.WCT_End_Date__c= Date.parse(PortEnd);
                }
                else {
                     portRecord.WCT_End_Date__c= null;
                }
               if (String.isNotBlank(PortStart2)) {
                    portRecord.WCT_Start_Date2__c = Date.parse(PortStart2);
                }
                  else {
                     portRecord.WCT_Start_Date2__c = null;
                }
                
                if (String.isNotBlank(PortEnd2)) {
                    portRecord.WCT_End_Date2__c = Date.parse(PortEnd2);
                }
                   else {
                     portRecord.WCT_End_Date2__c = null;
                }
                 if (String.isNotBlank(PortStart3)) {
                    portRecord.WCT_Start_Date3__c = Date.parse(PortStart3);
                }
                 else {
                     portRecord.WCT_Start_Date3__c= null;
                }
                if (String.isNotBlank(PortEnd3)) {
                    portRecord.WCT_End_Date3__c = Date.parse(PortEnd3);
                }
                       else {
                     portRecord.WCT_End_Date3__c= null;
                } 
               if (String.isNotBlank(PortStart4)) {
                    portRecord.WCT_Start_Date4__c = Date.parse(PortStart4);
                }
                        else {
                     portRecord.WCT_Start_Date4__c = null;
                }
                
                if (String.isNotBlank(PortEnd4)) {
                    portRecord.WCT_End_Date4__c = Date.parse(PortEnd4);
                }
                        else {
                     portRecord.WCT_End_Date4__c= null;
                }
                
                if (String.isNotBlank(PortStart5)) {
                    portRecord.WCT_Start_Date5__c = Date.parse(PortStart5);
                }
                   else {
                     portRecord.WCT_Start_Date5__c= null;
                }
                
                
                if (String.isNotBlank(PortEnd5)) {
                    portRecord.WCT_End_Date5__c = Date.parse(PortEnd5);
                }
                   else {
                     portRecord.WCT_End_Date5__c= null;
                }
                
            insert portRecord;
              pageError = false;
          PageReference page = new PageReference('/apex/WCT_Travel_Request_Form_ThankYou?id=' + portRecord.id+'&em='+CryptoHelper.encrypt(LoggedInContact.Email)); 
            page.setRedirect(true);
            return page;
            
        
          
        }
        else {
           pageErrorMessage = 'Please fill in all the required fields on the form.';
           pageError = true;
           return null;
        }
    
    }
    
    }