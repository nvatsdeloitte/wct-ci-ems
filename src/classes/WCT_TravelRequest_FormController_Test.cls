@isTest

public class WCT_TravelRequest_FormController_Test {

    public static testMethod void m1(){
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        rt=[select id from recordtype where DeveloperName = 'Business_Visa'];
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');

        PageReference pageRef = Page.WCT_Travel_Request_Form;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);     
        ApexPages.currentPage().getParameters().put('type','Employee');
       
        date startdate=date.Today().adddays(10);
        date enddate=date.Today().adddays(20);
        Test.starttest();
        WCT_TravelRequest_FormController controller=new WCT_TravelRequest_FormController();
        ApexPages.currentPage().getParameters().put('type','Business');
        controller=new WCT_TravelRequest_FormController();
        controller.employeeEmail='';
        controller.savePortRecord();
        controller.PortRecord.WCT_Travel_Start_Date__c = startdate;
        controller.PortRecord.WCT_Travel_End_Date__c = enddate;
        controller.PortRecord.WCT_Start_Date2__c=startdate;
        controller.PortRecord.WCT_End_Date2__c=enddate;
        
        controller.PortRecord.WCT_Start_Date3__c=startdate;
        controller.PortRecord.WCT_End_Date3__c = enddate;
        controller.PortRecord.WCT_Start_Date4__c=startdate;
        controller.PortRecord.WCT_End_Date4__c=enddate;
        
        controller.PortRecord.WCT_Start_Date5__c=startdate;
        controller.PortRecord.WCT_End_Date5__c = enddate;
        
        
        controller.savePortRecord();
        
        
        
        controller.getEmployeeDetails();
        controller.employeeEmail=con.email;
        controller.getEmployeeDetails();
       
        controller.savePortRecord(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        
        controller.employeeEmail='testingmobility@test.com';
        
        controller.cancelPortRecord();
        
        try{
             controller.getEmployeeDetails();
        }
        catch(Exception e)
        {}
        controller.invalidEmployee=true;
        controller=new WCT_TravelRequest_FormController();
        Test.stoptest();
    }
    
    


}