public class WCT_US_Departure_FormController  extends SitesTodHeaderController{

    /* public variables */
    public WCT_Mobility__c mobilityRecord{get;set;}
  
    /* Upload Related Variables */
    public Document doc {get;set;}
    public List<String> docIdList = new List<string>();
    public List<AttachmentsWrapper> UploadedDocumentList {get; set; }
    public task t{get;set;}
    public String taskid{get;set;}
    public List<Attachment> listAttachments {get; set;}
    public Map<Id, String> mapAttachmentSize {get; set;}  

    // Error Message related variables
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}
    
     //Dates
    public String departureDayUS {get;set;}
    public String arrivalDayUSI {get;set;}
    public String fromDate {get;set;}
    public String toDate {get;set;}  
    
    //Controller Related Variables
    public List<SelectOption> takePTOOptions{set; get;}
    public String takePTO{set; get;}   
 
    public WCT_US_Departure_FormController()
    {
        /*Initialize all Variables*/
        init();

        /*Get Task ID from Parameter*/
        getParameterInfo();  
        
        /*Task ID Null Check*/
        if(taskid=='' || taskid==null)   
        {
           invalidEmployee=true;
           return;
        }

        /*Get Task Instance*/
        getTaskInstance();
        
        /*Get Attachment List and Size*/
        getAttachmentInfo();
        
        /*Query to get Immigration Record*/  
        getMobilityDetails();
    }

    private void init(){

        //Custom Object Instances
        mobilityRecord = new  WCT_Mobility__c();
        
        //Dropdown related
        takePTOOptions=new List<SelectOption>();
        takePTOOptions.add(new SelectOption('Yes','Yes'));
        takePTOOptions.add(new SelectOption('No','No'));
        takePTO='No';
      
         
        //Document Related Init
        doc = new Document();
        UploadedDocumentList = new List<AttachmentsWrapper>();
        listAttachments = new List<Attachment>();
        mapAttachmentSize = new Map<Id, String>();
        
      
    }   

    private void getParameterInfo(){
        taskid = ApexPages.currentPage().getParameters().get('taskid');
    }

    private void getTaskInstance(){
        t=[SELECT Status,OwnerId,WhatId,WCT_Is_Visible_in_TOD__c, WCT_Auto_Close__c  FROM Task WHERE Id =: taskid];
    }

    private void getAttachmentInfo(){
        listAttachments = [SELECT ParentId , 
                                    Name, 
                                    BodyLength, 
                                    Id,
                                    CreatedDate
                                    FROM  Attachment 
                                    WHERE ParentId = :taskid
                                    ORDER BY CreatedDate DESC
                                    LIMIT 50 ];    
              
        for(Attachment a : listAttachments) {
            String size = null;

            if(1048576 < a.BodyLength){
                // Size greater than 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength){
                // Size greater than 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else{
                size = '' + a.BodyLength + ' bytes';
            }
            mapAttachmentSize.put(a.id, size);
        }
    }

    public void getMobilityDetails()
    {
        mobilityRecord = [SELECT id,WCT_Departure_From_US__c,WCT_Arrive_In_USI__c,WCT_From_Date__c,WCT_To_Date__c,OwnerId FROM WCT_Mobility__c
                            where id=:t.WhatId];
                           
    }


    public pageReference save()
    {
        
        /*Validation Rules*/
        if( (null == departureDayUS) ||('' == departureDayUS) || (null == arrivalDayUSI) ||('' == arrivalDayUSI))
            {
            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
        }
        if(String.isNotBlank(departureDayUS)){
            mobilityRecord.WCT_Departure_From_US__c = Date.parse(departureDayUS);
        }
        if(String.isNotBlank(arrivalDayUSI)){
            mobilityRecord.WCT_Arrive_In_USI__c = Date.parse(arrivalDayUSI);
        }
        if(mobilityRecord.WCT_Arrive_In_USI__c < mobilityRecord.WCT_Departure_From_US__c)
        {
            pageErrorMessage = 'Arrival date cannot be less than departure date';
            pageError = true;
            return null;
        }
        if(takePTO=='Yes')
        {
            if( (null == fromDate) ||('' == fromDate) || (null == toDate) ||('' == toDate))
            {
            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
            }
            if(String.isNotBlank(fromDate)){
                mobilityRecord.WCT_From_Date__c = Date.parse(fromDate);
            }
            if(String.isNotBlank(toDate)){
                mobilityRecord.WCT_To_Date__c = Date.parse(toDate);
            }
            if(mobilityRecord.WCT_To_Date__c < mobilityRecord.WCT_From_Date__c)
            {
                pageErrorMessage = 'To date cannot be less tha from date';
                pageError = true;
                return null;
            }
       }
        /*Changing the Owner Id of Task Temporarily to avoid Integrity Exception*/
        t.OwnerId=UserInfo.getUserId();
        upsert t;
           
        update mobilityRecord;

        /* Upload Documents as Related Attachments to Case After the Case Record is created */
        uploadRelatedAttachment();

        if(string.valueOf(mobilityRecord.Ownerid).startsWith('00G')){
            //owner is Queue. So set it to GM&I User
            //t.Ownerid = '005f0000001AWth';
            t.Ownerid = System.Label.GMI_User;
        }else{
            t.OwnerId = mobilityRecord.Ownerid;
        }
        
        //updating task record
        if(t.WCT_Auto_Close__c == true){   
            t.status = 'completed';
        }else{
            t.status = 'Employee Replied';  
        }

        t.WCT_Is_Visible_in_TOD__c = false; 
        upsert t;
        
        //Reset Page Error Message
        pageError = false;

        getAttachmentInfo();
        /*Set Page Reference After Save*/
        return new PageReference('/apex/WCT_US_Departure_Form_Thankyou?em='+CryptoHelper.encrypt(LoggedInContact.Email)+'&taskid='+taskid) ;  
        
        
    }

    private void uploadRelatedAttachment(){
        if(!docIdList.isEmpty()) { //If there is any Documents Attached in Web Form

            List<String> selectedDocumentId = new List<String>();
            for(AttachmentsWrapper aWrapper : UploadedDocumentList) {
                if(aWrapper.isSelected) {
                    selectedDocumentId.add(aWrapper.documentId);
                }
            }
            
            /* Select Documents which are Only Active (Selected) */
            List<Document> selectedDocumentList = new List<Document>();
            if(!selectedDocumentId.isEmpty()){
                selectedDocumentList = [
                                           SELECT 
                                               id,
                                               name,
                                               ContentType,
                                               Type,
                                               Body 
                                           FROM 
                                               Document 
                                           WHERE 
                                               id IN :selectedDocumentId
                                        ];
            }
            
            /* Convert Documents to Attachment */
            List<Attachment> attachmentsToInsertList = new List<Attachment>();
            for(Document doc : selectedDocumentList){
                Attachment a = new Attachment();
                a.body = doc.body;
                a.ContentType = doc.ContentType;
                a.Name= doc.Name;
                a.Description=String.valueof(taskid);
                a.parentid = taskid; //immigration Record Id which is been retrived.
                attachmentsToInsertList.add(a);                
            }
            
            if(!attachmentsToInsertList.isEmpty()){
                insert attachmentsToInsertList;
            }
            
            List<Document> listDocuments = new List<Document>();
            listDocuments = [
                                SELECT
                                    Id
                                FROM
                                    Document
                                WHERE
                                    Id IN :docIdList
                            ];
            if( (null != listDocuments) && (0 < listDocuments.size()) ) {
                delete listDocuments;
            }
        }
    }

    /*Invoked when Upload Button in VF page is clicked and the IDs are stored in the docIdList*/
    /*All the Files Uploaded are stored in the Documents. Documents does not require parent Id.This method is used to circumvent to upload
      the documents first and then add as Attachment to the Cases (Related List)*/

    public void uploadAttachment(){
        pageError = false;
        pageErrorMessage = '';
    
        if(ApexPages.hasMessages()) {
            pageErrorMessage = '';
            for(ApexPages.Message message : ApexPages.getMessages()) {
                pageErrorMessage += message.getSummary() + '\n';
            }            
            doc = new Document();
            pageError = true;
            return;
        }                                
        
        doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
        if(doc.body != null) {
            insert doc;
            docIdList.add(doc.id);
            doc = [SELECT id, name, bodylength FROM Document WHERE id = :doc.id];

            String size = '';
            if(1048576 < doc.BodyLength) {
                // Size greater than 1MB
                size = '' + (doc.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < doc.BodyLength) {
                // Size greater than 1KB
                size = '' + (doc.BodyLength / 1024) + ' KB';            
            }
            else {
                size = '' + doc.BodyLength + ' bytes';
            }
            UploadedDocumentList.add(new AttachmentsWrapper(true, doc.name, doc.id, size));
            doc = new Document();
        }
    }       
    

    //Documents Wrapper
    public class AttachmentsWrapper {
        public Boolean isSelected {get;set;}
        public String docName {get;set;}
        public String documentId {get;set;}
        public String size {get; set;}
            
        public AttachmentsWrapper(Boolean selected, String Name, String Id, String size){
            isSelected = selected;
            docName = Name ;
            documentId = Id;
            this.size = size;
        }        
    }
}