@isTest
private class WCT_UnreadEmailsView_Test{
    static testMethod void myUnitTest() {
        Test_Data_Utility.createCase();
        List<Case> caseList = [select Id, Case.WCT_Send_Email_On__c,
             Case.WCT_Corporate_Card_Team__c,
             Case.WCT_PDA__c,
             Case.WCT_Record_Management__c,
             Case.WCT_Immigration_Mail__c,
             Case.WCT_Vol_Restricted_Entity__c,
             Case.WCT_Accounting_Firm__c,
             Case.WCT_Resignation_Notification_ERS__c,
             Case.WCT_Request_for_New_Employer_Information__c,
             Case.WCT_Involuntary_Corporate_Cards__c,
             Case.WCT_Involuntary_PDA__c,
             Case.WCT_Involuntary_Termination__c,
             Case.WCT_Involuntary_Termination_Request_PTO__c,
             Case.WCT_Involuntary_Records_Management__c from Case limit 1];
        Profile prof = [Select Id from Profile where Name = 'System Administrator'];
        User usr = [Select Id, Email from User where isActive = true and ProfileId = :prof.Id limit 1];
        System.runAs(usr) {
            EmailMessage em = new EmailMessage();
            em.FromAddress = UserInfo.getUserEmail();
            em.FromName = UserInfo.getName();
            em.Headers = 'test header';
            em.HtmlBody = '<html><body>test body</body></html>';
            em.ParentId = caseList[0].Id;
            em.ToAddress = UserInfo.getUserEmail();
            insert em;
            WCT_UnreadEmailsView controller= new  WCT_UnreadEmailsView();
            controller.getEmailMessages();
            controller.selectedEmailStatus = String.valueOf(0);
            controller.getEmailMessages();
            controller.selectedEmailStatus = String.valueOf(5);
            controller.getEmailMessages();
            controller.selectedEmailStatus = String.valueOf(6);
            controller.getEmailMessages();
            controller.selectedEmailStatus = String.valueOf(7);
            controller.getEmailMessages();
            controller.selectedEmailStatus = String.valueOf(8);
            controller.getEmailMessages();
            controller.selectedEmailStatus = String.valueOf(9);
            controller.getEmailMessages();
            controller.getEmailStatuses();
            WCT_UnreadEmailsView.ShowAllEmailWrapper emailWrapper = New WCT_UnreadEmailsView.ShowAllEmailWrapper(em, UserInfo.getUserName());
        }
    }
}