@isTest
public class WCT_UpdateCTMileStone_Test {

    /*******Varibale Declaration********/
    public static Id profileIdRecCoo=[Select id from Profile where Name=:WCT_UtilConstants.Recruiting_Coordinator].Id;
    public static Id profileIdIntv=[Select id from Profile where Name=:WCT_UtilConstants.INTERVIWER_PROFILE_NAME].Id;
    public static Id profileIdRecr=[Select id from Profile where Name=:WCT_UtilConstants.RECRUITER_COMPANY].Id;
    public static String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();
    public static User userRecRecCoo, userRecRecIntv, userRecRecr;
    public static Contact Candidate;
    public static WCT_Requisition__c Requisition;
    public static WCT_Candidate_Requisition__c CandidateRequisition;
    
    /***************/

    /** 
        Method Name  : createUserRecCoo
        Return Type  : User
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecCoo()
    {
        userRecRecCoo=WCT_UtilTestDataCreation.createUser('RecCoo', profileIdRecCoo, 'arunsharmaRecCoo@deloitte.com', 'arunsharma4@deloitte.com');
        insert userRecRecCoo;
        return  userRecRecCoo;
    }
    /** 
        Method Name  : createUserRecr
        Return Type  : User
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecr()
    {
        userRecRecr=WCT_UtilTestDataCreation.createUser('Recr', profileIdRecr, 'arunsharmaRecr@deloitte.com', 'arunsharma4@deloitte.com');
        insert userRecRecr;
        return  userRecRecr;
    }
    /** 
        Method Name  : createUserIntv
        Return Type  : User
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserIntv()
    {
        userRecRecIntv=WCT_UtilTestDataCreation.createUser('Intv', profileIdIntv, 'arunsharmaIntv@deloitte.com', 'arunsharma4@deloitte.com');
        insert userRecRecIntv;
        return  userRecRecIntv;
    }
    /** 
        Method Name  : createCandidate
        Return Type  : Contact
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static Contact createCandidate()
    {
        Candidate=WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
        insert Candidate;
        return  Candidate;
    }    
    /** 
        Method Name  : createRequisition
        Return Type  : WCT_Requisition__c
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Requisition__c createRequisition()
    {
        Requisition=WCT_UtilTestDataCreation.createRequisition(userRecRecr.Id);
        insert Requisition;
        return  Requisition;
    }
    /** 
        Method Name  : createCandidateRequisition
        Return Type  : WCT_Candidate_Requisition__c
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Candidate_Requisition__c createCandidateRequisition()
    {
        CandidateRequisition=WCT_UtilTestDataCreation.createCandidateRequisition(Candidate.ID,Requisition.Id);
        insert CandidateRequisition;
        return  CandidateRequisition;
    } 
    
    static testMethod void myUnitTest() {
        userRecRecCoo=createUserRecCoo();
        userRecRecIntv=createUserIntv();
        userRecRecr=createUserRecr();
        Candidate=createCandidate();
        Requisition=createRequisition();
        CandidateRequisition=createCandidateRequisition();
        ApexPages.currentPage().getParameters().put('reqId', Requisition.Id);
        test.startTest();
        WCT_UpdateCTMileStone_Ctrl uctm = new WCT_UpdateCTMileStone_Ctrl();
        uctm.CTList[0].WCT_Interview_Status__c = 'Technical Screen – To Be Initiated';
        uctm.CTList[0].WCT_Interview_Step__c = 'Initial Screen';
        uctm.updateCTRecords();
        uctm.cancel();
        test.stopTest();
    }
    
}