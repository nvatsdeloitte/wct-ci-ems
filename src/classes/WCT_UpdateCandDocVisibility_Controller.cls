/**
 * Class Name  : WCT_UpdateCandidateDocVisibility_Controller   
 * Description : To Update Candidate Doc visibility at Interview level 
 */
 public class WCT_UpdateCandDocVisibility_Controller{
 
    //Variable Declaration
    public String IntvId {get;set;}
    public set<Id> candidateIdSet {get;set;}
    public List<CandDocWrapper> candDocWrapList{get;set;}
    public List<WCT_Candidate_Documents__c> AllcandDocs{get;set;}
    public Map<Id,boolean> AllCandDocMap {get;set;}
    public Map<id,List<WCT_Candidate_Documents__c>> candDocWrapMap {get;set;}
    public List<WCT_Candidate_Documents__c> candDocToUpdate {get;set;}
    
    public class CandDocWrapper{
        public string candName {get;set;}
        public List<WCT_Candidate_Documents__c> candDocList {get;set;}
        
        public CandDocWrapper(string conId,List<WCT_Candidate_Documents__c> cdList){
            candName = conId;
            candDocList = cdList;
        }   
    }
    
    //constructor
    public WCT_UpdateCandDocVisibility_Controller(){
        candidateIdSet = new set<Id>();
        candDocWrapList = new List<CandDocWrapper>();
        AllcandDocs = new List<WCT_Candidate_Documents__c>();
        AllCandDocMap = new Map<Id,boolean>();
        candDocWrapMap = new Map<id,List<WCT_Candidate_Documents__c>>();
        IntvId = ApexPages.currentPage().getParameters().get('id');
        WCT_Interview__c interview = [SELECT id,(SELECT id,WCT_Candidate_Tracker__c,WCT_Candidate_Tracker__r.WCT_Contact__c 
                                                FROM Interview_Candidate_Tracker_Junction__r) FROM WCT_Interview__c WHERE id = :IntvId];
        for(WCT_Interview_Junction__c  ij : interview.Interview_Candidate_Tracker_Junction__r){
            candidateIdSet.add(ij.WCT_Candidate_Tracker__r.WCT_Contact__c);
        }
        system.debug('%%%'+candidateIdSet);
        if(!candidateIdSet.isEmpty()){
            for(WCT_Candidate_Documents__c cd :[SELECT id,Attachment__c,CreatedDate,WCT_Candidate__c,WCT_Candidate__r.name,WCT_File_Name__c,WCT_RMS_ID__c,WCT_Visible_to_Interviewer__c FROM
                                                WCT_Candidate_Documents__c WHERE WCT_Candidate__c IN :candidateIdSet ORDER BY CreatedDate DESC]){
                if(candDocWrapMap.ContainsKey(cd.WCT_Candidate__c)){
                    List<WCT_Candidate_Documents__c> tmpList = new List<WCT_Candidate_Documents__c>();
                    tmpList = candDocWrapMap.get(cd.WCT_Candidate__c);
                    tmpList.add(cd);
                    candDocWrapMap.put(cd.WCT_Candidate__c,tmpList);
                }else{
                    List<WCT_Candidate_Documents__c> tmpcdList = new List<WCT_Candidate_Documents__c>();
                    tmpcdList.add(cd);
                    candDocWrapMap.put(cd.WCT_Candidate__c,tmpcdList);
                }
            
            }   

        }
        
        if(!candDocWrapMap.isEmpty()){
            for(Id candId : candDocWrapMap.keySet()){
                candDocWrapList.add(new CandDocWrapper(candDocWrapMap.get(candId)[0].WCT_Candidate__r.name,candDocWrapMap.get(candId)));
            } 
            for(CandDocWrapper wrap:candDocWrapList){
                AllcandDocs.addAll(wrap.candDocList);
            }
            for(WCT_Candidate_Documents__c cd:AllcandDocs){
                AllCandDocMap.put(cd.id,cd.WCT_Visible_to_Interviewer__c);
            }
        }
        
        
    
    }
    
    public pageReference updateCandDoc(){
        List<WCT_Candidate_Documents__c> candDocToUpdateTemp = new List<WCT_Candidate_Documents__c>();
        candDocToUpdate = new List<WCT_Candidate_Documents__c>();
        for(CandDocWrapper wrap:candDocWrapList){
            candDocToUpdateTemp.addAll(wrap.candDocList);
        }
        system.debug('%%%'+candDocToUpdateTemp);
        for(WCT_Candidate_Documents__c cd:candDocToUpdateTemp){
            system.debug('^^^'+AllCandDocMap.get(cd.id)+'***'+cd.WCT_Visible_to_Interviewer__c );
            if(cd.WCT_Visible_to_Interviewer__c <> AllCandDocMap.get(cd.id)){
                candDocToUpdate.add(cd);
            }           
        }
        system.debug('%%%'+candDocToUpdate);
        if(!candDocToUpdate.isEmpty()){
            update candDocToUpdate;
        }
        return new PageReference('/'+IntvId);
    } 
    
    public pageReference Cancel(){
        return new PageReference('/'+IntvId);
    } 
 }