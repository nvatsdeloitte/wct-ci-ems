/**  
    * Class Name  : WCT_UpdateCandTracker 
    * Description : This apex class will use to update Candidate Tracker's Interview Milestone
*/
public class WCT_UpdateCandTracker{
    public String nameFile{get;set;}
    public Blob contentFile{get;set;}
    public Integer noOfRows{get;set;}
    public Integer noOfRowsProcessed{get;set;}
    public Integer noOfError{get;set;}
    public List<List<String>> fileLines = new List<List<String>>();
    public Set<Id> stagingIds=new Set<Id>();
    
    public boolean duplicateRows = false;
    
    public List<WCT_sObject_Staging_Records__c> stagingRecordsList = new List<WCT_sObject_Staging_Records__c>();
    
    public static Map<String,List<String>> stepMilestoneValMap = new Map<String,List<String>>();

    public static Map<String,List<String>> getCTMappingfromLON(){
        if(stepMilestoneValMap.isEmpty()){
            for(WCT_List_Of_Names__c lon:[SELECT WCT_Interview_Step__c,WCT_Interview_Milestone__c,WCT_Type__c FROM WCT_List_Of_Names__c 
                                        WHERE WCT_Type__c = 'CTStepMileStoneMapping']){
                if(!stepMilestoneValMap.containsKey(lon.WCT_Interview_Step__c)){
                    list<string> tmpList = new list<string>();
                    tmpList.add(lon.WCT_Interview_Milestone__c);
                    stepMilestoneValMap.put(lon.WCT_Interview_Step__c,tmpList);
                }else{
                    list<string> tmpList = new list<string>();
                    tmpList = stepMilestoneValMap.get(lon.WCT_Interview_Step__c);
                    tmpList.add(lon.WCT_Interview_Milestone__c);
                    stepMilestoneValMap.put(lon.WCT_Interview_Step__c,tmpList);
                }
            }
        }
        return stepMilestoneValMap;

    }
    
    /** 
        Method Name  : readFile
        Return Type  : PageReference
        Description  : Read CSV and push records into staging table        
    */
    
    public Pagereference readFile()
    {
        stagingRecordsList = new List<WCT_sObject_Staging_Records__c>(); 
        noOfRowsProcessed=0;
        try{
            duplicateRows = false;
            stagingRecordsList = mapStagingRecords();
            if(duplicateRows){
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,'Duplicate unique Ids OR Blank Unique ID in file');
                ApexPages.addMessage(errormsg);
                return null;
            }
            
        }
        Catch(System.StringException stringException)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_String_Exception_Error+stringException.getMessage());
            ApexPages.addMessage(errormsg);    
        }
        Catch(System.ListException listException)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Columns_count_not_proper);
            ApexPages.addMessage(errormsg); 
        
        }
        
        Catch(Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_Exception);
            ApexPages.addMessage(errormsg);    
        
        }
        if(stagingRecordsList.size()>Limits.getLimitQueryRows())
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Error_Data_File_too_large + Limits.getLimitQueryRows());
            ApexPages.addMessage(errormsg);   
        }
        Database.SaveResult[] srList = Database.insert(stagingRecordsList, false);
        for(Database.SaveResult sr: srList)
        {
            if(sr.IsSuccess())
            {   
                stagingIds.add(sr.getId());
                noOfRowsProcessed=stagingIds.size();
            }
            else if(!sr.IsSuccess())
            {
                for(Database.Error err : sr.getErrors())
                {
                     ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.WCT_Data_Error+err.getMessage());
                     ApexPages.addMessage(errormsg);       
                }
            }
        }   
        if(!stagingIds.IsEmpty())
        {
        
            try{
                processStagingRecords(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
            }Catch(Exception e)
            {
                throw e;
            }
            
        }
          
        return null;
    }
    
    private List<WCT_sObject_Staging_Records__c> mapStagingRecords()
    {
        nameFile=contentFile.toString();
        filelines=parseCSV(nameFile, true);
        List<WCT_sObject_Staging_Records__c> stagingRecordsList=new List<WCT_sObject_Staging_Records__c>();
        noOfRows=fileLines.size();
        system.debug(filelines);
        set<string> uniqueIdSet = new set<string>();
        for(List<String> inputValues:filelines) 
        {   
            
            system.debug(inputValues.size()+'stringSize');
            WCT_sObject_Staging_Records__c stagingRecord = new WCT_sObject_Staging_Records__c();
            if(inputValues[0] <> null){
                if(!uniqueIdSet.contains(inputValues[0])){
                    uniqueIdSet.add(inputValues[0]);
                }else{
                    duplicateRows = true;
                }
            }else{
                duplicateRows = true;
            }
            stagingRecord.WCT_RMS_Requisition__c=inputValues[0];
            stagingRecord.WCT_Interview_Step__c=inputValues[1];
            stagingRecord.WCT_Interview_Status__c=inputValues[2];
            stagingRecord.WCT_Object__c = 'WCT_Candidate_Requisition__c';
            stagingRecord.WCT_status__c=WCT_UtilConstants.STAGING_STATUS_NOT_STARTED;
            stagingRecordsList.add(stagingRecord);
        }
        return stagingRecordsList;
    }

    /** 
        Method Name  : parseCSV
        Return Type  : String contents,Boolean skipHeaders
        Description  : Business logic for parsing the string         
    */
    private List<List<String>> parseCSV(String contents,Boolean skipHeaders) {
        List<List<String>> allFields = new List<List<String>>();
    
        // replace instances where a double quote begins a field containing a comma
        // in this case you get a double quote followed by a doubled double quote
        // do this for beginning and end of a field
        contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
        // now replace all remaining double quotes - we do this so that we can reconstruct
        // fields with commas inside assuming they begin and end with a double quote
        contents = contents.replaceAll('""','DBLQT');
        // we are not attempting to handle fields with a newline inside of them
        // so, split on newline to get the spreadsheet rows
        List<String> lines = new List<String>();
        try {
            lines = contents.split('\r\n');
        } catch (System.ListException e) {
            System.debug('Limits exceeded?' + e.getMessage());
        }
        system.debug(lines+'lines');
        Integer num = 0;
        for(String line : lines) {
            system.debug('line'+line);
            // check for blank CSV lines (only commas)
            if (line.replaceAll(',','').trim().length() == 0) continue;
            
            List<String> fields = line.split(',');  
            system.debug(fields);
            List<String> cleanFields = new List<String>();
            String compositeField;
            Boolean makeCompositeField = false;
            for(String field : fields) {
                if (field.startsWith('"') && field.endsWith('"')) {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                } else if (field.startsWith('"')) {
                    makeCompositeField = true;
                    compositeField = field;
                } else if (field.endsWith('"')) {
                    compositeField += ',' + field;
                    cleanFields.add(compositeField.replaceAll('DBLQT','"'));
                    makeCompositeField = false;
                } else if (makeCompositeField) {
                    compositeField +=  ',' + field;
                } else {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                }
            }
            
            allFields.add(cleanFields);
        }
        if (skipHeaders) allFields.remove(0);
        return allFields;       
    }
    
    public void processStagingRecords(Set<Id> stagingIds, String stagingStatus){
        set<string> CTUniqueIdentifiesSet = new set<string>();
        Map<String,WCT_sObject_Staging_Records__c> ctIndentifierStgRecMap = new Map<String,WCT_sObject_Staging_Records__c>();
        List<WCT_Candidate_Requisition__c> candTrackListToUpdate = new List<WCT_Candidate_Requisition__c>();
        List<WCT_sObject_Staging_Records__c> stgRecToUpdate = new List<WCT_sObject_Staging_Records__c>();
        Map<Id,string> CTIdUniqueIdentifierMap = new Map<Id,string>();
        
        set<string> CTUniqueIdentifiesSetfromSys = new set<string>();

        for(WCT_sObject_Staging_Records__c stgRec:[SELECT id,WCT_Interview_Status__c,WCT_Interview_Step__c,WCT_Related_Candidate_Tracker__c,
                                                    WCT_RMS_Requisition__c,WCT_Status__c FROM WCT_sObject_Staging_Records__c 
                                                    WHERE id IN :stagingIds AND WCT_Status__c = : stagingStatus]){
                                                    
                                                    
            Boolean isValidValues = true;
            if(stgRec.WCT_Interview_Step__c <> null){
                isValidValues = validateStepValues(stgRec.WCT_Interview_Step__c);
            }
            if(stgRec.WCT_Interview_Step__c <> null && stgRec.WCT_Interview_Status__c <> null){
                isValidValues = validateStepMilestoneValues(stgRec.WCT_Interview_Step__c,stgRec.WCT_Interview_Status__c);
            }
            
            if(stgRec.WCT_Interview_Step__c == null && stgRec.WCT_Interview_Status__c <> null){
                isValidValues = false;
            }
                                                    
            if(stgRec.WCT_RMS_Requisition__c <> null && isValidValues){
                CTUniqueIdentifiesSet.add(stgRec.WCT_RMS_Requisition__c);
                ctIndentifierStgRecMap.put(stgRec.WCT_RMS_Requisition__c,stgRec);
            }else{
                WCT_sObject_Staging_Records__c tmpStgRec = new WCT_sObject_Staging_Records__c();
                tmpStgRec = stgRec;
                tmpStgRec.WCT_Status__c = 'Failed';
                tmpStgRec.WCT_Error_Message__c = 'Either Unique Id is null OR Step Milestone values are not correct';
                stgRecToUpdate.add(tmpStgRec);
            }        
        
        }
        
        if(!CTUniqueIdentifiesSet.isEmpty()){
            for(WCT_Candidate_Requisition__c ct :[SELECT id,WCT_RMS_Requisition__c, WCT_Interview_Step__c,WCT_Interview_Status__c 
                                                  FROM WCT_Candidate_Requisition__c WHERE WCT_RMS_Requisition__c IN :CTUniqueIdentifiesSet]){
                
                WCT_Candidate_Requisition__c tempRec= new WCT_Candidate_Requisition__c();
                tempRec = ct;
                tempRec.WCT_Interview_Step__c = ctIndentifierStgRecMap.get(ct.WCT_RMS_Requisition__c).WCT_Interview_Step__c;
                tempRec.WCT_Interview_Status__c = ctIndentifierStgRecMap.get(ct.WCT_RMS_Requisition__c).WCT_Interview_Status__c;
                candTrackListToUpdate.add(tempRec);
                CTIdUniqueIdentifierMap.put(ct.id,ct.WCT_RMS_Requisition__c);
                CTUniqueIdentifiesSetfromSys.add(ct.WCT_RMS_Requisition__c);
            }   
        }
        for(string str:CTUniqueIdentifiesSet){
            if(!CTUniqueIdentifiesSetfromSys.contains(str)){
                system.debug('enterederror');
                WCT_sObject_Staging_Records__c tmpStgRec = new WCT_sObject_Staging_Records__c();
                tmpStgRec = ctIndentifierStgRecMap.get(str);
                tmpStgRec.WCT_Status__c = 'Failed';
                tmpStgRec.WCT_Error_Message__c = 'No Candidate Tracker with this Unique Identifier';
                stgRecToUpdate.add(tmpStgRec);
            }
        
        }
        if(!candTrackListToUpdate.isEmpty()){
            Database.SaveResult[] srList = Database.update(candTrackListToUpdate, false);
            for(Database.SaveResult sr: srList)
            {
                if(sr.isSuccess())
                {
                    WCT_sObject_Staging_Records__c tmpStgRec = new WCT_sObject_Staging_Records__c();
                    tmpStgRec = ctIndentifierStgRecMap.get(CTIdUniqueIdentifierMap.get(sr.getId()));
                    tmpStgRec.WCT_Status__c = 'Completed';
                    tmpStgRec.WCT_Error_Message__c = 'No Error';
                    stgRecToUpdate.add(tmpStgRec);
                    
                }
                if(!sr.IsSuccess())
                {
                    for(Database.Error err : sr.getErrors())
                    {
                        //ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,'Error in processing in some records'+err);
                        //ApexPages.addMessage(errormsg);       
                    }
                }
            }
            system.debug('size'+stgRecToUpdate.size());
        }
        
        if(!stgRecToUpdate.isEmpty()){
            Database.update(stgRecToUpdate, false);
        }

    }
    
    public boolean validateStepValues(string value){
        Map<String,List<String>> stepMilestoneValMapTemp = new Map<String,List<String>>();
        stepMilestoneValMapTemp = getCTMappingfromLON();
        if(!stepMilestoneValMapTemp.containsKey(value)){
            return false;
        }else{
            return true;
        }
    }

    public boolean validateStepMilestoneValues(string stepvalue,string milestoneValue){
        Map<String,List<String>> stepMilestoneValMapTemp = new Map<String,List<String>>();
        stepMilestoneValMapTemp = getCTMappingfromLON();
        if(!stepMilestoneValMapTemp.containsKey(stepvalue)){
            return false;
        }else{
            set<string> tmpSet = new set<string>();
            tmpSet.addAll(stepMilestoneValMapTemp.get(stepvalue));
            if(tmpSet.contains(milestoneValue)){
                return true;
            }else{
                return false;
            }
        }
    }
    
     public List<WCT_sObject_Staging_Records__c> getAllStagingRecords()
    {
        if (stagingIds!= NULL)
            if (stagingIds.size() > 0)
            {
                return [Select id,Name, WCT_Status__c,WCT_RMS_Requisition__c,WCT_Interview_Step__c,WCT_Interview_Status__c,WCT_Error_Message__c from WCT_sObject_Staging_Records__c where Id In:stagingIds and WCT_Status__c='Failed'];
                
            }
            else
                return null;                   
        else
            return null;
    } 

}