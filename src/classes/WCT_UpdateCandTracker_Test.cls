@isTest
private class WCT_UpdateCandTracker_Test {
  
    public static String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();
    public static List<Contact> contactList=new List<Contact>();
    public static User userRec;
    public static Id profileId=[Select id from Profile where Name=:WCT_UtilConstants.RECRUITER_COMPANY].Id;
    public static WCT_Requisition__c req;
    public static WCT_Candidate_Requisition__c candTarcker;
    public static list<WCT_List_Of_Names__c> LONList = new list<WCT_List_Of_Names__c>();
        
    /** 
        Method Name  : createContacts
        Return Type  : List<Contact>
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private static List<Contact> createContacts()
    {
      contactList=WCT_UtilTestDataCreation.createContactWithCandidate(candidateRecordTypeId);
      insert contactList;
      return contactList;
    }
    /** 
        Method Name  : createReq
        Return Type  : List<Contact>
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private static WCT_Requisition__c createRequistion()
    {
      req=WCT_UtilTestDataCreation.createRequistion();
      insert req;
      return req;
    }
    /** 
        Method Name  : create Candidate tracker
        Return Type  : WCT_Candidate_Requisition__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private static WCT_Candidate_Requisition__c createCandTracker()
    {
      candTarcker=WCT_UtilTestDataCreation.createCandidateRequisition(contactList[0].id,req.id);
      insert candTarcker;
      return candTarcker;
    }
    /** 
        Method Name  : createuser
        Return Type  : User
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUser()
    {
      userRec=WCT_UtilTestDataCreation.createUser('PBICSV', profileId, 'vinBhatiaPreBI@deloitte.com', 'vinBhatiaPreBI@deloitte.com');
      insert userRec;
      return  userRec;
    }
    
    /**
        Method Name: create LON record
        Return type : WCT_List_Of_Names__c
        Type : private
    **/
    private static List<WCT_List_Of_Names__c> createLON(){
            WCT_List_Of_Names__c tmp = new WCT_List_Of_Names__c();
            tmp.WCT_Type__c = 'CTStepMileStoneMapping';
            tmp.WCT_Interview_Step__c = 'Initial Screen';
            tmp.WCT_Interview_Milestone__c = 'Technical Screen – To Be Initiated';
            LONList.add(tmp);
            
            WCT_List_Of_Names__c tmpv1 = new WCT_List_Of_Names__c();
            tmpv1.WCT_Type__c = 'CTStepMileStoneMapping';
            tmpv1.WCT_Interview_Step__c = 'Initial Screen';
            tmpv1.WCT_Interview_Milestone__c = 'Technical Screen – Conducted';
            LONList.add(tmpv1);
            insert LONList;
            return LONList;
    }   
    
    /** 
        Method Name  : noOfColsException
        Return Type  : void
        Type      : Test Method
        Description  : Test will do on number of column in CSV and send error message on page.         
    */  
    static testMethod void noOfColsException() {
      contactList=createContacts();
      String blobCreator = 'UNIQUEId' + '\r\n' + 'Step' + '\r\n'  ; 
      PageReference pageRef = Page.WCT_UpLoadCandTracker_Page;
      Test.setCurrentPageReference(pageRef);
      WCT_UpdateCandTracker uploadCT=new WCT_UpdateCandTracker();
      uploadCT.contentFile=blob.valueof(blobCreator);
      uploadCT.readFile();
      List<Apexpages.Message> msgs = ApexPages.getMessages(); 
      boolean b = false;
      for(Apexpages.Message msg:msgs){
          if (msg.getDetail().contains(Label.Columns_count_not_proper)) b = true;
       }
       system.assert(b);
    }
    
    /** 
        Method Name  : fileNotValid
        Return Type  : void
        Type      : Test Method
        Description  : Test will do when no file will be selected or file is not valid CSV.         
    */  
    static testMethod void fileNotValid() {
      contactList=createContacts();
      PageReference pageRef = Page.WCT_UpLoadCandTracker_Page;
      Test.setCurrentPageReference(pageRef);
      WCT_UpdateCandTracker uploadCT=new WCT_UpdateCandTracker();
      uploadCT.readFile();
      List<Apexpages.Message> msgs = ApexPages.getMessages(); 
      boolean b = false;
      for(Apexpages.Message msg:msgs){
          if (msg.getDetail().contains(Label.Upload_Case_Exception)) b = true;
       }
       system.assert(b);
    }
    
     /** 
        Method Name  : readFileMethod
        Return Type  : void
        Type      : Test Method
        Description  : Test will do read file in all cases.         
    */  
    static testMethod void readFileMethod() {
      
      contactList=createContacts();
      req = createRequistion();
      candTarcker = createCandTracker();
      LONList = createLON();
      String csvheader='Unique ID,STep,Milestone';
      String dataRow='12345,Initial Screen,Technical Screen – To Be Initiated';
      String blobCreator = csvheader + '\r\n' + dataRow ; 
      PageReference pageRef = Page.WCT_UpLoadCandTracker_Page;
      Test.setCurrentPageReference(pageRef);
      WCT_UpdateCandTracker uploadCT=new WCT_UpdateCandTracker();
      uploadCT.contentFile=blob.valueof(blobCreator);
      uploadCT.readFile();
      List<WCT_sObject_Staging_Records__c> StageRecordList=uploadCT.getAllStagingRecords();
      System.assertEquals(1, StageRecordList.Size());
      
    }
    
    /** 
        Method Name  : readFileMethodinvalidStep
        Return Type  : void
        Type      : Test Method
        Description  : Test will do read file in all cases.         
    */  
    static testMethod void readFileMethodinvalidStep() {
      
      contactList=createContacts();
      req = createRequistion();
      candTarcker = createCandTracker();
      LONList = createLON();
      String csvheader='Unique ID,STep,Milestone';
      String dataRow='12345,Initial Screen,Technical Screen – To Be';
      String blobCreator = csvheader + '\r\n' + dataRow ; 
      PageReference pageRef = Page.WCT_UpLoadCandTracker_Page;
      Test.setCurrentPageReference(pageRef);
      WCT_UpdateCandTracker uploadCT=new WCT_UpdateCandTracker();
      uploadCT.contentFile=blob.valueof(blobCreator);
      uploadCT.readFile();
      List<WCT_sObject_Staging_Records__c> StageRecordList=uploadCT.getAllStagingRecords();
      System.assertEquals(1, StageRecordList.Size());
      
    }
    
    /** 
        Method Name  : readFileMethodvalidData
        Return Type  : void
        Type      : Test Method
        Description  : Test will do read file in all cases.         
    */  
    static testMethod void readFileMethodvalidData() {
      
      contactList=createContacts();
      req = createRequistion();
      candTarcker = createCandTracker();
      LONList = createLON();
      string uniqId = [SELECT id,WCT_RMS_Requisition__c FROM WCT_Candidate_Requisition__c LIMIT 1].WCT_RMS_Requisition__c;
      String csvheader='Unique ID,STep,Milestone';
      String dataRow= uniqId+',Initial Screen,Technical Screen – To Be Initiated';
      String blobCreator = csvheader + '\r\n' + dataRow ; 
      PageReference pageRef = Page.WCT_UpLoadCandTracker_Page;
      Test.setCurrentPageReference(pageRef);
      WCT_UpdateCandTracker uploadCT=new WCT_UpdateCandTracker();
      List<WCT_sObject_Staging_Records__c> StageRecordList=uploadCT.getAllStagingRecords();
      uploadCT.contentFile=blob.valueof(blobCreator);
      uploadCT.readFile();
      
      //System.assertEquals(1, StageRecordList.Size());
      
    }
    
}