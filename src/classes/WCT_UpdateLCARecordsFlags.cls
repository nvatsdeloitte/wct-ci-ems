global class WCT_UpdateLCARecordsFlags {
    
    WebService static  String updateFlags(String recordId)
    {
        
        WCT_LCA__c rec;
        try{
            rec=new WCT_LCA__c(id=recordId);
            rec.WCT_Updated_Fragomen_record__c=false;
            //rec.WCT_Email_Received__c=false;
            update rec;
        
        }Catch(Exception e)
        {
            WCT_ExceptionUtility.logException('WCT_UpdateLCARecordsFlags','Update LCA Records Flags',e.getMessage());
        }
        
        return 'Thanks for reviewing the LCA record.';
        
    }

}