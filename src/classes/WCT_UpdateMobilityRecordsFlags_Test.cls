@isTest
private class WCT_UpdateMobilityRecordsFlags_Test{
    static testMethod void testMyWebSvc(){ 
    
        WCT_Mobility__c rec = new WCT_Mobility__c();
        INSERT rec;
    
        Test.startTest();
        WCT_UpdateMobilityRecordsFlags.updateFlags(rec.Id);
        Test.stopTest(); 
    } 
}