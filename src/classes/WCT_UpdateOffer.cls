global class WCT_UpdateOffer {
    WebService static String UpdateOffer(String OfferId, string Last_Name, string contId)
    {
    WCT_Offer__c offer = new WCT_Offer__c(id=OfferId); 
    offer.WCT_Offer_Letter_Reviewed__c = true;
    offer.WCT_Offer_Letter_Password__c = Last_Name; 
    update offer; 

    // UnCheck the checkbox WCT_Taleo_Field_Updated__c on related Contact
    Contact cont = new Contact(id=contId);
    cont.WCT_Taleo_Field_Updated__c = false;
    update cont;
    return null;
    }
    WebService static String UpdateOffers(List<Id> OfferIds)
    {
      list<WCT_Offer__C> OfferRecs = new list<WCT_Offer__C>();
      string lastNamePwd;
      set<id> ContRecIds = new set<id>();
      list<Contact> ContRecs = new list<Contact>(); 
      for(wct_offer__c OffRec : [select id,WCT_Last_Name__c,WCT_Candidate__c,WCT_Offer_Letter_Reviewed__c,WCT_Offer_Letter_Password__c 
                        from wct_offer__c where id in :OfferIds]){
        OffRec.WCT_Offer_Letter_Reviewed__c = true;
      lastNamePwd = OffRec.WCT_Last_Name__c.replaceAll('![a-zA-Z0-9]', '');
      lastNamePwd = lastNamePwd.replaceAll('[\']', ''); 
      OffRec.WCT_Offer_Letter_Password__c = lastNamePwd.replaceAll(' ', '');
      OfferRecs.add(OffRec);
      ContRecIds.add(OffRec.WCT_Candidate__c);
      }
      for(Contact ContRec : [select id,WCT_Taleo_Field_Updated__c from Contact where id in :ContRecIds]){
        ContRec.WCT_Taleo_Field_Updated__c = false;
        ContRecs.add(ContRec);
      }
      if(!OfferRecs.IsEmpty()){update OfferRecs;}
      if(!ContRecs.IsEmpty()){update ContRecs;}
      return null;
    }
}