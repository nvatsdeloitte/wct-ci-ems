global class WCT_UpdateRecordsFlags {
    
    WebService static  String updateFlags(String recordId)
    {
        
        WCT_Immigration__c rec;
        try{
            rec=new WCT_Immigration__c(id=recordId);
            rec.WCT_Updated_Fragomen_record__c=false;
            rec.WCT_Email_Received__c=false;
            update rec;
        
        }Catch(Exception e)
        {
            WCT_ExceptionUtility.logException('WCT_UpdateRecordsFlags','Update Records Flags',e.getMessage());
        }
        
        return 'Thanks for reviewing the Immigration record.';
        
    }

}