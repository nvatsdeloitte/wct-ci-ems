public without sharing class WCT_UpdateSupportAreaOnCase {
    
    public Case caseToUpdate {get;set;}
    
    public WCT_UpdateSupportAreaOnCase(){
         caseToUpdate = [SELECT contactId,Id,CaseNumber,WCT_ResolutionNotes__c,WCT_Support_Area__c FROM Case 
                   WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    }
    
    public PageReference  updateCase(){
        Update caseToUpdate;
        return new PageReference('/'+caseToUpdate.id);
    }
    
    public PageReference  cancel(){
        return new PageReference('/'+caseToUpdate.id);
    }

}