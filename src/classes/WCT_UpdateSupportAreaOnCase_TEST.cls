@isTest
private class WCT_UpdateSupportAreaOnCase_TEST {

    static testMethod void updateSupportAreaTest(){
        Test_Data_Utility.createCase();
        Case ca = [SELECT Id from case Limit 1];
        
        test.starttest();
        PageReference  currPage  = page.WCT_UpdateSupportAreaOnCase_Page;
        Test.setCurrentPage(currPage);
        ApexPages.currentPage().getParameters().put('id', ca.id);
        WCT_UpdateSupportAreaOnCase ctrl = new WCT_UpdateSupportAreaOnCase();
        ctrl.updateCase();
        ctrl.cancel();
        test.stoptest();
        
    }

}