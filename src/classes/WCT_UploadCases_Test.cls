/**
    * Class Name  : WCT_UploadCases_Test
    * Description : This apex test class will use to test the WCT_UploadCases
*/

@isTest
private class WCT_UploadCases_Test {
    
    public static String caseRecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get(WCT_UtilConstants.CASE_RECORDTYPE_PRE_BI).getRecordTypeId();
    public static String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();
    public static List<Contact> contactList=new List<Contact>();
    public static List<WCT_PreBIQAReferenceTable__c> refList=new List<WCT_PreBIQAReferenceTable__c>();
    public static User userRec;
    public static Id profileId=[Select id from Profile where Name=:WCT_UtilConstants.RECRUITER_COMPANY].Id;
    public static WCT_Requisition__c req;
    public static Id permissionSetID=[Select id from PermissionSet where name=:WCT_UtilConstants.PRE_BI_PERMISSION_SET_NAME].Id;
        
    /** 
        Method Name  : createContacts
        Return Type  : List<Contact>
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private static List<Contact> createContacts()
    {
        contactList=WCT_UtilTestDataCreation.createContactWithCandidate(candidateRecordTypeId);
        insert contactList;
        return contactList;
    }
    /** 
        Method Name  : createReq
        Return Type  : List<Contact>
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private static WCT_Requisition__c createRequistion()
    {
        req=WCT_UtilTestDataCreation.createRequistion();
        insert req;
        return req;
    }
    /** 
        Method Name  : createuser
        Return Type  : User
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUser()
    {
        userRec=WCT_UtilTestDataCreation.createUser('PBICSV', profileId, 'vinBhatiaPreBI@deloitte.com', 'vinBhatiaPreBI@deloitte.com');
        insert userRec;
        return  userRec;
    }
    /** 
        Method Name  : permissionSetAssignment
        Return Type  : Void
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static void permissionSetAssignment(Id userId)
    {
        PermissionSetAssignment assignmentUser=new PermissionSetAssignment();
        assignmentUser.AssigneeId=userId;
        assignmentUser.PermissionSetId=permissionSetID;
        insert assignmentUser;
        
    }
    /** 
        Method Name  : createPreBiReferenceRecords
        Return Type  : List<WCT_PreBIQAReferenceTable__c>
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private static List<WCT_PreBIQAReferenceTable__c> createPreBiReferenceRecords()
    {
        refList=WCT_UtilTestDataCreation.createPreBiReferenceRecords();
        insert refList;
        return refList;
    }
    
    /** 
        Method Name  : noOfColsException
        Return Type  : void
        Type         : Test Method
        Description  : Test will do on number of column in CSV and send error message on page.         
    */  
    static testMethod void noOfColsException() {
        refList=createPreBiReferenceRecords();
        contactList=createContacts();
        String blobCreator = 'RMSId' + '\r\n' + 'ApplicationId' + '\r\n' + 'RecuiterName' + '\r\n' ; 
        PageReference pageRef = Page.WCT_UploadCases;
        Test.setCurrentPageReference(pageRef);
        WCT_UploadCases uploadCasesCls=new WCT_UploadCases();
        uploadCasesCls.contentFile=blob.valueof(blobCreator);
        uploadCasesCls.readFile();
        List<Apexpages.Message> msgs = ApexPages.getMessages(); 
        boolean b = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains(Label.Columns_count_not_proper)) b = true;
        }
        system.assert(b);
    }
    
    /** 
        Method Name  : fileNotValid
        Return Type  : void
        Type         : Test Method
        Description  : Test will do when no file will be selected or file is not valid CSV.         
    */  
    static testMethod void fileNotValid() {
        refList=createPreBiReferenceRecords();
        contactList=createContacts();
        PageReference pageRef = Page.WCT_UploadCases;
        Test.setCurrentPageReference(pageRef);
        WCT_UploadCases uploadCasesCls=new WCT_UploadCases();
        uploadCasesCls.readFile();
        List<Apexpages.Message> msgs = ApexPages.getMessages(); 
        boolean b = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains(Label.Upload_Case_Exception)) b = true;
        }
        system.assert(b);
    }
    /** 
        Method Name  : readFileMethod
        Return Type  : void
        Type         : Test Method
        Description  : Test will do read file in all cases.         
    */  
    static testMethod void readFileMethod() {
        refList=createPreBiReferenceRecords();
        system.debug(refList);
        contactList=createContacts();
        String csvheader='Version,Applicant ID,RMS ID,Last Name,First Name,Email Address,Recruiter Name,Recruiter Email Address,'+
                        'Requisition #,Q1,Q2,Q3,Q4,Q5,Q6,Q7,Q8,Q9,Q10,Q11,Q12,Q13,Q14,Q15,Q16,Q17,Q18,Q19,Q20,Q21,Q22,Q23,Q24,Q25,Q26,Q27,Q28,Q29,Q30,Q31,Q32,Q33,Q34,'+
                        'Q35,Q36,Q37,Q38,Q39,Q40,Q41,Q42,Q43,Q44,Q45,Q46,Q47,Q48,Q49,Q50,Q51,Q52,Q53,Q54,Q55,Q56,Q57,Q58,Q59,Q60,Q61,Q62,Q63,Q64,Q65,Q66,Q67,Q68,Q69,Q70,'+
                        'Q71,Q72,Q73,Q74,Q75,Q76,Q77,Q78,Q79,Q80,Q81,Q82,Q83,Q84,Q85,Q86,Q87,Q88,Q89,Q90,Q91,Q92,Q93,Q94,Q95,Q96,Q97,Q98,Q99,Q100';
        String dataRow='1,1,1234551,Smith,Joe,Email1@deloitte.com,Test user,cmackay@deloitte.ca,123456,Yes,I am not interested.,Yes,Unknown,A5,A6,A7,A8,A9,A10,A11,A12,A13,'+
                        'A14,A15,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,A26,A27,A28,A29,A30,A31,A32,A33,A34,A35,A36,A37,A38,A39,A40,A41,A42,A43,A44,A45,A46,A47,A48,A49'+
                        ',A50,A51,A52,A53,A54,A55,A56,A57,A58,A59,A60,A61,A62,A63,A64,A65,A66,A67,A68,A69,A70,A71,A72,A73,A74,A75,A76,A77,A78,A79,A80,A81,A82,A83,A84,A85,'+
                        'A86,A87,A88,A89,A90,A91,A92,A93,A94,A95,A96,A97,A98,A99,A100';
        String blobCreator = csvheader + '\r\n' + dataRow ; 
        PageReference pageRef = Page.WCT_UploadCases;
        Test.setCurrentPageReference(pageRef);
        WCT_UploadCases uploadCasesCls=new WCT_UploadCases();
        uploadCasesCls.contentFile=blob.valueof(blobCreator);
        uploadCasesCls.readFile();
        List<WCT_PreBIStageTable__c> preBIStageRecordList=uploadCasesCls.getAllStagingRecords();
        //System.assertEquals(0, preBIStageRecordList.Size());
        
    }
    /** 
        Method Name  : readFileMethodWithAllData
        Return Type  : void
        Type         : Test Method
        Description  : Test will do read file in all cases.         
    */  
    static testMethod void readFileMethodWithAllData() {
        refList=createPreBiReferenceRecords();
        contactList=createContacts();
        userRec=createUser();
        req=createRequistion();
        String csvheader='Version,Applicant ID,RMS ID,Last Name,First Name,Email Address,Recruiter Name,Recruiter Email Address,'+
                        'Requisition #,Q1,Q2,Q3,Q4,Q5,Q6,Q7,Q8,Q9,Q10,Q11,Q12,Q13,Q14,Q15,Q16,Q17,Q18,Q19,Q20,Q21,Q22,Q23,Q24,Q25,Q26,Q27,Q28,Q29,Q30,Q31,Q32,Q33,Q34,'+
                        'Q35,Q36,Q37,Q38,Q39,Q40,Q41,Q42,Q43,Q44,Q45,Q46,Q47,Q48,Q49,Q50,Q51,Q52,Q53,Q54,Q55,Q56,Q57,Q58,Q59,Q60,Q61,Q62,Q63,Q64,Q65,Q66,Q67,Q68,Q69,Q70,'+
                        'Q71,Q72,Q73,Q74,Q75,Q76,Q77,Q78,Q79,Q80,Q81,Q82,Q83,Q84,Q85,Q86,Q87,Q88,Q89,Q90,Q91,Q92,Q93,Q94,Q95,Q96,Q97,Q98,Q99,Q100';
        String dataRow='1,1,1234551,Smith,Joe,Email1@deloitte.com,Test user,vinBhatiaPreBI@deloitte.com,12345,Yes,I am not interested.,Yes,Unknown,A5,A6,A7,A8,A9,A10,A11,A12,A13,'+
                        'A14,A15,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,A26,A27,A28,A29,A30,A31,A32,A33,A34,A35,A36,A37,A38,A39,A40,A41,A42,A43,A44,A45,A46,A47,A48,A49'+
                        ',A50,A51,A52,A53,A54,A55,A56,A57,A58,A59,A60,A61,A62,A63,A64,A65,A66,A67,A68,A69,A70,A71,A72,A73,A74,A75,A76,A77,A78,A79,A80,A81,A82,A83,A84,A85,'+
                        'A86,A87,A88,A89,A90,A91,A92,A93,A94,A95,A96,A97,A98,A99,A100';
        String blobCreator = csvheader + '\r\n' + dataRow ; 
        System.runAs(userRec)
        {
            permissionSetAssignment(userRec.id);
            PageReference pageRef = Page.WCT_UploadCases;
            Test.setCurrentPageReference(pageRef);
            WCT_UploadCases uploadCasesCls=new WCT_UploadCases();
            uploadCasesCls.contentFile=blob.valueof(blobCreator);
            uploadCasesCls.readFile();
            List<WCT_PreBIStageTable__c> preBIStageRecordList=uploadCasesCls.getAllStagingRecords();
            System.assertEquals(0, preBIStageRecordList.Size());
        }   
    }
    /** 
        Method Name  : readDataException
        Return Type  : void
        Type         : Test Method
        Description  : Test will do read file in all cases with Data exception.         
    */  
    static testMethod void readDataException() {
        Test.startTest();
        userRec=createUser();
        refList=createPreBiReferenceRecords();
        contactList=createContacts();
        req=createRequistion();
        String csvheader='Version,Applicant ID,RMS ID,Last Name,First Name,Email Address,Recruiter Name,Recruiter Email Address,'+
                        'Requisition #,Q1,Q2,Q3,Q4,Q5,Q6,Q7,Q8,Q9,Q10,Q11,Q12,Q13,Q14,Q15,Q16,Q17,Q18,Q19,Q20,Q21,Q22,Q23,Q24,Q25,Q26,Q27,Q28,Q29,Q30,Q31,Q32,Q33,Q34,'+
                        'Q35,Q36,Q37,Q38,Q39,Q40,Q41,Q42,Q43,Q44,Q45,Q46,Q47,Q48,Q49,Q50,Q51,Q52,Q53,Q54,Q55,Q56,Q57,Q58,Q59,Q60,Q61,Q62,Q63,Q64,Q65,Q66,Q67,Q68,Q69,Q70,'+
                        'Q71,Q72,Q73,Q74,Q75,Q76,Q77,Q78,Q79,Q80,Q81,Q82,Q83,Q84,Q85,Q86,Q87,Q88,Q89,Q90,Q91,Q92,Q93,Q94,Q95,Q96,Q97,Q98,Q99,Q100';
        String dataRow='1,1,1234567890123456789011,Smith,Joe,Email1@deloitte.com,Test user,vinBhatiaPreBI@deloitte.com,12345,Yes,I am not interested.,Yes,Unknown,A5,A6,A7,A8,A9,A10,A11,A12,A13,'+
                        'A14,A15,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,A26,A27,A28,A29,A30,A31,A32,A33,A34,A35,A36,A37,A38,A39,A40,A41,A42,A43,A44,A45,A46,A47,A48,A49'+
                        ',A50,A51,A52,A53,A54,A55,A56,A57,A58,A59,A60,A61,A62,A63,A64,A65,A66,A67,A68,A69,A70,A71,A72,A73,A74,A75,A76,A77,A78,A79,A80,A81,A82,A83,A84,A85,'+
                        'A86,A87,A88,A89,A90,A91,A92,A93,A94,A95,A96,A97,A98,A99,A100';
        String blobCreator = csvheader + '\r\n' + dataRow ; 
        System.runAs(userRec)
        {
            permissionSetAssignment(userRec.id);
            PageReference pageRef = Page.WCT_UploadCases;
            Test.setCurrentPageReference(pageRef);
            WCT_UploadCases uploadCasesCls=new WCT_UploadCases();
            uploadCasesCls.contentFile=blob.valueof(blobCreator);
            uploadCasesCls.readFile();
            List<WCT_PreBIStageTable__c> preBIStageRecordList=uploadCasesCls.getAllStagingRecords();
            
            //System.assertEquals(0, preBIStageRecordList.Size());
        }   
        Test.stopTest();
    }
}