public class WCT_UploadOutreachActivity{

    public List<WCT_Outreach_Activity_Staging_Table__c> stagingRecordsList;
    public Blob contentFile{get;set;}
    public Integer noOfRows{get;set;}
    public Integer noOfRowsProcessed{get;set;}
    public Set<Id> stagingIds=new Set<Id>();     // Store all StagingIds after reading from file.
    public String nameFile{get;set;}    

    ID outreachId=ApexPages.currentPage().getParameters().get('id');     
    public List<List<String>> fileLines = new List<List<String>>();
    Map<String,Integer> questionColOrderMap=new Map<String,Integer>();

    WCT_parseCSV parseCSVInstance = new WCT_parseCSV();
    Map<String,id> outreachMap= new Map<String,id>();   
    Public Set<String> DuplicateEmails = new Set<String>();
    Public String DuplicateErrorMessage=''; 
        
     /** 
        Method Name  : readFile
        Return Type  : PageReference
        Description  : Read CSV and push records into staging table        
    */
    
    public Pagereference readFile()
    {
                
        noOfRowsProcessed=0;
        stagingRecordsList = new List<WCT_Outreach_Activity_Staging_Table__c>(); 
         if(!stagingIds.isEmpty())
         {
          stagingIds.Clear();
         }
              
                 
        try{
            stagingRecordsList= mapStagingRecords();
                 
            stagingRecordsList=validateStagingRecords(stagingRecordsList);           
           
        }
        Catch(System.StringException stringException)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_String_Exception_Error);
            ApexPages.addMessage(errormsg);    
        }
        Catch(System.ListException listException)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Columns_count_not_proper);
            ApexPages.addMessage(errormsg); 
        
        }
        
        Catch(Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_Exception);
            ApexPages.addMessage(errormsg); 
            system.debug('Exception-----'+e);  
        
        }
        if(stagingRecordsList.size()>Limits.getLimitQueryRows())
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Error_Data_File_too_large + Limits.getLimitQueryRows());
            ApexPages.addMessage(errormsg);   
        }
        Database.SaveResult[] srList = Database.insert(stagingRecordsList, false);
        for(Database.SaveResult sr: srList)
        {
            if(sr.IsSuccess())
            {   
                stagingIds.add(sr.getId());
                noOfRowsProcessed=stagingIds.size();
            }
            else if(!sr.IsSuccess())
            {
                for(Database.Error err : sr.getErrors())
                {
                     ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.WCT_Data_Error+err.getMessage());
                     ApexPages.addMessage(errormsg);       
                }
            }
        }   
        if(!stagingIds.IsEmpty())
        {
        
            try{
                CreateOutreachActivity(stagingIds);
                if(!DuplicateEmails.isEmpty())
                {
                 ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,DuplicateErrorMessage);
                ApexPages.addMessage(errormsg);
                }
            }Catch(Exception e)
            {
                throw e;
            }
            
        }
          
        return null;
    }
    
    /** 
        Method Name  : mapStagingRecords
        Return Type  : List<WCT_Outreach_Activity_Staging_Table__c>
        Description  : Map fields names with CSV col names         
    */
    private List<WCT_Outreach_Activity_Staging_Table__c> mapStagingRecords()
    {
        nameFile=contentFile.toString();
        
        filelines = parseCSVInstance.parseCSV(nameFile, true);
        system.debug('--@--'+filelines);
        List<WCT_Outreach_Activity_Staging_Table__c> stagingRecordsList=new List<WCT_Outreach_Activity_Staging_Table__c>();
        noOfRows=fileLines.size();
        system.debug('--#--'+filelines);
       
        Set <string> setAllEmails=new set<string>();
        for(List<String> inputValues:filelines) 
        {   
            Boolean createStg=false;
            Boolean caseFlag=false;
            Map<String,Boolean> flagCompare=new Map<String,Boolean>();
            system.debug(inputValues.size()+'stringSize');
            Boolean tempFlag=false;
            if(!setAllEmails.contains(inputValues[1])){
            setAllEmails.add(inputValues[1]);
            createStg=true;}
            else
            {
            DuplicateEmails.add(inputValues[1]);
            }
            if(createStg){
            WCT_Outreach_Activity_Staging_Table__c stagingRecord = new WCT_Outreach_Activity_Staging_Table__c();
            stagingRecord.WCT_Associated_Outreach__c=outreachId;            
            //stagingRecord.WCT_First_Name__c=inputValues[1];
            //stagingRecord.WCT_Last_Name__c=inputValues[2];
            //stagingRecord.WCT_Email__c=inputValues[3];
            //stagingRecord.WCT_Taleo_ID__c=inputValues[4];
            //stagingRecord.WCT_Personnel_Number__c=Integer.ValueOf(inputValues[4]);
            /*stagingRecord.WCT_Phone__c=Integer.ValueOf(inputValues[6]);
            stagingRecord.WCT_Mobile__c=Integer.ValueOf(inputValues[7]);*/
            //stagingRecord.WCT_Status__c=inputValues[8];

            if(inputValues.size()>0){stagingRecord.WCT_Personnel_Number__c=inputValues[0];}
            if(inputValues.size()>1){stagingRecord.WCT_Outreach_Contact_Email__c=inputValues[1];}
            //stagingRecord.WCT_Medical_Summary_for_Office_Location__c=inputValues[2];
            if(inputValues.size()>2){stagingRecord.WCT_Admission_Date__c=inputValues[2];}
            if(inputValues.size()>3){stagingRecord.WCT_Date_Mailed__c=inputValues[3];}
            if(inputValues.size()>4){stagingRecord.WCT_Date_Forms_Due__c=inputValues[4];}
            if(inputValues.size()>5){stagingRecord.WCT_Insert_Carriers__c=inputValues[5];}
            if(inputValues.size()>6){stagingRecord.WCT_Insert_Address_in_SAP__c=inputValues[6];}
            if(inputValues.size()>7){stagingRecord.WCT_Insert_Street_Address_Line2_in_SAP__c=inputValues[7];}
            if(inputValues.size()>8){stagingRecord.WCT_Insert_City_in_SAP__c=inputValues[8];}
            if(inputValues.size()>9){stagingRecord.WCT_Insert_State_in_SAP__c=inputValues[9];}
            if(inputValues.size()>10){stagingRecord.WCT_Insert_Zip_Postal_Code_in_SAP__c=inputValues[10];}
            if(inputValues.size()>11){stagingRecord.WCT_Insert_Date__c=inputValues[11];}
            if(inputValues.size()>12){stagingRecord.WCT_End_Date__c=inputValues[12];}
            if(inputValues.size()>13){stagingRecord.WCT_Effective_Date__c=inputValues[13];}
            if(inputValues.size()>14){stagingRecord.WCT_Transition_Date__c=inputValues[14];}
            if(inputValues.size()>15){stagingRecord.WCT_Payroll_Date__c=inputValues[15];}
            if(inputValues.size()>16){stagingRecord.WCT_Dependent_First_Name__c=inputValues[16];}
            if(inputValues.size()>17){stagingRecord.WCT_Dependent_Last_Name__c=inputValues[17];}
            if(inputValues.size()>18){stagingRecord.WCT_Enrollment_Date__c=inputValues[18];}
            if(inputValues.size()>19){stagingRecord.WCT_Enrollment_Deadline__c=inputValues[19];}
            if(inputValues.size()>20){stagingRecord.WCT_Analyst_Email__c=inputValues[20];}
            if(inputValues.size()>21){stagingRecord.WCT_Analyst_Name__c=inputValues[21];}
            if(inputValues.size()>22){stagingRecord.WCT_Analyst_Phone__c=inputValues[22];}
            if(inputValues.size()>23){stagingRecord.WCT_Deduction_Amount__c=inputValues[23];}
            if(inputValues.size()>24){stagingRecord.WCT_Deduction_Type__c=inputValues[24];}
            if(inputValues.size()>25){stagingRecord.WCT_Coverage_Level__c=inputValues[25];}
            if(inputValues.size()>26){stagingRecord.WCT_Coverage_Amount__c=inputValues[26];}
            if(inputValues.size()>27){stagingRecord.WCT_Coverage_Name__c=inputValues[27];}
            if(inputValues.size()>28){stagingRecord.WCT_Year_to_Date_Amount__c=inputValues[28];}
            

            stagingRecord.WCT_status__c=WCT_UtilConstants.STAGING_STATUS_NOT_STARTED;            
            
            //stagingRecord.WCT_Service_Line__c=WCT_UtilConstants.STAGING_STATUS_NOT_STARTED;
            /*if(questionColOrderMap.ContainsKey(WCT_UtilConstants.QUESTION_1) && inputValues.size()>(questionColOrderMap.get(WCT_UtilConstants.QUESTION_1))-1)
            {
                stagingRecord.WCT_A1__c=inputValues[(questionColOrderMap.get(WCT_UtilConstants.QUESTION_1))-1];
                tempFlag=findAnswer(WCT_UtilConstants.QUESTION_1,stagingRecord.WCT_A1__c);
                flagCompare.put(WCT_UtilConstants.QUESTION_1,tempFlag);
            }*/
 
            integer i=1;
            for(Boolean caseTempFlag:flagCompare.values())
            {
              system.debug(caseTempFlag+'eeee'+i);
                if(caseTempFlag)
                {
                    caseFlag=true;
                }
                i=i+1;
            }
            //caseFlag=checkCaseCreationFlag(stagingRecord);
            //stagingRecord.WCT_Case_type__c=caseFlag;
            if(!caseFlag)
            {
                stagingRecord.WCT_Status__c=WCT_UtilConstants.STAGING_STATUS_COMPLETED;
            }
            stagingRecordsList.add(stagingRecord);
            }
        }               
        if(!DuplicateEmails.isEmpty())
         {
           for(string s : DuplicateEmails)
            {
                if(DuplicateErrorMessage=='')
                DuplicateErrorMessage = 'Duplicate entries found for the following emails have not been inserted:'+s;
                else{
                DuplicateErrorMessage=DuplicateErrorMessage+','+s;
                }
            }   
          }
         
        return stagingRecordsList;
    } 
    
     /** 
        Method Name  : getAllStagingRecords
        Return Type  : List<WCT_Outreach_Activity_Staging_Table__c>
        Description  : Show all records into VF        
    */
    public List<WCT_Outreach_Activity_Staging_Table__c> getAllStagingRecords()
    {
        if (stagingIds!= NULL)
            if (stagingIds.size() > 0)
            {
                return [Select Id,Name, WCT_Status__c,WCT_First_Name__c,WCT_Last_Name__c,WCT_Outreach_Contact_Email__c,WCT_Personnel_Number__c,WCT_Error_Message__c from WCT_Outreach_Activity_Staging_Table__c where Id In:stagingIds and WCT_Status__c=:WCT_UtilConstants.STAGING_STATUS_ERROR];
                
            }
            else
                return null;                   
        else
            return null;
    }   
    Public Static List<WCT_Outreach_Activity_Staging_Table__c> validateStagingRecords(List<WCT_Outreach_Activity_Staging_Table__c> stagingRecordsList) {
           ID employeeRecordtypeID=WCT_Util.getRecordTypeIdByLabel('Contact','Employee');
           set<string> empEmailStagingTable = new set<string>();
           set<id> ExistingOutreach_set = new set<id>();
           Map<String,id> contactMap = new Map<String,id>(); 
           Map<String,id> contactMapPerNo = new Map<String,id>(); 
           map<Id,String> existingContactandOutReachMap=new map<Id,String>();
           
           for(WCT_Outreach_Activity_Staging_Table__c oastRec : stagingRecordsList)
            {
            
            ExistingOutreach_set.add(oastRec.WCT_Associated_Outreach__c);
            empEmailStagingTable.add(oastRec.WCT_Outreach_Contact_Email__c);
            
            }
            
           for(contact c : [Select id, Email, WCT_Person_Id__c from contact where recordtypeid =: employeeRecordtypeID and email in :empEmailStagingTable] )
            {
                contactMap.put(c.email,c.Id);
                contactMapPerNo.put(c.WCT_Person_Id__c,c.Id);
            }            

            Map<Id,WCT_Outreach__c> MapOutReach=new Map<Id,WCT_Outreach__c>([Select id,Name,WCT_Subject__c from WCT_Outreach__c where id in:ExistingOutreach_set ]);
            for(WCT_Outreach_Activity__c newOutReachActivity: [select WCT_Outreach__c,WCT_Contact__c from WCT_Outreach_Activity__c where WCT_Outreach__c in :
            ExistingOutreach_set])
            {        
            if(!existingContactandOutReachMap.containsKey(newOutReachActivity.WCT_Outreach__c))
                    existingContactandOutReachMap.put(newOutReachActivity.WCT_Outreach__c,newOutReachActivity.WCT_Contact__c);
                else
                    existingContactandOutReachMap.put(newOutReachActivity.WCT_Outreach__c,existingContactandOutReachMap.get(newOutReachActivity.WCT_Outreach__c)+','+newOutReachActivity.WCT_Contact__c);
            
            }   
            system.debug('@1@'+contactMap.Size()); 
            system.debug('%1%'+contactMap);
              for(WCT_Outreach_Activity_Staging_Table__c oast:stagingRecordsList)
            
             {
                
                system.debug('#1#'+oast.WCT_Outreach_Contact_Email__c);
                  if(contactMapPerNo.containsKey(oast.WCT_Personnel_Number__c)){          
                     oast.WCT_Employee__c = contactMap.get(oast.WCT_Personnel_Number__c);
                     oast.WCT_Status__c=WCT_UtilConstants.STAGING_STATUS_COMPLETED; 
                     oast.WCT_Error_Message__c = '';  
                     
                  }
                  if(contactMap.containsKey(oast.WCT_Outreach_Contact_Email__c)){  
                    system.debug('#2#'+oast.WCT_Outreach_Contact_Email__c);        
                     oast.WCT_Employee__c = contactMap.get(oast.WCT_Outreach_Contact_Email__c);
                     system.debug('#3#'+oast.WCT_Employee__c);
                     oast.WCT_Status__c=WCT_UtilConstants.STAGING_STATUS_COMPLETED; 
                     oast.WCT_Error_Message__c = ''; 
                      
                  }
                 else
                  {
                  oast.WCT_Status__c= WCT_UtilConstants.STAGING_STATUS_ERROR;
                  oast.WCT_Error_Message__c = 'No employee found for the given E-mail ID to associate with'+MapOutReach.get(oast.WCT_Associated_Outreach__c).Name;
                  continue;  
                  }
                  if(!existingContactandOutReachMap.isempty()&& (existingContactandOutReachMap.containskey(oast.WCT_Associated_Outreach__c)) ){
                  if(existingContactandOutReachMap.get(oast.WCT_Associated_Outreach__c).contains(contactMap.get(oast.WCT_Outreach_Contact_Email__c)) ){
                        oast.WCT_Status__c= WCT_UtilConstants.STAGING_STATUS_ERROR;
                        //oast.WCT_Error_Message__c = 'For the current outreach ' + oast.WCT_Associated_Outreach__c.Name + ' Subject : '+ oast.WCT_Associated_Outreach__c.WCT_Subject__c + ', employee is already associated with an Outreach Activity';
                        oast.WCT_Error_Message__c = 'For the current outreach'+MapOutReach.get(oast.WCT_Associated_Outreach__c).Name+' Subject : '+MapOutReach.get(oast.WCT_Associated_Outreach__c).WCT_Subject__c+', employee is already associated with an Outreach Activity'; 
                         }  
                  }   
             } 
             return stagingRecordsList;    
    }        
      
    Public Static Void CreateOutreachActivity(Set<Id> stagingIds){
        List<WCT_Outreach_Activity__c> oaList = new List<WCT_Outreach_Activity__c>();

        for(WCT_Outreach_Activity_Staging_Table__c oast : [Select Id,Name, WCT_Associated_Outreach__c,WCT_Status__c,WCT_Employee__c, 
                                                            WCT_Personnel_Number__c, WCT_Mobile__c, WCT_Phone__c, WCT_Medical_Summary_for_Office_Location__c, WCT_Admission_Date__c,
                                                            WCT_Insert_Zip_Postal_Code_in_SAP__c, WCT_Insert_Carriers__c, WCT_Insert_Address_in_SAP__c, WCT_Insert_City_in_SAP__c, 
                                                            WCT_Insert_State_in_SAP__c, WCT_Insert_Date__c, WCT_Effective_Date__c, WCT_Transition_Date__c, WCT_Payroll_Date__c,
                                                            WCT_Dependent_First_Name__c, WCT_Dependent_Last_Name__c, WCT_Enrollment_Date__c, WCT_Analyst_Name__c,
                                                            WCT_Analyst_Phone__c, WCT_Deduction_Amount__c, WCT_Deduction_Type__c, WCT_Coverage_Level__c, WCT_Coverage_Name__c,
                                                            WCT_Year_to_Date_Amount__c, WCT_Coverage_Amount__c, WCT_Date_Mailed__c, WCT_Date_Forms_Due__c,WCT_Insert_Street_Address_Line2_in_SAP__c,
                                                            WCT_End_Date__c, WCT_Analyst_Email__c, WCT_Enrollment_Deadline__c
                                                            from WCT_Outreach_Activity_Staging_Table__c where Id In:stagingIds and (WCT_Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED OR WCT_Status__c='Corrected')
                                                            ]) 
     {
        WCT_Outreach_Activity__c oa= new WCT_Outreach_Activity__c();
        oa.WCT_Outreach__c = oast.WCT_Associated_Outreach__c;
        oa.WCT_Contact__c = oast.WCT_Employee__c;
        //oa.WCT_Medical_Summary_for_Office_Location__c=oast.WCT_Medical_Summary_for_Office_Location__c;
        oa.WCT_Admission_Date__c=oast.WCT_Admission_Date__c;
        oa.WCT_Insert_Carriers__c=oast.WCT_Insert_Carriers__c;
        oa.WCT_Insert_Address_in_SAP__c=oast.WCT_Insert_Address_in_SAP__c;
        oa.WCT_Insert_City_in_SAP__c=oast.WCT_Insert_City_in_SAP__c;
        oa.WCT_Insert_State_in_SAP__c=oast.WCT_Insert_State_in_SAP__c;
        oa.WCT_Insert_Zip_Postal_Code_in_SAP__c=oast.WCT_Insert_Zip_Postal_Code_in_SAP__c;
        oa.WCT_Insert_Date__c=oast.WCT_Insert_Date__c;
        oa.WCT_Effective_Date__c=oast.WCT_Effective_Date__c;
        oa.WCT_Transition_Date__c=oast.WCT_Transition_Date__c;
        oa.WCT_Payroll_Date__c=oast.WCT_Payroll_Date__c;
        oa.WCT_Dependent_First_Name__c=oast.WCT_Dependent_First_Name__c;
        oa.WCT_Dependent_Last_Name__c=oast.WCT_Dependent_Last_Name__c;
        oa.WCT_Enrollment_Date__c=oast.WCT_Enrollment_Date__c;
        oa.WCT_Analyst_Name__c=oast.WCT_Analyst_Name__c;
        oa.WCT_Analyst_Phone__c=oast.WCT_Analyst_Phone__c;
        oa.WCT_Deduction_Amount__c=oast.WCT_Deduction_Amount__c;
        oa.WCT_Deduction_Type__c=oast.WCT_Deduction_Type__c;
        oa.WCT_Coverage_Level__c=oast.WCT_Coverage_Level__c;
        oa.WCT_Coverage_Name__c=oast.WCT_Coverage_Name__c;
        oa.WCT_Year_to_Date_Amount__c=oast.WCT_Year_to_Date_Amount__c;
        oa.WCT_Coverage_Amount__c=oast.WCT_Coverage_Amount__c;
        oa.WCT_Date_Mailed__c=oast.WCT_Date_Mailed__c;
        oa.WCT_Date_Forms_Due__c=oast.WCT_Date_Forms_Due__c;
        oa.WCT_Insert_Street_Address_Line2_in_SAP__c=oast.WCT_Insert_Street_Address_Line2_in_SAP__c;
        oa.WCT_End_Date__c=oast.WCT_End_Date__c;
        oa.WCT_Enrollment_Deadline__c=oast.WCT_Enrollment_Deadline__c;
        oa.WCT_Analyst_Email__c=oast.WCT_Analyst_Email__c;
        oalist.add(oa);
      }
      try{
       //system.debug('#########' + oalist);     
      insert oalist;  
       }
        Catch(Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,'Insertion of Outreach Activities failed');
            ApexPages.addMessage(errormsg); 
        }              
      } 
       public Pagereference Close()
        {
         PageReference pageRef = new PageReference('/' + OutreachId);
         pageRef.setRedirect(true); 
         return pageref;      
        }         
}