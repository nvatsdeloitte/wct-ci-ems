@isTest
public class WCT_UploadOutreachActivity_Test
{
    public static testmethod void testmethod1()
    {
        Test.StartTest();
        PageReference pageRef = Page.WCT_Bulk_Upload_Contacts;
        Test.setCurrentPage(pageRef); 
       
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='testoast@deloitte.com';
        con.WCT_Person_Id__c='104032';
        insert con;
        WCT_Outreach__c Outreach = WCT_UtilTestDataCreation.CreateOutreach();
        insert Outreach;
        ApexPages.CurrentPage().getParameters().put('id',Outreach.id);
        WCT_UploadOutreachActivity controller = new WCT_UploadOutreachActivity();
        //controller.outreachId=Outreach.Id;
        controller.contentFile=Blob.valueof('"WCT_Personnel_Number__c,WCT_Outreach_Contact_Email__c,WCT_Admission_Date__c,WCT_Date_Mailed__c,WCT_Date_Forms_Due__c,WCT_Insert_Carriers__c,WCT_Insert_Address_in_SAP__c,WCT_Insert_Street_Address_Line2_in_SAP__c,WCT_Insert_City_in_SAP__c,WCT_Insert_State_in_SAP__c,WCT_Insert_Zip_Postal_Code_in_SAP__c,WCT_Insert_Date__c,WCT_End_Date__c,WCT_Effective_Date__c,WCT_Transition_Date__c,WCT_Payroll_Date__c,WCT_Dependent_First_Name__c,WCT_Dependent_Last_Name__c,WCT_Enrollment_Date__c,WCT_Enrollment_Deadline__c,WCT_Analyst_Email__c,WCT_Analyst_Name__c,WCT_Analyst_Phone__c,WCT_Deduction_Amount__c,WCT_Deduction_Type__c,WCT_Coverage_Level__c,WCT_Coverage_Amount__c,WCT_Coverage_Name__c,WCT_Year_to_Date_Amount__c\r\n104032,testoast@deloitte.com,Admission Date 01/01,Date Mailed 01,Date Form Due 01,Insert Carrier 1,Insert Address 1,Insert Address 18,Insert City 1,Insert State 1,Insert Zip 1,Insert Date 01/01,End Date 01,Effective Date 01/01,Transaction Date 01/01,Payroll Date 01/01,Dep First Name 1,Dep Last Name 1,Enrollment Date 1,Enrollment Deadline 1,Analyst Email 1,Analyst Name 1,Analyst Phone 1,Ded Amount $100,Ded Type 1,Cov Level 1,Cov Amount $100,Cov Name 1,YTD Amount $100"');
        Controller.readFile();
        List<WCT_Outreach_Activity_Staging_Table__c> OutreachStageRecordList=Controller.getAllStagingRecords();
        //System.assertEquals(1, OutreachStageRecordList.Size());
        Test.StopTest();
     }
      public static testmethod void testmethod2()
    {
        Test.StartTest();
        PageReference pageRef = Page.WCT_Bulk_Upload_Contacts;
        Test.setCurrentPage(pageRef); 
        //ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_UploadOutreachActivity controller = new WCT_UploadOutreachActivity();
        controller.contentFile=Blob.valueof('"WCT_Personnel_Number__c,WCT_Outreach_Contact_Email__c\r\n104032,deeppatel@deloitte.com"');
        Controller.readFile();
        List<WCT_Outreach_Activity_Staging_Table__c> OutreachStageRecordList=Controller.getAllStagingRecords();
        Test.StopTest();
     }
      public static testmethod void testmethod3()
    {
        Test.StartTest();
        PageReference pageRef = Page.WCT_Bulk_Upload_Contacts;
        Test.setCurrentPage(pageRef); 
        //ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_UploadOutreachActivity controller = new WCT_UploadOutreachActivity();
        Controller.readFile();
        Test.StopTest();
     }
     public static testmethod void testmethod4()
    {
        Test.StartTest();
        PageReference pageRef = Page.WCT_Bulk_Upload_Contacts;
        Test.setCurrentPage(pageRef); 
        //ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_Outreach__c Outreach = WCT_UtilTestDataCreation.CreateOutreach();
        insert Outreach;
        ApexPages.CurrentPage().getParameters().put('id',Outreach.id);
        WCT_UploadOutreachActivity controller = new WCT_UploadOutreachActivity();
        Controller.Close();
        Test.StopTest();
     }
       public static testmethod void testmethod5()
    {
        Test.StartTest();
        PageReference pageRef = Page.WCT_Bulk_Upload_Contacts;
        Test.setCurrentPage(pageRef);
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='testoast@deloitte.com';
        con.WCT_Person_Id__c='104032';
        insert con; 
        WCT_Outreach__c Outreach = WCT_UtilTestDataCreation.CreateOutreach();
        insert Outreach;
        WCT_Outreach_Activity__c oa = WCT_UtilTestDataCreation.createOA();
        oa.WCT_Contact__c=con.id;
        oa.WCT_Outreach__c=Outreach.id;
        insert oa;
        ApexPages.CurrentPage().getParameters().put('id',Outreach.id);
        WCT_UploadOutreachActivity controller = new WCT_UploadOutreachActivity();
        //controller.outreachId=Outreach.Id;
        controller.contentFile=Blob.valueof('"WCT_Personnel_Number__c,WCT_Outreach_Contact_Email__c,WCT_Admission_Date__c,WCT_Date_Mailed__c,WCT_Date_Forms_Due__c,WCT_Insert_Carriers__c,WCT_Insert_Address_in_SAP__c,WCT_Insert_Street_Address_Line2_in_SAP__c,WCT_Insert_City_in_SAP__c,WCT_Insert_State_in_SAP__c,WCT_Insert_Zip_Postal_Code_in_SAP__c,WCT_Insert_Date__c,WCT_End_Date__c,WCT_Effective_Date__c,WCT_Transition_Date__c,WCT_Payroll_Date__c,WCT_Dependent_First_Name__c,WCT_Dependent_Last_Name__c,WCT_Enrollment_Date__c,WCT_Enrollment_Deadline__c,WCT_Analyst_Email__c,WCT_Analyst_Name__c,WCT_Analyst_Phone__c,WCT_Deduction_Amount__c,WCT_Deduction_Type__c,WCT_Coverage_Level__c,WCT_Coverage_Amount__c,WCT_Coverage_Name__c,WCT_Year_to_Date_Amount__c\r\n104032,testoast@deloitte.com,Admission Date 01/01,Date Mailed 01,Date Form Due 01,Insert Carrier 1,Insert Address 1,Insert Address 18,Insert City 1,Insert State 1,Insert Zip 1,Insert Date 01/01,End Date 01,Effective Date 01/01,Transaction Date 01/01,Payroll Date 01/01,Dep First Name 1,Dep Last Name 1,Enrollment Date 1,Enrollment Deadline 1,Analyst Email 1,Analyst Name 1,Analyst Phone 1,Ded Amount $100,Ded Type 1,Cov Level 1,Cov Amount $100,Cov Name 1,YTD Amount $100"');
        Controller.readFile();
        List<WCT_Outreach_Activity_Staging_Table__c> OutreachStageRecordList=Controller.getAllStagingRecords();
        //System.assertEquals(1, OutreachStageRecordList.Size());
        Test.StopTest();
     }
     public static testmethod void testmethod6()
    {
        Test.StartTest();
        PageReference pageRef = Page.WCT_Bulk_Upload_Contacts;
        Test.setCurrentPage(pageRef); 
       
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='testoast@deloitte.com';
        con.WCT_Person_Id__c='104032';
        insert con;
        WCT_Outreach__c Outreach = WCT_UtilTestDataCreation.CreateOutreach();
        insert Outreach;
        ApexPages.CurrentPage().getParameters().put('id',Outreach.id);
        WCT_UploadOutreachActivity controller = new WCT_UploadOutreachActivity();
        //controller.outreachId=Outreach.Id;
        controller.contentFile=Blob.valueof('"WCT_Personnel_Number__c,WCT_Outreach_Contact_Email__c,WCT_Admission_Date__c,WCT_Date_Mailed__c,WCT_Date_Forms_Due__c,WCT_Insert_Carriers__c,WCT_Insert_Address_in_SAP__c,WCT_Insert_Street_Address_Line2_in_SAP__c,WCT_Insert_City_in_SAP__c,WCT_Insert_State_in_SAP__c,WCT_Insert_Zip_Postal_Code_in_SAP__c,WCT_Insert_Date__c,WCT_End_Date__c,WCT_Effective_Date__c,WCT_Transition_Date__c,WCT_Payroll_Date__c,WCT_Dependent_First_Name__c,WCT_Dependent_Last_Name__c,WCT_Enrollment_Date__c,WCT_Enrollment_Deadline__c,WCT_Analyst_Email__c,WCT_Analyst_Name__c,WCT_Analyst_Phone__c,WCT_Deduction_Amount__c,WCT_Deduction_Type__c,WCT_Coverage_Level__c,WCT_Coverage_Amount__c,WCT_Coverage_Name__c,WCT_Year_to_Date_Amount__c\r\n 104034,testoast2@deloitte.com,Admission Date 01/01,Date Mailed 01,Date Form Due 01,Insert Carrier 1,Insert Address 1,Insert Address 18,Insert City 1,Insert State 1,Insert Zip 1,Insert Date 01/01,End Date 01,Effective Date 01/01,Transaction Date 01/01,Payroll Date 01/01,Dep First Name 1,Dep Last Name 1,Enrollment Date 1,Enrollment Deadline 1,Analyst Email 1,Analyst Name 1,Analyst Phone 1,Ded Amount $100,Ded Type 1,Cov Level 1,Cov Amount $100,Cov Name 1,YTD Amount $100""');
        Controller.readFile();
        List<WCT_Outreach_Activity_Staging_Table__c> OutreachStageRecordList=Controller.getAllStagingRecords();
        //System.assertEquals(1, OutreachStageRecordList.Size());
        Test.StopTest();
     }
    public static testmethod void testmethod7()
    {
        Test.StartTest();
        PageReference pageRef = Page.WCT_Bulk_Upload_Contacts;
        Test.setCurrentPage(pageRef); 
       
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='testoast@deloitte.com';
        con.WCT_Person_Id__c='104032';
        insert con;
        WCT_Outreach__c Outreach = WCT_UtilTestDataCreation.CreateOutreach();
        insert Outreach;
        ApexPages.CurrentPage().getParameters().put('id',Outreach.id);
        WCT_UploadOutreachActivity controller = new WCT_UploadOutreachActivity();
        //controller.outreachId=Outreach.Id;
        controller.contentFile=Blob.valueof('"WCT_Personnel_Number__c,WCT_Outreach_Contact_Email__c,WCT_Admission_Date__c,WCT_Date_Mailed__c,WCT_Date_Forms_Due__c,WCT_Insert_Carriers__c,WCT_Insert_Address_in_SAP__c,WCT_Insert_Street_Address_Line2_in_SAP__c,WCT_Insert_City_in_SAP__c,WCT_Insert_State_in_SAP__c,WCT_Insert_Zip_Postal_Code_in_SAP__c,WCT_Insert_Date__c,WCT_End_Date__c,WCT_Effective_Date__c,WCT_Transition_Date__c,WCT_Payroll_Date__c,WCT_Dependent_First_Name__c,WCT_Dependent_Last_Name__c,WCT_Enrollment_Date__c,WCT_Enrollment_Deadline__c,WCT_Analyst_Email__c,WCT_Analyst_Name__c,WCT_Analyst_Phone__c,WCT_Deduction_Amount__c,WCT_Deduction_Type__c,WCT_Coverage_Level__c,WCT_Coverage_Amount__c,WCT_Coverage_Name__c,WCT_Year_to_Date_Amount__c\r\n104033,testoast4444@deloitte.com,Admission Date 01/01,Date Mailed 01,Date Form Due 01,Insert Carrier 1,Insert Address 1,Insert Address 18,Insert City 1,Insert State 1,Insert Zip 1,Insert Date 01/01,End Date 01,Effective Date 01/01,Transaction Date 01/01,Payroll Date 01/01,Dep First Name 1,Dep Last Name 1,Enrollment Date 1,Enrollment Deadline 1,Analyst Email 1,Analyst Name 1,Analyst Phone 1,Ded Amount $100,Ded Type 1,Cov Level 1,Cov Amount $100,Cov Name 1,YTD Amount $100\r\n104032,testoast4444@deloitte.com,Admission Date 01/01,Date Mailed 01,Date Form Due 01,Insert Carrier 1,Insert Address 1,Insert Address 18,Insert City 1,Insert State 1,Insert Zip 1,Insert Date 01/01,End Date 01,Effective Date 01/01,Transaction Date 01/01,Payroll Date 01/01,Dep First Name 1,Dep Last Name 1,Enrollment Date 1,Enrollment Deadline 1,Analyst Email 1,Analyst Name 1,Analyst Phone 1,Ded Amount $100,Ded Type 1,Cov Level 1,Cov Amount $100,Cov Name 1,YTD Amount $100\r\n104035,testoast4444@deloitte.com,Admission Date 01/01,Date Mailed 01,Date Form Due 01,Insert Carrier 1,Insert Address 1,Insert Address 18,Insert City 1,Insert State 1,Insert Zip 1,Insert Date 01/01,End Date 01,Effective Date 01/01,Transaction Date 01/01,Payroll Date 01/01,Dep First Name 1,Dep Last Name 1,Enrollment Date 1,Enrollment Deadline 1,Analyst Email 1,Analyst Name 1,Analyst Phone 1,Ded Amount $100,Ded Type 1,Cov Level 1,Cov Amount $100,Cov Name 1,YTD Amount $100\r\n104040,testoast4445@deloitte.com,Admission Date 01/01,Date Mailed 01,Date Form Due 01,Insert Carrier 1,Insert Address 1,Insert Address 18,Insert City 1,Insert State 1,Insert Zip 1,Insert Date 01/01,End Date 01,Effective Date 01/01,Transaction Date 01/01,Payroll Date 01/01,Dep First Name 1,Dep Last Name 1,Enrollment Date 1,Enrollment Deadline 1,Analyst Email 1,Analyst Name 1,Analyst Phone 1,Ded Amount $100,Ded Type 1,Cov Level 1,Cov Amount $100,Cov Name 1,YTD Amount $100\r\n104041,testoast4445@deloitte.com,Admission Date 01/01,Date Mailed 01,Date Form Due 01,Insert Carrier 1,Insert Address 1,Insert Address 18,Insert City 1,Insert State 1,Insert Zip 1,Insert Date 01/01,End Date 01,Effective Date 01/01,Transaction Date 01/01,Payroll Date 01/01,Dep First Name 1,Dep Last Name 1,Enrollment Date 1,Enrollment Deadline 1,Analyst Email 1,Analyst Name 1,Analyst Phone 1,Ded Amount $100,Ded Type 1,Cov Level 1,Cov Amount $100,Cov Name 1,YTD Amount $100"');
        Controller.readFile();
        List<WCT_Outreach_Activity_Staging_Table__c> OutreachStageRecordList=Controller.getAllStagingRecords();
        //System.assertEquals(1, OutreachStageRecordList.Size());
        Test.StopTest();
     }
     
     
}