/**
    * Class Name  : WCT_Upload_Fragomen_Test
    * Description : This apex test class will use to test the WCT_Upload_Fragomen
*/

@isTest
private class WCT_Upload_Fragomen_Test{
    
    static testMethod void m1(){
        Id conRecType = [SELECT Id, Name FROM RecordType Where Name = 'Employee'].Id;
        Id immRecType = [SELECT Id, Name FROM RecordType Where Name = 'H1 Visa'].Id;
        Contact con = WCT_UtilTestDataCreation.createContact();
        con.Email = 'email1@tst.deloitte.com';
        con.RecordTypeId = conRecType;
        insert con;
        WCT_Immigration__c imm = WCT_UtilTestDataCreation.createImmigration(con.id);
        imm.WCT_Immigration_Status__c = 'Petition Collected';
        imm.WCT_Request_Initiation_Date__c = system.today();
        imm.RecordTypeId = immRecType;
        imm.WCT_Visa_Type__c = 'H1B Cap';
        insert imm;
        
    }    
    /** 
        Method Name  : readFileMethod
        Return Type  : void
        Type      : Test Method
        Description  : Test will do read file in all cases.         
    */         
    static testMethod void readFileMethod(){
        String csvheader = 'Identifier,Last Name,First Name,Email Address,Case Type,Category,L-1 Blanket or Individual,'+
            'H-1B Cap,Action [EOS, COS, COE, Amendment],Date Initation Sent to Fragomen,Initiator,Initiator Email Address,'+
            'Milestone,Milestone Date,RFE Due Date,Petition Start Date,Petition End Date,Personnel Number';
        String dataRow = '1,Ambrose,James,email1@tst.deloitte.com,H-1B,test,test,Cap,test,4/2/2015,Shivani,email2@tst.deloitte.com,'+
            'LCA filed,4/2/2014,4/2/2014,4/2/2014,4/2/2014,123456';
        String blobCreator = csvheader + '\r\n' + dataRow ;
        PageReference pageRef = Page.WCT_UploadImmigration;
        Test.setCurrentPageReference(pageRef);
        WCT_Fragomen_Validation_Controller uploadImmigrationsCls = new WCT_Fragomen_Validation_Controller();
        uploadImmigrationsCls.contentFile=blob.valueof(blobCreator);
        uploadImmigrationsCls.readFile();
    }
    
     /** 
        Method Name  : fileNotValid
        Return Type  : void
        Type      : Test Method
        Description  : Test will do when no file will be selected or file is not valid CSV.         
    */  
    static testMethod void fileNotValid() {
      PageReference pageRef = Page.WCT_Upload_Fragomen;
      Test.setCurrentPageReference(pageRef);
      WCT_Fragomen_Validation_Controller uploadImmigrationsCls = new WCT_Fragomen_Validation_Controller();
      uploadImmigrationsCls.readFile();
      List<Apexpages.Message> msgs = ApexPages.getMessages(); 
      boolean b = false;
      for(Apexpages.Message msg:msgs){
          if (msg.getDetail().contains(Label.Upload_Case_Exception)) b = true;
       }
       system.assert(b);
    }
    
     /** 
        Method Name  : noOfColsException
        Return Type  : void
        Type      : Test Method
        Description  : Test will do on number of column in CSV and send error message on page.         
    */  
    static testMethod void noOfColsException() {      
      String blobCreator = 'Identifier' + '\r\n' + 'Initiator' + '\r\n' + 'Milestone' + '\r\n' ; 
      PageReference pageRef = Page.WCT_Upload_Fragomen;
      Test.setCurrentPageReference(pageRef);
      WCT_Fragomen_Validation_Controller uploadImmigrationsCls = new WCT_Fragomen_Validation_Controller();
      uploadImmigrationsCls.contentFile = blob.valueof(blobCreator);
      uploadImmigrationsCls.readFile();
      List<Apexpages.Message> msgs = ApexPages.getMessages(); 
      boolean b = false;
      for(Apexpages.Message msg:msgs){
          if (msg.getDetail().contains(Label.Columns_count_not_proper)) b = true;
       }
       system.assert(b);
    }
    
}