public class WCT_Upsert_PassportInfoController extends SitesTodHeaderController{

    /* public variables */
    public WCT_Passport__c passportRecord{get;set;}

    /* Upload Related Variables */
    public String employeeEncryptId{get;set;}
    public String employeeEmail{get;set;}
    public String employeeId{get;set;}
    public String employeeLastName{get;set;}
    public String employeeMiddleName{get;set;}
    public String employeeFirstName{get;set;}
    public String passportNo{get;set;}
    public String citizenship{get;set;}
    public String passportCity{get;set;}
    public String passportCountry{get;set;}
    public String passportIssuanceDate{get;set;}
    public String passportExpirationDate{get;set;}

    public Document doc {get;set;}
    public List<String> docIdList = new List<string>();
    public List<AttachmentsWrapper> UploadedDocumentList {get; set; }
    public List<Attachment> listAttachments {get; set;}
    public Map<Id, String> mapAttachmentSize {get; set;}  
    public Integer countattach{get;set;}
    public String ssMsg {get; set;} 
    public String ssMsgStyle {get; set;} 

    // Error Message related variables
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}   

    public WCT_Upsert_PassportInfoController()
    {  
        /*Initialize all Variables*/
        init();
        
        /*Get Employee Encrypted Email from Parameter*/
        getParameterInfo();  

        /*Query to get the existing Records*/
        getExistingPassportInfo();              

        /*Get Attachment List and Size*/
        getAttachmentInfo();          
    }

    private void init()
    {
        //Custom Object Instances
        passportRecord = new WCT_Passport__c(); 
        countattach = 0;
       
        //Document Related Init
        doc = new Document();
        UploadedDocumentList = new List<AttachmentsWrapper>();
        listAttachments = new List<Attachment>();
        mapAttachmentSize = new Map<Id, String>(); 
    }     

    private void getParameterInfo(){
        employeeEncryptId = ApexPages.currentPage().getParameters().get('em');
        ssMsg = ApexPages.currentPage().getParameters().get('ss');
        
        if(ssMsg != null && ssMsg.equals('1')) {
            ssMsgStyle = 'padding-left:12px;';
        } else {
            ssMsgStyle = 'padding-left:12px;display:none;';
        }
        
        employeeEmail = CryptoHelper.decrypt(employeeEncryptId);
        employeeId = [SELECT id, Name, RecordTypeId, Email FROM Contact WHERE RecordType.Name = 'Employee' AND Email =:employeeEmail].Id;
    }

    private void getExistingPassportInfo(){
        List<WCT_Passport__c> passportList = [SELECT Id, WCT_Employee__c, WCT_Passport__c, WCT_Passport_First_Name__c, 
                                                     Passport_Last_Name__c, WCT_Passport_Middle_Name__c, WCT_Citizenship__c, WCT_Country_of_Passport__c, WCT_Passport_Issuing_City__c,
                                                     WCT_Passport_Issuance_Date__c, WCT_Passport_Expiration_Date__c, WCT_Record_Visible_to_ToD__c
                                              FROM WCT_Passport__c
                                              WHERE WCT_Employee__c =: employeeId
                                              LIMIT 1];

        if(passportList.size() > 0) {
            passportRecord = passportList[0];
        }
        
        if(passportRecord.id != null) {
            passportNo = passportRecord.WCT_Passport__c;
            employeeFirstName = passportRecord.WCT_Passport_First_Name__c;
            employeeLastName = passportRecord.Passport_Last_Name__c;
            employeeMiddleName = passportRecord.WCT_Passport_Middle_Name__c;
            citizenship = passportRecord.WCT_Citizenship__c;
            passportCountry = passportRecord.WCT_Country_of_Passport__c;
            passportCity = passportRecord.WCT_Passport_Issuing_City__c;
            passportIssuanceDate = passportRecord.WCT_Passport_Issuance_Date__c.format();
            passportExpirationDate = passportRecord.WCT_Passport_Expiration_Date__c.format();
        }            
    }
        
    private void getAttachmentInfo(){
        listAttachments = [SELECT ParentId, 
                                    Name, 
                                    BodyLength, 
                                    Id,
                                    CreatedDate
                                    FROM  Attachment 
                                    WHERE ParentId =: passportRecord.Id
                                    ORDER BY CreatedDate DESC
                                    LIMIT 50];    

        countattach=listAttachments.size();      
        
        for(Attachment a : listAttachments) {
            String size = null;

            if(1048576 < a.BodyLength){
                // Size greater than 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength){
                // Size greater than 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else{
                size = '' + a.BodyLength + ' bytes';
            }
            mapAttachmentSize.put(a.id, size);
        }
    }


    public pageReference save()
    {     
        if(passportNo == '' || passportNo == null || employeeFirstName == '' || employeeFirstName == null
           || employeeLastName == '' || employeeLastName == null || citizenship == '' || citizenship == null 
           || passportCountry == '' ||  passportCountry == null || passportCity == '' || passportCity == null 
           || passportIssuanceDate == '' ||  passportIssuanceDate == null || passportExpirationDate == '' || passportExpirationDate == null)
        {
            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
        }

         //checking for no attachment conditions
        if(countattach == 0) {
            pageErrorMessage = 'Attachment is required to submit the form.';
            pageError = true;
            return null;
        }

        passportRecord.WCT_Employee__c = employeeId;
        passportRecord.WCT_Passport__c = passportNo;
        passportRecord.WCT_Passport_First_Name__c = employeeFirstName;
        passportRecord.Passport_Last_Name__c = employeeLastName;
        passportRecord.WCT_Passport_Middle_Name__c = employeeMiddleName;
        passportRecord.WCT_Citizenship__c = citizenship;
        passportRecord.WCT_Country_of_Passport__c = passportCountry;
        passportRecord.WCT_Passport_Issuing_City__c = passportCity;
        passportRecord.WCT_Passport_Issuance_Date__c = Date.parse(passportIssuanceDate);
        passportRecord.WCT_Passport_Expiration_Date__c = Date.parse(passportExpirationDate);
        passportRecord.WCT_Record_Visible_to_ToD__c = true;
        
        upsert passportRecord;

        uploadRelatedAttachment();  
                        
        getAttachmentInfo(); 
        pageError = false; 
          
        PageReference retPage = Page.WCT_Upsert_PassportThankYou;
        retPage.setRedirect(true);
        retPage.getParameters().put('em',employeeEncryptId);
        retPage.getParameters().put('ss', '1');
        return retPage; 
    }

    /*Invoked when Upload Button in VF page is clicked and the IDs are stored in the docIdList*/
    /*All the Files Uploaded are stored in the Documents. Documents does not require parent Id.This method is used to circumvent to upload
      the documents first and then add as Attachment to the Passport (Related List)*/

    public void uploadAttachment(){
        pageError = false;
        pageErrorMessage = '';
    
        if(ApexPages.hasMessages()) {
            pageErrorMessage = '';
            for(ApexPages.Message message : ApexPages.getMessages()) {
                pageErrorMessage += message.getSummary() + '\n';
            }            
            doc = new Document();
            pageError = true;
            return;
        }                                
        
        doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
        if(doc.body != null) {
            insert doc;
            docIdList.add(doc.id);
            countattach++;
            doc = [SELECT id, name, bodylength FROM Document WHERE id = :doc.id];

            String size = '';
            if(1048576 < doc.BodyLength) {
                // Size greater than 1MB
                size = '' + (doc.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < doc.BodyLength) {
                // Size greater than 1KB
                size = '' + (doc.BodyLength / 1024) + ' KB';            
            }
            else {
                size = '' + doc.BodyLength + ' bytes';
            }
            UploadedDocumentList.add(new AttachmentsWrapper(true, doc.name, doc.id, size));
            doc = new Document();
        }
    }       
    
    //Documents Wrapper
    public class AttachmentsWrapper {
        public Boolean isSelected {get;set;}
        public String docName {get;set;}
        public String documentId {get;set;}
        public String size {get; set;}
            
        public AttachmentsWrapper(Boolean selected, String Name, String Id, String size){
            isSelected = selected;
            docName = Name ;
            documentId = Id;
            this.size = size;
        }        
    }    
    
    private void uploadRelatedAttachment(){

        if(!docIdList.isEmpty()) { //If there is any Documents Attached in Web Form

            List<String> selectedDocumentId = new List<String>();
            for(AttachmentsWrapper aWrapper : UploadedDocumentList) {
                if(aWrapper.isSelected) {
                    selectedDocumentId.add(aWrapper.documentId);
                }
            }
                
            /* Select Documents which are Only Active (Selected) */
            List<Document> selectedDocumentList = new List<Document>();
            if(!selectedDocumentId.isEmpty()){
                selectedDocumentList = [SELECT id,
                                               name,
                                               ContentType,
                                               Type,
                                               Body 
                                               FROM 
                                               Document 
                                               WHERE 
                                               id IN :selectedDocumentId];
            }
                
            /* Convert Documents to Attachment */
            List<Attachment> attachmentsToInsertList = new List<Attachment>();
            for(Document doc : selectedDocumentList){
                Attachment a = new Attachment();
                a.body = doc.body;
                a.ContentType = doc.ContentType;
                a.Name= doc.Name;
                a.Description=String.valueof(passportRecord.Id);
                a.parentid = passportRecord.Id; //Passport Record Id which is been retrived.
                attachmentsToInsertList.add(a);                
            }
            
            if(attachmentsToInsertList.size() > 0)
                countattach=attachmentsToInsertList.size();
            else if(listAttachments.size() > 0)
                countattach=listAttachments.size();

            if(!attachmentsToInsertList.isEmpty()){
                insert attachmentsToInsertList;
            }
                
            List<Document> listDocuments = new List<Document>();
            listDocuments = [SELECT Id
                                    FROM
                                    Document
                                    WHERE
                                    Id IN :docIdList];
            if( (null != listDocuments) && (0 < listDocuments.size()) ) {
                delete listDocuments;
            }
        } else {
            if(listAttachments.size() > 0)
                countattach=listAttachments.size();
        }
    }
    
}