@isTest
private class WCT_Upsert_PassportInfoController_Test {

    static testMethod void myUnitTest1() {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');

        PageReference pageRef = Page.Wct_Upsert_Passport;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);     
       
        Test.starttest();

        WCT_Upsert_PassportInfoController controller=new WCT_Upsert_PassportInfoController();      
        controller.doc=WCT_UtilTestDataCreation.createDocument();
        controller.uploadAttachment();
        controller.employeeLastName='Raja';
        controller.employeeMiddleName='';
        controller.passportNo='12345';
        controller.citizenship='Indian';
        controller.passportCity='Chennai';
        controller.passportCountry='India';
        controller.passportIssuanceDate = '12/01/2014';
        controller.passportExpirationDate = '12/01/2015';        
        controller.save();
        
        Test.stoptest();
    }    

    static testMethod void myUnitTest2() {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');

        PageReference pageRef = Page.Wct_Upsert_Passport;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);     
       
        Test.starttest();

        WCT_Upsert_PassportInfoController controller=new WCT_Upsert_PassportInfoController();      
        controller.doc=WCT_UtilTestDataCreation.createDocument();
        controller.employeeId=con.id;
        controller.uploadAttachment();
        controller.employeeLastName='Raja';
        controller.employeeMiddleName='';
        controller.passportNo='12345';
        controller.citizenship='Indian';
        controller.passportCity='Chennai';
        controller.passportCountry='India';
        controller.passportIssuanceDate = '12/01/2014';
        controller.passportExpirationDate = '12/01/2015';        
        controller.save();
                
        Test.stoptest();
    }    
}