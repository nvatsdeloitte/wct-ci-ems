/**
    * Class Name  : WCT_UtilConstants 
    * Description : Class will use as constants. 
*/

public class WCT_UtilConstants {
  public WCT_UtilConstants (){}
    
    /******************WCT_AttachmentTrigger.trigger********************************/
    public static final String INTEGRATION_PROFILE_NAME='6_Integration';
    
    /*************************WCT_UploadCases.cls***************************************************/
    
    public static final String ANSWER_YES='Yes';
    public static final String ANSWER_UNKNOWN='Unknown';
    public static final String ANSWER_NO='No';
    public static final String STAGING_STATUS_NOT_STARTED='Not Started';
    
    
    
    
    /*************************WCT_CreateCasesHelper.cls***************************************************/
    
    public static final String CASE_STATUS_NEW='New';
    public static final String CASE_PRIORITY_MEDIUM='3 - Medium';
    public static final String CASE_SUBJECT_PREBI='Pre-BI Issue';
    public static final String CASE_SUBCATEGORY1_EXPERIENCED='Experienced Hiring';
    public static final String CASE_SUBCATEGORY1_CAMPUS='Campus Hiring';
    public Static final String CASE_SUBCATEGORY2='Pre-Background Investigation';
    public static final String CASE_CATEFORY='Acquisition';
    public static final String CASE_ORIGIN='Pre-BI Report Upload';
    public static final String STAGING_STATUS_COMPLETED='Completed';
    public static final String STAGING_STATUS_ERROR='Failed';
    public static final String CASE_RECORDTYPE_PRE_BI='Pre-BI';
    public static final String REQUISITION_JOB_TYPE_EXPERIENCED='Experienced';
    public static final String QUESTION_1='Q01';
    public static final String QUESTION_2='Q02';
    public static final String QUESTION_3='Q03';
    public static final String QUESTION_4='Q04';
    public static final String QUESTION_5='Q05';
    public static final String QUESTION_6='Q06';
    public static final String QUESTION_7='Q07';
    public static final String QUESTION_8='Q08';
    public static final String QUESTION_9='Q09';
    public static final String QUESTION_10='Q10';
    public static final String QUESTION_11='Q11';
    public static final String QUESTION_12='Q12';
    public static final String QUESTION_13='Q13';
    public static final String QUESTION_14='Q14';
    public static final String QUESTION_15='Q15';
    public static final String QUESTION_16='Q16';
    public static final String QUESTION_17='Q17';
    public static final String QUESTION_18='Q18';
    public static final String QUESTION_19='Q19';
    public static final String QUESTION_20='Q20';
    public static final String QUESTION_21='Q21';
    public static final String QUESTION_22='Q22';
    public static final String QUESTION_23='Q23';
    public static final String QUESTION_24='Q24';
    public static final String QUESTION_25='Q25';
    public static final String QUESTION_26='Q26';
    public static final String QUESTION_27='Q27';
    public static final String QUESTION_28='Q28';
    public static final String QUESTION_29='Q29';
    public static final String QUESTION_30='Q30';
    public static final String QUESTION_31='Q31';
    public static final String QUESTION_32='Q32';
    public static final String QUESTION_33='Q33';
    public static final String QUESTION_34='Q34';
    public static final String QUESTION_35='Q35';
    public static final String QUESTION_36='Q36';
    public static final String QUESTION_37='Q37';
    public static final String QUESTION_38='Q38';
    public static final String QUESTION_39='Q39';
    public static final String QUESTION_40='Q40';
    public static final String QUESTION_41='Q41';
    public static final String QUESTION_42='Q42';
    public static final String QUESTION_43='Q43';
    public static final String QUESTION_44='Q44';
    public static final String QUESTION_45='Q45';
    public static final String QUESTION_46='Q46';
    public static final String QUESTION_47='Q47';
    public static final String QUESTION_48='Q48';
    public static final String QUESTION_49='Q49';
    public static final String QUESTION_50='Q50';
    public static final String QUESTION_51='Q51';
    public static final String QUESTION_52='Q52';
    public static final String QUESTION_53='Q53';
    public static final String QUESTION_54='Q54';
    public static final String QUESTION_55='Q55';
    public static final String QUESTION_56='Q56';
    public static final String QUESTION_57='Q57';
    public static final String QUESTION_58='Q58';
    public static final String QUESTION_59='Q59';
    public static final String QUESTION_60='Q60';
    public static final String QUESTION_61='Q61';
    public static final String QUESTION_62='Q62';
    public static final String QUESTION_63='Q63';
    public static final String QUESTION_64='Q64';
    public static final String QUESTION_65='Q65';
    public static final String QUESTION_66='Q66';
    public static final String QUESTION_67='Q67';
    public static final String QUESTION_68='Q68';
    public static final String QUESTION_69='Q69';
    public static final String QUESTION_70='Q70';
    public static final String QUESTION_71='Q71';
    public static final String QUESTION_72='Q72';
    public static final String QUESTION_73='Q73';
    public static final String QUESTION_74='Q74';
    public static final String QUESTION_75='Q75';
    public static final String QUESTION_76='Q76';
    public static final String QUESTION_77='Q77';
    public static final String QUESTION_78='Q78';
    public static final String QUESTION_79='Q79';
    public static final String QUESTION_80='Q80';
    public static final String QUESTION_81='Q81';
    public static final String QUESTION_82='Q82';
    public static final String QUESTION_83='Q83';
    public static final String QUESTION_84='Q84';
    public static final String QUESTION_85='Q85';
    public static final String QUESTION_86='Q86';
    public static final String QUESTION_87='Q87';
    public static final String QUESTION_88='Q88';
    public static final String QUESTION_89='Q89';
    public static final String QUESTION_90='Q90';
    public static final String QUESTION_91='Q91';
    public static final String QUESTION_92='Q92';
    public static final String QUESTION_93='Q93';
    public static final String QUESTION_94='Q94';
    public static final String QUESTION_95='Q95';
    public static final String QUESTION_96='Q96';
    public static final String QUESTION_97='Q97';
    public static final String QUESTION_98='Q98';
    public static final String QUESTION_99='Q99';
    public static final String QUESTION_100='Q100';
    
    
    public static final String CANDIDATE_RT='Candidate';
    public Static final String TASK_NOT_STARTED='Not Started';
    public Static final String TASK_PRIORITY_NORMAl='2-Normal';
    public Static final String TASK_RECRUITER_ASSIGNEE='Recruiter';
    public Static final String TASK_SUBJECT_RECRUITER='To -Do - ';
    public Static final String TASK_SUBJECT_FIRSTTRACK='Enter into Fastrack -';
    public Static final String TASK_SUBJECT_IMMIGRATION='IMMIGRATION ';
    
 // These are the temp varibale please do not use them.
    public Static final String  Case_Origin_Chat='Chat';
    public Static final String  Case_Origin_Email='Email';
    public Static final String  Case_Origin_Phone_Helpline='Phone - Helpline';
    public Static final String  Case_Origin_Phone_Voicemail='Phone - Voicemail';
    public Static final String  Case_Origin_Web='Web';
    public Static final String  Case_Prefered_Contact_Phone='Phone';
    public Static final String  Case_status_Canceled='Canceled';
    public Static final String  Case_status_Closed='Closed';
    public Static final String  Case_status_Closed_Duplicate='Closed-Duplicate';
    public Static final String  Case_status_Closed_Sent_to_BT='Closed-Sent to B&T';
    public Static final String  Case_status_Closed_Sent_to_PSN='Closed-Sent to PSN';
    public Static final String  Case_status_Resolved='Resolved';
// ****************************************************
   public Static final String  Task_Priority= '2-Normal'; 
   public Static final String  Task_Status=  'Not Started';         
   
//*************************************************************   
   public static final string Field_Custom_Exception='FIELD_CUSTOM_VALIDATION_EXCEPTION';
   public static final string WCT_caseEditPageForPreBi='WCT_caseEditPageForPreBi';
   public static final string WCT_AttachmentSuccessMessage='Attachment uploaded successfully';
   public static final string WCT_AttachmentErrorsMessage='Error uploading attachment';
   public static final string WCT_AttachmentRedirectURL='/apex/WCT_AttachmentRelatedList?id=';
   
   public static final string WCT_EditTaskUpdateError='INVALID_FIELD_FOR_INSERT_UPDATE';
   public static final string WCT_EditTaskInvalidCrossRefError='INVALID_CROSS_REFERENCE_KEY';
   public static final string WCT_EditTaskUpdateRemoveErrorString=': [';
   public static final string WCT_EditTaskStringActivityPageRtURL='/apex/ActivitiesPage?';
   public static final string WCT_EditTaskStringClosedCaseRtURL='/apex/WCT_Closed_Case?id=';
   public static final string WCT_EditTaskString_ret='ret';
   public static final string WCT_EditTaskString_TaskIDs='TaskIDs';
   public static final string WCT_EditTaskString_caseId='caseId';
   public static final string WCT_EditTaskString_ErrorMessage='ErrorMessage';
   public static final string WCT_EditTaskString_Task='Task';
   public static final string WCT_EditTaskString_TR_Task='TR_Task';
   public static final string WCT_EditTaskString_Pre_BI='Pre-BI';
   public static final string WCT_ClosedCaseString_Status='Status';
   public static final string WCT_ClosedCaseString_Closed='Closed';
   public static final string WCT_ClosedCaseString_Resolved='Resolved';
   public static final string WCT_ClosedCaseString_Canceled='Canceled';
   public static final string WCT_ClosedCaseString_Case='Case';
   
      
/*************************WCT_FindStagesCorrected.cls***************************************************/


    public Static final String PROCESS_NONE='There are no records to process.';
    public Static final String STAGING_STATUS_CORRECTED='Corrected';
    

/**************************WCT_UserHelper.cls *********************************************************/

    public Static final String INTERVIWER_PROFILE_NAME='10_Interviewer_Deloitte';
    public static final String INTERVIWER_PERMISSION_SET_NAME='X10_Interviewer_Portal_PermissionSet';
    public static final String INTERVIWER_PERMISSION_SET_LIC_NAME='CompanyCommunityPsl';
    public static final String PRE_BI_PERMISSION_SET_NAME='X10_Pre_BI_Support_Admin_Permission_Set';
    public static final String RECRUITER_COMPANY='10_CTS_Recruiter';
    public static final String Recruiting_Coordinator='10_CTS_Recruiting_Coordinator';
    public static final String CTS_Interviewer='CTS_Interviewer';
/*************************WCT_CancelInterview************************************************/
    public static final String Interview_Canceled='Canceled';
    
/*************************WCT_InterviewPortalController************************************************/
    public Static final String INTERVIWER_Portal_Today = 'Today';
    public Static final String INTERVIWER_Portal_UpComing = 'Upcoming';
    public Static final String INTERVIWER_Portal_Past = 'Past';
    public Static final String INTERVIWER_Portal_All = 'All';
/**************************WCT IEF AS PDF****************************************************************/
    public Static final String IEF_Form_Submitted = 'Submitted';
    public static final String Case_RecordType_case='Case'; 
    
 /**************************WCT_ContactCheckEmployeeRC.Tgr ******************************************/
    public Static final String CONTACT_GROUP_ADHOCCONTACTTYPE='Candidate Ad Hoc';
    public Static final String CONTACT_GROUP_CONTACTTYPE='Candidate';
    public Static final String CONTACT_GROUP_EMPLOYEETYPE='Employee';
    
    
/**************************Case_Trigger.Tgr ******************************************/

    public Static final String CASE_EMPLOYEE_INVOL_RC='Employee Lifecycle Events - Involuntary Separations';
    public Static final String CASE_EMPLOYEE_VOL_RC ='Employee Lifecycle Events - Voluntary Separations'; 
    public Static final String TASK_COMPLETED_STATUS='Completed';
    public Static final String TASK_DEFERRED_STATUS='Deferred';
    
    
 /************************WCT_CreateImmigrationsHelper.Class**************************/
    public Static final String Employee_RT='Employee';
    public Static final String H1_VISA_RT='H1 Visa';
    public Static final String L1_VISA_RT='L1 Visa';
    public Static final String B1_VISA_RT='B1 Visa';
    public Static final String LCA_FILED='LCA filed';
    public Static final String Business_VISA_RT='Business Visa';
    
 /************************WCT_OfferTriggerHandler.Class**************************/
 
    public Static final String Offer_Approved = 'Offer Approved';
    public Static final String Ready_for_Review = 'Ready for Review';
    public Static final String Offer_to_Be_Extended = 'Offer to Be Extended';
    public Static final String Returned_for_Revisions = 'Returned for Revisions';
    public Static final String Offer_Sent = 'Offer Sent';
    public Static final String Offer_Accepted = 'Offer Accepted';
    public Static final String Offer_Declined = 'Offer Declined'; 
    
 /************************WCT_updateFlagsOnLeaves_Batch_Test.cls**************************/
    public Static final String Military_RT='Military';
    public Static final String Personal_RT='Personal';
    
}