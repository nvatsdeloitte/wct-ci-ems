public class WCT_Visa_Approval_FormController extends SitesTodHeaderController{

 /* public variables */
 public WCT_Immigration__c ImmigrationRecord{get;set;}
 
  /* Upload Related Variables */
 public Document doc {get;set;}
 public List<String> docIdList = new List<string>();
 public List<AttachmentsWrapper> UploadedDocumentList {get; set; }
 public task t{get;set;}
 public String taskid{get;set;}
 public List<Attachment> listAttachments {get; set;}
 public Map<Id, String> mapAttachmentSize {get; set;} 
 public Integer countattach{get;set;}

 // Error Message related variables
 public boolean pageError {get; set;}
 public String pageErrorMessage {get; set;}
 public String supportAreaErrorMesssage {get; set;}   

 
 //Visa Dates
 public String visaStartDate {get;set;}
 public String visaEndDate {get;set;}
 
 public WCT_Visa_Approval_FormController()
{  
    
    
        /*Initialize all Variables*/
        init();

        /*Get Task ID from Parameter*/
        getParameterInfo();  
        
        /*Task ID Null Check*/
        if(taskid=='' || taskid==null)
        {
           invalidEmployee=true;
           return;
        }
        
        /*Get Task Instance*/
        getTaskInstance();
        
        /*Get Attachment List and Size*/
        getAttachmentInfo();
        
        /*Query to get Immigration Record*/  
        getImmigrationDetails();
}

 private void init(){

        //Custom Object Instances
       ImmigrationRecord= new WCT_Immigration__c (); 
        countattach = 0;
        //Document Related Init
        doc = new Document();
        UploadedDocumentList = new List<AttachmentsWrapper>();
        listAttachments = new List<Attachment>();
        mapAttachmentSize = new Map<Id, String>();

       
    }   
    
  private void getParameterInfo(){
        taskid = ApexPages.currentPage().getParameters().get('taskid');
    }

    private void getTaskInstance(){
        t=[SELECT Status, OwnerId, WhatId, WCT_Auto_Close__c, WCT_Is_Visible_in_TOD__c  FROM Task WHERE Id =: taskid];
    }

    private void getAttachmentInfo(){
        listAttachments = [SELECT ParentId , 
                                    Name, 
                                    BodyLength, 
                                    Id,
                                    CreatedDate
                                    FROM  Attachment 
                                    WHERE ParentId = :taskid
                                    ORDER BY CreatedDate DESC
                                    LIMIT 50 ];    
              
        for(Attachment a : listAttachments) {
            String size = null;

            if(1048576 < a.BodyLength){
                // Size greater than 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength){
                // Size greater than 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else{
                size = '' + a.BodyLength + ' bytes';
            }
            mapAttachmentSize.put(a.id, size);
        }
    }
   
public void getImmigrationDetails()
{
    
     //getting contact obejct record
     
       
    ImmigrationRecord = [SELECT   Id,
                                     Name,
                                     WCT_Visa_Expiration_Date__c,
                                     WCT_Visa_Interview_Q_A__c,
                                     WCT_Visa_Start_Date__c,
                                     WCT_Visa_Status__c,
                                     WCT_Immigration_Status__c ,
                                     WCT_Visa_Type__c ,
                                     WCT_Reason_for_Denial__c,
                                     Ownerid
                                     FROM WCT_Immigration__c 
                                     where id=:t.WhatId ];
  
}


public pageReference save()
{ 
    /*Custom Validation Rules are handled here*/
    
        if( (null == visaStartDate) ||('' == visaStartDate) || (null == visaEndDate) ||('' == visaEndDate))
            {
            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
        }
        if(String.isNotBlank(visaStartDate)){
            ImmigrationRecord.WCT_Visa_Start_Date__c = Date.parse(visaStartDate);
        }
        if(String.isNotBlank(visaEndDate)){
            ImmigrationRecord.WCT_Visa_Expiration_Date__c = Date.parse(visaEndDate);
        }
        if(ImmigrationRecord.WCT_Visa_Expiration_Date__c<date.today())
        {
            pageErrorMessage = 'Visa Expiration Date cannot be in the past.';
            pageError = true;
            return null;
        }
        
        if(ImmigrationRecord.WCT_Visa_Start_Date__c>ImmigrationRecord.WCT_Visa_Expiration_Date__c)
        {
            pageErrorMessage = 'Visa Expiration Date cannot be less then Visa Start Date.';
            pageError = true;
            return null;
        }
        
        /*Changing the Owner Id of Task Temporarily to avoid Integrity Exception*/
        t.OwnerId=UserInfo.getUserId();
        upsert t;

        //Update Dates
        if(String.isNotBlank(visaStartDate)){
            ImmigrationRecord.WCT_Visa_Start_Date__c = Date.parse(visaStartDate);
        }
        if(String.isNotBlank(visaEndDate)){
            ImmigrationRecord.WCT_Visa_Expiration_Date__c = Date.parse(visaEndDate);
        }
        
        
        /*update ImmigrationRecord Record*/
        upsert ImmigrationRecord ;

        /* Upload Documents as Related Attachments to Case After the Case Record is created */
        uploadRelatedAttachment();
        
        //checking for no attachment conditions
       
        if(countattach<=0)
        {
            pageErrorMessage = 'Attachment is required to submit the form.';
            pageError = true;
            return null;
        }
         
        
           //updating task record
        If (t.WCT_Auto_Close__c == true){   
            t.status = 'completed';
        }else{
            t.status = 'Employee Replied';  
        }

        if(string.valueOf(ImmigrationRecord.Ownerid).startsWith('00G')){
            //owner is Queue. So set it to GM&I User
           // t.Ownerid = '005f0000001AWth';
            t.Ownerid = System.Label.GMI_User; 

        }else{
             t.OwnerId = ImmigrationRecord.Ownerid;
          
        }

        t.WCT_Is_Visible_in_TOD__c=false; 
        upsert t;
        
        
        getAttachmentInfo();
        /*Set Page Reference After Save*/
        
        PageReference page=new PageReference('/apex/WCT_Visa_Approval_FormThankYou?em='+CryptoHelper.encrypt(LoggedInContact.Email)+'&taskid='+taskid) ;  
        page.setredirect(true);
        return page;
        //return null;
    
}

private void uploadRelatedAttachment(){
  
    if(!docIdList.isEmpty()) { //If there is any Documents Attached in Web Form

            List<String> selectedDocumentId = new List<String>();
            for(AttachmentsWrapper aWrapper : UploadedDocumentList) {
                if(aWrapper.isSelected) {
                    selectedDocumentId.add(aWrapper.documentId);
                }
            }
            
        /* Select Documents which are Only Active (Selected) */
            List<Document> selectedDocumentList = new List<Document>();
            if(!selectedDocumentId.isEmpty()){
                selectedDocumentList = [
                                           SELECT 
                                               id,
                                               name,
                                               ContentType,
                                               Type,
                                               Body 
                                           FROM 
                                               Document 
                                           WHERE 
                                               id IN :selectedDocumentId
                                        ];
            }
            
            /* Convert Documents to Attachment */
            List<Attachment> attachmentsToInsertList = new List<Attachment>();
            for(Document doc : selectedDocumentList){
                Attachment a = new Attachment();
                a.body = doc.body;
                a.ContentType = doc.ContentType;
                a.Name= doc.Name;
                a.Description=String.valueof(taskid);
                a.parentid = taskid; //immigration Record Id which is been retrived.
                attachmentsToInsertList.add(a); 
                               
            }
            countattach=attachmentsToInsertList.size();
            if(!attachmentsToInsertList.isEmpty()){
                insert attachmentsToInsertList;
            }
            
            List<Document> listDocuments = new List<Document>();
            listDocuments = [
                                SELECT
                                    Id
                                FROM
                                    Document
                                WHERE
                                    Id IN :docIdList
                            ];
            if( (null != listDocuments) && (0 < listDocuments.size()) ) {
                delete listDocuments;
            }
        }
    }

    /*Invoked when Upload Button in VF page is clicked and the IDs are stored in the docIdList*/
    /*All the Files Uploaded are stored in the Documents. Documents does not require parent Id.This method is used to circumvent to upload
      the documents first and then add as Attachment to the Cases (Related List)*/

    public void uploadAttachment(){
        pageError = false;
        pageErrorMessage = '';
    
        if(ApexPages.hasMessages()) {
            pageErrorMessage = '';
            for(ApexPages.Message message : ApexPages.getMessages()) {
                pageErrorMessage += message.getSummary() + '\n';
            }            
            doc = new Document();
            pageError = true;
            return;
        }                                
        
        doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
        if(doc.body != null) {
            insert doc;
            docIdList.add(doc.id);
            doc = [SELECT id, name, bodylength FROM Document WHERE id = :doc.id];

            String size = '';
            if(1048576 < doc.BodyLength) {
                // Size greater than 1MB
                size = '' + (doc.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < doc.BodyLength) {
                // Size greater than 1KB
                size = '' + (doc.BodyLength / 1024) + ' KB';            
            }
            else {
                size = '' + doc.BodyLength + ' bytes';
            }
            UploadedDocumentList.add(new AttachmentsWrapper(true, doc.name, doc.id, size));
            doc = new Document();
        }
    }       
    

 //Documents Wrapper
public class AttachmentsWrapper {
 public Boolean isSelected {get;set;}
 public String docName {get;set;}
 public String documentId {get;set;}
 public String size {get; set;}
        
 public AttachmentsWrapper(Boolean selected, String Name, String Id, String size){
  isSelected = selected;
  docName = Name ;
  documentId = Id;
  this.size = size;
   }        
 }
 
}