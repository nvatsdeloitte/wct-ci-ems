@isTest
public class WCT_Visa_Approval_FormController_Test
{
    public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Immigration__c immi=WCT_UtilTestDataCreation.createImmigration(con.id);
        insert immi;
        WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        insert taskRef; 
        task t=WCT_UtilTestDataCreation.createTask(immi.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t.WCT_Auto_Close__c=false;
        insert t;
        Test.starttest();
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_Visa_Approval_Form;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);
        datetime visastart=date.Today().adddays(-10);
        datetime visaend=date.Today().adddays(-1);
        WCT_Visa_Approval_FormController controller=new WCT_Visa_Approval_FormController();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller=new WCT_Visa_Approval_FormController();
        controller.save();
        controller.visaStartDate=visastart.format('MM/dd/yyyy');
        controller.visaEndDate=visaend.format('MM/dd/yyyy');
        controller.save();
        visaend=date.Today().adddays(1);
        visastart=date.Today().adddays(10);
        controller.visaStartDate=visastart.format('MM/dd/yyyy');
        controller.visaEndDate=visaend.format('MM/dd/yyyy');
        controller.save();
        controller.visaStartDate=visaend.format('MM/dd/yyyy');
        controller.visaEndDate=visastart.format('MM/dd/yyyy');
        controller.save();
        controller.doc=WCT_UtilTestDataCreation.createDocument();
        controller.countattach=1;
        controller.uploadAttachment();
        controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        controller.uploadAttachment();
        Test.stoptest();
        
     }
}