@isTest
public class WCT_Visa_Logistics_FormController_Test
{
    public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Immigration__c immi=WCT_UtilTestDataCreation.createImmigration(con.id);
        immi.WCT_First_Name_As_on_passport__c='Test';
        immi.WCT_Last_Name_As_on_passport__c='Test';
        immi.WCT_Pref_US_Location_for_Pass_Pickup__c='Test';
        immi.WCT_Attached_DS160_online_visa_form__c=true;
        immi.WCT_Attached_Petition_Copy__c=true;
        immi.WCT_Attached_Visa_Fee_Payment_Conf__c=true;
        immi.WCT_Attached_Passport_Bio_Pages_copy__c=true;
        immi.UID_Number__c='12345678';
        insert immi;
        WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        insert taskRef;
        task t=WCT_UtilTestDataCreation.createTask(immi.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t.WCT_Auto_Close__c=false;
        insert t;
        Test.starttest();
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_Visa_Logistics_Form;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_Visa_Logistics_FormController controller=new WCT_Visa_Logistics_FormController();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller=new WCT_Visa_Logistics_FormController();
        controller.save();
        controller.doc=WCT_UtilTestDataCreation.createDocument();
        controller.countattach=1;
        controller.uploadAttachment();
        controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        controller.uploadAttachment();
        Test.stoptest();      
     }
    public static testmethod void m2()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Immigration__c immi=WCT_UtilTestDataCreation.createImmigration(con.id);
        immi.WCT_First_Name_As_on_passport__c='Test';
        immi.WCT_Last_Name_As_on_passport__c='Test';
        immi.WCT_Pref_US_Location_for_Pass_Pickup__c='Test';
        immi.WCT_Attached_DS160_online_visa_form__c=true;
        immi.WCT_Attached_Petition_Copy__c=true;
        immi.WCT_Attached_Visa_Fee_Payment_Conf__c=true;
        immi.WCT_Attached_Passport_Bio_Pages_copy__c=true;
        insert immi;
        task t=WCT_UtilTestDataCreation.createTask(immi.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Auto_Close__c=false;
        insert t;
        Test.starttest();
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_Visa_Logistics_Form;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_Visa_Logistics_FormController controller=new WCT_Visa_Logistics_FormController();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller=new WCT_Visa_Logistics_FormController();
        controller.save();
        controller.doc=WCT_UtilTestDataCreation.createDocument();
        controller.countattach=1;
        controller.uploadAttachment();
        controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        controller.uploadAttachment();
        Test.stoptest();      
     }
}