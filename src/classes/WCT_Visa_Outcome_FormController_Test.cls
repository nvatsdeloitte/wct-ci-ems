@isTest
public class WCT_Visa_Outcome_FormController_Test
{
    public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Immigration__c immi=WCT_UtilTestDataCreation.createImmigration(con.id);
        immi.WCT_Immigration_Status__c='Visa Denied';
        insert immi;
        WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        insert taskRef; 
        task t=WCT_UtilTestDataCreation.createTask(immi.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t.WCT_Auto_Close__c=false;
        insert t;
        Test.starttest();
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_Visa_Outcome_Form;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_Visa_Outcome_FormController controller=new WCT_Visa_Outcome_FormController();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller=new WCT_Visa_Outcome_FormController();
        controller.insertorupdate();
        controller.ImmigrationRecord.WCT_Immigration_Status__c='Visa Approved';
        controller.ImmigrationRecord.WCT_Visa_Interview_Q_A__c='Test';
        controller.dropDownValue='Visa Approved';
        controller.displayVisaDenial=true;
        controller.ImmigrationRecord.WCT_Reason_for_Denial__c=null;
        controller.insertorupdate();
        controller.displayVisaDenial=false;
        controller.doc=WCT_UtilTestDataCreation.createDocument();
        controller.uploadAttachment();
        controller.insertorupdate(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        controller.uploadAttachment();
        Test.stoptest();
    }
}