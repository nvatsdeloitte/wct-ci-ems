/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class WCT_editCandidateDocumentController_test{

   public static Contact TestContact=new Contact();
   public static WCT_Candidate_Documents__c testCondiateDocument= new WCT_Candidate_Documents__c(); 
      /** 
        Method Name  : createContacts
        Return Type  : Contact
        Type      : private
        Description  : Create temp records for data mapping         
    **/
    private static Contact createContact()
    {
      Testcontact=WCT_UtilTestDataCreation.createContact();
      insert TestContact;
      return TestContact;
    }
    
      /** 
        Method Name  : createCandidate_Documents
        Return Type  : WCT_Candidate_Documents__c
        Type      : private
        Description  : Create temp records for data mapping         
    **/
    private static WCT_Candidate_Documents__c createCandidate_Documents()
    {
      testCondiateDocument.WCT_Candidate__c=TestContact.id;
      testCondiateDocument.WCT_Created_Date__c=system.now();
      insert testCondiateDocument;
      return testCondiateDocument;
    }
  
static testMethod void editCandidateDocument()
  {
   TestContact= createContact();
   testCondiateDocument=createCandidate_Documents();
   Test.setCurrentPage(Page.WCT_Edit_candidate_document); 
   system.currentPageReference().getParameters().put('id', TestContact.Id);
   WCT_edit_candidate_document_controller cd = new WCT_edit_candidate_document_controller();
   cd.updateDocuments(); 
   cd.cancel(); 
  }
  
   


}