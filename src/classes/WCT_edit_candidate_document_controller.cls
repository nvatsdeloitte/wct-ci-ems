public with sharing class WCT_edit_candidate_document_controller {

    public PageReference cancel() {
        PageReference pageRef = new PageReference('/' + CandidateId);
        pageRef.setRedirect(true); //Redirecting user to the candidate detail page. 
        return pageRef;
    }


 public PageReference updateDocuments() {
        update list_candidate_doc;
        PageReference pageRef = new PageReference('/' + CandidateId);
        pageRef.setRedirect(true); //Redirecting user to the candidate detail page. 
        return pageRef;
    }
public String timeStrDt {get;set;}
public String timeStrTm {get;set;}

public String CandidateId;
public List<WCT_Candidate_Documents__c> list_candidate_doc{get;set;}
public WCT_edit_candidate_document_controller()
        {
         CandidateId= ApexPages.currentPage().getParameters().get('id') ;
         list_candidate_doc= new list<WCT_Candidate_Documents__c >();
         list_candidate_doc=[select Name,WCT_File_Name__c,WCT_Visible_to_Interviewer__c,WCT_Created_Date__c,WCT_DateToshow__c, WCT_Candidate__c,Attachment__c  from WCT_Candidate_Documents__c where WCT_Candidate__c=: CandidateId];
         for(WCT_Candidate_Documents__c  List_cd: list_candidate_doc ){
          if(List_cd.WCT_Created_Date__c!=null){
            DateTime d = List_cd.WCT_Created_Date__c;
            timeStrDt = d.format('MM /dd/ yyyy');
            timeStrTm = d.format('hh:mm a');
            List_cd.WCT_DateToshow__c= timeStrDt +'  '+timeStrTm ;
             }
            }
        }
}