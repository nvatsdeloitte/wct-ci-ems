public class WCT_groupInterviewPageRdsnController {
    
    public integer actisze{get;set;}
    public integer upsze{get;set;}
    public integer allsze{get;set;}
    public integer pendsze{get;set;}
    public list<WCT_Interview_Forms__c > list_iform= new list<WCT_Interview_Forms__c >();
    public string errorMsg {get;set;}
    public string intdue{get;set;}
    public boolean status{get;set;}
    public PageReference SaveIEF() {
    errorMsg = '';
    for(GD_candidateWraper ls_WC: lst_toDisplay){
      for(GD_candidateWraper ls_WC1: candidateTowork){
       if(ls_WC.IntTracker.WCT_Candidate_Name__c==ls_WC1.IntTracker.WCT_Candidate_Name__c){
        WCT_Interview_Forms__c I_Form= new WCT_Interview_Forms__c();
        I_form.WCT_Interview_Junction__c=ls_WC1.IntTracker.id;
        I_form.WCT_Candidate_Name__c=ls_WC.IntTracker.WCT_Candidate_Name__c;
        I_form.Candidate_Name__c=ls_WC.IntTracker.WCT_Candidate_Name__c;
        I_form.WCT_IEF_Status__c='Submitted';
        I_form.WCT_Interviewer_Name__c=ls_WC1.IntTracker.WCT_Interviewer__r.name;
        I_Form.wct_gd_Approch__c=ls_WC.Approch;
        I_form.wct_GD_Articulation__c=ls_WC.Articulation;
        I_form.wct_GD_Leadership__c=ls_WC.Leadership;
        I_Form.wct_gd_Listening__c=ls_WC.Listening;
        I_form.wct_gd_Participation__c=ls_WC.Participation;
        I_form.wct_gd_Reasult__c=ls_WC.Reasult;
        I_form.WCT_Evaluation_Summary__c=ls_WC.AdditionalCommts;
        
        I_Form.wct_GD_Response__c=ls_WC.Response;
        system.debug('%%'+ls_WC.Response);
           system.debug('%%'+ls_WC.Total+'####'+ls_WC.Reasult);
        if(ls_WC.Total == null || ls_WC.Total =='' || ls_WC.Reasult == '' || ls_WC.Reasult==null){
            if(errorMsg == ''){
                errorMsg = 'Please enter Total and Result for Candidate '+ls_WC.IntTracker.WCT_Candidate_Name__c+',';
            }else{
                errorMsg += 'Please enter Total and Result for Candidate '+ls_WC.IntTracker.WCT_Candidate_Name__c+',';
            }
        }else{
            I_form.WCT_GD_Total__c=ls_WC.Total;
        }
          if((ls_WC.Reasult==null)||(ls_WC.Reasult=='')){
            I_Form.WCT_Interviewer_s_Final_Recommendation__c='Please enter final recomendation.';  
              system.debug('>>I_Form.WCT_Interviewer_s_Final_Recommendation__c>>>'+I_Form.WCT_Interviewer_s_Final_Recommendation__c);
              system.debug('>>ls_WC.Reasult>>>'+ls_WC.Reasult);
          }else
          {
               I_Form.WCT_Interviewer_s_Final_Recommendation__c=ls_WC.Reasult;
           
            system.debug('>>>I_Form.WCT_Interviewer_s_Final_Recommendation__c>>'+I_Form.WCT_Interviewer_s_Final_Recommendation__c);
        
          }
          I_Form.RecordTypeId= Schema.SObjectType.WCT_Interview_Forms__c.getRecordTypeInfosByName().get('Group Interview').getRecordTypeId();
          list_iform.add(I_form);
            
            }
         }
        }
          try{
              if(errorMsg <> ''){
                   String[] arrError = errorMsg.split(',');
                    for(String s:arrError){
                        
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,s));
                    }
                    list_iform.clear();
                  return null;
              }else{
                  insert list_iform;  
                  pageReference pdf = page.WCT_GroupPDFIntermediate_Page;
                  return pdf;
              }
             }catch(Exception e)
                {
                 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Error>>>'+e));
                }
        
        return null;
    }
    
    public PageReference createPdfs(){
        // Creatring PDF        
                
         //WCT_Gd_interview_PDF_Creation.createPDF(list_iform);
         //Creating IEF Attachments
         string CanName, IntvName, fileName, IntvId, InterviewRound,IntvType,IntTrackerName;
         list<Attachment> attToInsert = new list<Attachment>();
         if(!list_iform.isEmpty()){
         for(WCT_Interview_Forms__c iform : [SELECT WCT_Interview_Junction__r.WCT_Candidate_Name__c,WCT_Interview_Junction__r.WCT_Interviewer__r.Name,WCT_Interview_Junction__r.WCT_Interview__r.Name, 
                                                    WCT_Interview_Junction__r.WCT_Interview__r.WCT_Interview_Round__c ,WCT_Interview_Junction__r.WCT_Interview_Stage__c,
                                                    WCT_Interview_Junction__r.WCT_Interview_Type__c,WCT_Interview_Junction__r.Name from WCT_Interview_Forms__c 
                                                    where id IN :list_iform]){
                                                    
            CanName = iform.WCT_Interview_Junction__r.WCT_Candidate_Name__c;
            IntvName = iform.WCT_Interview_Junction__r.WCT_Interviewer__r.Name;
            IntvId = iform.WCT_Interview_Junction__r.WCT_Interview__r.Name;
            if(iform.WCT_Interview_Junction__r.WCT_Interview__r.WCT_Interview_Round__c<>null){
                InterviewRound = iform.WCT_Interview_Junction__r.WCT_Interview__r.WCT_Interview_Round__c;
            }else{
                InterviewRound = '';
            }
            if(iform.WCT_Interview_Junction__r.WCT_Interview_Type__c <> null){
                IntvType = iform.WCT_Interview_Junction__r.WCT_Interview_Type__c;
            }else{
                IntvType = '';
            }
            IntTrackerName = iform.WCT_Interview_Junction__r.Name;                                      
            fileName = CanName+'_'+InterviewRound+'_'+IntvType+'_'+IntvName+'_'+'Group-IEF_'+IntTrackerName+'.pdf';
            pageReference pdf = Page.WCT_GroupIEF_PDF;
            pdf.getParameters().put('id',iform.id);
            system.debug('form id'+iform.id);
            Blob body;
            try {
                body =  pdf.getContent();
            } catch (VisualforceException e) {
                system.debug('exception');
                body = Blob.valueOf('Some Text');
            }
            Attachment newAtt = new Attachment();
            newAtt.body = body;
            
            newAtt.Name= fileName;
            newAtt.parentid = iform.WCT_Interview_Junction__c;
            attToInsert.add(newAtt);
            newAtt = new Attachment();        
         }
         if(!attToInsert.isEmpty()){
            insert attToInsert;
         }
         }
                 
         return new Pagereference('/apex/WCT_groupInterviewPageRedesign');
    
    
    }


    public string interviewID{get;set;}
    public string intTrackerSubStatus{get;set;}
    public string intSubmType {get;set;}
    public string PanelList {get;set;}
    
    public  list<WCT_Interview_Junction__c> ls_IT{get;set;}  
    public  list<GD_candidateWraper> lst_toDisplay{get;set;}
    public list<GD_candidateWraper> candidateTowork{
        get
        {
            list<GD_candidateWraper> lst_gpcandidate= new list<GD_candidateWraper>();
            for(WCT_Interview_Junction__c lpIJ:ls_IT)
            {
                lst_gpcandidate.add(new GD_candidateWraper(lpIJ));
            }
            return lst_gpcandidate;
        }
        set;
    }
    public WCT_groupInterviewPageRdsnController ()
    {
        Intdue=System.currentPagereference().getParameters().get('interviewdue');
     WCT_InterviewPortalControllerRedesign portal=new WCT_InterviewPortalControllerRedesign();
        actisze=portal.acitemSize;
        upsze=portal.upSize;
        allsze=portal.allSize;
        pendsze=portal.pndmSize;
     errorMsg = '';
     ls_IT = new list<WCT_Interview_Junction__c>();
    lst_toDisplay= new  list<GD_candidateWraper>();
     interviewID= ApexPages.currentPage().getParameters().get('interviewId');
     intTrackerSubStatus = ApexPages.currentPage().getParameters().get('status');
     intSubmType = ApexPages.currentPage().getParameters().get('submType');
     if(intSubmType <> null && intSubmType == 'Single'){
        ls_IT =[select WCT_Candidate_Name__c, id,WCT_Interviewer__r.name from WCT_Interview_Junction__c where WCT_Interview__c =: interviewID
                 AND WCT_Interviewer__r.email = :UserInfo.getUserEmail() AND WCT_IEF_Submission_Status__c <> 'Submitted'];
    
     }else{
        ls_IT =[select WCT_Candidate_Name__c, id,WCT_Interviewer__r.name from WCT_Interview_Junction__c where WCT_Interview__c =: interviewID AND WCT_IEF_Submission_Status__c <> 'Submitted'];
     }
     if(!ls_IT.isEmpty()){
        set<String> intvrNames = new set<String>();
        for(WCT_Interview_Junction__c ij:ls_IT){
            intvrNames.add(ij.WCT_Interviewer__r.name);
        }
        if(!intvrNames.isEmpty()){
            PanelList=conListOfStr(intvrNames);
        }
     }
     set<string> set_candidateName= new set<string>();
     for(GD_candidateWraper lsC: candidateTowork)
     {
      if(!set_candidateName.contains(lsC.IntTracker.WCT_Candidate_Name__c))
      {
       lst_toDisplay.add(lsC);
      }
      set_candidateName.add(lsC.IntTracker.WCT_Candidate_Name__c);
      
     }
           
     
    }
    public pageReference redirectSubIEF(){
        if(intTrackerSubStatus <> null && intTrackerSubStatus == 'Submitted'){
            status=false;
            //return new Pagereference('/apex/WCT_GDThankyou_Page');
            return null;
        }else if(ls_IT.isEmpty()){
            status=false;
            //return new Pagereference('/apex/WCT_GDThankyou_Page');
            return null;
        }
        else{
             status=true;
            return null;
        }   
    }
    //Convert List of strings to string
     public string conListOfStr(set<string> strList){
        string retString = '';
        for(string s : strList){
            retString += s + ';'+'\n';
        }   
        return retString;
     }
    public class GD_candidateWraper
    {
        public WCT_Interview_Junction__c IntTracker{get;set;}
        public String Leadership{get;set;}
        public String Listening{get;set;}
        public String Participation{get;set;}
        public String Approch{get;set;}
        public String Articulation{get;set;}
        public String Response{get;set;}
        public String Total{get;set;}
        public String Reasult{get;set;}
        public String AdditionalCommts{get;set;}
        
        public List<SelectOption> getResultTypes() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('',''));
            options.add(new SelectOption('Select','Select'));
            options.add(new SelectOption('Reject','Reject'));
            options.add(new SelectOption('Tentative','Tentative'));
            return options;
        }
               
        public GD_candidateWraper(WCT_Interview_Junction__c tempTracker)
        {
            IntTracker=tempTracker;
          
            }
        
        public GD_candidateWraper()
        {
            IntTracker=new WCT_Interview_Junction__c() ;
           
        }
    }    

}