public class WCT_markAttendancePage 
    {    
        public Event eventRec{get;set;}    
        public List<WCT_Attendance_Tracker__c> employeeAttList{get;set;}    
        public WCT_markAttendancePage(ApexPages.StandardController controller)
        {        
                eventRec=(Event)controller.getRecord();                
                employeeAttList = [Select id,WCT_Event_Subject__c, WCT_Employee__r.Name, WCT_Attend__c, WCT_Event_ID__c,WCT_status__c,WCT_Response__c,WCT_Event_Start_Date__c,WCT_Event_End_Date__c from WCT_Attendance_Tracker__c where WCT_Event_ID__c  =:eventRec.Id];    
        }
    }