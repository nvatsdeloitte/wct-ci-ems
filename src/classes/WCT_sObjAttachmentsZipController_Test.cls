/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class WCT_sObjAttachmentsZipController_Test {

    public static Contact Candidate;
    public static String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();
    public static List<WCT_Leave__c> leaveListToInsert;
    public static Id LeavePerRecordTypeId=WCT_Util.getRecordTypeIdByLabel('WCT_Leave__c','Personal');
    
    /** 
        Method Name  : createCandidate
        Return Type  : Contact
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static Contact createCandidate()
    {
        Candidate=WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
        insert Candidate;
        return  Candidate;
    } 
    
    private static List<WCT_Leave__c> CreateLeaveList(){
        leaveListToInsert = WCT_UtilTestDataCreation.createLeaves(LeavePerRecordTypeId,Candidate);
        insert leaveListToInsert;
        return leaveListToInsert;
    }

    
    
    static testMethod void withParentUpload(){
        Candidate = createCandidate();
        Candidate.WCT_Personnel_Number__c = 12345;
        Update Candidate;
        leaveListToInsert = CreateLeaveList();
        Attachment att = new Attachment(parentId=candidate.id,body=Blob.valueOf('Unit Test Attachment Body'),name='testFile');
        insert att;
        Attachment attLeave = new Attachment(parentId=leaveListToInsert[0].id,body=Blob.valueOf('Unit Test Attachment Body'),name='testFile');
        insert attLeave;
        test.startTest();
        ApexPages.currentPage().getParameters().put('id',Candidate.Id);
        WCT_sObjAttachmentsZipController sObjZip= new WCT_sObjAttachmentsZipController();
        sObjZip.getAttachments();
        WCT_sObjAttachmentsZipController.getAttachment(att.id);
        sObjZip.empName = 'FirstName';
        sObjZip.empEmail = 'candidate';
        sObjZip.empPersonId = '12345';
        sObjZip.leaveName = 'LN';
        sObjZip.searchLeaves();
        sObjZip.zipcontent = 'text content';
        sObjZip.uploadZip();
        sObjZip.LeaveWrapperList[0].isSelected = true;
        sObjZip.getDocuments();
        sObjZip.zipFileName = 'test file';
        sObjZip.zipcontent = 'text content';
        sObjZip.uploadZip();
        test.stopTest();
    }
    
    static testMethod void withParentCase(){
        list<Case> caseList = new list<Case>();
        Test_Data_Utility.createCase();
        caseList = [select Id from Case limit 1];
        EmailMessage em = new EmailMessage(FromAddress = userInfo.getUserEmail(),ParentId=caseList[0].id, Incoming = True, ToAddress= 'rala@deloitte.com', Subject = 'Testing', TextBody = '23456 ');
        insert em;
        Attachment att = new Attachment(parentId=em.id,body=Blob.valueOf('Unit Test Attachment Body'),name='testFile');
        insert att;
        test.startTest();
        ApexPages.currentPage().getParameters().put('id',caseList[0].Id);
        WCT_sObjAttachmentsZipController sObjZip= new WCT_sObjAttachmentsZipController();
        sObjZip.getAttachments();
        WCT_sObjAttachmentsZipController.getAttachment(att.id);
        test.stopTest();
    }

}