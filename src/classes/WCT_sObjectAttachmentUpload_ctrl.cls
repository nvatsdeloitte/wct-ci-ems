public class WCT_sObjectAttachmentUpload_ctrl {

    public Attachment attOne {get;set;}
    public Attachment attTwo {get;set;}
    public Attachment attThree {get;set;}
    public Attachment attFour {get;set;}
    public Attachment attFive {get;set;}
    public string parentId {get;set;}
    
    public List<Attachment> attListToInsert {get;set;}
    
    public WCT_sObjectAttachmentUpload_ctrl(){
        parentId = Apexpages.currentpage().getparameters().get('id');
        if(parentId == null){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL,'Please provide Parent Id'));
        }
        attOne = new Attachment();
        attTwo = new Attachment();
        attThree = new Attachment();
        attFour = new Attachment();
        attFive = new Attachment();
        attListToInsert = new List<Attachment>();
    }
    
   public pagereference uploadAttachments(){
       if(attOne.name <> null && attOne.name <> ''){
           attOne.parentId = parentId;
           attListToInsert.add(attOne);
       }
       
       if(attTwo.name <> null && attTwo.name <> ''){
           attTwo.parentId = parentId;
           attListToInsert.add(attTwo);
       }
       
       if(attThree.name <> null && attThree.name <> ''){
           attThree.parentId = parentId;
           attListToInsert.add(attThree);
       }
       
       if(attFour.name <> null && attFour.name <> ''){
           attFour.parentId = parentId;
           attListToInsert.add(attFour);
       }
       
       if(attFive.name <> null && attFive.name <> ''){
           attFive.parentId = parentId;
           attListToInsert.add(attFive);
       }
       try{
           if(!attListToInsert.isEmpty()){
               Insert attListToInsert;
           }
           //goToParentPage();
           return new pageReference('/'+parentId);
       }catch(Exception ex){
           //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
           return null;
       }
   
   
   }
   
   public pageReference goToParentPage(){
       return new pageReference('/'+parentId);
   }

}