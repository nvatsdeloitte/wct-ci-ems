@isTest
private class WCT_sObjectLookUpCtrl_TEST{
    static testMethod void sobjectCtrl(){
        PageReference casePage = page.WCT_sObjectLookUpPage;
        test.setCurrentPage(casePage);
        ApexPages.currentpage().getParameters().put('objectname','Case');
        ApexPages.currentpage().getParameters().put('comp','TestComp');
        ApexPages.currentpage().getParameters().put('filter','Status = \'New\'');
        ApexPages.currentpage().getParameters().put('addfield','Status');
        WCT_sObjectLookUp_Controller caseLookUp = new WCT_sObjectLookUp_Controller();
        caseLookUp.getfieldList();
        caseLookUp.strSearchItem = 'test';
        caseLookUp.search();
        
        PageReference solutionPage = page.WCT_sObjectLookUpPage;
        test.setCurrentPage(solutionPage);
        ApexPages.currentpage().getParameters().put('objectname','Solution');
        ApexPages.currentpage().getParameters().put('comp','TestComp');
        ApexPages.currentpage().getParameters().put('filter','Status = \'Draft\'');
        ApexPages.currentpage().getParameters().put('addfield','Status');
        WCT_sObjectLookUp_Controller solutionLookUp = new WCT_sObjectLookUp_Controller();
        solutionLookUp.strSearchItem = 'test';
        solutionLookUp.search();
        
        PageReference AccPage = page.WCT_sObjectLookUpPage;
        test.setCurrentPage(AccPage);
        ApexPages.currentpage().getParameters().put('objectname','Account');
        ApexPages.currentpage().getParameters().put('comp','TestComp');
        ApexPages.currentpage().getParameters().put('filter','Type = \'Other\'');
        ApexPages.currentpage().getParameters().put('addfield','Type');
        WCT_sObjectLookUp_Controller accountLookUp = new WCT_sObjectLookUp_Controller();
        accountLookUp.strSearchItem = 'test';
        accountLookUp.search();
    
    }

}