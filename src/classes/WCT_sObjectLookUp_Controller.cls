/*
    * Class Name  : WCT_sObjectLookUp_Controller   
    * Description : Controller for Generic sObject Look Up Component
*/
public with sharing class WCT_sObjectLookUp_Controller{
    //Variable Declaration
    public String objType {get;set;}
    public String additionalFields {get;set;}
    public String filter {get;set;}
    public String compId {get;set;}
    public List<sObject> sObjList {get;set;}
    public String query {get;set;}
    public String querySearch {get;set;}
    public String strSearchItem {get;set;}
    public boolean notCaseSol {get;set;}
    public boolean caseobj {get;set;}
    public boolean sol {get;set;}
    public List<String> fieldList=new List<String>();
    public String Addfieldquery {get;set;}
        
    public List<String> getfieldList(){
        return fieldList;
    }
    //Constructor to show initial results based on type of object and filter
    public WCT_sObjectLookUp_Controller(){
        notCaseSol = false;
        caseobj = false;
        sol = false;
        objType = ApexPages.currentpage().getParameters().get('objectname');
        compId = ApexPages.currentpage().getParameters().get('comp');
        Map<string,string> param = ApexPages.CurrentPage().getParameters();
        if(param.containsKey('filter')){
            filter = ApexPages.currentpage().getParameters().get('filter');
        }
        
        if(param.containsKey('addfield')){
            additionalFields = ApexPages.currentpage().getParameters().get('addfield');
        }
        
        if (additionalFields != '')  {
        fieldList = additionalFields.split(',');
        }
        
        if (fieldList.size()>0) 
        {
            Addfieldquery = '';
            for (String s : fieldList) 
            {
                Addfieldquery += ', ' + s;
            }
        }
        
        if(objType <> 'Case' && objType <> 'Solution'){
        notCaseSol = true;
        query = 'SELECT id,Name';
        if (fieldList.size()>0){
            query += Addfieldquery ;
        }
        query += ' from '+objType+' ORDER BY NAME ASC LIMIT 50';
        if(filter.length()!=0){
            query = 'SELECT id,Name';
            if (fieldList.size()>0){
                query += Addfieldquery ;
            }
            query += ' from '+objType+' WHERE '+ filter +' ORDER BY NAME ASC LIMIT 50';
        }
        }else if(objType == 'Case'){
        caseobj = true;
        query = 'SELECT id,CaseNumber,Subject,Contact.name,Account.Name,Status from '+objType+' LIMIT 50';
        if(filter.length()!=0){
            query = 'SELECT id,CaseNumber,Subject,Contact.name,Account.Name,Status from '+objType+' WHERE '+ filter +' LIMIT 50';
        }
        }else if(objType == 'Solution'){
        sol = true;
        query = 'SELECT id,SolutionNumber from '+objType+' LIMIT 50';
        if(filter.length()!=0){
            query = 'SELECT id,SolutionNumber from '+objType+' WHERE '+ filter +' LIMIT 50';
        }
        }
        sObjList = database.query(query);
    }
    //Method to search results.
    public void search()
    {
        sObjList.clear();
        strSearchItem = strSearchItem;
        if(objType <> 'Case' && objType <> 'Solution'){
        notCaseSol = true;
        querySearch = 'SELECT id,Name';
        if (fieldList.size()>0){
            querySearch += Addfieldquery ;
        }
        querySearch += ' from '+objType;
        querySearch+= ' WHERE Name Like \'%'+strSearchItem+'%\'';
        if(filter.length()!=0){
            querySearch+= ' AND ' + filter ;
        }
        querySearch+= ' ORDER BY NAME ASC LIMIT 50';
        }else if(objType == 'Case'){
        caseobj = true;
        querySearch = 'SELECT id,CaseNumber,Subject,Contact.name,Account.name,Status from '+objType;
        querySearch+= ' WHERE CaseNumber Like \'%'+strSearchItem+'%\'';
        if(filter.length()!=0){
            querySearch+= ' AND ' + filter ;
        }
        querySearch+= 'LIMIT 50';
        }else if(objType == 'Solution'){
        sol = true;
        querySearch = 'SELECT id,SolutionNumber from '+objType;
        querySearch+= ' WHERE SolutionNumber Like \'%'+strSearchItem+'%\'';
        if(filter.length()!=0){
            querySearch+= ' AND ' + filter ;
        }
        querySearch+= 'LIMIT 50';
        }
        //strSearchItem =strSearchItem.remove('%');
        sObjList = database.query(querySearch);
    }
}