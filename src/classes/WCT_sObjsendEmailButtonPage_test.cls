@isTest
private class WCT_sObjsendEmailButtonPage_test {
    
    public static Contact Employee;
    public static List<WCT_Leave__c> leaveRecList;
    public static Contact createContact(String lastname){
        
       Contact c = new Contact(LastName = lastname , Email=lastname+'@abc.com');
       insert c;
       return c;
           
    } 
    
     /** 
        Method Name  : createEmployee
        Return Type  : Contact
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static Contact createEmployee()
    {
        Employee=WCT_UtilTestDataCreation.createEmployee(WCT_Util.getRecordTypeIdByLabel('Contact','Employee'));
        insert Employee;
        return  Employee;
    }  

    /** 
        Method Name  : createLeavesForEmp
        Return Type  : List<WCT_Leave__c>
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private static List<WCT_Leave__c> createLeavesForEmp(){
        leaveRecList = WCT_UtilTestDataCreation.createLeaves(WCT_Util.getRecordTypeIdByLabel('WCT_Leave__c','Personal'),Employee);
        insert leaveRecList;
        return leaveRecList;
    }
    
    
    
   
    /* **************************************************************************************
    
    Name : CancelUnitTest
    Description:- this method will test the cancel,open and close of pop up functionality
    
     ***************************************************************************************/
  
 
    static testMethod void CancelUnitTest() {
        Employee = createEmployee();
        leaveRecList = createLeavesForEmp();
                
        ApexPages.currentPage().getParameters().put('id', leaveRecList[0].id);
        WCT_sObjsendEmailButtonPage_ctrl SEB= new WCT_sObjsendEmailButtonPage_ctrl();
        SEB.cancel();
        SEB.CloseAttachmentPopup();
        SEB.CloseTempPopup();
        SEB.CloseLVEpopup();
        SEB.OpenAttachmentPopup();
    }
  
    /* **************************************************************************************
    
    Name : AttachmentUnitTest
    Description:- this method will test the add attachment functionality
    
     ***************************************************************************************/
    
    static testMethod void AttachmentUnitTest() {
       
         Employee = createEmployee();
         leaveRecList = createLeavesForEmp();
         Blob TestBodyAtt = Blob.valueof('TestStringBody');
         Attachment att = new Attachment(Body=TestBodyAtt,Name='Test',ParentId=leaveRecList[0].id);
         Insert att;
         
        ApexPages.currentPage().getParameters().put('id', leaveRecList[0].id);
        WCT_sObjsendEmailButtonPage_ctrl SEB= new WCT_sObjsendEmailButtonPage_ctrl();
        String TestStringBody = 'StringToBlob';
        Blob TestBody = Blob.valueof(TestStringBody);
        SEB.OpenAttachmentPopup();
        SEB.cDoc.Body=TestBody;
        SEB.cDoc.Name='Test';
        SEB.save();
        system.assertNotEquals(SEB.ShowAttachmentlist.size(),0);
        SEB.AttachementListFormCurrObj();
        SEB.CloseAttachmentPopup();
        SEB.CloseTempPopup();
        SEB.clearToField();
    
    }   
    
    /* **************************************************************************************
    
    Name : AddTemplateUnitTest
    Description:- this method will unit test the add Template functionality
    
     ***************************************************************************************/
    
    static testMethod void AddTemplateUnitTest() {
       
        Employee = createEmployee();
        leaveRecList = createLeavesForEmp(); 
        ApexPages.currentPage().getParameters().put('id', leaveRecList[0].id);
        WCT_sObjsendEmailButtonPage_ctrl SEB= new WCT_sObjsendEmailButtonPage_ctrl();
        List<Selectoption> sb=SEB.Folders;
        SEB.OpenSelectTemplatePopup();
        SEB.folderid=Label.Attachment_Zip_Document_Folder_Id;
        system.assertNotEquals(SEB.folderid,null);
        SEB.Searchtemplfiles();
        list<EmailTemplate> em= [SELECT Id,Name,Body,FolderId,HtmlValue,TemplateType FROM EmailTemplate where IsActive = true];
        String TextEmailTempId;
        for(EmailTemplate lpEm : em)
        {
            if(lpEm.TemplateType=='HTML')
            {
                SEB.sEmailTemplateID=lpEm.Id;
            }else
            {
                TextEmailTempId=lpEm.Id;
            }
        }
        SEB.SelectTemplate();
        SEB.sEmailTemplateID=TextEmailTempId; 
        SEB.SelectTemplate();
    }
     /* **************************************************************************************
    
    Name : SendEmailExceptionUnitTest
    Description:- this method will test the exceptional condition if To field is blank. 
    
     ***************************************************************************************/
    
    
    static testMethod void SendEmailExceptionUnitTest() {
       
        
        Employee = createEmployee();
        leaveRecList = createLeavesForEmp(); 
        ApexPages.currentPage().getParameters().put('id', leaveRecList[0].id);
        WCT_sObjsendEmailButtonPage_ctrl SEB= new WCT_sObjsendEmailButtonPage_ctrl();
        
        SEB.ToFieldEmail='';
        SEB.ToField='';
        SEB.ToFieldId='';
        SEB.SendEmail();
    
    } 
 
   /* **************************************************************************************
    
    Name : SendEmailSubjectExceptionUnitTest
    Description:- this method will test the exceptional condition if Subject field is blank.    
    
     ***************************************************************************************/
    static testMethod void SendEmailSubjectExceptionUnitTest() {
       
        Employee = createEmployee();
        leaveRecList = createLeavesForEmp(); 
        ApexPages.currentPage().getParameters().put('id', leaveRecList[0].id);
        WCT_sObjsendEmailButtonPage_ctrl SEB= new WCT_sObjsendEmailButtonPage_ctrl();
        Contact testContact=createContact('testContact');
        SEB.ToFieldId=testContact.id;
        system.assertEquals(SEB.subjectLine, '');
        
        SEB.SendEmail();
        
   }  
   
   /* **************************************************************************************
    
    Name : SendEmailWithoutBodyExceptionUnitTest
    Description:- this method will test the exceptional condition if body field is blank.   
    
     ***************************************************************************************/
 
   static testMethod void SendEmailWithoutBodyExceptionUnitTest() {
       
        Employee = createEmployee();
        leaveRecList = createLeavesForEmp(); 
        ApexPages.currentPage().getParameters().put('id', leaveRecList[0].id);
        WCT_sObjsendEmailButtonPage_ctrl SEB= new WCT_sObjsendEmailButtonPage_ctrl();
        Contact testContact=createContact('testContact');
        SEB.ToFieldId=testContact.id;
        SEB.BCcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.CcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue= SEB.ShowAdditionalFieldValue+','+SEB.getContactName(testContact.id, 'Email');
        SEB.subjectLine='Test Email';
        system.assertEquals(SEB.BodyContent, null);
        Seb.BodyContent='';
        SEB.SendEmail();
        
   }
   
   /* **************************************************************************************
    
    Name : SendEmailWithoutTemplateUnitTest
    Description:- this method will test the sending plaintext on email if template is nt selected.  
    
     ***************************************************************************************/
 
   
  static testMethod void SendEmailWithoutTemplateUnitTest() {
       
        Employee = createEmployee();
        leaveRecList = createLeavesForEmp(); 
        ApexPages.currentPage().getParameters().put('id', leaveRecList[0].id);
        WCT_sObjsendEmailButtonPage_ctrl SEB= new WCT_sObjsendEmailButtonPage_ctrl();
        Contact testContact=createContact('testContact');
        SEB.ToFieldId=testContact.id;
        SEB.BCcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.CcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue= SEB.ShowAdditionalFieldValue+','+SEB.getContactName(testContact.id, 'Email');
        system.assertEquals(SEB.templateId, '');
        SEB.subjectLine='Test Email';
        Seb.BodyContent='Test Email body';
        SEB.SendEmail();
        
   }   
   /* **************************************************************************************
    
    Name : SendEmailWithTemplateUnitTest
    Description:- this method will test the send the  email with template.  
    
     ***************************************************************************************/
 
   static testMethod void SendEmailWithTemplateUnitTest() {
       
        Employee = createEmployee();
        leaveRecList = createLeavesForEmp(); 
        ApexPages.currentPage().getParameters().put('id', leaveRecList[0].id);
        WCT_sObjsendEmailButtonPage_ctrl SEB= new WCT_sObjsendEmailButtonPage_ctrl();
        Contact testContact=Employee;
        SEB.ToFieldId=Employee.id;
        SEB.BCcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.CcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue= SEB.ShowAdditionalFieldValue+','+SEB.getContactName(testContact.id, 'Email');
        String fId = [Select Id from Folder where DeveloperName='Case_Folder'].Id;
        list<EmailTemplate> emTmp = [select Id,name,body,HtmlValue from EmailTemplate where FolderId =:fId Limit 1];
        SEB.templateId=emTmp[0].id;
        system.assertNotEquals(SEB.templateId, '');
        SEB.bodycontent = 'testing';
        SEB.SendEmail();
   }  
    
    
       /* **************************************************************************************
    
    Name : SendEmailWithTemplateUnitTest
    Description:- this method will test the send the  email with attachment.    
    
     ***************************************************************************************/
 
    
    static testMethod void SendEmailWithAttachmentUnitTest() {
       
        Employee = createEmployee();
        leaveRecList = createLeavesForEmp(); 
        ApexPages.currentPage().getParameters().put('id', leaveRecList[0].id);
        WCT_sObjsendEmailButtonPage_ctrl SEB= new WCT_sObjsendEmailButtonPage_ctrl();
        String TestStringBody = 'StringToBlob';
        Blob TestBody = Blob.valueof(TestStringBody);
        system.assertNotEquals(SEB.caseid ,null);
        
        SEB.OpenAttachmentPopup();
        SEB.cDoc.Body=TestBody;
        SEB.cDoc.Name='Test';
        SEB.save();
        SEB.AdditionalToFieldValue  = 'test@gmailt.com';
        SEB.CloseAttachmentPopup();
        Contact testContact=createContact('testContact');
        system.assertNotEquals(Seb.ShowAttachmentlist.size(), 0);
        SEB.ToField ='';
        SEB.subjectLine='test email';
        SEB.BodyContent='test body';
        SEB.SendEmail();
        system.debug('^^'+SEB.ToField+'^^1'+SEB.ToFieldId);
    } 
    
      /* **************************************************************************************
    
    Name : SendEmailButtonUnitTest
    Description:- this method will test the send the email with attachment and template.    
    
     ***************************************************************************************/
 
    static testMethod void SendEmailButtonUnitTest() {
       
        Employee = createEmployee();
        leaveRecList = createLeavesForEmp(); 
        ApexPages.currentPage().getParameters().put('id', leaveRecList[0].id);
        
        test.starttest();
        WCT_sObjsendEmailButtonPage_ctrl SEB= new WCT_sObjsendEmailButtonPage_ctrl();
        String TestStringBody = 'StringToBlob';
        Blob TestBody = Blob.valueof(TestStringBody);
        
        SEB.OpenAttachmentPopup();
        SEB.cDoc.Body=TestBody;
        SEB.cDoc.Name='Test';
        SEB.save();
        SEB.CloseAttachmentPopup();
        Contact testContact=Employee;
        SEB.ToFieldId=Employee.id;
        SEB.BCcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.BCcFieldValue=SEB.BCcFieldValue+','+SEB.getContactName(testContact.id, 'Email');
        SEB.CcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.CcFieldValue=SEB.CcFieldValue+','+SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue= SEB.ShowAdditionalFieldValue+','+SEB.getContactName(testContact.id, 'Email');
        list<Selectoption> RelatedToField = SEB.RelatedToSelectOptionValues;
        list<Selectoption> FromField = SEB.fromAddressValues;
        String fId = [Select Id from Folder where Type = 'Email' LIMIT 1].Id;
        list<EmailTemplate> emTmp = [select Id,name,body,HtmlValue from EmailTemplate where FolderId =:fId Limit 1];
        Blob TestBodyAtt = Blob.valueof('TestStringBody');
        Attachment att = new Attachment(Body=TestBodyAtt,Name='Test',ParentId=emTmp[0].id);
        Insert att;
        
        //SEB.closeEMpopup();
        //SEB.OpenEMPopup();
        SEB.templateId=emTmp[0].id;
        SEB.selectTemplate();
        SEB.bodycontent = 'testing';
        SEB.SendEmail();
        delete att;
        SEB.selectTemplate();
        //SEB.emailMessageList[0].isselected = true;
        //SEB.InsertEMAttachment();
        test.stoptest();
    } 
    

}