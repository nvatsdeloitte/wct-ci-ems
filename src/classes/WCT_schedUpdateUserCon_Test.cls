@isTest 
private class WCT_schedUpdateUserCon_Test {
    public static Id profileIdRecCoo=[Select id from Profile where Name=:WCT_UtilConstants.Recruiting_Coordinator].Id;
     public static Contact Employee;
     public static User userrec;
    private Static User createUserRecCoo()
    {
        userrec=WCT_UtilTestDataCreation.createUser('batUser', profileIdRecCoo, 'batUser@deloitte.com', 'batUser@deloitte.com');
        insert userrec;
        return  userrec;
    }
    private Static Contact createEmpoyee()
    {
        Employee=WCT_UtilTestDataCreation.createEmployee(WCT_Util.getRecordTypeIdByLabel('Contact', 'Employee'));
        Employee.WCT_Company_Code__c = 'Deloitte';
        Employee.phone = '123456789';
        Employee.WCT_Employee_Status__c = 'Active';
        Employee.email = 'batUser@deloitte.com';
        insert Employee;
        return  Employee;
    }    
    static testMethod void UpdUserFieldsFromConTest(){
        userrec = createUserRecCoo();
         
        system.runas(userrec){    
            Employee =  createEmpoyee();
        }
        test.startTest();
        WCT_schedUpdateUserFieldsFromCont s = new WCT_schedUpdateUserFieldsFromCont();
        system.schedule('user update Batch','0 0 2 1 * ?',s);
        test.stopTest();
    }     
}