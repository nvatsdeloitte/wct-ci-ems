global class WCT_scheduledArticleExpNotify_10days implements Schedulable{
   global void execute(SchedulableContext SC) {
      WCT_batchArticleExpNotify_10days artExp = new WCT_batchArticleExpNotify_10days(); 
      Database.executeBatch(artExp,200);
   }
   
}