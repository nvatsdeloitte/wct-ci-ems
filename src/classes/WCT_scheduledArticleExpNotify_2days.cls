global class WCT_scheduledArticleExpNotify_2days implements Schedulable{
   global void execute(SchedulableContext SC) {
      WCT_batchArticleExpNotify_2days artExp = new WCT_batchArticleExpNotify_2days();
      Database.executeBatch(artExp,200);
   }
}