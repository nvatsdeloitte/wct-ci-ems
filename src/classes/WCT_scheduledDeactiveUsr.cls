/**
    * Class Name  : WCT_scheduledDeactiveUsr 
    * Description : Class will schedule to Deactivate USers where Employee has withdrawan or Retiree Deloitte.  
*/

global class WCT_scheduledDeactiveUsr implements Schedulable {
    
   String query='Select id,Email from Contact where WCT_Employee_Status__c=\'Withdrawn\' or WCT_Employee_Status__c=\'Retiree\' ';
   
   global void execute(SchedulableContext sc) {
      WCT_DeactivateUser b = new WCT_DeactivateUser (query); 
      database.executebatch(b,2000);
   }
}