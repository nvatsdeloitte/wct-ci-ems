/**
    * Class Name  : WCT_scheduledDeactiveUsr_Test
    * Description : This apex test class will use to test the WCT_scheduledDeactiveUsr
*/
@isTest
private class WCT_scheduledDeactiveUsr_Test {

   /** 
        Method Name  : schedulingBatch
        Return Type  : testMethod
        Type 		 : private
        Description  : Create Pre-Bi Stage Table Records.         
    */
    static testMethod void schedulingBatch() {
        Test.startTest();
        WCT_scheduledDeactiveUsr  m = new WCT_scheduledDeactiveUsr  ();
		String sch = '20 30 8 10 2 ?';
		String jobID = system.schedule('Merge Job', sch, m);
		Test.stopTest();
    }
}