global class WCT_scheduledOfferStatus implements Schedulable { 
    /*private String sessionId;

    global WCT_scheduledOfferStatus( String sessionId ) {
        this.sessionId = sessionId;
    }*/
  global void execute(SchedulableContext sc) {
     
      WCT_Batch_Offer_Status_SentOffer_Update batchApex = new WCT_Batch_Offer_Status_SentOffer_Update();
      Database.executeBatch(batchApex,5);  

   }
}