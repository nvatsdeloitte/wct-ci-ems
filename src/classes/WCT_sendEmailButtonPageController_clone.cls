global with sharing class WCT_sendEmailButtonPageController_clone {

 
// *********************************************************************************************
// *************** variable declariation********************************************************
    public list<Document> cTempDocToinsert= new list<Document>();
    
    global Document cDoc{get;set;}
    public String ToFieldEmail { get; set; }
    public list<string> ShowAttachmentlist{get;set;} 
    public String RelatedToFieldId { get; set; }
    public String RelatedToField { get; set; }
    public boolean IsAttachmentRetrive= false;
    public list < SelectOption > RelatedToSelectOptionValues { 
    get
    {
     list<SelectOption> tempOptions= new list<SelectOption>();
     tempOptions.add(new selectOption('case','Case'));
     return  tempOptions;       
    } 
    set; }

    public String SelectedRetaedTovalue { get; set; }
    public String Case_TR_EM_RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get(Label.Case_TR_EM_RecordTypeId).getRecordTypeId();
    public String Case_ELE_ES_RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get(Label.Case_ELE_ES_RecordTypeId).getRecordTypeId();
    public list <string> ccid   = new list<String>(); 
    public list<string> bccId = new list<String>();  
    public list<String>    toEmailAddress= new list<string>();     
    User usr = [Select Id, Name, Profile.Name from User where Id = :UserInfo.getUserId()];  
   list<string> DocIds=new list<string>();
   public string templateId='';
   public String sEmailTemplateID {get;set;}
   public Boolean ShowselectTemplatepopup{get;set;}
   public Boolean ShowAttchPopUp{get;set;}
   public String fileName {get; set;}
   public Blob attachedFile {get; set;} 
   public List<EmailTemplate> Templatelist { get; set; }
   public list<selectOption> Folders { 
    get
    {
             list<selectOption> option= new list<selectoption>();
             option.add(new selectOption('','--Select--'));
             option.add(new SelectOption(UserInfo.getOrganizationId(), 'Unfiled Public Email Templates'));
                    for(folder f:[SELECT Id,Name,Type FROM Folder where Type='Email'])
                    {
                            if(f.name !='CIC Reports')
                            option.add(new selectOption(f.id,f.name));
                    }
             return option;
    }
     set; 
    }
    public String folderid { get; set; }
    public String BodyHiddenContentvalue { get; set; }
    public String BodyContent { get; set; }
    public String subjectLine { get; set; }
    public String BCcIdFieldValue { get; set; }
    public String BCcFieldValue { get; set; }
    public String CcIdFieldValue { get; set; }
    public String CcFieldValue { get; set; }
    public String AdditionalToFieldValue { get; set; }
    public String ShowAdditionalFieldValue { get; set; }
    public String ToFieldId { get; set; }
    public String ToField { get; set; }
    public string SelectedFromId{get;set;}
    public String caseid;
    public EmailTemplate emailTemp {get;set;}
    public string caseThreadId {get;set;}
    public Set<String> selTempdocIdList;
    
    public boolean clearToField {get;set;}
    public EmailMessage em {get;set;}
    public void clearToField(){
        ToFieldId = '';
        ToField = '';
        ToFieldEmail = '';
        clearToField = false;
    }

    public list < SelectOption > fromAddressValues{
        get {
            list < SelectOption > tempOption = new list < SelectOption > ();
                     
            
            list<OrgWideEmailAddress> owea = [SELECT Address,DisplayName,Id FROM OrgWideEmailAddress ];
           
            if((usr.Profile.Name == Label.CIC_agent_Profile)||(usr.Profile.Name == Label.CIC_Manager_Profile))
             {
               tempOption.add(new selectOption(Label.TalentCICInbox,Label.TalentCICInbox));
            }
            else
            {
                tempOption.add(new selectOption(UserInfo.getUserEmail(),UserInfo.getUserEmail()));
            }
            for(OrgWideEmailAddress eslist :owea) {
                  tempOption.add(new selectOption(eslist.Address,eslist.DisplayName+'<'+eslist.Address+'>'));
               }

            return tempOption;
        }
        set;
    }
    
    //EmailMessage related variables
    public List<EmailMessage> messagesOnCase {get;set;}
    public list<EmailMessagesWrapper> emailMessageList {get;set;}
    public boolean ShowEMpopup{get;set;}
    
 //****************************************************************************************************************
 //********************** send email method to send email**********************************************************   
    public PageReference SendEmail() {
    
         String s= ToFieldId;
         boolean edited = false; 
         Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
         toEmailAddress= new list<string>();
         list<String> AdditionalTo= new list<String>();
         List<Contact> conList = new List<Contact>();
         //message.setOrgWideEmailAddressId(SelectedFromId);
         
         //**********************  Setting TO Address . ************************************
         set<String> TempToEmails= new  set<string>();
         if(ShowAdditionalFieldValue.length()>0)
         {
         AdditionalTo= ShowAdditionalFieldValue.split(',');
          if(AdditionalTo.size()>0){
              TempToEmails.addAll(AdditionalTo);
             }
             toEmailAddress.AddAll(TempToEmails);
             //message.setToAddresses(toEmailAddress);
         }
         
   //**********************  Adding custom error message if To field is blank. ************************************   
         if(ToField =='' || ToField == null)
         { 
          ToFieldId='';
         }   
         /*if(ToFieldId.length()==0)
         {
            if(AdditionalTo.size()>0){
             conList = [SELECT Id,email from contact where email IN :toEmailAddress];
             if(!conList.isEmpty()){
                ToFieldId = conList[0].id;
             }else{
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Select at least one contact from either of To OR Additional to Lookup');
              ApexPages.addMessage(myMsg); 
              return null;
             }
            }else{
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SendEmail_To_fieldErrorMessage);
              ApexPages.addMessage(myMsg); 
              return null;
            }
         }*/
         if(ToFieldId=='' && AdditionalTo.isEmpty()){
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Select at least one contact from either of To OR Additional to Lookup');
              ApexPages.addMessage(myMsg); 
              return null;
         
         }
    
        
    //**********************  Setting BCC Address . ************************************
         
         if(BCcFieldValue.length()>0)
             {
                list<String> BCCField=BCcFieldValue.split(',');
                if(BCCField.size()>0){  
                        message.setBccAddresses(BCCField);
                  }
              }
   //**********************  Setting CC Address . ************************************
               
         if(CcFieldValue.length()>0)
             {
               list<String> CCField=CcFieldValue.split(',');
               if(CCField.size()>0){
                       message.setCcAddresses(CCField);
                   }
               }
   //******************************** Setting the thread ******************************
   //  list<EmailMessage> emailMessage=[select id,ReplyToEmailMessageId from EmailMessage where ParentId =: caseid];
     
    // String threadId="ref:_" & LEFT($Organization.Id,5) & SUBSTITUTE(RIGHT(Organization.Id,11), "0", "" )& "._" & LEFT(Id,5) & SUBSTITUTE(Left(RIGHT(Id,10), 5), "0", "") & RIGHT(Id,5) & ":ref";
    /* String OrgId=UserInfo.getOrganizationId();
     String EmailId=emailMessage[0].id;
     
     String ThreadId='ref:_'+OrgId.substring(0,5)+''+OrgId.substring((OrgId.length()-11),OrgId.length()).replaceAll('0','')+'._'+(EmailId.substring((EmailId.length()-10),EmailId.length()).substring(0,5)).replaceAll('0','')+''+EmailId.substring((EmailId.length()-5),EmailId.length())+':ref';
     system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  emailMessage >>>  '+ThreadId);
     message.setReferences(ThreadId); 
   */
     
   //**********************  Adding Custom message if there is no subject with the email. ******************
         
         if((templateId.length()==0)&&(subjectLine.length()==0))
         {
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SendEmail_subject_fieldErrorMessage);
              ApexPages.addMessage(myMsg); 
              return null;
         }
   
   //***************** Setting From Address ************************
   
        if(!fromAddressValues.isEmpty()){
            //message.setSenderDisplayName(SelectedFromId);
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: SelectedFromId];
            if ( owea.size() > 0 ) {
                message.setOrgWideEmailAddressId(owea.get(0).Id);
            }
        }
        
   //**********************  Setting Template,body if user selects template. ************************************
       /* if(templateId.length()>0){
            
            //Checking if body is changed
   
            EmailTemplate et = [Select id, body, DeveloperName,encoding, HtmlValue,TemplateType from EmailTemplate where id=: sEmailTemplateID]; 
            if(et.TemplateType == 'HTML') 
            { 
            message.setTemplateId(et.id); 
            } 
            
            //message.setTemplateId(templateId);
            message.setWhatId(RelatedToFieldId);
            message.setTargetObjectId(ToFieldId);    
          }
          else
          {*/
  //**********************  Setting subject, body if user don't select a template. ************************************
           //message.setWhatId(RelatedToFieldId);
           if(ToFieldId!=null && ToFieldId!=''){
           message.setTargetObjectId(ToFieldId);
           message.setWhatId(RelatedToFieldId);  
            } else{
               // message.setTargetObjectId(userInfo.getuserId());
                //message.setSaveAsActivity(false);
            } 
           subjectLine = subjectLine+' '+caseThreadId;
           message.setSubject(subjectLine);
           BodyContent = BodyContent +'\n\n'+ caseThreadId;
           message.setHtmlBody(BodyContent);
             if(BodyContent.length()==0)
             {
                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SendEmail_body_fieldErrorMessage);
                  ApexPages.addMessage(myMsg); 
                  return null;
              }
           //}
         
 //**********************  adding attachments to the email ************************************
        for(AttachmentList ListSelectedAttachment:ListAttachment )
        {
        if(ListSelectedAttachment.checkbox==true)
             {
                DocIds.add(ListSelectedAttachment.AttachmentId);
             }
        }
        if(DocIds.size()>0)
        {
            
           message.setDocumentAttachments(DocIds);
        }
     try{
     
 //**********************  Sending email ************************************
        message.setToAddresses(toEmailAddress);
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {message});
                if(tofieldId==''){
                system.debug('entered1');
                /*task t = new task();
                t.subject = subjectLine;
                t.OwnerId = userinfo.getuserId();
                t.status = 'Completed';
                t.WhatId = RelatedToFieldId;
                t.ActivityDate = system.today();
                t.recordtypeid = '01240000000DuEb';
                insert t;
                system.debug('entered2'+t);*/
                em = new EmailMessage();
                //em.ActivityId = t.id;
                system.debug('entered2'+message);
               
                if(message.getHtmlbody()!=null){
                string html=message.getHtmlbody();
                string result = html.replaceAll('<br/>', '\n');
                result = result.replaceAll('<br />', '\n');
                result = result.replaceAll('&nbsp;', '');
                result = result.replaceAll('&gt;', '');
                result = result.replaceAll('&lt;', '');
                //string HTML_TAG_PATTERN = '<.*?>';
                string HTML_TAG_PATTERN = '<[^>]+>|&nbsp;';
                pattern myPattern = pattern.compile(HTML_TAG_PATTERN);
                matcher myMatcher = myPattern.matcher(result);
                result = myMatcher.replaceAll('');
                em.htmlBody = result;
                em.TextBody = result;
                }
                if(message.getPlainTextbody() != null){
                em.TextBody = message.getPlainTextbody();}
                em.ParentId = RelatedToFieldId;
                em.Subject = subjectLine;
                em.MessageDate = system.now();
                if(message.getOrgWideEmailAddressId()!=null){
                    OrgWideEmailAddress[] oweaAdd = [select Id,Address,DisplayName from OrgWideEmailAddress where id =: message.getOrgWideEmailAddressId()];
                    em.FromAddress = oweaAdd[0].Address;
                    em.fromname = oweaAdd[0].DisplayName;
                }else{em.FromAddress = userinfo.getUseremail();
                      em.fromname = userinfo.getName();
                }
                if(message.getToAddresses()!=null){em.ToAddress = conListOfStr(message.getToAddresses());}
                if(message.getCCAddresses()!= null){em.CcAddress = conListOfStr(message.getCCAddresses());}
                if(message.getBccAddresses()!=null){em.BccAddress = conListOfStr(message.getBccAddresses());}
                
                
                system.debug('entered3');
            insert em;
            if(message.getDocumentAttachments()!=null){
                 system.debug('entered4'+em);
                List<Attachment> attOnEmail = new list<Attachment>();
                list<Document> DocToAtt=[select  id,name,ContentType,Type,Body FROM Document where id IN :message.getDocumentAttachments()];
                for(Document doc : DocToAtt){
                    Attachment newAtt = new Attachment();
                    newAtt.body = doc.body;
                    newAtt.ContentType = doc.ContentType;
                    newAtt.Name= doc.Name;
                    newAtt.parentid = em.id;
                    attOnEmail.add(newAtt);
                }
                if(!attOnEmail.isEmpty()){
                     system.debug('entered5'+attOnEmail.size());
                    insert attOnEmail;
                    system.debug('entered5'+attOnEmail);
                    //em.HasAttachment = true;
                    //update em;
                }
            }
           
          }
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, r+'>Message Id>'+message));   
     
         }catch(Exception e )
         {
          if(e.getMessage().contains('INVALID_EMAIL_ADDRESS'))
          {
                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SendEmail_Invalid_Email_Id);
                  ApexPages.addMessage(myMsg); 
                  return null;
          }else{
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
           return null;
           }           
         }  
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Support Request submitted successfully.')); 
        list<Document> DocToDelete=[select id from Document where id In : DocIds];
        delete DocToDelete;  
        pagereference PageRef = new pagereference('/'+caseid);
        PageRef.setRedirect(true);
        return PageRef;
    
    }
//**********************  Cancel method  ************************************
  

    public PageReference cancel() {
        pagereference PageRef = new pagereference('/'+caseid);
        PageRef.setRedirect(true);
        return PageRef;
    }

//**********************  Method to close Attachment Popup  ************************************
 
    public PageReference CloseAttachmentPopup() {
        ShowAttchPopUp=false;
        return null;
    }
     public PageReference CloseTempPopup() {
        ShowselectTemplatepopup=false;
        return null;
    }
    public PageReference CloseEMpopup() {
        ShowEMpopup=false;
        return null;
    }
//**********************  Method to open Attachment Popup  ************************************


    public PageReference OpenAttachmentPopup() {
        ShowAttchPopUp=true;
        return null;
    }
//**********************  Method to attach a file/ createing document  ************************************

    public PageReference save() {
      
    if(cDoc.Name!=null){
        cDoc.FolderId=Label.Attachment_Zip_Document_Folder_Id;
        insert cDoc;
        ShowAttachmentlist.add(cDoc.Name);
       // DocIds.add(cDoc.id);
        ListAttachment.add(new AttachmentList(true,cDoc.Name,cDoc.id));
        cDoc= new Document();
      }
    return null;
    }


//**********************  Method to attach a file/ createing document  ************************************
  public PageReference OpenSelectTemplatePopup() {
        ShowselectTemplatepopup=true;
        return null;
    }
    
  public PageReference OpenEMPopup() {
        ShowEMpopup=true;
        return null;
    }
 //**********************  Setting selected template on VF page  ************************************
  
   public PageReference SelectTemplate() {
            
            emailTemp = new EmailTemplate();
         for(EmailTemplate temp : [SELECT Id,Name,Body,subject,FolderId,HtmlValue,Encoding,TemplateType FROM EmailTemplate where id=: sEmailTemplateID ]){
              
               //Set<String> selTempdocIdList;
               system.debug('entered set'+selTempdocIdList);
               if(selTempdocIdList!=null && !selTempdocIdList.isEmpty()){
               system.debug('entered '+ListAttachment.size());
               if(ListAttachment.size()>0){
               system.debug('entered one');
               
               list<AttachmentList> TempAttList = new list<AttachmentList>();
                for(integer i=0;i<ListAttachment.size();i++){
                    system.debug('entered 2'+ListAttachment.size());
                    if(!selTempdocIdList.contains(ListAttachment[i].AttachmentId)){
                        TempAttList.add(ListAttachment[i]);
                        //ListAttachment.remove(i);
                        system.debug('entered 3'+ListAttachment.size());
                    }
                }
                ListAttachment.clear();
                if(!TempAttList.isEmpty()){
                    //ListAttachment.clear();
                    ListAttachment.addAll(TempAttList);
                }
                
               }
               selTempdocIdList.clear();
               }
               selTempdocIdList = new Set<String>();
               for(Attachment temAtt : [select id,body,ContentType,Name,parentid from Attachment where parentid =:temp.id]){
                //selTempdocIdList = new Set<String>();
                Document doc= new Document();
                doc.body = temAtt.body;
                doc.ContentType = temAtt.ContentType;
                doc.Name= temAtt.Name;
                doc.FolderId=Label.Attachment_Zip_Document_Folder_Id;
                insert doc;
                selTempdocIdList.add(doc.id);
                ListAttachment.add(new AttachmentList(true, doc.Name,doc.id));
                doc= new Document();               
               }
               Messaging.SingleEmailMessage tempMessage = new Messaging.SingleEmailMessage();
               tempMessage.setTemplateId(temp.id);
               //tempMessage.setTargetObjectId('003f000000DcI49'); 
               //tempMessage.setTargetObjectId('003f000000E15tM');  
               tempMessage.setWhatId(RelatedToFieldId);
               Savepoint sp = Database.setSavepoint();
               if(ToFieldId==''){
               contact c = new contact(recordtypeid='01240000000DuEW',lastname='.');
               c.email='test@gmail.com';
               //Savepoint sp = Database.setSavepoint();
               insert c;
               tempMessage.setTargetObjectId(c.id);
               }else{
               //templateid = ToFieldId.substring(0,15);
               system.debug('tofield'+ToFieldId.substring(0,15)+'&&'+ToFieldId);
               tempMessage.setTargetObjectId(ToFieldId.substring(0,15));}
               Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {tempMessage});
               
               //body = tempMessage.gethtmlbody();
               //subject=tempMessage.subject;
               //database.rollback(sp);
               
               
               if(temp.TemplateType=='HTML'){ BodyContent=tempMessage.gethtmlbody();
                //BodyContent=BodyContent.replaceAll(']]>','\n\n\n');
                //string tempBodyContent=EncodingUtil.urlEncode(temp.HtmlValue, temp.Encoding);
                //BodyContent='<html>'+EncodingUtil.urlDecode(tempBodyContent, temp.Encoding)+'</html>';
                subjectLine=tempMessage.subject;
                //templateId=temp.Id;
                //emailTemp.HtmlValue = temp.HtmlValue;
                //emailTemp.body = temp.body;
                }else if(temp.TemplateType=='Text')
                {
                BodyContent=tempMessage.getPlaintextBody();
                subjectLine=tempMessage.subject;
                //templateId=temp.Id;
                //emailTemp.body = temp.body;
                }
                 database.rollback(sp);          
                }
                ShowselectTemplatepopup=false;
                return null;
    }


//********************** Searching templates  ************************************
 

    public void Searchtemplfiles() {
        Templatelist = new list<EmailTemplate>();
                if(folderid!= null && folderid!='')
                    {
                       for(EmailTemplate et:[ SELECT Id,Name,Body,FolderId,Description,HtmlValue,TemplateType FROM EmailTemplate where FolderId=:folderid])
                       {
                         Templatelist.add(et); 
                        } 
                
                    }
          // return null;
        }


  
public list<AttachmentList> ListAttachment {get; set;}

 //********************** Default constructor  ************************************
   
 public WCT_sendEmailButtonPageController_clone()
 { 
    BCcFieldValue='';
    subjectLine='';
    CcFieldValue='';
    ShowAdditionalFieldValue='';
    ToFieldId='';
    ToField='';
    ToFieldEmail='';
    templateId='';
    cDoc= new Document();
    ShowselectTemplatepopup= false;
    ShowAttchPopUp=false;
    ShowEMpopup=false;
    ShowAttachmentlist= new list<String>();
    ListAttachment = new List<AttachmentList>();
    messagesOnCase = new List<EmailMessage>();
    emailMessageList = new list<EmailMessagesWrapper>();
    caseid=ApexPages.currentPage().getParameters().get('id') ; 
    messagesOnCase = [Select id,status,subject,Incoming,MessageDate from EmailMessage where parentId =:caseid];
    system.debug('****'+messagesOnCase.size());
    if(!messagesOnCase.isempty()){
        for(Emailmessage em:messagesOnCase){
            emailMessageList.add(new EmailMessagesWrapper(false,em.subject,em.status,em.id,em.MessageDate.format('MM/dd/yyyy h:mm a'))); 
            system.debug('&&&&'+em); 
        }
    }
    //caseid=ApexPages.currentPage().getParameters().get('id') ; 
    list<Case> cCurrentCase=[select RecordTypeId,WCT_Case_Thread_Id__c, CaseNumber,ContactId, Id from case where id =: caseid ];
    caseThreadId =  cCurrentCase[0].WCT_Case_Thread_Id__c;   
    system.debug('list_Case_attachment');
    String ISP =ApexPages.currentPage().getParameters().get('ISP');
        
        if(ISP=='Yes')
        {
            list<Interested_Party__c> lst_interdtedParty=[select id, WCT_Contact__r.Email from Interested_Party__c where WCT_Case__c =: caseid];
            //system.debug('>>>> Interested_Party__c  >>>>>>>>>>>>>>>>>>>>'+lst_interdtedParty[0].WCT_Contact__r.Email);
            for(Interested_Party__c loop_intPart : lst_interdtedParty)
            { 
                if(ShowAdditionalFieldValue=='')
                    {
                      ShowAdditionalFieldValue=loop_intPart .WCT_Contact__r.Email;
                    }else
                    {
                      ShowAdditionalFieldValue=ShowAdditionalFieldValue+','+loop_intPart .WCT_Contact__r.Email;
                    }
            
            }
             
        }
        
        
    
    if((cCurrentCase[0].RecordTypeId!=Case_TR_EM_RecordTypeId)&&(cCurrentCase[0].RecordTypeId!=Case_ELE_ES_RecordTypeId))
    {
      if(cCurrentCase[0].ContactId!=null){
      ToField= getContactName(cCurrentCase[0].ContactId, 'Name');
      ToFieldId=cCurrentCase[0].ContactId;
      ToFieldEmail= getContactName(cCurrentCase[0].ContactId, 'Email');
        }
    }else
    {
      ToField= '';
      ToFieldId='';
      ToFieldEmail='';
    }
    
    SelectedRetaedTovalue='case';
    RelatedToField=cCurrentCase[0].CaseNumber;
    RelatedToFieldId=caseid;
    folderid = UserInfo.getOrganizationId();
    Searchtemplfiles();
 }
  
 //********************** Method to get the contact name  ************************************
 
 public string getContactName(String contactId, String RetParameter)
     {
      list<Contact> contactTowork=[select name, id,Email from contact where id =:contactId];
      if(RetParameter=='Name'){
          return contactTowork[0].Name;
      }else
      {
       return contactTowork[0].Email;
      }
     }
     
     public void AttachementListFormCase()
     {
         if(!IsAttachmentRetrive){
          list<Attachment> list_Case_attachment=[select id, Name, Body  from Attachment where Attachment.ParentId =:caseid];
          for(Attachment at : list_Case_attachment){
           //  ShowAttachmentlist.add(at.Name);
           if(at.body!=null){
             Document doc= new Document();
             doc.Name=at.Name;
             doc.body=at.body;
             doc.FolderId=Label.Attachment_Zip_Document_Folder_Id;
             cTempDocToinsert.add(doc);
             }
          }
          list<WCT_Notes__c> list_Case_note=[select  WCT_Description__c, id, WCT_Subject__c  from WCT_Notes__c  where Case__c =:caseid];
           for(WCT_Notes__c  at : list_Case_note){
            if(at.WCT_Description__c!=null){
             //ShowAttachmentlist.add(at.WCT_Subject__c);
             Document doc= new Document();
             doc.Name=at.WCT_Subject__c;
             doc.body=Blob.valueof(at.WCT_Description__c);
             doc.FolderId=Label.Attachment_Zip_Document_Folder_Id;
             doc.ContentType='text/plain';
             doc.type = 'txt';
             cTempDocToinsert.add(doc);
             }
          }
          
          insert cTempDocToInsert;
          for(Document d: cTempDocToInsert)
          {
     //          DocIds.add(d.id);
             ListAttachment.add(new AttachmentList(false, d.Name,d.id));
           
          }
         cTempDocToInsert.clear();
         IsAttachmentRetrive=true;
         }
         
     }
     //Convert List of strings to string
     public string conListOfStr(list<string> strList){
        string retString = '';
        for(string s : strList){
            retString += s + ';'+'\n';
        }   
        return retString;
     }
     
     public class AttachmentList
     {
        public boolean checkbox{get;set;}
        public String  AttachmentName{get;set;}
        public String  AttachmentId{get;set;}
        
        public AttachmentList(Boolean isSelected, String Name,String id)
        {
             checkbox= isSelected;
             AttachmentName=name;
             AttachmentId= id;
             
        }
        
     } 
     
     public class EmailMessagesWrapper{
        public boolean isSelected {get;set;}
        public string subject {get;set;}
        public string status {get;set;}
        public string emid {get;set;}
        public string mdate {get;set;}
     
        public EmailMessagesWrapper(Boolean selected, String sub,String sta,string id,string dt){
            isSelected = selected;
            subject = sub;
            status = sta;
            emid = id; 
            mdate = dt;
        }    
     }
     
     public pagereference InsertEMAttachment(){
        for(EmailMessagesWrapper emwrap:emailMessageList){
            if(emwrap.isSelected){
            pageReference pdf = Page.WCT_EmailMessagePDF;
            pdf.getParameters().put('id',emwrap.emid);
            Blob body;
            try {
                body =  pdf.getContent();
            } catch (VisualforceException e) {
                body = Blob.valueOf('Some Text');
            }
            Document doc= new Document();
            doc.Name=emwrap.subject+'.pdf';
            doc.body=body;
            doc.FolderId=Label.Attachment_Zip_Document_Folder_Id;
            insert doc;
            ListAttachment.add(new AttachmentList(true, doc.Name,doc.id));  
            doc = new Document();
            }
        }
        ShowEMpopup=false;
        return null;
    }
}