@isTest
public class WCT_todLeaveForm_Test
{
    public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        rt=[select id from recordtype where DeveloperName = 'Emergency_Contact'];
        Contact conemer=WCT_UtilTestDataCreation.createEmployee(rt.id);
        conemer.WCT_Primary_Contact__c =con.id;
        datetime startdate=date.Today().adddays(-10);
        datetime enddate=date.Today().adddays(-1);
        Test.starttest();
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_LeavesForm;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);
        todLeaveForm controller=new todLeaveForm ();
        insert conemer;
        
        
        controller=new todLeaveForm ();
        controller.saveLeavesRecord();
        controller.loggedInContact.WCT_ExternalEmail__c='Testingleaves@test.com';
        controller.saveLeavesRecord();
        controller.objLeaves.WCT_Code_of_Ethics_Certification__c=true;
        controller.estimatedLastWorkDay=startdate.format('MM/dd/yyyy');
        controller.estimatedReturnWorkDay=enddate.format('MM/dd/yyyy');
        controller.personalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.personalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.personalUnpaidStartDate=startdate.format('MM/dd/yyyy');
        controller.personalUnpaidEndDate=enddate.format('MM/dd/yyyy');
        rt=[select id from recordtype where DeveloperName = 'Personal'];
        controller.strRecordType = rt.id;  
        controller.saveLeavesRecord();   
        controller.objLeaves.WCT_Leave_Reason__c='test';
        controller.doc=WCT_UtilTestDataCreation.createDocument();
        controller.uploadAttachment();
        controller.saveLeavesRecord();  
        controller.parentalChildDOB=enddate.format('MM/dd/yyyy');
        controller.parentalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalSTDStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalSTDEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalPaidParentalStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalPaidParentalEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalAdditionalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalAdditionalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalUnpaidStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalUnpaidEndDate=enddate.format('MM/dd/yyyy');
        rt=[select id from recordtype where DeveloperName = 'Parental'];
        controller.strRecordType = rt.id;  
        controller.saveLeavesRecord();  
        rt=[select id from recordtype where DeveloperName = 'Military'];
        controller.strRecordType = rt.id;  
        controller.saveLeavesRecord();  
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        controller.uploadAttachment(); 
        Test.stoptest();
    }
    /*public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createContact();
        con.LastName='Testing Approval';
        con.email='test@test.com';
        con.WCT_Visa_Type__c='temp';
        con.MobilePhone='123';
        con.recordtypeid=rt.id;
        insert con;
        rt=[select id from recordtype where DeveloperName = 'Emergency_Contact'];
        Contact conemer=WCT_UtilTestDataCreation.createContact();
        conemer.LastName='Testing Approval';
        conemer.email='testing@test.com';
        conemer.MobilePhone='123';
        conemer.WCT_Relationship__c='Friend';
        conemer.WCT_Primary_Contact__c =con.id;
        conemer.recordtypeid=rt.id;
        Test.starttest();
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_LeavesForm;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);
        todLeaveForm leaves=new todLeaveForm ();
        insert conemer;
        leaves=new todLeaveForm ();
        leaves.saveLeavesRecord();
        
        leaves.saveLeavesRecord();
        datetime estimatedLastWorkDay=date.Today().adddays(10);
        datetime estimatedReturnWorkDay=date.Today().adddays(20);
        leaves.estimatedLastWorkDay=estimatedLastWorkDay.format('MM/dd/yyyy');
        leaves.estimatedReturnWorkDay=estimatedReturnWorkDay.format('MM/dd/yyyy');
        leaves.objLeaves.WCT_Code_of_Ethics_Certification__c=true;
        rt=[select id from recordtype where DeveloperName = 'Personal'];
        leaves.strRecordType = rt.id;
        leaves.saveLeavesRecord();
        leaves.objLeaves.WCT_Leave_Reason__c='test';
        leaves.docIdList.add('test');
        Attachment a=WCT_UtilTestDataCreation.createAttachment(leaves.objLeaves.id);
        a.name='test';
        //insert a;
        Document d=WCT_UtilTestDataCreation.createDocument(); 
        todLeaveForm.AttachmentsWrapper ta = new todLeaveForm.AttachmentsWrapper(true,'',String.valueof(d.id),'2000');   
        leaves.UploadedDocumentList.add(ta); 
        leaves.doc=d;
        leaves.uploadAttachment();
        leaves.pageError=true;
        leaves.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        leaves.uploadAttachment();
        leaves.saveLeavesRecord(); 
        rt=[select id from recordtype where DeveloperName = 'Parental'];
        leaves.strRecordType = rt.id;
        leaves.saveLeavesRecord();
        leaves.parentalChildDOB=estimatedLastWorkDay.format('MM/dd/yyyy');
        leaves.parentalPaidParentalStartDate=estimatedLastWorkDay.format('MM/dd/yyyy');
        leaves.parentalPaidParentalEndDate=estimatedReturnWorkDay.format('MM/dd/yyyy');
       // leaves.objLeaves.WCT_Is_Employee_Child_Caregiver__c='Primary Caregiver';
        leaves.saveLeavesRecord(); 
        rt=[select id from recordtype where DeveloperName = 'Military'];
        leaves.strRecordType = rt.id;
        leaves.saveLeavesRecord();
        //leaves.objLeaves.WCT_Military_Branch__c='U.S. Army';
        //leaves.objLeaves.WCT_Military_Status__c='Active';
        leaves.saveLeavesRecord(); 
        test.stoptest();
    }*/
}