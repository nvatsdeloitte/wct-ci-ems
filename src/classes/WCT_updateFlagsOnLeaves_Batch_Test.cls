/**
    * Class Name  : WCT_updateFlagsOnLeaves_Batch_Test
    * Description : This apex test class will use to test the WCT_updateFlagsOnLeaves_Batch
*/
@isTest
private class WCT_updateFlagsOnLeaves_Batch_Test{

    public static List<WCT_Leave__c> leaveList = new List<WCT_Leave__c>();
    public static String empRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.Employee_RT).getRecordTypeId();
    public static String MilitaryRectypeId = Schema.SObjectType.WCT_Leave__c.getRecordTypeInfosByName().get(WCT_UtilConstants.Military_RT).getRecordTypeId();
     /** 
        Method Name  : createLeaves
        Return Type  : List<WCT_Leave__c>
        Type      : private
        Description  : Create Leave Records.         
    */
    private Static List<WCT_Leave__c> createLeaves()
    {
      Contact c = WCT_UtilTestDataCreation.createEmployee(empRecordTypeId);
      leaveList = WCT_UtilTestDataCreation.createLeaves(MilitaryRectypeId,c);
      insert leaveList;
      return leaveList;
    }
   
    /** 
        Method Name  : leaveBatchTestMethod
        Return Type  : void        
        Description  : Run Leaves Batch class WCT_updateFlagsOnLeaves_Batch.         
    */
    static testMethod void leaveBatchTestMethod(){
        Test.startTest();
        leaveList = createLeaves();
        WCT_updateFlagsOnLeaves_Batch leaveFlag = new WCT_updateFlagsOnLeaves_Batch();
        ID batchprocessid = DataBase.ExecuteBatch(leaveFlag,200);
        Test.stopTest();
    }
}