global class WCT_updateFlagsOnLeaves_schedule implements Schedulable
{
   global void execute(SchedulableContext sc) 
       {
            WCT_updateFlagsOnLeaves_Batch batch = new WCT_updateFlagsOnLeaves_Batch();
            ID batchprocessid = Database.executeBatch(batch,200); 
       }
}