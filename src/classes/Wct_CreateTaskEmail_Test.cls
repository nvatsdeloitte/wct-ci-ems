/************
* This class is coded to test Inbound Email Service "Wct_CreateTaskEmailinOutreachActivity"


*/

 @isTest
  public class Wct_CreateTaskEmail_Test
   {
    public static testmethod void Taskcreatedusinginbound()
    {
     Messaging.InboundEmail email = new Messaging.InboundEmail();
        email.plainTextBody = null;
        email.fromAddress='test@gmail.com';
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        
        Contact conRec = WCT_UtilTestDataCreation.createContactRec();
        insert conRec;
        
        WCT_Outreach__c outr= WCT_UtilTestDataCreation.createoutreach();
        insert outr;
         
        schema.WCT_Outreach_Activity__c outra= new  schema.WCT_Outreach_Activity__c();
        outra.WCT_Contact__c =conrec.id;
        outra.WCT_Outreach__c=outr.id;
        insert outra;
             
        WCT_Outreach_Activity__c e1=[select id from WCT_Outreach_Activity__c limit 1];
        email.Subject = 'RE: Sandbox: Ineligible to change this years Flexible Spending Account - Ref ; ['+String.valueOf(e1.id)+']';
        
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
     
        
        Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body = 'my attachment text';
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'texttwo/plain';
        email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
        
        Wct_CreateTaskEmailinOutreachActivity eventHandler = new Wct_CreateTaskEmailinOutreachActivity();     
                
        //run test
        Test.startTest();
        eventHandler.handleInboundEmail(email, envelope);
        email.Subject = 'RE: Sandbox: Ineligible to change this years Flexible Spending Account - Ref ; ['+String.valueOf(e1.id)+']';
        eventHandler.handleInboundEmail(email, envelope);
        
        Test.stopTest();
    }
    
    }