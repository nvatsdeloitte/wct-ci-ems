/************
* This Inbound Email Service will capture all the incoming emails and will create new Task


*/


  global class Wct_CreateTaskEmailinOutreachActivity implements Messaging.InboundEmailHandler {
 
   global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, 
                                                       Messaging.InboundEnvelope env){
 
    // Create an InboundEmailResult object for returning the result of the 
    // Apex Email Service
    Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
    String myPlainText= '';
    list<string> ls= new list<string>();
    String strTest = email.subject;
    String[] arrTest = strTest.split(';');
    for (string a:arrtest)
     {
      a = a.replace('[','');
      a = a.replace(']','');
      a =a.replace(' ','');
      ls.add(a);
     }
    myPlainText = email.plainTextBody;
    Task newTask = new Task ();
    
    try 
    {
    WCT_Outreach_Activity__c WOA= [select id, name,createdbyid from WCT_Outreach_Activity__c where id =:ls[1] ];
    Contact vCon = [SELECT Id, Name, Email FROM Contact WHERE Email = :email.fromAddress LIMIT 1];
        newTask.Description =  myPlainText;
        newTask.Priority = 'Normal';
        newTask.Subject = 'Not Started';
      //newTask.Status = 'In Progress';
        newTask.Subject = email.subject;
        newtask.email_status__c='Replied';
        //newtask.contact_email__c= email.fromaddress;
        newTask.OwnerId = woa.createdbyid;
        newTask.IsReminderSet = true;
        newTask.ReminderDateTime = System.now()+1;
        newTask.Whatid= WOA.id;
        newTask.WhoId =  vCon.Id;
     // Insert the new Task 
     insert newTask;    
     List<Attachment> attachmentList = new List<Attachment>();
       if(email.textAttachments != null){
       for (Messaging.InboundEmail.TextAttachment textAttachment : email.textAttachments) {
         Attachment attachment = new Attachment();
         attachment.Name = textAttachment.fileName;
         attachment.Body = Blob.valueOf(textAttachment.body);
         attachment.ParentId = newtask.Id;
         attachment.OwnerId = woa.createdbyid;
         attachmentList.add(attachment);
        }
      }
       if(email.binaryAttachments != null){
       for (Messaging.InboundEmail.BinaryAttachment binayAttachment : email.binaryAttachments) {
         Attachment attachment = new Attachment();
         attachment.Name = binayAttachment.fileName;
         attachment.Body = binayAttachment.body;
         attachment.ParentId = newtask.Id;
         attachment.OwnerId =woa.createdbyid;
         attachmentList.add(attachment);
        }
       }
      insert attachmentList;
     System.debug('New Task Object+++++++++++++++++++++++: ' + newTask );   
    }
    // If an exception occurs when the query accesses 
    // the contact record, a QueryException is called.
    // The exception is written to the Apex debug log.
   catch (QueryException e) {
       System.debug('Query Issue: ' + e);
   }
   
   // Set the result to true. No need to send an email back to the user 
   // with an error message
   result.success = true;
   
   // Return the result for the Apex Email Service
   return result;
  }
}