public class Wct_Create_Leave_from_Case {
   
    public string casecloseid= ApexPages.currentPage().getParameters().get('id');
    public string casecloseconid= ApexPages.currentPage().getParameters().get('conid');
    public list<recordtype> rectyp{get;set;}
    public String filterId {get;set;}

    public Wct_Create_Leave_from_Case(){
     rectyp=[SELECT Description,Name FROM RecordType where sobjecttype='wct_leave__c'];
    }

    public list<recordtype> getrec()
    {
     return rectyp;
    }
    
    public PageReference processRequests() {
     pagereference pg = new pagereference('/a0L/e?retURL=%2Fa0L%2Fo&RecordType='+filterId+'&ent=01I40000000MMwf&'+label.wct_Casetoclose+'='+casecloseid+'&'+label.wct_Casetoclosecon+'='+casecloseconid);
     return pg;
    }

    public pagereference reccancel(){
     pagereference pg = new pagereference('/a0L/o');
     return pg;
    } 
    
    public List<SelectOption> getleaverecd(){
     Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.wct_leave__c; 
     Map<String,Schema.RecordTypeInfo> contactRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
     List<SelectOption> options = new List<SelectOption>();
     for(String recordtypeName :contactRecordTypeInfo.keyset())
    {
      options.add(new SelectOption(contactRecordTypeInfo.get(recordtypeName).getRecordTypeId(),recordtypeName ));
    }
     return options;
    }
}