@istest
 Public class Wct_Create_Leave_from_Case_test{
  static testMethod void Createleaveandclosetask(){
   
   recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee' limit 1];
    
    Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
    con.Email='testclocsa@deloitte.com';
    con.WCT_Person_Id__c='101232';
    con.WCT_Cost_Center__c='42224';
    insert con;
    
    case cas=WCT_UtilTestDataCreation.createCase(con.id);
    insert cas;
    
    recordtype rtt=[select id from recordtype where DeveloperName = 'other' limit 1];
    
    wct_leave__c lea = new wct_leave__c(recordtypeid=rtt.id);
    insert lea;
    
    PageReference pageRef = Page.Wct_Create_Leave_from_Case;
    Test.setCurrentPage(pageRef);
    ApexPages.currentPage().getParameters().put('id',string.valueof(cas.id));
    
    Wct_Create_Leave_from_Case wctclfc = new Wct_Create_Leave_from_Case();
    wctclfc.processRequests();
    wctclfc.reccancel();
    wctclfc.getrec();
    wctclfc.getleaverecd();
    
 }
 
 }