@isTest
private class Wct_EmailMessage_Reopened_ImmiCase_Test {

    static testmethod void test_immigration(){
    recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
    Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
      con.Email='testimmi@deloitte.com';
      con.WCT_Person_Id__c='104032';
      con.WCT_Cost_Center__c='42324';
      insert con;
     
    Case c = new  Case(); 
     c.ContactId =  con.id ;
     c.WCT_Category__c = 'Benefits'; 
     c.subject = 'Test Case';
     c.description= 'Test Descriptiopn'; 
     c.Origin='GMI - Immigration';
     c.Status ='Resolved';
     c.wct_category__c='Technology Issue - Talent';
     c.WCT_Requisition_FSS__c ='test';
     c.Priority='1-High';
     c.WCT_Requisition_Service_Area__c='testdata';
     insert c;
     
    Case c1=[select id, ContactId from Case where Id =:c.id];
     c1.ContactId =null;
     Update c1;
 
   Profile p = [SELECT Id FROM Profile WHERE name ='system administrator']; 
    User u2 = new User(Alias = 'newimmi', Email='newimmigration@testorg.com', 
              EmailEncodingKey='UTF-8', LastName='pegaTesting', LanguageLocaleKey='en_US', 
              LocaleSidKey='en_US', ProfileId = p.Id, 
              TimeZoneSidKey='America/Los_Angeles', UserName='newuserpeg@testorg.com');
    System.runAs(u2) { 
      emailmessage em= new emailmessage();
      em.parentid=c.id;
      em.status='0';
      em.subject='test';
      insert em;
      }
     }
    }