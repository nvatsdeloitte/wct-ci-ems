global class Wct_OfferApproval_ConsolidatedEmail implements Database.Batchable<sObject>,Schedulable{

    global void execute(SchedulableContext SC) {
        Wct_OfferApproval_ConsolidatedEmail batch = new Wct_OfferApproval_ConsolidatedEmail();
        ID batchprocessid = Database.executeBatch(batch,100); 
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'SELECT Id,Name,WCT_Candidate__c,WCT_Team_Mailbox__c,WCT_RMS_ID__c,WCT_Full_Name__c,WCT_status__c,WCT_Recruiter_Email__c,WCT_Candidate_Email__c,WCT_Offer_RFR_Time__c FROM WCT_Offer__c where WCT_status__c = \'Ready for Review\' and WCT_Recruiter_Email__c != null and WCT_Offer_RFR_Time__c != null';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        set<string> setRecEmails = new set<string>();
        set<string> setteamEmails = new set<string>();
        map<string,string> RecTeamEmails = new map<string,string>();
        Map<String, List<WCT_Offer__c>> recOffers = new Map<String, List<WCT_Offer__c>>(); 
        Decimal seconds;
        Decimal hrs;
            
        for(sObject tmp: scope) {
            WCT_Offer__c offerRecord = (WCT_Offer__c) tmp;
            seconds = BusinessHours.diff(Label.WCT_Default_Business_Hours_ID, offerRecord.WCT_Offer_RFR_Time__c, System.now())/ 1000;
            hrs = seconds / 3600;
            system.debug('hrs:value'+hrs);
            system.debug('seconds :value'+seconds );
            if(hrs > 24) {        
                setRecEmails.add(offerRecord.WCT_Recruiter_Email__c);
                RecTeamEmails.put(offerRecord.WCT_Recruiter_Email__c,offerRecord.WCT_Team_Mailbox__c);
                
                List<WCT_Offer__c> offerList = recOffers.get(offerRecord.WCT_Recruiter_Email__c);
                if(offerList == null) {
                    offerList = new List<WCT_Offer__c>();
                } 
                offerList.add(offerRecord);
                recOffers.put(offerRecord.WCT_Recruiter_Email__c,offerList);
            }
        }

        String strEmailTop ='';
        String strEmailBody ='';
        String strEmailBottom ='';
        
        strEmailTop  += '<!DOCTYPE html> <html> <head> <style> table,th,td { border: 1px solid black; border-collapse:collapse; }, th,td { padding:5px; }, </style> </head> <body> <br> Dear Recruiter,<br> <br> Offer Letter(s) are pending for more than 24 Hours.<br> <br> <br> <table> <thead> <tr> <th>Offer Name</th> <th>Candidate Name</th> <th>RMS ID</th> <th>Candidate Email</th> <th>Offer letter link</th> </tr> </thead> <tbody>';
        
        strEmailBottom += '</tbody> </table> <br> Thank you,<br> Deloitte Recruting.<br> </body> </html>';

        for(string strRecEmail: recOffers.keyset()) {    
            strEmailBody ='';        
            for(WCT_Offer__c offerRecord : recOffers.get(strRecEmail)) {
                    strEmailBody += '<tr><td>'+offerRecord.Name+'</td><td>'+offerRecord.WCT_full_Name__c+'</td> <td>'+offerRecord.WCT_RMS_ID__c+'</td>';
                    strEmailBody += '<td>'+offerRecord.WCT_Candidate_Email__c+'</td> <td><a href='+Label.BaseURL+'/'+offerRecord.id+'>URL</a></td></tr>';
            }
            
            List<string> ToEmailAddress = new List<string>();
            List<string> ToCcAddress = new List<string>();
        
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();                 
            ToEmailAddress.add(strRecEmail);
            if(RecTeamEmails != null && RecTeamEmails.get(strRecEmail) != null){
                 ToCcAddress.add(RecTeamEmails.get(strRecEmail));
            }
            
            mail.settoAddresses(ToEmailAddress);
            mail.setccAddresses(ToCcAddress);
            mail.setSubject('Immediate Action: Offer Letter(s) are waiting for Approval');
            mail.setHTMLBody(strEmailTop+strEmailBody+strEmailBottom);
            mail.setSaveAsActivity(false);
            if(ToEmailAddress != null){
                Messaging.sendEmail(new Messaging.SingleEmailMessage [] {mail});
            }
        }
   }

   global void finish(Database.BatchableContext BC){
      
   }
}