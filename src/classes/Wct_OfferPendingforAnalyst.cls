global class Wct_OfferPendingforAnalyst implements Database.Batchable<sObject>,Schedulable{

    global void execute(SchedulableContext SC) {
        Wct_OfferPendingforAnalyst batch = new Wct_OfferPendingforAnalyst();
        ID batchprocessid = Database.executeBatch(batch,100); 
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String strSql = 'SELECT Id,Name,Owner.Email,WCT_Candidate_Email__c,WCT_Team_Mailbox__c,WCT_RMS_ID__c,WCT_Full_Name__c,WCT_status__c,WCT_AgentState_Time__c' +
                     ' FROM WCT_Offer__c'+
                     ' where WCT_status__c IN (\'Offer to Be Extended\', \'Draft in Progress\', \'Returned for Revisions\')'+
                     ' AND owner.email != null AND WCT_AgentState_Time__c != null'; 
        system.debug('query123:'+strSql);
        return database.getQuerylocator(strSql);
        
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        set<string> setRecEmails = new set<string>();
        set<string> setteamEmails = new set<string>();
        map<string,string> RecTeamEmails = new map<string,string>();
        Map<String, List<WCT_Offer__c>> recOffers = new Map<String, List<WCT_Offer__c>>(); 
        Decimal seconds;
        Decimal hrs;
        
        for(sObject tmp : scope) {
             WCT_Offer__c offerRecord = (WCT_Offer__c) tmp;
             seconds = BusinessHours.diff(Label.WCT_Default_Business_Hours_ID, offerRecord.WCT_AgentState_Time__c, System.now())/ 1000;
             hrs = seconds / 3600;
             if(hrs > 4) {
                setRecEmails.add(offerRecord.Owner.Email);
                RecTeamEmails.put(offerRecord.Owner.Email,offerRecord.WCT_Team_Mailbox__c);
                system.debug('RecTeamEmails:'+RecTeamEmails);
                List<WCT_Offer__c> offerList = recOffers.get(offerRecord.Owner.Email);
                if(offerList == null) {
                    offerList = new List<WCT_Offer__c>();
                } 
                offerList.add(offerRecord);
                recOffers.put(offerRecord.Owner.Email,offerList);
            }
        }
        
        String strEmailTop ='';
        String strEmailBody ='';
        String strEmailBottom ='';
        
        strEmailTop  += '<!DOCTYPE html>';
        strEmailTop  += '<html> <head> <style>';
        strEmailTop  += 'table,th,td';
        strEmailTop  += '{ border: 1px solid black; border-collapse:collapse; }, th,td';
        strEmailTop  += ' { padding:5px; }';
        strEmailTop  += '</style> </head> <body> <br> Dear Analyst,<br> <br>';
        strEmailTop  += 'Offer Letter(s) are pending for more than 4 Hours.<br> <br> <br> <table> <thead>';
        strEmailTop  += ' <tr> <th>Offer Name</th> <th>Candidate Name</th> <th>RMS ID</th> <th>Candidate Email</th> <th>Status</th> <th>Offer letter link</th>';
        strEmailTop  += '</tr> </thead> <tbody>';
        
        strEmailBottom += '</tbody> </table> <br> Thank you,<br> Deloitte Recruting.<br> </body> </html>';
        
        for(string strRecEmail: recOffers.keyset()) {
            strEmailBody ='';
            for(WCT_Offer__c offerRecord : recOffers.get(strRecEmail)) {
            
                    strEmailBody += '<tr> <td>'+offerRecord.Name+'</td> <td>'+offerRecord.WCT_full_Name__c+'</td><td>'+offerRecord.WCT_RMS_ID__c+'</td><td>'+offerRecord.WCT_Candidate_Email__c+'</td>';
                    strEmailBody += '<td>'+offerRecord.WCT_status__c+'</td><td><a href='+Label.BaseURL+'/'+offerRecord.id+'>URL</a></td></tr>';
            }
            
            list<string> ToEmailAddress = new list<string>();
            List<string> ToCcAddress = new List<string>();
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            ToEmailAddress.add(strRecEmail);
            system.debug('strRecEmail1:'+strRecEmail);
            if(RecTeamEmails != null && RecTeamEmails.get(strRecEmail) != null){
                 ToCcAddress.add(RecTeamEmails.get(strRecEmail));
            }
            system.debug('strRecEmail:'+strRecEmail);
            system.debug('ToCcAddress:'+ToCcAddress);
            
            mail.settoAddresses(ToEmailAddress);
            system.debug('ToEmailAddress:'+ToEmailAddress);
            mail.setccAddresses(ToCcAddress);
            system.debug('ToCcAddress1:'+ToCcAddress);
            mail.setSubject('Immediate Action: Offer Letter(s) are pending');
            mail.setHTMLBody(strEmailTop+strEmailBody+strEmailBottom);
            mail.setSaveAsActivity(false);
            if(ToEmailAddress != null){
                Messaging.sendEmail(new Messaging.SingleEmailMessage [] {mail});
            }
        }
    }

    global void finish(Database.BatchableContext BC){
      
    }
}