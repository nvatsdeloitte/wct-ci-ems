@isTest
public class Wct_OfferPendingforAnalyst_Test{
    static Id employeeRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
    //static Id candidateRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('candidate').getRecordTypeId();
    static testMethod void Wct_OfferPendingforAnalyst_TestMethod (){
    
     Profile prof = [select id from profile where name='system Administrator'];
     User usr = new User(alias = 'usr', email='us.name@vmail.com',
                emailencodingkey='UTF-8', lastname='lstname',
                timezonesidkey='America/Los_Angeles',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = prof.Id,
                username='testuser128@testorg.com');
                insert usr;
    
       Account acc = new Account();
       acc.Name='Test';
       Insert acc;
       system.debug('acc values ----'+acc);
       
       Contact con = new Contact ();
       con.AccountId= acc.Id;
       con.LastName='tec';
       con.Email='user@gmail.com';
       Insert con;
       system.debug('con values ----'+con);
       
        
        set<string> setRecEmails = new set<string>();
        Map<String, List<WCT_Offer__c>> recOffers = new Map<String, List<WCT_Offer__c>>(); 
        
    
       WCT_Offer__c  objCusWct= new WCT_Offer__c();
        
        objCusWct.WCT_status__c = 'Offer to Be Extended';
        objCusWct.WCT_Candidate__c = con.Id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.OwnerId = usr.id;
        objCusWct.WCT_AgentState_Time__c = Date.today();
        objCusWct.WCT_Basic__c = '200';
        objCusWct.WCT_Annual_LTA__c='200';
        objCusWct.WCT_Annual_Provident_Fund__c='1200';
        objCusWct.WCT_Annual_Spcl_Allowance__c='800';
        objCusWct.WCT_Lookup_Code__c='1442';
        objCusWct.WCT_Internship_End_Date__c=Date.today();
        objCusWct.WCT_Last_Name_INTERN__c='';
        insert objCusWct;
        system.debug('objCusWct values ----'+objCusWct);
        

        Test.StartTest();
        
        Wct_OfferPendingforAnalyst objBatch = new Wct_OfferPendingforAnalyst();
        ID batchprocessid = Database.executeBatch(objBatch);
        
        Test.StopTest();
          
    }
}