@isTest
public class Wct_OfferSent_ReminderEmail_test{
    static Id candidateRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Candidate').getRecordTypeId();
   
    public static testMethod void test() {
    
    Profile prof = [select id from profile where name='system Administrator'];
     User usr = new User(alias = 'usr', email='us.name@vmail.com',
                emailencodingkey='UTF-8', lastname='lstname',
                timezonesidkey='America/Los_Angeles',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = prof.Id,
                username='testuser128@testorg.com');
                insert usr;
    
       Account acc = new Account();
       acc.Name='Test';
       acc.phone='123456790';
       Insert acc;
       system.debug('acc values ----'+acc);
       
       Contact con = new Contact ();
       con.AccountId= acc.Id;
       con.LastName='tec';
       con.Email='user@gmail.com';
       con.WCT_User_Group__c = 'US GLS India';
       Insert con;
      
       List<WCT_Offer__c> off=[select id from WCT_Offer__c];
       System.assert(off.size()==0);
    
        WCT_Offer__c  objCusWct= new WCT_Offer__c();
        objCusWct.WCT_status__c = 'Offer sent';
        objCusWct.WCT_Candidate__c = con.Id;
        objCusWct.OwnerId = usr.id;
        insert objCusWct;
        
        system.debug('objCusWct'+objCusWct); 
        datetime dt5 = system.now();
        objCusWct.WCT_Offer_Sent_Date__c = dt5.addDays(-3);
        update objCusWct;
       
        Test.StartTest();
        Wct_OfferSent_ReminderEmail objBatch = new Wct_OfferSent_ReminderEmail();
        ID batchprocessid = Database.executeBatch(objBatch);
        Test.StopTest();
        
    }
}