@isTest
private class Wct_Update_LeaveChild_Status_Test {

static testmethod void test_trigger()
   {
    Talent_Delivery_Contact_Database__c t = new Talent_Delivery_Contact_Database__c ();
    t.name='42324';
    t.India_Talent_Contact_1__c='test@gmail.com';
    t.India_Talent_Contact_2__c='test@gmail.com';
    t.Us_Talent_Contact_1__c ='USteam@gmail.com';
    t.us_Talent_Contact_1__c='USTeam@gmail.com';
    t.Payroll_Contact_Email__c='Teamtest@gmail.com';
    insert t;
    recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
    Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
    con.Email='testoast@deloitte.com';
    con.WCT_Person_Id__c='104032';
    con.WCT_Cost_Center__c='42324';
    insert con;
    WCT_Leave__c wl = new WCT_Leave__c ();
    wl.WCT_Employee__c =con.id;
    wl.India_Talent_Contact_1__c=t.India_Talent_Contact_1__c;
    wl.India_Talent_Contact_2__c=t.India_Talent_Contact_2__c;
    wl.Us_Talent_Contact_1__c =t.US_Talent_Contact_1__c;
    wl.us_Talent_Contact_1__c=t.US_Talent_Contact_2__c;
    wl.Payroll_Contact_Email__c= t.Payroll_Contact_Email__c;
    insert wl;
    WCT_Leave__c wl1 = new WCT_Leave__c ();
    wl.WCT_Employee__c =null;
     wl1.India_Talent_Contact_1__c='';
    wl1.India_Talent_Contact_2__c='';
    wl1.Us_Talent_Contact_1__c ='';
    wl1.us_Talent_Contact_1__c='';
    wl1.Payroll_Contact_Email__c= '';
    insert wl1;
   
   }

static testmethod void Child_Status_Depending_on_Parental_status()
   {
    recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
    Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
    con.Email='testoast@deloitte.com';
    con.WCT_Person_Id__c='104032';
    con.WCT_Cost_Center__c='42324';
    insert con;
    WCT_Leave__c wl = new WCT_Leave__c ();
    wl.WCT_Employee__c =con.id;
    wl.WCT_Leave_Status__c= 'new';
    insert wl;

    wl.WCT_Leave_Status__c= 'Closed';
    update wl;
    WCT_Leave__c wl1 = new WCT_Leave__c ();
    wl1.WCT_Employee__c =con.id;
    wl1.WCT_Leave_Status__c= 'new';
    wl1.WCT_Related_Record__c=wl.id;
    insert wl1;
    
    wl1.WCT_Leave_Status__c= 'Closed';
    update wl1;
    
    WCT_Leave__c wl2 = new WCT_Leave__c ();
    wl2.WCT_Employee__c =con.id;
    wl2.WCT_Leave_Status__c= 'Cancelled';
    insert wl2;
    
    WCT_Leave__c wl3 = new WCT_Leave__c ();
    wl3.WCT_Employee__c =con.id;
    wl3.WCT_Related_Record__c=wl2.id;
    insert wl3;
    
    wl3.WCT_Leave_Status__c= 'Cancelled';
    update wl3;
   }

static testmethod void Create_Task_When_Leaves_created_with_TOD_Profile()
   {
    recordtype rtask = [select id, name from recordtype where name='task'];
    recordtype rt = [select id, name from recordtype where name='personal' ];
    profile pro=[select id from profile where name ='ToD Profile'];
    user u = [select id from user where profileid=:pro.id limit 1];
    Profile p = [SELECT Id FROM Profile WHERE name ='ToD Profile']; 
    User u2 = new User(Alias = 'newUser', Email='newusers@testorg.com', 
             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
             LocaleSidKey='en_US', ProfileId = p.Id, 
             TimeZoneSidKey='America/Los_Angeles', UserName='newusers@testorg.com');
    System.runAs(u2)
    {
        WCT_Leave__c wl = new WCT_Leave__c ();
        wl.RecordTypeid= rt.id;
        wl.WCT_Related_Record__c=wl.id;
        insert wl;
        task t = new task();
        t.recordtypeid = rtask.id;
        //t.ownerid=u.id;
        t.subject = wl.name+ ' - Emergency Leave Request ';
        t.Priority='High';
        t.ActivityDate=system.today();
        t.whatid=wl.id;
        t.status= 'Not Started';
        t.isreminderset= true;
        t.ReminderDateTime= system.today();
        insert t;
    }
   }
  }