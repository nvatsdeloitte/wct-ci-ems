public class Wct_eEvents_AR_Eventcheckin {

 
public datetime siteenddate {get;set;}
public datetime sitestartdate {get;set;}
public boolean Thankyoumess { get; set; }
public event__c event { get; set; }
public string lastsearchName{get;set;}
Public boolean mainpage {get;set;}
Public boolean nomatches {get;set;}
Public boolean dateerror {get;set;}
Public boolean Resultspage {get;set;}
public String eid = ApexPages.currentPage().getParameters().get('eid');
public String searchlastname { get; set; }
List<EventRegistration> ERList{get;set;}
List<Event_Registration_Attendance__c> selectedERs = new List<Event_Registration_Attendance__c>();

    public Wct_eEvents_AR_Eventcheckin() 
    {
    Thankyoumess=false;
    nomatches=false;
    mainpage = true;
    Resultspage= false;
    dateerror= false;
    event=[select id,name,venue_city_AR__c,venue_state_AR__c,School__c,AR_Start_Date_Time_Text__c,AR_End_Date_Time_Text__c,recordtype.name,AR_image_for_registration_page__c,Start_Date_Time__c,NIR_Venue__c from event__c where id=:eid ];
    List <String> stringParts = event.AR_Start_Date_Time_Text__c.split(' ');
            sitestartdate=date.parse(stringParts[0]);
            List <String> stringParts1 = event.AR_end_Date_Time_Text__c.split(' ');
            siteenddate=date.parse(stringParts1[0]);
    }
    public PageReference backb() 
     {
        pagereference pg = new pagereference('/apex/Wct_eEvents_AR_Eventcheckin?eid='+eid);
        pg.setRedirect(true);

        return pg;
     }
    // first pb table
    public PageReference ers() 
     {
        getERs();
        return null;
     }
    public List<EventRegistration> getERs()
     {
      mainpage = false;
       Resultspage= true;
       Thankyoumess=false;
       dateerror= false;
       nomatches=false;
        ERList=new List<EventRegistration >();
        date d = system.today();
        lastsearchName = '%' + searchlastname + '%';
        for(Event_Registration_Attendance__c ER : [select Id,attended__c,Attended_Status__c, contact__r.Email,Current_Title__c,Curent_Employer__c,contact__r.FirstName,contact__r.LastName from Event_Registration_Attendance__c  where contact__r.LastName Like:lastsearchName and event__c=:eid    ])
        ERList.add(new EventRegistration(ER));
        if(ERlist.size()==0)
        nomatches=true;
        return ERList;
     }
 
    //checkbox selection
    public PageReference getSelected()
     {
       selectedERs.clear();
      
       for(EventRegistration ERwrapper : ERList)
       if(ERwrapper.selected == true)
        {
        selectedERs.add(ERwrapper.ERt);
        GetSelectedErs();
        }
        Thankyoumess=true;
     mainpage=false;
     resultspage=false;
     dateerror= false;
      nomatches=false;
        return null;

     }
 
    
 //selected table
    public List<Event_Registration_Attendance__c> GetSelectedErs()
    {
    list<Event_Registration_Attendance__c> upER= new list<Event_Registration_Attendance__c>();
     if(selectedErs.size()>0)
    {
    
    list<Event_Registration_Attendance__c> LERA= [select id,event__r.End_Date_Time__c from Event_Registration_Attendance__c where id IN: selectedErs ];
     
    for(Event_Registration_Attendance__c ERA : LERA)
     {
     ERA.attended__c= true;     
     upER.add(ERA);
    
     }
     update upER;
     
     }    
     return null;
 
    }  
 
 //Inner Class
    public class EventRegistration
    {
        public Event_Registration_Attendance__c Ert{get; set;}
        public Boolean selected {get; set;}
        public EventRegistration(Event_Registration_Attendance__c a )
         {
             ERt = a;
             selected = false;
 
        }
 
    }

    public string getDocumentImageUrl()
    {
    
    
    Map<String,eEvent_AR_Images__c> allStates = eEvent_AR_Images__c.getAll();
    
    if(event.AR_image_for_registration_page__c<>null)
    {List<Document> lstDocument = [Select Id,Name from Document where id =: allStates.get(event.AR_image_for_registration_page__c).eEvent_AR_Images_ID__c limit 1];
    
    string strOrgId = UserInfo.getOrganizationId();
    string strDocUrl = '/servlet/servlet.ImageServer?oid=' + strOrgId + '&id=';
    return strDocUrl + lstDocument[0].Id;
    }
    
    else{
     string strOrgId = UserInfo.getOrganizationId();
    string strDocUrl = '/servlet/servlet.ImageServer?oid=' + strOrgId + '&id=';
    return strDocUrl + allStates.get('Tree branches growing.jpeg').eEvent_AR_Images_ID__c;}
    }

}