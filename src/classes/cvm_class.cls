public class cvm_class {

    public case caseObj{get;set;}
    public string strings{get;set;}
    public string emValue {get;set;}
    public contact conObj{get;set;}
    public boolean bCreateRecords{get;set;}
    public CVM_Contractor_Request__c cvmObj{get;set;}
    public CVM_Contractor_Record__c crecObj {get;set;}
    /* Cont Rec */
    wrapper objwrapper;
    public String appId;
    public CVM_Contractor_Record__c delRec;
    public List < CVM_Contractor_Record__c > delRecList;
    List < wrapper > lstwrapper = new List < wrapper > ();
    public boolean newRec;
    public integer rowIndex{get;set;}
    public Integer counter{get;set;}
    public List <wrapper> lst {get;set;}
    
    public cvm_class(){
         emValue = ApexPages.currentPage().getParameters().get('em');
         string userEmail = cryptoHelper.decrypt(emValue);
         conObj=new contact();
         bCreateRecords=false;
         try {
             conObj = [select id,name, email from contact where email = : userEmail limit 1];
             if(conObj!=null){
                 cvmObj= new CVM_Contractor_Request__c();
                 crecObj = new CVM_Contractor_Record__c();
                 caseObj = new Case();
                 caseObj.contactId = conObj.id;
             }
        
        /* Cont Rec */
        newRec = false;
        delRecList = new List < CVM_Contractor_Record__c > ();
        lst = new list < wrapper > ();
        counter = 0;

          
             
             
         }
         catch(Exception e)
        {
          System.debug('----Exception---'+e.getMessage()+'-----at Line #----'+e.getLineNumber());
        }
         
       
    }
    
    public void SetContractorRecord() {
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('bCreateRecords'))){
    string IsCreateContractRecs=apexpages.currentpage().getparameters().get('bCreateRecords');
    if(IsCreateContractRecs=='true'){
     bCreateRecords=true;
     AddRow();  
    }
    else{
     bCreateRecords=false;
     lst=new List<wrapper>();
    }
    }
    }
    
    public PageReference saveAction() {

        try{
        
            caseObj.Priority = ' 3- Medium';
            caseObj.status = 'New';
            caseObj.Origin = 'Phone';
            caseObj.WCT_Category__c = 'ELE';
            caseObj.subject = 'Contractor & Vendor Management';
            caseObj.description = 'Contractor & Vendor Management- Contractor Record Creation';
            insert caseObj;
            if(caseObj.Id!=null){
            Task objTask=new Task(
                ActivityDate = Date.today(),
                Subject='Contractor & Vendor Management',
                OwnerId ='00540000002DsNU' ,//UserInfo.getUserId()
                WhatId=caseObj.Id,
                Status='In Progress');
                
                insert objTask;
            }
            system.debug('----CaseId---'+caseObj);
            cvmObj.CVM_Case__c= caseObj.id;
            insert cvmObj;
            system.debug('---------bCreateRecords---------'+bCreateRecords);
            system.debug('---------lst---------'+lst);
            if(bCreateRecords)
            {
            List<CVM_Contractor_Record__c> lstConRec=new List<CVM_Contractor_Record__c>();
            CVM_Contractor_Record__c objConRec;
             for(wrapper objWrap:lst){
             objConRec= new CVM_Contractor_Record__c();
             objConRec=objWrap.app;
             objConRec.CVM_Request_ID__c = cvmObj.id;
             lstConRec.add(objConRec);
             }
             upsert lstConRec;
               
            }
        PageReference objPageRef = new PageReference('/apex/trtprojectthankyou');
        objPageRef.setRedirect(true);
        return objPageRef;  
        }
        catch(Exception e)
        {
          System.debug('----Exception---'+e.getMessage()+'-----at Line #----'+e.getLineNumber());
          return null;
        }

    }

    /* Contractor Record Methods */
    

   
    public pagereference delmethod() {
        if(rowIndex!=null){
        try{
        list < wrapper > lstWrapClone = new list < wrapper > ();
        
        lst.remove(rowIndex);
        lstWrapClone=lst;
        lst = new list < wrapper > ();
        integer lp=0;
        for(wrapper objWrap:lstWrapClone){
        objWrap.rowNo=lp;
        lst.add(objWrap);
        lp++;
        }
        }
        catch(Exception ex){
        }
        }
        return null;
    }
   

    public PageReference AddRow() {

        objwrapper = new wrapper(new CVM_Contractor_Record__c());
        counter++;
        objwrapper.counterWrap = counter;
        objwrapper.isEdit = true;
        objwrapper.rowNo = lst.size();
        lst.add(objwrapper);
        newRec = true;
        return null;
    }
    
    public class wrapper {
        public CVM_Contractor_Record__c app {get;set;}
        public integer rowNo {get;set;}
        public boolean isEdit{get;set;}
        public Integer counterWrap{get;set;}
        public wrapper(CVM_Contractor_Record__c app) {
            this.app = app;

        }
    }
    
    
}