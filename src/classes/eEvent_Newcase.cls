/* Developer Name : Shareef (asyed@deloitte.com)
   This is to create a new case when clicked button which is hosted on event records  */


public with sharing class eEvent_Newcase {
 public case ocase {get;set;}
 
 public List<Event__c> Event{get; set;}
 public List<Contact> contacts{get; set;}
 public List<RecordType> recselection{get;set;}
 public integer  requestType{get;set;}
  public eEvent_Newcase() {
    ocase = new  Case();
    recselection= new List<RecordType>();
    Event= New List<Event__c>();
    contacts= new List<Contact>();
    requestType=validateRequest();
      if(requestType==1)
      {
        recselection =[Select id,Name from RecordType where Name=:ApexPages.currentPage().getParameters().get('rectype')];
        contacts=[select id,Name from contact where id=:ApexPages.currentPage().getParameters().get('contid')]; 
     }
     else if(requestType==2)
      {
          recselection =[Select id,Name from RecordType where Name=:ApexPages.currentPage().getParameters().get('rectype')];
          Event=[select id,Name from Event__c where id=:ApexPages.currentPage().getParameters().get('compid')];
      }
  
    
    Ocase.WCT_Category__c='Alumni Relations';
    Ocase.WCT_SubCategory1__c='eEvent Image for Registration';
    Ocase.Subject='AR eEvent - Add New Image for Registration';
    Ocase.Description='Please add attached image for Registration to AR eEvent object.';
       
     
    }
    
 
    public integer validateRequest()
    {
        integer status = 0;
        if(ApexPages.currentPage().getParameters().get('actionType')!=null)
        {
            if(ApexPages.currentPage().getParameters().get('actionType')=='new')
            {
                if(ApexPages.currentPage().getParameters().get('contid')!=null )
                {
                    status = 1;
                }
            }
           
            
        }
        
        return status;
    }
    
    public pageReference Save()
    {
        insert ocase;
        return new PageReference('/'+ocase.id);
    }

}