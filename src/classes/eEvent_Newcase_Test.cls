@isTest
public class eEvent_Newcase_Test {

    
     public static  testmethod void test1()
    {
         // set up some test data to work with
        Event__c Event = new Event__c(Name='Test', School__c='Test');
        insert Event;
       
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Contact; 
        Map<String,Schema.RecordTypeInfo> contactRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = contactRecordTypeInfo .get('Employee').getRecordTypeId();
        List<Contact> tempContact= WCT_UtilTestDataCreation.createContactAsCandidateLst(rtId);
        insert tempContact;

        // start the test execution context
        Test.startTest();

        // set the test's page to your VF page (or pass in a PageReference)
        Test.setCurrentPage(Page.AR_Company_NewCase);
        ApexPages.currentPage().getParameters().put('Eveid',Event.id);
        ApexPages.currentPage().getParameters().put('actionType','edit');
        // call the constructor new ApexPages.StandardController(Event)
       eEvent_Newcase controller = new eEvent_Newcase();
        
         // set the test's page to your VF page (or pass in a PageReference)
        Test.setCurrentPage(Page.eEvent_Newcase);
        ApexPages.currentPage().getParameters().put('Eveid',tempContact[0].id);
        ApexPages.currentPage().getParameters().put('actionType','new');
        // call the constructor
         controller = new eEvent_Newcase();
        
    // stop the test
        Test.stopTest();
        
    }
}