public class interPrototypeLandingPageController {
    public list<WCT_Interview_Junction__c> todayLst {get;set;}
    public list<WCT_Interview_Junction__c> upComingLst {get;set;}
    public list<WCT_Interview_Junction__c> pastLst {get;set;}
    public list<WCT_Interview_Junction__c> allLst {get;set;}
    private list<WCT_Interview_Junction__c> allDataLst {get;set;}
    public Integer tdSize {get;set;}
    public Integer upSize {get;set;}
    public Integer ptSize {get;set;}
    public Integer allSize {get;set;}

    public string profileName {get;set;}
    /*
    @Constructor
    */
    public interPrototypeLandingPageController() {
    todayLst    = new list<WCT_Interview_Junction__c>();
    upComingLst = new list<WCT_Interview_Junction__c>();
    pastLst     = new list<WCT_Interview_Junction__c>();
    allLst      = new list<WCT_Interview_Junction__c>();
    
    /* Querying Profile Name For HomePage Component to redirect to VF - Communities Licenses have no access to Query Profile thought JS. :( )*/
    User UserInf =[Select profile.Name from User where id=:UserInfo.getUserId()];
    profileName = UserInf.profile.Name;
    
        //Hiring level,WCT_IEF_Submission_Status__c,WCT_Contact__r.Name, WCT_Contact__r.WCT_Taleo_Id__c,  is removed from Inteview. So removing it from Query
        allDataLst = [SELECT Name, WCT_CandidateTrackerOwner__c,WCT_Candidate_Name__c,WCT_Candidate_Tracker__c,WCT_Email__c,WCT_End_Date_Time__c,
						WCT_Hiring_Level__c,WCT_Interviewer_Email__c,WCT_Interviewer_Level__c,
						WCT_Interviewer__c,WCT_Interview_Junction_Status__c,WCT_Interview_Medium__c,
						WCT_Interview_Stage__c,WCT_Interview_Status__c,WCT_Interview_Type__c,WCT_Interview__c,WCT_Job_Type__c,
						WCT_Location_Details__c,WCT_Mobile__c,WCT_Requisition_ID__c,WCT_Requisition_RC_Email__c,WCT_Requisition_Recruiter_Email__c,
						WCT_Sample_Questions_Case_Materials__c,WCT_Service_Line__c,WCT_Start_Date_Time__c 
						FROM WCT_Interview_Junction__c 
                    where WCT_Start_Date_Time__c !=null 
                    order by WCT_Start_Date_Time__c desc
                    limit:Limits.getLimitQueryRows()];
        tdSize = rtnsize(allDataLst, 'Today');
        upSize = rtnsize(allDataLst, 'UpComing');
        ptSize = rtnsize(allDataLst, 'Past');
        allSize = rtnsize(allDataLst, 'All');
        todayLst();//Default landing list...

    }
    private void clearListData(){
        todayLst.clear();
        upComingLst.clear();
        pastLst.clear();
        allLst.clear();
    }
    public void todayLst(){
        clearListData();
        todayLst = rtnLst(allDataLst,'Today');
    }
    public void upComingLst(){
        clearListData();
        upComingLst = rtnLst(allDataLst,'UpComing');
    }
    public void pastLst(){
        clearListData();
        pastLst = rtnLst(allDataLst,'Past');
    }
    public void allLst(){
        clearListData();
        allLst = rtnLst(allDataLst,'All');
    }            
    public list<WCT_Interview_Junction__c> rtnLst(list<WCT_Interview_Junction__c> AllDt, String crt){
        list<WCT_Interview_Junction__c> lstDt = new list<WCT_Interview_Junction__c>();
        for(WCT_Interview_Junction__c ads : AllDt){
            if(crt=='Today'){
                if(ads.WCT_Start_Date_Time__c > date.today() && ads.WCT_Start_Date_Time__c < (date.today()+1)){
                    lstDt.add(ads);
                }
            }
            if(crt=='UpComing'){
                if(ads.WCT_Start_Date_Time__c > (date.today()+1)){
                    lstDt.add(ads);
                }
            }
            if(crt=='Past'){
                if(ads.WCT_Start_Date_Time__c < date.today()){
                    lstDt.add(ads);
                }
            }
        }
        if(crt=='All'){
            lstDt.addAll(AllDt);
        }
        
        return lstDt;
    }
    public Integer rtnsize(list<WCT_Interview_Junction__c> AllDt, String crt){
        Integer lstSize = 0;
        for(WCT_Interview_Junction__c ads : AllDt){
            if(crt=='Today'){
                if(ads.WCT_Start_Date_Time__c > date.today() && ads.WCT_Start_Date_Time__c < (date.today()+1)){
                    lstSize+=1;
                }
            }
            if(crt=='UpComing'){
                if(ads.WCT_Start_Date_Time__c > (date.today()+1)){
                    lstSize+=1;
                }
            }
            if(crt=='Past'){
                if(ads.WCT_Start_Date_Time__c < date.today()){
                    lstSize+=1;
                }
            }
            if(crt=='All'){
                lstSize+=1;
            }
        
        }
        return lstSize;
    }
    
}