public class sObjectCreateManualTasks{

    public list<WCT_List_Of_Names__c> TasksListForCurrentObj {get;set;}
    public string currObjType {get;set;}
    public string currentRecId {get;set;}
    public sObject currObject {get;set;}
    public List<String> AccessibleFields = new List<String>();
    public List<TaskObjWrapper> taskWrapList = new list<TaskObjWrapper>();
    
    public List<TaskObjWrapper> taskWrapListToDisplay {get;set;}
    public List<Task> taskToCreate {get;set;}
    private static Map<String, Schema.sObjectType> sObjectByKeyPrefix {
        get {
            if (sObjectByKeyPrefix == null) {
                sObjectByKeyPrefix = new Map<String, Schema.sObjectType>();
                for (Schema.SObjectType sObj : Schema.getGlobalDescribe().values()) {
                    sObjectByKeyPrefix.put(sObj.getDescribe().getKeyPrefix(), sObj);
                }
            }
            return sObjectByKeyPrefix;
        }
        
        private set;
    } 
    
    public sObjectCreateManualTasks(){
        currObjType = ApexPages.currentPage().getParameters().get('objType');
        currentRecId = ApexPages.currentPage().getParameters().get('Id');
        taskWrapListToDisplay = new list<TaskObjWrapper>();
        taskToCreate = new List<Task>();
        TasksListForCurrentObj = [SELECT id,name,WCT_Criteria_for_Due_Date__c,WCT_Due_Date__c,WCT_Task_Due_Date__c, WCT_Task_Assigned_User__c,
                                    WCT_Task_for_Object__c, WCT_Task_Subject__c,Time_Frame_for_Task__c FROM WCT_List_Of_Names__c 
                                    WHERE recordTypeId = :WCT_Util.getRecordTypeIdByLabel('WCT_List_Of_Names__c', 'sObject Task Reference')
                                     AND WCT_Task_for_Object__c = :currObjType];
        string currKeyPrefix = ApexPages.currentpage().getParameters().get('id').subString(0,3);
        Schema.sObjectType currsObjectType = sObjectByKeyPrefix.get(currKeyPrefix);
        system.debug('^^'+TasksListForCurrentObj);
        string currentObject;
        currentObject =  currsObjectType.getDescribe().getLabel();
        //Dynamic reference of all fields
        Map<String, Schema.SobjectField> fields =  currsObjectType.getDescribe().fields.getMap();
        for (String s : fields.keySet()) {
            if ((s != 'id') && (fields.get(s).getDescribe().isAccessible())) {
                AccessibleFields.add(s);
                
            }
        }
         String Query = 'SELECT id,' + joinList(AccessibleFields, ', ') + 
                          ' FROM ' + currsObjectType.getDescribe().getName() + 
                          ' WHERE Id = :currentRecId';
        sObject sobj = database.query(Query);
        
        for(WCT_List_Of_Names__c lon:TasksListForCurrentObj){
            taskWrapList.add(new TaskObjWrapper(false,lon));        
        
        }      
        
        if(!taskWrapList.isEmpty()){
            for(TaskObjWrapper tw:taskWrapList){
                WCT_List_Of_Names__c lon = tw.LONRec;
                if(lon.WCT_Criteria_for_Due_Date__c == null){
                    lon.WCT_Task_Due_Date__c = system.today();
                }else if(lon.WCT_Criteria_for_Due_Date__c == 'Today'){
                    if(lon.Time_Frame_for_Task__c == 'Days'){
                        lon.WCT_Task_Due_Date__c = system.today().addDays(Integer.valueOf(lon.WCT_Due_Date__c));
                    }else if(lon.Time_Frame_for_Task__c == 'Months'){
                        lon.WCT_Task_Due_Date__c = system.today().addMonths(Integer.valueOf(lon.WCT_Due_Date__c));
                    }else if(lon.Time_Frame_for_Task__c == 'Years'){
                        lon.WCT_Task_Due_Date__c = system.today().addYears(Integer.valueOf(lon.WCT_Due_Date__c));
                    }
                }
                else{
                    if((Date)sobj.get(lon.WCT_Criteria_for_Due_Date__c) <> null){
                        if(lon.Time_Frame_for_Task__c == 'Days'){
                            lon.WCT_Task_Due_Date__c = ((Date)(sobj.get(lon.WCT_Criteria_for_Due_Date__c))).addDays(Integer.valueOf(lon.WCT_Due_Date__c));
                        }else if(lon.Time_Frame_for_Task__c == 'Months'){
                            lon.WCT_Task_Due_Date__c = ((Date)sobj.get(lon.WCT_Criteria_for_Due_Date__c)).addMonths(Integer.valueOf(lon.WCT_Due_Date__c));
                        }else if(lon.Time_Frame_for_Task__c == 'Years'){
                            lon.WCT_Task_Due_Date__c = ((Date)sobj.get(lon.WCT_Criteria_for_Due_Date__c)).addYears(Integer.valueOf(lon.WCT_Due_Date__c));
                        }
                    }
                    
                }
                taskWrapListToDisplay.add(new TaskObjWrapper(false,lon));
            }       
        }


        
    }
    private static String joinList(List<String> theList, String separator) {

        if (theList == null)   { return null; }
        if (separator == null) { separator = ''; }

        String joined = '';
        Boolean firstItem = true;
        for (String item : theList) {
            if(null != item) {
                if(firstItem){ firstItem = false; }
                else { joined += separator; }
                joined += item;
            }
        }
        return joined;
    }
    
    public class TaskObjWrapper{
        public boolean isSelected {get;set;}
        public WCT_List_Of_Names__c LONRec {get;set;}
        
        public TaskObjWrapper(boolean sel,WCT_List_Of_Names__c lonR){
            isSelected = sel;
            LONRec = lonR;
        }
    
    }  

    public pageReference createTasks(){
        for(TaskObjWrapper tw:taskWrapListToDisplay){
            if(tw.isSelected){
                Task ta = new Task();
                ta.subject = tw.LONRec.WCT_Task_Subject__c;
                ta.whatId = currentRecId;
                ta.status = 'Not Started';
                ta.priority = '2-Normal';
                if(tw.LONRec.WCT_Task_Assigned_User__c <> null){
                    ta.ownerId=  tw.LONRec.WCT_Task_Assigned_User__c;
                }else if(currObjType == 'WCT_Leave__c'){
                    ta.ownerId= system.label.Leaves_User;
                }
                
                if(currObjType == 'WCT_Leave__c'){
                    ta.recordTypeId = WCT_Util.getRecordTypeIdByLabel('Task', 'Leaves');
                }
                if(tw.LONRec.WCT_Task_Due_Date__c <> null){
                    ta.ActivityDate = tw.LONRec.WCT_Task_Due_Date__c;
                }
                taskToCreate.add(ta);
            }   
        }   
        if(!taskToCreate.isEmpty()){
            insert taskToCreate;
        }
        return new PageReference('/'+currentRecId);
    }
    
    public pageReference cancel(){
        return new PageReference('/'+currentRecId);
    }
}