@istest
public class test_eEvents_feedback
{

static testmethod void eEvents_feedback_external()
{

Account acc=new Account(name='eEvents_feedback_external');                  
     insert acc;
     Contact con = new Contact();
     con.LastName = 'eEvents_feedback_external';
     con.FirstName = 'eEvents_feedback_external';
     con.Email = 'eEvents_feedback_external@invalid.com';
     con.Teams__c = 'Campus';
     con.WCT_Candidate_School__c =  'School of Arts';
     con.Degree_program__c = 'BA';
     insert con;
School__c school = new School__c(name = 'eEvents_feedback_external');
     insert school;
 Event__c event = new Event__c();
     event.School__c = school.id;
     event.Name = 'eEvents_feedback_external';
     event.Event_Cap__c = 1000;
     event.Account__c = acc.id;
     event.Start_Date_Time__c=system.now();
     event.end_date_time__c=system.now();
     event.AR_image_for_registration_page__c='Chairs';
     insert event;
Event_Registration_Attendance__c ObjEventReg = new  Event_Registration_Attendance__c();
     ObjEventReg.Contact__c = con.id;
     ObjEventReg.Event__c = event.id;       
     ObjEventReg.Attended__C = false;    
     ObjEventReg.Attending__C = true;  
     ObjEventReg.Attendee_Type__c = 'Alumni';
     ObjEventReg.invited__C=true;
     insert ObjEventReg;
     
 

        PageReference pageRef = Page.Wct_eEvents_AR_internalfeedback;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('eid',string.valueof(event.id));
        ApexPages.currentPage().getParameters().put('conid',string.valueof(con.id));
        PageReference pageRef1 = Page.Wct_eEvents_AR_externalfeedback;
        Test.setCurrentPage(pageRef1);
        ApexPages.currentPage().getParameters().put('eid',string.valueof(event.id));
        ApexPages.currentPage().getParameters().put('conid',string.valueof(con.id));
        Wct_eEvents_AR_externalfeedback ARL= new Wct_eEvents_AR_externalfeedback(); 
        ARL.getQA1();
        ARL.getQA2();
        ARL.getQA3();
        ARL.getQA4();
        ARL.getARfunction();
        ARL.getARlevel();
        ARL.feedsubmit();
        ARL.Scheduled_follow_up_meeting_with_alum=false;
        ARL.Contributed_to_an_opportunity_to_propose=false;
        ARL.Contributed_to_a_win_or_likeli=false;
        ARL.Made_contact_with_potential_boomerang=false;
         ARL.AR_Outcome_No_direct_outcome=false;
        ARL.AR_Outcome_Other=false;
        ARL.feedsubmitinternal();
}
}