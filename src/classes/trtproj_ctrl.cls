public class trtproj_ctrl extends SitesTodHeaderController {
    public project__c prj {get;set;}
    public boolean fieldRender {get;set;}
    public String requesttype {get;set;}
    public String inc {get;set;}
    public string emValue {get;set;}
    public string ProcessArea {get;set;}
    Map < string, string > mapFS = new Map < string, string > ();
    public boolean IsCreated {get;set;}
    public List < project__c > prjLst {get;set;}
    
    public trtproj_ctrl() {
        fieldRender = false;
        Preinit();
    }

    public void Preinit() {
        IsCreated = false;
        //Create New Record in project Obj
        emValue = ApexPages.currentPage().getParameters().get('em');
        if (string.IsNotEmpty(ApexPages.currentPage().getParameters().get('IsCreated'))) {
            IsCreated = true;
        }
        string userEmail = cryptoHelper.decrypt(emValue);
        try {
            contact conObj = [select id, email from contact where email = : userEmail limit 1];
            prj = new project__c();
            if (conObj.id != null) prj.Requestor__c = conObj.id;
            ProcessArea = '';
            Map < String, Schema.fieldset > fieldSetMap = Schema.SObjectType.Project__c.fieldSets.getMap();
            for (string strFS: fieldSetMap.keyset()) {
                mapFS.put(fieldSetMap.get(strFS).getLabel(), strFS);
            }
            prjLst = new List < project__c > ();
            prjLst = [select id, name,Request_Type__c, Project_Type__c, Project_Status__c, Status__c, Project_Number__c, Description__c, Requestor__c,Project_Lead_USI__c from project__c where Requestor__r.email = : userEmail];
        } catch (Exception e) {}
    }


    public Attachment attachment {
        get {
            if (attachment == null) attachment = new Attachment();
            return attachment;
        }
        set;
    }


    public void getProcessArea() {
        if (string.IsNotEmpty(ApexPages.currentPage().getParameters().get('ProcessArea'))) {
            ProcessArea = ApexPages.currentPage().getParameters().get('ProcessArea');
            ProcessArea = mapFS.get(ProcessArea);
            fieldRender = true;
        }
    }

    public pagereference Save() {
        try {

            system.debug('----attachment-----' + attachment + ' ---- prj ---' + prj);
            
            insert prj;
            system.debug('----prj-----' + prj);
            if (attachment != null && attachment.Name != null) {
                attachment.OwnerId = UserInfo.getUserId();
                attachment.ParentId = prj.Id; // the record the file is attached to
                attachment.IsPrivate = true;

                try {
                    insert attachment;

                } catch (DMLException e) {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error uploading attachment'));

                } finally {
                    attachment = new Attachment();
                }
            }
            // ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,'Project is Created'));
            PageReference objPageRef = new PageReference('/apex/trtproject?em=' + emValue);
            if (prj.Id != null) {

                objPageRef = new PageReference('/apex/trtprojectthankyou');
                objPageRef.setRedirect(true);
            }
            return objPageRef;
        } catch (exception ex) {
            system.debug('----Exception-----' + ex.getMessage());
            return null;
        }
    }

    public void Clear() {
        Preinit();
    }
}