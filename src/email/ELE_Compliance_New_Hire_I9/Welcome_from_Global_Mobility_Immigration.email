<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>

<body>
<table width="820"style="border:#575757 solid 10px; border-collapse:collapse" >
<tr>
<td style="border-bottom:#575757 solid 15px">
<img src="http://image.s6.exacttarget.com/lib/fe93127271610d7d77/m/1/clip_image001.png" width="380" height="53"  style="margin-left:20px"/><br/>
<img src="https://talent--c.na2.content.force.com/servlet/servlet.ImageServer?id=01540000001vLaL&oid=00D40000000MxFD&lastMod=1440183597000" align="right"><br/>

<p style="width:100%; color:#002776; font-family:Calibri; font-size:30px; margin-left:20px">Welcome!<br/> <br/>

<span style="color:#81bc00;">From Global Mobility & Immigration</span></p>

<p style="display:inline-block; width:90%; font-family:Arial, Helvetica, sans-serif; font-size:12px; margin-left:20px"> In this email:<br><br>
<a href="#1" title="Obligations & Responsibilities">Obligations & Responsibilities</a><br>
<a href="#2" title="Labor Condition Application (LCA)">Labor Condition Application (LCA)</a><br>
<a href="#3" title="Reporting Address Changes">Reporting Address Changes</a><br>
<a href="#4" title="Applying for a US Visa Stamp Abroad">Applying for a US Visa Stamp Abroad</a><br>
<a href="#5" title="Traveling to a Country Outside the U.S. for Business">Traveling to a Country Outside the U.S. for Business</a><br>
<a href="#6" title="Applying for a U.S. Social Security Number">Applying for a U.S. Social Security Number</a><br>
<a href="#7" title="I-94 Arrival/Departure Card Process">I-94 Arrival/Departure Card Process</a><br>
<a href="#8" title="Contact List">Contact List</a><br></p>

<a name="1"></a><h3 id="1" style="margin-left:12px; color:#00A1DE"> Obligations & Responsibilities</h3>
<p style="display:inline-block; width:90%; font-family:Arial, Helvetica, sans-serif; font-size:12px; margin-left:20px;margin-right:20px;text-align:justify">Whether you are working in the U.S. in temporary work status (H-1, L-1, E-3, TN, F-1 OPT, etc) or in Lawful Permanent Residence (green card) status, it is important that you know your obligations for maintaining your status as a non-US citizen who is working in the U.S. long-term.  We invite you to visit the Global Mobility & Immigration <a href="https://deloittenet.deloitte.com/CL/Global/Immig/Pages/home.aspx"> website </a> which has its own presence on DeloitteNet.<br/><br>

While Global Mobility & Immigration and outside counsel track certain expiration dates on your temporary immigration documents you too have certain <a href="https://deloittenet.deloitte.com/CL/Global/Immig/Pages/EmployeeResponsibility.aspx"> Responsibilities </a> associated with maintaining your status in the U.S.<br>
<a href="https://deloittenet.deloitte.com/CL/Global/Immig/Pages/home.aspx">More</a><br>

<a name="2"></a><h3 id="2" style="margin:12px; color:#00A1DE">Labor Condition Application (LCA)</h3>

<p style="display:inline-block; width:90%; font-family:Arial, Helvetica, sans-serif; font-size:12px; margin-left:20px;margin-right:20px;text-align:justify"><b>H-1B, H-1B1 and E-3 Workers OR those currently in the process of securing one of these visas:</b> <br>Please be advised that H-1B, H-1B1, and E-3 visa holders are only authorized to work at a geographical location (stating the exact address including zip code) on a certified labor condition application (LCA).  You must receive the certified LCA in order to deploy to any new work location.   A work location for LCA purposes would include a client site, project site and/or Deloitte office. Failure to notify your law firm in a timely manner may necessitate the filing of an amended petition and restrictions on travel.<br><br>

Please note that if you are an H-1B, H-1B1 or E-3 Workers OR currently in the process of securing one of these visas, you will receive a request to complete an LCA attestation.  Failure to comply with LCA rules can open the organization to liability and can endanger your visa status.  Therefore, understand LCA compliance is not a choice and please follow always request an LCA the moment you know you will be deployed to a new client site, or when you know your reporting office will change.<br><br>

<b>If you are a new hire who has not started your employment with Deloitte,</b> please do not send a separate email request for an LCA at this time. The appropriate LCA(s) will be processed on your behalf without submitting a request.<br><br>

<b>If you are a current Deloitte Consulting  employee in H1B, H1B1 or E3 status,</b> you must request and receive a certified LCA before deploying to a new or different client location.  You can request an LCA by submitting the "Deloitte LCA Initiation" form via the Input tile in WWIHP. Please access the initiation form in WWIHP by clicking <a href="https://samlsso.deloitte.com/FragomenSSOLanding/FragomenSSOLandingpage.aspx?Issuer=Deloitte.IHP">here.</a><br><br>

<b>If you are a current Deloitte Non-Consulting (AERS, Tax, Fas, Services or DTT, etc.)  employee in H1B or E3 status,</b> you must request and receive a certified LCA before deploying to a new or different client location.   You can request an LCA by submitting the questionnaire “New Work Site LCA” in ImmSTAR®.  Please access the questionnaire in ImmSTAR® by clicking<a href="https://samlsso.deloitte.com/seyfarthssolanding/SeyFarthSSOLandingPage.aspx"> here.</a><br><br>

For more information on the LCA compliance requirement, please click on the LCA FAQ  here: <a href="https://deloittenet.deloitte.com/CL/Global/Immig/Pages/LaborConditionApplication.aspx">DeloitteNet link for LCAs</a> <br><br>

<a name="3"></a><h3 id="3" style="margin:12px; color:#00A1DE">Reporting Address Changes</h3>

<p style="display:inline-block; width:90%; font-family:Arial, Helvetica, sans-serif; font-size:12px; margin-left:20px;margin-right:20px;text-align:justify">All non-U.S. citizens, including lawful permanent residents, are required to notify DHS of changes in address within 10 days from the day of your move to the new location.   To notify DHS, you should complete an AR-11 Change of Address form (link below).  The only foreign nationals exempt from this requirement are non-immigrants in the A (foreign diplomats) and G (representatives of international organizations) visa categories and non-immigrants who are not required to possess a visa and are in the United States for fewer than 30 days.<a href="http://www.uscis.gov/portal/site/uscis/menuitem.5af9bb95919f35e66f614176543f6d1a/?vgnextoid=c1a94154d7b3d010VgnVCM10000048f3d6a1RCRD&vgnextchannel=db029c7755cb9010VgnVCM10000045f3d6a1RCRD"> AR-11 Change of Address Form</a><br><br>

In addition to notifying DHS, you should also update your<a href="https://deloittenet.deloitte.com/CB/Info/Pages/home.aspx">Contact Information on DeloitteNet.</a><br><br></p>

<a name="4"></a><h3 id="4" style="margin:12px; color:#00A1DE">Applying for a US Visa Stamp Abroad</h3>
<p style="display:inline-block; width:90%; font-family:Arial, Helvetica, sans-serif; font-size:12px; margin-left:20px;margin-right:20px;text-align:justify">Always notify outside immigration counsel prior to finalizing plans for travel abroad.  An assessment should be done before you leave the U.S. to determine if the travel plans may impact any pending immigration applications and to ensure you have the proper documents for reentry. If you are making U.S. visa applications abroad visit the<a href="http://www.usembassy.gov/"> Consulate Website </a> or make an <a href="http://travel.state.gov/visa/temp/wait/wait_4638.html"> appointment for visa </a> stamping BEFORE you book your travel.  Application and appointment fees are reimbursable through Global Mobility & Immigration (see reimbursement form link below).<br><br>

Employment verification letters (EVLs) required for your visa application can be obtained by either submitting a non-urgent request via a <a href="https://talentondemand.deloittenet.deloitte.com/default.aspx#!/my-information/employment-letter/">webform</a> to the PSN (or you may access the webform by typing 2222 in the “Jump To” field on DeloitteNet), or by calling 1-800-DELOITTE.  A lead time of 5-7 days should be honored to ensure your signed letter arrives via mail or fax prior to your departure. You should have these documents in your possession and show them only if specifically requested at the time of entry into the U.S.  This letter, which is to be used for visa stamping purposes, will reflect the following details: Name, Employing Entity, Office Assigned, Salary, and Hire Date<br><br>

You can reimburse the costs associated with the visa stamp itself, but not airfare, travel or lodging while obtaining the stamp. <b> Please pay for your stamp with your corporate card</b>.  In order to apply for reimbursement for visa stamping fees, please follow this instructions on this page: <a href="https://deloittenet.deloitte.com/About/Bus/Global/OGD/Immig/Pages/Forms.aspx">Visa Stamping Reimbursement Process</a><br><br>

All U.S. personnel must report expenses within <b>30 days of the date incurred</b>. Failure to report expenses within 30 days of the date incurred is grounds to decline reimbursement to the individual for the expenses. This process is not for India practitioners – please contact usindiatalentimmigration@deloitte.com for instructions on processing.  Please keep your original documents and carefully follow all instructions on the<b> immigration page</b>.<br>

<a name="5"></a><h3 id="5" style="margin:12px; color:#00A1DE">Traveling to a Country Outside the U.S. for Business</h3>
<p style="display:inline-block; width:90%; font-family:Arial, Helvetica, sans-serif; font-size:12px; margin-left:20px;margin-right:20px;text-align:justify">Deloitte's Global Mobility team supports all international business travel and is a go-to resource for obtaining the most appropriate immigration requirements.<br><br>
All professionals who are traveling internationally for Deloitte are required to receive an assessment of their immigration needs prior to making any travel plans to ensure compliance with country-specific immigration requirements. You are responsible for understanding the immigration rules and providing accurate information in completing the required assessment.  If you are traveling outside the U.S. and want to know if you need a visa, please take the assessment provided by clicking the links below.<br><br>
If your international travel is for client-billable work, please click <a href="https://deloittenet.deloitte.com/CL/Pages/Cross-Border-Projects.aspx">here.</a>
<br><br>If your international travel is NOT for client-billable work, please click <a href="https://deloittenet.deloitte.com/CL/Pages/GVDestinationCountries.aspx">here.</a> <br></p>

<a name="6"></a><h3 id="6" style="margin:12px; color:#00A1DE">Applying for a U.S. Social Security Number</h3>
<p style="display:inline-block; width:90%; font-family:Arial, Helvetica, sans-serif; font-size:12px; margin-left:20px;margin-right:20px;text-align:justify">Within 7-10 business days after arriving in the U.S., you will be required to apply for a social security card so that you are able to accept a salary from a U.S. company.  In order to apply for a social security card, you must appear in person at your local Social Security Administration (SSA) office.  To find your local office, click here: <a href="https://secure.ssa.gov/ICON/main.jsp">SSA office locator</a><br><br>
Please read the FAQs and the “more information” link on the page about the process for application:<a href="http://ssa-custhelp.ssa.gov/app/answers/detail/a_id/76">  Social Security FAQs</a><br><br>
You will be required, in some instances, to provide an I-94 card as evidence of your work status.  Customs and Border Protection (CBP) no longer provides paper I-94 cards upon entry.  Therefore, before your SSA appointment, please visit the CBP website to download a copy of your I-94 card. To log onto CBP and request an I-94 copy, click here and complete the form: <a href="https://i94.cbp.dhs.gov/I94/request.html">CBP I-94 Request.</a><br><br>
To begin the I-9 Employment Eligibility Verification process, please do the following: <br/></p>

<a name="7"></a><h3 id="7" style="margin:12px;color:#00A1DE">I-94 Arrival/Departure Card Process</h3>
<p style="display:inline-block; width:90%; font-family:Arial, Helvetica, sans-serif; font-size:12px; margin-left:20px;margin-right:20px;text-align:justify">U.S. Customs and Border Protection (“CBP”) has begun automating the Form I-94 Arrival/Departure Record at air and sea ports of entry. <b>The paper form will no longer be provided to a traveler upon arrival, except if traveling by land.</b> Travelers arriving to the U.S. via air or sea who need to prove their legal immigration status will be able to access their arrival/departure record information online. Under the new automated process, a CBP officer will stamp the travel document (typically a passport) of each arriving traveler. The admission stamp will show the date of admission, class of admission, and the date until which the traveler is admitted. Travelers will also receive upon arrival a flyer alerting them to go to CBP’s website to review and print their admission record information. <br><br>

Because advance information is only transmitted for air and sea travelers, CBP will still issue a paper I-94 at land border ports of entry for the time being.<br><br>

<u><b>Best Practice</u></b>: After your return to the U.S., it is a best practice to log onto the CBP website, print a copy of the I-94 card to keep with your passport. Additionally, you should provide a copy to your respective law firm to ensure you were admitted in the correct status and for the full duration of your valid stay.<b> Please remember that it is your most recent I-94 card, and not your visa stamp or USCIS approval notice*, that determines the duration of your legal period of stay in the U.S. so it is imperative that your entry information be completely correct.</b> CBP can and does make mistakes, so please always do check.

*the USCIS Approval Notice (I-797) is the controlling document only if you filed and received an extension of stay after you last traveled abroad and re-entered the U.S. In this case, extension of stay documents (I-797) will have a new I-94 card at the bottom that becomes the controlling document and takes the place of your last downloaded I-94 card.<br><br>

To log onto CBP and request an I-94 copy, click here and complete the form:<a href="https://i94.cbp.dhs.gov/I94/request.html">CBP I-94 Request.</a><br><br>

Once you have received a copy of your electronic I-94, please provide the copy to your law firm to ensure your duration of stay is correct, in addition to securing a copy into your passport. Please follow this procedure when providing copies:<br>

<ol start="1"style="display:inline-block; width:90%;font-family:Arial, Helvetica, sans-serif; font-size:12px; margin-left:20px">
<li>If you are a non-Consulting employee, visit our website to access the single-sign on to Seyfarth’s ImmSTAR® database by clicking here: <a href="https://samlsso.deloitte.com/seyfarthssolanding/SeyFarthSSOLandingPage.aspx">Law Firm Access.</a> Once you have signed on, please upload a scanned copy of your I-94 record to the secure Document Vault under the “document upload” tab. The law firm will check your entry information to ensure you were properly admitted and advise if steps need to be taken to correct an I-94 error. </li><br><br>
<li>If you are a Consulting employee, please scan and email a copy of the I-94 to<a href="DeloitteConsulting@fragomen.com">Fragomen.</a> The law firm will check your entry information to ensure you were properly admitted and advise if steps need to be taken to correct an I-94 error. </li><br></p></ol>

<a name="8"></a><h3 id="8" style="margin:12px;color:#00A1DE">Contact List</h3>
<p style="display:inline-block; width:90%; font-family:Arial, Helvetica, sans-serif; font-size:12px; margin-left:20px;margin-right:20px;text-align:justify"><b>If you have any questions at all, at any time, you can direct your immigration questions to the appropriate law firm contact below:</b><br>
<table border=1 style="width:100%">
  <th style="color:#00A1DE"> Area of Service </th>
 <th style="color:#00A1DE"> Name </th>
<th style="color:#00A1DE"> Email </th>
<th style="color:#00A1DE"> Phone </th>
  <tr>
    <td><b>Consulting</b><br>
   Employee Last Name A-L
  </td>
    <td><b>Kimberly O’Connell</b><br>
  On-site Specialist<br>
  Fragomen, Del Rey, Bernsen & Loewy, LLP
    </td>		
    <td>kioconnell@deloitte.com</td>
<td>215-299-4578<td>
  </tr>
 <tr>
    <td><b>Consulting</b><br>
   Employee Last Name M-Z
  </td>
    <td><b>Rita Grassi</b><br>
  On-site Specialist<br>
  Fragomen, Del Rey, Bernsen & Loewy, LLP
    </td>		
    <td>rgrassi@deloitte.com</td>
<td>215-430-6278<td>
  </tr>
<tr>
    <td><b>All Non-Consulting Entities</b><br>
   (AERS, FAS,  DTBA, Tax, EA, DTTSI, etc.)
  </td>
    <td><b>Amanda Duke</b><br>
  On-site Specialist<br>
  
Seyfarth, Shaw, LLP

    </td>		
    <td>aduke@deloitte.com  </td>
<td>617-946-4961<td>
  </tr>
</table><br>
<a name="9"></a><h3 id="9" style="margin:12px;color:#00A1DE">Related Links</h3>
<a href="http://www.fragomen.com/newsresources/xprGeneralContentFrag.aspx?xpST=ResourceGeneral&key=3a29031e-0d56-4dbe-87c4-04fc244cb353&activeEntry=d1fe0e70-85fe-46c3-9345-48f6fd2495b6">US Government Processing Times</a>"
<p style="display:inline-block; width:90%; font-family:Arial, Helvetica, sans-serif; font-size:10px; margin-left:20px;margin-right:20px;text-align:justify">
Global Mobility & Immigration<br>
1700 Market Street<br>
Philadelphia, PA 19103<br>
USA<br>

Deloitte refers to one or more of Deloitte Touche Tohmatsu Limited, a UK private company limited by guarantee, and its network of member firms, each of which is a legally separate and independent entity. Please see <a href="www.deloitte.com/about">www.deloitte.com/about </a> for a detailed description of the legal structure of Deloitte Touche Tohmatsu Limited and its member firms.<br><br>
This publication is for internal distribution and use only among personnel of Deloitte Touche Tohmatsu Limited, its member firms, and their related entities (collectively, the “Deloitte Network”). None of the Deloitte Network shall be responsible for any loss whatsoever sustained by any person who relies on this publication.<br><br>
© 2015 Deloitte Global Services Limited<br>
<a href="https://global.deloitteresources.com/">Home |</a><a href="https://global.deloitteresources.com/Pages/Security.aspx"> Security |</a> <a href="https://global.deloitteresources.com/Pages/Legal.aspx">Legal |</a> <a href="https://global.deloitteresources.com/Pages/Privacy.aspx">Privacy</a>
<br>
</td>
</tr>

</table>

</td>
</tr>
</table>
</body>
</html>