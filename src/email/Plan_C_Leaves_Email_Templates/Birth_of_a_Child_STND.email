<table height="400" width="550" cellpadding="5" border="0" cellspacing="5" >
<tr height="400" valign="top" >
<td style=" color:#000000; font-size:12pt; background-color:#FFFFFF; font-family:arial; bLabel:main; bEditID:r3st1;" tEditID="c1r1" locked="0" aEditID="c1r1" >
<![CDATA[<p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><span style="color: rgb(0, 0, 0); font-family: Verdana, sans-serif; font-size: 10pt;">Dear&nbsp;</span><font face="Verdana, sans-serif" size="2">{!WCT_Leave__c.WCT_Employee_Name__c}</font><span style="color: rgb(0, 0, 0); font-family: Verdana, sans-serif; font-size: 10pt;">:</span></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><span style='font-family: "Verdana","sans-serif"; font-size: 10pt;'>
<br></span></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><span style='font-family: "Verdana","sans-serif"; font-size: 10pt;'>Congratulations
on the new (or upcoming) addition to your family. We have been notified of your
need for a leave of absence (LOA). You will find information and links below to
help guide you through your Parental Leave. Here is a link to the Parental
Leave checklist to get you started.
https://deloittenet.deloitte.com/CB_PlanC/Insurance/LTSTD/Pages/ParentalLeaveChecklistSTD.aspx<br>
<br>
Attached please find information regarding rights you may have under the Family
and Medical Leave Act (FMLA). Click here to read additional information
regarding FMLA.
https://deloittenet.deloitte.com/CB_PlanC/Insurance/LTSTD/Pages/FamilyMedicalLeaveofAbsence.aspx<br>
<br>
<b><u>Family and Medical Leave Act (FMLA)</u></b><o:p></o:p></span></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><span style="font-family: Symbol; font-size: 10pt; mso-ascii-font-family: Verdana;">·</span><span style='font-family: "Verdana","sans-serif"; font-size: 10pt;'>&nbsp; FMLA Notification <o:p></o:p></span></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><span style="font-family: Symbol; font-size: 10pt; mso-ascii-font-family: Verdana;">·</span><span style='font-family: "Verdana","sans-serif"; font-size: 10pt;'>&nbsp; FMLA Rights and Responsibilities<o:p></o:p></span></p><p class="MsoNormal"><span style="color: rgb(0, 0, 0); font-family: Symbol; font-size: 10pt;">·</span><font face="Verdana, sans-serif" size="2">&nbsp; If you are eligible for FMLA, you have a right under the FMLA for up to 12 weeks of unpaid leave in a 12-month period calculated as a "rolling" 12-month period.&nbsp; The 12 month period is&nbsp; measured backward from the date of any FMLA leave usage.</font><br><font face="Verdana, sans-serif" size="2">
..</font><br>
<b style="color: rgb(0, 0, 0); font-family: Verdana, sans-serif; font-size: 10pt;"><u>Short Term Disability (STD) Information</u></b><br><font face="Verdana, sans-serif" size="2">
If you have not already, please contact MetLife (Deloitte's Short-Term
Disability Administrator) to begin your claim for STD benefits. MetLife can be
reached by calling 1-800-Deloitte (1-800-335-6488) and following the prompts
for disability. MetLife will determine your eligibility to receive paid STD
benefits. If your claim is approved by MetLife, STD benefits will begin
following your absence from work, due to illness or injury, for seven
consecutive calendar days. </font><br>
<br><font face="Verdana, sans-serif" size="2">
There is a one week waiting period before any STD benefits will be payable.
During this week, PTO should be charged in the amount of hours you would
normally work in seven consecutive calendar days (not to exceed 40 hours). If
you do not have a week of PTO available, any PTO you have accrued will be
credited towards the waiting period, and the balance of the one week waiting
period will be time without pay.</font><br>
<br><font face="Verdana, sans-serif" size="2">
If you work part of a day, that day will not be credited to the waiting period.
A working day constitutes the entire work day, and the one week waiting period
consists of no more than five consecutive working days. Click here to review
complete details regarding the STD program.
https://deloittenet.deloitte.com/CB_PlanC/Life/ND/Documents/Short_Term_Disability_SPD.pdf</font><br>
<br><font face="Verdana, sans-serif" size="2">
You will continue to receive approved STD benefits through the Deloitte
payroll. If appropriate medical information is not received timely and approved
by MetLife, you will be required to use all accrued PTO. If you exhaust all
accrued PTO, you will be placed on unapproved leave of absence. It is your
responsibility to ensure MetLife receives appropriate medical information from
your healthcare provider. Please carefully review your specific
responsibilities for your required actions before, during and after your leave
ends.</font><br>
<br><font face="Verdana, sans-serif" size="2">
Contact your leadership and/or Talent Representative to confirm the date you
will return to work. Discuss any restrictions or accommodations you will need
in order to return to work with your leadership or Talent Representative. You
may also need to provide the release to return to work form to your leadership
before you return to work if MetLife has not already received it. Click here to
print a copy of this form for your convenience.&nbsp;https://deloittenet.deloitte.com/CB_PlanC/Life/ND/Documents/Short_Term_Disability_SPD.pdf</font><br>
<br>
<b style="color: rgb(0, 0, 0); font-family: Verdana, sans-serif; font-size: 10pt;"><u>Parental Leave Information</u></b><br><font face="Verdana, sans-serif" size="2">
Professionals who add to their families through birth or adoption are eligible
for two weeks paid parental leave based on their current rate
of pay, provided they plan to return to work after the birth or adoption. This
paid leave period must be taken within the first twelve months after the birth
or placement for adoption and be taken in no less than 1 week increments.
</font></p><p class="MsoNormal"><font face="Verdana" size="2"></font><br><font face="Verdana, sans-serif" size="2">
It is the expectation of the U.S. Firms that the total time off for new parents
to adjust to the birth or placement for adoption of a child should not exceed
24 weeks (except where state laws provide a greater benefit).This time off may
include periods of disability leave, paid and unpaid parental leave, and time
taken as Personal Time Off (PTO) in lieu of unpaid parental leave.</font><br>
<br><font face="Verdana, sans-serif" size="2">
An additional period of unpaid parental leave of up to 12 weeks may be
requested within 12 months following the birth or placement for adoption of a
child. This additional period will be granted to the extent possible based on
the needs of the U.S. Firms, clients, and the professional.</font><br>
<br><font face="Verdana, sans-serif" size="2">
Click here for complete program details and to obtain the Parental Leave
Request Form.
https://deloittenet.deloitte.com/CB_PlanC/Insurance/LTSTD/Pages/ParentalLeaveofAbsence.aspx.
Please return the approved completed form to the Leaves Team within 15 calendar
days after you receive this email notification.</font><br>
<br>
<b style="color: rgb(0, 0, 0); font-family: Verdana, sans-serif; font-size: 10pt;"><u>New Parent Resources</u></b><br><font face="Verdana, sans-serif" size="2">
Click the following links for valuable information on DeloitteNet:<o:p></o:p></font></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><span style="font-family: Symbol; font-size: 10pt; mso-ascii-font-family: Verdana;">·</span><span style='font-family: "Verdana","sans-serif"; font-size: 10pt;'>&nbsp; <b>Working Family Resource Center - </b>https://deloittenet.deloitte.com/CB_PlanC/Life/Pages/WorkingFamilyResourceCenter.aspx<o:p></o:p></span></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><span style="font-family: Symbol; font-size: 10pt; mso-ascii-font-family: Verdana;">·</span><span style='font-family: "Verdana","sans-serif"; font-size: 10pt;'>&nbsp; <b>LifeWorks Online - </b>http://www.lifeworks.com/index.aspx?netlogin=lbs3DD6j5mRYonrtokTXRUXg68z13Gmv<o:p></o:p></span></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><span style="font-family: Symbol; font-size: 10pt; mso-ascii-font-family: Verdana;">·</span><span style='font-family: "Verdana","sans-serif"; font-size: 10pt;'>&nbsp; <b>New Parent Program - </b>https://deloittenet.deloitte.com/CB_PlanC/Life/Parent/Pages/BecomingAParentOverview.aspx<o:p></o:p></span></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><span style="font-family: Symbol; font-size: 10pt; mso-ascii-font-family: Verdana;">·</span><span style='font-family: "Verdana","sans-serif"; font-size: 10pt;'>&nbsp; <b>Lactation Support Program - </b>https://deloittenet.deloitte.com/CB_PlanC/Life/Parent/Pages/LactationSupportProgram.aspx<o:p></o:p></span></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><span style="font-family: Symbol; font-size: 10pt; mso-ascii-font-family: Verdana;">·</span><span style='font-family: "Verdana","sans-serif"; font-size: 10pt;'>&nbsp; <b>Deloitte Parents Network (DPN) - </b>https://deloittenet.deloitte.com/PC/SC/networks/DPN/Pages/Home.aspx<br>
<br>
<b><u>DTE Compliance</u></b><br>
<br>
Other than PTO (and possibly holiday) please do not report time in DTE for the
periods of approved leave. Please leave DTE completely blank for the date(s) of
approved leave. <b><u>You may receive missing time notices at the start of your
leave which you can disregard. </u></b>DTE will later be updated to reflect the
approved LOA period(s) and the missing time notices will stop.<br>
<br>
<b><u>Corporate Cards, Electronic Devices (Laptops, PDAs, etc.) and Network
Access</u></b><o:p></o:p></span></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><span style="font-family: Symbol; font-size: 10pt; mso-ascii-font-family: Verdana;">·</span><span style='font-family: "Verdana","sans-serif"; font-size: 10pt;'>&nbsp; If you possess an American Express Corporate
charge card, Sprint FONcard, Verizon Conferencing card, or a P-Card, these may
be affected by your leave of absence. Please contact the Corporate Cards Group
with any questions by emailing them at corporatecards@deloitte.com.<o:p></o:p></span></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;">



















<span style='font-family: Symbol; font-size: 10pt; mso-bidi-font-family: "Times New Roman"; mso-fareast-font-family: Calibri; mso-ascii-font-family: Verdana; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;'>·</span><span style='font-family: "Verdana","sans-serif"; font-size: 10pt; mso-bidi-font-family: "Times New Roman"; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;'>&nbsp; The policies regarding laptop use and network
access while on leave are administered by your local office and Regional
Technology group. You are required to contact your local Talent representative
about any Deloitte provided devices (Laptops, PDAs, etc.) in your possession
prior to the beginning of your leave of absence.</span></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><span style='font-family: "Verdana","sans-serif"; font-size: 10pt; mso-bidi-font-family: "Times New Roman"; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;'><br></span></p><span style='font-family: "Verdana","sans-serif"; font-size: 10pt; mso-bidi-font-family: "Times New Roman"; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;'><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><b><u><span style='font-family: "Verdana",sans-serif; font-size: 10pt; mso-bidi-font-family: Calibri;'>Voluntary
Self-Identification of Disability</span></u></b></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><font face="Times New Roman" size="3">

<br></font></p><p style="margin: 0in 0in 0pt;"><span style='line-height: 115%; font-family: "Verdana",sans-serif; font-size: 10pt; mso-bidi-font-family: Calibri; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;'>As a reminder, because we are a government contractor, we have reporting
obligations regarding our efforts in reaching out to, hiring, and providing
equal opportunity to qualified people with disabilities. &nbsp;<b>Further, we
recognize the value of a diverse workforce and continue to promote awareness of
abilities inclusion across the organization</b>. &nbsp;To help us measure how
well we are doing, please consider updating your disability status, if
appropriate, to accurately reflect whether you have or have previously had a
disability.&nbsp; Your participation is voluntary and any information submitted
will not be used against you in any way.&nbsp; Click here for more information
or to update your status.<span style="color: rgb(31, 73, 125);">&nbsp; </span></span><span style='line-height: 115%; font-family: "Calibri",sans-serif; font-size: 11pt; mso-bidi-font-family: "Times New Roman"; mso-bidi-theme-font: minor-bidi; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;'><a href="https://talentondemand.deloittenet.deloitte.com/default.aspx#!/my-information/personal-information/"><span style='line-height: 115%; font-family: "Verdana",sans-serif; font-size: 10pt; mso-bidi-font-family: Calibri;'><font color="#0000ff">https://talentondemand.deloittenet.deloitte.com/default.aspx#!/my-information/personal-information/</font></span></a></span></p></span><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><font style="font-family: Verdana, Helvetica, sans-serif; font-size: small;"><br></font></p><p class="MsoNormal" style="color: rgb(0, 0, 0); font-family: arial; font-size: 12pt;"><font style="font-family: Verdana, Helvetica, sans-serif; font-size: small;">If you have any questions, please call&nbsp;</font><strong style="font-family: Verdana, Helvetica, sans-serif; font-size: small;">1-800-DELOITTE (1-800-335-6488)</strong><span style="font-family: Verdana, Helvetica, sans-serif; font-size: small;">&nbsp;and enter your Personnel Number, which is&nbsp;</span><b style="font-family: Verdana, Helvetica, sans-serif; font-size: small;">{!WCT_Leave__c.WCT_Personnel__c}</b><span style="font-family: Verdana, Helvetica, sans-serif; font-size: small;">. Any necessary forms can be faxed to&nbsp;</span><strong style="font-family: Verdana, Helvetica, sans-serif; font-size: small;">615.884.0860</strong><font style="font-family: Verdana, Helvetica, sans-serif; font-size: small;">.</font><br>
<br><font face="Verdana, sans-serif" size="2">
Thank you,</font><br>
<br>
<br>
<span style="color: navy; font-family: Arial, sans-serif; font-size: 8pt;">U.S.
Leaves Team | Talent<br>
Deloitte Services LP<br>
4022 Sells Drive, Hermitage, TN, 37076, USA<br>
Tel: +1 800 335 6488 | Fax: +1 615 884 0860<br>
<a href="www.deloitte.com">www.deloitte.com</a></span><br>
<br>
<span style="color: rgb(146, 212, 0); font-family: Arial, sans-serif; font-size: 8pt;">Please
consider the environment before printing.</span> <font face="arial" size="3"><o:p></o:p></font></p>]]></td>
</tr>
</table>