<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Analytics_Leadership_Priority__c</fullName>
        <externalId>false</externalId>
        <label>Analytics Leadership Priority</label>
        <picklist>
            <picklistValues>
                <fullName>High</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Medium</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Low</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Business_Case__c</fullName>
        <externalId>false</externalId>
        <label>Business Case</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Channel_Process_Area__c</fullName>
        <externalId>false</externalId>
        <label>Channel Process Area</label>
        <picklist>
            <picklistValues>
                <fullName>Talent Acquisition</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Performance Management</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Global Mobility and Immigration</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>SAP Reporting</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Learning Reporting</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Resource Management</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Survey</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Data from Multiple Source</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Description_of_the_Project__c</fullName>
        <externalId>false</externalId>
        <label>Description of the Project</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Efficiency_Gain__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Provide details of current reports getting cancelled because of the Analytics Project</inlineHelpText>
        <label>Efficiency Gain</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Estimated_Time_of_Closure__c</fullName>
        <externalId>false</externalId>
        <label>Estimated Time of Closure</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Name_of_the_Project__c</fullName>
        <externalId>false</externalId>
        <label>Name of the Project</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Project_Assigned_To_Analyst__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Project Assigned To (Analyst)</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Analytics Pipeline (Task Assigned To (Analyst))</relationshipLabel>
        <relationshipName>Analytics_Pipeline2</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Project_Assigned_to__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Project Assigned To (Manager)</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Analytics Pipeline (Task Assigned To)</relationshipLabel>
        <relationshipName>Analytics_Pipeline1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Requestor__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Requestor</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Analytics Pipeline</relationshipLabel>
        <relationshipName>Analytics_Pipeline</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>New</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>In-progress</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Closed</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Time_saved__c</fullName>
        <externalId>false</externalId>
        <label>Time saved (In Hrs)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Analytics Pipeline</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>Analytics_Pipeline</fullName>
        <columns>Name_of_the_Project__c</columns>
        <columns>Channel_Process_Area__c</columns>
        <columns>Requestor__c</columns>
        <columns>Project_Assigned_To_Analyst__c</columns>
        <columns>Project_Assigned_to__c</columns>
        <columns>Description_of_the_Project__c</columns>
        <columns>Status__c</columns>
        <columns>Business_Case__c</columns>
        <columns>Estimated_Time_of_Closure__c</columns>
        <columns>Analytics_Leadership_Priority__c</columns>
        <filterScope>Everything</filterScope>
        <label>Analytics Pipeline Projects</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>AP-{0000}</displayFormat>
        <label>Analytics Pipeline Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Analytics Pipeline</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Task_Assigned</fullName>
        <active>true</active>
        <errorConditionFormula>IF(AND((Project_Assigned_To_Analyst__c ==  Project_Assigned_to__c ),OR(NOT(ISBLANK(Project_Assigned_To_Analyst__c)),NOT(ISBLANK(Project_Assigned_to__c)))), true, false)</errorConditionFormula>
        <errorMessage>Task Assigned To (Analyst) cannot be same as Task Assigned To (Manager)</errorMessage>
    </validationRules>
</CustomObject>
