<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>WCT_Description__c</fullName>
        <externalId>false</externalId>
        <label>Description</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>WCT_Offer_Reason__c</fullName>
        <description>These are the &apos;Reason&apos; values for the different offer statuses - reference the dependent picklist tab for dependencies</description>
        <externalId>false</externalId>
        <label>Offer Reason</label>
        <picklist>
            <controllingField>WCT_Offer_Status__c</controllingField>
            <picklistValues>
                <fullName>Accepted counter-offer from current employer</fullName>
                <controllingFieldValues>Offer Declined</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Accepted offer for another position within the organization</fullName>
                <controllingFieldValues>Offer Declined</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Accepted offer from another employer - Big 4</fullName>
                <controllingFieldValues>Offer Declined</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Accepted offer from another employer - non Big 4</fullName>
                <controllingFieldValues>Offer Declined</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Job level not acceptable</fullName>
                <controllingFieldValues>Offer Declined</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Monetary compensation too low</fullName>
                <controllingFieldValues>Offer Declined</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Non-monetary employment package unsatisfactory (please specify)</fullName>
                <controllingFieldValues>Offer Declined</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Not willing to relocate / Office location is unsuitable</fullName>
                <controllingFieldValues>Offer Declined</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Not willing to travel</fullName>
                <controllingFieldValues>Offer Declined</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Not interested in this position / Not in line with career goals</fullName>
                <controllingFieldValues>Offer Declined</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Personal reasons given</fullName>
                <controllingFieldValues>Offer Declined</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Process took too long</fullName>
                <controllingFieldValues>Offer Declined</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Pursuing additional education</fullName>
                <controllingFieldValues>Offer Declined</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Pursing leadership assignment (internal candidates only)</fullName>
                <controllingFieldValues>Offer Declined</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Staying in current position</fullName>
                <controllingFieldValues>Offer Declined</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>No reason given</fullName>
                <controllingFieldValues>Offer Declined</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Does not meet preferred skills for position</fullName>
                <controllingFieldValues>Offer Revoked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Does not meet the required criteria for the position: education level/grade average</fullName>
                <controllingFieldValues>Offer Revoked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Does not meet the required criteria for the position: relevant skills/experience</fullName>
                <controllingFieldValues>Offer Revoked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Failed testing/assessment</fullName>
                <controllingFieldValues>Offer Revoked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Incomplete profile</fullName>
                <controllingFieldValues>Offer Revoked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Independence issues</fullName>
                <controllingFieldValues>Offer Revoked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Internal applicant is not eligible to transfer</fullName>
                <controllingFieldValues>Offer Revoked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Matched to another requisition</fullName>
                <controllingFieldValues>Offer Revoked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>More qualified external candidate selected</fullName>
                <controllingFieldValues>Offer Revoked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>More qualified internal candidate selected</fullName>
                <controllingFieldValues>Offer Revoked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>No show for interview</fullName>
                <controllingFieldValues>Offer Revoked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Not eligible to work in this country / immigration barriers</fullName>
                <controllingFieldValues>Offer Revoked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Offer not approved by Deloitte-terms not acceptable</fullName>
                <controllingFieldValues>Offer Revoked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Position was canceled</fullName>
                <controllingFieldValues>Offer Revoked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Position unavailable</fullName>
                <controllingFieldValues>Offer Revoked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Salary expectations are not in line with the position</fullName>
                <controllingFieldValues>Offer Revoked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Unsatisfactory background check</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Did not complete the hiring process</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>WCT_Offer_Status__c</fullName>
        <description>Offer to Be Extended - Brand new offer object generated for India Analyst to start draft of offer/manual updates to manual fields
 Draft In Progress - India Annalyst is in progress of making manual updates to fields
 Offer Details Revised - Offer has been sent back to the IA for revisions 
Offer Approved - Offer has been generated and approved by the Recruiter 
Offer Sent - Offer has been sent to the Candidate 
Offer Accepted - Candidate Recieved the Offer, and accepted the Offer 
Offer Declined - Candidate has recieved Offer, and declined the Offer 
Offer Revoked - Offer has been sent to the Candidate, and Deloitte has decided to rescind offer
 Offer in Negotiation - Offer has been sent to the candidate and candidate decided to negotiate offer
 Returned for Revisions - Recruiter has reviewed offer but it requires revisions either in Taleo OR SFDC
 Pending - Offer is sent to the Candidate and waiting for a response from the Candidate</description>
        <externalId>false</externalId>
        <label>Offer Status</label>
        <picklist>
            <picklistValues>
                <fullName>Offer to Be Extended</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Offer Approved</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Offer Sent</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Offer Accepted</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Offer Declined</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Offer Revoked</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Ready For Review</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Draft in Progress</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Returned for Revisions</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Offer Cancelled</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Manual (Outside of SFDC)</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>WCT_Offer__c</fullName>
        <externalId>false</externalId>
        <label>Offer</label>
        <referenceTo>WCT_Offer__c</referenceTo>
        <relationshipLabel>Offer Notes</relationshipLabel>
        <relationshipName>Offer_Notes</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>WCT_Subject__c</fullName>
        <externalId>false</externalId>
        <label>Subject</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Offer Note</label>
    <nameField>
        <displayFormat>ON-{0000}</displayFormat>
        <label>Offer Note Id</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Offer Notes</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <webLinks>
        <fullName>WCT_New_Offer_Note</fullName>
        <availability>online</availability>
        <description>This button is created to auto populate the Note Status with Offer Status</description>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New Offer Note</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>/{!$Label.WCT_OfferNote_ObjectId}/e?{!$Label.WCT_NewNoteOfferLookupId}={!WCT_Offer__c.Name}&amp;{!$Label.WCT_NewNoteOfferLookupId}_lkid={!WCT_Offer__c.Id}&amp;{!$Label.WCT_NewNoteOfferStatusId}={!WCT_Offer__c.WCT_status__c}&amp;retURL=%2F{!WCT_Offer__c.Id}</url>
    </webLinks>
</CustomObject>
