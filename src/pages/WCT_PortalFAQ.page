<apex:page showHeader="false" sidebar="false" standardStylesheets="false" >
<head>
    <link rel="shortcut icon" href="/resource/1388209558000/InterviewPortalStyle/css/images/favicon.ico" />
    <apex:stylesheet value="{!URLFOR($Resource.InterviewPortalStyle, '/css/style.css')}"/>
    <apex:includeScript value="{!URLFOR($Resource.InterviewPortalStyle, '/js/jquery-1.10.2.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.InterviewPortalStyle, 'js/jquery.tablesorter.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.InterviewPortalStyle, '/js/modernizr.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.InterviewPortalStyle, '/js/functions.js')}"/>
</head>
<body>
    <div class="wrapper">
            <c:WCT_InterviewPortalHeader />
        <section class="main">
            <div class="box box-faqs">
                <h2 class="box-headline">FAQs</h2>
                <div class="box-content">
                    <ul>

                        <li class="entry">
                            <div class="clearfix">
                                <span>Q:</span><p><strong>What is my responsibility as an Interviewer?</strong></p>
                            </div>
                            <div class="clearfix">
                                <span>A:</span><p>Your primary responsibility will be to conduct your interview during the start and end times set by your Recruiting Coordinator on the Upcoming Interviews Page, past, and future interviews. Once an interview is conducted, you will be responsible for filling out an Interview Evaluation Form for each candidate and submit via the portal within 3 days of conducting the interview.</p>
                            </div>
                        </li>
                        <li class="entry">
                            <div class="clearfix">
                                <span>Q:</span><p><strong>What if I do not have internet access during the interview to complete the online Interview Evaluation Form? Can I fill out this form offline?</strong></p>
                            </div>
                            <div class="clearfix">
                                <span>A:</span><p>Upon scheduling an interview, eInterview will send you a meeting invite containing an offline Word version of the evaluation form, if an offline version is available. You may complete this form offline and upload to the interview record via the Portal. Alternatively, you may download the offline Word document by navigating to the Interview detail page on the portal (by clicking on the Interview Number).</p>
                            </div>
                        </li>
                        <li class="entry">
                            <div class="clearfix">
                                <span>Q:</span><p><strong>How long do I have to complete an Interview Evaluation Form?</strong></p>
                            </div>
                            <div class="clearfix">
                                <span>A:</span><p>You will have up to 3 days after you’ve conducted an interview to submit your Interview Evaluation Form. Once you start filling out a form, you can save the draft for 48 hours for further edits. If you do not return to the form within the 48 hour period, your latest updates will automatically be submitted to Salesforce.</p>
                            </div>
                        </li>
                        <li class="entry">
                            <div class="clearfix">
                                <span>Q:</span><p><strong>What if I need to reschedule an interview last minute?</strong></p>
                            </div>
                            <div class="clearfix">
                                <span>A:</span><p>Please work with your Recruiting Coordinator to reschedule an interview. You can view the Recruiting Coordinators assigned to your specific interviews in the All Interviews Page. Clicking on the Recruiting Coordinator’s name will open a new Outlook email window with the coordinator’s name per-populated.</p>
                            </div>
                        </li>
                        
                        <li class="entry">
                            <div class="clearfix">
                                <span>Q:</span><p><strong>What does the Action Item tab represent on the Interviewer Portal?</strong></p>
                            </div>
                            <div class="clearfix">
                                <span>A:</span><p>Any interviews that require your attention.  For instance, completed interviews that do not have a completed interview evaluation form.</p>
                            </div>
                        </li>
                        
                        <li class="entry">
                            <div class="clearfix">
                                <span>Q:</span><p><strong>What does the Upcoming Interviews tab represent on the Interviewer Portal?</strong></p>
                            </div>
                            <div class="clearfix">
                                <span>A:</span><p>Interviews that are scheduled for you to conduct.  This tab will only have interviews in the future.  If an interview was scheduled for 30 minutes ago it would have moved to the All Interviews tab and the Actions Item tab if the evaluation form is still outstanding.</p>
                            </div>
                        </li>
                        
                        <li class="entry">
                            <div class="clearfix">
                                <span>Q:</span><p><strong>What does the All Interviews tab represent on the Interviewer Portal?</strong></p>
                            </div>
                            <div class="clearfix">
                                <span>A:</span><p>A summary of all interviews where you are the interviewer (past and future).</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section><!-- /.main -->
    </div><!-- /.wrapper -->
</body> 
</apex:page>