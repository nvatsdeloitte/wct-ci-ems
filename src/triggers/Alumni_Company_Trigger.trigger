trigger Alumni_Company_Trigger on Company__c (before insert, before update, after insert,after update ) {
if(Trigger.isbefore){
    new CompanyTriggerHandler_AT().assignCompanyName(Trigger.New);
    id clientRecTypeId= Schema.SObjectType.Company__c.getRecordTypeInfosByName().get('Client').getRecordTypeId();  
    id nonclientRecTypeId= Schema.SObjectType.Company__c.getRecordTypeInfosByName().get('Non-Client').getRecordTypeId(); 
    for(Company__c c:Trigger.New){
        if(c.Client_Account_Group_Code__c =='Member Firm' || c.Client_Account_Group_Code__c =='Prospective Client' || c.Client_Account_Group_Code__c =='' || c.Client_Account_Group_Code__c==null){
            c.RecordTypeId =nonclientRecTypeId;
        }
        else
        {
            c.RecordTypeId =clientRecTypeId;
        }
    }
}
}