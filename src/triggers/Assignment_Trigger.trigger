trigger Assignment_Trigger on WCT_Assignment__c (before insert, after insert,before update, after update) {

    if (Trigger.isafter && trigger.isInsert)
    
    {
    RenewPassTask.RenewPass(Trigger.new);

    }    
   
    if(Trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert))
    {
        WCT_Assignment_Update_Project_Manager.updateProjectManager(Trigger.new);
        
        
    }    
      if(Trigger.isUpdate) {
            List<String> assignmentList = new List<String>();
            Map<Id, String> mobilityAssigmentTypeMap = new Map<Id, String>();
            
            for(WCT_Assignment__c  assignmentRec : Trigger.new) {            
             //  if((Trigger.newmap.get(assignmentRec.id).WCT_Mobility_Assignment_Type__c) != (Trigger.oldmap.get(assignmentRec.id).WCT_Mobility_Assignment_Type__c)){
                   if(assignmentRec.WCT_Status__c == 'Active') {
                    assignmentList.add(assignmentRec.WCT_Mobility__c);
                    mobilityAssigmentTypeMap.put(assignmentRec.WCT_Mobility__c, assignmentRec.WCT_Mobility_Assignment_Type__c);
           //   }
               }        
            }
            if(!assignmentList.isEmpty()){
               List<WCT_Mobility__c> recrptUpdate = [SELECT id, Name, WCT_Assignment_Type__c FROM WCT_Mobility__c WHERE id IN:assignmentList];
               List<WCT_Mobility__c> recUpdateList = new List<WCT_Mobility__c>();
               
               for (WCT_Mobility__c rec : recrptUpdate) {
                   rec.WCT_Assignment_Type__c = mobilityAssigmentTypeMap.get(rec.id);
                   recUpdateList.add(rec);
               }
               
               if(!recUpdateList.isEmpty()) {
                   update recUpdateList;
               } 
            } 
        }          
}