trigger CER_ExpenseTrigger on CER_Expense_Reimbursement__c (before insert, after insert, before update, after update) 
{

    /*
     * Trigger is Before logic will go here.
	 */
    if(Trigger.isBefore)
    {
       /*Trigger is Insert And is Before*/
        if(Trigger.isInsert)
        {
            CER_ExpenseTriggerHandler.syncGroupWithRecordType(Trigger.New);
            CER_ExpenseTriggerHandler.flagForReview(Trigger.New);
        }
        /*Trigger is Update And is Before*/
        if(Trigger.isUpdate)
        {
            CER_ExpenseTriggerHandler.syncGroupWithRecordType(Trigger.New);
            CER_ExpenseTriggerHandler.checkForDupeExp(Trigger.New, Trigger.Old);
            CER_EXpenseTriggerHandler.updateTimeStamps(Trigger.New, Trigger.Old);
            CER_EXpenseTriggerHandler.updateTotalProcessingTime(Trigger.New, Trigger.Old);
        }
        
    }
    
    /*
     * Trigger is After logic will go here.
	 */
    System.debug('### Called'+Trigger.new);
    if(Trigger.isAfter)
    {
        /*Trigger is Insert And is After*/
         if(Trigger.isInsert)
        {
            
        }
         /*Trigger is Update And is After*/
        if(Trigger.isUpdate)
        {
            
        }
        
    }
    
    
}