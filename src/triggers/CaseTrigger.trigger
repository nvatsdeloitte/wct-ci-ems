/*****************************************************************************************
    Name    : CaseTrigger
    Desc    : A trigger on case. All the trigger logics will be written in Case_Utility class and calls to that class will be made from this trigger.
    Approach: 
                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Rahul Agarwal               30 Aug, 2013         Created 
******************************************************************************************/
trigger CaseTrigger on Case (before update, before insert, after update, after insert) {
    // all the insert events
    List<Case> caseToAcknowledgeList = new List<Case>();//Used in Acknowledgement functionality
    list<Case> lstCaseToWork= new list<Case>();
    set<id> lstCaseToWorkId= new set<id>();
    list<Task> lsttaskToUpdate= new  list<Task>();
    String involuntaryRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(WCT_UtilConstants.CASE_EMPLOYEE_INVOL_RC).getRecordTypeId();
    String voluntaryRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(WCT_UtilConstants.CASE_EMPLOYEE_VOL_RC).getRecordTypeId();
    String processRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Process Issue').getRecordTypeId();
    String changeRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change Control Request').getRecordTypeId();
    Map<Id,String> oldOwnerMap=new Map<Id,String>();
    String userKeyPrefix = Schema.getGlobalDescribe().get('User').getDescribe().getKeyPrefix();
                     

    map<id, id> map_case_owner_ids= new map<id,id>();
    if(trigger.isInsert){
        // all the before insert events
        if(trigger.isBefore){
            // To encrypt Subject and Description when Case record type is either Talent Relations - Concern or Employee Lifecycle Events - Voluntary 
            // Separations or Employee Lifecycle Events - Involuntary Separations
            Case_Utility.updateCaseSubjectAndDes(trigger.new);
            Case_Utility.setDueDateForDeliveryCases(trigger.new);
            Case_Utility.populateContactOnCase(trigger.new);
            Case_Utility.populateCaseOwner(trigger.new);
            for(Case caseRec:Trigger.new)
            {
              if((caseRec.recordTypeId == voluntaryRecordTypeId || caseRec.recordTypeId == involuntaryRecordTypeId) && caseRec.WCT_Send_Email_On__c==null)
                caseRec.WCT_Send_Email_On__c=System.now();
            }
            //Start AcknowledgeMent Mails
            for(Case caseRecLON:Trigger.new){
            if(caseRecLON.WCT_Category__c != null && caseRecLON.Priority != null && !caseRecLON.WCT_IsAcknowledgeMent_sent__c){
                caseToAcknowledgeList.add(caseRecLON);    
            }
            }
            if(!caseToAcknowledgeList.isEmpty()){
               Case_Utility.populateLONOnCase(caseToAcknowledgeList);
            }
            //End AcknowledgeMent Mails
        }
        // all the after insert events
        if(trigger.isAfter){
           Case_Utility.AtatchArticleOnCase(Trigger.new);
        }
        //Added by Deepthi toranala on 5/26/2015
        TRT_Reportingtrigger_ctrl.isInsert=true;// static variable used to avoid recurssive 

    }
    // all the update events
    if(trigger.isUpdate){
        // all the before update events
        if(trigger.isBefore){
            if(!Case_Utility.beforeTrigger){
                Case_Utility.caseRecordTypeChanged(trigger.newMap, trigger.oldMap);
            }
            Case_Utility.setDueDateForDeliveryCases(trigger.new);
            Case_Utility.sendSMSEmailToCIC(trigger.new);
            //Case_Utility.populateCaseOwner(trigger.new);
            for(Case caseRec:Trigger.new)
            {
              if((caseRec.recordTypeId == voluntaryRecordTypeId || caseRec.recordTypeId == involuntaryRecordTypeId) && caseRec.WCT_Send_Email_On__c==null && caseRec.recordTypeId != processRecTypeId && caseRec.recordTypeId != changeRecTypeId)
                caseRec.WCT_Send_Email_On__c=System.now();
            }
            //Start AcknowledgeMent Mails before Update
            for(Case caseRecLON:Trigger.new){
            if(caseRecLON.WCT_Category__c != null && caseRecLON.Priority != null && !caseRecLON.WCT_IsAcknowledgeMent_sent__c){
                caseToAcknowledgeList.add(caseRecLON);    
            }
            }
            if(!caseToAcknowledgeList.isEmpty()){
               Case_Utility.populateLONOnCase(caseToAcknowledgeList);
            }
            new Case_Utility().CaseOwnerChanged(Trigger.New,Trigger.oldMap);// Will set Case OwnerChangeWrg Flag true
            //End AcknowledgeMent Mails before Update
            // Added by Deepthi Toranala 5/26/2015
            TRT_Reportingtrigger_ctrl.caseBeforeUpdate(trigger.new,trigger.old);
        }
        // all the after update events
        if(trigger.isAfter){
            String userProfileId = userinfo.getProfileId().substring(0,15);
            /*
            if(userProfileId == System.Label.CIC_Agent_Profile_Id || userProfileId == System.Label.CIC_Manager_Profile_Id){
                 Case_Utility.runAssignmentRule(Trigger.OldMap, Trigger.new);
            }
            */
            // owner casecaseing 
            for(Case Loop_NewCase: Trigger.new)
            {
                 if((Loop_NewCase.OwnerId!= Trigger.OldMap.get(Loop_NewCase.Id).OwnerId)&& (Loop_NewCase.recordTypeId == voluntaryRecordTypeId || Loop_NewCase.recordTypeId == involuntaryRecordTypeId ))
                     {
                     String OwernId = Loop_NewCase.OwnerId;
                     OwernId=OwernId.substring(0,3);
                     if(OwernId==userKeyPrefix)
                     {
                         lstCaseToWork.add(Loop_NewCase);
                         lstCaseToWorkId.add(Loop_NewCase.id);
                         map_case_owner_ids.put(Loop_NewCase.id,Loop_NewCase.OwnerId);
                         
                     }
                     String oldOwnerId=Trigger.OldMap.get(Loop_NewCase.Id).OwnerId;
                     oldOwnerId=oldOwnerId.substring(0,3);
                     if(oldOwnerId!=userKeyPrefix)
                     {
                       oldOwnerMap.put(Loop_NewCase.id,'Queue');  
                     }
                     
                     }
                
            }
            
            List<Task> lstOpenActivityToupdate=[select id, whatId,ownerId from task where (WhatId IN : lstCaseToWorkId) AND (Status!=: WCT_UtilConstants.TASK_COMPLETED_STATUS AND Status!=: WCT_UtilConstants.TASK_DEFERRED_STATUS) ];
            for(task loop_Task : lstOpenActivityToupdate)
            {
                 for(Case loop_Case: lstCaseToWork)
                 {
                     if((loop_Case.id==loop_Task.WhatId) && (loop_Task.ownerId == loop_Case.ownerId || oldOwnerMap.containsKey(loop_Case.Id)))
                     {
                        loop_Task.OwnerId = map_case_owner_ids.get(loop_Case.Id);
                        lsttaskToUpdate.add(loop_Task);
                     }
                 }
            }
            Database.update(lsttaskToUpdate);
            //Added by Deepthi on may 25th
            TRT_Reportingtrigger_ctrl.caseAfterUpdate(trigger.new,trigger.old);
          }
    }
}