/**************************************************************************************
Apex Trigger Name:  ClearanceTrigger 
Version          : 1.0 
Created Date     : 15 April 2015
Function         : Trigger to create assets and to updateremainderdates
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte / Sandeep        15/04/2015             Original Version
*************************************************************************************/
trigger ClearanceTrigger on Clearance_separation__c (after insert, before insert, before update) {
        
        
        if(Trigger.isAfter &&  Trigger.isInsert)
        {
        
           ELE_CreateClearanceClass.createAsset(Trigger.new);
      
        }
    if(Trigger.isBefore && (Trigger.IsInsert|| Trigger.Isupdate))
    {
        ELE_CreateClearanceClass.updateReminderDate(Trigger.new);
    }
    
    
}