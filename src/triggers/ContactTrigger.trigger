trigger ContactTrigger on Contact (before insert, before update, after insert,after update, after delete ) {

   system.debug('%%%%%%%%%%% Trigger  Started');
    if(Trigger.isbefore){
        system.debug('%%%%%%%%%%% Trigger isBefore');
        new ContactTriggerHandler_AT().PopulateAccount(Trigger.New);
        system.debug('**************'+system.Label.Talent_Integration_eEvents_User_Id+'%%%%%%%%%%%%'+userinfo.getuserid());
        if(system.Label.Talent_Integration_eEvents_User_Id == userinfo.getuserid()){
         new ContactTriggerHandler_AT().assignSchoolName(Trigger.New);
        }
        new ContactTriggerHandler_AT().assignCompanyName(Trigger.New, Trigger.Old);
        new ContactTriggerHandler_AT().CheckDuplicateEmail(Trigger.New);
        
        if(Trigger.isInsert){
            system.debug('%%%%%%%%%%% Trigger isBefore isInsert started');
            
            new ContactTriggerHandler_AT().AddMergeCandidateOnCandidate(Trigger.New);
            new ContactTriggerHandler_AT().CurrencyNumberFormat(Trigger.New,new Map<id,contact>());
            system.debug('%%%%%%%%%%% Trigger isBefore isInsert Ended');
        }
        if(Trigger.isUpdate){
            system.debug('%%%%%%%%%%% Trigger isBefore isUpdate Started');
            // TO fix the issue where Employee already exists in SFDC and applies for poistion
           
      ContactTriggerHandler_AT.IsChanged(Trigger.New,Trigger.oldMap);
      new ContactTriggerHandler_AT().CurrencyNumberFormat(Trigger.New,Trigger.oldMap);
      new ContactTriggerHandler_AT().avoidEmployeeToContactOnTaleoLoad(Trigger.New, Trigger.oldMap);
            system.debug('%%%%%%%%%%% Trigger isBefore isUpdate Ended');
        }
        
             system.debug('%%%%%%%%%%% Trigger isBefore Ended');
  
    }
    
    if(Trigger.isAfter){
        system.debug('%%%%%%%%%%% Trigger isafter Started');
    if(Trigger.isInsert){
        system.debug('%%%%%%%%%%% Trigger isafter isInsert Started');
        
      if(SYSTEM.Label.Integration_Profile_Id == userInfo.getProfileId()){
        //new ContactTriggerHandler_AT().MergeAdhocCandidateToCandidate(Trigger.new);
        new ContactTriggerHandler_AT().InsertOfferLetteronConInsert(Trigger.new);
       
        if(!system.isFuture()){
            new ContactTriggerHandler_AT().handleTaleoLoad(Trigger.new);
        }
      }
       /*Method calling to update companies  Alumni present check and alumni count.*/
       new ContactTriggerHandler_AT().updateCompanyAlumniPresent(Trigger.new, null,ContactTriggerHandler_AT.INSERT_REQUEST);
        system.debug('%%%%%%%%%%% Trigger isafter isInsert Ended');
    }
    if(Trigger.isUpdate){
        system.debug('%%%%%%%%%%% Trigger isafter isUpdate Started');
      new ContactTriggerHandler_AT().InsertOfferLetter(Trigger.new,Trigger.oldMap);
      new ContactTriggerHandler_AT().updateWrittenOfferExtendedDate(Trigger.new,Trigger.oldMap);
      if(SYSTEM.Label.Integration_Profile_Id == userInfo.getProfileId()){
          if(!system.isFuture()){
            new ContactTriggerHandler_AT().handleTaleoLoad(Trigger.new);
          }
      }
        /*Method calling to update companies  Alumni present check and alumni count.*/
       new ContactTriggerHandler_AT().updateCompanyAlumniPresent(Trigger.new, Trigger.old,ContactTriggerHandler_AT.UPDATE_REQUEST);
        system.debug('%%%%%%%%%%% Trigger isafter isUpdate Ended');
        
    }
          if(Trigger.isDelete)
    {
        system.debug('%%%%%%%%%%% Trigger isafter isDelete Started');
        /*Method calling to update companies  Alumni present check and alumni count. In case of deleting a contact*/
       new ContactTriggerHandler_AT().updateCompanyAlumniPresent(null, Trigger.old,ContactTriggerHandler_AT.DELETE_REQUEST);
        system.debug('%%%%%%%%%%% Trigger isafter isDelete Ended');
    }
   system.debug('%%%%%%%%%%% Trigger isafter Ended');
  }

    system.debug('%%%%%%%%%%% Trigger  Ended');
}