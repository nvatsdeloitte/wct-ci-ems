/**************************************************************************************
Apex Trigger Name:  ELE_Separation_Trigger
Version          : 1.0 
Created Date     : 13 April 2015
Function         : Trigger to create exit interview and update address
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte / Sandeep        13/04/2015             Original Version
*************************************************************************************/
trigger ELE_Separation_Trigger on ELE_Separation__c (after insert, after update, before insert, before update) 
{
    
    
    if(Trigger.IsInsert && Trigger.IsAfter)
    {
        ELE_CreateClearanceClass.CreateExitInterview(Trigger.new);
    }  
    if((Trigger.IsInsert || Trigger.Isupdate )&& Trigger.IsBefore)
    {
        ELE_CreateClearanceClass.updateAddress(Trigger.new);
    }
    
}