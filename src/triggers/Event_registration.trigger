trigger Event_registration on Event_Registration_Attendance__c (before insert, before update,after insert,after update) {
    
    String GMIRecordTypeId=Schema.SObjectType.Event_Registration_Attendance__c.getRecordTypeInfosByName().get('GM&I').getRecordTypeId();
    // String ARRecordTypeId=Schema.SObjectType.Event_Registration_Attendance__c.getRecordTypeInfosByName().get('Alumni Relation').getRecordTypeId();
    
     Set<Id> ImmIds = new Set<Id>();
     Set<Id> MobIds = new Set<Id>();
     
     Set<Id> ImmUpdateIds = new Set<Id>();
     Set<Id> MobupdateIds = new Set<Id>();
     
     if(Trigger.isBefore){
     // wct_checkinAR.populateImageURL(Trigger.new);
     WCT_Eevents_TriggerHandler.populatecourseNumber(Trigger.new);
     }
    
    if(trigger.isAfter && (trigger.isUpdate || trigger.isInsert)){
       
        for(Event_Registration_Attendance__c EveReg:Trigger.new)
        {
             if(Trigger.isInsert && EveReg.WCT_ImmigrationName__c != null && EveReg.Attended__c==true &&(EveReg.RecordTypeId==GMIRecordTypeId))
            {    
               ImmIds.add(EveReg.WCT_ImmigrationName__c);
            }
            if(Trigger.isInsert && EveReg.WCT_MobilityName__c!= null && EveReg.Attended__c==true &&(EveReg.RecordTypeId==GMIRecordTypeId))
            {    
               MobIds.add(EveReg.WCT_MobilityName__c);
            }
            if(Trigger.isUpdate && EveReg.WCT_ImmigrationName__c!= null && EveReg.Attended__c==true &&(EveReg.RecordTypeId==GMIRecordTypeId))
            {
                ImmUpdateIds.add(EveReg.WCT_ImmigrationName__c);
            } 
            if(Trigger.isUpdate && EveReg.WCT_MobilityName__c!= null && EveReg.Attended__c==true &&(EveReg.RecordTypeId==GMIRecordTypeId))
            {
                MobupdateIds.add(EveReg.WCT_MobilityName__c);
            }  
         }
        
        }
   if(!ImmIds.IsEmpty()){
            WCT_Eevents_TriggerHandler.ImmTaskInsert(ImmIds);
            
   }  
   if(!MobIds.IsEmpty()){
       WCT_Eevents_TriggerHandler.MobTaskInsert(MobIds);
            
   }  
   if(!ImmUpdateIds.IsEmpty()){
       WCT_Eevents_TriggerHandler.ImmTaskUpdate(ImmUpdateIds);
            
   }  
   if(!MobupdateIds.IsEmpty()){
           WCT_Eevents_TriggerHandler.MobTaskUpdate(MobupdateIds);
               } 
     List<Id> ImmcntIds = new List<Id>();
     List<Id> UpdateImmcntIds = new List<Id>();
     
     List<Id> MobcntIds = new List<Id>();
     List<Id> UpdateMobcntIds = new List<Id>();
     
     if(trigger.isAfter && (trigger.isUpdate)){
       
        for(Event_Registration_Attendance__c EveReg:Trigger.new)
        {
                
            if(Trigger.isUpdate && EveReg.WCT_ImmigrationName__c!= null && EveReg.Unattended__c!=Trigger.oldMap.get(EveReg.Id).Unattended__c && EveReg.Unattended__c==true &&(EveReg.RecordTypeId==GMIRecordTypeId) && EveReg.WCT_Is_immigration_record__c==true)
            {
                ImmcntIds.add(EveReg.WCT_ImmigrationName__c);
                system.debug('test0'+ImmcntIds);
            } 
            if(Trigger.isUpdate && EveReg.WCT_ImmigrationName__c!= null && EveReg.Unattended__c!=Trigger.oldMap.get(EveReg.Id).Unattended__c && EveReg.Unattended__c==false &&(EveReg.RecordTypeId==GMIRecordTypeId) && EveReg.WCT_Is_immigration_record__c==true)
            {
                UpdateImmcntIds.add(EveReg.WCT_ImmigrationName__c);
               // system.debug('test1'+UpdateImmcntIds);
            }
            
            if(Trigger.isUpdate && EveReg.WCT_MobilityName__c!= null && EveReg.Attended__c==Trigger.oldMap.get(EveReg.Id).Attended__c && EveReg.Attended__c==false &&(EveReg.RecordTypeId==GMIRecordTypeId) && EveReg.WCT_Is_Mobility_record__c==true)
            {
                MobcntIds.add(EveReg.WCT_MobilityName__c);
            } 
            if(Trigger.isUpdate && EveReg.WCT_MobilityName__c!= null && EveReg.Attended__c!=Trigger.oldMap.get(EveReg.Id).Attended__c && EveReg.Attended__c==true &&(EveReg.RecordTypeId==GMIRecordTypeId) && EveReg.WCT_Is_Mobility_record__c==true)
            {
                UpdateMobcntIds.add(EveReg.WCT_MobilityName__c);
            }  
         }
        
        } 
        
        if(!ImmcntIds.IsEmpty()){
                 system.debug('test1'+ImmcntIds);
               WCT_Eevents_TriggerHandler.ImmcntInsert(ImmcntIds); 
            
        } 
        
        if(!UpdateImmcntIds.IsEmpty()){
            WCT_Eevents_TriggerHandler.ImmcntUpdate(UpdateImmcntIds); 
            
        }   
            
        if(!MobcntIds.IsEmpty()){
            WCT_Eevents_TriggerHandler.MobcntInsert(MobcntIds); 
            
        } 
        
        if(!UpdateMobcntIds.IsEmpty()){
            WCT_Eevents_TriggerHandler.MobcntUpdate(UpdateMobcntIds); 
            
        }       
   
}