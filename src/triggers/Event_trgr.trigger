trigger Event_trgr on Event (before insert, before update, after insert) {
	if(trigger.isAfter && trigger.isInsert){
		Set<Id> CaseIds = new Set<Id>();
	    list<Case> caseRec = new list<Case>();
	    for(Event t : Trigger.new){
	        CaseIds.add(t.WhatId);
	    }
	    Case_Utility.updateCaseStatus_InProgress(CaseIds);
	}
    if(trigger.isBefore){
		Set<Id> CaseIds = new Set<Id>();
		list<Case> caseList = new list<Case>();
		if(trigger.isInsert){
			for(Event e : trigger.new){
				String caseId = e.WhatId;
				if(e.WhatId <> null && caseId.left(3) == '500'){
					CaseIds.add(e.WhatId);
				}
			}
			caseList = [select Id from Case where Id IN : CaseIds AND IsClosed = true];
			for(Case c : caseList){
				for(Event e : trigger.new){
					if(c.Id == e.WhatId){
						e.addError('You cannot create/edit Task on a closed case');
					}
				}
			}
		}
		if(trigger.isUpdate){
			system.debug('<><><>' + 'I am here');
			for(Event e : trigger.old){
				String caseId = e.WhatId;
				if(e.WhatId <> null && caseId.left(3) == '500'){
					CaseIds.add(e.WhatId);
				}
			}
			caseList = [select Id from Case where Id IN : CaseIds AND IsClosed = true];
			for(Case c : caseList){
				for(Event e : trigger.old){
					if(c.Id == e.WhatId){
						for(Event ne : trigger.new){
							if(ne.Id == e.Id)
							ne.addError('You cannot create/edit Task on a closed case');
						}
					}
				}
			}
		}
		
	}
}