trigger ExpenseLineItemTrigger on CER_Expense_Line_Item__c (before update, before insert) {

if(Trigger.isBefore && (Trigger.isUpdate))
{
	CER_ExpenseLineItemHandler.updateAcceptedByAndOn(Trigger.New, Trigger.Old);

}

}