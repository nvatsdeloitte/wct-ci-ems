trigger FMLA on WCT_FMLA_Hours__c (before update, before insert, after insert, after update, after delete) {

////////////////////////////////Roll up summary of FMLA hours and state FMLA hours into parental leave record when record is creating///////////////////////////

if(Trigger.isbefore && trigger.isinsert||Trigger.isbefore && trigger.isupdate)
{
    for(WCT_FMLA_Hours__c FMLAA :trigger.new){
    
     if(FMLAA.WCT_FMLA_Hours_Used__c==null)
      FMLAA.WCT_FMLA_Hours_Used__c=0;
        
     if(FMLAA.WCT_state_FMLA_Hours_Used__c==null)
      FMLAA.WCT_state_FMLA_Hours_Used__c=0;  
    }
}
if(Trigger.isafter && trigger.isinsert){

Map<id,decimal> StateFMLATOTAL = new Map<id,decimal>();
Map<id,decimal> FMLATOTAL = new Map<id,decimal>();
list<WCT_Leave__c> FMLA= new list<WCT_Leave__c>();
list<WCT_FMLA_Hours__c> fmlahours= new list<WCT_FMLA_Hours__c>();
    
    for(WCT_FMLA_Hours__c FMLAA :trigger.new){
   
     if(FMLAA.WCT_FMLA_Hours_Used__c!=null )
      FMLAtotal.put(FMLAA.WCT_Associated_Leave__c,FMLAA.WCT_FMLA_Hours_Used__c);
     
     if(FMLAA.WCT_state_FMLA_Hours_Used__c!=null )
      StateFMLAtotal.put(FMLAA.WCT_Associated_Leave__c,FMLAA.WCT_State_FMLA_Hours_Used__c);
    }
    
    for(wct_leave__c a:[SELECT id,Wct_FMLA_Used_Over_the_Past_12_Months1__c,state_FMLA_Used_Over_the_Past_12_Months__c 
                          FROM wct_leave__c 
                          WHERE id in:FMLAtotal.keyset() ]){
     if(a.state_FMLA_Used_Over_the_Past_12_Months__c ==null)
       a.state_FMLA_Used_Over_the_Past_12_Months__c =0;
     
     if(a.Wct_FMLA_Used_Over_the_Past_12_Months1__c ==null)
      a.Wct_FMLA_Used_Over_the_Past_12_Months1__c =0;
    
     if(a.Wct_FMLA_Used_Over_the_Past_12_Months1__c !=null){
      a.Wct_FMLA_Used_Over_the_Past_12_Months1__c+=FMLAtotal.get(a.id);}
     
     if(a.state_FMLA_Used_Over_the_Past_12_Months__c !=null){
      a.state_FMLA_Used_Over_the_Past_12_Months__c+=stateFMLAtotal.get(a.id);}
        FMLA.add(a);
    }
        update FMLA;
}

////////////////////////////////Roll up summary of FMLA hours and state FMLA hours into parental leave record when record is updating///////////////////////////

if( Trigger.isafter && trigger.isupdate )
{
Map<id,decimal> FMLATOTALNEW = new Map<id,decimal>();
Map<id,decimal> FMLATOTALOLD = new Map<id,decimal>();
Map<id,decimal> StateFMLATOTALNEW = new Map<id,decimal>();
Map<id,decimal> StateFMLATOTALOLD = new Map<id,decimal>();
list<WCT_Leave__c> FMLA= new list<WCT_Leave__c>();



    for(WCT_FMLA_Hours__c NEWFMLA :trigger.new){
     
     if(NEWFMLA.WCT_FMLA_Hours_Used__c!=null ){
      FMLATOTALNEW.put(NEWFMLA.WCT_Associated_Leave__c,NEWFMLA.WCT_FMLA_Hours_Used__c);
      WCT_FMLA_Hours__c OLDFMLA = Trigger.oldMap.get(NEWFMLA.Id);
      FMLATOTALOLD.put(OLDFMLA.WCT_Associated_Leave__c,OLDFMLA.WCT_FMLA_Hours_Used__c);}
     
     if(NEWFMLA.WCT_State_FMLA_Hours_Used__c!=null ){ 
      stateFMLATOTALNEW.put(NEWFMLA.WCT_Associated_Leave__c,NEWFMLA.WCT_state_FMLA_Hours_Used__c);
      WCT_FMLA_Hours__c stateOLDFMLA = Trigger.oldMap.get(NEWFMLA.Id);
      stateFMLATOTALOLD.put(stateOLDFMLA.WCT_Associated_Leave__c,stateOLDFMLA.WCT_state_FMLA_Hours_Used__c); }

    }
    
    for(wct_leave__c a:[SELECT id,Wct_FMLA_Used_Over_the_Past_12_Months1__c, state_FMLA_Used_Over_the_Past_12_Months__c
                   FROM wct_leave__c 
                   WHERE id in:FMLATOTALNEW.keyset()])  
      {
     if(a.Wct_FMLA_Used_Over_the_Past_12_Months1__c ==null)
      a.Wct_FMLA_Used_Over_the_Past_12_Months1__c =0;
     
     if(a.state_FMLA_Used_Over_the_Past_12_Months__c ==null)
      a.state_FMLA_Used_Over_the_Past_12_Months__c =0;
     
     if(a.Wct_FMLA_Used_Over_the_Past_12_Months1__c !=null)                      
      a.Wct_FMLA_Used_Over_the_Past_12_Months1__c +=FMLATOTALNEW.get(a.id)-FMLATOTALOLD.get(a.id);
     
     if(a.state_FMLA_Used_Over_the_Past_12_Months__c !=null)  
     
     { 
     decimal fmlavalue =stateFMLATOTALOLD.get(a.id);
     if(fmlavalue ==null) 
     fmlavalue=0;
          
     a.state_FMLA_Used_Over_the_Past_12_Months__c +=stateFMLATOTALNEW.get(a.id)-fmlavalue;}
      
      FMLA.add(a);
      
      update FMLA;
     }
   }
   
////////////////////////////////Roll up summary of FMLA hours and state FMLA hours into parental leave record when record is deleting///////////////////////////   
   
if(Trigger.isDelete && trigger.isafter){
Map<id,decimal> stateFMLATOTAL1 = new Map<id,decimal>();
Map<id,decimal> FMLATOTAL1 = new Map<id,decimal>();
list<WCT_Leave__c> FMLA1= new list<WCT_Leave__c>();

    for(WCT_FMLA_Hours__c FMLAA1 :trigger.old){

     if(FMLAA1.WCT_FMLA_Hours_Used__c!=null )
      FMLAtotal1.put(FMLAA1.WCT_Associated_Leave__c,FMLAA1.WCT_FMLA_Hours_Used__c);

     if(FMLAA1.WCT_state_FMLA_Hours_Used__c!=null )
      stateFMLAtotal1.put(FMLAA1.WCT_Associated_Leave__c,FMLAA1.WCT_state_FMLA_Hours_Used__c);
     }
   
   
    for(wct_leave__c a1:[SELECT id,Wct_FMLA_Used_Over_the_Past_12_Months1__c,state_FMLA_Used_Over_the_Past_12_Months__c 
                          FROM wct_leave__c 
                          WHERE id in:FMLAtotal1.keyset() ]){

     if(a1.Wct_FMLA_Used_Over_the_Past_12_Months1__c ==null)
      a1.Wct_FMLA_Used_Over_the_Past_12_Months1__c =0;

     if(a1.state_FMLA_Used_Over_the_Past_12_Months__c ==null)
      a1.state_FMLA_Used_Over_the_Past_12_Months__c =0;

     if(a1.Wct_FMLA_Used_Over_the_Past_12_Months1__c !=null){
      a1.Wct_FMLA_Used_Over_the_Past_12_Months1__c  -=fmlatotal1.get(a1.id);}

     if(a1.state_FMLA_Used_Over_the_Past_12_Months__c !=null){
     decimal fmlavalue =statefmlatotal1.get(a1.id);
     if(fmlavalue ==null) 
     fmlavalue=0;
      a1.state_FMLA_Used_Over_the_Past_12_Months__c  -=fmlavalue;}
      fmla1.add(a1);
     }
     update fmla1;
 }


}