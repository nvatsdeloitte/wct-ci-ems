trigger Former_InternTrigger on Former_Intern__c (before insert,before update) {
    if(Trigger.isBefore && Trigger.isUpdate)
    {
        UpdateEncryptedEmailhandler.encryptemail(Trigger.new);
    }
   if(Trigger.isBefore && Trigger.isInsert)
    {
           UpdateEncryptedEmailhandler.encryptemail(Trigger.new);
    }
}