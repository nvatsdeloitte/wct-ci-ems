trigger ImmigrationTrigger on WCT_Immigration__c (before insert, before update, after insert, after update) {
    if(Trigger.isAfter)
    {
        WCT_GMI_CreateTasks.createImmigrationTask(Trigger.oldMap,Trigger.new,true);
        if(Trigger.isUpdate){
           WCT_GMI_CreateTasks.updateImmigrationTaskOwner(Trigger.oldMap,Trigger.newMap,Trigger.new);
           WCT_GMI_CreateTasks.updateImmigrationUsersOnTask(Trigger.oldMap, Trigger.newMap, Trigger.new);
        } 
        /* // commented the code to check 50001 error - Case no: 01018141
        if(!WCT_GMI_createSLAvalues.isAlreadyRun){
            WCT_GMI_createSLAvalues.createGMIvalues(Trigger.New,Trigger.Old,'WCT_Immigration__c');
            WCT_GMI_createSLAvalues.isAlreadyRun=true;
        }
        */
    }
    else if(Trigger.isBefore){
        WCT_ImmigrationTriggerHandler.populateISTDateTime(Trigger.new);
        if(Trigger.isUpdate){
            WCT_ImmigrationTriggerHandler.updateIsStatusChanged(Trigger.oldMap,Trigger.new);
            WCT_ImmigrationTriggerHandler.dependentFormSubmitted(Trigger.new);
        }
    }

}