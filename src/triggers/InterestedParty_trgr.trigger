trigger InterestedParty_trgr on Interested_Party__c (after insert) {
    Set<Id> CaseIds = new Set<Id>();
    list<Case> caseRec = new list<Case>();
    for(Interested_Party__c ip:Trigger.new){
        CaseIds.add(ip.WCT_Case__c);
    }
    Case_Utility.updateCaseStatus_InProgress(CaseIds);
}