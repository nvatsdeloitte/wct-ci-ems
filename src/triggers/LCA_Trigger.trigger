trigger LCA_Trigger on WCT_LCA__c (before insert, before update, after insert, after update) {
 if(Trigger.isAfter)
    {
        WCT_GMI_CreateTasks.createLCATask(Trigger.oldMap,Trigger.new,true);
        /*if(Trigger.isUpdate){
           WCT_GMI_CreateTasks.updateLCATaskOwner(Trigger.oldMap,Trigger.newMap,Trigger.new);
           WCT_GMI_CreateTasks.updateLCAUsersOnTask(Trigger.oldMap, Trigger.newMap, Trigger.new);
        } */
        
        if(!WCT_GMI_createSLAvalues.isAlreadyRun){
            WCT_GMI_createSLAvalues.createGMIvalues(Trigger.New,Trigger.Old,'WCT_LCA__c');
            WCT_GMI_createSLAvalues.isAlreadyRun=true;
        }
    }    
}