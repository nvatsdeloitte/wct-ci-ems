trigger Mobility_Trigger on WCT_Mobility__c (before insert, after insert,before update, after update) {
    if(Trigger.isafter && Trigger.isInsert)
    {
   WCT_GMI_CreateTasks.newvisa(Trigger.new);
  
    }
    if(Trigger.isafter && (Trigger.isInsert || Trigger.isUpdate))
    {
    //WCT_CreateImmigrationsHelper.createMobilityImmigrations(Trigger.new);
    }
    if(Trigger.isAfter)
    {
       WCT_GMI_CreateTasks.createMobilityTask(Trigger.oldMap,Trigger.new,true);
       if(Trigger.isUpdate){
           WCT_GMI_CreateTasks.updateMobilityTaskOwner(Trigger.oldMap,Trigger.newMap,Trigger.new); 
           WCT_GMI_CreateTasks.updateMobilityUsersOnTask(Trigger.oldMap,Trigger.newMap,Trigger.new);
           WCT_Mobility_TriggerHandler.generateTask(Trigger.new);     
       }
       /* // commented the code to check 50001 error - Case no: 01018141
       if(!WCT_GMI_createSLAvalues.isAlreadyRun){
            WCT_GMI_createSLAvalues.createGMIvalues(Trigger.New,Trigger.Old,'WCT_Mobility__c');
            WCT_GMI_createSLAvalues.isAlreadyRun=true;
        }*/
    }
    else if(Trigger.isBefore && Trigger.isUpdate)
    {
           WCT_Mobility_TriggerHandler.updateIsStatusChanged(Trigger.oldMap,Trigger.new);
    }
    if(Trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert))
    {
           WCT_Mobility_TriggerHandler.updateUSRcCode(Trigger.new);
           WCT_Mobility_Update_RMandPM.updateRMandPM(Trigger.new);
    }  
 }