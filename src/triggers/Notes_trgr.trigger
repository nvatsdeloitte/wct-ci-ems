trigger Notes_trgr on WCT_Notes__c (after insert) {
    Set<Id> CaseIds = new Set<Id>();
    list<Case> caseRec = new list<Case>();
    for(WCT_Notes__c n:Trigger.new){
        CaseIds.add(n.Case__c);
    }
    Case_Utility.updateCaseStatus_InProgress(CaseIds);
}