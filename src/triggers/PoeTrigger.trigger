trigger PoeTrigger on WCT_Port_of_Entry__c (before insert,before update) {


if(Trigger.isBefore && Trigger.isInsert || Trigger.isUpdate){
WCT_POETriggerHandler.populateContactInfo(Trigger.new);
}

}