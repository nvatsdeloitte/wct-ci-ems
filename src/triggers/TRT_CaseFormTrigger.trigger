trigger TRT_CaseFormTrigger on Case_form_Extn__c (before update,after update,after insert) {
    

    if(trigger.isBefore && Trigger.isUpdate )
    { 
        TRT_Reportingtrigger_ctrl.caseformBeforeUpdate(trigger.new);
        TRT_Reportingtrigger_ctrl.updateEscalationtime(trigger.new,trigger.old);
    }
     if(trigger.isAfter && Trigger.isUpdate )
    { 
        TRT_Reportingtrigger_ctrl.caseformAfterUpdate(trigger.new,trigger.old);
    }
   
     
        
}