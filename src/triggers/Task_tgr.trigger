trigger Task_tgr on Task (before insert, before update, after insert,after update, before delete) {
    
   
    
    Set<Id> taskIdsSetInsert=new Set<Id>();  // To insert into ToD
    Set<Id> taskIdSetDelete=new Set<Id>();   //To Delete into ToD
    Set<Id> UsersInGroupSet=new Set<Id>();
    String taskImmigrationRecordTypeId=Schema.SObjectType.Task.getRecordTypeInfosByName().get('Immigration').getRecordTypeId();
    String taskLCARecordTypeId=Schema.SObjectType.Task.getRecordTypeInfosByName().get('LCA').getRecordTypeId();
    String taskMobilityRecordTypeId=Schema.SObjectType.Task.getRecordTypeInfosByName().get('Mobility').getRecordTypeId();
     Id taskPreBIRecordTypeId=Schema.SObjectType.Task.getRecordTypeInfosByName().get('Pre-BI').getRecordTypeId();
     Id currentUserId= UserInfo.getUserId();
    String errorMsg = 'ERROR:This Pre-Bi Task Cannot be Deleted.';
    
    if(trigger.isAfter && trigger.isUpdate){
        if(WCT_GMI_CreateTasks.doNotRunTaskTrig==false){
            WCT_Task_Trigger_Handler.createGMIDependentTasks(Trigger.newMap, Trigger.new);
        }
    }
    
    
    if(trigger.isBefore && trigger.isDelete){
        for(GroupMember groupMemberId :[Select Id, UserOrGroupId,Group.Name From GroupMember where Group.Name = 'Pre-BI']){
            UsersInGroupSet.add(groupMemberId.UserOrGroupId);
        }
       
       if(!UsersInGroupSet.contains(currentUserId)){
            
            for(Task taskId:Trigger.old){        
                if(taskId.RecordTypeId==taskPreBIRecordTypeId){
                        taskId.addError(errorMsg,false);
                    } 
                }         
            
        }
    }
    if(trigger.isAfter && (trigger.isUpdate || trigger.isInsert)){
    
       /*  commented the code to check 50001 error - Case no: 01018141
        //Start - GMI SLA
        if(!WCT_GMI_createSLAvalues.isTaskMethodRun){
            List<Task> GMITask = new List<Task>();
            for(Task ta:trigger.New){
                if(ta.whatId <> null){
                    String sObjName = ta.whatId.getSObjectType().getDescribe().getName();
                    if(sObjName == 'WCT_Immigration__c' || sObjName == 'WCT_Mobility__c' || sObjName == 'WCT_LCA__c'){
                        GMITask.add(ta);
                    }
                }
            
            }
            if(!GMITask.isEmpty()){
                WCT_GMI_createSLAvalues.Create_GMI_SLA_value(GMITask);
            }
        }
        //End - GMI SLA
        */
        
        for(Task taskTemp:Trigger.new)
        {
            if(Trigger.isInsert && taskTemp.WCT_Is_Visible_in_TOD__c==true && taskTemp.Status!='Completed' && taskTemp.Status!='Canceled' && (taskTemp.RecordTypeId==taskImmigrationRecordTypeId || taskTemp.RecordTypeId==taskLCARecordTypeId || taskTemp.RecordTypeId==taskMobilityRecordTypeId))
            {    
                System.debug('test - Task');
                taskIdsSetInsert.add(taskTemp.Id);
            }
            if(Trigger.isUpdate && taskTemp.WCT_Is_Visible_in_TOD__c!=Trigger.oldMap.get(taskTemp.Id).WCT_Is_Visible_in_TOD__c && taskTemp.WCT_Is_Visible_in_TOD__c==false && (taskTemp.Status=='Completed' || taskTemp.Status=='Canceled' || taskTemp.Status=='Employee Replied')&& (taskTemp.RecordTypeId==taskImmigrationRecordTypeId || taskTemp.RecordTypeId==taskLCARecordTypeId || taskTemp.RecordTypeId==taskMobilityRecordTypeId))
            {
                taskIdSetDelete.add(taskTemp.Id);
            }
            
            
        }
        if(!taskIdsSetInsert.IsEmpty()){
            List<Id> tempListId=new List<Id>();
            for(Id tempId:taskIdsSetInsert)
                tempListId.add(tempId);
            WCT_ToDToDoItemsManager.addToDo(tempListId);
            
        }
            //WCT_ToDToDoItemsManager.addToDo(listTaskIds);
        if(!taskIdSetDelete.IsEmpty())
        {
            List<Id> tempListId=new List<Id>();
            for(Id tempId:taskIdSetDelete)
                tempListId.add(tempId);
            WCT_ToDToDoItemsManager.deleteToDo(tempListId);
        }
    }
    if(trigger.isAfter && trigger.isInsert){
        Set<Id> CaseIds = new Set<Id>();
        list<Case> caseRec = new list<Case>();
        for(Task t : Trigger.new){
            CaseIds.add(t.WhatId);
        }
        Case_Utility.updateCaseStatus_InProgress(CaseIds);        
    }
    
    /* eEvents custom list view */
    
    Set<Id> ImmIds = new Set<Id>();
     Set<Id> ImmrevIds = new Set<Id>();
     Set<Id> ImmdeleteIds = new Set<Id>();
     Set<Id> ImmbriefdeleteIds = new Set<Id>();
     Set<Id> RevImmbriefdeleteIds = new Set<Id>();
     map<Id,date> Immdate = new map<Id,date>();
     
     Set<Id> MobIds = new Set<Id>();
     Set<Id> MobrevIds = new Set<Id>();
     Set<Id> MobdeleteIds = new Set<Id>();
     Set<Id> MobbriefdeleteIds = new Set<Id>();
     Set<Id> RevMobbriefdeleteIds = new Set<Id>();
     map<Id,date> Mobdate = new map<Id,date>();
     
    if(trigger.isAfter && (trigger.isUpdate || trigger.isInsert)){
        for(Task taskTemp1:Trigger.new)
        {
            if(Trigger.isInsert && taskTemp1.WCT_Is_Visible_in_TOD__c==true && taskTemp1.Status!='Completed' && taskTemp1.Status!='Canceled' && (taskTemp1.RecordTypeId==taskImmigrationRecordTypeId) && (taskTemp1.subject == 'Attend Q&A session'))
            {    
                ImmIds.add(taskTemp1.WhatId);
            }
            if(Trigger.isInsert && taskTemp1.WCT_Is_Visible_in_TOD__c==true && taskTemp1.Status!='Completed' && taskTemp1.Status!='Canceled' && (taskTemp1.RecordTypeId==taskMobilityRecordTypeId) && (taskTemp1.subject == 'Attend Q&A session'))
            {    
                MobIds.add(taskTemp1.WhatId);
                 system.debug('MobIds1'+MobIds);
            }   
            if(Trigger.isUpdate && taskTemp1.WCT_Is_Visible_in_TOD__c!=Trigger.oldMap.get(taskTemp1.Id).WCT_Is_Visible_in_TOD__c && taskTemp1.WCT_Is_Visible_in_TOD__c==false && (taskTemp1.Status=='Completed' || taskTemp1.Status=='Canceled' || taskTemp1.Status=='Employee Replied') && (taskTemp1.RecordTypeId==taskImmigrationRecordTypeId) && (taskTemp1.subject=='Attend Q&A session'))
            {
                ImmdeleteIds.add(taskTemp1.WhatId);
                Immdate.put(taskTemp1.WhatId,taskTemp1.ActivityDate);
            }  
            if(Trigger.isUpdate && taskTemp1.WCT_Is_Visible_in_TOD__c!=Trigger.oldMap.get(taskTemp1.Id).WCT_Is_Visible_in_TOD__c && taskTemp1.WCT_Is_Visible_in_TOD__c==false && (taskTemp1.Status=='Completed' || taskTemp1.Status=='Canceled' || taskTemp1.Status=='Employee Replied') && (taskTemp1.RecordTypeId==taskMobilityRecordTypeId) && (taskTemp1.subject=='Attend Q&A session'))
            {
                MobdeleteIds.add(taskTemp1.WhatId);
                Mobdate.put(taskTemp1.WhatId,taskTemp1.ActivityDate);
            }  
            if(Trigger.isUpdate && taskTemp1.WCT_Is_Visible_in_TOD__c==true && taskTemp1.Status!='Completed' && taskTemp1.Status!='Canceled' && (taskTemp1.RecordTypeId==taskImmigrationRecordTypeId) && (taskTemp1.subject == 'Attend Q&A session'))
            {    
                ImmrevIds.add(taskTemp1.WhatId);
            }
           if(Trigger.isUpdate && taskTemp1.WCT_Is_Visible_in_TOD__c==true && taskTemp1.Status!='Completed' && taskTemp1.Status!='Canceled' && (taskTemp1.RecordTypeId==taskMobilityRecordTypeId) && (taskTemp1.subject == 'Attend Q&A session'))
            {    
                MobrevIds.add(taskTemp1.WhatId);
                system.debug('MobrevIds'+MobrevIds);
            }   
            if(Trigger.isUpdate && taskTemp1.WCT_Is_Visible_in_TOD__c==false && (taskTemp1.Status=='Completed' || taskTemp1.Status=='Canceled' || taskTemp1.Status=='Employee Replied') && (taskTemp1.RecordTypeId==taskImmigrationRecordTypeId) && (taskTemp1.subject=='Attend Visa briefing session'))
            {
                ImmbriefdeleteIds.add(taskTemp1.WhatId);
            }   
            if(Trigger.isUpdate && taskTemp1.WCT_Is_Visible_in_TOD__c!=Trigger.oldMap.get(taskTemp1.Id).WCT_Is_Visible_in_TOD__c && taskTemp1.WCT_Is_Visible_in_TOD__c==false && (taskTemp1.Status=='Completed' || taskTemp1.Status=='Canceled' || taskTemp1.Status=='Employee Replied') && (taskTemp1.RecordTypeId==taskMobilityRecordTypeId) && (taskTemp1.subject=='Complete pre-departure training session'))
            {
                MobbriefdeleteIds.add(taskTemp1.WhatId);
            }   
            if(Trigger.isUpdate && taskTemp1.Status!='Completed' && taskTemp1.Status!='Canceled' && (taskTemp1.RecordTypeId==taskImmigrationRecordTypeId) && (taskTemp1.subject == 'Attend Visa briefing session'))
            {    
                RevImmbriefdeleteIds.add(taskTemp1.WhatId);
            }
            if(Trigger.isUpdate && taskTemp1.WCT_Is_Visible_in_TOD__c==true && taskTemp1.Status!='Completed' && taskTemp1.Status!='Canceled' && (taskTemp1.RecordTypeId==taskMobilityRecordTypeId) && (taskTemp1.subject == 'Complete pre-departure training session'))
            {    
                RevMobbriefdeleteIds.add(taskTemp1.WhatId);
            }      
        }
    }
    if(!ImmIds.IsEmpty())
    {
        WCT_Eevents_TriggerHandler.ImmUpdate(ImmIds);
    }
    if(!ImmdeleteIds.IsEmpty())
    {
        WCT_Eevents_TriggerHandler.ImmDelete(ImmdeleteIds);
    }     
    if(!ImmrevIds.IsEmpty())
    {
        WCT_Eevents_TriggerHandler.ImmRevList(ImmrevIds);
    }     
    if(!ImmbriefdeleteIds.IsEmpty())
    {
        WCT_Eevents_TriggerHandler.ImmbriefdeleteList(ImmbriefdeleteIds);
    }  
    if(!RevImmbriefdeleteIds.IsEmpty())
    {
        WCT_Eevents_TriggerHandler.ImmRevbriefdeleteList(RevImmbriefdeleteIds);
    }     
    if(!MobIds.IsEmpty())
    {
        WCT_Eevents_TriggerHandler.MobUpdate(MobIds);
        // system.debug('MobIds1'+MobIds);
    }
    if(!MobrevIds.IsEmpty())
    {
        WCT_Eevents_TriggerHandler.MobrevIdList(MobrevIds);
    }
    if(!MobdeleteIds.IsEmpty())
    {
        WCT_Eevents_TriggerHandler.MobdeleteIdList(MobdeleteIds);
    }     
    if(!MobbriefdeleteIds.IsEmpty())
    {
        WCT_Eevents_TriggerHandler.MobbriefdeleteIdList(MobbriefdeleteIds);
    }     
    if(!RevMobbriefdeleteIds.IsEmpty())
    {
        WCT_Eevents_TriggerHandler.MobRevbriefdeleteIdList(RevMobbriefdeleteIds);
    } 
    
    
    if(trigger.isBefore){
        Set<Id> CaseIds = new Set<Id>();
        list<Case> caseList = new list<Case>();
        if(trigger.isInsert){
            for(Task t : trigger.new){
                String caseId = t.WhatId;
                if(t.WhatId <> null && caseId.left(3) == '500'){
                    CaseIds.add(t.WhatId);
                }
            }
            caseList = [select Id from Case where Id IN : CaseIds AND IsClosed = true];
            for(Case c : caseList){
                for(Task t : trigger.new){
                    if(c.Id == t.WhatId){
                        t.addError('You cannot create/edit Task on a closed case');
                    }
                }
            }
        }
        if(trigger.isUpdate){
            system.debug('<><><>' + 'I am here');
            for(task t:trigger.new)
            {
                if(t.status!=trigger.oldMap.get(t.id).status && (t.Status=='Completed' || t.Status=='Canceled' ) && ((t.RecordTypeId==taskImmigrationRecordTypeId || t.RecordTypeId==taskLCARecordTypeId || t.RecordTypeId==taskMobilityRecordTypeId)))
                {
                    t.WCT_Is_Visible_in_TOD__c=false;
                    t.WCT_GMI_Closed_Task_Notification_Date__c=system.today();
                }
            }
            for(Task t : trigger.old){
                String caseId = t.WhatId;
                
                if(t.WhatId <> null && caseId.left(3) == '500'){
                    CaseIds.add(t.WhatId);
                }
            }
            caseList = [select Id from Case where Id IN : CaseIds AND IsClosed = true];
            for(Case c : caseList){
                for(Task t : trigger.old){
                    if(c.Id == t.WhatId){
                        for(task nt : trigger.new){
                            if(nt.Id == t.Id)
                            nt.addError('You cannot create/edit Task on a closed case');
                        }
                    }
                }
            }
        }
        
    }
}