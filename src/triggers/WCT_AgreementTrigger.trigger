/**************************************************************************************
Apex Trigger Name:  WCT_AgreementTrigger
Version          : 1.0 
Created Date     : 25 March 2013
Function         : Trigger to 
                   -> To update Offer status field based on Agreement status field
                   -> To Set SendOnBehalfOf with Offer Owner On Agreement
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                   25/03/2013             Original Version
*************************************************************************************/

trigger WCT_AgreementTrigger on echosign_dev1__SIGN_Agreement__c (After update, Before update,Before Insert) {
     if(trigger.IsAfter){
        if(trigger.isUpdate){
            new WCT_AgreementTriggerHandler().OfferStatusChangeOnUpdate(Trigger.new,Trigger.oldMap);
             if(Trigger.new.size()==1 && Trigger.new[0].WCT_Agreement_Bounced__c == true && Trigger.oldMap.get(Trigger.new[0].id).WCT_Agreement_Bounced__c != true) {
                new WCT_AgreementTriggerHandler().sendBouncedEmail(Trigger.new[0]);
            }    
        }
     }
     if(trigger.IsBefore){
         if(trigger.IsInsert || trigger.IsUpdate){
             new WCT_AgreementTriggerHandler().SetSendOnBehalfOf(Trigger.new);
             new WCT_AgreementTriggerHandler().SetPDFPassword(Trigger.new);
             new WCT_AgreementTriggerHandler().SetCCEmailId(Trigger.new);
         }
     }
}