/**************************************************************************************
Apex Trigger Name:  AttachmentTrigger
Version          : 1.0 
Created Date     : 19 December 2013
Function         : Trigger to 
                    ->get ParentId from Candidate Document using Name field to attachment before insert
                    -> To update Attachment link in Candidate Document after insert 
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                   19/12/2013             Original Version
* 
*************************************************************************************/
trigger WCT_AttachmentTrigger on Attachment (before insert,after insert,after update) {

    if(trigger.IsBefore){
        if(trigger.isInsert){
            //profile p = [select id from profile where name = :WCT_UtilConstants.INTEGRATION_PROFILE_NAME];
            if(SYSTEM.Label.Integration_Profile_Id == userInfo.getProfileId()){
                new WCT_AttachmentTriggerHandler().UpdateParentIdOnInsert(Trigger.new); 
                //new WCT_AttachmentTriggerHandler().DeleteOldAttachmentHavingSameDesc(Trigger.new);
            }
        new WCT_AttachmentTriggerHandler().DeleteDuplicateAttachment(Trigger.new); 
        }
    }
    
    if(trigger.IsAfter){
        if(trigger.isInsert){
            new WCT_AttachmentTriggerHandler().UpdateAttachmentURL(Trigger.new);
            new WCT_AttachmentTriggerHandler().UpdateAttachmentURLonIntvJun(Trigger.new);
            if(SYSTEM.Label.Integration_Profile_Id == userInfo.getProfileId()){
                new WCT_AttachmentTriggerHandler().DeleteOldAttachmentHavingSameDesc(Trigger.new);
            }
        }
        if(trigger.isUpdate){
            new WCT_AttachmentTriggerHandler().UpdateAttachmentURLonIntvJun(Trigger.new);
        }
    }
}