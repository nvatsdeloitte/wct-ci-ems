/**************************************************************************************
Apex Trigger Name: WCT_CandidateAquisitionTrigger
Version          : 1.0 
Created Date     : 19 November 2013
Function         : Trigger to implement below functionalities on Interview
                   1.Populate Owner and Recommended level fields before insertion 
                   2.Cancel Interviews on Candidate Aqisition Cancelation
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                    19/11/2013              Original Version
*************************************************************************************/
trigger WCT_CandidateAquisitionTrigger on WCT_Candidate_Requisition__c (after insert,before insert,after update,before update) {
    
    if( Trigger.isInsert){
        if(Trigger.isbefore){
            WCT_CandidateAquisitionTriggerHandler.populateFieldsonCreation(trigger.new);
         }
    }
    if( trigger.isAfter){
     //   WCT_CandidateAquisitionTriggerHandler.UpdateInterviewsOnAquisitioncancellation (trigger.newmap);
    }
}