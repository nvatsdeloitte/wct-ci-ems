/*
Description : This trigger will run on EmailMessage object before Inserting
Author : Deloitte

Updated by        CR no.        Description
------------    ---------    -------------------------------------------------------

*/

trigger WCT_EmailMessageTrigger on EmailMessage (before insert, After insert){
   
   if(Trigger.IsBefore){
        if(Trigger.IsInsert){
            
            try{
            
            String checkFromAddress = Label.WCT_Check_From_Address;
            if(Trigger.new[0].Status == '0')
                    new WCT_EmailMessageTriggerHandler().CheckExistingCase(Trigger.new[0]); 
            else {
                    if(checkFromAddress == 'true'){
                            new WCT_EmailMessageTriggerHandler().checkEmailWithRole(Trigger.new[0]);     
                 }  
            }  
            }catch(Exception e )
            {
                 WCT_ExceptionUtility.logException('EmailMessage Trigger','Attching email to existing Case',e.getMessage());
            }
        }
    }
     
     
   if(Trigger.IsAfter){
        if(Trigger.IsInsert){
           list<id> lcaseown = new list<id> ();
           list<id> use = new list<id> ();
           
           //profile p = [select name,id from profile where id=:userinfo.getProfileId()];
           //list<profile> pro=[select id from profile where name ='System Administrator'or name ='System Administrator Custom'];

         for(emailmessage em:trigger.new)
         {
          if(em.status=='0')
          {
          lcaseown.add(em.parentid);
          }
         }
          list<case> lcon = [select ownerid, casenumber, id from case where  (id=:lcaseown and WCT_Category__c ='Technology Issue - Talent' and owner.profile.name='System Administrator') or (id=:lcaseown and WCT_Category__c ='Technology Issue - Talent' and owner.profile.name='System Administrator Custom')];
          List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
         for(case sendemail: lcon)
         {
         for(user u:[select lastname, firstname from user where id=: sendemail.ownerid])
         {
         String str='<P>'+'Hi'+' '+u.firstname+' '+u.lastname+','+'<br/>'+ '<br/>'+'Please look into the new email on Case -'+sendemail.casenumber+ ' and address it.'+'</p>';
          Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
          singleMail.setTargetObjectId(sendemail.ownerid);
          singlemail.setsubject('You Got New Reply on Case Number - '+sendemail.casenumber);
          singlemail.sethtmlBody(str); 
          singleMail.setSaveAsActivity(false);
          emails.add(singleMail);
         }
         }
          Messaging.sendEmail(emails);
       }
      }
         
}