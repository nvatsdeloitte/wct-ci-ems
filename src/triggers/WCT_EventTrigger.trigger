trigger WCT_EventTrigger on Event (before insert, before update, after insert, after update) {
    if(trigger.isAfter){
        if(trigger.isInsert){
            Set<Id> CaseIds = new Set<Id>();
            set<Id> EventIds =  new Set<Id>(); // Set of all the Event ID.
            list<Case> caseRec = new list<Case>();
            for(Event t : Trigger.new){
                if(t.WhatId!= null){
                    CaseIds.add(t.WhatId);
                    if(String.valueOf(t.WhatId).startsWith(Label.InterviewObjkeyPrefix)){EventIds.add(t.Id);}
                }
            }
            Case_Utility.updateCaseStatus_InProgress(CaseIds);
            if(!EventIds.isEmpty()){
                WCT_Interview_Scheduling_Utill.handelInvities(EventIds);
                WCT_EventTrigger_Handler.populateStartEndDatesOnInterview(Trigger.new);
                WCT_EventTrigger_Handler.checkEventDate(Trigger.new);
            }
        }
        if(trigger.isUpdate){
            set<Id> EventIds =  new Set<Id>(); // Set of all the Event ID.
            for(Event t : Trigger.new){
                 if(t.WhatId!= null){if(String.valueOf(t.WhatId).startsWith(Label.InterviewObjkeyPrefix)){EventIds.add(t.Id);}}
            }
            if(!EventIds.isEmpty()){
                WCT_Interview_Scheduling_Utill.handelInvities(EventIds);
                WCT_EventTrigger_Handler.populateStartEndDatesOnInterview(Trigger.new);
                WCT_EventTrigger_Handler.checkEventDate(Trigger.new);
            }
        }
        
    }
    if(trigger.isBefore){
        Set<Id> CaseIds = new Set<Id>();
        list<Case> caseList = new list<Case>();
        if(trigger.isInsert){
            for(Event e : trigger.new){
                String caseId = e.WhatId;
                if(e.WhatId <> null && caseId.left(3) == '500'){
                    CaseIds.add(e.WhatId);
                }
            }
            caseList = [select Id from Case where Id IN : CaseIds AND IsClosed = true];
            for(Case c : caseList){
                for(Event e : trigger.new){
                    if(c.Id == e.WhatId){
                        e.addError('You cannot create/edit Task on a closed case');
                    }
                }
            }
        }
        if(trigger.isUpdate){
            system.debug('<><><>' + 'I am here');
            //Logic:Start/End Date check - Written this login to check weather Evet Start or End Date has been edited or not.
           /* for(Event evtN:trigger.new){
                Event evtO = trigger.oldMap.get(evtN.Id);
                    if(evtO.StartDateTime!=evtN.StartDateTime || evtO.EndDateTime != evtN.EndDateTime){
                        evtN.IsDateEdited__c=true;
                    }
                }
*/
            // the following method is called to Check if sendUpdateInvite is to be set or not. 
            WCT_EventTrigger_Handler.setSendUpdateInvite(trigger.old, trigger.new);
            
            //Logic:Start/End Date check
            for(Event e : trigger.old){
                String caseId = e.WhatId;
                if(e.WhatId <> null && caseId.left(3) == '500'){
                    CaseIds.add(e.WhatId);
                }
            }
            caseList = [select Id from Case where Id IN : CaseIds AND IsClosed = true];
            for(Case c : caseList){
                for(Event e : trigger.old){
                    if(c.Id == e.WhatId){
                        for(Event ne : trigger.new){
                            if(ne.Id == e.Id)
                            ne.addError('You cannot create/edit Task on a closed case');
                        }
                    }
                }
            }
        }
        
    }
    list<Event> eventInterviewRec = new list<Event>();
    if(Trigger.isInsert && Trigger.isBefore){
        for(Event t : Trigger.new){
         if(t.WhatId!= null){if(String.valueOf(t.WhatId).startsWith(Label.InterviewObjkeyPrefix)){eventInterviewRec.add(t);}}
        }
        if(!eventInterviewRec.isEmpty())WCT_EventTrigger_Handler.checkSingleEventPerInterview(eventInterviewRec);       
    }
    //if(Trigger.isAfter){
        //WCT_EventTrigger_Handler.populateStartEndDatesOnInterview(Trigger.new);
        //WCT_EventTrigger_Handler.checkEventDate(Trigger.new);
    //}    
}