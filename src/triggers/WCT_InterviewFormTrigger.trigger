/**************************************************************************************
Apex Trigger Name: WCT_InterviewFormTrigger
Created Date     : 12 December 2013
Function         : Once the Data will come from ClickTool to Interview Trigger will create .pfd file for Interview Forms 
                    at associated Interview record.

Note: This trigger can't be bulkify  as its making asynchronous call.                     
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                    12/12/2013              Original Version
*************************************************************************************/

trigger WCT_InterviewFormTrigger on WCT_Interview_Forms__c(after insert, after update){
    for(WCT_Interview_Forms__c newForm:  trigger.new ){
    if(newForm.RecordTypeid <> WCT_Util.getRecordTypeIdByLabel('WCT_Interview_Forms__c', 'Group Interview')){
        if(trigger.isAfter){
            if(trigger.isInsert){
                if(Trigger.New[0].WCT_Interview_Junction__c != null)
                    WCT_CreatePDFWS.createPDFWSFunc(UserInfo.getSessionId(), Trigger.New[0].Id, Trigger.New[0].WCT_Interview_Junction__c);
           }
            if(trigger.isUpdate){
                if(Trigger.New[0].WCT_Interview_Junction__c != null)
                    WCT_CreatePDFWS.createPDFWSFunc(UserInfo.getSessionId(), Trigger.New[0].Id, Trigger.New[0].WCT_Interview_Junction__c);
            }
        }
     }
    }
}