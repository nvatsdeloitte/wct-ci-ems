/**********
Name:WCT_InterviewJunctionTrigger
Function         : Trigger to implement below functionalities on Interview
                       1.Populate Interviewer fields from Related contact before insertion & updation
                       2.Update Candidate Aquisition Status to 'In-progress' on creation of Interview Junction

* Developer                           Date                     Description
* ----------------------------------------------------------------------------                 
* Mrudula(Deloitte)                   12/26/2013              Original Version
*************************************************************************************/
trigger WCT_InterviewJunctionTrigger on WCT_Interview_Junction__c (before insert,before update,after insert,after update,after delete) {
    if(Trigger.isBefore){    
        if(Trigger.isInsert){
           WCT_InterviewTriggerHandler.populateInterviewerDetailsFromRelatedContactOnInsert(Trigger.new);            
        }
        if(Trigger.isUpdate){            
            WCT_InterviewTriggerHandler.populateInterviewerDetailsFromRelatedContactOnUpdate(Trigger.new,trigger.oldmap);
        }
        
    }
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            WCT_InterviewTriggerHandler.UpdateCandidateRequisitionStatus(Trigger.new);
            WCT_InterviewTriggerHandler.UpdateCandidateRelTracker(Trigger.new);
            WCT_InterviewTriggerHandler.InsertInterviewShareRecords(Trigger.new);
        }
        if(Trigger.isUpdate){
            WCT_InterviewTriggerHandler.InsertInterviewShareRecords(Trigger.new);
        }
        if(Trigger.isDelete){
            WCT_InterviewTriggerHandler.DeleteIntvShareRecords(Trigger.old);
        }
        
    }
    
}