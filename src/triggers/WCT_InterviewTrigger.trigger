/**************************************************************************************
Apex Trigger Name: WCT_InterviewTrigger
Version          : 1.0 
Function         : Trigger to implement below functionalities on Interview
                   1.Complete/Cancel All Interview Junctions associated with Interview on Interview Complete/Cancel.

* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                   14/11/2013              Original Version
*************************************************************************************/
                 
trigger WCT_InterviewTrigger on WCT_Interview__c (after insert, after update){
    
    if(Trigger.isAfter){        
        
        if(Trigger.isUpdate){
            
            WCT_InterviewTriggerHandler.UpdateInterviewJunctionsOnInterviewOnCancelAndComplete(Trigger.newmap,Trigger.oldmap);
            WCT_InterviewTriggerHandler.PrepareIntTrackerList(Trigger.New,Trigger.OldMap);
        }
    }
}