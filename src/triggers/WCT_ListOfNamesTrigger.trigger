trigger WCT_ListOfNamesTrigger on WCT_List_Of_Names__c (before insert,before Update) {
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            WCT_ListOfNamesTriggerHandler.GetRelatedDocumentsOnInterviewForms(trigger.new);
        }
        if(Trigger.isUpdate){
           WCT_ListOfNamesTriggerHandler.GetRelatedDocumentsOnInterviewForms(trigger.new); 
        }
    }
}