/**************************************************************************************
Apex Trigger Name:  WCT_OfferTrigger
Version          : 1.0 
Created Date     : 03 April 2013
Function         : Trigger to 
                   -> To update contact salary field based on Agreement status field
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                   03/04/2013             Original Version
*************************************************************************************/

trigger WCT_OfferTrigger on WCT_Offer__c (Before Update, After update,Before Insert,After Insert) {
    if(trigger.IsBefore){
        
        if(trigger.isUpdate){
            try{
              new WCT_OfferTriggerHandler().ApplyFormatForCurrenct(Trigger.new);
              WCT_OfferTriggerHandler.CheckIndiaAnalyst(UserInfo.getUserId(),Trigger.New,Trigger.OldMap);
                //new WCT_OfferTriggerHandler().UpdateFieldParam(Trigger.new);
              new WCT_OfferTriggerHandler().sendEmailCCRec(Trigger.new,Trigger.OldMap);
              new WCT_OfferTriggerHandler().ExceptionIfNoAttachment(Trigger.new,Trigger.OldMap);
              if(WCT_OfferTriggerHandler.offerTemplateAssigned==true) {
                  new WCT_OfferTriggerHandler().OfferTempAssignmentLogic(Trigger.new, Trigger.oldMap);    //- Arun 6/20
                  WCT_OfferTriggerHandler.offerTemplateAssigned = false;
              }    
              new WCT_OfferTriggerHandler().offerStateAbbreviation(Trigger.new);
              new WCT_OfferTriggerHandler().salaryCalculator(Trigger.new);
              new WCT_OfferTriggerHandler().offerMessage(Trigger.new);
              new WCT_OfferTriggerHandler().changeOwner(UserInfo.getUserId(),Trigger.New,Trigger.OldMap);
              new WCT_OfferTriggerHandler().updateOfferSentToWrittenOffer(Trigger.New,Trigger.OldMap);
              
              if(Trigger.new.size()==1 && Trigger.new[0].WCT_status__c=='Offer Declined' && Trigger.new[0].WCT_Declined_Reason__c!=null && Trigger.new[0].WCT_Declined_Email_Alert_Sent__c!=true){
                  new WCT_OfferTriggerHandler().sendEmailOfferDeclined(Trigger.new[0]);
              }               
            }
            catch(exception ex){
                throw ex;
            }
        
         }
         if(trigger.isInsert){
           new WCT_OfferTriggerHandler().offerStateAbbreviation(Trigger.new);
           new WCT_OfferTriggerHandler().salaryCalculator(Trigger.new);
           new WCT_OfferTriggerHandler().offerMessage(Trigger.new);
         }
         
     }      
    if(trigger.IsAfter){
        if(trigger.isUpdate){
            try{
                if(Trigger.new.size()==1){
                    // Passing index 0 becaouse this functionality have to use from VF only
                    if(Trigger.new[0].WCT_status__c=='Ready for Review' && Trigger.new[0].WCT_Status__c !=Trigger.OldMap.get(Trigger.new[0].ID).WCT_Status__c){
                        if(WCT_OfferTriggerHandler.isfirstcall==true) {
                            new WCT_OfferTriggerHandler().sendEmailRedyToReview(Trigger.new[0]);
                            WCT_OfferTriggerHandler.isfirstcall=false;
                        }
                    }
                    /*if(Trigger.new[0].WCT_status__c=='Offer Sent' && Trigger.new[0].WCT_Status__c !=Trigger.OldMap.get(Trigger.new[0].ID).WCT_Status__c){
                        new WCT_OfferTriggerHandler().sendOfferPassword(Trigger.new[0]);
                    }*/
                    new WCT_OfferTriggerHandler().sendOfferPassword(Trigger.New,Trigger.OldMap);
                }
            }
            catch(exception ex){
                throw ex;
            }
            new WCT_OfferTriggerHandler().updateCandidateEmail(Trigger.New,Trigger.OldMap);
            WCT_OfferSLAUTIL.createAndUpdateOfferStateRecords(Trigger.OldMap,Trigger.new);
        }
        if(Trigger.IsInsert){
            WCT_OfferSLAUTIL.createOfferStateOnOfferInsert(Trigger.new);
        } 
    }
}