trigger WCT_OutReachTrg on WCT_Outreach__c (before Update) {
     
    for(WCT_Outreach__c outRec : trigger.new){
        if(outRec.WCT_Status__c == '5. Completed'){
            list<Task> taskcountLst = [SELECT Id FROM Task where Status !='Completed' and WhatId=:outRec.Id limit 1];

            if(taskcountLst.size()>0){
                outRec.adderror(' All Open Tasks must be "Completed" before Outreach can be "Completed".');
            }     
        }
    }

}