trigger WCT_OutreachActivityTigger on WCT_Outreach_Activity__c (before insert,before update, after update) {
     
    for(WCT_Outreach_Activity__c outRec : trigger.new){
        if(outRec.WCT_Status__c == 'Completed'){
            list<Task> taskcountLst = [SELECT Id FROM Task where Status !='Completed' and WhatId=:outRec.Id limit 1];

            if(taskcountLst.size()>0){
                outRec.adderror(' All Open Tasks must be "Completed" before Outreach Activity can be "Completed".');
            }     
        }
    }



    //WCT_OutreachActivityTiggerHandler.CreateExistingOutreachSetAndMap(Trigger.New);
    //WCT_OutreachActivityTiggerHandler.CheckExistingContact(Trigger.New);
}