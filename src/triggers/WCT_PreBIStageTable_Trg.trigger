trigger WCT_PreBIStageTable_Trg on WCT_PreBIStageTable__c (after insert, after update) {
    set<string> candidateEmails;
    set<string> candidateRMSID;
    public static final string EmployeeRtypeId = WCT_Util.getRecordTypeIdByLabel('Contact', 'Employee');
    public static final string CandRtypeId = WCT_Util.getRecordTypeIdByLabel('Contact', 'Candidate');
    set<String> recordTypeIds = new set<String>();
    Map<String, Id> rmsPrebiMap = new Map<String, Id>();
    Map<Id, Id> stagingCases = new Map<Id, Id>();
    List<WCT_PreBIStageTable__c> preBiList;
    List<WCT_PreBIStageTable__c> updatePreBiList;
    Public Static Boolean preBiCaseUpdated = false;
    
    if(trigger.isAfter){
        recordTypeIds.add(EmployeeRtypeId);
        recordTypeIds.add(CandRtypeId);
      if(trigger.isInsert){
        candidateEmails = new set<String>();
        candidateRMSID = new set<String>();
        updatePreBiList = new List<WCT_PreBIStageTable__c>();
        
        /**
        *    Line 25-40    
        *    Code was added to make one prebi staging record as parent to other records based on RMS id.
        *    Right now this functionality is only for Criminal History Case.
        **/
        preBiList = [SELECT id,WCT_RMS_ID__c,WCT_Parent_PreBI__c,WCT_Criminal_History__c FROM WCT_PreBIStageTable__c WHERE id IN: Trigger.newMap.keySet()];
        for(WCT_PreBIStageTable__c stagingRecord:preBiList){
            if(stagingRecord.WCT_Criminal_History__c == true) {
                String rmsid = (String) stagingRecord.WCT_RMS_ID__c;
                if(rmsPrebiMap.containsKey(rmsid)) {
                    stagingRecord.WCT_Parent_PreBI__c = rmsPrebiMap.get(rmsid);
                    updatePreBiList.add(stagingRecord);
                } else {
                    rmsPrebiMap.put(rmsid, stagingRecord.id);
                }
            }
        }
        
        if(!updatePreBiList.isEmpty()) {
            UPDATE updatePreBiList;
        }
        
        for(WCT_PreBIStageTable__c stagingRecord:trigger.new){   
            candidateRMSID.add(stagingRecord.WCT_RMS_ID__c);
            candidateEmails.add(stagingRecord.WCT_Candidate_Email__c);
        }
        
        if(!candidateEmails.isEmpty() || !candidateRMSID.isEmpty()){
            list <contact> contWithPreBiRec = new list <contact>();
            for(contact conRec : [select WCT_Employment_Application_Complete__c,recordTypeId from contact 
                                    where (Email in :candidateEmails
                                    OR WCT_Taleo_Id__c in :candidateRMSID) AND recordTypeId IN :recordTypeIds]){
                conRec.WCT_Employment_Application_Complete__c = 'Yes';
                contWithPreBiRec.add(conRec);
            }
            if(!contWithPreBiRec.IsEmpty()){
                //update contWithPreBiRec;
                DataBase.SaveResult[] srList = DataBase.Update(contWithPreBiRec,false);
                List<Exception_Log__c> excLogToInsert = new List<Exception_Log__c>();
                    
                for(Database.SaveResult sr: srList){
                    if(!sr.IsSuccess()){
                        for(Database.Error err : sr.getErrors()){
                            Exception_Log__c exc = WCT_ExceptionUtility.returnExcLog('PRE-BI Stage Table Trigger','Insert Method',err.getMessage());
                            excLogToInsert.add(exc);
                        }
                    }
                }
                if(!excLogToInsert.isEmpty()){
                    DataBase.Insert(excLogToInsert,false);
                }
            }
        }
      }
      if(trigger.isUpdate){
        candidateEmails = new set<String>();
        candidateRMSID = new set<String>();

        for(WCT_PreBIStageTable__c stagingRecord:trigger.new){   
            if(stagingRecord.WCT_RMS_ID__c != trigger.oldMap.get(stagingRecord.id).WCT_RMS_ID__c){candidateRMSID.add(stagingRecord.WCT_RMS_ID__c);}
            if(stagingRecord.WCT_Candidate_Email__c != trigger.oldMap.get(stagingRecord.id).WCT_Candidate_Email__c){candidateEmails.add(stagingRecord.WCT_Candidate_Email__c);}
        }
        
        if(!candidateEmails.isEmpty() || !candidateRMSID.isEmpty()){
            list <contact> contWithPreBiRec = new list <contact>();
            for(contact conRec : [select WCT_Employment_Application_Complete__c,recordTypeId from contact 
                                    where (Email in :candidateEmails
                                    OR WCT_Taleo_Id__c in :candidateRMSID) AND recordTypeId IN :recordTypeIds]){
                conRec.WCT_Employment_Application_Complete__c = 'Yes';
                contWithPreBiRec.add(conRec);
            }
            if(!contWithPreBiRec.IsEmpty()){
                //update contWithPreBiRec;
                DataBase.SaveResult[] srListUpdate = DataBase.Update(contWithPreBiRec,false);
                List<Exception_Log__c> excLogToInsert = new List<Exception_Log__c>();
                    
                for(Database.SaveResult sr: srListUpdate){
                    if(!sr.IsSuccess()){
                        for(Database.Error err : sr.getErrors()){
                            Exception_Log__c exc = WCT_ExceptionUtility.returnExcLog('PRE-BI Stage Table Trigger','Update Method',err.getMessage());
                            excLogToInsert.add(exc);
                        }
                    }
                }
                if(!excLogToInsert.isEmpty()){
                    DataBase.Insert(excLogToInsert,false);
                }
            }
        }
      }
    }
}