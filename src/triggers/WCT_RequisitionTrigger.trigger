/**************************************************************************************
Apex Trigger Name:  RequisitionTrigger
Version          : 1.0 
Created Date     : 20 November 2013
Function         : Trigger to implement below functionalities on Requisition
                   1.Populate Owner before insertion 
                   2.Update Owner before update
                   3.Populate Recruiting Coordinator before insertion
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                   20/11/2013              Original Version
* Deloitte
*************************************************************************************/
trigger WCT_RequisitionTrigger on WCT_Requisition__c (before insert, before update,after update) {

    if(trigger.isBefore){
        if(trigger.isInsert){
            new WCT_RequisitionTriggerHandler().PopulateRecruitingCoordinatorRecruiterFromEmailOnInsert(Trigger.new);
            new WCT_RequisitionTriggerHandler().UpdateOwnerOnInsert(Trigger.new);
            new WCT_RequisitionTriggerHandler().PopulateRecruitingCoordinatorOnInsert(Trigger.new);
        }
    
        if(trigger.isUpdate){
            new WCT_RequisitionTriggerHandler().PopulateRecruitingCoordinatorRecruiterFromEmailOnUpdate(Trigger.oldmap,Trigger.new);
            new WCT_RequisitionTriggerHandler().UpdateOwnerOnUpdate(Trigger.oldmap,Trigger.new);
            }
    }
    if(trigger.isAfter){
        if(trigger.isUpdate){
            new WCT_RequisitionTriggerHandler().UpdateRCEmailOnJunction(Trigger.oldmap,Trigger.new);
        }
    }
           
}