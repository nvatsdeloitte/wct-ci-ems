/****************
* This Apex Trigger calculate sum of Tasks records with Email_Read_Unread__c = true of every Outreach Activity 
  and populate the total in Outreach Activity Unread_Email_Count__c number field.
  
* Validation is coded to avoid the user to save the Task record without Email_Read_Unread__c checked. 

* Trigger will calculate the sum of Task records deleted with Email_Read_Unread__c = false and update the count in parent Outreach Activity record level. 

* Task owner should be parental Outreach Activity record owner. 
  
*/  


trigger Wct_OpentaskEmail_count on Task (before insert, before Update, after insert, after update, before delete ) {
 //Schema.DescribeSObjectResult r = CustomObject__c.sObjectType.getDescribe();
//String keyPrefix = r.getKeyPrefix();
 string taskc_PREFIX = WCT_Outreach_Activity__c.sObjectType.getDescribe().getKeyPrefix();
Id profileId=userinfo.getProfileId();
static boolean isCurrentProfileQueried = false;
String profileName;
if(!isCurrentProfileQueried){
profileName=[Select Id,Name from Profile where Id=:profileId].Name;
isCurrentProfileQueried = true;
}
set<id> setids = new set<id> ();
set<id> setdelids = new set<id> ();
set<id> setownids = new set<id> ();
list<task> tasklist = new list<task> ();
list<task> taskdellist = new list<task> ();
map<id,WCT_Outreach_Activity__c> mapwctoa = new map<id,WCT_Outreach_Activity__c> ();
list<WCT_Outreach_Activity__c> listwctout = new list<WCT_Outreach_Activity__c> ();
list<WCT_Outreach_Activity__c> listdelwctout = new list<WCT_Outreach_Activity__c> ();
list<id> looplist = new list<id>();
Map<id, integer> outreachTaskcountMap=new Map<id, integer>();

 if(Trigger.isUpdate && trigger.isbefore || trigger.isinsert && trigger.isbefore ||trigger.isinsert && trigger.isafter ||trigger.isupdate && trigger.isafter)
 {
  for(task newtask : trigger.new)
   {
   
   if(newtask.Email_status__c == 'Replied' &&  String.valueOf(newtask.whatid).startsWith(taskc_PREFIX) )
   
    setids.add(newtask.whatid);
   }
   if(!setids.isEmpty()){
       List<WCT_Outreach_Activity__c> woas=[select Unread_Email_Count__c from WCT_Outreach_Activity__c where id in :setids limit:Limits.getLimitQueryRows()];
       for(WCT_Outreach_Activity__c woa :woas )
       {
        looplist.add(woa.id);
       }
   }
   if(!looplist.isEmpty()){
   for(task t:[select id,whatid   from task where whatid in:looplist and Email_Read_Unread__c = true and Email_status__c = 'Replied'])
   {
   
   integer count=outreachTaskcountMap.get(t.whatid);
   if(count==null)
   {
       count=1;
   }
   else
   {
    count++;
   
   }
   
   
   outreachTaskcountMap.put(t.whatid,count);
   
    //tasklist.add(t);
    
   }
   }
   if(!setids.isEmpty()){
       for(WCT_Outreach_Activity__c woa :[select Unread_Email_Count__c from WCT_Outreach_Activity__c where id in :setids] )
       {
       
            integer count= outreachTaskcountMap.get(woa.id);
            if(count==null)
            {
            count=0;
            }
           woa.Unread_Email_Count__c=count;
             listwctout.add(woa);
       }
   
    update listwctout;
    }
 }
 
 if(Trigger.isdelete && trigger.isbefore )
 {
  for(task newtask : trigger.old)
  {
   if (newtask.Email_Read_Unread__c==true && newtask.email_status__c =='Replied' &&  String.valueOf(newtask.whatid).startsWith(taskc_PREFIX))
   setdelids.add(newtask.whatid);
  }
  if(!setdelids.isEmpty()){
  for(WCT_Outreach_Activity__c woa: [select Unread_Email_Count__c from WCT_Outreach_Activity__c where id in:setdelids])
   {
    woa.Unread_Email_Count__c=woa.Unread_Email_Count__c-setdelids.size();
    listdelwctout.add(woa);
   }
    update listdelwctout;
    }
 }

 if(Trigger.isUpdate && trigger.isbefore )
 {
  for(task t:trigger.new)
   {
     
    if(t.Email_Read_Unread__c==true && t.Email_status__c == 'Replied'&&  String.valueOf(t.whatid).startsWith(taskc_PREFIX) &&(profilename=='11_CTS_PSN_BDC' || profilename=='11_CTS_PSN_ERC'|| profilename=='System Administrator'|| profilename=='2_CIC_Manager'|| profilename=='2_CIC_Basic'))
    t.Email_Read_Unread__c.addError('Please Uncheck EMAIL READ / UNREAD checkbox to Proceed');
   }
 }


 if(Trigger.isUpdate && trigger.isbefore || trigger.isinsert && trigger.isbefore)
 {
 list<id> looplists = new list<id>();
Map<id, id> outreachTaskcountMaps=new Map<id, id>();
  for (task t:trigger.new)
   {
  
   if(t.status=='Not Started'&& t.Email_status__c == 'Replied' &&  String.valueOf(t.whatid).startsWith(taskc_PREFIX))
   looplists.add(t.whatid);
   }
   //t.Ownerid=UserInfo.getUserId();
  if(!looplists.isEmpty()) {
      for(WCT_Outreach_Activity__c ownerchange : [select createdbyid from WCT_Outreach_Activity__c where id in :looplists])
       {
        outreachTaskcountMaps.put(ownerchange.id,ownerchange.createdbyid);
        //t.Ownerid=ownerchange.createdbyid;
       }
  } 
  if(!outreachTaskcountMaps.isEmpty()){
   for (task t:trigger.new)
   {
    if(outreachTaskcountMaps.get(t.whatid)!=null)
    {
       t.Ownerid=outreachTaskcountMaps.get(t.whatid);
      }
   }
   }

 }

}