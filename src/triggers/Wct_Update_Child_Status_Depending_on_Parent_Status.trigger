trigger Wct_Update_Child_Status_Depending_on_Parent_Status on WCT_Leave__c (before insert, before update, after Insert, after Update)
 {

if(trigger.isafter)
   {
    set<id> newcanleaves = new set<id> ();
    set<id> newcloleaves = new set<id> ();
      
    list<wct_leave__c> childleaves = new list<wct_leave__c> ();
    list<wct_leave__c> childcloleaves = new list<wct_leave__c> ();
   for (WCT_Leave__c newleave : Trigger.new)
   {
   if(newleave.WCT_Leave_Status__c=='Cancelled')
    newcanleaves.add(Newleave.id);
   if(newleave.WCT_Leave_Status__c=='closed')
    newcloleaves.add(newleave.id);
   }
    list<wct_leave__c> childleave = [select id, WCT_Leave_Status__c from wct_leave__c where WCT_Related_Record__c=:newcanleaves];
   for (wct_leave__c updatechildleave : childleave)
   {
    updatechildleave.WCT_Leave_Status__c='Cancelled';
    childleaves.add(updatechildleave);
   }
    update childleaves;
    list<wct_leave__c> childcloleave = [select id, WCT_Leave_Status__c from wct_leave__c where WCT_Related_Record__c=:newcloleaves];
   for (wct_leave__c updatechildleave : childcloleave)
   {
    updatechildleave.WCT_Leave_Status__c='Closed';
    childcloleaves.add(updatechildleave);
   }
    update childcloleaves;
   }

if(trigger.isbefore )
{

List<wct_leave__c> listleave = new list<wct_leave__c> ();
for(wct_leave__c l:trigger.new)
{
listleave.add(l);
}

list<Talent_Delivery_Contact_Database__c> listtalentcon = [select id,Us_Talent_Contact_4__c, India_Talent_Contact_1__c,name,India_Talent_Contact_2__c,Payroll_Contact_Email__c,US_Talent_Contact_1__c,US_Talent_Contact_3__c,US_Talent_Contact_2__c from Talent_Delivery_Contact_Database__c];
for(wct_leave__c leaves:listleave)
{
if(leaves.wct_employee__c !=null)
{
for(Talent_Delivery_Contact_Database__c t: listtalentcon)
{
if(t.name== Leaves.WCT_Cost_Center__c)
{
if(t.India_Talent_Contact_1__c != '')
Leaves.India_Talent_Contact_1__c=t.India_Talent_Contact_1__c;

if(t.India_Talent_Contact_2__c != '')
leaves.India_Talent_Contact_2__c=t.India_Talent_Contact_2__c;

if(t.us_Talent_Contact_1__c != '')
leaves.US_Talent_Contact_1__c=t.US_Talent_Contact_1__c;

if(t.us_Talent_Contact_2__c != '')
leaves.US_Talent_Contact_2__c=t.US_Talent_Contact_2__c;

if(t.us_Talent_Contact_3__c != '')
leaves.US_Talent_Contact_3__c=t.US_Talent_Contact_3__c;

if(t.us_Talent_Contact_4__c != '')
leaves.US_Talent_Contact_4__c=t.US_Talent_Contact_4__c;

if(t.Payroll_Contact_Email__c != '')
leaves.Payroll_Contact_Email__c= t.Payroll_Contact_Email__c;
}
}
}
if(leaves.wct_employee__c ==null)
{
leaves.india_Talent_Contact_1__c='';
leaves.India_Talent_Contact_2__c='';
leaves.US_Talent_Contact_1__c='';
leaves.US_Talent_Contact_2__c='';
leaves.US_Talent_Contact_3__c='';
leaves.US_Talent_Contact_4__c='';
leaves.Payroll_Contact_Email__c='';
}
}

}

if(trigger.isafter && trigger.isinsert)
      {
          
          map< id,id> mapclosecase= new map<id,id>();
          list<case> fincloseca= new list<case>();
      list<task> remindtask = new list<task> ();
      list<recordtype> rt = [select id, name from recordtype where name='personal' or name = 'military' or name ='parental' ];
      recordtype rtask = [select id, name from recordtype where name='task'];
     user u = [select id from user where name='Leaves General'];
      //group g= [select Id from Group where Name = 'Leaves Queue' and Type = 'gruop'];
      set<id> rtleaves = new set<id> ();
      profile p = [select name,id from profile where id=:userinfo.getProfileId()];
      profile pro=[select id from profile where name ='ToD Profile'];
       for(wct_leave__c newleave: trigger.new)
        {
        for(recordtype r:rt)
         {
         if(newleave.recordtypeid == r.id && p.id==pro.id  )
          {
           rtleaves.add(newleave.id);
          }
         }
        }
        list<wct_leave__c> recleave = [select id,name, WCT_Leave_Status__c from wct_leave__c where id=:rtleaves];
        for(wct_leave__c reminder: recleave )
        {
        task t= new task();
        t.recordtypeid = rtask.id;
        t.subject = '  New Leave Request '+reminder.name;
        t.Priority='High';
        t.ActivityDate=system.today();
        t.whatid=reminder.id;
        t.ownerid=u.id;
        t.status= 'Not Started';
        //t.ownerid='00G40000001W7vr';
        t.isreminderset= true;
        t.ReminderDateTime= system.today();
        remindtask.add(t);
        }
         if(remindtask.size()>0)
         {
          insert remindtask;
         }
         
////////////////////////* Close Case Functionality for Leaves Team *//////////////////////////////////
         
          for(wct_leave__c templeave: trigger.new)
          {
           if(templeave.Wct_Casetoclose__c <>''&&templeave.Wct_Casetoclose__c !='')   
           {
             
               mapclosecase.put(templeave.Wct_Casetoclose__c,templeave.id);
           } 
              
          }
         
          list<case> closecasees = [select id from case where id IN:mapclosecase.keyset()];
          for(case closeca:closecasees)
          {
          
            closeca.status='closed';
              closeca.leave__c=mapclosecase.get(closeca.id);
              fincloseca.add(closeca);
              
          }
          update fincloseca;
        } 
       }