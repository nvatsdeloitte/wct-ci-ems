trigger eventDuplicatePreventer on Event__c(before insert, before update, after insert, after update) {

    Map<String, Event__c> eventMap = new Map<String, Event__c>();
       if(trigger.isBefore && (trigger.isUpdate || trigger.isInsert)){  
    for (Event__c event : System.Trigger.new) {
        
        // Make sure we don't treat an email address that  
        // isn't changing during an update as a duplicate.  
    
        if ((event.Name != null) && (System.Trigger.isInsert || (event.Name != 
                    System.Trigger.oldMap.get(event.Id).Name))) {
        
            // Make sure another new event isn't also a duplicate  
    
            if (eventMap.containsKey(event.Name)) {
                event.Name.addError('Another new Event has the '+ 'same Event Name.');
            } else {
                eventMap.put(event.Name, event);
            }
       }
    }
    
    // Using a single database query, find all the leads in  
    
    // the database that have the same email address as any  
    
    // of the events being inserted or updated.  
    
    for (Event__c event: [SELECT Name FROM Event__c WHERE Name IN :eventMap.KeySet()]) {
        Event__c newEvent = eventMap.get(event.Name);
        system.debug('newEvent ===>>'+newEvent);
        if(newEvent != null && newEvent.Name != null){
        newEvent.Name.addError(' Please create a unique event name.');
        }
    }
    }
    
    if(Trigger.isBefore){
     wct_checkinAR.populateImageURL1(Trigger.new);
      WCT_Eevents_TriggerHandler.populateARformattedeventinfo(Trigger.new);
      WCT_Eevents_TriggerHandler.checkfornumber(Trigger.new);
    
     
     }
    

    
    Set<Id> eventIdsSetUpdate = new Set<Id>();
    String eventGMIRecordTypeId=Schema.SObjectType.Event__c.getRecordTypeInfosByName().get('GM&I').getRecordTypeId();
    
    if(trigger.isAfter && trigger.isUpdate){
        for(Event__c eventTemp:Trigger.new){
            if(Trigger.isUpdate && eventTemp.Is_Event_Information_Changed__c==true && eventTemp.Is_Event_Information_Changed__c!=Trigger.oldMap.get(eventTemp.Id).Is_Event_Information_Changed__c && (eventTemp.RecordTypeId==eventGMIRecordTypeId)) {
                eventIdsSetUpdate.add(eventTemp.Id);
            }
        }
        if(!eventIdsSetUpdate.isEmpty()) {
            WCT_Eevents_TriggerHandler.updateEventInfo(eventIdsSetUpdate);
        }
      /*  
       // update campaign
         // insertcampaign_TriggerHandler upsertcampaignctr = new insertcampaign_TriggerHandler ();
         // upsertcampaignctr.upsertcampaign(trigger.new,'Update');
       CampaignAndCampignMember_TriggerHandler objCACM=new CampaignAndCampignMember_TriggerHandler();
       objCACM.CampaignManagement(trigger.new,'update');*/
    }
   /* if (Trigger.isAfter && Trigger.isInsert) {
         //Insert campaign
         // insertcampaign_TriggerHandler insertcampaignctr = new insertcampaign_TriggerHandler ();
         // insertcampaignctr.upsertcampaign(trigger.new,'Insert');
         CampaignAndCampignMember_TriggerHandler objCACM=new CampaignAndCampignMember_TriggerHandler();
         objCACM.CampaignManagement(trigger.new,'insert');
    }*/
}