<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CER_Candidate_Expense_Additional_Document_Received</fullName>
        <description>Candidate Expense Additional Document Received</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>candidateexpense@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CER_Expense_Reiumbersement/CER_Candidate_expense_record_updated</template>
    </alerts>
    <alerts>
        <fullName>CER_Candidate_Expense_Approval_Needed_Reminder</fullName>
        <description>Candidate Expense Approval Needed Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>CER_Rec_Coordinator_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>candidateexpense@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CER_Expense_Reiumbersement/Candidate_Expense_Approval_Reminder</template>
    </alerts>
    <alerts>
        <fullName>CER_Candidate_Expense_Federal_Receipts_Received</fullName>
        <description>Candidate Expense Federal Receipts Received</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>candidateexpense@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CER_Expense_Reiumbersement/CER_FederalReceiptsReceived</template>
    </alerts>
    <alerts>
        <fullName>CER_Candidate_Expense_Paid</fullName>
        <description>Candidate Expense Paid</description>
        <protected>false</protected>
        <recipients>
            <field>CER_Requester_Name__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>CER_Recruiter_Coordinator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>candidateexpense@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CER_Expense_Reiumbersement/CER_Candidate_expense_submission_paid</template>
    </alerts>
    <alerts>
        <fullName>CER_Candidate_Expense_Recruiter_Coordinator_Approval_Received</fullName>
        <description>Candidate Expense Recruiter Coordinator Approval Received</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>candidateexpense@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CER_Expense_Reiumbersement/CER_Candidate_expense_record_updated</template>
    </alerts>
    <alerts>
        <fullName>CER_Candidate_Expense_Rejected_By_AP</fullName>
        <description>Candidate Expense Rejected By AP</description>
        <protected>false</protected>
        <recipients>
            <field>CER_Recruiter_Coordinator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>candidateexpense@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CER_Expense_Reiumbersement/CER_Candidate_expense_submission_rejected</template>
    </alerts>
    <alerts>
        <fullName>CER_Candidate_Expense_Submission_Approved_NON_PDAT</fullName>
        <description>Candidate Expense Submission Approved NON PDAT</description>
        <protected>false</protected>
        <recipients>
            <field>CER_Requester_Name__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>CER_Recruiter_Coordinator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>candidateexpense@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CER_Expense_Reiumbersement/CER_Candidate_expense_submission_approved</template>
    </alerts>
    <alerts>
        <fullName>CER_Candidate_Expense_Submission_Approved_PDAT</fullName>
        <description>Candidate Expense Submission Approved PDAT</description>
        <protected>false</protected>
        <recipients>
            <field>CER_Recruiter_Coordinator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>candidateexpense@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CER_Expense_Reiumbersement/CER_Candidate_expense_submission_approved</template>
    </alerts>
    <alerts>
        <fullName>CER_Candidate_Expense_Submission_Confirmation_Action</fullName>
        <description>Candidate Expense Submission Confirmation Email Action</description>
        <protected>false</protected>
        <recipients>
            <field>CER_Requester_Name__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>candidateexpense@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CER_Expense_Reiumbersement/CER_Submission_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>CER_Candidate_Expense_Submission_Confirmation_Cord_Email_Action</fullName>
        <description>Candidate Expense Submission Confirmation Cord Email Action</description>
        <protected>false</protected>
        <recipients>
            <field>CER_Recruiter_Coordinator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>candidateexpense@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CER_Expense_Reiumbersement/Candidate_Expense_Submission_Confirmation_Other_Cord</template>
    </alerts>
    <alerts>
        <fullName>CER_Candidate_Expense_Submission_Confirmation_PDAT_Cord_Email_Action</fullName>
        <description>Candidate Expense Submission Confirmation PDAT Cord Email Action</description>
        <protected>false</protected>
        <recipients>
            <field>CER_Recruiter_Coordinator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>candidateexpense@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CER_Expense_Reiumbersement/Candidate_Expense_Submission_Confirmation_Other_Cord</template>
    </alerts>
    <fieldUpdates>
        <fullName>CER_Update_Ownership_taken_date_FU</fullName>
        <field>CER_Ownership_taken_Date__c</field>
        <formula>now()</formula>
        <name>Update Ownership taken date FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Candidate Expense Approval Needed Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>CER_Expense_Reimbursement__c.CER_Request_Status__c</field>
            <operation>equals</operation>
            <value>Pending Coordinator Approval</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CER_Candidate_Expense_Approval_Needed_Reminder</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
