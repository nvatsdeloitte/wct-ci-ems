<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AR_Company_Request_Completed</fullName>
        <description>AR_Company Request Completed</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_ReportedBy__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>alumnirequest@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Alumni_Relations/Company_Notification</template>
    </alerts>
    <alerts>
        <fullName>A_Case_has_been_assigned_for_you</fullName>
        <description>A Case has been assigned for you</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>US_Customer_Interaction_Center/Customer_Interaction_Center_Follow_Up_Case_Escalated</template>
    </alerts>
    <alerts>
        <fullName>A_new_case_has_been_added_to_your_queue_PPS_Employee_Solutions_US</fullName>
        <description>A new case has been added to your queue mail send to user, if case is added to PPS Employee Solutions</description>
        <protected>false</protected>
        <recipients>
            <recipient>smandha@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Payroll_Email_Templates/A_new_case_has_been_added_to_your_queue</template>
    </alerts>
    <alerts>
        <fullName>A_new_case_has_been_added_to_your_queue_mail_send_to_user_if_case_is_added_to_PP</fullName>
        <description>A new case has been added to your queue mail send to user, if case is added to PPS Resource US</description>
        <protected>false</protected>
        <recipients>
            <recipient>jkijinski@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lahudson@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Payroll_Email_Templates/A_new_case_has_been_added_to_your_queue</template>
    </alerts>
    <alerts>
        <fullName>Acknowledge_Ment_to_Contact</fullName>
        <description>Case AcknowledgeMent to Contact</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/AcknoledgeMent_Mail_for_All_except_ELE_IVS</template>
    </alerts>
    <alerts>
        <fullName>Advisory_TR_queue_Email_Alert</fullName>
        <description>Advisory TR queue Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Advisory_TR_Queue_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Folder/Talent_Relations_Case_added_to_the_Queue</template>
    </alerts>
    <alerts>
        <fullName>Assigned_to_Case_Owner</fullName>
        <description>Assigned to Case Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>eInterviews_Automated_Templates/WCT_PreBi_Case_Assigned</template>
    </alerts>
    <alerts>
        <fullName>Audit_TR_queue_Email_Alert</fullName>
        <description>Audit TR queue Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Audit_TR_Queue_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Folder/Talent_Relations_Case_added_to_the_Queue</template>
    </alerts>
    <alerts>
        <fullName>Case_AcknowledgeMent_to_Contact_for_Ele_Separations</fullName>
        <description>Case AcknowledgeMent to Contact for Ele Separations</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/AcknoledgeMent_Mail_for_Vol_Separations</template>
    </alerts>
    <alerts>
        <fullName>Case_AcknowledgeMent_to_Contact_for_Ele_Transfers</fullName>
        <description>Case AcknowledgeMent to Contact for Ele Transfers</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/AcknoledgeMent_Mail_for_Transfers</template>
    </alerts>
    <alerts>
        <fullName>Case_Reopen_Email_Alert</fullName>
        <ccEmails>narao@deloitte.com</ccEmails>
        <description>Case Reopen Email Alert</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Case_Folder/Case_Reopen_Talent_technology_Issue</template>
    </alerts>
    <alerts>
        <fullName>Case_closed_notification_to_Case_Contact</fullName>
        <description>Customer Interaction Center Follow Up-Close Case</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>US_Customer_Interaction_Center/Customer_Interaction_Center_Follow_Up_Close_ase</template>
    </alerts>
    <alerts>
        <fullName>Case_closed_notification_to_Reported_Contact</fullName>
        <description>Customer Interaction Center Follow Up-Close Case</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_ReportedBy__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>US_Customer_Interaction_Center/Customer_Interaction_Center_Follow_Up_Close_ase</template>
    </alerts>
    <alerts>
        <fullName>Case_escalation_notification_to_contact</fullName>
        <description>Customer Interaction Center Follow Up-Escalated</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>US_Customer_Interaction_Center/Customer_Interaction_Center_Follow_Up_Escalated</template>
    </alerts>
    <alerts>
        <fullName>Change_Control_Send_Mail_to_Case_Owner</fullName>
        <description>Change Control - Send Mail to Case Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/CHANGE_CONTROL_Generate_an_Email_to_the_CASE_OWNER</template>
    </alerts>
    <alerts>
        <fullName>Change_Control_Send_Mail_to_Case_Reported_By</fullName>
        <description>Change Control - Send Mail to Case Reported By</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_ReportedBy__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/CHANGE_CONTROL_Generate_an_Email_to_the_REPORTED_BY</template>
    </alerts>
    <alerts>
        <fullName>Compensation_Email_Alert</fullName>
        <description>Compensation Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/Compensation_email_alert</template>
    </alerts>
    <alerts>
        <fullName>Consulting_TR_queue_Email_Alert</fullName>
        <description>Consulting TR queue Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Consulting_TR_Queue_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Folder/Talent_Relations_Case_added_to_the_Queue</template>
    </alerts>
    <alerts>
        <fullName>Customer_Interaction_Center_Follow_Up</fullName>
        <description>Customer Interaction Center Follow Up</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CIC_Email_Template/Customer_Interaction_Center_Follow_Up</template>
    </alerts>
    <alerts>
        <fullName>Customer_Interaction_Center_Follow_Up_USI</fullName>
        <description>Customer Interaction Center Follow Up_USI</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CIC_Email_Template/Customer_Interaction_Center_Follow_Up_India</template>
    </alerts>
    <alerts>
        <fullName>ELE_Form_Professional_Alert</fullName>
        <description>ELE Form Professional Alert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Mail_Consolidation_Email_Templates/ELE_Case_Created_Professional_Alert</template>
    </alerts>
    <alerts>
        <fullName>Email_if_it_is_not_assigned_for_5_hrs</fullName>
        <description>Email if it is not assigned for 5 hrs</description>
        <protected>false</protected>
        <recipients>
            <recipient>nvats@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_CF_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>Email_notification_sent_to_contact</fullName>
        <description>Email notification sent to contact</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/Acknowledgement_mail_from_ELE_Transfers</template>
    </alerts>
    <alerts>
        <fullName>Email_once_it_is_not_allocated_for</fullName>
        <description>Email once it is not allocated for</description>
        <protected>false</protected>
        <recipients>
            <recipient>nvats@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>Federal_TR_queue_Email_Alert</fullName>
        <description>Federal TR queue Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Federal_TR_Queue_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Folder/Talent_Relations_Case_added_to_the_Queue</template>
    </alerts>
    <alerts>
        <fullName>Mail_from_ELE_Separations_team</fullName>
        <description>Mail from ELE Separations team</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/Acknowledgement_mail_from_ELE_Separations</template>
    </alerts>
    <alerts>
        <fullName>New_Case</fullName>
        <description>New Case</description>
        <protected>false</protected>
        <recipients>
            <recipient>asyed@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rpiramanayagam@deloitte.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sabasha@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shoghosh@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vilthakur@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>visreddy@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CIC_Email_Template/AUTOMATED_MESSAGE_Notification_to_tech_team_of_case</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_tech_team_of_high_critical_case</fullName>
        <description>Notification to tech team of high / critical case</description>
        <protected>false</protected>
        <recipients>
            <recipient>Admin_Dev_Team</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>arshetty@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dirangasamy@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>fazhussain@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>joharamos@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kvalakonda@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>narao@deloitte.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rpiramanayagam@deloitte.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sabasha@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sandvarma@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shoghosh@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>visreddy@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CIC_Email_Template/AUTOMATED_MESSAGE_Notification_to_tech_team_of_high_critical_case</template>
    </alerts>
    <alerts>
        <fullName>Process_Issue_send_email_to_specified_people</fullName>
        <description>Process Issue : send email to specified people</description>
        <protected>false</protected>
        <recipients>
            <recipient>debwetzel@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dyoshimura@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/New_Process_Issue_Case_Created</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_When_Accounting_Firm_is_true_on_Send_Separation_Email</fullName>
        <ccEmails>usindiasepam@DELOITTE.com,expensecompliance@deloitte.com</ccEmails>
        <description>Send Email When Accounting Firm  is true on Send Separation Email</description>
        <protected>false</protected>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/Send_Email_To_Resigned_Employee_Accounting_Firm_Voluntary</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_When_Case_Send_Separation_Emails_to_voluntary_Request_for_New_Employe</fullName>
        <ccEmails>usindiasepam@DELOITTE.com</ccEmails>
        <description>Send Email When Case Send Separation Emails to voluntary Request for New Employer Information  is true on Send Separation Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/Send_Email_To_Request_For_New_Employer_Information_Voluntary</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_When_Case_Send_Separation_Emails_to_voluntary_Resignation_Notificatio</fullName>
        <ccEmails>usindiasepam@DELOITTE.com</ccEmails>
        <description>Send Email When Case Send Separation Emails to voluntary Resignation Notification - ERS is true on Send Separation Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/Send_Email_For_Resignation_Notification_ERS_Voluntary</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_When_Corporate_team_is_true_on_Send_Separation_Email</fullName>
        <ccEmails>corporatecards@deloitte.com,expensecompliance@deloitte.com , usindiasepam@DELOITTE.com</ccEmails>
        <description>Send Email When Corporate team is true on Send Separation Email</description>
        <protected>false</protected>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/Send_Email_For_USXM_Request_Voluntary</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_When_Immigration_Request_is_true_on_Send_Separation_Email</fullName>
        <ccEmails>GCrepayment@DELOITTE.com , usindiasepam@DELOITTE.com</ccEmails>
        <description>Send Email When Immigration Request is true on Send Separation Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/Send_Email_For_Immigration_Request_Voluntary</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_When_Involuntary_Corporate_Cards_is_true_on_Send_Separation_Email</fullName>
        <ccEmails>corporatecards@deloitte.com,expensecompliance@deloitte.com, usindiasepam@DELOITTE.com</ccEmails>
        <description>Send Email When  Involuntary – Corporate Cards is true on Send Separation Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/Send_Email_For_Corporate_Card_Balance_Request_Involuntary</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_When_Involuntary_PDA_is_true_on_Send_Separation_Email</fullName>
        <ccEmails>USMobilityCOE@DELOITTE.com,usindiasepam@DELOITTE.com</ccEmails>
        <description>Send Email When Involuntary - PDA  is true on Send Separation Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/Send_Email_For_PDA_Request_Involuntary</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_When_Involuntary_Records_Managementis_true_on_Send_Separation_Email</fullName>
        <ccEmails>USNationalRecordsManagementFilesCheckedOut@DELOITTE.com , usindiasepam@DELOITTE.com</ccEmails>
        <description>Send Email When Involuntary – Records Managementis true on Send Separation Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Folder/Send_Email_For_Records_Management_Request_Involuntary</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_When_Involuntary_Termination_Request_PTO_is_true_on_Send_Separation_E</fullName>
        <ccEmails>ppshradmin@deloitte.com,</ccEmails>
        <ccEmails>usindiasepam@DELOITTE.com</ccEmails>
        <description>Send Email When Involuntary Termination - Request PTO  is true on Send Separation Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/Send_Email_For_Involuntary_Termination_Request_PTO_and_Note_Loan_Information</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_When_Involuntary_Termination_is_true_on_Send_Separation_Email</fullName>
        <ccEmails>GCrepayment@DELOITTE.com,</ccEmails>
        <ccEmails>usindiasepam@DELOITTE.com</ccEmails>
        <description>Send Email When Involuntary Termination is true on Send Separation Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/Send_Email_For_Involuntary_Termination</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_When_PDA_is_true_on_Send_Separation_Email</fullName>
        <ccEmails>USMobilityCOE@DELOITTE.com ,alumni2@deloitte.com, usindiasepam@DELOITTE.com</ccEmails>
        <description>Send Email When PDA is true on Send Separation Email</description>
        <protected>false</protected>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/Send_Email_For_PDA_Request_Voluntary</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_When_Records_Management_is_true_on_Send_Separation_Email</fullName>
        <ccEmails>USNationalRecordsManagementFilesCheckedOut@DELOITTE.com,usindiasepam@DELOITTE.com</ccEmails>
        <description>Send Email When Records Management  is true on Send Separation Email</description>
        <protected>false</protected>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/Send_Email_For_Records_Management_Request_Voluntary</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_When_Voluntary_Restricted_Entity</fullName>
        <ccEmails>complianceonboarding@deloitte.com,usindiasepam@DELOITTE.com</ccEmails>
        <description>Send Email When Voluntary Restricted Entity</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/Send_Email_For_Restricted_Entity_Resignation_Notification_Voluntary</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Acquisition_Q_Members</fullName>
        <description>Send Email to Acquisition Q Members US</description>
        <protected>false</protected>
        <recipients>
            <recipient>CTS_Acquisition_and_Mobility_Manager</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>US_Customer_Interaction_Center/Customer_Interaction_Center_Follow_Up_Case_Escalated</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Acquisition_Q_Members_India</fullName>
        <description>Send Email to Acquisition Q Members India</description>
        <protected>false</protected>
        <recipients>
            <recipient>CTS_Acquisition_and_Mobility_Manager_IN</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>US_Customer_Interaction_Center/Customer_Interaction_Center_Follow_Up_Case_Escalated</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Development_Performance_Management_Q_Members</fullName>
        <description>Send Email to Development: Performance Management Q Members - US</description>
        <protected>false</protected>
        <recipients>
            <recipient>CTS_Performance_Mgmt_US</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>US_Customer_Interaction_Center/Customer_Interaction_Center_Follow_Up_Case_Escalated</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Development_Performance_Management_Q_Members_USI</fullName>
        <description>Send Email to Development: Performance Management Q Members - USI</description>
        <protected>false</protected>
        <recipients>
            <recipient>CTS_Performance_Mgmt_USI</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>US_Customer_Interaction_Center/Customer_Interaction_Center_Follow_Up_Case_Escalated</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_ININ</fullName>
        <description>Send Email to ININ</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CIC_Email_Template/CICQ_Send_Email_to_ININ</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Knowledge_Management_Q_Members</fullName>
        <description>Send Email to Knowledge Management Q Members</description>
        <protected>false</protected>
        <recipients>
            <recipient>CTS_Knowledge_Management</recipient>
            <type>role</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>US_Customer_Interaction_Center/Customer_Interaction_Center_Follow_Up_Case_Escalated</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Mobility_Q_Members</fullName>
        <description>Send Email to Mobility Q Members</description>
        <protected>false</protected>
        <recipients>
            <recipient>CTS_Out_of_Scope_Mobility</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>cquancard@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jpeek@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nhessling@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rgrassi@deloitte.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>US_Customer_Interaction_Center/Customer_Interaction_Center_Follow_Up_Case_Escalated</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Operations_Mgt_Continuous_Improvements_Q_Members</fullName>
        <description>Send Email to Operations Mgt: Continuous Improvements Q Members</description>
        <protected>false</protected>
        <recipients>
            <recipient>CTS_Out_of_Scope_Operations_Management_Continuous</recipient>
            <type>role</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>US_Customer_Interaction_Center/Customer_Interaction_Center_Follow_Up_Case_Escalated</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Relocation_Q_Members</fullName>
        <description>Send Email to Relocation Q Members</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sub_Admin</recipient>
            <type>role</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>US_Customer_Interaction_Center/Customer_Interaction_Center_Follow_Up_Case_Escalated</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Reporting_Q_Members</fullName>
        <description>Send Email to Reporting Q Members</description>
        <protected>false</protected>
        <recipients>
            <recipient>CTS_Out_of_Scope_Reporting</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>mkarmaran@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>US_Customer_Interaction_Center/Customer_Interaction_Center_Follow_Up_Case_Escalated</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Resource_Management_Q_Members</fullName>
        <description>Send Email to Resource Management Q Members</description>
        <protected>false</protected>
        <recipients>
            <recipient>CTS_Out_of_Scope_Resource_Management</recipient>
            <type>role</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>US_Customer_Interaction_Center/Customer_Interaction_Center_Follow_Up_Case_Escalated</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_TR_Intake_Coordinator_Q_Members</fullName>
        <ccEmails>ustalentrelations@deloitte.com</ccEmails>
        <description>Send Email to TR Intake Coordinator Q Members</description>
        <protected>false</protected>
        <recipients>
            <recipient>CTS_TR_Intake_Coordinator</recipient>
            <type>role</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>US_Customer_Interaction_Center/Customer_Interaction_Center_Follow_Up_Case_Escalated</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_TR_USI_Group</fullName>
        <ccEmails>usitalentrelations@deloitte.com</ccEmails>
        <description>Send Email to TR USI Group</description>
        <protected>false</protected>
        <recipients>
            <recipient>CTS_TR_USI_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>US_Customer_Interaction_Center/Customer_Interaction_Center_Follow_Up_Case_Escalated</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Technology_Q_Members</fullName>
        <description>Send Email to Technology Q Members</description>
        <protected>false</protected>
        <recipients>
            <recipient>CTS_Out_of_Scope_Technology</recipient>
            <type>role</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>US_Customer_Interaction_Center/Customer_Interaction_Center_Follow_Up_Case_Escalated</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Vendor_Relationship_Management_Q_Members</fullName>
        <description>Send Email to Vendor Relationship Management Q Members</description>
        <protected>false</protected>
        <recipients>
            <recipient>mwynne@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>US_Customer_Interaction_Center/Customer_Interaction_Center_Follow_Up_Case_Escalated</template>
    </alerts>
    <alerts>
        <fullName>Send_High_Priority_Email_to_users_with_2_CIC_Manager_profile</fullName>
        <description>Send High Priority Email to users with 2_CIC_Manager profile</description>
        <protected>false</protected>
        <recipients>
            <recipient>CIC_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CIC_Email_Template/Customer_Interaction_Center_Follow_Up_Priority</template>
    </alerts>
    <alerts>
        <fullName>Send_Mail_to_Case_Owner</fullName>
        <description>Process Issue - Send Mail to Case Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/PROCESS_ISSUE_Generate_an_Email_to_the_CASE_OWNER</template>
    </alerts>
    <alerts>
        <fullName>Send_Mail_to_Case_Reported_By</fullName>
        <description>Process Issue - Send Mail to Case Reported By</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_ReportedBy__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_Reported_By_Email_Process_Issue__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Folder/PROCESS_ISSUE_Generate_an_Email_to_the_REPORTED_BY</template>
    </alerts>
    <alerts>
        <fullName>Services_TR_queue_Email_Alert</fullName>
        <description>Services TR queue Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Services_TR_Queue_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Folder/Talent_Relations_Case_added_to_the_Queue</template>
    </alerts>
    <alerts>
        <fullName>TDR_Send_an_email_on_providing_additional_comments_by_practitioner</fullName>
        <description>TDR_Send an email on providing additional comments by practitioner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Mail_Consolidation_Email_Templates/TDR_Myrequest_Update</template>
    </alerts>
    <alerts>
        <fullName>TDR_Sendemail_for_additionalinfo</fullName>
        <description>TDR Sendemail for additionalinfo</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Mail_Consolidation_Email_Templates/TDR_Additional_Info_Required</template>
    </alerts>
    <alerts>
        <fullName>TRT_Case_Change_Owner_Lea</fullName>
        <description>TRT Case Owner Change Lea</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRT_Learning_AM</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>TRT_Case_Owner_Change_GMI</fullName>
        <description>TRT Case Owner Change GMI</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRT_GMNI_AM</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>TRT_Case_Owner_Change_GMI2</fullName>
        <description>TRT Case Owner Change GMI2</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRT_GMNI_AM_DSS</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>TRT_Case_Owner_Change_LE</fullName>
        <description>TRT Case Owner Change LE</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRT_LE_AM</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>TRT_Case_Owner_Change_LE2</fullName>
        <description>TRT Case Owner Change LE2</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRT_LE_AM_DSS</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>TRT_Case_Owner_Change_Lea2</fullName>
        <description>TRT Case Owner Change Lea2</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRT_Learning_AM_DSS</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>TRT_Case_Owner_Change_Other</fullName>
        <description>TRT Case Owner Change Other</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRT_Other_AM</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>TRT_Case_Owner_Change_Other2</fullName>
        <description>TRT Case Owner Change Other2</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRT_other_AM_DSS</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>TRT_New_Request</fullName>
        <description>TRT New Request</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRT_Public_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TRT_Email_Templates/TRT_NewReportingRequest</template>
    </alerts>
    <alerts>
        <fullName>TRT_OwnerNotChanged_LE</fullName>
        <description>TRT_OwnerNotChanged LE</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRT_Public_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>TRT_OwnerNotChanged_PM</fullName>
        <description>TRT OwnerNotChanged PM</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRT_PM_AM</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>TRT_OwnerNotChanged_PM2</fullName>
        <description>TRT OwnerNotChanged PM2</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRt_PM_AM_DSS</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>TRT_OwnerNotChanged_RM</fullName>
        <description>TRT OwnerNotChanged RM</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRT_RM_AM</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>TRT_OwnerNotChanged_RM2</fullName>
        <description>TRT OwnerNotChanged RM2</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRT_RM_AM_DSS</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>TRT_OwnerNotChanged_Sap</fullName>
        <description>TRT_OwnerNotChanged Sap</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRT_SAP_AM</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>TRT_OwnerNotChanged_Sap2</fullName>
        <description>TRT_OwnerNotChanged Sap2</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRT_SAP_AM_DSS</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>TRT_OwnerNotChanged_Survey</fullName>
        <description>TRT OwnerNotChanged Survey</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRT_Survey_AM</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>TRT_OwnerNotChanged_Survey2</fullName>
        <description>TRT OwnerNotChanged Survey2</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRT_Survey_AM_DSS</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>TRT_OwnerNotChanged_TA</fullName>
        <description>TRT OwnerNotChanged TA</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRT_TA_AM</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>TRT_OwnerNotChanged_TA2</fullName>
        <description>TRT OwnerNotChanged TA2</description>
        <protected>false</protected>
        <recipients>
            <recipient>TRT_TA_AM_DSS</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>usctstalentreportingandanalytics@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TRT_Email_Templates/TRT_OwnerNotChanged</template>
    </alerts>
    <alerts>
        <fullName>Talent_Relations_Email_Alert</fullName>
        <description>Talent Relations Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Talent_Relations_Queue_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Folder/Talent_Relations_Case_added_to_the_Queue</template>
    </alerts>
    <alerts>
        <fullName>Tax_TR_queue_Email_Alert</fullName>
        <description>Tax TR queue Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Tax_TR_Queue_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Folder/Talent_Relations_Case_added_to_the_Queue</template>
    </alerts>
    <alerts>
        <fullName>USI_TR_queue_Email_Alert</fullName>
        <description>USI TR queue Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>USI_TR_Queue_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Folder/Talent_Relations_Case_added_to_the_Queue</template>
    </alerts>
    <alerts>
        <fullName>USI_Tax_email_trigger</fullName>
        <description>USI Tax email trigger</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>usindiaelecompensation@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Folder/ELE_COM_USI_Tax_Talent_Team</template>
    </alerts>
    <alerts>
        <fullName>USI_consult_email_trigger</fullName>
        <description>USI consult email trigger</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>usindiaelecompensation@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Folder/USI_Consulting_team</template>
    </alerts>
    <alerts>
        <fullName>US_Consulting_and_Tax_email_alert_when_case_is_rasied</fullName>
        <description>US Consulting and Tax email alert when case is rasied</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>uselecompensation@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Folder/US_Consulting_Email_notification</template>
    </alerts>
    <alerts>
        <fullName>WCT_Critical_P1_Benefits_case_created</fullName>
        <ccEmails>mkonnoju@deloitte.com</ccEmails>
        <description>Critical P1 Benefits case created</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>General_PSN_Case_Management/WCT_Immediate_Action_Required_Critical_P1_Benefits_case_created</template>
    </alerts>
    <alerts>
        <fullName>WCT_Critical_priority_case_is_2_hours_from_breaching_SLA</fullName>
        <ccEmails>shortan@deloitte.com</ccEmails>
        <description>Critical-priority case is 2 hours from breaching SLA</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>General_PSN_Case_Management/WCT_Critical_priority_case_2_hours_from_breaching_SLA</template>
    </alerts>
    <alerts>
        <fullName>WCT_SMSEmail_High_Priority_Case_Created</fullName>
        <ccEmails>6154147589@messaging.sprintpcs.com</ccEmails>
        <ccEmails>6159697080@vtext.com</ccEmails>
        <ccEmails>6155198206@txt.att.net</ccEmails>
        <ccEmails>6154033571@txt.att.net</ccEmails>
        <ccEmails>6152091926@txt.att.net</ccEmails>
        <description>Notify the PSN when a high priority case is coming in</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>General_PSN_Case_Management/WCT_SMSEmail_High_Priority_Case_Created</template>
    </alerts>
    <alerts>
        <fullName>WCT_SMSEmail_Notify_PSN_of_escalations_of_cases_with_critical_priority</fullName>
        <ccEmails>6154147589@messaging.sprintpcs.com</ccEmails>
        <ccEmails>6159697080@vtext.com</ccEmails>
        <ccEmails>6155198206@txt.att.net</ccEmails>
        <ccEmails>6154033571@txt.att.net</ccEmails>
        <ccEmails>6152091926@txt.att.net</ccEmails>
        <description>Notify PSN of escalations of cases with critical priority.</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>General_PSN_Case_Management/WCT_SMSEmail_Critical_Priority_Case_Escalated_From_Tier1_To_Tier2</template>
    </alerts>
    <alerts>
        <fullName>WCT_SMSEmail_Notify_PSN_when_a_P1_Benefits_case_is_about_to_breach_SLA</fullName>
        <ccEmails>6154147589@messaging.sprintpcs.com</ccEmails>
        <ccEmails>6159697080@vtext.com</ccEmails>
        <ccEmails>6155198206@txt.att.net</ccEmails>
        <ccEmails>6154033571@txt.att.net</ccEmails>
        <ccEmails>6152091926@txt.att.net</ccEmails>
        <description>Send a text message on phone when a P1 Benefits case is about to breach SLA</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>General_PSN_Case_Management/WCT_SMSEmail_P1_Benefits_Case_About_To_Breach_SLA</template>
    </alerts>
    <alerts>
        <fullName>WCT_SMS_Email_Critical_Priority_Case</fullName>
        <ccEmails>6154147589@messaging.sprintpcs.com</ccEmails>
        <ccEmails>6159697080@vtext.com</ccEmails>
        <ccEmails>6155198206@txt.att.net</ccEmails>
        <description>SMS Email: Critical Priority Case</description>
        <protected>false</protected>
        <senderAddress>psnbenefits@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>General_PSN_Case_Management/WCT_SMSEmail_Critical_Priority_Case_Escalated_From_Tier1_To_Tier2</template>
    </alerts>
    <fieldUpdates>
        <fullName>AR_Assign_to_AR_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Alumni_Relations_India</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>AR_Assign to AR queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Advisory_TR_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Advisory_TR_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Advisory TR queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Audit_TR_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Audit_TR_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Audit TR queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Canceled</fullName>
        <field>Status</field>
        <literalValue>Canceled</literalValue>
        <name>Case Canceled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Category_Programs</fullName>
        <field>WCT_Category__c</field>
        <literalValue>ELE: Programs</literalValue>
        <name>Case Category Programs</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Category_Update</fullName>
        <field>WCT_Category__c</field>
        <literalValue>Payroll - US</literalValue>
        <name>Case Category Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Category_Update_ELE_Onboarding</fullName>
        <field>WCT_Category__c</field>
        <literalValue>ELE: New Hire Onboarding</literalValue>
        <name>Case Category Update ELE Onboarding</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Category_Update_India_Offer_Letters</fullName>
        <field>WCT_Category__c</field>
        <literalValue>India Offer Letters</literalValue>
        <name>Case Category Update India Offer Letters</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Category_Update_Learning_Delivery</fullName>
        <description>Update Category Field to Poll Everywhere</description>
        <field>WCT_Category__c</field>
        <literalValue>Learning: Poll Everywhere</literalValue>
        <name>Case Category Update Learning Delivery 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Category_Update_Learning_Delivery_2</fullName>
        <description>Update Category to CPE</description>
        <field>WCT_Category__c</field>
        <literalValue>Learning: Continuing Professional Education (CPE)</literalValue>
        <name>Case Category Update Learning Delivery 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Category_Update_Learning_Delivery_3</fullName>
        <field>WCT_Category__c</field>
        <literalValue>Learning: Catalog Management</literalValue>
        <name>Case Category Update Learning Delivery 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Category_Update_Learning_Delivery_4</fullName>
        <description>Update Case Category to CRM</description>
        <field>WCT_Category__c</field>
        <literalValue>Learning: Customer Relationship Management (CRM)</literalValue>
        <name>Case Category Update Learning Delivery 4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Category_Update_Learning_Delivery_5</fullName>
        <description>Update Category to MTM</description>
        <field>WCT_Category__c</field>
        <literalValue>Learning: Metrics That Matter (MTM)</literalValue>
        <name>Case Category Update Learning Delivery 5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Category_Update_Learning_Design</fullName>
        <field>WCT_Category__c</field>
        <literalValue>Learning: Learning Design Services</literalValue>
        <name>Case Category Update Learning Design</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Category_Update_Performance_Manag</fullName>
        <field>WCT_Category__c</field>
        <literalValue>Development: Performance Management</literalValue>
        <name>Case Category Update Performance Manag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Category_Update_Separations</fullName>
        <field>WCT_Category__c</field>
        <literalValue>Separations</literalValue>
        <name>Case Category Update Separations</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Category_Update_Talent_Relations</fullName>
        <description>Update Category to Inquiry</description>
        <field>WCT_Category__c</field>
        <literalValue>Talent Relations: Inquiry</literalValue>
        <name>Case Category Update Talent Relations</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Category_Update_Transfers</fullName>
        <field>WCT_Category__c</field>
        <literalValue>ELE: Transfers</literalValue>
        <name>Case Category Update Transfers</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Category_Update_US_Offer_Letters</fullName>
        <field>WCT_Category__c</field>
        <literalValue>US Offer Letters</literalValue>
        <name>Case Category Update US Offer Letters</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Category_Update_to_Payroll_US</fullName>
        <field>WCT_Category__c</field>
        <literalValue>Payroll - US</literalValue>
        <name>Case Category Update to Payroll - US</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_RecordType_update_For_Finance_cases</fullName>
        <description>This Field updates record type to Finance for Finance cases</description>
        <field>RecordTypeId</field>
        <lookupValue>Finance</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Case_RecordType_update_For_Finance_cases</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Reopen_Datetime</fullName>
        <description>Case_Reopened_Date_time</description>
        <field>Case_Reopened_Date_time__c</field>
        <formula>TODAY()</formula>
        <name>Case Reopen Datetime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Reopen_Update</fullName>
        <field>Case_Reopen_Status__c</field>
        <formula>&apos;Case Reopend/updated With the Workflow &apos; +   TEXT(TODAY())</formula>
        <name>Case Reopen Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_SetCaseOwnerFld</fullName>
        <field>OwnerChangeWrg__c</field>
        <literalValue>0</literalValue>
        <name>Case: SetCaseOwnerFld</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Sub_Category_2_Update_to_Blank</fullName>
        <field>WCT_SubCategory2__c</field>
        <name>Case Sub Category 2 Update to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Sub_Category_3_Update_to_Blank</fullName>
        <field>Sub_Category_3__c</field>
        <name>Case Sub Category 3 Update to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Sub_Category_Update_to_Blank</fullName>
        <field>WCT_SubCategory1__c</field>
        <name>Case Sub Category Update to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Time_Check</fullName>
        <field>TimeCheck__c</field>
        <literalValue>1</literalValue>
        <name>Case Time Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Category_to_Acquisition</fullName>
        <field>WCT_Category__c</field>
        <literalValue>Acquisition</literalValue>
        <name>Case: Update Category to Acquisition</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Priority_to_Critical</fullName>
        <field>Priority</field>
        <literalValue>1 - Critical</literalValue>
        <name>Case: Update Priority to Critical</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_R_Type_to_Leaves_for_Leave</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Leaves</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Case- Update R-Type to Leaves for Leave</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_Resolution_Notes</fullName>
        <field>WCT_ResolutionNotes__c</field>
        <formula>&apos;This case is closed Automatically based on Subject.&apos;</formula>
        <name>Case: Update Resolution Notes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_SubCategory_to_Offer_Letter</fullName>
        <field>WCT_SubCategory1__c</field>
        <literalValue>Offer letters</literalValue>
        <name>Case: Update SubCategory to Offer Letter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_status_update_to_Reopened</fullName>
        <field>Status</field>
        <literalValue>Reopened</literalValue>
        <name>Case status update to Reopened</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Category</fullName>
        <field>WCT_Category__c</field>
        <literalValue>Leaves and Disabilities - US</literalValue>
        <name>Update Category to LeavesDisability US</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Owner_to_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>CICQ</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change Owner to Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Support_area_to_US</fullName>
        <description>this will change the support area to US as requested by Bonnie</description>
        <field>WCT_Support_Area__c</field>
        <literalValue>US</literalValue>
        <name>Change Support area to US</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Consulting_TR_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Consulting_TR_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Consulting TR queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Federal_TR_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Federal_TR_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Federal TR queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_update_subcateg3_N_A</fullName>
        <field>Sub_Category_3__c</field>
        <literalValue>N/A</literalValue>
        <name>Field update subcateg3 N/A</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IsNew_flag_update_to_false</fullName>
        <field>IsNew__c</field>
        <literalValue>0</literalValue>
        <name>IsNew flag update to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Is_Queue_check_for_Advisory_TR_queue</fullName>
        <field>Is_Queue_check__c</field>
        <literalValue>1</literalValue>
        <name>Is Queue check for Advisory TR queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Is_Queue_check_for_Audit_TR_queue</fullName>
        <field>Is_Queue_check__c</field>
        <literalValue>1</literalValue>
        <name>Is Queue check for Audit TR queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Is_Queue_check_for_Consulting_TR_queue</fullName>
        <field>Is_Queue_check__c</field>
        <literalValue>1</literalValue>
        <name>Is Queue check for Consulting TR queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Is_Queue_check_for_Federal_TR_queue</fullName>
        <field>Is_Queue_check__c</field>
        <literalValue>1</literalValue>
        <name>Is Queue check for Federal TR queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Is_Queue_check_for_Services_TR_queue</fullName>
        <field>Is_Queue_check__c</field>
        <literalValue>1</literalValue>
        <name>Is Queue check for  Services TR queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Is_Queue_check_for_Talent_Relations</fullName>
        <field>Is_Queue_check__c</field>
        <literalValue>1</literalValue>
        <name>Is Queue check for Talent Relations</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Is_Queue_check_for_Tax_TR_queuefinal</fullName>
        <field>Is_Queue_check__c</field>
        <literalValue>1</literalValue>
        <name>Is Queue check for Tax TR queuefinal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Is_Queue_check_for_USI_TR_queue</fullName>
        <field>Is_Queue_check__c</field>
        <literalValue>1</literalValue>
        <name>Is Queue check for USI TR queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_type_to_Payroll</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Payroll</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record type to Payroll</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reopen_date_and_time_update</fullName>
        <description>This field update is used to update reopen date and time at which the case is reopened</description>
        <field>Reopened_Date_time__c</field>
        <formula>NOW()</formula>
        <name>Reopen date and time update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Services_TR_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Services_TR_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Services TR queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetUpdatedbyNonOwnertoTrue</fullName>
        <field>WCT_Updated_by_Non_Owner__c</field>
        <literalValue>1</literalValue>
        <name>SetUpdatedbyNonOwnertoTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Support_Area_to_PSN</fullName>
        <field>WCT_Support_Area__c</field>
        <literalValue>US-PSN</literalValue>
        <name>Set Support Area to PSN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sub_Category_3_Will_be_Blank</fullName>
        <field>Sub_Category_3__c</field>
        <name>Sub Category 3 Will be Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>PreviousValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TDR_Uncheckadditionalinforequired</fullName>
        <description>On providing additional comments by requestor uncheck additional info field</description>
        <field>TDR_IsAdditionalInfoRequired__c</field>
        <literalValue>0</literalValue>
        <name>TDR Uncheckadditionalinforequired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Talent_Relations</fullName>
        <field>OwnerId</field>
        <lookupValue>Talent_Relations</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Talent Relations</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Talent_Relations_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Talent_Relations</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Talent Relations Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tax_TR_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Tax_TR_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Tax TR queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>USI_TR_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>USI_TR_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>USI TR queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Assigned_Status</fullName>
        <field>Status</field>
        <name>Update Assigned Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Category</fullName>
        <description>Update Case Category to &apos;Technology Issue Talent&apos;</description>
        <field>WCT_Category__c</field>
        <literalValue>Technology Issue - Talent</literalValue>
        <name>Update_Case_Category</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Category_GMMI_Immigration</fullName>
        <field>WCT_Category__c</field>
        <literalValue>Immigration</literalValue>
        <name>Update Case Category GMMI - Immigration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Category_GMMI_Mobility</fullName>
        <field>WCT_Category__c</field>
        <literalValue>Mobility</literalValue>
        <name>Update Case Category GMMI - Mobility</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Category_PSN_Benifits</fullName>
        <field>WCT_Category__c</field>
        <literalValue>Benefits - US</literalValue>
        <name>Update Case Category PSN Benifits</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Category_TRT_Reporting</fullName>
        <field>WCT_Category__c</field>
        <literalValue>Reporting &amp; Analytics</literalValue>
        <name>Update Case Category TRT-Reporting</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Category_TRT_Tech</fullName>
        <field>WCT_Category__c</field>
        <literalValue>Technology Issue - Talent</literalValue>
        <name>Update Case Category TRT-Tech</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Origin_to_Clicktools</fullName>
        <field>Origin</field>
        <literalValue>Clicktools</literalValue>
        <name>Update Case Origin to Clicktools</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Sub_Category</fullName>
        <description>Update Case Sub Category1 to &apos;Talent Connect (Careerify)&apos;</description>
        <field>WCT_SubCategory1__c</field>
        <literalValue>Talent Connect (Careerify)</literalValue>
        <name>Update_Case_Sub_Category</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Category_Field_for_Compliance</fullName>
        <description>Update Category Field to ELE: Compliance if case origin is for the appropriate mailbox</description>
        <field>WCT_Category__c</field>
        <literalValue>ELE: Compliance</literalValue>
        <name>Update Category Field for Compliance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Duplicate_Status</fullName>
        <field>Status</field>
        <literalValue>Closed – Duplicate</literalValue>
        <name>Update Duplicate Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Escalated</fullName>
        <field>WCT_Escalated__c</field>
        <literalValue>1</literalValue>
        <name>Update Escalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_EscalatedTime</fullName>
        <field>WCT_EscalatedTime__c</field>
        <formula>IF(ISBLANK(WCT_EscalatedTime__c), NOW(), WCT_EscalatedTime__c )</formula>
        <name>Update EscalatedTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_In_Progress_Status</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Update In Progress Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Pending_Tier_2_Action</fullName>
        <field>Status</field>
        <literalValue>Pending – Tier 2 Action</literalValue>
        <name>Update Status to Pending – Tier 2 Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Tier_2</fullName>
        <field>Status</field>
        <literalValue>Assigned to Tier 2</literalValue>
        <name>Update Status to Tier 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_support_area</fullName>
        <field>WCT_Support_Area__c</field>
        <literalValue>US</literalValue>
        <name>Update to support area</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Set_Support_Area_to_India</fullName>
        <field>WCT_Support_Area__c</field>
        <literalValue>INDIA</literalValue>
        <name>Set Support Area to India</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Set_Support_Area_to_US</fullName>
        <field>WCT_Support_Area__c</field>
        <literalValue>US</literalValue>
        <name>Set Support Area to US</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>talentcheck</fullName>
        <field>Is_Queue_check__c</field>
        <literalValue>1</literalValue>
        <name>talentcheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>test</fullName>
        <field>Is_check_talent_relation__c</field>
        <literalValue>1</literalValue>
        <name>test</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_description_of_case</fullName>
        <field>Description</field>
        <formula>&quot;USI Tax&quot;</formula>
        <name>update description of case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_recordtype</fullName>
        <field>Gen_RecordType__c</field>
        <formula>TEXT(TRT_Emailtocasegroup__c)</formula>
        <name>TRT Update recordtype</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_subject_of_case</fullName>
        <field>Subject</field>
        <formula>&quot;USI Tax&quot;</formula>
        <name>update subject of case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AR_Assign to AR queue</fullName>
        <actions>
            <name>AR_Assign_to_AR_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_SubCategory1__c</field>
            <operation>equals</operation>
            <value>Company Edit,New Company Request</value>
        </criteriaItems>
        <description>assigns new company requests to Alumni Relations - India queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AR_Company Request Complete</fullName>
        <actions>
            <name>AR_Company_Request_Completed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_SubCategory1__c</field>
            <operation>equals</operation>
            <value>Company Edit,New Company Request</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign To Federal TR queue</fullName>
        <actions>
            <name>Federal_TR_queue_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Federal_TR_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Is_Queue_check_for_Federal_TR_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(UPPER(Reported_by_FSS__c) &lt;&gt; &apos;FEDERAL EA&apos;,  OR(Reported_By_Federal_Practice__c = TRUE, WCT_Federal_Employee__c = TRUE),  OR($Profile.Id = $Label.CIC_Agent_Profile_Id , $Profile.Id = $Label.CIC_Manager_Profile_Id),  PRIORVALUE(OwnerId) &lt;&gt; OwnerId,  OwnerId = $Label.TR_Queue_ID,Is_Queue_check__c=false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign To Tax TR queue</fullName>
        <actions>
            <name>Tax_TR_queue_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Is_Queue_check_for_Tax_TR_queuefinal</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Tax_TR_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(OR(UPPER(Reported_By_Region__c) &lt;&gt; &apos;US – USI&apos;, UPPER(Reported_By_Region__c) &lt;&gt; &apos;US–USI&apos; , UPPER(Reported_By_Region__c) &lt;&gt; &apos;US - USI&apos;, UPPER(Reported_By_Region__c) &lt;&gt; &apos;US-USI&apos;) , Reported_By_Federal_Practice__c &lt;&gt; TRUE, OR(UPPER(Reported_by_FSS__c) = &apos;TAX&apos;, UPPER(Reported_by_FSS__c)= &apos;DELOITTE TAX LLP&apos;),OR($Profile.Id = $Label.CIC_Agent_Profile_Id , $Profile.Id = $Label.CIC_Manager_Profile_Id),  PRIORVALUE(OwnerId) &lt;&gt; OwnerId,  OwnerId= $Label.TR_Queue_ID,Is_Queue_check__c=false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign to Advisory TR queue</fullName>
        <actions>
            <name>Advisory_TR_queue_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Advisory_TR_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Is_Queue_check_for_Advisory_TR_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(OR(UPPER(Reported_By_Region__c) &lt;&gt; &apos;US – USI&apos;, UPPER(Reported_By_Region__c) &lt;&gt; &apos;US–USI&apos;, UPPER(Reported_By_Region__c) &lt;&gt; &apos;US - USI&apos;, UPPER(Reported_By_Region__c) &lt;&gt; &apos;US-USI&apos;) , Reported_By_Federal_Practice__c &lt;&gt; TRUE,  OR(UPPER(Reported_by_FSS__c) = &apos;AERS&apos;,  UPPER(Reported_by_FSS__c) = &apos;ADVISORY&apos;,  UPPER(Reported_by_FSS__c) = &apos;FAS&apos;,  UPPER(Reported_by_FSS__c) = &apos;DELOITTE ADVISORY&apos;,  UPPER(Reported_by_FSS__c) = &apos;AERS ADVISORY&apos;),  OR($Profile.Id = $Label.CIC_Agent_Profile_Id , $Profile.Id = $Label.CIC_Manager_Profile_Id),  PRIORVALUE(OwnerId) &lt;&gt; OwnerId,  OwnerId = $Label.TR_Queue_ID,Is_Queue_check__c=false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign to Audit TR queue</fullName>
        <actions>
            <name>Audit_TR_queue_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Audit_TR_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Is_Queue_check_for_Audit_TR_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(OR(UPPER(Reported_By_Region__c) &lt;&gt; &apos;US – USI&apos;, UPPER(Reported_By_Region__c) &lt;&gt; &apos;US–USI&apos; , UPPER(Reported_By_Region__c) &lt;&gt; &apos;US - USI&apos;, UPPER(Reported_By_Region__c) &lt;&gt; &apos;US-USI&apos; ), Reported_By_Federal_Practice__c &lt;&gt; TRUE, UPPER(Reported_by_FSS__c) = &apos;AUDIT&apos;, OR($Profile.Id = $Label.CIC_Agent_Profile_Id , $Profile.Id = $Label.CIC_Manager_Profile_Id),  PRIORVALUE(OwnerId) &lt;&gt; OwnerId,  OwnerId = $Label.TR_Queue_ID,Is_Queue_check__c=false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign to Consulting TR queue</fullName>
        <actions>
            <name>Consulting_TR_queue_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Consulting_TR_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Is_Queue_check_for_Consulting_TR_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR(UPPER(Reported_By_Region__c) &lt;&gt; &apos;US – USI&apos;, UPPER(Reported_By_Region__c) &lt;&gt; &apos;US–USI&apos;, UPPER(Reported_By_Region__c) &lt;&gt; &apos;US - USI&apos;, UPPER(Reported_By_Region__c) &lt;&gt; &apos;US-USI&apos;) ,  Reported_By_Federal_Practice__c &lt;&gt; TRUE,  UPPER(Reported_by_FSS__c) = &apos;CONSULTING&apos;,  OR($Profile.Id = $Label.CIC_Agent_Profile_Id , $Profile.Id = $Label.CIC_Manager_Profile_Id),  PRIORVALUE(OwnerId) &lt;&gt; OwnerId,  OwnerId = $Label.TR_Queue_ID,Is_Queue_check__c=FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign to Services TR queue</fullName>
        <actions>
            <name>Services_TR_queue_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Is_Queue_check_for_Services_TR_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Services_TR_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(OR(UPPER(Reported_By_Region__c) &lt;&gt; &apos;US – USI&apos;, UPPER(Reported_By_Region__c) &lt;&gt; &apos;US–USI&apos;, UPPER(Reported_By_Region__c) &lt;&gt; &apos;US - USI&apos;, UPPER(Reported_By_Region__c) &lt;&gt; &apos;US-USI&apos;), Reported_By_Federal_Practice__c &lt;&gt; TRUE,  OR(UPPER(Reported_by_FSS__c) = &apos;WORKPLACE SERVIES&apos;,  UPPER(Reported_by_FSS__c) = &apos;SERVICES&apos;,  UPPER(Reported_by_FSS__c) = &apos;FEDERAL EA&apos;,  UPPER(Reported_by_FSS__c) = &apos;FINANCE&apos;,  UPPER(Reported_by_FSS__c) = &apos;GENERAL COUNSEL&apos;,  UPPER(Reported_by_FSS__c) = &apos;INVESTMENTS&apos;,  UPPER(Reported_by_FSS__c) = &apos;MARKET DEVELOPMENT&apos;,  UPPER(Reported_by_FSS__c) = &apos;REGULATORY &amp; PROF. MATTERS&apos;,  UPPER(Reported_by_FSS__c) = &apos;RISK&apos;,  UPPER(Reported_by_FSS__c) = &apos;RISK, REPUTATION &amp; REG AFFAIR&apos;,  UPPER(Reported_by_FSS__c) = &apos;SB&amp;I&apos;,  UPPER(Reported_by_FSS__c) = &apos;STRATEGY INNOVATION TRANSFORM&apos;,  UPPER(Reported_by_FSS__c) = &apos;TALENT&apos;,  UPPER(Reported_by_FSS__c) = &apos;TECHNOLOGY&apos;),  OR($Profile.Id = $Label.CIC_Agent_Profile_Id , $Profile.Id = $Label.CIC_Manager_Profile_Id),  PRIORVALUE(OwnerId) &lt;&gt; OwnerId,  OwnerId = $Label.TR_Queue_ID,Is_Queue_check__c=FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign to Talent Relations</fullName>
        <actions>
            <name>Talent_Relations_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Talent_Relations</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>talentcheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(OR(Reported_by_Anonymous__c = TRUE, UPPER(Reported_by_Contact_Type__c) = &apos;EXTERNAL&apos;,  UPPER(Reported_by_FSS__c) = &apos;GLOBAL&apos;,  UPPER(Reported_by_FSS__c) = &apos;NATIONAL&apos;,  UPPER(Reported_by_FSS__c) = &apos;PARENT&apos;,  UPPER(Reported_by_FSS__c) = &apos;USA&apos;),OR($Profile.Id = $Label.CIC_Agent_Profile_Id , $Profile.Id = $Label.CIC_Manager_Profile_Id),  PRIORVALUE(OwnerId) &lt;&gt; OwnerId,  OwnerId = $Label.TR_Queue_ID,Is_Queue_check__c=false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign to USI TR queue</fullName>
        <actions>
            <name>USI_TR_queue_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Is_Queue_check_for_USI_TR_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>USI_TR_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(OR(UPPER(Reported_By_Region__c) = &apos;US – USI&apos;, UPPER(Reported_By_Region__c) = &apos;US–USI&apos;, UPPER(Reported_By_Region__c) = &apos;US - USI&apos;, UPPER(Reported_By_Region__c) = &apos;US-USI&apos;), OR( $Profile.Id = $Label.CIC_Agent_Profile_Id , $Profile.Id = $Label.CIC_Manager_Profile_Id), PRIORVALUE(OwnerId) &lt;&gt; OwnerId, OwnerId = $Label.TR_Queue_ID,Is_Queue_check__c=false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CHANGE CONTROL - Send Mail to Case Owner</fullName>
        <actions>
            <name>Change_Control_Send_Mail_to_Case_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send emails to Case Owner for Change Control Request type</description>
        <formula>AND( RecordType.DeveloperName == &apos;Change_Control_Request&apos;, ISCHANGED( OwnerId ) ) || AND( ISNEW() ,RecordType.DeveloperName == &apos;Change_Control_Request&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CHANGE CONTROL - Send Mail to Case Reported BY</fullName>
        <actions>
            <name>Change_Control_Send_Mail_to_Case_Reported_By</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send emails to Case Reported By for Change Control Request type</description>
        <formula>RecordType.DeveloperName == &apos;Change_Control_Request&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case %3A Update support area of 5 users</fullName>
        <actions>
            <name>Update_to_support_area</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When case is raised from below 5 users then support area is changed to US.</description>
        <formula>OR(
CONTAINS( ContactId , &apos;0034000001Dv7Ve&apos;), 
CONTAINS( ContactId , &apos;0034000001Dv2y5&apos;), 
CONTAINS( ContactId , &apos;0034000001KKTTX&apos;),CONTAINS( ContactId , &apos;0034000001DvA71&apos;),CONTAINS( ContactId , &apos;0034000001DvSc7&apos;),CONTAINS( ContactId , &apos;0034000001DvOEm&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case Cancel Automatic Reply cases for Offers</fullName>
        <actions>
            <name>Case_Canceled</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_Resolution_Notes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Deloitte India Offers,Deloitte US Offers</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>startsWith</operation>
            <value>Automatic: reply,[WARNING: MESSAGE ENCRYPTED</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Category Learning Design</fullName>
        <actions>
            <name>Case_Category_Update_Learning_Design</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Learning Design Team</value>
        </criteriaItems>
        <description>Update Case Category to Learning Design Services</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Category Sub-Category Update for India Offer Letters</fullName>
        <actions>
            <name>Case_Update_Category_to_Acquisition</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_SubCategory_to_Offer_Letter</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WCT_Set_Support_Area_to_India</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Deloitte India Offers</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Category Sub-Category Update for US Offer Letters</fullName>
        <actions>
            <name>Case_Update_Category_to_Acquisition</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_SubCategory_to_Offer_Letter</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WCT_Set_Support_Area_to_US</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Deloitte US Offers</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Category Update</fullName>
        <actions>
            <name>Case_Category_Programs</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>US ELE Certification,US ELE Programs</value>
        </criteriaItems>
        <description>Update Case Category to ELE: Programs</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Category Update -- Payroll - US</fullName>
        <actions>
            <name>Case_Category_Update_to_Payroll_US</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Record_type_to_Payroll</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Firm Payroll - US,PPS Resource - US,PPS Employee Solutions - US</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Category Update Compliance Queue</fullName>
        <actions>
            <name>Update_Category_Field_for_Compliance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>US ELE Personnelfiles,US I-9 Support Center Mailbox,US IND EMP Name Change,US National E-Verify</value>
        </criteriaItems>
        <description>Update Case Category to ELE: Compliance</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Category Update ELE Onboarding</fullName>
        <actions>
            <name>Case_Category_Update_ELE_Onboarding</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>ELE Onboarding</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Category Update Learning Delivery 1</fullName>
        <actions>
            <name>Case_Category_Update_Learning_Delivery</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Learning Technology Team</value>
        </criteriaItems>
        <description>If Case Origin is  &apos;Learning Technology Team&quot; Update Category to &quot;Learning: Poll Everywhere&quot;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Category Update Learning Delivery 2</fullName>
        <actions>
            <name>Case_Category_Update_Learning_Delivery_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>US CTS CPE Support,US Local CPE Support</value>
        </criteriaItems>
        <description>If Case Origin is &apos;US CTS CPE Support&apos; update Category to &apos;Learning: CPE&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Category Update Learning Delivery 3</fullName>
        <actions>
            <name>Case_Category_Update_Learning_Delivery_3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>US Deloitte Learning Center</value>
        </criteriaItems>
        <description>If Case Origin is &quot;US Deloitte Learning Center&quot; update Category to &quot;Learning: Catalog Management&quot;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Category Update Learning Delivery 4</fullName>
        <actions>
            <name>Case_Category_Update_Learning_Delivery_4</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>US Learning Delivery Support,US Industry Learning CRM,US FAS Learning CRM,US Tax Learning CRM,US Advisory Learning CRM,US Audit Learning CRM,US Consulting Learning CRM,US Services Learning CRM,US LD Learning CRM,US Federal Learning CRM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>IFRS Training,US Audit Learning</value>
        </criteriaItems>
        <description>If Case Origin is &quot;US Learning Delivery Support&quot; update Category to &quot;Learning: CRM&quot;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Category Update Learning Delivery 5</fullName>
        <actions>
            <name>Case_Category_Update_Learning_Delivery_5</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>US National Learning Evaluations</value>
        </criteriaItems>
        <description>If Case Origin is &quot;US National Learning Evaluations&quot; update Category to &quot;Learning: MTM&quot;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Category Update Separations</fullName>
        <actions>
            <name>Case_Category_Update_Separations</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>US ELE Separations,USI India Exit Management</value>
        </criteriaItems>
        <description>Update Case Category to Separations</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Category Update Talent Relations</fullName>
        <actions>
            <name>Case_Category_Update_Talent_Relations</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>US Talent Relations,USI Talent Relations</value>
        </criteriaItems>
        <description>Update Case Category to Inquiry</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Category Update Transfers</fullName>
        <actions>
            <name>Case_Category_Update_Transfers</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>US ELE Transfers,US India Internal Mobility</value>
        </criteriaItems>
        <description>Update Case Category to ELE: Transfers</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Category Update USI Performance Management</fullName>
        <actions>
            <name>Case_Category_Update_Performance_Manag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>USI Performance Management</value>
        </criteriaItems>
        <description>If Case Origin is &quot;US Learning Delivery Support&quot; update Category Development: Performance Management</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Category Update to Payroll - US</fullName>
        <actions>
            <name>Case_Category_Update_to_Payroll_US</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Payroll</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Created Notification to the Contact</fullName>
        <actions>
            <name>Customer_Interaction_Center_Follow_Up</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>IsNew_flag_update_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>US Talent CIC Inbox</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsNew__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>IF The Case Origin is Email to Case then 
Send E-mail to Case.Contact using the &quot;A Case has been created for you..&quot; template</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case Created Notification to the Contact_USI</fullName>
        <actions>
            <name>Customer_Interaction_Center_Follow_Up_USI</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>IsNew_flag_update_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>USI Talent CIC Inbox</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsNew__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>IF The Case Origin is Email to Case then 
Send E-mail to Case.Contact using the &quot;A Case has been created for you..&quot; template</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case ELE-InVolSep Priority to Critical</fullName>
        <actions>
            <name>Case_Update_Priority_to_Critical</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Employee Lifecycle Events - Involuntary Separations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Category__c</field>
            <operation>equals</operation>
            <value>Separations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_SubCategory1__c</field>
            <operation>equals</operation>
            <value>Involuntary Separations</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Email to ININ</fullName>
        <actions>
            <name>Send_Email_to_ININ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send Email to ININ when ININ Is updated</description>
        <formula>NOT(ISPICKVAL(Status , &apos;Canceled&apos;)) &amp;&amp; (ISCHANGED( OwnerId )|| ISNEW() ) &amp;&amp; (Owner:Queue.DeveloperName = &apos;Customer_Interaction_Center_India&apos; || Owner:Queue.DeveloperName =  &apos;CICQ&apos; || Owner:Queue.DeveloperName = &apos;PSN_US&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case Involuntary Seperation new</fullName>
        <actions>
            <name>Enter_in_HR_Admin</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Initiate_Involuntary_Separation_Process</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_SubCategory1__c</field>
            <operation>equals</operation>
            <value>Involuntary Separations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Support_Area__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <description>Create a series of Tasks associated to the Case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Priority is Set to Critical</fullName>
        <active>true</active>
        <description>Send E-mail</description>
        <formula>ISPICKVAL( Priority , &apos;1 - Critical&apos;) &amp;&amp;
ISPICKVAL( Status , &apos;New&apos;) &amp;&amp;
(Owner:User.Profile.Name = &apos;2_CIC_Basic&apos; || Owner:User.Profile.Name = &apos;2_CIC_Manager&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_High_Priority_Email_to_users_with_2_CIC_Manager_profile</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Case_Time_Check</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Rule to update record type For Finance cases</fullName>
        <actions>
            <name>Case_RecordType_update_For_Finance_cases</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Case.Case_Owner_Role__c</field>
            <operation>equals</operation>
            <value>Finance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Finance - India</value>
        </criteriaItems>
        <description>This workflow rule is used to update the record type to Finance for Finance cases</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Rule to update reopen date %26 time for Finance Cases</fullName>
        <actions>
            <name>Reopen_date_and_time_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Reopened</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Finance</value>
        </criteriaItems>
        <description>This Workflow rule updates the reopened date and time for the case whenever the case is reopened</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Send Separation Emails for Corporate Card Balance Request</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_Send_Email_On__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Corporate_Card_Team__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Case Send Separation Emails for Corporate Card Balance Request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_When_Corporate_team_is_true_on_Send_Separation_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.WCT_Send_Email_On__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Send Separation Emails for Immigration Request</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_Send_Email_On__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Immigration_Mail__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Case Send Separation Emails for Immigration Request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_When_Immigration_Request_is_true_on_Send_Separation_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.WCT_Send_Email_On__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Send Separation Emails for Involuntary %E2%80%93 Corporate Cards</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_Involuntary_Corporate_Cards__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Send_Email_On__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Case Send Separation Emails for Involuntary – Corporate Cards</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_When_Involuntary_Corporate_Cards_is_true_on_Send_Separation_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.WCT_Send_Email_On__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Send Separation Emails for Involuntary %E2%80%93 Records Management</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_Involuntary_Records_Management__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Send_Email_On__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Case Send Separation Emails for Involuntary – Records Management</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_When_Involuntary_Records_Managementis_true_on_Send_Separation_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.WCT_Send_Email_On__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Send Separation Emails for Involuntary - PDA</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_Involuntary_PDA__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Send_Email_On__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Case Send Separation Emails for Involuntary - PDA</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_When_Involuntary_PDA_is_true_on_Send_Separation_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.WCT_Send_Email_On__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Send Separation Emails for Involuntary Termination</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_Involuntary_Termination__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Send_Email_On__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Case Send Separation Emails for Involuntary Termination</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_When_Involuntary_Termination_is_true_on_Send_Separation_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.WCT_Send_Email_On__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Send Separation Emails for Involuntary Termination - Request PTO</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_Involuntary_Termination_Request_PTO__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Send_Email_On__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Case Send Separation Emails for Involuntary Termination - Request PTO</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_When_Involuntary_Termination_Request_PTO_is_true_on_Send_Separation_E</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.WCT_Send_Email_On__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Send Separation Emails for PDA</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_Send_Email_On__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_PDA__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_When_PDA_is_true_on_Send_Separation_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.WCT_Send_Email_On__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Send Separation Emails for Records Management Request</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_Send_Email_On__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Record_Management__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Case Send Separation Emails for Records Management Request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_When_Records_Management_is_true_on_Send_Separation_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.WCT_Send_Email_On__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Send Separation Emails to voluntary Accounting Firm</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_Accounting_Firm__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Send_Email_On__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Case Send Separation Emails to voluntary Accounting Firm</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_When_Accounting_Firm_is_true_on_Send_Separation_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.WCT_Send_Email_On__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Send Separation Emails to voluntary Request for New Employer Information</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_Request_for_New_Employer_Information__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Send_Email_On__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Case Send Separation Emails to voluntary Request for New Employer Information</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_When_Case_Send_Separation_Emails_to_voluntary_Request_for_New_Employe</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.WCT_Send_Email_On__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Send Separation Emails to voluntary Resignation Notification - ERS</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_Resignation_Notification_ERS__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Send_Email_On__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Case Send Separation Emails to voluntary Resignation Notification - ERS</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_When_Case_Send_Separation_Emails_to_voluntary_Resignation_Notificatio</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.WCT_Send_Email_On__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Send Separation Emails to voluntary Restricted Entity</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_Vol_Restricted_Entity__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Send_Email_On__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Case Send Separation Emails to voluntary Restricted Entity</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_When_Voluntary_Restricted_Entity</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.WCT_Send_Email_On__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case Status Update to Duplicate</fullName>
        <actions>
            <name>Update_Duplicate_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>&quot;IF Case.WCT__DuplicateOf__c IS NOT NULL THEN
SET Case.Status = &quot;&quot;Closed - Duplicate&quot;&quot;&quot;</description>
        <formula>NOT(ISBLANK(WCT_Duplicate_Of__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Transfers</fullName>
        <actions>
            <name>Issue_Transfer_Memo_to_Employee_if_applicable</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Issue_a_Transfer_Transmittal</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Receive_Signed_Transfer_Memo_if_applicable</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_Category__c</field>
            <operation>equals</operation>
            <value>ELE: Transfers</value>
        </criteriaItems>
        <description>Create a series of Tasks associated to the Case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Voluntary Seperation or Retirement 2</fullName>
        <actions>
            <name>Enter_in_HR_Admin</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Initiate_Voluntary_Separation_Process</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_SubCategory1__c</field>
            <operation>equals</operation>
            <value>Retirement,Voluntary Separation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Support_Area__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <description>Case with Sub-Category 1 IN (&quot;Retirement,Voluntary Separation, Death, Property Collection, Termination Processing&quot;)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case added to PPS Employee Solutions - US queue</fullName>
        <actions>
            <name>A_new_case_has_been_added_to_your_queue_PPS_Employee_Solutions_US</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>PPS Employee Solutions - US</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case added to PPS_Resource_US queue</fullName>
        <actions>
            <name>A_new_case_has_been_added_to_your_queue_mail_send_to_user_if_case_is_added_to_PP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>PPS Resource - US</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case created notification to contact from separations team</fullName>
        <actions>
            <name>Mail_from_ELE_Separations_team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Employee Lifecycle Events - Voluntary Separations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Support_Area__c</field>
            <operation>notEqual</operation>
            <value>INDIA</value>
        </criteriaItems>
        <description>Email notification sent to contact</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case created notification to contact from transfers team</fullName>
        <actions>
            <name>Email_notification_sent_to_contact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_Category__c</field>
            <operation>equals</operation>
            <value>ELE: Transfers</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case is Assigned to Acquisition  Send Mail to Members</fullName>
        <actions>
            <name>Send_Email_to_Acquisition_Q_Members</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When a case has been escalated to Acquisition Q Send an Email to all the members in that Q</description>
        <formula>ISCHANGED( OwnerId ) &amp;&amp; Owner:Queue.QueueName = &apos;Acquisition&apos; &amp;&amp; NOT(ISPICKVAL(WCT_Support_Area__c,&apos;INDIA&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case is Assigned to Acquisition  Send Mail to Members India</fullName>
        <actions>
            <name>Send_Email_to_Acquisition_Q_Members_India</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When a case has been escalated to Acquisition Q Send an Email to all the members in that Q</description>
        <formula>ISCHANGED( OwnerId ) &amp;&amp; Owner:Queue.QueueName = &apos;Acquisition&apos; &amp;&amp; (ISPICKVAL(WCT_Support_Area__c,&apos;INDIA&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case is Assigned to Development%3A Performance Management Send Mail to Members US</fullName>
        <actions>
            <name>Send_Email_to_Development_Performance_Management_Q_Members</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>When a case has been escalated to Development: Performance Management Q Send an Email to all the members in that Q</description>
        <formula>ISCHANGED( OwnerId ) &amp;&amp; Owner:Queue.QueueName = &apos;Development: Performance Management&apos; &amp;&amp;  ISPICKVAL(WCT_Support_Area__c,&apos;US&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case is Assigned to Development%3A Performance Management Send Mail to Members USI</fullName>
        <actions>
            <name>Send_Email_to_Development_Performance_Management_Q_Members_USI</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>When a case has been escalated to Development: Performance Management Q Send an Email to all the members in that Q</description>
        <formula>ISCHANGED( OwnerId ) &amp;&amp; Owner:Queue.QueueName = &apos;Development: Performance Management&apos; &amp;&amp;  ISPICKVAL(WCT_Support_Area__c,&apos;INDIA&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case is Assigned to Jeevan Send Mail to Members</fullName>
        <actions>
            <name>New_Case</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When a case has been escalated to Jeevan Q Send an Email to all the members in that Queue</description>
        <formula>ISCHANGED( OwnerId ) &amp;&amp; Owner:Queue.QueueName = &apos;Jeevan Team&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case is Assigned to Knowledge Management Send Mail to Members</fullName>
        <actions>
            <name>Send_Email_to_Knowledge_Management_Q_Members</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When a case has been escalated to Knowledge Management Q Send an Email to all the members in that Q</description>
        <formula>ISCHANGED( OwnerId ) &amp;&amp; Owner:Queue.QueueName = &apos;Knowledge Management&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case is Assigned to Mobility Send Mail to Members</fullName>
        <actions>
            <name>Send_Email_to_Mobility_Q_Members</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When a case has been escalated to Mobility Q Send an Email to all the members in that Q</description>
        <formula>ISCHANGED( OwnerId ) &amp;&amp;  Owner:Queue.DeveloperName  = &apos;Mobility_US&apos; &amp;&amp; NOT(ISPICKVAL(WCT_Support_Area__c,&apos;INDIA&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case is Assigned to Operations Mgt%3A Continuous Improvements Send Mail to Members</fullName>
        <actions>
            <name>Send_Email_to_Operations_Mgt_Continuous_Improvements_Q_Members</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When a case has been escalated to Operations Mgt: Continuous Improvements Q Send an Email to all the members in that Q</description>
        <formula>ISCHANGED( OwnerId ) &amp;&amp; Owner:Queue.QueueName = &apos;Operations Mgt: Continuous Improvements&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case is Assigned to Reporting Send Mail to Members</fullName>
        <actions>
            <name>Send_Email_to_Reporting_Q_Members</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When a case has been escalated to Reporting Q Send an Email to all the members in that Q</description>
        <formula>ISCHANGED( OwnerId ) &amp;&amp; Owner:Queue.QueueName = &apos;Reporting&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case is Assigned to Resource Management Send Mail to Members</fullName>
        <actions>
            <name>Send_Email_to_Resource_Management_Q_Members</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When a case has been escalated to Resource Management Q Send an Email to all the members in that Q</description>
        <formula>ISCHANGED( OwnerId ) &amp;&amp; Owner:Queue.QueueName = &apos;Resource Management&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case is Assigned to Talent Relations Send Mail to Members</fullName>
        <actions>
            <name>Send_Email_to_TR_Intake_Coordinator_Q_Members</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>When a case has been escalated to Talent Relations Q Send an Email to all the members in that Q</description>
        <formula>ISCHANGED( OwnerId ) &amp;&amp; Owner:Queue.QueueName = &apos;Talent Relations&apos; &amp;&amp; NOT(ISPICKVAL(WCT_Support_Area__c,&apos;INDIA&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case is Assigned to Talent Relations Send Mail to Members - India</fullName>
        <actions>
            <name>Send_Email_to_TR_USI_Group</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When a case has been escalated to Talent Relations Q Send an Email to all the members in that Q</description>
        <formula>ISCHANGED( OwnerId ) &amp;&amp; Owner:Queue.QueueName = &apos;Talent Relations&apos; &amp;&amp; (ISPICKVAL(WCT_Support_Area__c,&apos;INDIA&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case is Assigned to Technology Send Mail to Members</fullName>
        <actions>
            <name>Send_Email_to_Technology_Q_Members</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When a case has been escalated to Technology Q Send an Email to all the members in that Q</description>
        <formula>ISCHANGED( OwnerId ) &amp;&amp; Owner:Queue.QueueName = &apos;Technology&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case is Assigned to Vendor Relationship Management Send Mail to Members</fullName>
        <actions>
            <name>Send_Email_to_Vendor_Relationship_Management_Q_Members</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When a case has been escalated to Vendor Relationship Management Q Send an Email to all the members in that Q</description>
        <formula>ISCHANGED( OwnerId ) &amp;&amp; Owner:Queue.QueueName = &apos;Vendor Relationship Management&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case is Assigned to a Non CIC User</fullName>
        <actions>
            <name>Update_Status_to_Pending_Tier_2_Action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>IF the Case.CaseOwner is updated AND the Case.CaseOwner is a User AND the Case.CaseOwner &lt;&gt; Case.LastModifiedBy AND Case Owner&apos;s Profile is NOT IN (2_CIC_Agent, 2_CIC_Manager) THEN
Send E-mail to Case.CaseOwner</description>
        <formula>AND( $Profile.Name &lt;&gt; &apos;6_Integration&apos;,ISBLANK(Owner:Queue.Id), NOT(ISBLANK( Owner:User.Id )),   Owner:User.Profile.Name &lt;&gt; &apos;2_CIC_Basic&apos;, Owner:User.Profile.Name &lt;&gt; &apos;2_CIC_Manager&apos;,Owner:User.Id &lt;&gt; CreatedById, RecordType.Name &lt;&gt;&apos;Pre-BI&apos;,RecordType.Name &lt;&gt; &apos;Process Issue&apos;,RecordType.Name &lt;&gt;&apos;Change Control Request&apos;, NOT(ISPICKVAL( Origin , &apos;US ELE Personnelfiles&apos;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case is Assigned to a Queue other than %22CIC Queue%22</fullName>
        <actions>
            <name>Update_Escalated</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_EscalatedTime</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_to_Tier_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>&quot;IF the Case is Assigned to a Queue that is NOT the CIC Queue
SET WCT__Escalated__c = Yes
SET WCT_EscalatedTime__c = current date and time&quot;</description>
        <formula>( Owner:Queue.DeveloperName &lt;&gt; &apos;CICQ&apos; &amp;&amp;   Owner:Queue.DeveloperName &lt;&gt; &apos;Customer_Interaction_Center_India&apos; &amp;&amp;   Owner:Queue.DeveloperName &lt;&gt; &apos;PSN_US&apos; ) &amp;&amp; NOT(ISBLANK(Owner:Queue.Id))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case is Updated OR Another record is appended to the Case</fullName>
        <actions>
            <name>Case_Reopen_Datetime</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Reopen_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_In_Progress_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>&quot;IF the Case is Updated OR Another record is appended to the Case
UPDATE the Status = In Progress&quot;. 

The workflow amended to prevent the closed cases to reopen.</description>
        <formula>AND(   $Profile.Name &lt;&gt; &apos;6_Integration&apos;, NOT( ISNEW()),  NOT(ISPICKVAL( Status , &apos;Pending - Customer Action&apos;)),  NOT(ISPICKVAL( Status , &apos;Pending – Tier 2 Action&apos;)),  NOT(ISPICKVAL( Status , &apos;Pending - Vendor Action&apos;)),  NOT(ISPICKVAL( Status , &apos;Pending – Tier 3 Action&apos;)),  NOT(ISPICKVAL( Status , &apos;Pending - Non SFDC&apos;)),   
NOT(IsClosed = True),  
NOT(ISPICKVAL( Status , &apos;Resolved&apos;)),   NOT(ISCHANGED(OwnerId)),  NOT(ISCHANGED(Status)),  NOT(ISCHANGED(Unread_Email_on_Closed_Case__c)), NOT(ISCHANGED(WCT_isEmailonCase__c)),   NOT (OR(RecordType.DeveloperName = &apos;Process_Issue&apos;,     RecordType.DeveloperName =&apos;Change_Control_Request&apos;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case is Updated OR Another record is appended to the Case BKP1</fullName>
        <active>false</active>
        <description>&quot;IF the Case is Updated OR Another record is appended to the Case
UPDATE the Status = In Progress&quot;. 

The workflow amended to prevent the closed cases to reopen.</description>
        <formula>AND(   $Profile.Name &lt;&gt; &apos;6_Integration&apos;, NOT( ISNEW()),  NOT(ISPICKVAL( Status , &apos;Pending - Customer Action&apos;)),  NOT(ISPICKVAL( Status , &apos;Pending – Tier 2 Action&apos;)),  NOT(ISPICKVAL( Status , &apos;Pending - Vendor Action&apos;)),  NOT(ISPICKVAL( Status , &apos;Pending – Tier 3 Action&apos;)),  NOT(ISPICKVAL( Status , &apos;Pending - Non SFDC&apos;)),   NOT(ISPICKVAL( Status , &apos;Closed - Sent to GSD&apos;)),  NOT(ISPICKVAL( Status , &apos;Closed - Sent to OHD&apos;)),  NOT(ISPICKVAL( Status , &apos;Closed - Sent to FHD&apos;)),  NOT(ISPICKVAL( Status , &apos;Closed - Sent to PSN&apos;)),  NOT(ISPICKVAL( Status , &apos;Closed – Sent to B&amp;T&apos;)),  NOT(ISPICKVAL( Status , &apos;Closed&apos;)),  NOT(ISPICKVAL( Status , &apos;Closed – Duplicate&apos;)),  NOT(ISPICKVAL( Status , &apos;Resolved&apos;)),   NOT(ISCHANGED(OwnerId)),  NOT(ISCHANGED(Status)),  NOT(ISCHANGED(Unread_Email_on_Closed_Case__c)), NOT(ISCHANGED(WCT_isEmailonCase__c)),   NOT (OR(RecordType.DeveloperName = &apos;Process_Issue&apos;,     RecordType.DeveloperName =&apos;Change_Control_Request&apos;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case is Updated OR Another record is appended to the Case Bkp</fullName>
        <active>false</active>
        <description>&quot;IF the Case is Updated OR Another record is appended to the Case
UPDATE the Status = In Progress&quot;</description>
        <formula>AND( $Profile.Name &lt;&gt; &apos;6_Integration&apos;,NOT( ISNEW()), NOT(ISPICKVAL( Status , &apos;Pending - Customer Action&apos;)), NOT(ISPICKVAL( Status , &apos;Pending – Tier 2 Action&apos;)), NOT(ISPICKVAL( Status , &apos;Pending - Vendor Action&apos;)), NOT(ISPICKVAL( Status , &apos;Pending – Tier 3 Action&apos;)), NOT(ISPICKVAL( Status , &apos;Pending - Non SFDC&apos;)), NOT(ISCHANGED(OwnerId)), NOT(ISCHANGED(Status)), NOT(ISCHANGED(Unread_Email_on_Closed_Case__c)),NOT(ISCHANGED(WCT_isEmailonCase__c)),  NOT (OR(RecordType.DeveloperName = &apos;Process_Issue&apos;,     RecordType.DeveloperName =&apos;Change_Control_Request&apos;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case is assigned only for US Tax and Consulting Professionals</fullName>
        <actions>
            <name>Compensation_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ToD_Case_Category__c</field>
            <operation>equals</operation>
            <value>U.S. Consulting Professional – Request a compensation discussion with a leader,U.S. Tax Professional - Request a compensation discussion with a leader</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case status Reopened for closed case when new mail arrives</fullName>
        <actions>
            <name>Case_status_update_to_Reopened</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Unread_Email_on_Closed_Case__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>GMI - Immigration</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Clear SubCategories for Payroll US</fullName>
        <actions>
            <name>Case_Sub_Category_2_Update_to_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Sub_Category_3_Update_to_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Sub_Category_Update_to_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Payroll</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Category__c</field>
            <operation>equals</operation>
            <value>Payroll - US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_SubCategory1__c</field>
            <operation>notEqual</operation>
            <value>01.Pay Advice-Left Side (Earnings),02.Pay Advice-Right Side(Taxes/Deduction),03.General Payroll Inquiry,04.W2/Wage Reporting</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update Escalated checkbox</fullName>
        <actions>
            <name>Update_Escalated</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_EscalatedTime</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_to_Tier_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( OwnerId ) &amp;&amp; 
( (PRIORVALUE(WCT_User_Tier__c) = &apos;Tier 1&apos; &amp;&amp; WCT_User_Tier__c &lt;&gt; &apos;Tier 1&apos;) || 
(WCT_User_Tier__c &lt;&gt; &apos;Tier 1&apos; &amp;&amp; PRIORVALUE(WCT_User_Tier__c) = &apos;Tier 2&apos; &amp;&amp; 
(PRIORVALUE(Case_Owner_Role__c) = &apos;PSN ERC&apos; || PRIORVALUE(Case_Owner_Role__c) = &apos;PSN BDC&apos; || PRIORVALUE(Case_Owner_Role__c) = &apos;PSN Internal Support&apos; )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update Support Area to USI for India ExitCases</fullName>
        <actions>
            <name>WCT_Set_Support_Area_to_India</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Employee Lifecycle Events - Voluntary Separations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>USI India Exit Management</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Contact_Name_Region__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A UpdateCaseOwner Support Are Warning</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerChangeWrg__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_SetCaseOwnerFld</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Trigger_Time_01__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Case%3AUpdate fields for PPS Leaves Email-to-case</fullName>
        <actions>
            <name>Category</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WCT_Set_Support_Area_to_US</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>PPS Leaves Inbox</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case- Update R-Type to Leaves for Leave category cases</fullName>
        <actions>
            <name>Case_Update_R_Type_to_Leaves_for_Leave</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_Category__c</field>
            <operation>equals</operation>
            <value>Leaves and Disabilities - US</value>
        </criteriaItems>
        <description>Record Type will be updated to Leaves whenever case category is Leaves and Disalilities- US</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Control-Scope Of Service-Implementation%2Fsocialization%2Fcommunication of the change is underway</fullName>
        <actions>
            <name>Create_project_plan_for_implementation_including_technology_needs_engage_TTGB_as</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Identify_project_manager_for_implementation_if_new_process_scope</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Manage_KM_items</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Manage_business_case_items</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Manage_communication_items</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Manage_logistics_items</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Manage_metrics_items</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Manage_systems_technology_items</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Manage_training_items</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Change Control Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Implementing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Sub_Type__c</field>
            <operation>equals</operation>
            <value>Change in Talent Scope of Services</value>
        </criteriaItems>
        <description>Implementation/socialization/communication of the change is underway</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change Control-Talent To Business-Implementation%2Fsocialization%2Fcommunication of the change is underway</fullName>
        <actions>
            <name>Identify_project_manager_for_implementation_if_needed</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Manage_KM_items_Talent_To_Business</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Manage_communication_items_Talent_To_Business</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Manage_logistics_items_if_applicable</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Manage_metrics_items_Talent_To_Business</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Manage_systems_technology_items_if_applicable</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Manage_training_items_Talent_To_Business</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Change Control Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Implementing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Sub_Type__c</field>
            <operation>equals</operation>
            <value>Change in Talent to Business SLA</value>
        </criteriaItems>
        <description>Implementation/socialization/communication of the change is underway</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ELE Form Professional Alert</fullName>
        <actions>
            <name>ELE_Form_Professional_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
            <value>ELE NEW CASE CREATED</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Case Assinged</fullName>
        <actions>
            <name>Assigned_to_Case_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>And (  Or(ISNew(),  ISCHANGED(OwnerId)) ,  RecordType.Name =&apos;Pre-BI&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notification of High%2Furgent technology case</fullName>
        <actions>
            <name>Notification_to_tech_team_of_high_critical_case</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Technology</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>1 - Critical,2 - High</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify PSN of escalations of cases with critical priority</fullName>
        <actions>
            <name>WCT_SMSEmail_Notify_PSN_of_escalations_of_cases_with_critical_priority</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notify PSN via SMS when a Critical priority case is escalated from Tier 1 to Tier 2.</description>
        <formula>(Owner:Queue.DeveloperName &lt;&gt; &apos;CICQ&apos; &amp;&amp; Owner:Queue.DeveloperName &lt;&gt; &apos;India_Customer_Interaction_Center&apos; &amp;&amp; Owner:Queue.DeveloperName &lt;&gt; &apos;US_PSN_Customer_Interaction_Center&apos;) &amp;&amp; NOT(ISBLANK(Owner:Queue.Id)) &amp;&amp; ISPICKVAL( Priority , &apos;1 - Critical&apos;)  &amp;&amp; ISPICKVAL(WCT_Support_Area__c, &apos;US-PSN&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Susan of Critical case about to breach SLA%2E</fullName>
        <active>false</active>
        <formula>NOT(ISBLANK(WCT_Due_Date__c))  &amp;&amp;   Owner:Queue.DeveloperName = &apos;PSN_ERC_US&apos;  &amp;&amp;    ISPICKVAL(Priority, &apos;1 - Critical&apos;)  &amp;&amp;   ISPICKVAL(WCT_Support_Area__c, &apos;US-PSN&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>WCT_Critical_priority_case_is_2_hours_from_breaching_SLA</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Notify the PSN when a high priority case %28P1%29 is coming in via SMS that will notify the team</fullName>
        <actions>
            <name>WCT_SMSEmail_High_Priority_Case_Created</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>2 - High</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Support_Area__c</field>
            <operation>equals</operation>
            <value>US-PSN</value>
        </criteriaItems>
        <description>Ability to notify the PSN when a high priority case (P1) is coming in via SMS that will notify the team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PROCESS ISSUE - Send Mail to Case Owner</fullName>
        <actions>
            <name>Send_Mail_to_Case_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send emails to Case Owner for Process Issue type</description>
        <formula>AND( RecordType.DeveloperName == &apos;Process_Issue&apos;, ISCHANGED( OwnerId ) ) || AND( ISNEW() ,RecordType.DeveloperName == &apos;Process_Issue&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PROCESS ISSUE - Send Mail to Case Reported BY</fullName>
        <actions>
            <name>Process_Issue_send_email_to_specified_people</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Send_Mail_to_Case_Reported_By</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send emails to Case Reported By for Process Issue type</description>
        <formula>RecordType.DeveloperName == &apos;Process_Issue&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send Email to Delivery Cloud</fullName>
        <actions>
            <name>A_Case_has_been_assigned_for_you</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>SFDC Delivery Cloud</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send SMS when a case of high or critical priority is added to the ERC queue</fullName>
        <actions>
            <name>WCT_SMS_Email_Critical_Priority_Case</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send a text message on phone when a P1 Benefits case is about to breach SLA</description>
        <formula>Owner:Queue.DeveloperName = &apos;PSN_ERC_US&apos;  &amp;&amp;    (ISPICKVAL(Priority, &apos;1 - Critical&apos;) || ISPICKVAL(Priority, &apos;2 - High&apos;))  &amp;&amp;   ISPICKVAL(WCT_Support_Area__c, &apos;US-PSN&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Status to Canceled for Automatic reply%3A or Undelivered or Mailbox full</fullName>
        <actions>
            <name>Case_Canceled</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_Resolution_Notes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1  OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8)</booleanFilter>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>Automatic reply:</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>Undeliverable:</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>Mailbox full</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>Auto Reply</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>Auto reply</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>Auto-Reply</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>Please Do Not Reply</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>Message Recall Failure</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Status to Closed</fullName>
        <actions>
            <name>Case_Canceled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>startsWith</operation>
            <value>Message Recall Failure:,Automatic reply:,Accepted:,Declined:,Tentative:</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>ELE: New Hire Onboarding,Development: Performance Management,Mobility - India,Immigration - India</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Status to Closed for auto response</fullName>
        <actions>
            <name>Case_Canceled</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Update_Resolution_Notes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>auto response</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Mobility - India,Immigration - India</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Setting the Support Area to %22INDIA%22 On CTS Case Creation based on Contact Region</fullName>
        <actions>
            <name>WCT_Set_Support_Area_to_India</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(OR ( NOT( CONTAINS( ContactId , &apos;0034000001Dv7Ve&apos;) ),NOT( CONTAINS( ContactId , &apos;0034000001Dv2y5&apos;) ),NOT( CONTAINS( ContactId , &apos;0034000001KKTTX&apos;) ),NOT( CONTAINS( ContactId , &apos;0034000001DvA71&apos;) ),NOT( CONTAINS( ContactId , &apos;0034000001DvSc7&apos;) ),NOT( CONTAINS( ContactId , &apos;0034000001DvOEm&apos;) ) ),
OR( CONTAINS(Contact.WCT_Region__c, &apos;INDIA&apos;), CONTAINS( UPPER(Contact.WCT_Region__c), &apos;US - USI&apos;) ) &amp;&amp; NOT( ISPICKVAL(Origin,&apos;Deloitte US Offers&apos;) || ISPICKVAL(Origin,&apos;Deloitte India Offers&apos;) ||  ISPICKVAL(Origin,&apos;USI Talent CIC Inbox&apos;) || ISPICKVAL(Origin,&apos;US Talent CIC Inbox&apos;) || ISPICKVAL(Origin,&apos;PSN Benefits&apos;) )  &amp;&amp; ( Owner:Queue.DeveloperName &lt;&gt; &apos;CICQ&apos;  &amp;&amp; Owner:Queue.DeveloperName &lt;&gt; &apos;India_Customer_Interaction_Center&apos; &amp;&amp; Owner:Queue.DeveloperName &lt;&gt; &apos;PSN_US&apos; )  &amp;&amp; ISPICKVAL(WCT_Support_Area__c,&apos;&apos;) &amp;&amp; ( CreatedBy.Profile.Name &lt;&gt;&apos;2_CIC_Basic&apos; &amp;&amp; CreatedBy.Profile.Name &lt;&gt;&apos;2_CIC_Manager&apos; &amp;&amp; NOT(CONTAINS($Label.PSN_User_Roles,CreatedBy.UserRole.DeveloperName)) ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Setting the Support Area to %22India%22 on CIC Case Creation</fullName>
        <actions>
            <name>WCT_Set_Support_Area_to_India</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>if Case Owner = US CIC General Queue OR (Case origin=&quot;Phone,Chat&quot; &amp; Case created by Role is &quot;US CIC Agent,Manager&quot;</description>
        <formula>((Owner:Queue.DeveloperName  = &apos;Customer_Interaction_Center_India&apos;&amp;&amp; Owner:Queue.Id  &lt;&gt; NULL) || (CreatedBy.UserRole.DeveloperName =&apos;CIC_Manager_USI&apos; ||CreatedBy.UserRole.DeveloperName =&apos;CIC_Agent_USI&apos; ) ) &amp;&amp; ( ISPICKVAL(WCT_Support_Area__c,&apos;&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Setting the Support Area to %22PSN%22 on CIC Case Creation</fullName>
        <actions>
            <name>Set_Support_Area_to_PSN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>((Owner:Queue.DeveloperName  = &apos;PSN_US&apos;  &amp;&amp;   Owner:Queue.Id  &lt;&gt; NULL) ||  ( CONTAINS($Label.PSN_User_Roles,CreatedBy.UserRole.DeveloperName)  )) &amp;&amp; ( ISPICKVAL(WCT_Support_Area__c,&apos;&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Setting the Support Area to %22US%22 On CTS Case Creation based on Contact Region</fullName>
        <actions>
            <name>WCT_Set_Support_Area_to_US</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(ISBLANK(Contact_Name_Region__c) || (NOT(CONTAINS(Contact_Name_Region__c,&apos;INDIA&apos;))&amp;&amp; NOT(CONTAINS( UPPER(Contact_Name_Region__c),&apos;US - USI&apos;))) ) &amp;&amp; NOT( ISPICKVAL(Origin,&apos;Deloitte US Offers&apos;) || ISPICKVAL(Origin,&apos;Deloitte India Offers&apos;) || ISPICKVAL(Origin,&apos;USI Talent CIC Inbox&apos;) || ISPICKVAL(Origin,&apos;US Talent CIC Inbox&apos;) || ISPICKVAL(Origin,&apos;PSN Benefits&apos;) || ISPICKVAL(Origin,&apos;USI India Exit Management&apos;) )&amp;&amp; ( Owner:Queue.DeveloperName &lt;&gt; &apos;CICQ&apos;  &amp;&amp; Owner:Queue.DeveloperName &lt;&gt; &apos;India_Customer_Interaction_Center&apos;  &amp;&amp; Owner:Queue.DeveloperName &lt;&gt; &apos;PSN_US&apos; )  &amp;&amp; (  ISPICKVAL(WCT_Support_Area__c,&apos;&apos;) )  &amp;&amp; ( CreatedBy.Profile.Name &lt;&gt;&apos;2_CIC_Basic&apos;  &amp;&amp; CreatedBy.Profile.Name &lt;&gt;&apos;2_CIC_Manager&apos; &amp;&amp;  NOT(CONTAINS($Label.PSN_User_Roles,CreatedBy.UserRole.DeveloperName)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Setting the Support Area to %22US%22 on CIC Case Creation</fullName>
        <actions>
            <name>WCT_Set_Support_Area_to_US</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>( (Owner:Queue.DeveloperName  = &apos;CICQ&apos;  &amp;&amp;   Owner:Queue.Id  &lt;&gt; NULL) || (CreatedBy.UserRole.DeveloperName =&apos;CIC_Manager&apos; ||CreatedBy.UserRole.DeveloperName =&apos;CIC_Agent&apos; ) ) &amp;&amp; ( ISPICKVAL(WCT_Support_Area__c,&apos;&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sub category 3 updates N%2FA when sub cat 1 is RMS%2FTalent connect</fullName>
        <actions>
            <name>Field_update_subcateg3_N_A</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.WCT_SubCategory1__c</field>
            <operation>equals</operation>
            <value>RMS,Talent Connect (Careerify)</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TDR Send Email On Providing Additional comments%09To Owner</fullName>
        <actions>
            <name>TDR_Send_an_email_on_providing_additional_comments_by_practitioner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow is to send email to case owner on providing additional comments from TOd sites page.</description>
        <formula>IF(AND(OR(TDR_Additional_Comments__c != Null,TDR_Additional_Comments__c != &apos;&apos;),Gen_RecordType__c != &apos;&apos;,Gen_Request_Type__c !=&apos;&apos;),  TRUE, FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TDR_SendEmail For AdditionaI Info</fullName>
        <actions>
            <name>TDR_Sendemail_for_additionalinfo</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>TDR_Uncheckadditionalinforequired</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This wrokflow is to send email to requestor asking for additional information for the request made.</description>
        <formula>IF(AND(TDR_IsAdditionalInfoRequired__c == True,Gen_RecordType__c != &apos;&apos;,Gen_Request_Type__c !=&apos;&apos;),  TRUE, FALSE)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TRT Case Owner Change GMI</fullName>
        <active>true</active>
        <formula>CreatedDate = LastModifiedDate &amp;&amp;ISPICKVAL(WCT_Category__c,&apos;TRT Reporting&apos;)&amp;&amp; (Gen_RecordType__c =&apos;TRT GMNI&apos;) &amp;&amp; (TRT_Questions__c=false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TRT_Case_Owner_Change_GMI</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_3__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>TRT_Case_Owner_Change_GMI2</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_5__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>TRT Case Owner Change LE</fullName>
        <active>true</active>
        <formula>CreatedDate = LastModifiedDate &amp;&amp;ISPICKVAL(WCT_Category__c,&apos;TRT Reporting&apos;)&amp;&amp; (Gen_RecordType__c =&apos;TRT Learning Evaluation&apos;) &amp;&amp; (TRT_Questions__c=false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TRT_Case_Owner_Change_LE</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_3__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>TRT_Case_Owner_Change_LE2</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_5__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>TRT Case Owner Change Lea</fullName>
        <active>true</active>
        <formula>CreatedDate = LastModifiedDate &amp;&amp;ISPICKVAL(WCT_Category__c,&apos;TRT Reporting&apos;)&amp;&amp; (Gen_RecordType__c =&apos;TRT Learning Reporting&apos;) &amp;&amp; (TRT_Questions__c=false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TRT_Case_Change_Owner_Lea</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_3__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>TRT_Case_Owner_Change_Lea2</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_5__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>TRT Case Owner Change Other</fullName>
        <active>true</active>
        <formula>CreatedDate = LastModifiedDate &amp;&amp;ISPICKVAL(WCT_Category__c,&apos;TRT Reporting&apos;)&amp;&amp; (Gen_RecordType__c =&apos;TRT Other Reporting&apos;) &amp;&amp; (TRT_Questions__c=false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TRT_Case_Owner_Change_Other</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_3__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>TRT_Case_Owner_Change_Other2</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_5__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>TRT Case Owner Change PM</fullName>
        <active>true</active>
        <description>This work flow fires an email when case owner is not changed for more than 24hrs for Performance management</description>
        <formula>CreatedDate = LastModifiedDate &amp;&amp;ISPICKVAL(WCT_Category__c,&apos;TRT Reporting&apos;)&amp;&amp; (Gen_RecordType__c =&apos;TRT PM Reporting&apos;) &amp;&amp; (TRT_Questions__c=false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TRT_OwnerNotChanged_PM</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_3__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>TRT_OwnerNotChanged_PM2</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_5__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>TRT Case Owner Change Questions</fullName>
        <active>true</active>
        <formula>CreatedDate = LastModifiedDate &amp;&amp;ISPICKVAL(WCT_Category__c,&apos;TRT Reporting&apos;)&amp;&amp; (Gen_RecordType__c =&apos;TRT TA Reporting&apos;) &amp;&amp;(TRT_Questions__c=false)&amp;&amp;  WCT_isEmailonCase__c = true &amp;&amp;  TRT_Questions__c = true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_once_it_is_not_allocated_for</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_3__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_if_it_is_not_assigned_for_5_hrs</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_5__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>TRT Case Owner Change RM</fullName>
        <active>true</active>
        <formula>CreatedDate = LastModifiedDate &amp;&amp;ISPICKVAL(WCT_Category__c,&apos;TRT Reporting&apos;)&amp;&amp; (Gen_RecordType__c =&apos;TRT RM Reporting&apos;) &amp;&amp; (TRT_Questions__c=false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TRT_OwnerNotChanged_RM</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_3__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>TRT_OwnerNotChanged_RM2</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_5__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>TRT Case Owner Change Sap</fullName>
        <active>true</active>
        <description>This work flow fires an email when case owner is not changed for more than 24hrs</description>
        <formula>CreatedDate = LastModifiedDate &amp;&amp;ISPICKVAL(WCT_Category__c,&apos;TRT Reporting&apos;)&amp;&amp; (Gen_RecordType__c =&apos;TRT SAP Reporting&apos;) &amp;&amp; (TRT_Questions__c=false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TRT_OwnerNotChanged_Sap</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_3__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>TRT_OwnerNotChanged_Sap2</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_5__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>TRT Case Owner Change TA</fullName>
        <active>true</active>
        <formula>CreatedDate = LastModifiedDate &amp;&amp;ISPICKVAL(WCT_Category__c,&apos;TRT Reporting&apos;)&amp;&amp; (Gen_RecordType__c =&apos;TRT TA Reporting&apos;) &amp;&amp;(TRT_Questions__c=false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TRT_OwnerNotChanged_TA</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_3__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>TRT_OwnerNotChanged_TA2</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_5__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>TRT Case Owner Change survey</fullName>
        <active>true</active>
        <formula>CreatedDate = LastModifiedDate &amp;&amp;ISPICKVAL(WCT_Category__c,&apos;TRT Reporting&apos;)&amp;&amp; (Gen_RecordType__c =&apos;TRT Survey&apos; || Gen_RecordType__c =&apos;TRT Survey Reporting&apos;) &amp;&amp; (TRT_Questions__c=false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TRT_OwnerNotChanged_Survey</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_3__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>TRT_OwnerNotChanged_Survey2</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.TRT_BH_Created_date_5__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>TRT Update recordtype</fullName>
        <actions>
            <name>update_recordtype</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(ISPICKVAL( TRT_Emailtocasegroup__c,&quot;&quot; )), ISCHANGED( TRT_Emailtocasegroup__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Tod category US and USI Tax consult update sub and description</fullName>
        <actions>
            <name>update_description_of_case</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_subject_of_case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.ToD_Case_Category__c</field>
            <operation>equals</operation>
            <value>USI Tax Professionals - Compensation Query</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Description</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <description>Case raised from Tod form for TOD category US and USI Tax consult update subject and description as TOD Form</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>US Consulting and Tax Email notification</fullName>
        <actions>
            <name>US_Consulting_and_Tax_email_alert_when_case_is_rasied</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.ToD_Case_Category__c</field>
            <operation>equals</operation>
            <value>U.S. Consulting Professional – Request a compensation discussion with a leader,U.S. Tax Professional - Request a compensation discussion with a leader</value>
        </criteriaItems>
        <description>US Consulting and Tax Email notification when case is raised.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Category GMMI - Immigration</fullName>
        <actions>
            <name>Update_Case_Category_GMMI_Immigration</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>GMI - Immigration</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Category GMMI - Mobility</fullName>
        <actions>
            <name>Update_Case_Category_GMMI_Mobility</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>GMI - Mobility</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Category PSN Benifits</fullName>
        <actions>
            <name>Update_Case_Category_PSN_Benifits</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>PSN Benefits</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Origin</fullName>
        <actions>
            <name>Update_Case_Origin_to_Clicktools</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND($Profile.Name = &apos;6_Integration&apos;,RecordType.DeveloperName = &apos;Process_Issue&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Flag on Case Updated by Non-Owners</fullName>
        <actions>
            <name>SetUpdatedbyNonOwnertoTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>LastModifiedById&lt;&gt;OwnerId &amp;&amp;  NOT(ISCHANGED( WCT_Updated_by_Non_Owner__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update_Case_Category for Issue with Talent Referral Portal</fullName>
        <actions>
            <name>Update_Case_Category</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Case_Sub_Category</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.WCT_Support_Area__c</field>
            <operation>equals</operation>
            <value>INDIA,US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ToD_Case_Category__c</field>
            <operation>equals</operation>
            <value>Issue with Talent Referral Portal</value>
        </criteriaItems>
        <description>Update sub category1 to &apos;Talent Connect (Careerify)&apos;  when tod category is &apos;Issue with Talent Referral Portal&apos;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>email when TOD form category USI Tax</fullName>
        <actions>
            <name>USI_Tax_email_trigger</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.ToD_Case_Category__c</field>
            <operation>equals</operation>
            <value>USI Tax Professionals - Compensation Query</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>email when TOD form category USI consult</fullName>
        <actions>
            <name>USI_consult_email_trigger</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.ToD_Case_Category__c</field>
            <operation>equals</operation>
            <value>USI Consulting Compensation Communicatio,USI Consulting Professional – Request a compensation discussion with a leader</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Assign_new_counselor_if_applicable</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Assign new counselor (if applicable)</subject>
    </tasks>
    <tasks>
        <fullName>Auto_Liability_Form_if_applicable</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Auto Liability Form (if applicable)</subject>
    </tasks>
    <tasks>
        <fullName>Coordinate_with_Payroll_re_Paid_Time_Off_Payouts_and_Repayments</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>1-High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Coordinate with Payroll (re: Paid Time Off Payouts and Repayments)</subject>
    </tasks>
    <tasks>
        <fullName>Coordinate_with_individual_to_assist_them_in_submitting_final_expenses</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Coordinate with individual to assist them in submitting final expenses</subject>
    </tasks>
    <tasks>
        <fullName>Create_project_plan_for_implementation_including_technology_needs_engage_TTGB_as</fullName>
        <assignedToType>owner</assignedToType>
        <description>Review the Change Control Requests SOP: Implement and Socialize Change section and utilize attachment(s)</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>2. Create project plan for implementation, including technology needs (engage TTGB as needed) and considerations</subject>
    </tasks>
    <tasks>
        <fullName>Ensure_DTE_is_completed</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>1-High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Ensure DTE is completed</subject>
    </tasks>
    <tasks>
        <fullName>Ensure_Employee_has_submitted_their_time</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>1-High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Ensure Employee has submitted their time</subject>
    </tasks>
    <tasks>
        <fullName>Ensure_all_expenses_have_been_submitted</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>1-High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Ensure all expenses have been submitted</subject>
    </tasks>
    <tasks>
        <fullName>Ensure_employee_submits_ID_card</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>1-High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Ensure employee submits ID card</subject>
    </tasks>
    <tasks>
        <fullName>Ensure_employee_submits_laptop_bag_and_adapter_to_ITS</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>1-High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Ensure employee submits laptop, bag, and adapter to ITS</subject>
    </tasks>
    <tasks>
        <fullName>Enter_in_HR_Admin</fullName>
        <assignedToType>owner</assignedToType>
        <description>Enter in HR Admin</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Enter in HR Admin</subject>
    </tasks>
    <tasks>
        <fullName>Identify_project_manager_for_implementation_if_needed</fullName>
        <assignedToType>owner</assignedToType>
        <description>Review the Change Control Requests SOP: Implement and Socialize Change section</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>1. Identify project manager for implementation, if needed</subject>
    </tasks>
    <tasks>
        <fullName>Identify_project_manager_for_implementation_if_new_process_scope</fullName>
        <assignedToType>owner</assignedToType>
        <description>Review the Change Control Requests SOP: Implement and Socialize Change section</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>1. Identify project manager for implementation if new process/scope</subject>
    </tasks>
    <tasks>
        <fullName>Initiate_Involuntary_Separation_Process</fullName>
        <assignedToType>owner</assignedToType>
        <description>Initiate Involuntary Separation Process</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Initiate Involuntary Separation Process</subject>
    </tasks>
    <tasks>
        <fullName>Initiate_Voluntary_Separation_Process</fullName>
        <assignedToType>owner</assignedToType>
        <description>Initiate Voluntary Separation Process</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Initiate Voluntary Separation Process</subject>
    </tasks>
    <tasks>
        <fullName>Issue_Transfer_Memo_to_Employee_if_applicable</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Issue Transfer Memo to Employee (if applicable)</subject>
    </tasks>
    <tasks>
        <fullName>Issue_a_Transfer_Transmittal</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Issue a Transfer Transmittal</subject>
    </tasks>
    <tasks>
        <fullName>Manage_KM_items</fullName>
        <assignedToType>owner</assignedToType>
        <description>Review the Change Control Requests SOP: Implement and Socialize Change section and utilize attachment(s)</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>5. Manage KM items</subject>
    </tasks>
    <tasks>
        <fullName>Manage_KM_items_Talent_To_Business</fullName>
        <assignedToType>owner</assignedToType>
        <description>Review the Change Control Requests SOP: Implement and Socialize Change section and utilize attachment(s)</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>4. Manage KM items</subject>
    </tasks>
    <tasks>
        <fullName>Manage_business_case_items</fullName>
        <assignedToType>owner</assignedToType>
        <description>Review the Change Control Requests SOP: Implement and Socialize Change section and utilize attachment(s) if appropriate</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>6. Manage business case items</subject>
    </tasks>
    <tasks>
        <fullName>Manage_communication_items</fullName>
        <assignedToType>owner</assignedToType>
        <description>Review the Change Control Requests SOP: Implement and Socialize Change section and utilize attachment(s)</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>4. Manage communication items</subject>
    </tasks>
    <tasks>
        <fullName>Manage_communication_items_Talent_To_Business</fullName>
        <assignedToType>owner</assignedToType>
        <description>Review the Change Control Requests SOP: Implement and Socialize Change section and utilize attachment(s)</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>3. Manage communication items</subject>
    </tasks>
    <tasks>
        <fullName>Manage_logistics_items</fullName>
        <assignedToType>owner</assignedToType>
        <description>Review the Change Control Requests SOP: Implement and Socialize Change section and utilize attachment(s) if appropriate</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>9. Manage logistics items</subject>
    </tasks>
    <tasks>
        <fullName>Manage_logistics_items_if_applicable</fullName>
        <assignedToType>owner</assignedToType>
        <description>Review the Change Control Requests SOP: Implement and Socialize Change section and utilize attachment(s)</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>7. Manage logistics items, if applicable</subject>
    </tasks>
    <tasks>
        <fullName>Manage_metrics_items</fullName>
        <assignedToType>owner</assignedToType>
        <description>Review the Change Control Requests SOP: Implement and Socialize Change section and utilize attachment(s)</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>7. Manage metrics items</subject>
    </tasks>
    <tasks>
        <fullName>Manage_metrics_items_Talent_To_Business</fullName>
        <assignedToType>owner</assignedToType>
        <description>Review the Change Control Requests SOP: Implement and Socialize Change section and utilize attachment(s)</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>5. Manage metrics items</subject>
    </tasks>
    <tasks>
        <fullName>Manage_systems_technology_items</fullName>
        <assignedToType>owner</assignedToType>
        <description>Review the Change Control Requests SOP: Implement and Socialize Change section and utilize attachment(s)</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>8. Manage systems/technology items</subject>
    </tasks>
    <tasks>
        <fullName>Manage_systems_technology_items_if_applicable</fullName>
        <assignedToType>owner</assignedToType>
        <description>Review the Change Control Requests SOP: Implement and Socialize Change section and utilize attachment(s)</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>6. Manage systems/technology items, if applicable</subject>
    </tasks>
    <tasks>
        <fullName>Manage_training_items</fullName>
        <assignedToType>owner</assignedToType>
        <description>Review the Change Control Requests SOP: Implement and Socialize Change section and utilize attachment(s)</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>3. Manage training items</subject>
    </tasks>
    <tasks>
        <fullName>Manage_training_items_Talent_To_Business</fullName>
        <assignedToType>owner</assignedToType>
        <description>Review the Change Control Requests SOP: Implement and Socialize Change section and utilize attachment(s)</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>2. Manage training items</subject>
    </tasks>
    <tasks>
        <fullName>Management_Agreement_if_applicable</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Management Agreement (if applicable)</subject>
    </tasks>
    <tasks>
        <fullName>Notify_Business_Advisor</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>1-High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Notify Business Advisor (as appropriate)</subject>
    </tasks>
    <tasks>
        <fullName>Prepare_Separation_Memo</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>1-High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Prepare Separation Memo</subject>
    </tasks>
    <tasks>
        <fullName>Print_out_documentation_to_bring_to_Separation_Meeting</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Print out documentation to bring to Separation Meeting</subject>
    </tasks>
    <tasks>
        <fullName>Re_Assign_Counselees</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>1-High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Re-Assign Counselees (as appropriate)</subject>
    </tasks>
    <tasks>
        <fullName>Realign_transferring_employee_s_counselees_if_applicable</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Realign transferring employee’s counselees (if applicable)</subject>
    </tasks>
    <tasks>
        <fullName>Receive_Signed_Transfer_Memo_if_applicable</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Receive Signed Transfer Memo (if applicable)</subject>
    </tasks>
    <tasks>
        <fullName>Relocations_Agreement_if_applicable</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Relocations Agreement (if applicable)</subject>
    </tasks>
    <tasks>
        <fullName>Repayment_Agreement_if_applicable</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Repayment Agreement (if applicable)</subject>
    </tasks>
    <tasks>
        <fullName>Send_documents_via_email_to_be_acknowledged_by_the_Employee</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>1-High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send documents via email to be acknowledged by the Employee</subject>
    </tasks>
    <tasks>
        <fullName>Send_notification_to_IT_re_access_to_systems</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>1-High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send notification to IT (re: access to systems)</subject>
    </tasks>
    <tasks>
        <fullName>Send_notification_with_Expense_Compliance</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>1-High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send notification with Expense Compliance</subject>
    </tasks>
    <tasks>
        <fullName>Send_notification_with_National_Pension_Compliance</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>1-High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send notification with National Pension Compliance</subject>
    </tasks>
    <tasks>
        <fullName>Send_notification_with_Payroll_and_RCS_Compliance</fullName>
        <assignedToType>owner</assignedToType>
        <description>Validate whether employee is going to a Restricted Entity</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>1-High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send notification with Payroll and RCS Compliance</subject>
    </tasks>
    <tasks>
        <fullName>Set_up_Separation_Meeting_with_Employee_and_appropriate_parties</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>1-High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Set up Separation Meeting with Employee and appropriate parties</subject>
    </tasks>
    <tasks>
        <fullName>Set_up_Separation_Package</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>1-High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Set up Separation Package</subject>
    </tasks>
    <tasks>
        <fullName>Update_HR_applications_following_the_separation</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.WCT_Due_Date__c</offsetFromField>
        <priority>1-High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Update HR applications following the separation</subject>
    </tasks>
</Workflow>
