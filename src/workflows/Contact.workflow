<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Email_to_Interviewer</fullName>
        <description>Send Email to Interviewer</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>eInterviews_Automated_Templates/WCT_Interviewer_Portal_Login</template>
    </alerts>
    <fieldUpdates>
        <fullName>AR_Populate_AR_Eligible</fullName>
        <description>populate with false</description>
        <field>AR_Alumni_Program_Eligible__c</field>
        <literalValue>0</literalValue>
        <name>AR_Populate AR Eligible</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_Alumni_Program_Eligible</fullName>
        <field>AR_Alumni_Program_Eligible__c</field>
        <literalValue>1</literalValue>
        <name>AR_Populate Alumni Program Eligible</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_Current_Employer</fullName>
        <field>AR_Current_Employer__c</field>
        <formula>WCT_Company_Name__c</formula>
        <name>AR_Populate Current Employer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_Current_Title</fullName>
        <field>AR_Current_Title__c</field>
        <formula>WCT_New_Title__c</formula>
        <name>AR_Populate Current Title</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_Deceased_Checkbox</fullName>
        <field>AR_Deceased__c</field>
        <literalValue>1</literalValue>
        <name>AR_Populate Deceased Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_Deloitte_Email</fullName>
        <field>AR_Deloitte_Email__c</field>
        <formula>Email</formula>
        <name>AR_Populate Deloitte Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_ERS_flag</fullName>
        <field>AR_ERS_Flag__c</field>
        <literalValue>1</literalValue>
        <name>AR_Populate ERS flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_Email</fullName>
        <field>Email</field>
        <formula>IF(ISPICKVAL(AR_Preferred_Email__c,&quot;Personal Email&quot;), AR_Personal_Email__c, AR_ExternalWork_Email__c )</formula>
        <name>AR_Populate Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_Email_for_Communication</fullName>
        <field>AR_Email_for_Communication__c</field>
        <formula>IF(ISPICKVAL(AR_Preferred_Email__c,&quot;Personal Email&quot;),  AR_Personal_Email__c, AR_ExternalWork_Email__c )</formula>
        <name>AR_Populate Email for Communication</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_Email_with_personal</fullName>
        <field>Email</field>
        <formula>AR_Personal_Email__c</formula>
        <name>AR_Populate Email with personal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_Mobile_Phone</fullName>
        <field>AR_Mobile_Phone__c</field>
        <formula>MobilePhone</formula>
        <name>AR_Populate Mobile Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_Nickname_Field</fullName>
        <field>AR_Nickname__c</field>
        <formula>WCT_Preferred_Name__c</formula>
        <name>AR_Populate Nickname Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_Person_ID</fullName>
        <field>WCT_Person_Id__c</field>
        <formula>AR_Friend_ID__c</formula>
        <name>AR_Populate Person ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_Personal_Email</fullName>
        <field>AR_Personal_Email__c</field>
        <formula>WCT_ExternalEmail__c</formula>
        <name>AR_Populate Personal Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_Preferred_Email</fullName>
        <field>AR_Preferred_Email__c</field>
        <literalValue>Personal Email</literalValue>
        <name>AR_Populate Preferred Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_Primary_Address_City</fullName>
        <field>AR_Primary_Address_City__c</field>
        <formula>WCT_Home_City__c</formula>
        <name>AR_Populate Primary Address City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_Primary_Address_Country</fullName>
        <field>AR_Primary_Address_Country__c</field>
        <formula>WCT_Home_Country__c</formula>
        <name>AR_Populate Primary Address Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_Primary_Address_State</fullName>
        <field>AR_Primary_Address_State__c</field>
        <formula>WCT_Home_State__c</formula>
        <name>AR_Populate Primary Address State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_Primary_Address_Zip</fullName>
        <field>AR_Primary_Address_Zip__c</field>
        <formula>WCT_Home_Zip__c</formula>
        <name>AR_Populate Primary Address Zip</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_Receive_Comms</fullName>
        <description>Populate with ineligible for employee deaths</description>
        <field>AR_Receive_Communications__c</field>
        <literalValue>Ineligible</literalValue>
        <name>AR_Populate Receive Comms</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_email_for_Comm_default</fullName>
        <field>AR_Email_for_Communication__c</field>
        <formula>AR_Personal_Email__c</formula>
        <name>AR_Populate email for Comm (default)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_primary_city_with_unknown</fullName>
        <field>AR_Primary_Address_City__c</field>
        <formula>&quot;Unknown&quot;</formula>
        <name>AR_Populate primary city with unknown</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_primary_state_with_unknown</fullName>
        <field>AR_Primary_Address_State__c</field>
        <formula>&quot;Unknown&quot;</formula>
        <name>AR_Populate primary state with unknown</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_primary_zip_with_unknown</fullName>
        <field>AR_Primary_Address_Zip__c</field>
        <formula>&quot;Unknown&quot;</formula>
        <name>AR_Populate primary zip with unknown</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_prior_employer_Rel_2</fullName>
        <field>AR_Prior_Employer__c</field>
        <formula>priorvalue( AR_Current_employer_HIDDEN__c )</formula>
        <name>Populate prior employer Rel 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Populate_title_with_unknown</fullName>
        <field>AR_Current_Title__c</field>
        <formula>&quot;Unknown&quot;</formula>
        <name>AR_Populate title with unknown</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Receive_comms_inelig</fullName>
        <field>AR_Receive_Communications__c</field>
        <literalValue>Ineligible</literalValue>
        <name>AR_Receive comms inelig</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Update_Prior_Employer</fullName>
        <field>AR_Prior_Employer__c</field>
        <formula>PRIORVALUE(  AR_Current_Employer__c )</formula>
        <name>AR_Update Prior Employer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Update_Prior_Industry</fullName>
        <field>AR_Prior_Industry__c</field>
        <formula>TEXT(PRIORVALUE( AR_Current_Industry__c ))</formula>
        <name>AR_Update Prior Industry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Update_Prior_Title</fullName>
        <field>AR_Prior_Title__c</field>
        <formula>PRIORVALUE( AR_Current_Title__c )</formula>
        <name>AR_Update Prior Title</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Update_Receive_Comms_to_Eligible</fullName>
        <field>AR_Receive_Communications__c</field>
        <literalValue>Eligible</literalValue>
        <name>AR_Update Receive Comms to Eligible</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Update_Receive_Communications</fullName>
        <field>AR_Receive_Communications__c</field>
        <literalValue>Unsubscribe</literalValue>
        <name>AR_Update Receive Communications</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Update_receive_comms_to_inelig_for_de</fullName>
        <field>AR_Receive_Communications__c</field>
        <literalValue>Ineligible</literalValue>
        <name>AR_Update receive comms to inelig for de</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_Update_receive_comms_with_email_opt_o</fullName>
        <field>AR_Receive_Communications__c</field>
        <literalValue>Unsubscribe</literalValue>
        <name>AR_Update receive comms with email opt o</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AR_alumni_elig_to_false</fullName>
        <field>AR_Alumni_Program_Eligible__c</field>
        <literalValue>0</literalValue>
        <name>AR_alumni elig to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Owner_Update_Talent_Integratio</fullName>
        <field>OwnerId</field>
        <lookupValue>talentintegration@deloitte.com.wct.prd</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Contact Owner Update - Talent Integratio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Update_Candidate_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Candidate</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Contact: Update Candidate Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Update_Dependent_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Dependant</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Contact: Update Dependent Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Update_Emergency_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Emergency_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Contact: Update Emergency Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Update_Record_Type</fullName>
        <description>Updates the Contact Record Type to Employee</description>
        <field>RecordTypeId</field>
        <lookupValue>WCT_Employee</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Contact: Update Employee Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Last_name_shadow_update</fullName>
        <field>Last_Name_shadow__c</field>
        <formula>LastName</formula>
        <name>Last name shadow update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_employer_with_unknown</fullName>
        <field>AR_Current_Employer__c</field>
        <formula>&quot;Unknown&quot;</formula>
        <name>Populate employer with unknown</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateTxtCurrentEmployeForAdhocCandidate</fullName>
        <field>Current_Employer__c</field>
        <formula>WCT_Current_Employer_lkp__r.WCT_Record_Name__c</formula>
        <name>UpdateTxtCurrentEmployeForAdhocCandidate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateTxtSchoolNameForAdhocCandidate</fullName>
        <field>WCT_Candidate_School__c</field>
        <formula>WCT_School_Education_Institution_lkp__r.WCT_Record_Name__c</formula>
        <name>UpdateTxtSchoolNameForAdhocCandidate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Attributes_Modified_Date</fullName>
        <field>AR_Last_Attributes_Modified_Date__c</field>
        <formula>now()</formula>
        <name>Update Attributes Modified Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Record_Type_to_PDAT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>PDAT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Contact Record Type to PDAT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Function_field_to_Function_shadow</fullName>
        <field>Function_Shadow__c</field>
        <formula>WCT_Function__c</formula>
        <name>Update Function field to Function shadow</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_name_Shadow</fullName>
        <field>Last_Name_shadow__c</field>
        <formula>LastName</formula>
        <name>Update Last name Shadow field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Merge_Comment_track_with_Merge_Co</fullName>
        <description>This field update action, update the field from</description>
        <field>Merge_Comment_Track__c</field>
        <formula>Merge_Comment__c</formula>
        <name>Update Merge Comment track with Merge Co</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Prior_Industry</fullName>
        <description>updates prior industry field with the value from the current industry</description>
        <field>AR_Prior_Industry__c</field>
        <formula>PRIORVALUE( AR_Current_Employer_Industry__c )</formula>
        <name>Update Prior Industry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AR_Populate Alumni Program Eligible</fullName>
        <actions>
            <name>AR_Populate_Alumni_Program_Eligible</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>AR_Update_Receive_Comms_to_Eligible</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>Contact.WCT_Alumni_Access__c</field>
            <operation>equals</operation>
            <value>Y</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Friend</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.AR_Deceased__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate Alumni Program Eligible No</fullName>
        <actions>
            <name>AR_Receive_comms_inelig</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>AR_alumni_elig_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Contact.WCT_Alumni_Access__c</field>
            <operation>equals</operation>
            <value>N</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Friend</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate Current Employer</fullName>
        <actions>
            <name>AR_Populate_Current_Employer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Contact.WCT_Company_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.AR_Current_Employer__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Friend</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate Current Title</fullName>
        <actions>
            <name>AR_Populate_Current_Title</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Contact.AR_Current_Title__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_New_Title__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Friend</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate Deceased Checkbox</fullName>
        <actions>
            <name>AR_Populate_AR_Eligible</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>AR_Populate_Deceased_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>AR_Populate_Receive_Comms</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 4) AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Contact.WCT_Reason_for_Action__c</field>
            <operation>equals</operation>
            <value>Employee Death</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Friend</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Action_Type__c</field>
            <operation>equals</operation>
            <value>Death</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate Deloitte Email</fullName>
        <actions>
            <name>AR_Populate_Deloitte_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.AR_Deloitte_Email__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Employee_Group__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate ERS flag</fullName>
        <actions>
            <name>AR_Populate_ERS_flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.AR_ERS_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Function__c</field>
            <operation>equals</operation>
            <value>AERS Advisory</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate Email field</fullName>
        <actions>
            <name>AR_Populate_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.WCT_Type__c</field>
            <operation>equals</operation>
            <value>Separated,Retiree/pensioner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.AR_Email_for_Communication__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Populate email field with email for communcation for separated employees for alumni relations team; deloitte email is stored in the Deloitte Email field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate Email for Communication</fullName>
        <actions>
            <name>AR_Populate_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>AR_Populate_Email_for_Communication</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Contact.AR_Preferred_Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Type__c</field>
            <operation>equals</operation>
            <value>Retiree/pensioner,separated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Friend</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate Email for Communication%28default%29</fullName>
        <actions>
            <name>AR_Populate_Email_with_personal</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>AR_Populate_email_for_Comm_default</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Contact.AR_Preferred_Email__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Type__c</field>
            <operation>equals</operation>
            <value>separated,Retiree/pensioner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Friend</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate Mobile Phone</fullName>
        <actions>
            <name>AR_Populate_Mobile_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.AR_Mobile_Phone__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MobilePhone</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate Nickname Field</fullName>
        <actions>
            <name>AR_Populate_Nickname_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISBLANK( AR_Nickname__c ),NOT(ISBLANK( WCT_Preferred_Name__c )),OR(ISPICKVAL( WCT_Contact_Type__c  ,&quot;Friend&quot;),ISPICKVAL(WCT_Contact_Type__c ,&quot;Employee&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate Person ID for friends</fullName>
        <actions>
            <name>AR_Populate_Person_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISBLANK( WCT_Person_Id__c ), ISPICKVAL(WCT_Contact_Type__c, &quot;Friend&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate Personal Email</fullName>
        <actions>
            <name>AR_Populate_Personal_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Contact.WCT_ExternalEmail__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.AR_Personal_Email__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Friend</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate Primary Address City</fullName>
        <actions>
            <name>AR_Populate_Primary_Address_City</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Contact.AR_Primary_Address_City__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Home_City__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Friend</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate Primary Address Country</fullName>
        <actions>
            <name>AR_Populate_Primary_Address_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Contact.AR_Primary_Address_Country__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Home_Country__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Friend</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate Primary Address State</fullName>
        <actions>
            <name>AR_Populate_Primary_Address_State</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Contact.AR_Primary_Address_State__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Home_State__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Friend</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate Primary Address Zip</fullName>
        <actions>
            <name>AR_Populate_Primary_Address_Zip</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Contact.AR_Primary_Address_Zip__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Home_Zip__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Friend</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate Receive Communications</fullName>
        <actions>
            <name>AR_Update_Receive_Communications</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Contact.HasOptedOutOfEmail</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Friend</value>
        </criteriaItems>
        <description>Populate with Unsubscribe when Email opt out = TRUE</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate employer with unknown</fullName>
        <actions>
            <name>Populate_employer_with_unknown</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.WCT_Type__c</field>
            <operation>equals</operation>
            <value>Separated,Retiree/pensioner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Current_Employer__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.AR_Current_Employer__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Populates Current Employer field with &quot;Unknown&quot; when WCT current employer field is blank</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate primary city with unknown</fullName>
        <actions>
            <name>AR_Populate_primary_city_with_unknown</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.WCT_Home_City__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.AR_Primary_Address_City__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Type__c</field>
            <operation>equals</operation>
            <value>separated,Retiree/pensioner</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate primary state with unknown</fullName>
        <actions>
            <name>AR_Populate_primary_state_with_unknown</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.WCT_Home_State__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.AR_Primary_Address_State__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Type__c</field>
            <operation>equals</operation>
            <value>separated,Retiree/pensioner</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate primary zip with unknown</fullName>
        <actions>
            <name>AR_Populate_primary_zip_with_unknown</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.WCT_Home_Zip__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.AR_Primary_Address_Zip__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Type__c</field>
            <operation>equals</operation>
            <value>separated,Retiree/pensioner</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Populate title with unknown</fullName>
        <actions>
            <name>AR_Populate_title_with_unknown</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.WCT_New_Title__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.AR_Current_Title__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Type__c</field>
            <operation>equals</operation>
            <value>separated,Retiree/pensioner</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Update Preferred Email</fullName>
        <actions>
            <name>AR_Populate_Preferred_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.AR_Preferred_Email__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Employee,Friend</value>
        </criteriaItems>
        <description>set default to Personal Email when blank</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AR_Update Prior Industry</fullName>
        <actions>
            <name>Update_Prior_Industry</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates prior industry field with the current industry when the current industry changes</description>
        <formula>AND(ISCHANGED( AR_Current_Employer_Industry__c ), NOT(ISBLANK(PRIORVALUE( AR_Current_Employer_Industry__c ))),OR(ISPICKVAL( WCT_Contact_Type__c  ,&quot;Friend&quot;), ISPICKVAL(WCT_Contact_Type__c ,&quot;Employee&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>AR_Update Prior Title</fullName>
        <actions>
            <name>AR_Update_Prior_Title</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates prior title field with the current title when the current title changes</description>
        <formula>AND(ISCHANGED(AR_Current_Title__c ), NOT(ISBLANK(PRIORVALUE(AR_Current_Title__c ))),
(PRIORVALUE(AR_Current_Title__c) &lt;&gt; &quot;UNKNOWN&quot;),
OR(ISPICKVAL( WCT_Contact_Type__c  ,&quot;Friend&quot;), ISPICKVAL(WCT_Contact_Type__c ,&quot;Employee&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>AR_Update receive comms to inelig for deceased</fullName>
        <actions>
            <name>AR_Update_receive_comms_to_inelig_for_de</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.AR_Deceased__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.AR_Receive_Communications__c</field>
            <operation>notEqual</operation>
            <value>Ineligible</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>AR_Update_Prior Employer</fullName>
        <actions>
            <name>AR_Update_Prior_Employer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(ISCHANGED(AR_Current_Employer__c), NOT(ISBLANK(PRIORVALUE(AR_Current_Employer__c))), OR(ISPICKVAL( WCT_Contact_Type__c  ,&quot;Friend&quot;), ISPICKVAL(WCT_Contact_Type__c ,&quot;Employee&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>AR_Update_Prior EmployerR2</fullName>
        <actions>
            <name>AR_Populate_prior_employer_Rel_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>created new updated rule for release 2 using company lookup field instead of the free text field to populate prior employer; also logic built in to avoid using &quot;Not in List&quot; value</description>
        <formula>AND( ISCHANGED(AR_Current_Company__c),  NOT(ISBLANK(PRIORVALUE(AR_Current_Company__c))), (PRIORVALUE(AR_Current_Company__c) &lt;&gt; &quot;Not in List&quot; &amp;&amp; PRIORVALUE(AR_Current_Company__c) &lt;&gt; &quot;UNKNOWN&quot;),  OR( ISPICKVAL( WCT_Contact_Type__c  ,&quot;Friend&quot;), ISPICKVAL(WCT_Contact_Type__c ,&quot;Employee&quot;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Set Candidate Record Type</fullName>
        <actions>
            <name>Contact_Update_Candidate_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Candidate</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Set Dependent Record Type</fullName>
        <actions>
            <name>Contact_Update_Dependent_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Dependent</value>
        </criteriaItems>
        <description>Set the Dependent Record Type when initially Created using Dataload</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Set Emergency Record Type</fullName>
        <actions>
            <name>Contact_Update_Emergency_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Emergency</value>
        </criteriaItems>
        <description>Set the Emergency Record Type when initially Created using Dataload</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Set Employee Record Type</fullName>
        <actions>
            <name>Contact_Update_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <description>Set the Employee Record Type when initially Created using Dataload</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3AUpdateAdhocCandidateCurrentEmployerWithRecord Name</fullName>
        <actions>
            <name>UpdateTxtCurrentEmployeForAdhocCandidate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>For Ad Hoc Candidates, &quot;Record Name&quot; (Text) needs to have the &quot;shortened&quot; values mapped to the longer values so that reporting can function correctly</description>
        <formula>RecordTypeId = $Label.AdhocCandidateRecordtypeID  &amp;&amp;   (ISCHANGED( WCT_Current_Employer_lkp__c )|| ISNEW() )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3AUpdateAdhocCandidateSchoolNameWithRecord Name</fullName>
        <actions>
            <name>UpdateTxtSchoolNameForAdhocCandidate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>For Ad Hoc Candidates, &quot;School Name&quot; (Text) needs to have the &quot;shortened&quot; values mapped to the longer values so that reporting can function correctly</description>
        <formula>($Profile.Name &lt;&gt; &apos;6_Integration&apos; &amp;&amp; $Profile.Name &lt;&gt; &apos;Event_Profile&apos;) &amp;&amp; RecordTypeId = $Label.AdhocCandidateRecordtypeID  &amp;&amp;   (ISCHANGED(WCT_School_Education_Institution_lkp__c)|| ISNEW() )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Handle Org 1 Contact Records</fullName>
        <actions>
            <name>Update_Contact_Record_Type_to_PDAT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.CER_Org_1_Id__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send Email to Interviewer</fullName>
        <actions>
            <name>Send_Email_to_Interviewer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Interviewer__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Function field to Function shadow</fullName>
        <actions>
            <name>Update_Function_field_to_Function_shadow</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.WCT_Type__c</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Function__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <description>When ever there is a change in function field it will update function shadow field. Alumni team requested for reporting</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Last Attributes Modified Date</fullName>
        <actions>
            <name>Update_Attributes_Modified_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Evaluate the rule when a record is created, and every time it’s edited</description>
        <formula>OR(ISCHANGED( FirstName ), ISCHANGED( LastName ), ISCHANGED( AR_Current_Title__c ),ISCHANGED(AR_Current_Company__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Last name field to Last name shadow</fullName>
        <actions>
            <name>Last_name_shadow_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.WCT_Type__c</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.LastName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.WCT_Contact_Type__c</field>
            <operation>equals</operation>
            <value>Employee</value>
        </criteriaItems>
        <description>When ever there is a change in last name field it will update last name shadow field. Alumni team requested for reporting</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Merge Comment track with Merge Comment</fullName>
        <actions>
            <name>Update_Merge_Comment_track_with_Merge_Co</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Merge_Comment__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Updated Merged Ad Hoc Candidate Owner To Talent Integration</fullName>
        <actions>
            <name>Contact_Owner_Update_Talent_Integratio</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This is required as the owner should not see the merged candidate</description>
        <formula>NOT(ISNEW()) &amp;&amp; Contains(Email,&apos;merged&apos;) &amp;&amp; WCT_Related_Contact_Status__c = &apos;Merged&apos; &amp;&amp; WCT_Type__c = &apos;Candidate Ad Hoc&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
