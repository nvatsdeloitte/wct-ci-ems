<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Check_Is_Email_on_Case</fullName>
        <field>WCT_isEmailonCase__c</field>
        <literalValue>1</literalValue>
        <name>Check Is Email on Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EM_Update_First_Response_Time</fullName>
        <field>WCT_First_Response_Sent_Date__c</field>
        <formula>NOW()</formula>
        <name>EM: Update First Response Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EM_Update_First_Response_checkbox</fullName>
        <field>WCT_First_Response_Sent__c</field>
        <literalValue>1</literalValue>
        <name>EM: Update First Response checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unread_Email_on_Closed_Case</fullName>
        <field>Unread_Email_on_Closed_Case__c</field>
        <literalValue>1</literalValue>
        <name>Unread Email on Closed Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Case Unread Email on Closed Case</fullName>
        <actions>
            <name>Unread_Email_on_Closed_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When an email comes into a closed case, this workflow, reopens the case and populates the &quot;Unread Email on Closed Case?&quot; field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EM%3A Update First response time</fullName>
        <actions>
            <name>EM_Update_First_Response_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>EM_Update_First_Response_checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>CONTAINS(ToAddress, Parent.WCT_ReportedBy__r.Email) &amp;&amp; NOT(Incoming) &amp;&amp; NOT(Parent.WCT_First_Response_Sent__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EM%3AUpdate status of Case checkbox</fullName>
        <actions>
            <name>Check_Is_Email_on_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Unread_Email_on_Closed_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Incoming &amp;&amp; NOT( ISBLANK(ParentId ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
