<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_alert_with_Exception_log_from_a_batch_class</fullName>
        <description>Email alert with Exception log from a batch class</description>
        <protected>false</protected>
        <recipients>
            <field>Running_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CIC_Email_Template/Email_with_exception_data</template>
    </alerts>
    <rules>
        <fullName>WCT Exception Log to login user</fullName>
        <actions>
            <name>Email_alert_with_Exception_log_from_a_batch_class</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Running_User__c &lt;&gt; NULL</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
