<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Exit_Interview_Intimation</fullName>
        <description>Exit Interview Intimation</description>
        <protected>false</protected>
        <recipients>
            <field>ELE_Assigned_To__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>useleseparations@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Voluntary_Separation/Exit_Interview_Intimation</template>
    </alerts>
    <rules>
        <fullName>ELE Survey Intimation</fullName>
        <actions>
            <name>Exit_Interview_Intimation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ELE_Assigned_To__c != &apos;&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
