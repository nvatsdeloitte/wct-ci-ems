<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>WF_Trigger_Notice</fullName>
        <description>WF Trigger Notice</description>
        <protected>false</protected>
        <recipients>
            <recipient>ryeturi@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Requirement_Hours_WF_Notice</template>
    </alerts>
    <fieldUpdates>
        <fullName>BME_Check_Scope_Item</fullName>
        <field>Scope_Item__c</field>
        <literalValue>1</literalValue>
        <name>BME: Check Scope Item</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BME_Uncheck_Enhancement</fullName>
        <field>Enhancement__c</field>
        <literalValue>0</literalValue>
        <name>BME: Uncheck Enhancement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BME_Uncheck_Scope_Item</fullName>
        <field>Scope_Item__c</field>
        <literalValue>0</literalValue>
        <name>BME: Uncheck Scope Item</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BME_Update_Enhancement_Checkbox</fullName>
        <field>Enhancement__c</field>
        <literalValue>1</literalValue>
        <name>BME: Update Enhancement Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Requirement_Pct_Hrs_Completed</fullName>
        <field>Percent_Complete__c</field>
        <formula>IF(Total_Hours__c &gt;0,
    (Requirement_Hours_Completed__c+
     Biz_Process_Design_Hours_Completed__c+
     Technical_Design_Hours_Completed__c+
     Configuration_Hours_Completed__c+
     Development_Hours_Completed__c+
     QA_Hours_Completed__c+
     Training_Hours_Completed__c)/Total_Hours__c,0)</formula>
        <name>Requirement Pct Hrs Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Percent_Hours_Complete</fullName>
        <field>Percent_Complete__c</field>
        <formula>IF( Requirements_Hours__c  +


  Biz_Process_Design_Hours__c +


  Technical_Design_Hours__c +


  Configuration_Hours__c +


  Development_Hours__c &gt; 0,(
  (Requirements_Hours__c * Requirement_Hours_Percent_Complete__c) +


  (Biz_Process_Design_Hours__c * Biz_Process_Design_Hrs_Percent_Complete__c) +


  (Technical_Design_Hours__c * Technical_Design_Hours_Percent_Complete__c) +


  (Configuration_Hours__c * Configuration_Hrs_Percent_Complete__c) +


  (Development_Hours__c * Development_Hrs_Percent_Complete__c))/
(
  Requirements_Hours__c  +


  Biz_Process_Design_Hours__c +


  Technical_Design_Hours__c +


  Configuration_Hours__c +


  Development_Hours__c),0)</formula>
        <name>Update Percent Hours Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BME%3A Update Percent Hours Completed</fullName>
        <actions>
            <name>Update_Percent_Hours_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( ISCHANGED(Requirements_Hours__c ) ,
    ISCHANGED( Requirement_Hours_Percent_Complete__c ) ,
    ISCHANGED( Biz_Process_Design_Hours__c ) ,
    ISCHANGED( Biz_Process_Design_Hrs_Percent_Complete__c ) ,
    ISCHANGED( Technical_Design_Hours__c ) ,
    ISCHANGED( Technical_Design_Hours_Percent_Complete__c ) ,
    ISCHANGED( Configuration_Hours__c ) ,
    ISCHANGED( Configuration_Hrs_Percent_Complete__c ) ,
    ISCHANGED( Development_Hours__c ) ,
    ISCHANGED( Development_Hrs_Percent_Complete__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BME%3AUncheck Enhancement and Scope Item for Req</fullName>
        <actions>
            <name>BME_Uncheck_Enhancement</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BME_Uncheck_Scope_Item</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Requirement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Requirement</value>
        </criteriaItems>
        <criteriaItems>
            <field>Requirement__c.Enhancement__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Requirement__c.Scope_Item__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BME%3AUpdate Enhancement Checkbox</fullName>
        <actions>
            <name>BME_Uncheck_Scope_Item</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BME_Update_Enhancement_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Requirement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Enhancement</value>
        </criteriaItems>
        <criteriaItems>
            <field>Requirement__c.Enhancement__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Update Enhancement Checkbox</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BME%3AUpdate Scope Item Checkbox</fullName>
        <actions>
            <name>BME_Check_Scope_Item</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BME_Uncheck_Enhancement</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Requirement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Scope Item</value>
        </criteriaItems>
        <criteriaItems>
            <field>Requirement__c.Scope_Item__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Update Scope Item Checkbox</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Total Requirements Completed</fullName>
        <actions>
            <name>Requirement_Pct_Hrs_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Calculate the total % Completed hours for the Requirement.</description>
        <formula>OR(ISCHANGED( Requirements_Hours__c ),
   ISCHANGED(  Requirement_Hours_Completed__c  ),
   ISCHANGED( Biz_Process_Design_Hours__c  ),
   ISCHANGED( Biz_Process_Design_Hours_Completed__c  ),
   ISCHANGED(    Technical_Design_Hours__c ),
   ISCHANGED( Technical_Design_Hours_Completed__c  ),
   ISCHANGED( Configuration_Hours__c  ),
   ISCHANGED( Configuration_Hours_Completed__c  ),
   ISCHANGED( Development_Hours__c ),
   ISCHANGED( Development_Hours_Completed__c ),
   ISCHANGED( QA_Hours__c ),
   ISCHANGED( QA_Hours_Completed__c ),
   ISCHANGED( Training_Hours__c ),
   ISCHANGED( Training_Hours_Completed__c  ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
