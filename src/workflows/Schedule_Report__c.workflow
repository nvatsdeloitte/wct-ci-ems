<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SR_send_report_attachment</fullName>
        <ccEmails>schedule_report@14bv8efv4p58n579olgefnmebyh8mwqjozw0rs4zomuvv5toi0.4-mxfdea0.na2.apex.salesforce.com</ccEmails>
        <description>SR send report attachment</description>
        <protected>false</protected>
        <recipients>
            <recipient>gmadurai@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Report_Sent_to__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Schedule_Report_Template_Folder/MyReportAttachment_1</template>
    </alerts>
    <rules>
        <fullName>SR Send Report as attachment</fullName>
        <actions>
            <name>SR_send_report_attachment</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Schedule_Report__c.ReportTrigger__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
