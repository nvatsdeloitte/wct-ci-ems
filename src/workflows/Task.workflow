<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Task_Update_closed_Date</fullName>
        <description>Update closed Date with current time</description>
        <field>WCT_Closed_Date__c</field>
        <formula>NOW()</formula>
        <name>Task: Update closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Prority_High</fullName>
        <field>Priority</field>
        <literalValue>1-High</literalValue>
        <name>Update Prority -High</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>stampmobilitytaskcloseddatefortask</fullName>
        <field>WCT_GMI_Closed_Task_Notification_Date__c</field>
        <formula>Today()</formula>
        <name>Stamp Mobility Task Closed Date For Task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Task Priority Update for Pre-Bi</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>contains</operation>
            <value>India Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pre-BI</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>notEqual</operation>
            <value>Completed,Cancelled</value>
        </criteriaItems>
        <description>IF Task.Owner = India Support Team AND Status &lt;&gt; Completed, Canceled AND Task.Due Date &lt; Today THEN
UPDATE Task
SET Task.Priority = “1-High”</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Prority_High</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Task.ActivityDate</offsetFromField>
            <timeLength>-24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Task%3A Populate Completed Time</fullName>
        <actions>
            <name>Task_Update_closed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed,Deferred,Canceled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>Immigration,LCA,Mobility</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
