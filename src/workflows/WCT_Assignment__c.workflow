<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>sendemailtorecordownerwhenassignmentenddateischanged</fullName>
        <description>Send email to record owner when Assignment End Date is changed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mobility/WCT_Assignment_End_Date_Change_Notification</template>
    </alerts>
    <rules>
        <fullName>Send email to record owner when Assignment End Date is changed</fullName>
        <actions>
            <name>sendemailtorecordownerwhenassignmentenddateischanged</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(WCT_End_Date__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
