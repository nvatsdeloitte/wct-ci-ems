<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>RemindAdhocCandidateToApplyThroughTaleo</fullName>
        <description>RemindAdhocCandidateToApplyThroughTaleo</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>eInterviews_Automated_Templates/WCT_Reminding_a_candidate_to_apply_through_Taleo</template>
    </alerts>
    <fieldUpdates>
        <fullName>CT_UpdateCandidateTrackerCanelledToTrue</fullName>
        <description>UpdateCandidateTrackerCanelledToTrue</description>
        <field>WCT_CandidateTrackerCanceled__c</field>
        <literalValue>1</literalValue>
        <name>CT:UpdateCandidateTrackerCanelledToTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CT_Update_Current_Emplyr_from_Cand</fullName>
        <field>WCT_CR_Current_Employer__c</field>
        <formula>WCT_Contact__r.Current_Employer__c</formula>
        <name>CT:Update Current Emplyr from Cand</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CT_Update_Other_Emplyr_from_Cand</fullName>
        <field>WCT_CR_Other_Employer__c</field>
        <formula>WCT_Contact__r.WCT_CN_Other_Employer__c</formula>
        <name>CT:Update Other Emplyr from Cand</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CT_Update_Other_Institution_from_Cand</fullName>
        <field>WCT_CR_Other_Institution__c</field>
        <formula>WCT_Contact__r.WCT_CN_Other_Institution__c</formula>
        <name>CT:Update Other Institution from Cand</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CT_Update_RMS_Requisition</fullName>
        <field>WCT_RMS_Requisition__c</field>
        <formula>WCT_Contact__r.WCT_Taleo_Id__c +&apos;_&apos;+ WCT_Requisition__r.Name</formula>
        <name>CT:Update RMS_Requisition</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CT_Update_School_Name_from_Candidate</fullName>
        <field>WCT_CR_School__c</field>
        <formula>WCT_Contact__r.WCT_Candidate_School__c</formula>
        <name>CT:Update School Name from Candidate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CT_Update_Time_Taken_to_Start_first_Intv</fullName>
        <field>WCT_Time_till_start_of_first_Interview__c</field>
        <formula>WCT_Related_Interview_Tracker__r.WCT_Start_Date_Time__c</formula>
        <name>CT:Update Time Taken to Start first Intv</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CT_Update_Time_to_schedule_first_Intv</fullName>
        <field>WCT_Time_to_Schedule_First_Interview__c</field>
        <formula>WCT_Related_Interview_Tracker__r.CreatedDate</formula>
        <name>CT:Update Time to schedule first Intv</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CandidateReq_CandidateReq_to_Completed</fullName>
        <description>Updated Candidate Requisition Status to &quot;Completed&quot;</description>
        <field>WCT_Candidate_Requisition_Status__c</field>
        <literalValue>Completed</literalValue>
        <name>CandidateReq: CandidateReq to Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateAquisitionStatusToCanceled</fullName>
        <description>UpdateAquisitionStatusToCanceled</description>
        <field>WCT_Candidate_Requisition_Status__c</field>
        <literalValue>Canceled</literalValue>
        <name>UpdateAquisitionStatusToCanceled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_AcquisitionStatusChangedTrue</fullName>
        <field>WCT_Acquisition_Status_Changed__c</field>
        <literalValue>1</literalValue>
        <name>Update 	 AcquisitionStatusChangedTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_IntvMilestoneWithdrewAcquiStatCancel</fullName>
        <description>When Interview Milestone is set to &quot;Withdrew&quot; (in any capacity), the Acquisition Status will move to canceled</description>
        <field>WCT_Candidate_Requisition_Status__c</field>
        <literalValue>Canceled</literalValue>
        <name>Interview Milestone set Withdrew then Ac</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CT%3AUpdate Interview Times from IT</fullName>
        <actions>
            <name>CT_Update_Time_Taken_to_Start_first_Intv</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CT_Update_Time_to_schedule_first_Intv</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT( ISNULL( WCT_Related_Interview_Tracker__c ) ) &amp;&amp; NOT( ISNULL(  WCT_Related_Interview_Tracker__r.WCT_Start_Date_Time__c  ) )&amp;&amp; ISNULL( WCT_Time_till_start_of_first_Interview__c ) &amp;&amp; ISNULL( WCT_Time_to_Schedule_First_Interview__c )&amp;&amp;  DATEVALUE(WCT_Related_Interview_Tracker__r.CreatedDate)  &gt;  DATE(2014,4,13)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CandTrack%3APopulate CT fields from Candidate</fullName>
        <actions>
            <name>CT_Update_Current_Emplyr_from_Cand</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CT_Update_Other_Emplyr_from_Cand</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CT_Update_Other_Institution_from_Cand</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CT_Update_School_Name_from_Candidate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate CT fields from Adhoc Candidate on creation only</description>
        <formula>WCT_Contact__r.RecordType.DeveloperName = &apos;Candidate_Ad_Hoc&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CandTrack%3APopulate RMS Requisition</fullName>
        <actions>
            <name>CT_Update_RMS_Requisition</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(AND(WCT_Contact__r.RecordType.DeveloperName = &apos;Candidate&apos;, ISNEW() ) || AND(WCT_Contact__r.RecordType.DeveloperName = &apos;Candidate&apos;,  ISCHANGED( WCT_Requisition__c )  ) )&amp;&amp;NOT($Profile.Name = &apos;6_Integration&apos;)&amp;&amp; ISBLANK( WCT_RMS_Requisition__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CandidateTracker%3A %27CandidateTrackerStatus%27ToComplete based on Interview Milestone</fullName>
        <actions>
            <name>CandidateReq_CandidateReq_to_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Candidate_Requisition__c.WCT_Interview_Status__c</field>
            <operation>equals</operation>
            <value>Interview Process Completed – Offer</value>
        </criteriaItems>
        <description>When the &quot;Interview Milestone&quot; is changed to &quot;Interview Process Completed – Offer&quot; or &quot;Rejected&quot;, the &quot;Candidate Tracker Status&quot; should change into &quot;Completed&quot;. The rejected status is removed as per the UAT Defect:D-00328.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CandidateTracker%3ARemind Adhoc Candidate on Interview Scheduled</fullName>
        <actions>
            <name>RemindAdhocCandidateToApplyThroughTaleo</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Remind Adhoc Candidate to apply through Taleo on Interview Scheduled .</description>
        <formula>(ISCHANGED(WCT_Interview_Status__c)  &amp;&amp;  WCT_Contact__r.RecordTypeId = $Label.AdhocCandidateRecordtypeID  &amp;&amp;  CONTAINS( WCT_MilestoneStatusText__c ,&apos;Scheduled&apos;)  &amp;&amp;  NOT(CONTAINS( WCT_MilestoneStatusText__c ,&apos;To be&apos;))  )  ||  ( ISNEW() &amp;&amp; WCT_Contact__r.RecordTypeId = $Label.AdhocCandidateRecordtypeID )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CandidateTracker%3AUpdate AquisitionStatusCheckboxOnInterviewMilestoneChangeOnNonAdhocCandidates</fullName>
        <actions>
            <name>Update_AcquisitionStatusChangedTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update AquisitionStatusCheckbox On change of Interview Milestone Change of Non AdHoc Candidates</description>
        <formula>ISCHANGED(WCT_Interview_Status__c) &amp;&amp; WCT_Contact__r.RecordTypeId  &lt;&gt;$Label.AdhocCandidateRecordtypeID</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CandidateTracker%3AUpdate AquisitionStatusTo Canceled when InterviewviewMilestone is Withdrew</fullName>
        <actions>
            <name>UpdateAquisitionStatusToCanceled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Candidate_Requisition__c.WCT_Interview_Status__c</field>
            <operation>contains</operation>
            <value>Withdrew</value>
        </criteriaItems>
        <description>When Interview Milestone is set to &quot;Withdrew&quot; (in any capacity), the Tracker Status will move to canceled</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CandidateTracker%3AUpdate CandidateTrackerCanceledtoTrueOnCancel</fullName>
        <actions>
            <name>CT_UpdateCandidateTrackerCanelledToTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Candidate_Requisition__c.WCT_Candidate_Requisition_Status__c</field>
            <operation>equals</operation>
            <value>Canceled</value>
        </criteriaItems>
        <description>Update CandidateTrackerCanceled  to &quot;True&quot; On Cancel Status of Candidate Tracker. This checkbox is used in the VF page to show the popup.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
