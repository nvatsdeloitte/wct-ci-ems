<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_a_notice_of_Visa_Expired_upon_reaching_expiration_Date</fullName>
        <description>Email a notice of “Visa Expired” upon reaching expiration Date</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Assignment_Owner__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>WCT_Immigration/WCT_Immigration_Visa_Category_Petition_Expired_New</template>
    </alerts>
    <alerts>
        <fullName>Email_notification_to_collect_petition</fullName>
        <description>Email notification to collect petition</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Assignment_Owner__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>WCT_Immigration/WCT_Collection_of_Visa_Petition1</template>
    </alerts>
    <alerts>
        <fullName>Email_notification_to_employee_of_Petition_Denial</fullName>
        <description>Email notification to employee of Petition Denial</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Assignment_Owner__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_Resource_Manager__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>WCT_Immigration/Immigration_Petition_Denied</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_Employee_and_RM_on_H1B_amendment_petition_filing_with_USCIS</fullName>
        <description>Notification to Employee and RM on H1B amendment petition filing with USCIS</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Assignment_Owner__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_Resource_Manager__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>WCT_Immigration/Notice_of_Petition_Filing_with_the_USCIS</template>
    </alerts>
    <alerts>
        <fullName>Visa_Logistics_Information_received_from_Employee</fullName>
        <description>Visa Logistics Information received from Employee</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>WCT_Immigration/Visa_Logistics_Information_received_from_Employee</template>
    </alerts>
    <alerts>
        <fullName>businessleadreceivesnotificationthatthepetitionforavisaapplicationisapprove</fullName>
        <description>Business Lead receives notification that the Petition for a Visa Application is approved</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Assignment_Owner__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_Project_Manager__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_Resource_Manager__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>WCT_Immigration/WCT_Immigration_Petition_Approved1</template>
    </alerts>
    <alerts>
        <fullName>emailanoticeofpetitionexpireduponreachingexpirationdate</fullName>
        <description>Email a notice of “Petition Expired” upon reaching expiration Date</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Assignment_Owner__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>WCT_Immigration/WCT_Immigration_Visa_Category_Petition_Expired1</template>
    </alerts>
    <alerts>
        <fullName>employee_declines_3_consecutive_Q_A_Call_invites</fullName>
        <description>Ability to automatically trigger an escalated notification to the Business Lead/RM when employee declines 3 consecutive Q&amp;A Call invites</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Assignment_Owner__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_Resource_Manager__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>eEvent_Templates/Escalated_notification_to_the_Business_Lead_RM_When_unattended_Immgigration_Even</template>
    </alerts>
    <alerts>
        <fullName>notifyfragomenbusinessleadandrmofpetitionwithdrawalwhenapetitioniswithdrawn</fullName>
        <ccEmails>USIndiaImmigration@deloitte.com</ccEmails>
        <description>Notify Fragomen, Business Lead and RM of Petition Withdrawal when a Petition is withdrawn</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_FSS_SPOC__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_Project_Manager__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_Resource_Manager__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <recipient>fragomen@deloitte.com.wct.prd</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>WCT_Immigration/WCT_Immigration_Petition_Withdrawn1</template>
    </alerts>
    <alerts>
        <fullName>sendemailwhenimigrationstatusrequestforevidence</fullName>
        <description>Send Email when Imigration Status = &apos;Request For Evidence&apos;</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Assignment_Owner__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_Business_SPOC__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_FSS_SPOC__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_Resource_Manager__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>WCT_Immigration/WCT_Immigration_RFE_Due</template>
    </alerts>
    <alerts>
        <fullName>welcomeemailforimmigration</fullName>
        <description>Welcome Email for Immigration</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Assignment_Owner__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>WCT_Immigration/WCT_Immigration_Welcome_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Fragomen_Milestone_Change</fullName>
        <field>WCT_Fragomen_Milestone_Change__c</field>
        <literalValue>1</literalValue>
        <name>Fragomen Milestone Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Immgration_unattended_count</fullName>
        <description>make the value as 0</description>
        <field>WCT_Unattend_event_Count__c</field>
        <name>Immgration unattended count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Employee_Email_Id_on_Immigration</fullName>
        <field>Employee_Email_Id__c</field>
        <formula>WCT_Assignment_Owner__r.Email</formula>
        <name>Update Employee Email Id on Immigration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_First_Name_on_Immigration</fullName>
        <field>First_Name__c</field>
        <formula>WCT_Assignment_Owner__r.FirstName</formula>
        <name>Update First Name on Immigration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Function_on_Immigration</fullName>
        <field>Function__c</field>
        <formula>WCT_Assignment_Owner__r.WCT_Function__c</formula>
        <name>Update Function on Immigration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Home_Location_on_Immigration</fullName>
        <field>Home_Location__c</field>
        <formula>WCT_Assignment_Owner__r.WCT_Office_City_Personnel_Subarea__c</formula>
        <name>Update Office Location on Immigration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Name_on_Immigration</fullName>
        <field>Last_Name__c</field>
        <formula>WCT_Assignment_Owner__r.LastName</formula>
        <name>Update Last Name on  Immigration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Service_Area_on_Immigration</fullName>
        <field>Service_Area__c</field>
        <formula>WCT_Assignment_Owner__r.WCT_Service_Area__c</formula>
        <name>Update Service Area on Immigration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Service_Line_on_Immigration</fullName>
        <field>Service_Line__c</field>
        <formula>WCT_Assignment_Owner__r.WCT_Service_Line__c</formula>
        <name>Update Service Line on Immigration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Visa_Expiration_check</fullName>
        <field>WCT_Visa_Expiration_Check__c</field>
        <literalValue>1</literalValue>
        <name>Visa Expiration check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>gmiassigntoqueue</fullName>
        <field>OwnerId</field>
        <lookupValue>WCT_GMI</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>GMI Assign to Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setimmigrationstatusnewoncreate</fullName>
        <field>WCT_Immigration_Status__c</field>
        <literalValue>New</literalValue>
        <name>Set Immigration Status New on Create</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setimmigrationvisatypeb1</fullName>
        <description>Visa Type to be pre-populated as &quot;B1/B2&quot; when Immigration Record Type is &quot;Business Visa&quot;</description>
        <field>WCT_Visa_Type__c</field>
        <literalValue>B1/B2</literalValue>
        <name>Set Immigration Visa Type B1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setimmigrationvisatypel2</fullName>
        <description>Visa Type to be pre-populated as &quot;L2&quot; when Immigration Record Type is &quot;EAD&quot;</description>
        <field>WCT_Visa_Type__c</field>
        <literalValue>L2 with EAD</literalValue>
        <name>Set Immigration Visa Type L2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updaterfedatewithsystemdate</fullName>
        <field>WCT_RFE_Date__c</field>
        <formula>Today()</formula>
        <name>Update RFE Date with System Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updatingpetitionoutcomedate</fullName>
        <field>WCT_Petition_Outcome_Date__c</field>
        <formula>WCT_Milestone_Date__c</formula>
        <name>Updating Petition Outcome Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>x150dayselapsedfromrequestinitdate</fullName>
        <description>Automatically update status of Immigration record to &quot;Petition canceled&quot; if Petition is on hold AND if &quot;150&quot; days have elapsed from the date of initiation of Visa application (&quot;Request Initiation Date&quot; field)</description>
        <field>WCT_Immigration_Status__c</field>
        <literalValue>Petition Canceled</literalValue>
        <name>150 days elapsed from request init date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>x90dayselapsedfromrequestinitdate</fullName>
        <description>Ability to automatically update status of Immigration record to &quot;&quot;Petition on hold&quot; if &quot;90&quot; days have elapsed from the date of initiation of Visa application</description>
        <field>WCT_Immigration_Status__c</field>
        <literalValue>Petition On Hold</literalValue>
        <name>90 days elapsed from request init date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Ability to automatically trigger an escalated notification to the Business Lead%2FRM when employee declines 3 consecutive</fullName>
        <actions>
            <name>employee_declines_3_consecutive_Q_A_Call_invites</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Immgration_unattended_count</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Unattend_event_Count__c</field>
            <operation>equals</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Immigration_Status__c</field>
            <operation>notEqual</operation>
            <value>Visa Expired</value>
        </criteriaItems>
        <description>Ability to automatically trigger an escalated notification to the Business Lead/RM when employee declines 3 consecutive Q&amp;A Call invites</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Business Lead receives an email to confirm decision to proceed with Request for Evidence</fullName>
        <active>false</active>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Immigration_Status__c</field>
            <operation>equals</operation>
            <value>Request For Evidence</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Visa_Type__c</field>
            <operation>equals</operation>
            <value>L1B Individual,L1A Individual,L1B Extension,L1A Extension,H1B CAP,H1B Transfer,H1B Extension</value>
        </criteriaItems>
        <description>Business Lead receives an email to confirm decision to proceed with Request for Evidence</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Business Lead receives notification that the Petition for a Visa Application is approved</fullName>
        <actions>
            <name>businessleadreceivesnotificationthatthepetitionforavisaapplicationisapprove</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Immigration_Status__c</field>
            <operation>equals</operation>
            <value>Petition Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Visa_Type__c</field>
            <operation>equals</operation>
            <value>L1B Individual,L1A Individual,L1B Extension,L1A Extension,H1B CAP,H1B Transfer,H1B Extension</value>
        </criteriaItems>
        <description>Business Lead receives notification that the Petition for a Visa Application is approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Alert - Petition Filed with USCIS for H1B Amendment</fullName>
        <actions>
            <name>Notification_to_Employee_and_RM_on_H1B_amendment_petition_filing_with_USCIS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Visa_Type__c</field>
            <operation>equals</operation>
            <value>H1B Amendment</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Immigration_Status__c</field>
            <operation>equals</operation>
            <value>Petition Filed with USCIS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email a notice of %E2%80%9CPetition Expired%E2%80%9D upon reaching expiration Date</fullName>
        <actions>
            <name>emailanoticeofpetitionexpireduponreachingexpirationdate</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Visa_Type__c</field>
            <operation>equals</operation>
            <value>L1B Individual,L1A Individual,L1B Extension,L1A Extension,H1B CAP,H1B Transfer,H1B Extension,L1B Blanket,L1A Blanket</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Immigration_Status__c</field>
            <operation>equals</operation>
            <value>Petition Expired</value>
        </criteriaItems>
        <description>Email a notice of “Petition Expired” upon reaching expiration Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email a notice of %E2%80%9CVisaExpired%E2%80%9D upon reaching expiration Date</fullName>
        <actions>
            <name>Email_a_notice_of_Visa_Expired_upon_reaching_expiration_Date</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Visa_Expiration_check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Immigration__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>H1 Visa,L1 Visa</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Immigration_Status__c</field>
            <operation>equals</operation>
            <value>Petition Approved,Petition Shipped/Sent,Petition Received,Petition Collected,Visa Appointment Scheduled,Visa Approved,Visa Stamped,Petition Expired</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Visa_To_be_Expire__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Visa_Expiration_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Visa_Expiration_Check__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email notification to employee of Petition Denial</fullName>
        <actions>
            <name>Email_notification_to_employee_of_Petition_Denial</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Immigration_Status__c</field>
            <operation>equals</operation>
            <value>Petition Denied</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Immigration__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>H1 Visa,L1 Visa</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email notification to employee to collect the petition</fullName>
        <actions>
            <name>Email_notification_to_collect_petition</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Immigration_Status__c</field>
            <operation>equals</operation>
            <value>Petition Received</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification to Employee with Appointment Date of Consulate Interview appointment</fullName>
        <active>false</active>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Visa_Interview_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Visa_Type__c</field>
            <operation>notEqual</operation>
            <value>L2 with EAD</value>
        </criteriaItems>
        <description>Notification to Employee with Appointment Date of Consulate Interview appointment</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Fragomen%2C Business Lead and RM of Petition Withdrawal when a Petition is withdrawn</fullName>
        <actions>
            <name>notifyfragomenbusinessleadandrmofpetitionwithdrawalwhenapetitioniswithdrawn</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Immigration_Status__c</field>
            <operation>equals</operation>
            <value>Petition Withdrawn</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Visa_Type__c</field>
            <operation>equals</operation>
            <value>L1B Individual,L1A Individual,L1B Extension,L1A Extension,H1B CAP,H1B Transfer,H1B Extension</value>
        </criteriaItems>
        <description>Notify Fragomen, Business Lead and RM of Petition Withdrawal when a Petition is withdrawn</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Fragomen%2C Business Lead and RM of RFE Withdrawal</fullName>
        <active>false</active>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Immigration_Status__c</field>
            <operation>equals</operation>
            <value>Request For Evidence Withdrawn</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Visa_Type__c</field>
            <operation>equals</operation>
            <value>L1B Individual,L1A Individual,L1B Extension,L1A Extension,H1B CAP,H1B Transfer,H1B Extension</value>
        </criteriaItems>
        <description>Notify Fragomen, Business Lead and RM of RFE Withdrawal</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RFE Status Change</fullName>
        <actions>
            <name>Fragomen_Milestone_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(ISCHANGED( WCT_Milestone__c )) &amp;&amp;   ((ISPICKVAL( WCT_Immigration_Status__c , &apos;Request For Evidence Expired&apos;)) || (ISPICKVAL( WCT_Immigration_Status__c , &apos;Request For Evidence&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email when Immigration Status %3D %27Request for Evidence%22</fullName>
        <actions>
            <name>sendemailwhenimigrationstatusrequestforevidence</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Immigration_Status__c</field>
            <operation>equals</operation>
            <value>Request For Evidence</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Check_for_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Visa_Type__c</field>
            <operation>equals</operation>
            <value>L1B Individual,L1A Individual,L1B Extension,L1A Extension,H1B Transfer,H1B Extension</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send alert to owner when employee submits visa logistics information</fullName>
        <actions>
            <name>Visa_Logistics_Information_received_from_Employee</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Visa_Logistics_Info_Received__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set B1 Immigration Visa Type</fullName>
        <actions>
            <name>setimmigrationvisatypeb1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Immigration__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>B1 Visa</value>
        </criteriaItems>
        <description>Visa Type to be pre-populated as &quot;B1/B2&quot; when Immigration Record Type is &quot;Business Visa&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Immigration Queue on Create</fullName>
        <actions>
            <name>gmiassigntoqueue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Immigration Status New on Create</fullName>
        <actions>
            <name>setimmigrationstatusnewoncreate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Immigration_Status__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Immigration Visa Type L2</fullName>
        <actions>
            <name>setimmigrationvisatypel2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Immigration__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>L2 with EAD</value>
        </criteriaItems>
        <description>Visa Type to be pre-populated as &quot;L2&quot; when Immigration Record Type is &quot;EAD&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Stamp RFE Date Field when Immigration Status %3D %27Request For Evidence%27</fullName>
        <actions>
            <name>updaterfedatewithsystemdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Immigration_Status__c</field>
            <operation>equals</operation>
            <value>Request For Evidence</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Stamping date in Petition Outcome Date field when Milestone Field %3D Petition Approved</fullName>
        <actions>
            <name>updatingpetitionoutcomedate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Milestone__c</field>
            <operation>equals</operation>
            <value>Petition Approved</value>
        </criteriaItems>
        <description>Stamping date in Petition Outcome Date field when Milestone Field = Petition Approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Employee Information</fullName>
        <actions>
            <name>Update_Employee_Email_Id_on_Immigration</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_First_Name_on_Immigration</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Function_on_Immigration</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Home_Location_on_Immigration</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Last_Name_on_Immigration</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Service_Area_on_Immigration</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Service_Line_on_Immigration</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>True</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Welcome Email for Immigration</fullName>
        <actions>
            <name>welcomeemailforimmigration</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 4)  OR (1 AND 2 AND 3 AND 4)</booleanFilter>
        <criteriaItems>
            <field>WCT_Immigration__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>L2 with EAD</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Immigration__c.Immigration_Email_status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Immigration_Bvisa_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Immigration__c.WCT_Immigration_Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
