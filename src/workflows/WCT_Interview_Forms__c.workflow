<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>IT_UpdateIEFSubmissiontoDraft</fullName>
        <description>UpdateIEFSubmissiontoDraft</description>
        <field>WCT_IEF_Submission_Status__c</field>
        <literalValue>Draft</literalValue>
        <name>IT:UpdateIEFSubmissiontoDraft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>WCT_Interview_Junction__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IT_UpdateIEFSubmissiontoSubmitted</fullName>
        <description>IT:UpdateIEFSubmissiontoSubmitted</description>
        <field>WCT_IEF_Submission_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>IT:UpdateIEFSubmissiontoSubmitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>WCT_Interview_Junction__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IT_UpdateInterviewerRecommendation</fullName>
        <description>UpdateInterviewerRecommendation from IEF Form</description>
        <field>WCT_Interviewer_Recommendation__c</field>
        <formula>WCT_Interviewer_s_Final_Recommendation__c</formula>
        <name>IT:UpdateInterviewerRecommendation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>WCT_Interview_Junction__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IT_Update_Submitted_Date</fullName>
        <field>WCT_IEF_Last_Submitted_Date__c</field>
        <formula>Now()</formula>
        <name>IT Update Submitted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>WCT_Interview_Junction__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateInterviewerRecommendation</fullName>
        <description>UpdateInterviewerRecommendation from IEF Form</description>
        <field>WCT_Interviewer_Recommendation__c</field>
        <formula>WCT_Interviewer_s_Final_Recommendation__c</formula>
        <name>UpdateInterviewerRecommendation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>WCT_Interview_Junction__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>InterviewForm%3AUpdateIEF Version on Interview</fullName>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Interview_Forms__c.WCT_Form_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>IEF version would be increasing as Interview Form gets updated</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IntvForm%3AIEF Submission Status on %22InterviewTracker%22 to %22Draft%22 on IEF submission</fullName>
        <actions>
            <name>IT_UpdateIEFSubmissiontoDraft</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IT_UpdateInterviewerRecommendation</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IT_Update_Submitted_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Interview_Forms__c.WCT_IEF_Status__c</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <description>Update IEF Submission Status on &quot;InterviewTracker&quot; to &quot;Draft&quot; on IEF creation if the IEF is submitted by Save as Draft</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IntvForm%3AIEF Submission Status on %22InterviewTracker%22 to %22Sumbmitted%22 on IEF submission</fullName>
        <actions>
            <name>IT_UpdateIEFSubmissiontoSubmitted</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IT_UpdateInterviewerRecommendation</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IT_Update_Submitted_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Interview_Forms__c.WCT_IEF_Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <description>The IEF Submission Status will need to be update to &quot;Submitted&quot; after an IEF is submitted on behalf of an interview</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
