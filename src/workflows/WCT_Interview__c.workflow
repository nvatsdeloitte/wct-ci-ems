<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Interview_Upd_Intv_Status_to_canceled</fullName>
        <field>WCT_Interview_Status__c</field>
        <literalValue>Canceled</literalValue>
        <name>Interview: Upd Intv Status to canceled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Interview_UpdateInterviewStatus</fullName>
        <description>Update Interview Status to Complete</description>
        <field>WCT_Interview_Status__c</field>
        <literalValue>Complete</literalValue>
        <name>Interview:UpdateInterviewStatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Event_Updated_to_false</fullName>
        <field>IsEvent_Updated__c</field>
        <literalValue>0</literalValue>
        <name>Update Event Updated to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_count_of_reschedule_field</fullName>
        <field>Rescheduled__c</field>
        <formula>if(ISBLANK(Rescheduled__c), 1,ABS((Rescheduled__c) + 1) )</formula>
        <name>Update count of reschedule field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Count of Rescheduled Interviews</fullName>
        <actions>
            <name>Update_count_of_reschedule_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(WCT_Interview_Start_Time__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Interview%3AUpdate InterviewStatus to Canceled on all Trackers Cancellation</fullName>
        <actions>
            <name>Interview_Upd_Intv_Status_to_canceled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Interview:Update Interview Status to Cancelled once all Trackers are Cancelled</description>
        <formula>ISCHANGED( WCT_Number_of_Canceled_Trackers__c)  &amp;&amp;  WCT_Number_of_Canceled_Trackers__c =  WCT_Number_of_Trackers__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Interview%3AUpdate InterviewStatus to Completion Trackers Completion</fullName>
        <actions>
            <name>Interview_UpdateInterviewStatus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Interview:Update Interview Status to Completion Trackers Completion</description>
        <formula>OR( ISCHANGED( WCT_Number_of_Canceled_Trackers__c) ,ISCHANGED(WCT_Number_of_Completed_Trackers__c)) &amp;&amp; (WCT_Number_of_Completed_Trackers__c + WCT_Number_of_Canceled_Trackers__c = WCT_Number_of_Trackers__c )&amp;&amp;  WCT_Number_of_Completed_Trackers__c &gt; 0</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Is event updated</fullName>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Interview__c.IsEvent_Updated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Event_Updated_to_false</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
