<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>updatestatustolcaexpired</fullName>
        <field>WCT_Status__c</field>
        <literalValue>LCA Expired</literalValue>
        <name>Update Status to LCA Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>LCA%3A LCA Expired when LCA reaches the LCA expiration date</fullName>
        <active>true</active>
        <criteriaItems>
            <field>WCT_LCA__c.WCT_Expiration_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>submitapprovedlcadocumentation</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>10</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>WCT_LCA__c.CreatedDate</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Submit approved LCA documentation</subject>
    </tasks>
</Workflow>
