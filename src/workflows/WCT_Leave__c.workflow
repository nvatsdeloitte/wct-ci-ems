<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approaching_18_Months_on_Military_Leave</fullName>
        <description>Approaching 18 Months on Military Leave</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_RM_or_BA_contact_for_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>eleleavesteam@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Leaves_Email_Templates/WCT_Military_Leave_18_Months</template>
    </alerts>
    <alerts>
        <fullName>Notify_Benefits_of_the_need_to_send_Baby_Packet_to_employee</fullName>
        <description>Notify Benefits of the need to send Baby Packet to employee</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>eleleavesteam@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Leaves_Email_Templates/Notify_Benefits_of_the_need_to_send_Baby_Packet_to_employee</template>
    </alerts>
    <alerts>
        <fullName>Reaching_12_Months_Benefits_Termination_to_RM_BA</fullName>
        <description>Reaching 12 Months (Benefits Termination to RM/BA)</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_RM_or_BA_contact_for_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>eleleavesteam@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Leaves_Email_Templates/WCT_Rching12MnthsBnfitsTrmintn_to_RM_BA</template>
    </alerts>
    <alerts>
        <fullName>Send_Confirm_Return_to_Work_email_to_employee_cc_RM_BA</fullName>
        <description>Send Confirm Return to Work email to employee, cc RM/BA.</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_RM_or_BA_contact_for_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>eleleavesteam@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Leaves_Email_Templates/WCT_AUTO_Confirmation_of_Return_to_Work</template>
    </alerts>
    <alerts>
        <fullName>Send_Welcome_Back_email_to_the_employee_cc_RM_BA</fullName>
        <description>Send &quot;Welcome Back&quot; email to the employee, cc RM/BA.</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_RM_or_BA_contact_for_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>eleleavesteam@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Leaves_Email_Templates/WCT_Welcome_Back_Email</template>
    </alerts>
    <alerts>
        <fullName>Send_notification_to_employee_cc_RM_BA</fullName>
        <description>Send notification of 18 months on Military leave to employee and RM/BA.</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_RM_or_BA_contact_for_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>eleleavesteam@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Leaves_Email_Templates/WCT_Military_Leave_18_Months</template>
    </alerts>
    <alerts>
        <fullName>Send_reminder_email_to_employee</fullName>
        <description>Send reminder email to employee</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_RM_or_BA_contact_for_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>eleleavesteam@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Leaves_Email_Templates/WCT_Welcome_Back_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_Owner</fullName>
        <description>Change owner to default leave queue</description>
        <field>OwnerId</field>
        <lookupValue>Leaves_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change Owner to Queue</fullName>
        <actions>
            <name>Change_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Changing case owner to default Leaves queue</description>
        <formula>Owner:Queue.DeveloperName  &lt;&gt; &quot;Leaves_Queue&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Direct Billing Account - When  9041 Needed</fullName>
        <actions>
            <name>Direct_Billing_Account</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>WCT_Leave__c.WCT_X9041_needed__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Leave %3A Welcome Back e-mail reminder</fullName>
        <active>false</active>
        <criteriaItems>
            <field>WCT_Leave__c.WCT_Return_to_work_date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>WCT_Send_Welcome_Back_e_mail</name>
                <type>Task</type>
            </actions>
            <offsetFromField>WCT_Leave__c.WCT_Return_to_work_date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Leave %3AReturn to work _Reminder</fullName>
        <active>false</active>
        <criteriaItems>
            <field>WCT_Leave__c.WCT_Leave_Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Leave__c.WCT_Leave_End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>WCT_Return_to_work_confirmation_email</name>
                <type>Task</type>
            </actions>
            <offsetFromField>WCT_Leave__c.WCT_Leave_End_Date__c</offsetFromField>
            <timeLength>-14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Leave%3A Check for FMLA Paperwork Completion</fullName>
        <actions>
            <name>Check_for_FMLA_Paperwork_Completion</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>WCT_Leave__c.WCT_FMLA_Leave_Packet_Sent_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Leave%3A If leaves hasn%27t received all required info%2Fapproval within 7 days of leave request initiation</fullName>
        <active>false</active>
        <criteriaItems>
            <field>WCT_Leave__c.WCT_Approval_Status__c</field>
            <operation>notEqual</operation>
            <value>Approved</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>If_leaves_hasn_t_received_all_required_info_approval_within_7_days_of_leave_requ</name>
                <type>Task</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Leave%3A Initiate review process %28Review For Separation With OGC%2C TR%26C And CoE As Appropriate%29</fullName>
        <actions>
            <name>Initiate_review_proces_Review_For_Separation_With_OGC_TR_C_And_CoE_As_Appropriat</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <formula>WCT_Flag_Initiate_review_process__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Leave%3A Note if employee confirms return date 1 week before return%2C or call if no response</fullName>
        <actions>
            <name>Note_if_employee_confirms_return_date_1_week_before_return_or_call_if_no_respons</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>WCT_Leave__c.WCT_Confirm_Return_to_Work__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Leave__c.WCT_Leave_Type__c</field>
            <operation>equals</operation>
            <value>FMLA,FMLA+STD,No FMLA</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Leave%3A Notify Benefits of the need to send Baby Packet to employee</fullName>
        <actions>
            <name>Notify_Benefits_of_the_need_to_send_Baby_Packet_to_employee</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>WCT_Leave__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Parental</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Leave%3A Send notification of 12 months%28other than Military leave%29</fullName>
        <actions>
            <name>Reaching_12_Months_Benefits_Termination_to_RM_BA</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Review_leave_after_Employee_completes_11_months_of_leave</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AND(  RecordType.DeveloperName  &lt;&gt; &apos;Military&apos;  , WCT_Flag_Initiate_review_process__c, ISPICKVAL( WCT_Leave_End_Date_Status__c ,&apos;Confirmed&apos;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Leave%3A Send notification of 18 months on Military leave</fullName>
        <actions>
            <name>Send_notification_to_employee_cc_RM_BA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( RecordType.DeveloperName ==&apos;Military&apos; , WCT_Flag_18_months_military_notify__c == true , ISPICKVAL( WCT_Leave_End_Date_Status__c,&apos;Confirmed&apos;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Check_for_FMLA_Paperwork_Completion</fullName>
        <assignedTo>leavesuser@deloitte.com.wct.prd</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>15</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>WCT_Leave__c.WCT_FMLA_Leave_Packet_Sent_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Check for FMLA Paperwork Completion</subject>
    </tasks>
    <tasks>
        <fullName>Direct_Billing_Account</fullName>
        <assignedTo>leavesuser@deloitte.com.wct.prd</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Setup Direct Billing for Unpaid Leave</description>
        <dueDateOffset>-30</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>WCT_Leave__c.WCT_Leave_Start_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Direct Billing Account</subject>
    </tasks>
    <tasks>
        <fullName>If_leaves_hasn_t_received_all_required_info_approval_within_7_days_of_leave_requ</fullName>
        <assignedTo>leavesuser@deloitte.com.wct.prd</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>If leaves hasn&apos;t received all required info/approval within 7 days of leave request initiation, close leave record and notify employee</subject>
    </tasks>
    <tasks>
        <fullName>Initiate_review_proces_Review_For_Separation_With_OGC_TR_C_And_CoE_As_Appropriat</fullName>
        <assignedTo>leavesuser@deloitte.com.wct.prd</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>30</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Initiate review process (Review For Separation With OGC, TR&amp;C And CoE As Appropriate)</subject>
    </tasks>
    <tasks>
        <fullName>Note_if_employee_confirms_return_date_1_week_before_return_or_call_if_no_respons</fullName>
        <assignedTo>leavesuser@deloitte.com.wct.prd</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>-7</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>WCT_Leave__c.WCT_Return_to_work_date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Note if employee confirms return date 1 week before return, or call if no response</subject>
    </tasks>
    <tasks>
        <fullName>Notify_Benefits_of_the_need_to_send_Baby_Packet_to_employee</fullName>
        <assignedTo>leavesuser@deloitte.com.wct.prd</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Notify Benefits of the need to send Baby Packet to employee</subject>
    </tasks>
    <tasks>
        <fullName>Review_leave_after_Employee_completes_11_months_of_leave</fullName>
        <assignedTo>leavesuser@deloitte.com.wct.prd</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>330</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Review leave after Employee completes 11 months of leave</subject>
    </tasks>
    <tasks>
        <fullName>WCT_Return_to_work_confirmation_email</fullName>
        <assignedToType>owner</assignedToType>
        <description>Reminder to send return to work confirmation email</description>
        <dueDateOffset>-14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>WCT_Leave__c.WCT_Leave_End_Date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Return to work confirmation email</subject>
    </tasks>
    <tasks>
        <fullName>WCT_Send_Welcome_Back_e_mail</fullName>
        <assignedToType>owner</assignedToType>
        <description>Reminder to send Welcome back email to the employee</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>WCT_Leave__c.WCT_Return_to_work_date__c</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send Welcome Back e-mail</subject>
    </tasks>
</Workflow>
