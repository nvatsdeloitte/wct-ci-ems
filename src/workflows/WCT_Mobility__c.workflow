<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_notification_to_employee_for_Departure_Details</fullName>
        <description>Email notification to employee for Departure Details</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Mobility_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mobility/WCT_Mobility_Departure_Email</template>
    </alerts>
    <alerts>
        <fullName>Notification_sent_to_Employee_on_a_weekly_basis_2_days_before_travel_start_Date</fullName>
        <description>Notification sent to Employee on a weekly basis 2 days before travel start Date</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Mobility_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_Resource_Manager__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mobility/Mandatory_action_required_Business_start_date_confirmation</template>
    </alerts>
    <alerts>
        <fullName>OGC_GMITeam</fullName>
        <ccEmails>usiindiaimmigration@deloitte.com</ccEmails>
        <description>OGC GMI Team Email Alert</description>
        <protected>false</protected>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mobility/OGC_GMI_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>OGC_professional_Email</fullName>
        <ccEmails>usiindiaimmigration@deloitte.com</ccEmails>
        <description>OGC professional Email</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Mobility_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mobility/OGC_Professional_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>WCT_ACTION_REQUIRED_US_Travel_Request_Initiation_Business</fullName>
        <description>ACTION REQUIRED: US Travel Request Initiation - Business</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_USI_Resource_Manager__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mobility/WCT_ACTION_REQUIRED_US_Travel_Request_Initiation_Business</template>
    </alerts>
    <alerts>
        <fullName>WCT_ACTION_REQUIRED_US_Travel_Request_Initiation_Employment</fullName>
        <description>ACTION REQUIRED: US Travel Request Initiation - Employment</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_USI_Resource_Manager__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mobility/WCT_ACTION_REQUIRED_US_Travel_Request_Initiation_Employment</template>
    </alerts>
    <alerts>
        <fullName>WCT_ACTION_REQUIRED_US_Travel_Request_Initiation_Travel_Vendor_Business</fullName>
        <ccEmails>travelvendor@deloitte.com</ccEmails>
        <description>ACTION REQUIRED: US Travel Request Initiation Travel Vendor - Business</description>
        <protected>false</protected>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mobility/WCT_ACTION_REQUIRED_US_Travel_Request_Initiation_Travel_Vendor_Business</template>
    </alerts>
    <alerts>
        <fullName>WCT_ACTION_REQUIRED_US_Travel_Request_Initiation_Travel_Vendor_Employment</fullName>
        <ccEmails>travelvendor@deloitte.com</ccEmails>
        <description>ACTION REQUIRED: US Travel Request Initiation Travel Vendor - Employment</description>
        <protected>false</protected>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mobility/WCT_ACTION_REQUIRED_US_Travel_Request_Initiation_Travel_Vendor_Employment</template>
    </alerts>
    <alerts>
        <fullName>escalated_notification_to_the_Business_Lead_RM_when_employee_declines_3_consecut</fullName>
        <description>escalated notification to the Business Lead/RM when employee declines 3 consecutive Q&amp;A  MobilityCall invites</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Mobility_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_USI_Report_Mngr__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>WCT_USI_Resource_Manager__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>eEvent_Templates/Mobility_Employee_Decline_call_Q_A_Notifications</template>
    </alerts>
    <alerts>
        <fullName>notificationescelationsenttobusinessleadrmswhendocumentationpendingasonlast</fullName>
        <description>Notification escelation sent to Business Lead/RMs when documentation pending as on Last Working Day in India</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Mobility_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_USI_Resource_Manager__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mobility/WCT_Employee_name_non_compliant_Pre_departure_process</template>
    </alerts>
    <alerts>
        <fullName>notificationsentravelenddateextension</fullName>
        <ccEmails>usindiaimmigration@deloitte.com</ccEmails>
        <ccEmails>usindiabusinesstravel@deloitte.com</ccEmails>
        <description>Notification sent to  professional regarding travel end date extension</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Mobility_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mobility/WCT_Employee_name_Bvisa_travel_end_date_extension_Travel_Team</template>
    </alerts>
    <alerts>
        <fullName>notificationsenttoemployeeonaweeklybasis3daysbeforetravelenddatetocompleteo</fullName>
        <description>Notification sent to Employee on a weekly basis/3 days before travel End Date to complete On-Travel Activities</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Mobility_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_USI_Resource_Manager__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mobility/WCT_Mandatory_action_required_End_date_confirmation</template>
    </alerts>
    <alerts>
        <fullName>notificationsenttoresourcemanager14daysbeforescheduleddateforlastworkingday</fullName>
        <description>Notification Sent To Resource Manager 14 Days Before Scheduled Date For Last Working Day In The US</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Mobility_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_USI_Resource_Manager__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mobility/WCT_Employee_name_Project_end_date</template>
    </alerts>
    <alerts>
        <fullName>notificationsenttoresourcemanagerbusinessleademployeewhenvisadeniedanddeplo</fullName>
        <description>Notification sent to Resource Manager/Business Lead/Employee when Visa Denied and deployement is cancelled</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Mobility_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_USI_Report_Mngr__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>WCT_USI_Resource_Manager__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>WCT_US_Project_Mngr__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mobility/WCT_Employee_name_Visa_denied_Deployment_cancelled</template>
    </alerts>
    <alerts>
        <fullName>notificationsenttotraveldeskwithemployeeinfotocompletetravelinsurance</fullName>
        <ccEmails>deloitte.hyd.in@contactcwt.com</ccEmails>
        <description>Notification sent to travel desk with employee info to complete travel insurance</description>
        <protected>false</protected>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mobility/WCT_Employee_name_Visa_type_travel_end_date_extension_Travel_Team</template>
    </alerts>
    <alerts>
        <fullName>reportsenttototalrewardscoefromsfdcfromfirststepinevpreassignmentb</fullName>
        <ccEmails>bisingh@deloitte.com</ccEmails>
        <ccEmails>thakusingh@deloitte.com</ccEmails>
        <description>Report sent to Total Rewards CoE from SFDC from first step in EV Pre-Assignment B</description>
        <protected>false</protected>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mobility/WCT_Country_Onsite_fitments_requisition</template>
    </alerts>
    <alerts>
        <fullName>sendwelcomeemailbusinessvisa</fullName>
        <description>Send Welcome email - Business Visa</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Mobility_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mobility/WCT_Welcome_Email_Business_Visa</template>
    </alerts>
    <alerts>
        <fullName>sendwelcomeemailemploymentvisa</fullName>
        <description>Send Welcome email - Employment Visa</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Mobility_Employee__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>WCT_USI_Resource_Manager__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>usigmi@deloitte.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Mobility/WCT_Welcome_Email_Employment_Visa</template>
    </alerts>
    <fieldUpdates>
        <fullName>Check_Travel_End_Date_Extended</fullName>
        <field>WCT_Travel_End_Date_Extended__c</field>
        <literalValue>1</literalValue>
        <name>Check &apos;Travel End Date Extended&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Travel_End_Date_Extended_New</fullName>
        <field>WCT_Travel_End_Date_Extended__c</field>
        <literalValue>1</literalValue>
        <name>Check Travel End Date Extended New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Extended_LWD</fullName>
        <field>Extended_Months__c</field>
        <formula>(Year(WCT_Last_Working_Day_in_US__c)*12+ Month(WCT_Last_Working_Day_in_US__c))-
(Year(PRIORVALUE(WCT_Last_Working_Day_in_US__c))*12+ Month(PRIORVALUE(WCT_Last_Working_Day_in_US__c)))</formula>
        <name>Extended LWD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mobility_Unattended_event_coount</fullName>
        <field>WCT_Unattend_Mobility_Event_Count__c</field>
        <name>Mobility Unattended event coount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mobility_status_update</fullName>
        <field>WCT_Mobility_Status__c</field>
        <literalValue>Travel Extended</literalValue>
        <name>Mobility status update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OGC_Task_status_c</fullName>
        <field>OGC_Task_status__c</field>
        <literalValue>1</literalValue>
        <name>OGC_Task_status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Assignment_Type_Long_Term</fullName>
        <field>WCT_Assignment_Type__c</field>
        <literalValue>Long Term</literalValue>
        <name>Set Assignment Type = Long Term</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Assignment_Type_Medium_Term</fullName>
        <field>WCT_Assignment_Type__c</field>
        <literalValue>Medium Term</literalValue>
        <name>Set Assignment Type = Medium Term</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Assignment_Type_Short_Term</fullName>
        <field>WCT_Assignment_Type__c</field>
        <literalValue>Short Term</literalValue>
        <name>Set Assignment Type = Short Term</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Dependent_Visa_Task_Created</fullName>
        <field>WCT_DependentVisaTask_Created__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck &apos;Dependent Visa Task Created&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Employee_Email_Id_on_Mobility</fullName>
        <field>Employee_Email_Id__c</field>
        <formula>WCT_Mobility_Employee__r.Email</formula>
        <name>Update Employee Email Id on Mobility</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_First_Name_on_Mobility</fullName>
        <field>First_Name__c</field>
        <formula>WCT_Mobility_Employee__r.FirstName</formula>
        <name>Update First Name on Mobility</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Function_on_Mobility</fullName>
        <field>Employee_Function__c</field>
        <formula>WCT_Mobility_Employee__r.WCT_Function__c</formula>
        <name>Update Function on Mobility</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Home_Location_on_Mobility</fullName>
        <field>Home_Location__c</field>
        <formula>WCT_Mobility_Employee__r.WCT_Office_City_Personnel_Subarea__c</formula>
        <name>Update Office Location on Mobility</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Name_on_Mobility</fullName>
        <field>Last_Name__c</field>
        <formula>WCT_Mobility_Employee__r.LastName</formula>
        <name>Update Last Name on Mobility</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Service_Area_on_Mobility</fullName>
        <field>Service_Area__c</field>
        <formula>WCT_Mobility_Employee__r.WCT_Service_Area__c</formula>
        <name>Update Service Area on Mobility</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Service_Line_on_Mobility</fullName>
        <field>Service_Line__c</field>
        <formula>WCT_Mobility_Employee__r.WCT_Service_Line__c</formula>
        <name>Update Service Line on Mobility</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Insurance_Task_Status_c</fullName>
        <description>updating the field value as TRUE</description>
        <field>WCT_Insurance_Task_Status__c</field>
        <literalValue>1</literalValue>
        <name>WCT_Insurance_Task_Status__c</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Uncheck_Is_Last_Working_Day_In_US</fullName>
        <field>WCT_Is_Last_Working_Day_In_The_US_Alert__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck &apos;Is Last Working Day In US&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Uncheck_Is_Travel_End_Date_Alert</fullName>
        <field>WCT_Is_Travel_End_Date_Alert__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck &apos;Is Travel End Date Alert&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>mobilitychangestatuson1stworkingda</fullName>
        <description>SFDC should automatically change the status of Mobility record to &quot;Employee On-assignment: Current Assignment&quot;</description>
        <field>WCT_Mobility_Status__c</field>
        <literalValue>Employee On Assignment</literalValue>
        <name>Mobility Change Status on 1st working da</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>mobilityupdaterecordowner</fullName>
        <field>OwnerId</field>
        <lookupValue>WCT_GMI</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Mobility - Update Record owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>mobilityupdaterecordstatus</fullName>
        <field>WCT_Mobility_Status__c</field>
        <literalValue>New</literalValue>
        <name>Mobility - Update Record Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>uncheckregeneratetasks</fullName>
        <field>WCT_Regenerate_Tasks__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck &apos;Regenerate Tasks&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ACTION REQUIRED%3A US Travel Request Initiation - Business</fullName>
        <actions>
            <name>WCT_ACTION_REQUIRED_US_Travel_Request_Initiation_Business</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Mobility_Type__c</field>
            <operation>equals</operation>
            <value>Business Visa</value>
        </criteriaItems>
        <description>Once Mobility request form for Business travel/Employment travel is submitted by the employee, then an automated notification should go to the Resource Manager of the employee</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ACTION REQUIRED%3A US Travel Request Initiation - Employment</fullName>
        <actions>
            <name>WCT_ACTION_REQUIRED_US_Travel_Request_Initiation_Employment</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Mobility_Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Mobility_Type__c</field>
            <operation>equals</operation>
            <value>Employment Visa</value>
        </criteriaItems>
        <description>Once Mobility request form for Business travel/Employment travel is submitted by the employee, then an automated notification should go to the Resource Manager of the employee</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ACTION REQUIRED%3A US Travel Request Initiation Travel Vendor - Business</fullName>
        <actions>
            <name>WCT_ACTION_REQUIRED_US_Travel_Request_Initiation_Travel_Vendor_Business</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Mobility_Status__c</field>
            <operation>equals</operation>
            <value>Initiate Onboarding</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Mobility_Type__c</field>
            <operation>equals</operation>
            <value>Business Visa</value>
        </criteriaItems>
        <description>When status of record changes to “Initiate onboarding”, then a notification should be sent to the travel vendor (to notify them that the person is eligible for booking his tickets)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ACTION REQUIRED%3A US Travel Request Initiation Travel Vendor - Employment</fullName>
        <actions>
            <name>WCT_ACTION_REQUIRED_US_Travel_Request_Initiation_Travel_Vendor_Employment</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Mobility_Status__c</field>
            <operation>equals</operation>
            <value>Initiate Onboarding</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Mobility_Type__c</field>
            <operation>equals</operation>
            <value>Employment Visa</value>
        </criteriaItems>
        <description>When status of record changes to “Initiate onboarding”, then a notification should be sent to the travel vendor (to notify them that the person is eligible for booking his tickets)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email notification to employee for Departure Details</fullName>
        <actions>
            <name>Email_notification_to_employee_for_Departure_Details</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Mobility_Status__c</field>
            <operation>equals</operation>
            <value>Ready for Departure</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Mobility__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Employment Visa</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Employee On Assignment%3A Rollover - - Uncheck %27Dependent Visa Task Created%27</fullName>
        <actions>
            <name>Uncheck_Dependent_Visa_Task_Created</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Mobility_Status__c</field>
            <operation>equals</operation>
            <value>Employee On Assignment: Rollover</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Extended Last Working Day</fullName>
        <actions>
            <name>Extended_LWD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(WCT_Last_Working_Day_in_US__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Mobility Change Status on 1st working day US</fullName>
        <actions>
            <name>mobilitychangestatuson1stworkingda</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_First_Working_Day_in_US__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Mobility - Business Visa</fullName>
        <actions>
            <name>sendwelcomeemailbusinessvisa</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Mobility__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business Visa</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Mobility - Employment Visa</fullName>
        <actions>
            <name>sendwelcomeemailemploymentvisa</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Mobility__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Employment Visa</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification Sent To Resource Manager 14 Days Before Scheduled Date For Last Working Day In The US</fullName>
        <actions>
            <name>notificationsenttoresourcemanager14daysbeforescheduleddateforlastworkingday</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Is_Last_Working_Day_In_The_US_Alert__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Mobility__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Employment Visa</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Mobility_Status__c</field>
            <operation>equals</operation>
            <value>Employee On Assignment,Employee On Assignment: Assignment Extended,Employee On Assignment: Rollover</value>
        </criteriaItems>
        <description>Notification Sent To Resource Manager  “X” Days Before Scheduled Date For “Last Working Day In The US”</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification escelation sent to Business Lead%2FRMs when documentation pending as on Last Working Day in India</fullName>
        <actions>
            <name>notificationescelationsenttobusinessleadrmswhendocumentationpendingasonlast</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Is_Travel_Start_Date_Alert__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Notification escelation sent to Business Lead/RMs when documentation pending as on Last Working Day in India</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification sent to Employee on a weekly basis%2F2 days before travel Start Date to complete On-Travel Activities</fullName>
        <actions>
            <name>Notification_sent_to_Employee_on_a_weekly_basis_2_days_before_travel_start_Date</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Is_Business_Travel_Start_Date_Alert__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Mobility__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business Visa</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Mobility_Status__c</field>
            <operation>equals</operation>
            <value>Ready for Travel</value>
        </criteriaItems>
        <description>Notification sent to Employee on a weekly basis/2 days before travel Start Date to complete On-Travel Activities</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification sent to Employee on a weekly basis%2F3 days before travel End Date to complete On-Travel Activities</fullName>
        <actions>
            <name>notificationsenttoemployeeonaweeklybasis3daysbeforetravelenddatetocompleteo</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Is_Travel_End_Date_Alert__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Mobility__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business Visa</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Mobility_Status__c</field>
            <operation>equals</operation>
            <value>Employee on Travel</value>
        </criteriaItems>
        <description>Notification sent to Employee on a weekly basis/3 days before travel End Date to complete On-Travel Activities</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification sent to Resource Manager%2FBusiness Lead%2FEmployee when Visa Denied and deployement is cancelled</fullName>
        <actions>
            <name>notificationsenttoresourcemanagerbusinessleademployeewhenvisadeniedanddeplo</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>WCT_Mobility__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Employment Visa,Business Visa</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Mobility_Status__c</field>
            <operation>equals</operation>
            <value>Deployment Cancelled</value>
        </criteriaItems>
        <description>Notification sent to Resource Manager/Business Lead/Employee when Visa Denied and deployement is cancelled</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification sent to travel desk with employee info to complete travel insurance</fullName>
        <actions>
            <name>notificationsenttotraveldeskwithemployeeinfotocompletetravelinsurance</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notification sent to travel desk with employee info to complete travel insurance</description>
        <formula>AND( ISCHANGED(WCT_Travel_End_Date__c), RecordType.Name =&apos;Business Visa&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notification sent to travel desk with employee info-Bvisa</fullName>
        <actions>
            <name>notificationsentravelenddateextension</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notification sent to travel desk with employee info-Bvisa</description>
        <formula>AND( ISCHANGED(WCT_Travel_End_Date__c), RecordType.Name =&apos;Business Visa&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OGC Approval Status</fullName>
        <actions>
            <name>OGC_GMITeam</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>OGC_professional_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_OGC_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Report sent to Total Rewards CoE from SFDC from first step in EV Pre-Assignment B</fullName>
        <actions>
            <name>reportsenttototalrewardscoefromsfdcfromfirststepinevpreassignmentb</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Mobility_Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Mobility__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Employment Visa</value>
        </criteriaItems>
        <description>Report sent to Total Rewards CoE from SFDC from first step in EV Pre-Assignment B</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Assignment Type %3D Long Term</fullName>
        <actions>
            <name>Set_Assignment_Type_Long_Term</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Projected_Assignment_Duration__c</field>
            <operation>greaterThan</operation>
            <value>12</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Assignment Type %3D Medium Term</fullName>
        <actions>
            <name>Set_Assignment_Type_Medium_Term</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Projected_Assignment_Duration__c</field>
            <operation>greaterThan</operation>
            <value>4</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Projected_Assignment_Duration__c</field>
            <operation>lessOrEqual</operation>
            <value>12</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Assignment Type %3D Short Term</fullName>
        <actions>
            <name>Set_Assignment_Type_Short_Term</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Projected_Assignment_Duration__c</field>
            <operation>lessOrEqual</operation>
            <value>4</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Travel Duration More than 30 days</fullName>
        <actions>
            <name>WCT_Insurance_Task_Status_c</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>WCT_Travel_Duration__c &gt; 30 &amp;&amp; (( WCT_Travel_Start_Date__c +29) == TODAY())&amp;&amp;  ISPICKVAL(WCT_Mobility_Status__c, &apos;Employee on Travel&apos;) &amp;&amp;  PRIORVALUE(WCT_Insurance_Task_Status__c) != TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Travel Duration More than 30 days for OGC</fullName>
        <actions>
            <name>OGC_Task_status_c</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Travel_Duration__c</field>
            <operation>greaterThan</operation>
            <value>30</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Mobility__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business Visa</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Mobility_Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Travel End Date is extended</fullName>
        <actions>
            <name>Check_Travel_End_Date_Extended</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(PRIORVALUE( WCT_Travel_End_Date__c ) &lt; WCT_Travel_End_Date__c) &amp;&amp;  ISPICKVAL(WCT_Mobility_Status__c, &apos;Employee on Travel&apos;) &amp;&amp;  WCT_Mobility_Type__c = &apos;Business Visa&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Travel End Date is extended New</fullName>
        <actions>
            <name>Check_Travel_End_Date_Extended_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>(WCT_Travel_Duration__c &gt; 30 &amp;&amp;  WCT_Mobility_Type__c = &apos;Business Visa&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Travel extended status change</fullName>
        <actions>
            <name>Mobility_status_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Mobility__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business Visa</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Travel_Duration__c</field>
            <operation>greaterThan</operation>
            <value>30</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Insurance_Task_Status__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Mobility_Status__c</field>
            <operation>equals</operation>
            <value>Employee on Travel</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck %27Is Last Working Day In US%27 when Last Working Day in US changes</fullName>
        <actions>
            <name>WCT_Uncheck_Is_Last_Working_Day_In_US</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( WCT_Last_Working_Day_in_US__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck %27Is Travel End Date Alert%27 when Travel End Date changes</fullName>
        <actions>
            <name>WCT_Uncheck_Is_Travel_End_Date_Alert</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( WCT_Travel_End_Date__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck %27Regenerate Tasks%27 after Tasks are regenerated</fullName>
        <actions>
            <name>uncheckregeneratetasks</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Regenerate_Tasks__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Employee Information On Mobility</fullName>
        <actions>
            <name>Update_Employee_Email_Id_on_Mobility</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_First_Name_on_Mobility</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Function_on_Mobility</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Home_Location_on_Mobility</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Last_Name_on_Mobility</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Service_Area_on_Mobility</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Service_Line_on_Mobility</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>True</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>When Mobility record Queue will be created</fullName>
        <actions>
            <name>mobilityupdaterecordowner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>When Mobility record will be created</fullName>
        <actions>
            <name>mobilityupdaterecordstatus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Mobility_Status__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>escalated notification to the Business Lead%2FRM when employee declines 3 consecutive mobility events</fullName>
        <actions>
            <name>escalated_notification_to_the_Business_Lead_RM_when_employee_declines_3_consecut</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Mobility_Unattended_event_coount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Unattend_Mobility_Event_Count__c</field>
            <operation>equals</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Mobility__c.WCT_Mobility_Status__c</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Ability to automatically trigger an escalated notification to the Business Lead/RM when employee declines 3 consecutive mobility eEvents</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
