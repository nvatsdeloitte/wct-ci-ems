<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Offer_Note_Status_On_Offer</fullName>
        <field>WTC_Latest_Offer_Note_Status__c</field>
        <formula>TEXT(WCT_Offer_Status__c)</formula>
        <name>Update Offer Note Status On Offer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>WCT_Offer__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Offer_declined_Reason</fullName>
        <field>WCT_Declined_Reason__c</field>
        <formula>WCT_Description__c</formula>
        <name>Update Offer declined Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>WCT_Offer__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Offer declined Reason Update rule</fullName>
        <actions>
            <name>Update_Offer_declined_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer_Note__c.WCT_Offer_Status__c</field>
            <operation>equals</operation>
            <value>Offer Declined</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Offer Note Status On Offer</fullName>
        <actions>
            <name>Update_Offer_Note_Status_On_Offer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule will update the WTC_Latest_Offer_Note_Status__c field with the Created Notes status</description>
        <formula>True</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
