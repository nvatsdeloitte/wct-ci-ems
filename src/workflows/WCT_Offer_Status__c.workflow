<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Offer_Status_Update_Owner_Name</fullName>
        <field>WCT_Owner_Name__c</field>
        <formula>IF( ISBLANK( WCT_Related_Offer__r.Owner:Queue.Id) , (WCT_Related_Offer__r.Owner:User.FirstName+&apos; &apos;+ WCT_Related_Offer__r.Owner:User.LastName  ) , WCT_Related_Offer__r.Owner:Queue.QueueName  )</formula>
        <name>Offer Status: Update Owner Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offer_Status_Update_Owner_Profile_queue</fullName>
        <field>WCT_Owner_Profile_Queue_Dev_Name__c</field>
        <formula>If( ISBLANK(WCT_Related_Offer__r.Owner:Queue.QueueName),
      WCT_Related_Offer__r.Owner:User.Profile.Name  ,
      WCT_Related_Offer__r.Owner:Queue.DeveloperName 
)</formula>
        <name>Offer Status: Update Owner Profile/queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offer_Status_Update_Time_in_Status</fullName>
        <field>Time_in_Status__c</field>
        <formula>WCT_Time_in_Status__c</formula>
        <name>Offer Status:Update Time in Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Offer Status%3A Update Owner Name</fullName>
        <actions>
            <name>Offer_Status_Update_Owner_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Offer_Status_Update_Owner_Profile_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer_Status__c.WCT_Owner_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Offer Status%3AUpdate Time in Status</fullName>
        <actions>
            <name>Offer_Status_Update_Time_in_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer_Status__c.WCT_Leave_State_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
