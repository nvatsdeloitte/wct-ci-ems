<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_alert_for_Candidate_application_incomplete</fullName>
        <description>Email alert for Candidate application incomplete</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Recruiter_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>WCT_Recruiter_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>WCT_Team_Mailbox__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>WCT_Offer_Letter_Templates/Candidate_application_is_Incomplete</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_for_missing_offer_details_to_recruiters</fullName>
        <description>Email alert for missing offer details to recruiters</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Recruiter_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>WCT_Recruiter_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>WCT_Team_Mailbox__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>WCT_Offer_Letter_Templates/Email_alert_for_missing_offer_details</template>
    </alerts>
    <alerts>
        <fullName>Offer_Approved</fullName>
        <description>Offer Approved</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Recruiter_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>WCT_Recruiter_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>WCT_Recruiting_Coordinator_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>WCT_Team_Mailbox__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>WCT_Offer_Letter_Templates/WTC_Offer_Approved</template>
    </alerts>
    <alerts>
        <fullName>Offer_Returned_for_Revisions</fullName>
        <description>Offer Returned for Revisions</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Recruiter_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>WCT_Recruiter_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>WCT_Recruiting_Coordinator_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>WCT_Team_Mailbox__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>WCT_Offer_Letter_Templates/WTC_Offer_Returned_for_Revision</template>
    </alerts>
    <alerts>
        <fullName>Offer_has_been_Updated</fullName>
        <description>Offer Letter Mismatch</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Team_Mailbox__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>WCT_Offer_Letter_Templates/Offer_has_been_Updated</template>
    </alerts>
    <alerts>
        <fullName>Offer_is_Ready_for_Review</fullName>
        <description>Offer is Ready for Review</description>
        <protected>false</protected>
        <recipients>
            <field>WCT_Recruiter_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>WCT_Recruiter_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>WCT_Recruiting_Coordinator_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>WCT_Team_Mailbox__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>WCT_Offer_Letter_Templates/WTC_Offer_is_Ready_for_Review</template>
    </alerts>
    <alerts>
        <fullName>WCT_Offer_Analyst_Changed</fullName>
        <description>Offer Analyst Changed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>WCT_Offer_Letter_Templates/WCT_Offer_Analyst_Changed</template>
    </alerts>
    <fieldUpdates>
        <fullName>Check_Manual_Offer_Draft</fullName>
        <field>WCT_Manual_Offer__c</field>
        <literalValue>1</literalValue>
        <name>Check Manual Offer Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_set_values_on_creation</fullName>
        <field>WCT_Set_Values_On_Creation__c</field>
        <literalValue>1</literalValue>
        <name>Check set values on creation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fuel_Expenses_Description_field_update</fullName>
        <field>WCT_Fuel_Expenses_Description__c</field>
        <formula>&quot;Petrol / Driver / Insurance / Repairs &amp; Maintenance&quot;</formula>
        <name>Fuel Expenses Description- field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lock_Record_Type_Conference</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Conference_Offer_Locked</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Lock Record Type Conference</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lock_Record_Type_India_Campus</fullName>
        <field>RecordTypeId</field>
        <lookupValue>India_Campus_Hire_Offer_Locked</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Lock Record Type India Campus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lock_Record_Type_India_Experienced</fullName>
        <field>RecordTypeId</field>
        <lookupValue>India_Experienced_Hire_Offer_Locked</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Lock Record Type India Experienced</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lock_Record_Type_US_Campus</fullName>
        <field>RecordTypeId</field>
        <lookupValue>US_Campus_Hire_Offer_Locked</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Lock Record Type US Campus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lock_Record_Type_US_Experienced</fullName>
        <field>RecordTypeId</field>
        <lookupValue>US_Experienced_Hire_Offer_Locked</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Lock Record Type US Experienced</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Monthly_Total_Salary</fullName>
        <field>WCT_Total_Salary_Monthly__c</field>
        <formula>TEXT( ROUND( WCT_Total_Salary_Annually__c /12, 2))</formula>
        <name>Monthly Total Salary</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offer_Check_OfferApproved_Field</fullName>
        <field>WCT_OfferApproved__c</field>
        <literalValue>1</literalValue>
        <name>Offer:Check OfferApproved Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offer_Update_Returned_for_RevisionChkBox</fullName>
        <field>WCT_Offer_sent_For_Revision__c</field>
        <literalValue>1</literalValue>
        <name>Offer:Update Returned for RevisionChkBox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offer_Update_record_type_to_conference</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Conference_Offer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Offer: Update record type to conference</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offer_change_Owner_to_India_Campus_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Deloitte_India_Campus_Offers</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Offer:change Owner to India Campus Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offer_change_Owner_to_India_Exp_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Deloitte_India_Experienced_Offers</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Offer:change Owner to India Exp Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offer_change_Owner_to_US_Campus_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Deloitte_US_Campus_Offers</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Offer:change Owner to US Campus Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offer_change_Owner_to_US_Exp_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Deloitte_US_Experienced_Offers</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Offer:change Owner to US Exp Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offer_change_rec_type_to_india_exp</fullName>
        <field>RecordTypeId</field>
        <lookupValue>WCT_India_Experienced_Hire_Offer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Offer:change rec type to india exp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offer_change_rectype_to_India_Campus</fullName>
        <field>RecordTypeId</field>
        <lookupValue>WCT_India_Campus_Hire_Offer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Offer:change rectype to India Campus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offer_change_rectype_to_US_Campus</fullName>
        <field>RecordTypeId</field>
        <lookupValue>WCT_US_Campus_Hire_Offer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Offer:change rectype to US Campus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offer_change_rectype_to_US_Exp</fullName>
        <field>RecordTypeId</field>
        <lookupValue>WCT_US_Experienced_Hire_Offer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Offer:change rectype to US Exp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type</fullName>
        <field>WCT_Change_record_type__c</field>
        <literalValue>0</literalValue>
        <name>Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recruiter_Coordinator_Email_Update</fullName>
        <description>This field update will update the recruiter coordinator Email Id</description>
        <field>WCT_Recruiting_Coordinator_Email__c</field>
        <formula>WCT_Recruiting_Coordinator__r.Email</formula>
        <name>Recruiter Coordinator Email Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Revised_Offer_Checkbox</fullName>
        <field>WCT_Revised_Offer_Checkbox__c</field>
        <literalValue>1</literalValue>
        <name>Revised Offer Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Service_area_editable_field_updates</fullName>
        <field>WCT_Service_Area_2__c</field>
        <formula>&quot;Deloitte Advisory&quot;</formula>
        <name>Service area editable field updates</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Declined_Email_alert_sent</fullName>
        <field>WCT_Declined_Email_Alert_Sent__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Declined Email alert sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Offer_Sent_checkbox</fullName>
        <field>WCT_Offer_Letter_Sent__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Offer Sent checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Tracker_Field_updated</fullName>
        <field>WCT_Tracker_Fields_Updated__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Tracker Field updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Agent_State_StartTime</fullName>
        <field>WCT_AgentState_Time__c</field>
        <formula>NOW()</formula>
        <name>Update Agent State Start Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Automated_Status_Field_to_False</fullName>
        <description>This field update updates the automated status change to false</description>
        <field>Automated_Status_Change__c</field>
        <literalValue>0</literalValue>
        <name>Update Automated Status Field to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_EA</fullName>
        <field>WCT_EA_Updated__c</field>
        <literalValue>0</literalValue>
        <name>Update EA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Hiring_Location</fullName>
        <field>WCT_Hiring_Location2__c</field>
        <formula>WCT_Hiring_Location__c</formula>
        <name>Update Hiring Location</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Internship_Start_Date</fullName>
        <field>WCT_Internship_Start_Date__c</field>
        <formula>WCT_Candidate__r.WCT_Internship_Start_Date__c</formula>
        <name>Update Internship Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_NextGenAmt_with_Other_Offer_bonus</fullName>
        <field>WCT_NextGen_Amount__c</field>
        <formula>WCT_Candidate__r.WCT_Other_Offer_Bonus_IFA_Amounts__c</formula>
        <name>Update NextGenAmt with Other Offer bonus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_OfferApproved_Datetime</fullName>
        <field>WCT_Offer_Approved_Date__c</field>
        <formula>NOW()</formula>
        <name>Update OfferApproved Datetime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Offer_Ready_for_Review_DateTime</fullName>
        <field>WCT_Offer_RFR_Time__c</field>
        <formula>NOW()</formula>
        <name>Update Offer Ready for Review Date Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PromiAmt_with_Other_Offer_bonus</fullName>
        <field>WCT_Promisory_Note_IFA_Amount__c</field>
        <formula>WCT_Candidate__r.WCT_Other_Offer_Bonus_IFA_Amounts__c</formula>
        <name>Update PromiAmt with Other Offer bonus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Tentative_Intern_Start_Date</fullName>
        <field>WCT_Tentative_Intern_Start_Date__c</field>
        <formula>Text(WCT_Candidate__r.WCT_Internship_Start_Date__c)</formula>
        <name>Update Tentative Intern Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Tentative_Internship_Start_Date</fullName>
        <field>WCT_Tentative_Intern_Start_Date__c</field>
        <formula>Text(WCT_Candidate__r.WCT_Internship_Start_Date__c)</formula>
        <name>Update Tentative Internship Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Tite_Designation_Editable</fullName>
        <field>WCT_Title_Designation_Function_Ed__c</field>
        <formula>WCT_Candidate__r.WCT_RequisitionHiringLevel__c</formula>
        <name>Update Tite Designation Editable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Title_Agreement_Header_Footer</fullName>
        <field>WCT_Agreement_Title_HF__c</field>
        <formula>WCT_Candidate__r.WCT_RequisitionHiringLevel__c</formula>
        <name>Update Title Agreement Header/Footer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Title_Agreement_Title</fullName>
        <field>WCT_Title3__c</field>
        <formula>WCT_Candidate__r.WCT_RequisitionHiringLevel__c</formula>
        <name>Update Title Agreement Title</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Title_Designation_Function</fullName>
        <field>WCT_Title_Designation_Function_INTERN__c</field>
        <formula>WCT_Candidate__r.WCT_RequisitionHiringLevel__c</formula>
        <name>Update Title Designation Function</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Tracker_Field</fullName>
        <field>WCT_Tracker_Fields_Updated__c</field>
        <literalValue>1</literalValue>
        <name>Update Tracker Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Annual_Basic_Pay</fullName>
        <field>WCT_Annual_Basic__c</field>
        <formula>TEXT( (MAX((CEILING(((( WCT_Total_Salary_Annually__c /12))*0.35)/50)*50),6500)) * 12)</formula>
        <name>Annual Basic Pay</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Annual_Employer_PF</fullName>
        <field>WCT_Annual_Provident_Fund__c</field>
        <formula>TEXT( VALUE( WCT_Provident_Fund__c )  * 12)</formula>
        <name>Annual Employer PF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Annual_HRA</fullName>
        <field>WCT_Annual_HRA__c</field>
        <formula>TEXT(VALUE( WCT_HRA__c )*12)</formula>
        <name>Annual HRA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Annual_LTA</fullName>
        <field>WCT_Annual_LTA__c</field>
        <formula>TEXT((VALUE( WCT_LTA__c ))*12)</formula>
        <name>Annual LTA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Annual_Special_Allowance</fullName>
        <field>WCT_Annual_Spcl_Allowance__c</field>
        <formula>TEXT((VALUE( WCT_Spcl_Allowance__c ))*12)</formula>
        <name>Annual Special Allowance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Monthly_Basic_Pay</fullName>
        <field>WCT_Basic__c</field>
        <formula>TEXT(MAX((CEILING(((( WCT_Total_Salary_Annually__c /12))*0.35)/50)*50),6500))</formula>
        <name>Monthly Basic Pay</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Monthly_Employer_PF</fullName>
        <field>WCT_Provident_Fund__c</field>
        <formula>IF(WCT_International_Worker__c=true, 
TEXT(CEILING(3/28 * (ROUND(WCT_Total_Salary_Annually__c /12, 2) - VALUE(WCT_Basic__c )*0.5))), 
TEXT(IF(VALUE(WCT_Basic__c)*0.12 &gt; VALUE($Label.WCT_Offer_Monthly_PF), VALUE(WCT_Basic__c)*0.12, VALUE($Label.WCT_Offer_Monthly_PF)))
)</formula>
        <name>Monthly Employer PF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Monthly_HRA</fullName>
        <field>WCT_HRA__c</field>
        <formula>TEXT(VALUE( WCT_Basic__c )*0.5)</formula>
        <name>Monthly HRA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Monthly_LTA</fullName>
        <field>WCT_LTA__c</field>
        <formula>TEXT(VALUE( WCT_Basic__c )*0.1)</formula>
        <name>Monthly LTA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Monthly_Medical_Insurance_Premium</fullName>
        <field>WCT_Monthly_Premium__c</field>
        <formula>TEXT( WCT_Annual_Medical_Insurance_Premium_Num__c / 12)</formula>
        <name>Monthly Medical Insurance Premium</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Monthly_Special_Allowance</fullName>
        <field>WCT_Spcl_Allowance__c</field>
        <formula>TEXT(( ROUND( WCT_Total_Salary_Annually__c /12, 2))-( VALUE( WCT_Basic__c ) +  VALUE( WCT_Provident_Fund__c ) +   VALUE( WCT_HRA__c ) +  VALUE( WCT_LTA__c ) + 2200 + 1250 + 1600  ))</formula>
        <name>Monthly Special Allowance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Offer_Letter_Reviewed_Check_box</fullName>
        <description>Once the offer status is changed to approved, offer letter reviewed checkbox is unchecked.</description>
        <field>WCT_Offer_Letter_Reviewed__c</field>
        <literalValue>0</literalValue>
        <name>Offer Letter Reviewed Check box to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Recruiter_Email_Update</fullName>
        <description>This field update will update the email value of recruiter</description>
        <field>WCT_Recruiter_Email__c</field>
        <formula>WCT_Recruiter__r.Email</formula>
        <name>Recruiter Email Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Update_Service_Area</fullName>
        <field>WCT_Service_Area_2__c</field>
        <formula>WCT_Candidate__r.WCT_Service_Area__c</formula>
        <name>Update Service Area</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Update_Service_Line</fullName>
        <field>WCT_Service_Line_2__c</field>
        <formula>WCT_Candidate__r.WCT_Service_Line__c</formula>
        <name>Update Service Line</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Update_Title</fullName>
        <field>WCT_Title2__c</field>
        <formula>WCT_Candidate__r.WCT_RequisitionHiringLevel__c</formula>
        <name>Update Title</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Defaulting Fuel Expenses Description on creation of new offer record</fullName>
        <actions>
            <name>Fuel_Expenses_Description_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.RecordTypeId</field>
            <operation>contains</operation>
            <value>India</value>
        </criteriaItems>
        <description>Fuel Expenses Description will have a default value on creation of new record for India templates</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Manual Offer Draft</fullName>
        <actions>
            <name>Check_Manual_Offer_Draft</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_status__c</field>
            <operation>equals</operation>
            <value>Manual (Outside of SFDC)</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Offer %3A Change Record Type Conference</fullName>
        <actions>
            <name>Lock_Record_Type_Conference</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_status__c</field>
            <operation>equals</operation>
            <value>Offer Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Offer__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Conference Offer</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_Change_record_type__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Offer %3A Change Record Type India Campus</fullName>
        <actions>
            <name>Lock_Record_Type_India_Campus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_status__c</field>
            <operation>equals</operation>
            <value>Offer Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Offer__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>India Campus Hire Offer</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_Change_record_type__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Offer %3A Change Record Type India Campus based on job type and user group</fullName>
        <actions>
            <name>Offer_change_Owner_to_India_Campus_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Offer_change_rectype_to_India_Campus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>WCT_User_Group__c = &apos;US GLS India&apos;&amp;&amp;  NOT(CONTAINS(WCT_Job_Type__c, &apos;Experienced&apos;))&amp;&amp; NOT( CONTAINS(  RecordType.Name,&apos;Intern&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Offer %3A Change Record Type India Exp based on job type and user grp</fullName>
        <actions>
            <name>Offer_change_Owner_to_India_Exp_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Offer_change_rec_type_to_india_exp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>WCT_User_Group__c = &apos;US GLS India&apos;&amp;&amp; CONTAINS(  WCT_Job_Type__c,&apos;Experienced&apos;) &amp;&amp; NOT( CONTAINS(  RecordType.Name,&apos;Intern&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Offer %3A Change Record Type India Experienced</fullName>
        <actions>
            <name>Lock_Record_Type_India_Experienced</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_status__c</field>
            <operation>equals</operation>
            <value>Offer Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Offer__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>India Experienced Hire Offer</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_Change_record_type__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Offer %3A Change Record Type US Campus</fullName>
        <actions>
            <name>Lock_Record_Type_US_Campus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_status__c</field>
            <operation>equals</operation>
            <value>Offer Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Offer__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>US Campus Hire Offer</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_Change_record_type__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Offer %3A Change Record Type US Campus based on job type and user grp</fullName>
        <actions>
            <name>Offer_change_Owner_to_US_Campus_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Offer_change_rectype_to_US_Campus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>WCT_User_Group__c = &apos;United States&apos;&amp;&amp;  NOT(CONTAINS(WCT_Job_Type__c, &apos;Experienced&apos;))&amp;&amp; NOT( CONTAINS(  RecordType.Name,&apos;Intern&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Offer %3A Change Record Type US Exp based on job type and user grp</fullName>
        <actions>
            <name>Offer_change_Owner_to_US_Exp_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Offer_change_rectype_to_US_Exp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>WCT_User_Group__c = &apos;United States&apos;&amp;&amp;  CONTAINS(  WCT_Job_Type__c,&apos;Experienced&apos;)&amp;&amp; NOT( CONTAINS(  RecordType.Name,&apos;Intern&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Offer %3A Change Record Type US Experienced</fullName>
        <actions>
            <name>Lock_Record_Type_US_Experienced</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_status__c</field>
            <operation>equals</operation>
            <value>Offer Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Offer__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>US Experienced Hire Offer</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_Change_record_type__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Offer Analyst</fullName>
        <actions>
            <name>WCT_Offer_Analyst_Changed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( WCT_Offer_Owner__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Offer Analyst Changed</fullName>
        <actions>
            <name>Update_Agent_State_StartTime</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(OR(ISPICKVAL(WCT_status__c, &quot;Offer to Be Extended&quot;), ISPICKVAL(WCT_status__c, &quot;Draft in Progress&quot;), ISPICKVAL(WCT_status__c, &quot;Returned for Revisions&quot;)) , OR(ISCHANGED(WCT_status__c), ISCHANGED(OwnerId)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Offer Approved</fullName>
        <actions>
            <name>Offer_Approved</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Uncheck_Offer_Sent_checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_OfferApproved_Datetime</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WCT_Offer_Letter_Reviewed_Check_box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Offer_Approved</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_status__c</field>
            <operation>equals</operation>
            <value>Offer Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Offer Automated Checkbox Update</fullName>
        <actions>
            <name>Update_Automated_Status_Field_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_status__c</field>
            <operation>equals</operation>
            <value>Offer Declined</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Offer__c.Automated_Status_Change__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow Unchecks the Automated Checkbox to False if status is changed to Pending</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Offer Returned for Revisions</fullName>
        <actions>
            <name>Offer_Returned_for_Revisions</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Offer_Update_Returned_for_RevisionChkBox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Offer_Returned_for_Revisions</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_status__c</field>
            <operation>equals</operation>
            <value>Returned for Revisions</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Offer has been Updated</fullName>
        <actions>
            <name>Update_Tracker_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND($Profile.Name != &apos;6_Integration&apos;, WCT_RecordCreated_from_BulkUpload__c == true, OR(ISCHANGED(WCT_status__c),ISCHANGED(Length_Of_Internship__c), PRIORVALUE(WCT_Start_Date__c)!=WCT_Start_Date__c,  PRIORVALUE(WCT_Hiring_Location__c) !=WCT_Hiring_Location__c,  PRIORVALUE(WCT_Start_Date_Year__c)!=WCT_Start_Date_Year__c,  PRIORVALUE(WCT_Salary__c)!=WCT_Salary__c,  PRIORVALUE(WCT_Sign_On_Bonus__c)!= WCT_Sign_On_Bonus__c,  PRIORVALUE(WCT_Candidate_Relocation_Amt__c)!=WCT_Candidate_Relocation_Amt__c,  PRIORVALUE(WCT_Due_Date__c)!= WCT_Due_Date__c,  PRIORVALUE(WCT_NextGen_Amount__c)!= WCT_NextGen_Amount__c,  PRIORVALUE(WCT_Travel_Required__c)!=WCT_Travel_Required__c,  PRIORVALUE(WCT_Hiring_Partner__c)!=WCT_Hiring_Partner__c,  PRIORVALUE(WCT_Recruiter__c)!= WCT_Recruiter__c,  PRIORVALUE(WCT_Recruiting_Coordinator__c)!=WCT_Recruiting_Coordinator__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Offer is Ready for Review</fullName>
        <actions>
            <name>Offer_is_Ready_for_Review</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Offer_Ready_for_Review_DateTime</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Offer_is_Ready_for_Review</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_status__c</field>
            <operation>equals</operation>
            <value>Ready for Review</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Offer letter Mismatch</fullName>
        <actions>
            <name>Offer_has_been_Updated</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Uncheck_Tracker_Field_updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_Tracker_Fields_Updated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Offer%3A Change Record type to Conference</fullName>
        <actions>
            <name>Offer_Update_record_type_to_conference</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>WCT_User_Group__c = &apos;United States&apos; &amp;&amp; CONTAINS(WCT_Job_Type__c, &apos;Summer Job&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Offer%3A Check Offer Approved checkbox</fullName>
        <actions>
            <name>Offer_Check_OfferApproved_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_status__c</field>
            <operation>equals</operation>
            <value>Offer Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Offer_Set_Values_On_Creation</fullName>
        <actions>
            <name>Update_Internship_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Tentative_Intern_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Tentative_Internship_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Tite_Designation_Editable</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Title_Agreement_Header_Footer</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Title_Agreement_Title</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Title_Designation_Function</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WCT_Update_Service_Area</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WCT_Update_Service_Line</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WCT_Update_Title</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.Name</field>
            <operation>notEqual</operation>
            <value>NULL</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_Set_Values_On_Creation__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Offer_Set_Values_On_Creation2</fullName>
        <actions>
            <name>Check_set_values_on_creation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.Name</field>
            <operation>notEqual</operation>
            <value>NULL</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_Set_Values_On_Creation__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Offer_to_be_Extended</fullName>
        <actions>
            <name>Offer_to_be_Extended</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_status__c</field>
            <operation>equals</operation>
            <value>Offer to Be Extended</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Recruiter Cancel Offer</fullName>
        <actions>
            <name>Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  ISPICKVAL(PRIORVALUE( WCT_status__c ),&quot;Offer Cancelled&quot;),  NOT(ISPICKVAL(( WCT_status__c ),&quot;Offer Cancelled&quot;))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Revised Offer Logic</fullName>
        <actions>
            <name>Revised_Offer_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_status__c</field>
            <operation>equals</operation>
            <value>Offer Sent</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Salary Componenets</fullName>
        <actions>
            <name>WCT_Annual_Basic_Pay</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WCT_Annual_Employer_PF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WCT_Annual_HRA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WCT_Annual_LTA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WCT_Annual_Special_Allowance</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WCT_Monthly_Basic_Pay</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WCT_Monthly_Employer_PF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WCT_Monthly_HRA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WCT_Monthly_LTA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WCT_Monthly_Special_Allowance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_Total_Salary_Annually__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Salary Components 2</fullName>
        <actions>
            <name>Monthly_Total_Salary</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WCT_Monthly_Medical_Insurance_Premium</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_Total_Salary_Annually__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck Declined Email Alert Sent checkbox</fullName>
        <actions>
            <name>Uncheck_Declined_Email_alert_sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Offer status has changed from Offer declined to any other status uncheck the offer declined email alert sent checkbox</description>
        <formula>AND(ISPICKVAL(PRIORVALUE(WCT_status__c), &apos;Offer Declined&apos;), ISCHANGED(WCT_status__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Offer EA</fullName>
        <actions>
            <name>Update_EA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_EA_Updated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Updates service area when the entity Advisary%2FFAS</fullName>
        <actions>
            <name>Service_area_editable_field_updates</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR (3 AND 4)) AND 5</booleanFilter>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_Candidate_Entity__c</field>
            <operation>equals</operation>
            <value>Deloitte Financial Advisory Services LP</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_Candidate_Entity__c</field>
            <operation>equals</operation>
            <value>Deloitte Transactions and Business Analytics LLP</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_Candidate_Entity__c</field>
            <operation>equals</operation>
            <value>Deloitte &amp; Touche LLP</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_Service_Area__c</field>
            <operation>contains</operation>
            <value>Business Risk,Accounting &amp; Valuation,Risk Analytics,Technology Risk</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_Set_Values_On_Creation__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WCT_Offer_Due_Date_Exceeded</fullName>
        <active>false</active>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_Due_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>SFDC will create a logic statement to automatically change an offer letter tracker status to &quot;Pending&quot; when the offer letter is past it&apos;s due date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>WCT_Offer__c.WCT_Due_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>WCT_Recruiter_Email</fullName>
        <actions>
            <name>WCT_Recruiter_Email_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule updates the recruiter email id in the WCT_Recruiter_Email__c</description>
        <formula>ISCHANGED(WCT_Recruiter__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WCT_Recruiting_Coordinator_Email</fullName>
        <actions>
            <name>Recruiter_Coordinator_Email_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule updates the recruiter coordinator email id in the WCT_Recruiting_Coordinator_Email__c</description>
        <formula>ISCHANGED( WCT_Recruiting_Coordinator__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>offer%3A Email alert when Candidate application incomplete</fullName>
        <actions>
            <name>Email_alert_for_Candidate_application_incomplete</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_status__c</field>
            <operation>equals</operation>
            <value>Candidate application incomplete</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>offer%3A Email alert when missing offer details</fullName>
        <actions>
            <name>Email_alert_for_missing_offer_details_to_recruiters</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Offer__c.WCT_status__c</field>
            <operation>equals</operation>
            <value>Missing Offer Details</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Offer_Approved</fullName>
        <assignedToType>owner</assignedToType>
        <description>Task created whenever email alert is sent out</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Offer Approved</subject>
    </tasks>
    <tasks>
        <fullName>Offer_Returned_for_Revisions</fullName>
        <assignedToType>owner</assignedToType>
        <description>Task is created whenever email is sent out</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Offer Returned for Revisions</subject>
    </tasks>
    <tasks>
        <fullName>Offer_is_Ready_for_Review</fullName>
        <assignedToType>owner</assignedToType>
        <description>Task is created whenever email is sent out</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Offer is Ready for Review</subject>
    </tasks>
    <tasks>
        <fullName>Offer_to_be_Extended</fullName>
        <assignedToType>owner</assignedToType>
        <description>Task is created whenever email is sent out</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Offer to be Extended</subject>
    </tasks>
</Workflow>
