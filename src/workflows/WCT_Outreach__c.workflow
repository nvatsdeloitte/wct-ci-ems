<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Completion_Date</fullName>
        <field>WCT_Completed_Date__c</field>
        <formula>now()</formula>
        <name>Completion Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Outreach Completion Date</fullName>
        <actions>
            <name>Completion_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Outreach__c.WCT_Status__c</field>
            <operation>equals</operation>
            <value>5. Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Outreach Reminder</fullName>
        <actions>
            <name>X30_Day_Customer_Follow_up_Reminder</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>X60_Day_Reminder_to_Resolve_Any_Open_Tasks2</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>X_15_day_customer_follow_up_reminder</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>WCT_Outreach__c.WCT_Type__c</field>
            <operation>equals</operation>
            <value>Active Open Enrollment (AOE) Outreach,Cyclical Outreach,Life Event Outreach,Mandatory Partner Retirement Outreach</value>
        </criteriaItems>
        <criteriaItems>
            <field>WCT_Outreach__c.WCT_Type__c</field>
            <operation>equals</operation>
            <value>Partner Promotions Outreach,Payroll Impact Outreach,Retiree Open Enrollment (ROE) Outreach,Spouse / Dependents leaving/coming US,Vender/Tier 3 Management Requests Outreach</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>WCT_Outreach__c.CreatedDate</offsetFromField>
            <timeLength>60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>WCT_Outreach__c.CreatedDate</offsetFromField>
            <timeLength>15</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>X15_day_customer_follow_up_reminder</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>15</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>WCT_Outreach__c.CreatedDate</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>15 day customer follow-up reminder</subject>
    </tasks>
    <tasks>
        <fullName>X30_Day_Customer_Follow_up_Reminder</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>30</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>WCT_Outreach__c.CreatedDate</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>30 Day Customer Follow-up Reminder</subject>
    </tasks>
    <tasks>
        <fullName>X60_Day_Reminder_to_Resolve_Any_Open_Tasks2</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>60</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>WCT_Outreach__c.CreatedDate</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>60 Day Reminder to Resolve Any Open Tasks</subject>
    </tasks>
    <tasks>
        <fullName>X60_day_reminder_to_resolve_any_open_tasks</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>60</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>WCT_Outreach__c.CreatedDate</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>60 day reminder to resolve any open tasks</subject>
    </tasks>
    <tasks>
        <fullName>X_15_day_customer_follow_up_reminder</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>15</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>WCT_Outreach__c.CreatedDate</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>15 Day Customer Follow-up Reminder</subject>
    </tasks>
    <tasks>
        <fullName>X_60_day_reminder_to_resolve_any_open_tasks</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>60</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>WCT_Outreach__c.CreatedDate</offsetFromField>
        <priority>2-Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>60 Day Reminder to Resolve Any Open Tasks</subject>
    </tasks>
</Workflow>
