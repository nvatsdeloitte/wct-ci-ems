<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Error_Message</fullName>
        <field>WCT_Error_Message__c</field>
        <name>Error Message</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Pre_Bi_Stage_Status_to_Corrected</fullName>
        <field>WCT_Status__c</field>
        <literalValue>Corrected</literalValue>
        <name>WCT_Pre-Bi_Stage_Status to Corrected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>WCT_Pre-Bi_Stage_Status to Corrected</fullName>
        <actions>
            <name>Error_Message</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>WCT_Pre_Bi_Stage_Status_to_Corrected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( ISCHANGED( WCT_RMS_ID__c ) , ISCHANGED( WCT_Recruiter_Email__c ) , ISCHANGED( WCT_Candidate_Email__c ), ISCHANGED(  WCT_Requisition_ID__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
