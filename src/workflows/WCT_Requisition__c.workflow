<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Req_Primary_Location_from_Taleo_to_SFDC</fullName>
        <field>WCT_Primary_Location__c</field>
        <formula>WCT_Requisition_Primary_Location__c</formula>
        <name>Req:Primary Location from Taleo to SFDC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Req_UpdateCandidateRequisitioRCToTrue</fullName>
        <field>WCT_RequisitionRCChange__c</field>
        <literalValue>1</literalValue>
        <name>Req:UpdateCandidateRequisitioRCToTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdatePrmiaryLocationToTBD</fullName>
        <field>WCT_Primary_Location__c</field>
        <formula>&quot;TBD&quot;</formula>
        <name>UpdatePrmiaryLocationToTBD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCTUpdate_Requisition_Name_with_Taleo_Id</fullName>
        <description>UpdateS Requisition Name with Taleo Id</description>
        <field>Name</field>
        <formula>WCT_Requisition_Number__c</formula>
        <name>Update Requisition Name with Taleo Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WCT_Prim_Location_to_TBD_ReqId_starts_S</fullName>
        <description>Change “Primary Location&quot; to &quot;TBD&quot; when ReqID begins with an S (campus hires)</description>
        <field>WCT_Primary_Location__c</field>
        <formula>&apos;TBD&apos;</formula>
        <name>Primary Location to TBD ReqId starts S</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Requisition%3AUpdate Primary Location</fullName>
        <actions>
            <name>Req_Primary_Location_from_Taleo_to_SFDC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Default WCT_Primary_Location__c value to  WCT_Requisition_Primary_Location__c except Requisitions Starting with S</description>
        <formula>Not(ISBLANK( WCT_Requisition_Primary_Location__c ) ) &amp;&amp;  NOT(BEGINS( Name , &apos;S&apos;)) &amp;&amp;  (ISCHANGED(WCT_Requisition_Primary_Location__c)|| ISNEW() )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Requisition%3AUpdatePrimaryLocationToTBD when RequisitionIDStartswithS</fullName>
        <actions>
            <name>UpdatePrmiaryLocationToTBD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Requisition__c.Name</field>
            <operation>startsWith</operation>
            <value>S</value>
        </criteriaItems>
        <description>Change “Primary Location&quot; to &quot;TBD&quot; when ReqID begins with an S (campus hires)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Requisition%3AUpdateRecruitingCoorChangeCheckbox</fullName>
        <actions>
            <name>Req_UpdateCandidateRequisitioRCToTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This Rule will update the  RequisitionRC Change Checkbox toTrue. This checkbox would be used in the inline vf page to display a pop up on Requisition Recruter Cordinator change.</description>
        <formula>ISCHANGED(WCT_Recruiting_Coordinator__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Requisition Name with Taleo Id</fullName>
        <actions>
            <name>WCTUpdate_Requisition_Name_with_Taleo_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>WCT_Requisition__c.WCT_Requisition_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
